# TODOS
------------

## 10-05-2017
- [x] CHANGE appStore to _root store
- [x] ADD sass compilation
- [ ] make viewInputs as direct child of appStore
- [ ] make uiState as direct child of appStore
- [ ] set up client env files
- [ ] set up proxy api env file
- [ ] research error handling for micro
- [ ] add jest examples
- [ ] add examples folder
- [ ] add jest config babelrc [link](https://github.com/zeit/next.js/tree/master/examples/with-jest)
- [ ] apply indent styles to all files based on editor config
- [ ] create list of useful help functions to manage arrays objects and maps
    - [ ] coversions
    - [ ] filtering
- [ ] research pretty urls
- [ ] setup babel for node server side [link](https://github.com/babel/example-node-server)
    * looks like its not a desired situation
    * performance issues
    * not so straight forward require runnging node-babel vs just node
- [ ] create api service routes (break in to a microservice)
    - [ ] /api/:service/:action
    - [ ] study [link](https://github.com/snd/url-pattern)
- [ ] learn how to use git attributes in a project and apply to framework
    - [ ] doc [link](https://git-scm.com/docs/gitattributes)
    - [ ] example [link](https://git-scm.com/docs/gitattributes)


## 10-04-2017
- [x] CHANGE baseStore to _root
- [x] ADD inspector component to framework

## 09-29-2017
- [x] change appStore to appStore
- [x] add an apiStore to recieve data from api Calls
## 09/28/2017
- [x] remove utils file
## 09/23/2017
- [x] parse dynamic string litrals


## 09/21/2017
-
- [x] yarn add micro-cors
- [x] yarn add micro-rate-limit


## 09/20/2017
- [x] add 4000 port of external data services
- [x] add .env
    - [x] https://www.npmjs.com/package/dotenv
    - [x] add env keys
- [x] add .gitignore
- [x] add micro-rate-limit
- [o] add our babel transform to our node server.js command
- [x] git init


## 09/17/2017
- [x] add server.js
- [x] add micro
- [x] add micro routes
- [x] add micro cors [link](https://github.com/possibilities/micro-cors)
- [x] add env vars
- [x] add a test page for viewing jest test results
- [x] add yarn
- [x] add __READ.md to subfolders to descript what each folder does
- [x] add api folder
    - [ ] api example
- [x] use yarn instead of npm
- [x] add semantic versioning notes to REF
- [x] explore markdown to json [link](https://www.npmjs.com/package/markdown-to-json)


## 09/13/2017
- [x] add babel.rc
- [x] add next.config.js [🔗](https://github.com/Sly777/ran/blob/master/next.config.js)
- [x] add folders
    * [x] __mocks__
    * [x] __test__
    * [x] _layouts
    * [x] models
    * [x] _getInitialProps
    * [x] static
    * [x] store
        - initRootStore
- [x] add mobx store
- [x] add jest
- [x] add inline react svg
    * $ npm install --save inline-react-svg [ref](https://github.com/zeit/next.js/pull/982)

- [x] add docs
    * TODO | CHANGELOG | DISCUSSION | REF | README
- [x] add static resources
    * [x] /static/img
    * [x] /static/icons