# REFERENCES
------------

list of weblinks related to projet libs and files

#### project

* github location

#### nextjs

* nextjs github [link](https://github.com/zeit/next.js/)

#### mobx

* mobx github [link](https://github.com/mobxjs/mobx)

#### sermantic versioning

* semantic versioning [link](http://semver.org/)

#### yarn

* yarn package manager [link](https://yarnpkg.com/en/)