const ssrHttpCookie = require('cookie')
const jsCookie = require('js-cookie')

const next = require('next')
const routes = require('./routes')
const express = require('express');
const LRUCache = require('lru-cache')
const compression = require('compression')

// const USU = require('./utils/UserSessionUtils')

// server settings
const _IS_DEV = process.env.NODE_ENV !== 'production' // TODO approp name: IS_NOT_PRODUCTION
const ENABLE_CACHE = !_IS_DEV // enable on prod only
const _PORT = parseInt(process.env.PORT, 10) || 8080
const customHost = process.env.HOST;
const _HOST = customHost || null;
const NOPE = 'donotcache'

// load next
const app = next({ dev: _IS_DEV})

// This is where we cache our rendered HTML pages
const   ssrCache = new LRUCache({
    max: 100,
    maxAge: 1000 * 60 * 10 // 10 min
})
ssrCache.reset()

// TODO we've had 2 request handler. Let's simplify to one.

// const UNUSED defaultNextHandler = app.getRequestHandler();
const thirdPartyHandler = routes.getRequestHandler(app, ({req, res, route, query}) => {
    console.log('3rd party (Fridays) route handler. url: ' +req.url)
/*
    Object.keys(route).forEach( key => {
        console.log('Route[ ' +key+ ' ] = ' +route[key])
    })
*/
    if (req.url === '/project/undefined') {
        console.log('Skipping /project/undefined. The cause of this request is TBD.')
        res.send('')
        return
    }

    renderAndCache(req, res, route.page, query)
    //app.render(req, res, route.page, query)
})

// ship it
app.prepare().then(() => {
  const server = express()

  server.use(thirdPartyHandler)
  server.use(compression())

  server.listen(_PORT, _HOST, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${_PORT}`)
  })

})

/*
 * NB: make sure to modify this to take into account anything that should trigger
 * an immediate page change (e.g a locale stored in req.session)
 * TODO: page change? No. Page update? Yes, every incoming event.
 */
function getCacheKey (req) {
    const uid = retrieveUserId(req)
    return !uid? NOPE : (uid+ ':' +req.url)
}

async function renderAndCache (req, res, pagePath, queryParams) {
    const key = getCacheKey(req)
    if (key === NOPE)
        console.warn('################# could not retrieve user id, cannot cache.')
    else
        console.log('################# RENDER AND CACHE ' +key)


    // If we have a page in the cache, let's serve it
    if (ENABLE_CACHE && key !== NOPE && ssrCache.has(key)) {
        console.log('### SSR CACHE: Cache Hit on url ' +req.url+ '. key: ' +key)
        res.setHeader('x-cache', 'HIT')
        res.send(ssrCache.get(key))
        return
    }

    try {
        console.log('### SSR CACHE: Cache miss on url. Fetching ' +req.url+ '. key: ' +key)

        // If not let's render the page into HTML
        const html = await app.renderToHTML(req, res, pagePath, queryParams)

        // Something is wrong with the request, let's skip the cache
        if (res.statusCode !== 200) {
            console.log('### SSR CACHE: Status code not 200, so not caching results. ' +res.statusCode)
            res.send(html)
            return
        }

        // Let's cache this page
        if (ENABLE_CACHE) {
            ssrCache.set (key, html)
            console.log ('### SSR CACHE: Fetch went well, caching ' + req.url)
        }

        res.setHeader('x-cache', 'MISS')
        res.send(html)
    } catch (err) {
        app.renderError(err, req, res, pagePath, queryParams)
    }
}

function retrieveUserId(req) {
    let userId = null

    if (req) { // server side
        console.log("SSR: Checking for User ID: ")
        const cookies = req.headers.cookie;
        if (typeof cookies === 'string') {
            const cookiesJSON = ssrHttpCookie.parse (cookies);
            console.log("cookies: " +cookies)
            userId = cookiesJSON.userId;
            console.log("SSR: User ID is " +userId+ ". type: " +(typeof userId))
        } else {
            console.log("SSR: no cookies from jsCookie")
        }
    } else {
        userId = jsCookie.get('userId')
        console.log("Clientside render: User ID is " +userId)
    }
    return userId
}
