"use strict";

import axios from 'axios'
// import { env, currEnv, EventSource,CardChangeEvents,ProjectChangeEvents,CardDelete,DeleteCommentFromProject,ProjectUpdate,AddCardToProject,
//          AddCommentToProject,CardUpdate,DeleteReactionToCard,DeleteCommentFromCard,AddReactionToCard,AddCommentToCard,ProjectJoin,ToastyEvents,
//          ProjectView,CardMove,CreateCard,ANON,ApiEndpoints,ApiTempKeyName,ApiTempKeyValue,baseUrl,EventHigh,EventMedium,IgnoredEvents,ProjectCreate,
//          ProjectDelete,ProjectLeave,UserAcceptInvite,UserIntegrationCreated } from './serviceWorker/SWConfig'

import { env, currEnv, ANON, ApiTempKeyValue, ApiTempKeyName, baseUrl } from "/Users/garlandjose/workspace/wrangl-reload/pages/sw/SWConfig"
import { getMessage, EventSource,CardChangeEvents,ProjectChangeEvents,CardDelete,DeleteCommentFromProject,ProjectUpdate,AddCardToProject,
    AddCommentToProject,CardUpdate,DeleteReactionToCard,DeleteCommentFromCard,AddReactionToCard,AddCommentToCard,ProjectJoinDeprecated,ToastyEvents,
    ProjectView,CardMove,CreateCard,EventHigh,EventMedium,IgnoredEvents,ProjectCreate,
    ProjectDelete,ProjectTakeBreak,UpdateReactionInCard,UserIntegrationCreated } from '/config/EventConfig'



self.isToasty = (event) => {
    return !!event && (ToastyEvents.indexOf(event.type) !== -1)
}

self.hydrateEvent = (event) => {
    let apiCalls = []
    return new Promise( (resolve, reject) => {

        if (event.userId)
            apiCalls.push( self.getUser(event.userId) )
        if (event.cardId)
            apiCalls.push( self.getCard(event.cardId) )
        if (event.projectId)
            apiCalls.push( self.getProject(event.projectId) )

        Promise.all( apiCalls ).then( responses => {
            event.user = responses.find( r => r.data.user_id !== undefined)
            event.card = responses.find( r => r.data.card_id !== undefined)
            event.project = responses.find( r => r.data.project_id !== undefined)

            resolve( event )
        }).catch( error => {
            throw new Error(error)
        })
    })
}


let msgOptions = {
    // Visual
    body: "<String>",
    icon: "https://pogo.io/static/img/logo2.png",
    image: "https://pogo.io/static/img/logo2.png",
    badge: "https://pogo.io/static/img/logo2.png",
    vibrate: "[500,110,500,110,450,110,200,110,170,40,450,110,200,110,170,40,500]",
    sound: "https://pogo.io/static/audio/pop_drip.wav",
    dir: "auto"

    // Behavioral
    // "tag": "<String>",
    // "data": {foo: 1, marc: 2}
    // "requireInteraction": "false",

    // "renotify": "<Boolean>",
    // "silent": "<Boolean>",

    // Both
    // "actions": "<Array of Strings>",

    // Info, no visual effect
    // "timestamp": "<Long>"
}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}


// 2 notifs are mergeable iff they have the same actors and actions. Targets will generally not match.
self.isMergeable = function(notifA, notifB) {
    return false
    if (!notifA.userId || !notifB.userId || !notifA.type || !notifB.type)
        return false;

    return (notifA.userId === notifB.userId && notifA.type === notifB.type)
}

// Immediate Claim
self.addEventListener('install', function(event) {
    event.waitUntil(self.skipWaiting());
});

self.addEventListener('activate', function(event) {
    event.waitUntil(self.clients.claim());
});

self.addEventListener('push', function(event) {
    self.eventData = null
    if (event.data) {
        console.log('Received push notification: ', event.data.text());
        self.eventData = event.data.json()
        debugger

    } else {
        console.log('Received push event with no data.');
    }

    // ignore events we don't care about
    // if (ignoredEvents.indexOf(eventData.eventType) >= 0) {
    //     return
    // }

    event.waitUntil( // entire promise chain inside waituntil to keep service   worker alive
        registration.getNotifications().then(function(openNotifs) {
            let notif, mergeableNotif = null, testN = 0;
debugger
            // Find mergeable notification(s)
            for (let i = 0; i < openNotifs.length; i++) {
                if (openNotifs[i].data) {
                    if (self.isMergeable (openNotifs[i].data, self.eventData)) {
                        mergeableNotif = openNotifs[i];
                        testN++;
                    }
                }
            }
            console.log ("### found " + testN + " mergeable notifications.")

            // Combine new event with existing, and close existing.
            if (mergeableNotif !== null) {
                let oldCount = mergeableNotif.data.count || 1
                self.eventData.count += oldCount
                mergeableNotif.close ()
            }

            // ready to notify
            self.clients.matchAll ().then (function (clientList) {
                let sendOfflinePush = false
                var hasFocusedClients = clientList.some (function (client) {
                    return client.focused;
                });

                if (hasFocusedClients) {
                    // user is active. in-app notif will suffice.
                } else if (clientList.length > 0) {
                    // wrangl is open somewhere, but isn't the active tab/window.
                    sendOfflinePush = true
                } else {
                    // wrangl isn't currently open anywhere.
                    sendOfflinePush = true
                }

                if (sendOfflinePush) { // TODO temp: add true to Always send
                    self.hydrateEvent (self.eventData).then( eventData => {
                        msgOptions.body = getMessage(eventData, 1)
                        msgOptions.data = eventData
                        msgOptions.tag = eventData.eventType

                        // return self.registration.showNotification ("Wrangl Dev " + eventData.eventType.capitalize(), msgOptions);
                        return self.registration.showNotification (eventData.wrangl.title, msgOptions);
                    })
                }
                return null
            })
        })
    )
});

self.addEventListener('notificationclick', function(event) {
    let eventData = event.notification.data
    let projectUrl = currEnv.baseUrl + "project/" +eventData.projectId
    return clients.openWindow(projectUrl);

    // this fails to find existing tabs, so above we just open new tab by default.
/*
    event.waitUntil(
        self.clients.matchAll().then(function(clientList) {
            if (clientList.length > 0) {
                return clientList[0].focus ();
            }
            return self.clients.openWindow(wranglUrl);
        })
    );
*/

});

self.getUser = (userId) => {
    if (!userId) {
        debugger
        // throw new Error('getUserById: missing userId')
    }
    return axios({
        method: 'POST',
        headers: {
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: userRead,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            "user_id" : userId
        }
    })
}

self.getCard = (cardId) => {
    if (!cardId) {
        debugger
        // throw new Error('getUserById: missing userId')
    }
    return axios ({
        method: 'POST',
        headers: {
            // 'Content-Type': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardRead,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            "card_id": cardId
        }
    })
}

self.getProject = (projectId) => {
    if (!projectId) {
        debugger
        // throw new Error('getUserById: missing userId')
    }
    return axios ({
        method: 'POST',
        headers: {
            // 'Content-Type': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: projectRead,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            "project_id": projectId,
            "log_event": false

        }
    })
}

