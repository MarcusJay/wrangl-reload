import {Component} from 'react'
import LoginOrSignup from '/components/Account/LoginOrSignup'
import {appLayout} from '~/_layouts'

import uiStateStore from '/store/uiStateStore'
import UserSessionUtils from "../utils/UserSessionUtils";
import userStore from "../store/userStore";
import RU from "../utils/ResponsiveUtils";

class PageSignIn extends Component {

    static async getInitialProps(ctx) {
        // TODO note
        // next returns the following:
        /*
         ctx err = undefined
         ctx req = [object Object]
           - contains about 30 undefined fields
         ctx res = [object Object]
         - contains about 30 undefined fields
         ctx pathname = /login
         ctx query = [object Object]
         - is unparseable, throws error 'cannot convert object to primitive' , even while trying to print individual fields.
         - I suspect it contains circular references as well.
         ctx asPath = /login/asdf?jgjgjgjgjg

        Due to above issues, ignore req, res, and query. Just grab the pathname and asPath, which is enough.
         */

/*
        for (var prop in ctx) {
            if (typeof ctx[prop] !== 'object')
                console.log('ctx ' +prop+ ' = ' +ctx[prop])
            else if (prop !== 'req' && prop !== 'res') {
                let obj = ctx[prop]
                for (var p in obj) {
                    console.log('object ' +prop+ ' field: ' +obj.p)
                }
            }
        }
*/
        const ua = req? req.headers['user-agent'] : null
        const device = RU.getDeviceType(ua)
        const deviceProps = RU.getDeviceProps(ua)


        let userId = UserSessionUtils.retrieveUserId(req)
        let user
        if (userId) {
            user = await userStore.getUserById (userId)
        }

        const {req, pathname, query, asPath} = ctx
        const isServer = !!req;

        // console.log('login page. SSR: ' +SSR+ ". query: " +query+ ", query.id: " +query.id)
        // console.log('login page. pathname: ' +pathname)
        // console.log('login page. asPath: ' +asPath)
        // console.log('login page. url: ' +req.url)


        // get project if any
        let projectId = asPath.replace(/\/login\/?/,'')
        let qIndex = projectId.indexOf('?')
        if (qIndex > 0)
            projectId = projectId.substring(0,qIndex)

        // skip login if possible
        if (user) {
            if (projectId && projectId.length > 30 )
                res.redirect('/project/' +projectId )
            else
                res.redirect('/')
            return
        }
        console.log ('Login page. SSR: ' + !!req + ". path: " + pathname+ ", asPath: " + asPath+ ', projectId: ' +projectId)
        return {projectId: projectId, pathname: pathname, asPath: asPath, userId: userId, user: user,
                deviceProps: deviceProps, device: device
        }
    }


    constructor(props) {
        super (props)
        uiStateStore.currentPage = uiStateStore.LS
        RU.device = props.device
        RU.setDeviceProps(props.deviceProps)

        if (props.user || props.userId) {
            console.error("User exists and yet we're logging in. Why? ")
            debugger
        }
    }

    render() {
        const range = "bcdefg"
        const idx = Math.floor(Math.random() * range.length)
        const bgPic = '/static/img/bgs/' +range.charAt(idx)+ '.jpg'

        return(
            <div className="landing">
                <div className="transparent"></div>
                <div className="topBar centered">
                    <span className='welcome'>Welcome to <span className="wrangle">Pogo.io</span></span>
                    <span className="detail">Private Beta</span>
                </div>
                <div className="centered loginWrap">
                    <LoginOrSignup show='login' projectId={this.props.projectId}/>
                </div>
                <div className="legal bottom ">
                    (c) 2018 Nudge, Inc.
                    <a href='/static/tos.html' target='_blank'>Terms of Service</a>
                </div>
                <div className="credit bottom ">
                    Photo by Joshua Ness
                </div>

            </div>
        )

    }

}
export default appLayout(PageSignIn)