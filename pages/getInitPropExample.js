import {Component} from 'react'

import Router from 'next/router';

import {Inspector} from 'react-inspector';

// @observer
export default class ProjectsPage extends Component {

    static async getInitialProps({req, query, pathname}) {
        const isServer = !!req;
        let initProps = {
            query : query,
            path: pathname,
            'isServer' : isServer,
        }

        // pass to constructor
        return initProps
    }

    constructor(props) {
        super (props)
    }

    componentWillMount() {
    }

    render() {
        return(
            <div>
                Via next/Router
                <Inspector data={Router.router} />

                Via getInitialProps
                <Inspector data={this.props} />
            </div>
        )
    }
}
