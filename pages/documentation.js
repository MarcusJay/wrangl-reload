// framework
import {Component} from 'react'
import PropTypes from 'prop-types'
import {observer} from 'mobx-react'
// init preloads (async data calls)
// export { Initialize } from '~/_getInitialProps';
import {Initialize} from '~/_getInitialProps'
// html
import Head from 'next/head'
import {BaseLayout} from '../_layouts'
// external
import Markdown from 'markdown-to-jsx';
// ui
import {Grid, Segment, Tab} from 'semantic-ui-react'
// markdown
import mdTODO from '~/_TODO.md';
import mdREADME from '~/README.md';
import mdDISCUSSION from '~/_DISCUSSION.md';
import mdREF from '~/_REF.md';
import mdCHANGELOG from '~/CHANGELOG.md';

// api
// import { bittrex } from '../services/api/bittrex'

// helpers
// import { pricingSplits } from '../utils/pricingSplits'
// import { easing } from '../utils/easing'

// https://github.com/zeit/next.js/issues/1498
@observer
class pageHome extends Component {

    render() {
        return (
            <BaseLayout {...this.props}>
            <Head>
            <title>Documentation</title>
                <style jsx>{`
                    pre {
                      font-size: .8rem;
                      white-space: pre;
                    }
                    [type=checkbox], [type=radio] {
                        box-sizing: border-box;
                        padding: 0;
                        margin-right: 10px;
                    }
                `}</style>
            </Head>
            <Grid stackable columns={1}>
                <Grid.Column>
                  <Segment>
                    <Tab menu={{ secondary: true }} panes={[
                          { menuItem: '📕  README', render: () => <Tab.Pane attached={false}><Markdown>{mdREADME}</Markdown></Tab.Pane> },
                          { menuItem: '☑️  TODO', render: () => <Tab.Pane attached={false}><Markdown>{mdTODO}</Markdown></Tab.Pane> },
                          { menuItem: '💬  DISCUSSION', render: () => <Tab.Pane attached={false}><Markdown>{mdDISCUSSION}</Markdown></Tab.Pane> },
                          { menuItem: '📚  REFERENCES', render: () => <Tab.Pane attached={false}><Markdown>{mdREF}</Markdown></Tab.Pane> },
                          { menuItem: '📜  CHANGELOG', render: () => <Tab.Pane attached={false}><Markdown>{mdCHANGELOG}</Markdown></Tab.Pane> },
                          { menuItem: '☠️  ISSUES', render: () => <Tab.Pane attached={false}><div>issues tracking</div></Tab.Pane> },
                          { menuItem: '📉  ACTIVITY', render: () => <Tab.Pane attached={false}><div>project activity</div></Tab.Pane> }
                        ]} />
                  </Segment>
                </Grid.Column>
            </Grid>
            </BaseLayout>
        )
    }
}


export default Initialize(pageHome)



pageHome.propTypes = {
    document: PropTypes.shape({
        title: PropTypes.string,
        ogImage: PropTypes.string
    })
}

pageHome.defaultProps = {
    document: {
        title: 'template page',
        ogImage: ''
    }

}