import {Component} from 'react'
import {Link, Router} from '~/routes'
import {Grid} from 'semantic-ui-react'
import {observer} from 'mobx-react'
import PusherService from '/services/PusherService'

import {appLayout} from '~/_layouts'
import projectStore from '/store/projectStore'
import uiStateStore from '/store/uiStateStore'
import userStore from '/store/userStore'
import categoryStore from '/store/categoryStore'
import cardStore from '/store/cardStore'
import integrationStore from "/store/IntegrationStore";

import PLTitlePane from '/components/ProjectSummaries/PLTitlePane'
import PLTopLeftPane from '/components/ProjectSummaries/PLTopLeftPane'
import PLWorkPane from '/components/ProjectSummaries/PLWorkPane'
import withDragDropContext from '/components/common/WithDragDropContext'

import RU from '/utils/ResponsiveUtils'
import UserSessionUtils from '/utils/UserSessionUtils'
import {UserRole} from "../config/Constants";
import eventStore from "../store/eventStore";
import {createPreview} from "../utils/ResponsiveUtils";
import {Preview} from "react-dnd-multi-backend";
import LeftPane from "../components/common/LeftPane";
import PGMobileSidebar from "../components/common/PGMobileSidebar";
import ErrorBoundary from "../components/common/ErrorBoundary";

// Optional Redirects, since this is /
// TODO use custom server.js routing instead of here?
// const MICHELLE = 'michelle.wrangle.me'

@observer
class ProjectsPage extends Component {

    static async getInitialProps(ctx) {
        const { req, res, query } = ctx
        const SSR = !!req
        const ua = req? req.headers['user-agent'] : null
        const device = RU.getDeviceType(ua)
        const deviceProps = RU.getDeviceProps(ua)

        // Note: ssr caching returns entire JS without evaling this serverside code.

        console.log('@ / Host: ' +(req? req.headers.host : 'no req'))

        let userId = UserSessionUtils.retrieveUserId(req)
        if (!userId) {
            console.log('Redirecting to https://home.pogo.io')
            res.redirect('https://home.pogo.io' )
            return
        }

        // note: client: 1 userStore per user session.
        //   serverside: 1 userStore per server instance
        const userP = userStore.getUserById (userId)
        const prefsP = userStore.getUserPrefs (userId) // need to know which placeholder loading anim
        const [user, prefs] = await Promise.all([userP, prefsP])
        if (!user) {
            console.log('Redirecting to https://home.pogo.io')
            res.redirect('https://pogo.io' )
            return
        } else {
            userStore.setCurrentUser( user, true )
        }

        return { user: user, prefs: prefs, device: device, deviceProps: deviceProps, ssr: SSR}
    }

    constructor(props) {
        super (props)

        integrationStore.init(props.user.id, '')
        uiStateStore.currentPage = uiStateStore.PP
        userStore.setCurrentUser (props.user, true)
        userStore.setUserPrefs (props.prefs)
        projectStore.clearCurrentProject()

        // userStore.userPrefs = props.userPrefs
        // userStore.setUserConnections (props.connections)
        // userStore.followConnections()
        // userStore.userTags = props.userTags
        // categoryStore.userCategories = props.userCats
        // categoryStore.userCategories = props.userCategories
        // cardStore.userCards = ArrayUtils.arrayUnique(props.userCards)
        // projectStore.setUserProjects(props.projects, props.folio)
        // projectStore.setCurrentProject(null)

        RU.device = props.device
        RU.setDeviceProps(props.deviceProps)

        // Fetching unseen events here rather than initialProps, to assure dependencies like userProjects are met.
        // eventStore.getInstance().fetchUnseenEventsNew(props.user.id)


        this.state = {
            isDark: false,
            // userTags: [],
            showTags: true
        }
        this.setIsDark = this.setIsDark.bind(this)
    }

    onSidebarOpen = (ev) => {
        uiStateStore.toggleSidebar()
    }

    setIsDark(isDark) {
        this.setState({isDark: isDark})
    }

    createProject() {
        projectStore.createProject().then(project => {
            Router.pushRoute('/project/'+project.id, {id: project.id})
        })
    }

    createProjects(n, baseName, catId) {
        if (n < 0 || n > 50) {
            return
        }

        if (!baseName)
            baseName = 'New Board'
        if (n > 1)
            projectStore.createProjects(n, baseName).then(response => {
                uiStateStore.setCreatingProjects(false)
            })

        // single project, put in approp category
        else
            projectStore.createProject().then(project => {
                if (catId) {
                    categoryStore.assignProjectToCategory(catId, project.id)
                }
                // but also go to it anyway, hoping user realizes it's in right place per MC
                Router.pushRoute('/project/'+project.id, {id: project.id})
            })

    }

    leaveProject(projectId) {
        const userId = userStore.currentUser.id
        projectStore.removeUserFromProject(projectId, userId, userId, false).then( response => {
            projectStore.getUserProjects().then( results => {
                // projectStore.userProjects = projects // redundant. Trying to trigger mobx.
            })
        })
    }

    deleteProject(projectId) {
        const project = projectStore.getProjectFromUserProjects(projectId)
        projectStore.deleteProject(project).then( response => {
            projectStore.getUserProjects().then( results => {
            })
        })
    }

    addUserToProject(projectId, newMemberId, trigger) {
        let project = projectStore.getProjectFromUserProjects(projectId)
        let newMember = userStore.getUserFromConnections(newMemberId)
        let currentUserId = userStore.currentUser.id

        projectStore.addUserToProject(projectId, currentUserId, newMemberId, trigger, UserRole.INVITED).then( response => {
            projectStore.getUserProjects().then( results => {
                // projectStore.userProjects = projects // redundant. Trying to trigger mobx.
            })
        })
        return {project: project, user: newMember}
    }

    importCardsToProject(cards, project) {
        cardStore.cloneCardsIntoProject(cards, project).then( response => {
            // TODO support multi drag
            project.cardTotal += 1
        })
    }

    importAssetsToProject(assets, project, source) {
        if (!assets || assets.length === 0 || !project)
            throw new Error("importAssets: no assets or project.")

        cardStore.createCardsFromFiles (assets, project, source).then (response => {
            project.cardTotal += 1
            cardStore.attachDropboxImagesToCards (response.data.cards, assets, this.showImportProgress.bind(this))
        })
    }

    filterByUser(userId) {
        if (userId) {
            console.log("filtering by user: " +userId)
            projectStore.filterProjectsByUserId(userId)
        } else {
            console.log("removing user filter")
            projectStore.resetFilters()
        }
    }

    filterByTag(tag) {
        if (tag) {
            console.log("filtering by tag: " +tag)
        } else {
            console.log("removing tag filter")
        }
    }

    sortBy(key) {
        projectStore.getUserProjects(userStore.currentUser.id, key)
    }

    toggleTags() {
        this.setState({showTags: !this.state.showTags})
    }

    logout = () => {
        UserSessionUtils.endUserSession()
        window.location.href = '/login'
    }

    generatePreview(type, item, style) {
        return createPreview(type, item, style)
    }

    componentWillReact() {
        console.log('Projects Page will react.')
    }

    componentDidMount() {
        // moved out of getInitialProps 7.23.18 for faster page loads
        const userId = userStore.currentUser.id

        // data fetching
        // no 'then' promise handlers here, since stores have updated their own observables

        const evs = eventStore.getInstance()
        // evs.fetchUnseenEventsNew(userId)
        evs.getUserBadges()

        PusherService.connect() // will (re)connect, and (re)follow our active user

    }

    render() {
        // const {showSidebar} = this.state

        const device = RU.getDeviceType()
        const mobile = ['computer','tablet'].indexOf(device) === -1
        const leftPaneCols = uiStateStore.voMode? 0 : uiStateStore.minimizeLeftCol? 1 : uiStateStore.maximizeLeftCol? 8 : 3
        const workCols = 16 - leftPaneCols
        const slpState = uiStateStore.showSidebar? ' open':' closed'
        const midState = uiStateStore.showSidebar? '':' expanded'

        return(
            <div>

                {/*<DevPane />*/}
                <Preview generator={this.generatePreview} />

                {/* Desktop Layout */}
                {['computer','tablet'].indexOf(device) !== -1 &&
                    <Grid celled style={{marginTop: 0, marginBottom: 0, height: '100%', position: 'fixed'}} stretched>
                        <Grid.Row className="framing">
                            <Grid.Column width={leftPaneCols} className={'jSideBG SLP' +slpState} style={{paddingBottom: 0}} >
                                <PLTopLeftPane />
                            </Grid.Column>
                            <Grid.Column className='jMidBG' width={workCols} style={{paddingBottom: '0', paddingLeft: '1.5rem'}} stretched>
                                <PLTitlePane projects={projectStore.userProjects}
                                             user={userStore.currentUser}
                                             totalProjects={projectStore.totalProjects}
                                             createProjects={(n,name) => {this.createProjects(n,name)}}
                                />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row style={{height: '92%'}} stretched>
                            <Grid.Column width={leftPaneCols} className={'darkFraming pad0 SLP' +slpState} style={{ height: '101%', marginTop: '-1px', paddingLeft: '0!important'}}>
                                <ErrorBoundary>
                                    <LeftPane projects={projectStore.userProjects}
                                            totalProjects={projectStore.totalProjects}
                                            createProjects={(n,name) => {this.createProjects(n,name)}}
                                            user={userStore.currentUser}
                                            userCards={cardStore.userCards}
                                            connections={userStore.currentConnections}
                                            totalConnections={userStore.totalConnections}
                                            filterByUser={this.filterByUser}
                                            filterByTag={this.filterByTag}
                                            // userCats={categoryStore.userCategories}

                                    />
                                </ErrorBoundary>
                            </Grid.Column>
                            <Grid.Column className={'jMidBG FMP' +midState} width={workCols} style={{ padding: '0 0 0 1.5rem'}} stretched>
                                <ErrorBoundary>
                                <PLWorkPane
                                            sortBy={(key) => this.sortBy(key)}
                                            // update={this.props.url.query.update}
                                            mobile={false}
                                            showTags={this.state.showTags}
                                            toggleTags={() => {this.toggleTags()}}
                                            onDark={this.setIsDark}
                                            readOnly={false}
                                            totalProjects={projectStore.totalProjects}
                                            currentUser={userStore.currentUser}
                                            users={this.props.connections}
                                            totalConnections={userStore.totalConnections}
                                            createProjects={(n,name,catId) => {this.createProjects(n,name,catId)}}
                                            addUserToProject={(projectId, newMemberId, trigger) => {this.addUserToProject(projectId, newMemberId, trigger)}}
                                            importCards={(cards, project) => {this.importCardsToProject(cards, project)}}
                                            importAssets={(assets, project) => {this.importAssetsToProject(assets, project)}}
                                            leaveProject={(projectId) => {this.leaveProject(projectId)}}
                                            deleteProject={(project) => {this.deleteProject(project)}}
                                            />
                                </ErrorBoundary>
                            </Grid.Column>
                        </Grid.Row>

                        <style jsx>{`
                          height: 100%;
                          overflow-y: hidden;
                        `}</style>
                    </Grid>
                }

                {/* Mobile Layout */}
                {device === 'mobile' &&
                <span>
                  <Grid stretched className='hidden-scroll fullW' style={{marginTop: 0, marginBottom: 0, height: '98.5%', position: 'fixed'}}>
                    <Grid.Row className="mFraming flush fullW">
                        <Grid.Column width={16} style={{paddingBottom: '0 !important'}}>
                            <PLTitlePane projects={projectStore.userProjects}
                                         user={userStore.currentUser}
                                         totalProjects={projectStore.totalProjects}
                                         createProjects={(n,name) => {this.createProjects(n,name)}}
                                         mobile/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row style={{height: '96%', paddingTop: 0}}>
                        <Grid.Column width={16} >
                            <PLWorkPane
                                mobile
                                // update={this.props.url.query.update}
                                showTags={this.state.showTags}
                                toggleTags={() => {this.toggleTags()}}
                                onDark={this.setIsDark}
                                readOnly={false}
                                totalProjects={projectStore.totalProjects}
                                currentUser={userStore.currentUser}
                                users={this.props.connections}
                                totalConnections={userStore.totalConnections}
                                createProjects={(n,name,catId) => {this.createProjects(n,name,catId)}}
                                importCards={(cards, project) => {this.importCardsToProject(cards, project)}}
                                importAssets={(assets, project) => {this.importAssetsToProject(assets, project)}}
                                addUserToProject={(projectId, newMemberId, trigger) => {this.addUserToProject(projectId, newMemberId, trigger)}}
                                leaveProject={(projectId) => {this.leaveProject(projectId)}}
                                deleteProject={(project) => {this.deleteProject(project)}}
                            />
                        </Grid.Column>
                    </Grid.Row>

                    <style jsx>{`
                                height: 100%;
                                overflow-y: hidden;
                            `}</style>
                  </Grid>
                    <PGMobileSidebar
                        component={
                            <LeftPane   projects={projectStore.userProjects}
                                        totalProjects={projectStore.totalProjects}
                                        createProjects={(n,name) => {this.createProjects(n,name)}}
                                        user={userStore.currentUser}
                                        userCards={cardStore.userCards}
                                        connections={userStore.currentConnections}
                                        totalConnections={userStore.totalConnections}
                                        filterByUser={this.filterByUser}
                                        filterByTag={this.filterByTag}
                                        mobile
                            />
                        }

                    >
                    </PGMobileSidebar>

                </span>
                }

            </div>
        )
    }
}

let sortObjectsByIdList = function(objectList, idList) {
    let sortedObjects = []
    let obj
    idList.forEach( (id, i) => {
        obj = (i < objectList.length && objectList[i].id === id)? objectList[i] : objectList.find( obj => obj.id === id ) // first check is to avoid unnecessary find() if projects are unsorted, as array orders be the same.
        if (obj !== undefined)
            sortedObjects.push(obj)
    })
    return sortedObjects
}


export default appLayout(withDragDropContext(ProjectsPage))