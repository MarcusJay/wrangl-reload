import {Component} from 'react'
import {appLayout} from '~/_layouts'
import {Container, Dimmer, Header, Loader, Segment} from "semantic-ui-react";

import uiStateStore from '/store/uiStateStore'
import UserApiService from "../services/UserApiService";
import userStore from "../store/userStore";
import UserSessionUtils from "../utils/UserSessionUtils";


class code extends Component {

    static async getInitialProps(ctx) {
        const { req, res } = ctx

        let userId = UserSessionUtils.retrieveUserId(req)
        if (!userId) {
            res.redirect('/login/code?code')
            return
        }

        let user = await userStore.getUserById (userId)
        if (!user) {
            res.redirect('/login/code?code')
            return
        }

        console.log('Requesting user account code for ' +user.displayName)
        let codeResponse = await UserApiService.createUserAccountCode(user.id)
        console.log('Received code ' +codeResponse)

        return {accountCode: codeResponse.data.link_code}
    }


    constructor(props) {
        super (props)
        uiStateStore.currentPage = uiStateStore.AC
        this.state = {accountCode: props.accountCode || '', loading: false}
    }

    componentWillMount() {
    }

    componentDidMount() {
        let a =1
    }

    render() {
        const { accountCode } = this.state
        let chars = []
        for (let i=0; i< accountCode.length; i++)
            chars.push(accountCode.charAt(i))

        // const {showSuccess, showFailure, passwordError} = this.state
        // const showForm = !showSuccess && !showFailure
        const pageStyle = {boxShadow: '0 0 40px rgba(0,0,0,0.3) !important', border: '1px solid $gBorder !important'}

        return (
            <div className="landing">
                <div className="transparent"></div>
                <div className="topBar centered">
                    <span className='welcome'>Account Link Code</span>
                    <span className="detail">Pogo.io Private Beta</span>
                </div>
                <div className="centered loginWrap"  style={{width: '60%'}}>
                    <div style={{textAlign: 'center'}}>
                        <Dimmer.Dimmable dimmed={this.state.loading}>
                            <Dimmer active={this.state.loading} inverted>
                                <Loader inverted style={{marginTop: '0', height: '10%'}}>Generating...</Loader>
                            </Dimmer>
                            <Container className='loginPage' style={{minHeight: '30rem'}}>
                                <div className="centered">
                                    <Segment padded  className="loginBox" style={pageStyle}>
                                        <div>
                                            <Header>Account Link Code</Header>
                                            <div>Enter this code on your mobile device. </div>
                                            <div className='codeOuter'>
                                                {chars.map( char => {
                                                    return <span className='codeChar'>{char}</span>
                                                })}
                                            </div>
                                        </div>
                                    </Segment>

                                </div>

                            </Container>
                        </Dimmer.Dimmable>
                    </div>

                </div>
                <div className="legal bottom ">
                    (c) 2018 Nudge, Inc.
                    <a href='/static/tos.html' target='_blank'>Terms of Service</a>
                </div>
                <div className="credit bottom ">
                    Photo by Joshua Ness
                </div>

            </div>
        )

    }

}
export default appLayout(code)