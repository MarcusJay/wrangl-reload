import {Component} from 'react'
import {appLayout} from '~/_layouts'
import {Button, Icon, Loader} from 'semantic-ui-react'
import integrationStore from '/store/IntegrationStore'
import DropboxService from "../integrations/DropboxService";
import {isBrowser} from "../utils/solo/isBrowser";
import {urlParamsToJson} from "../utils/solo/urlParamsToJson";

class dropboxAuth extends Component {

static async getInitialProps(ctx) {
        const { req, res, query, err, pathname, asPath } = ctx

        // TODO note
        // next returns the following:
        /*
         ctx err = undefined
         ctx req = [object Object]
           - contains about 30 undefined fields
         ctx res = [object Object]
         - contains about 30 undefined fields
         ctx pathname = /login
         ctx query = [object Object]
         - is unparseable, throws error 'cannot convert object to primitive' , even while trying to print individual fields.
         - I suspect it contains circular references as well.
         ctx asPath = /login/asdf?jgjgjgjgjg

        Due to above issues, ignore req, res, and query. Just grab the pathname and asPath, which is enough.
         */

        return {pathname: pathname, asPath: asPath}
    }


    constructor(props) {
        super (props)
        this.state = {loading: true, details: '', headline: ''}
        // console.log('Dropbox Auth Results: ', props.req, props.res, props.query, props.err, props.pathname, props.asPath)
        if (isBrowser()) {
            const urlPath = props.url.asPath
            const params = urlParamsToJson(urlPath)
            this.isError = params['error'] !== undefined // === 'access_denied'

            this.setState({headline: 'Connecting...', tokenPending: true})
            if (!this.isError) {
                const state = params['/dropboxAuth?state']
                const code = params['code']
                // const stateParams = Utils.urlParamsToJson(state)
                const userId = state.substring(2, state.indexOf('|'))
                DropboxService.requestToken(code).then( response => {
                    debugger
                    const token = response.data.token
                    this.setState({tokenPending: false, details: 'You can now access your dropbox files in Pogo.io!', headline: 'SUCCESS'})
                    integrationStore.setDropboxAccessToken(userId, token)
                })


            } else {
                this.setState({details : "If you would like access to your dropbox files, please Authorize Pogo.io."})
                this.note = "Note: We will never move, edit, or alter your files in any way. We only link to them."
            }
            // this.setState({loading: false})
        }
    }

    closeWindow() {
        self.close()
    }

    componentDidMount() {
        this.setState({loading: false})
    }

    render() {
        const iconName = this.isError? 'dont' : 'plus'

        if (this.state.loading)
            return (
                <Loader active={this.state.loading}>Connecting...</Loader>
            )
        else

        return(
            <div className="landing">
                <div className="topBar centered">
                    <span className='welcome'><span className="wrangle">Pogo.io</span></span>
                    <span className="detail">Private Beta</span>
                </div>

                <div className="centered loginWrap">
                    <img src="/static/img/logo2.png" width="150" style={{width: '150px', marginBottom: '-50px'}}/>
                    <Icon name={iconName} size="big"/>
                    <Icon name="dropbox" size="massive" style={{width: '150px'}}/>
                </div>
                <div className="centered">
                    {!this.isError &&
                    <h1 style={{fontWeight: 'bolder !important', letterSpacing: '1rem', lineHeight: '3rem'}}>{this.state.headline}</h1>
                    }

                    <div className="details">{this.state.details}</div>
                    {this.isError &&
                    <div className="details">{this.note}</div>
                    }
                    {!this.state.tokenPending &&
                    <Button className='button primary go' style={{marginTop: '2rem'}} onClick={() => this.closeWindow()}>Close</Button>
                    }
                </div>

            </div>
        )

    }

}
export default appLayout(dropboxAuth)