import {Component} from 'react'
import {Grid} from 'semantic-ui-react'
import {observer} from 'mobx-react'
// import Router from 'next/router'
import routes, {Link, Router} from '~/routes'

import PDTitlePane from '/components/ProjectDetail/PDTitlePane'
import LeftPane from "/components/common/LeftPane";
import PDWorkPane from '/components/ProjectDetail/PDWorkPane'
// import PDSettings from '/components/ProjectDetail/PDSettings'
import {appLayout} from '~/_layouts'
import Toaster from '/services/Toaster'

import cardStore from '/store/cardStore'
import tagStore from '/store/tagStore'
import userStore from '/store/userStore'
import categoryStore from '/store/categoryStore'
import projectStore from '/store/projectStore'
import uiStateStore from '/store/uiStateStore'
import integrationStore from "/store/IntegrationStore";
import UserSessionUtils from '/utils/UserSessionUtils'

import withDragDropContext from '/components/common/WithDragDropContext'

import RU from '/utils/ResponsiveUtils'
import {VIEW_CARD, VIEW_IMAGES} from "/config/Constants"
import {Preview} from "react-dnd-multi-backend";
import {createPreview} from "../utils/ResponsiveUtils";
import eventStore from "../store/eventStore";
import PLTopLeftPane from "../components/ProjectSummaries/PLTopLeftPane";
import {SendForApproval} from "../store/uiStateStore";
import UserModel from "../models/UserModel";
import UserApiService from "../services/UserApiService";
import ProjectApiService from "../services/ProjectApiService";
import {SecurityLevel, UserStatus} from "../config/Constants";
import JoinPrompt from "../components/Approvals/JoinPrompt";
import PGMobileSidebar from "../components/common/PGMobileSidebar";
import PusherService from "../services/PusherService";
import ErrorBoundary from "../components/common/ErrorBoundary";
import markAsViewed from "../utils/solo/markAsViewed";
import {b64DecodeUnicode} from "../utils/solo/b64DecodeUnicode";

@observer
class ProjectPage extends Component {

    static async getInitialProps(ctx) {
        const { req, res, query, pathname, asPath } = ctx
        const SSR = !!req
        const ua = req? req.headers['user-agent'] : null
        const device = RU.getDeviceType(ua)
        const deviceProps = RU.getDeviceProps(ua)

        // console.log('project detail page. SSR: ' +SSR+ ". query: " +query+ ", query.id: " +query.id)
        // console.log('project detail page. pathname: ' +pathname)
        // console.log('project detail page. asPath: ' +asPath)
        // console.log('project detail page. url: ' +(req? req.url : 'none because clientside'))

        let projectId =  query.id
        console.log(`senderId: ${query.sid} email: ${query.ue} name: ${query.un}`)
        if (projectId === "undefined") {
            // console.warn("getInitialProps: query.id is the string 'undefined'.")
            throw new Error("## project.js:getInitialProps: query.id is 'undefined'. Should be during clientside route changes only.")
        }
        else if (!projectId) {
            res.redirect('projectsPage')
            return
        }

        console.log('#### project detail page. Project ID: ' +projectId)

        // First, obtain min info to determine whether anon user can view this project.
        let userId = UserSessionUtils.retrieveUserId(req)

        // Note: MUST GET EVENTS BEFORE READING PROJECT, which auto-dismisses events. (disabled during testing)
        // let unseenEventsNew
        // if (userId && projectId && projectId !== 'undefined' && projectId !== 'null') {
        //     unseenEventsNew = await eventStore.getInstance ().fetchUnseenEventsNew (userId, projectId)
        //     console.log ('really fetched.')
        // }
        // else if (eventStore.getInstance().unseenEventHistoryNew[projectId] === undefined) { // prefer stale over no data
        //     unseenEventsNew = {[projectId]: {lastViewed: new Date ().toLocaleString (), newEvents: []}}
        //     console.log ('did not fetch.')
        // }
        // Object.keys(unseenEventsNew[projectId]).forEach( key => console.log('key: ' +key+ ', value: ' +unseenEventsNew[projectId][key]) )

        const secInfo = await ProjectApiService.getProjectSecurity(projectId)
        const secLevel = secInfo.security_level || 0

        const pv = (query.pv !== undefined || !userId) // presentation view

        console.log("userId: " +(userId || 'Anon'))
        if (!userId && secLevel === SecurityLevel.MembersOnly) {
            res.redirect('/login/' +projectId+ '?' +projectId)
            return
        }

        // User (or anon) can view this project.
        // Update: Actually, we don't know if user can view it if it's private until we check members list.
        let user = userId? await userStore.getUserById (userId) : null
        if (SSR === false && user !== null) {
            userStore.setCurrentUser( user, true )
        }

        // crossbrowser base64 decoding from https://scotch.io/tutorials/how-to-encode-and-decode-strings-with-base64-in-javascript
        const senderId = query.sid? b64DecodeUnicode(query.sid) : null
        const recipientEmail = query.ue? b64DecodeUnicode(query.ue) : null
        console.log(`senderId: ${senderId} email: ${recipientEmail}`)

        return {
            pv: pv,
            userId: userId,
            user: user,
            projectId: projectId,
            senderId: senderId,
            recipientEmail: recipientEmail,
            secLevel: secLevel,
            device: device,
            deviceProps: deviceProps,
            ssr: SSR}

        // 1. Tasks independent of current user
        // let projectHistoryP = eventStore.getInstance().fetchProjectHistory(projectId)
        // let reactionSetsP = ReactionApiService.getDefaultReactionSets()
        // let creatorP = userStore.getUserById(project.creatorId)
/*
        if (!user) {
            // Check for invitation? No, user was generating with role = 0 when invititation was generated. So this user would exist.
            debugger
            if (query.ue !== undefined) {
                // get user email and name.
                // set parameters to start tour. need tour service?
            }

            let [creator, reactionSets, projectHistory] = await Promise.all([creatorP, reactionSetsP, projectHistoryP])
            return { project: project, creator: creator, user: null, wcw: SSR, projectHistory: projectHistory,
                     reactionSets: reactionSets, device: device, deviceProps: deviceProps, pv: pv }
        }

        // 2. Tasks that require current user
        else if (user) { // TODO REFACTOR OUT OF PAGE. Done.
            let {projects, folio} = await projectStore.getUserProjects(userId) // accept cached projects, immediate resolve
            let userPrefsP = userStore.getUserPrefs(userId)
            // let unseenEventsP = eventStore.getInstance().fetchUnseenEventsNew(userId, projectStore.currentProject)
            let userCardsP = cardStore.getUserCards(userId, true, true) // accept cached cards, use cardmodel
            let userConnectionsP = userStore.getUserConnections(userId)
            let [creator, userCards, userConnections, reactionSets, userPrefs, projectHistory] = await Promise.all([creatorP, userCardsP, userConnectionsP, reactionSetsP, userPrefsP, projectHistoryP])
/!*
            if (!isActive(userId, project.id)) {
                const eventSource = (!!query.ue && !!query.up)? EventSource.EmailInvite : EventSource.DirectLink
                await projectStore.addUserToProject(project.id, null, userId, eventSource, UserRole.ACTIVE)
            }
*!/
            return { project: project, creator: creator, user: user, userCards: userCards, wcw: SSR,
                     userProjects: projects, folio: folio, connections: userConnections, reactionSets: reactionSets,
                     pv: pv, userPrefs: userPrefs, projectHistory: projectHistory,
                     unseenEventsNew: unseenEventsNew, device: device, deviceProps: deviceProps }
        }
*/


    }

    constructor(props) {
        super(props)

        uiStateStore.currentPage = uiStateStore.PD
        uiStateStore.voMode = props.pv

        this.projectId = props.projectId
        if (props.user) {
            integrationStore.init(props.user.id, '')
            userStore.setCurrentUser( props.user, true )
            // userStore.setUserConnections (props.connections)
            // userStore.followConnections()
            // userStore.userPrefs = props.userPrefs
            // projectStore.setUserProjects(props.userProjects, props.folio)
            // cardStore.userCards = ArrayUtils.arrayUnique(props.userCards)
        }
        this.isAnon = !props.user || props.user.status === UserStatus.ANON

        // userStore.currentCreator = props.creator
        // projectStore.setCurrentProject(props.project) // sets clientside projectStore, from value provided by SSR projectStore.

        // const evs = eventStore.getInstance()
        // evs.allEventHistory[props.project.id] = props.projectHistory
        // evs.unseenEventHistoryNew[props.project.id] = props.unseenEventsNew? props.unseenEventsNew[props.project.id] : {events: []}

        // cardStore.setCurrentCards(projectStore.currentProject.cards) // sets clientside cardStore, from value provided by SSR projectStore.
        // tagStore.currentTagSet = props.project.tagSet2
        // ReactionApiService.setDefaultReactionSets( props.reactionSets )

        RU.device = props.device
        RU.setDeviceProps(props.deviceProps)

        // if (!projectStore.currentProject.reactionSetId && ReactionApiService.currentReactionSet !== null) {
        //     projectStore.setReactionSet (projectStore.currentProject, ReactionApiService.currentReactionSet)
        // }

        this.state = {
            view: 'cards',
            importing: false,
            importItems: [],
            importCount: 0,
            // showTags: false, /*UserSessionUtils.getSessionPref('showTags'),*/
            showLetters: true, /*UserSessionUtils.getSessionPref('showLetters'),*/
            showActions: true  /*UserSessionUtils.getSessionPref('showActions')}*/
        }
    }

    /**
     * When events are pushed out (votes, card adds, comments), we can trigger updates/refreshes from here.
     * @param event
     * @code see WranglEvent.js
     */
    handleOnlinePushEvent(event, listener) {
        if (!listener)
            return
        let self = listener

        if (event.wrangl.id === projectStore.currentProject.id) {

        } else {

        }
    }

    deleteProject(project) {
        projectStore.deleteProject(project).then( response => {
            uiStateStore.disableUPFetch = true
            Router.pushRoute('/', {update: false})
        })
    }

    leaveProject(projectId) {
        if (this.isAnon)
            return

        const userId = userStore.currentUser.id
        projectStore.removeUserFromProject(projectId, userId, userId, false).then( response => {
            uiStateStore.disableUPFetch = false
            Router.pushRoute('/', {update: true})
        })
    }

    saveCard(card) {
        if (this.saveInProgress) {
            console.log('Ignoring save already in progress')
            return
        }
        this.saveInProgress = true
        cardStore.saveCard(card).then( card => {
            this.saveInProgress = false
            this.refreshCards() // TODO or replace with collection mutation
        }).catch( error => {
            this.saveInProgress = false
        })
    }

    createCards(cards) {
        cardStore.createCardsFromAssets( cards, projectStore.currentProject ).then( response => {
            this.refreshCards() // Note: CardCreate event will eventually come back to us, but could take several seconds. So grab it here, and suppress event reaction.
        })
    }

    importCards(cards) {
        cardStore.cloneCardsIntoProject(cards, projectStore.currentProject).then( response => {
            this.refreshCards()
        })
    }

    importAssets(assets, source) {
        if (!assets || assets.length === 0)
            throw new Error("importAssets: no assets .")

        this.setState({importItems: assets})
        uiStateStore.importingAssets = true
        if (source === 'dropbox') {
            debugger
            // DropboxService.getShareLinks(assets).then( assetsWithLinks => {
                cardStore.createCardsFromFiles (assets, projectStore.currentProject, source).then (response => {
                    const cards = response.data.cards
                    tagStore.createDropboxFolderTags(cards, assets)
                    cardStore.attachDropboxImagesToCards (cards, assets, this.showImportProgress.bind(this))
                })
            // })
        } else {
            cardStore.createCardsFromAssets (assets, projectStore.currentProject, source).then (response => {
                // tagStore.autoTagCardsFromSources(response.data.cards, assets, this.onAutoTagComplete) works but disabled.
                this.refreshCards ()
            }).catch( error => {
                console.log(error)
                this.setState ({importing: false, importItems: [], importCount: 0})
                Toaster.warn("There was a problem with the image upload. Please try a different image.")
            })
        }
    }

    onAutoTagComplete = (success) => {
        console.log('Autotagging complete. ')
        debugger
        this.refreshCards()
    }

    showImportProgress(count, msg) {
        if (msg === 'end') {
            this.refreshCards()
            if (count === this.state.importItems.length) {
                this.setState ({importing: false, importItems: [], importCount: 0})
            }
        }
        else if (count === 'start') {
            this.setState({importing: true, importCount: Math.max(count, this.state.importCount) })
        }
    }

    uploadAssets() {
        // Upload Component has uploaded assets directly. Just refresh cards.
        this.refreshCards()

        /*
         cardStore.createCardsFromFiles(cards, projectStore.currentProject).then(response => {
         let images = fileRefs.map ( asset => asset.preview )
         cardStore.attachFilesToCards(response.data.cards, fileRefs, this.showUploadProgress.bind(this))
         })
         */
    }

    refreshCards() {
        projectStore.fetchCurrentProject(projectStore.currentProject.id).then( project => {
            console.log("Current project has " +projectStore.currentProject.cards.length+ ' cards.')
        })
        if (!this.isAnon) {
            cardStore.getUserCards (userStore.currentUser.id, false, true).then (response => {
                uiStateStore.importingAssets = false
            })
        }
    }

    refreshMembers = () => {
        projectStore.fetchCurrentProject(projectStore.currentProject.id).then( project => {
        })
    }

    refreshTagBar = () => {
        projectStore.fetchCurrentProject(projectStore.currentProject.id).then( project => {
        })
    }

    viewCards() {
        if (!this.isAnon)
            userStore.setUserPref(null, 'card_view', VIEW_CARD)
        this.setState({view: 'cards'})
    }

    viewImages() {
        if (!this.isAnon)
            userStore.setUserPref(null, 'card_view', VIEW_IMAGES)
        this.setState({view: 'images'})
    }

    toggleTags() {
        userStore.userPrefs.showTags = !userStore.userPrefs.showTags
        if (!this.isAnon)
            userStore.setUserPref(null, 'show_tags', userStore.userPrefs.showTags)
        // this.setState({showTags: !this.state.showTags})
    }

    toggleLetters() {
        UserSessionUtils.setSessionPref('showLetters', !this.state.showLetters)
        this.setState({showLetters: !this.state.showLetters})
    }

    toggleActions() {
        UserSessionUtils.setSessionPref('showActions', !this.state.showActions)
        this.setState({showActions: !this.state.showActions})
    }


    loadComments() {
        // userStore.setUserPref(null, 'card_view', VIEW_COMMENTS)
        cardStore.getAllCardDetailsInCurrentProject().then( cardDetails => {
            this.setState({view: 'comments'})
        })
    }

    generatePreview(type, item, style) {
        return createPreview(type, item, style)
    }

    createProject = () => {
        projectStore.createProject().then(project => {
            Router.pushRoute('/project/'+project.id, {id: project.id})
        })
    }

    getAllTheStuff(projectId, userId) {
        // data fetching.
        // 1. Grabbing events here rather than component that needs it due to order-of-calls dependency.
        // If api resolves this, move events out of here into EventDetailsPopup.
        // 2. Get project in question
        // 3. Get conversation, here rather than Conversations, due to
        //      a) DidUpdate circular calls,
        //      b) copying props to state
        //      c) initialized and isConvoPending didn't prevent overwriting and multi calls
        //      d) so let's go for a clean mobx solution


        const evs = eventStore.getInstance()
        evs.getUserBadges ()
        categoryStore.getUserCategories ()
        projectStore.fetchCurrentProject (projectId, userId, true).then (project => {
            projectStore.getCurrentProjectConvo (projectId)
            const cat = categoryStore.getProjectCategory(projectId)
            if (cat)
                categoryStore.currCatId = cat.id
        }).catch (error => {
            console.error (error)
            routes.Router.replaceRoute ('/', '/', {shallow: false})
        })

        if (!userStore.userPrefs)
            userStore.getUserPrefs (userId)
    }

    componentDidMount() {
        // We must auto-gen empty user in order for all the upcoming api calls to work. We'll then present user with customization
        if (!userStore.currentUser) {
            UserApiService.createAnonUser().then( response => {
                const newUser = new UserModel(response.data)
                userStore.setCurrentUser(newUser, true)
                this.getAllTheStuff(this.projectId, newUser.id)
            })
        } else {
            this.getAllTheStuff(this.projectId, userStore.currentUser.id)
        }

        markAsViewed(this.projectId, 'project')
        PusherService.connect() // will (re)connect, and (re)follow our active user

        // first time opening a project, show board members. After that, it's up to user.
        if (!uiStateStore.showSidebar) {
            uiStateStore.toggleSidebar()
            uiStateStore.openMembers = true
        }

        // auto-connect invited users
        const {senderId, recipientEmail} = this.props
        if (senderId && recipientEmail) {
            if (userStore.currentUser && userStore.currentUser.id !== senderId) { // self-invitation
                const activeEmail = userStore.currentUser.email
                // If we are the intended recipient of invitation
                if (activeEmail && activeEmail.toLowerCase() === recipientEmail.toLowerCase()) {
                    userStore.connectUsers(senderId, userStore.currentUser.id)
                }
            }
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!userStore.currentUser) // anon user is being created. no need to update yet.
            return

        const userId = userStore.currentUser.id
        const prevProjectId = prevProps.projectId
        const currProject = projectStore.currentProject
        const newPropsProjectId = this.props.projectId

        if (!!this.projectId && !!prevProjectId && !!currProject && !!currProject.id)
            console.log('componentDidUpdate: previous: ' +prevProjectId.substring(0,5)+ ', current: ' +currProject.id.substring(0,5)+ '. this: ' +this.projectId.substring(0,5)+ ', new props: ' +(newPropsProjectId? newPropsProjectId.substring(0,5):''))

        if (prevProjectId !== newPropsProjectId) {
            cardStore.currentCards = []
            projectStore.fetchCurrentProject (newPropsProjectId, userId, true).then(project => {
                projectStore.getCurrentProjectConvo(newPropsProjectId)
                markAsViewed(newPropsProjectId, 'project')
                const cat = categoryStore.getProjectCategory(newPropsProjectId)
                if (cat)
                    categoryStore.currCatId = cat.id
            }).catch( error => {
                console.error (error)
                routes.Router.replaceRoute ('/', '/', {shallow: false})
            })
        }

    }

    // LEFT OFF:
        /*
            SOLUTION:
            It was just Mobx in dev mode. HMR kills it. It stops propagating observable changes to its observers.
            Since pages are built on fly in dev mode, once page is built, HMR is done, Mobx is now at least partially dead.
            Is this properly documented somewhere?
            See
            a) https://mobx.js.org/best/stateless-HMR.html mentions the issue, but for stateless, non @observable tagged components.
            b) https://github.com/mobxjs/mobx-react/issues/430 - related. but nextjs removed react-hot-loader. hence prob exists.
            c) https://github.com/mobxjs/mobx-state-tree/issues/372 - perhaps the real prob. Workaround in doubt.
            d) no, (c) refers to mobx-state-tree, a newer container around mobx.

            Now that it's fixed, shall we try re-enabling SSR caching? Yes. Caching re-enabled. 

            Original issue:
            It was workin great, rendering page immediately, then filling in data,
            then an issue arises when the app STARTS from a /project page.
            When starting app from a project, then go to My Projects, then choose another project.
            The project.js page's life cycles are never called e.g. mount or update, in order to fetch the appropriate project.
            Ppossible Causes:
            1. SSR caching. Since SSR only seems to activate on /project when app is launched from there.
                Then a cache hit on a cached project page would mean nothing is called.
                HOWEvER< the cache key should contain the project ID in the url, so it shouldn't be a hit.

                Nevertheless, try DISABLING SSR CACHE and see what happens.

                tried quick disable before leaving, didn't help. Try really disabling.

                CHECK IF PROJECT ID IS BEING OBTAINED BY QUERY PARAM IN GET INITIAL PROS.
                IF NOT, HOW DO WE GET IT FROM ROUTER REQUEST????

            2. Verify whether ANYTHING in the page is getting called. Breakpoint or console in constructor, or WilLReact or anywhere.

            3. Mobx + HMR



        */


    componentWillReact() {
        console.log('Project Detail Page will react.')
    }

    render() {
        console.log("ssr device: " +this.props.device)
        const isApproval = uiStateStore.rightUnModal === SendForApproval

        const leftPaneCols = uiStateStore.voMode? 0 : uiStateStore.minimizeLeftCol || !uiStateStore.showSidebar? 1 : uiStateStore.maximizeLeftCol? 8 : 3
        const workColumns = 16 - leftPaneCols

        const device = RU.getDeviceType()
        const mobile = ['computer','tablet'].indexOf(device) === -1

        const slpState = uiStateStore.showSidebar? ' open':' closed'
        const midState = uiStateStore.showSidebar? '':' expanded'

        const workClass = 'jMidBG EMP' +midState+ (isApproval? ' apPane':'')

        return(
            <div >

                {/*<DevPane />*/}
                <Preview generator={this.generatePreview} />

                {/*Desktop Layout*/}
                {!mobile &&
                    <Grid celled className="project hidden-scroll-light" style={{height: '100%', marginBottom: '0', position: 'fixed'}}>
                        <Grid.Row className="framing" style={{ boxShadow: '10px 0 10px red !important'}}>
                            {!uiStateStore.voMode &&
                            <Grid.Column width={leftPaneCols} className={'jSideBG SLP' +slpState} stretched style={{paddingBottom: 0}}>
                                <PLTopLeftPane
                                    project={projectStore.currentProject}
                                    user={userStore.currentUser}
                                    readOnly={this.props.readOnly}
                                    senderId={this.props.senderId}
                                    recipientEmail={this.props.recipientEmail}
                                    mobile={false}/>
                            </Grid.Column>
                            }

                            <Grid.Column className='jMidBG' width={workColumns} style={{ padding: '.5rem'}}>
                                <PDTitlePane project={projectStore.currentProject} user={userStore.currentUser} />
                            </Grid.Column>

                        </Grid.Row>

                        <Grid.Row stretched style={{boxShadow: 'none', height: '90%'}} >

                            {!uiStateStore.voMode &&
                            <Grid.Column computer={leftPaneCols} mobile={1} stretched className={'jSideBG pad0 SLP' +slpState}
                                         style={{ paddingLeft: '0 !important', height: '101%'}} >
                                <ErrorBoundary>
                                    <LeftPane project={projectStore.currentProject}
                                                user={userStore.currentUser}
                                                projects={projectStore.userProjects}
                                                createProject={this.createProject}
                                                userCards={cardStore.userCards}
                                                settings={() => {this.showSettings()}}
                                                mobile={false}
                                                importCards={(cards) => {this.importCards(cards)}}
                                                deleteProject={(project) => this.deleteProject(project)}
                                                connections={userStore.currentConnections}
                                                refreshMembers={this.refreshMembers}
                                                refreshTagBar={this.refreshTagBar}
                                    />
                                </ErrorBoundary>
                            </Grid.Column>
                            }

                            <Grid.Column className={workClass} width={workColumns}
                                         stretched style={{ boxShadow: '0 -1px 0 0 #d4d4d5', padding: 0}} >
                                <ErrorBoundary>
                                <PDWorkPane project={projectStore.currentProject}
                                            // cards={cardStore.currentCards}
                                            // onCardsChange={() => {this.refreshCards()}}
                                            user={userStore.currentUser}
                                            mobile={false}

                                            view={this.state.view}
                                            viewCards={() => {this.viewCards()}}
                                            viewImages={() => {this.viewImages()}}
                                            loadComments={() => {this.loadComments()}}
                                            showTags={userStore.userPrefs? userStore.userPrefs.showTags : false}
                                            showLetters={this.state.showLetters}
                                            showActions={this.state.showActions}
                                            toggleTags={() => {this.toggleTags()}}
                                            toggleLetters={() => {this.toggleLetters()}}
                                            toggleActions={() => {this.toggleActions()}}

                                            saveCard={(card) => {this.saveCard(card)}}
                                            refreshCards={() => {this.refreshCards()}}
                                            // filterCards={() => {this.filterfCards(query,tags)}}
                                            refreshMembers={() => {this.refreshMembers()}}
                                            createCards={(cards) => {this.createCards(cards)}}
                                            importCards={(cards) => {this.importCards(cards)}}
                                            importAssets={(assets, source) => {this.importAssets(assets, source)}}
                                            uploadAssets={(cards, files) => {this.uploadAssets(cards, files)}}
                                            uploadCount={this.state.importCount}
                                            createProject={(n) => {this.createProject(n)}}
                                            deleteProject={(project) => this.deleteProject(project)}
                                            leaveProject={(project) => this.leaveProject(project)}
                                />
                                </ErrorBoundary>
                                <JoinPrompt/>

                            </Grid.Column>
                        </Grid.Row>

                        <style jsx>{`
                          height: 100%;
                          overflow-y: hidden;
                        `}</style>
                    </Grid>
                }

                {/*Mobile Layout*/}
                {mobile &&
                <span>
                <Grid celled style={{marginTop: 0, marginBottom: 0, height: '98.5%', position: 'fixed'}}>
                    <Grid.Row className="mFraming flush">
                        <Grid.Column width={16} style={{paddingBottom: '0 !important'}}>
                                <PDTitlePane project={projectStore.currentProject} user={userStore.currentUser} mobile/>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row stretched style={{boxShadow: 'none', height: '90%'}} className='darkFraming'>
                            <Grid.Column width={16} stretched style={{ boxShadow: '0 -1px 0 0 #d4d4d5', padding: 0}} >
                                <ErrorBoundary>
                                <PDWorkPane project={projectStore.currentProject}
                                            user={userStore.currentUser}

                                            mobile={true}
                                            view={this.state.view}
                                            viewCards={() => {this.viewCards()}}
                                            viewImages={() => {this.viewImages()}}
                                            loadComments={() => {this.loadComments()}}
                                            showTags={userStore.userPrefs? userStore.userPrefs.showTags : false}
                                            showLetters={this.state.showLetters}
                                            showActions={this.state.showActions}
                                            toggleTags={() => {this.toggleTags()}}
                                            toggleLetters={() => {this.toggleLetters()}}
                                            toggleActions={() => {this.toggleActions()}}

                                            saveCard={(card) => {this.saveCard(card)}}
                                            refreshCards={() => {this.refreshCards()}}
                                            createCards={(cards) => {this.createCards(cards)}}
                                            importCards={(cards) => {this.importCards(cards)}}
                                            importAssets={(assets, source) => {this.importAssets(assets, source)}}
                                            uploadAssets={(cards, files) => {this.uploadAssets(cards, files)}}
                                            uploadCount={this.state.importCount}
                                            createProject={(n) => {this.createProject(n)}}
                                />
                                </ErrorBoundary>
                                <JoinPrompt/>
                            </Grid.Column>
                        </Grid.Row>

                        <style jsx>{`
                          height: 100%;
                          overflow-y: hidden;
                        `}</style>
                    </Grid>
                    <PGMobileSidebar
                        component={
                            <LeftPane
                                projects={projectStore.userProjects}
                                totalProjects={projectStore.totalProjects}
                                createProjects={(n,name) => {this.createProjects(n,name)}}
                                user={userStore.currentUser}
                                userCards={cardStore.userCards}
                                connections={userStore.currentConnections}
                                totalConnections={userStore.totalConnections}
                                filterByUser={this.filterByUser}
                                filterByTag={this.filterByTag}
                                mobile
                            />
                    }/>
                </span>
                }

            </div>
        )
    }
}

export default appLayout(withDragDropContext(ProjectPage))