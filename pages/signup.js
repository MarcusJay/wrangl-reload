import {Component} from 'react'
import LoginOrSignup from '/components/Account/LoginOrSignup'
import {appLayout} from '~/_layouts'
import uiStateStore from "../store/uiStateStore";
import RU from "../utils/ResponsiveUtils";

class PageSignUp extends Component {

    static async getInitialProps(ctx) {
        const {req, pathname, asPath} = ctx
        const isServer = !!req;
        const ua = req? req.headers['user-agent'] : null
        const device = RU.getDeviceType(ua)
        const deviceProps = RU.getDeviceProps(ua)

        let projectId = asPath.replace(/\/signup\/?/,'')
        let qIndex = projectId.indexOf('?')
        if (qIndex > 0)
            projectId = projectId.substring(0,qIndex)
        console.log ('Signup page. SSR: ' + !!req + ". path: " + pathname+ ", asPath: " + asPath+ ', projectId: ' +projectId)
        return {projectId: projectId, pathname: pathname, asPath: asPath,
                deviceProps: deviceProps, device: device
        }
    }

    constructor(props) {
        super (props)
        RU.device = props.device
        RU.setDeviceProps(props.deviceProps)
        uiStateStore.currentPage = uiStateStore.LS
    }

    render() {
        return(
            <div className="landing">
                <div className="transparent"></div>
                <div className="topBar centered">
                    <span className='welcome'>Welcome to <span className="wrangle">Pogo.io</span></span>
                    <span className="detail">Private Beta</span>
                </div>
                <div className="centered loginWrap">
                    <LoginOrSignup show='signUp' projectId={this.props.projectId}/>
                </div>
                <div className="legal bottom ">
                    (c) 2018 Nudge, Inc.
                    <a href='/static/tos.html' target='_blank'>Terms of Service</a>
                </div>
                <div className="credit bottom ">
                    Photo by Joshua Ness
                </div>
            </div>
        )

    }

}
export default appLayout(PageSignUp)