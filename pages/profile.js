import {Component} from 'react'
import {Grid, Menu, Sidebar} from 'semantic-ui-react'
import {Link, Router} from '~/routes'
import {observer} from 'mobx-react'
import {ArrowLeft} from 'react-feather'

import {appLayout} from '~/_layouts'
import projectStore from '/store/projectStore'
import uiStateStore from '/store/uiStateStore'
import userStore from '/store/userStore'

import PFTitlePane from '/components/ProjectSummaries/PFTitlePane'
import PLLeftPane from '/components/ProjectSummaries/PLLeftPane'
import PLWorkPane from '/components/ProjectSummaries/PLWorkPane'
import UserSessionUtils from '/utils/UserSessionUtils'
import withDragDropContext from '/components/common/WithDragDropContext'
import RU from "../utils/ResponsiveUtils";
import PFWorkPane from "../components/ProjectSummaries/PFWorkPane";
import LeftPane from "../components/common/LeftPane";
import cardStore from "../store/cardStore";
import PLTopLeftPane from "../components/ProjectSummaries/PLTopLeftPane";
import PusherService from "../services/PusherService";
import ErrorBoundary from "../components/common/ErrorBoundary";

// @DragDropContext(HTML5Backend)
@observer
class Profile extends Component {

    static async getInitialProps(ctx) {
        const { req, res, query } = ctx
        const SSR = !!req
        const ua = req? req.headers['user-agent'] : null
        const device = RU.getDeviceType(ua)
        const deviceProps = RU.getDeviceProps(ua)
        console.log('Profile page. SSR: ' +!!req+ ". query: " +query+ ", query.id: " +query.id)

        let profileId =  query.id
        if (!profileId) {
            res.redirect ('projectsPage')
            return
        }

        let userId = UserSessionUtils.retrieveUserId(req)
        if (typeof userId === 'undefined') {
            res.redirect('/login/' )
            return
        }

        // note: client: 1 userStore per user session.
        //   serverside: 1 userStore per server instance
        let user = await userStore.getUserById (userId)
        if (!user) {
            res.redirect('/login/' )
            return
        }
        if (SSR === true) {
            // console.log('SSR. userSessions count: ' +Object.keys(userStore.userSessions).length)
            // if (userStore.userSessions[userId] === undefined)
            //     userStore.userSessions[userId] = user
        } else if (SSR === false) {
            userStore.setCurrentUser( user, true )
        }

        if (!userStore.userPrefs) {
            // ?
        }

        return { userId: userId, user: user, profileId: profileId, device: device, deviceProps: deviceProps }
    }

    constructor(props) {
        super (props)
        uiStateStore.currentPage = uiStateStore.PF
        userStore.setCurrentUser( props.user, true )
        this.state = {totalProjects: 0}
        RU.device = props.device
        RU.setDeviceProps(props.deviceProps)

    }

    filterByTag(tag) {
        if (tag) {
            console.log("filtering by tag: " +tag)
        } else {
            console.log("removing tag filter")
        }

    }

    getFriendProfile() {
        const {profileId, userId} = this.props
        if (!profileId) //
            return

        let profile = null

        // if profile has changed
        if (!userStore.currentFriend || userStore.currentFriend.id !== profileId) {

            // get new profile from connections
            if (!userStore.currentConnections || userStore.currentConnections.length === 0) {
                userStore.getUserConnections (userId).then (response => {
                    userStore.currentFriend = userStore.getUserFromConnections (profileId)
                })
            } else {
                userStore.currentFriend = userStore.getUserFromConnections(profileId)
            }

        }

    }

    createProject = () => {
        projectStore.createProject().then(project => {
            Router.pushRoute('/project/'+project.id, {id: project.id})
        })
    }

    componentDidMount() {
        this.getFriendProfile()
        if (!userStore.userPrefs)
            userStore.getUserPrefs(userStore.currentUser.id)

        PusherService.connect() // will (re)connect, and (re)follow our active user
        uiStateStore.showDiscussion = true
    }

    componentDidUpdate() {
        if (!userStore.currentFriend || userStore.currentFriend.id !== this.props.profileId) {
            this.getFriendProfile ()
            console.log('profile.js update')
        }
    }

    render() {
        const device = RU.getDeviceType()
        const leftPaneCols = uiStateStore.voMode? 0 : !uiStateStore.showSidebar || uiStateStore.minimizeLeftCol? 1 : uiStateStore.maximizeLeftCol? 8 : 3
        const workCols = 16 - leftPaneCols

        const {profileId} = this.props
        const totalProjects = projectStore.projectsInCommon? projectStore.projectsInCommon.length : 0
        const profile = userStore.currentFriend
        const showMissingUser = !profileId || !profile

        const slpState = uiStateStore.showSidebar? ' open':' closed'
        const midState = uiStateStore.showSidebar? '':' expanded'

        if (!profile)
            return (
                <div className='errorPage'>
                    <Link route='projectsPage' data-tip='Home'>
                        <img src="/static/img/pogoMulti.png" className='pointer' width='100px'
                             data-tip="Home"/>
                    </Link><br/>
                    <div>
                        <div className='mb1'> Sorry, that user does not exist.</div>
                        <Link route='projectsPage' data-tip='Home' >
                            <span className='jBlue pointer'>Home</span>
                        </Link>
                    </div>
                </div>
            )


        return(
          <div>
            {/*Desktop Layout*/}
            {['computer', 'tablet'].indexOf (device) !== -1 &&
                <Grid celled className="project hidden-scroll-light" style={{height: '100%', marginBottom: '0', position: 'fixed'}}>
                  <Grid.Row className="framing">
                      <Grid.Column className={'jSideBG SLP ' + slpState} width={leftPaneCols} stretched style={{paddingBottom: 0}}>
                          <PLTopLeftPane readOnly={true} style={{padding: '0 !important'}} profile={profile}/>
                      </Grid.Column>
                      <Grid.Column className='jMidBG' width={13} style={{ paddingLeft: '1.5rem', paddingBottom: '1rem'}} >
                          <PFTitlePane user={userStore.currentUser}
                                       friend={profile}
                                       mobile={false}
                                       totalProjects={totalProjects} readOnly={true}/>
                      </Grid.Column>
                  </Grid.Row>
                  <Grid.Row stretched style={{boxShadow: 'none', height: '90%'}} >
                      <Grid.Column computer={leftPaneCols} mobile={1} stretched className={'jSideBG pad0 SLP' +slpState}
                                   style={{ paddingLeft: '0 !important', height: '101%'}} >
                          <ErrorBoundary>
                            <LeftPane
                                    projects={projectStore.projectsInCommon}
                                    user={userStore.currentUser}
                                    userCards={cardStore.userCards}
                                    createProject={this.createProject}
                                    settings={() => {this.showSettings()}}
                                    mobile={false}
                                    deleteProject={(project) => this.deleteProject(project)}
                                    connections={userStore.currentConnections}
                                    refreshMembers={this.refreshMembers}
                                    refreshTagBar={this.refreshTagBar}
                            />
                          </ErrorBoundary>

                      </Grid.Column>
                      <Grid.Column width={workCols} style={{padding: 0}} className={'jMidBG EMPF' +midState}>
                          <ErrorBoundary>
                            <PFWorkPane
                                      mobile={false}
                                      friend={profile}
                                      onDark={this.setIsDark}
                                      totalProjects={totalProjects}
                                      showTags={true}
                                      users={userStore.currentConnections}
                                      currentUser={userStore.currentUser}
                                      totalConnections={userStore.totalConnections}
                                      readOnly={true}
                            />
                          </ErrorBoundary>
                      </Grid.Column>
                  </Grid.Row>

                  <style jsx>{`
                  height: 100%;
                  overflow-y: hidden;
                `}</style>
              </Grid>
            }

            {/* Mobile Layout */}
            {device === 'mobile' &&
            <Grid celled style={{marginTop: 0, marginBottom: 0, height: '98.5%', position: 'fixed'}}>
                <Grid.Row className="mFraming flush">
                    <Grid.Column width={16} style={{paddingBottom: '0 !important'}}>
                          <PFTitlePane user={userStore.currentUser}
                                       friend={profile}
                                       mobile={true}
                                       totalProjects={totalProjects} readOnly={true} />
                      </Grid.Column>
                  </Grid.Row>
                  <Grid.Row style={{height: '90%'}}>

                      <Grid.Column width={16} style={{padding: 0}}>
                          <Sidebar.Pushable>
                              <Sidebar as={Menu} animation='overlay' width='thin' visible={uiStateStore.showSidebar}
                                       icon='labeled' vertical inverted>
                                  <Menu.Item style={{textAlign: 'left'}}>
                                      <Link route='projectsPage'>
                                          <div style={{float: 'left', fontSize: '1.1rem'}}>
                                              <ArrowLeft color="#d0d0d0" size={20} style={{margin: '0 0 .5rem 0'}}/>
                                              <span className='small bnl'> All Boards</span>
                                          </div>
                                      </Link>
                                      <PLLeftPane projects={projectStore.projectsInCommon}
                                                  user={profile}
                                                  totalProjects={totalProjects}
                                                  connections={userStore.currentConnections}
                                                  totalConnections={userStore.totalConnections}
                                                  userTags={userStore.userTags}
                                                  mobile
                                                  readOnly={true}
                                                  filterByUser={null}
                                                  filterByTag={this.filterByTag}
                                      />
                                  </Menu.Item>
                              </Sidebar>
                              <Sidebar.Pusher>
                                  <PLWorkPane projects={projectStore.projectsInCommon}
                                              onDark={this.setIsDark}
                                              // view={this.state.initialView}
                                              mobile
                                              friend={profile}
                                              totalProjects={totalProjects}
                                              showTags={true}
                                              users={userStore.currentConnections}
                                              currentUser={userStore.currentUser}
                                              totalConnections={userStore.totalConnections}
                                              readOnly={true}
                                  />
                              </Sidebar.Pusher>
                          </Sidebar.Pushable>
                      </Grid.Column>
                  </Grid.Row>

                  <style jsx>{`
              height: 100%;
              overflow-y: hidden;
            `}</style>
              </Grid>
            }
          </div>
        )
    }
}

export default appLayout(withDragDropContext(Profile))