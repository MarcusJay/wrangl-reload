import {Component} from 'react'
import {appLayout} from '~/_layouts'
import {Button, Container, Dimmer, Header, Loader, Segment} from "semantic-ui-react";
import withDragDropContext from '/components/common/WithDragDropContext'
import {Link} from '~/routes'

import uiStateStore from '/store/uiStateStore'
import ProjectApiService from "../services/ProjectApiService";
import userStore from "../store/userStore";
import {Interests, Professions} from "../config/OnBoardingConfig";
import ProjectModel from "../models/ProjectModel";
import RU from "../utils/ResponsiveUtils";
import UserSessionUtils from "../utils/UserSessionUtils";
import {MasonryContainer} from "../components/ProjectSummaries/WorkPane/masonry";


class Customize extends Component {

    static async getInitialProps(ctx) {
        const {req, pathname, asPath} = ctx
        const isServer = !!req;
        const ua = req? req.headers['user-agent'] : null
        const device = RU.getDeviceType(ua)
        const deviceProps = RU.getDeviceProps(ua)

        // First, obtain min info to determine whether anon user can view this project.
        let userId = UserSessionUtils.retrieveUserId(req)
        let user = userId? await userStore.getUserById (userId) : null

        const professionIds = Professions
        let professions = [], interests = []
        const interestIds = Interests
        // TODO replace this with bulk api calls
        let profCalls = [], intCalls = []
        professionIds.forEach( pId => profCalls.push(ProjectApiService.getProjectById(pId)) )
        interestIds.forEach( iId => intCalls.push(ProjectApiService.getProjectById(iId)) )
        let results = await Promise.all (profCalls)
        console.log('results size: ' +results.length)
        results.forEach( apiResponse => {
            console.log('got project from api: ' +apiResponse.data.project_title)
            professions.push( new ProjectModel(apiResponse.data))
        })

        results = await Promise.all (intCalls)
        console.log('results size: ' +results.length)
        results.forEach( apiResponse => {
            console.log('got project from api: ' +apiResponse.data.project_title)
            interests.push( new ProjectModel(apiResponse.data))
        })
        console.log('IP: returning ' +professions.length+ ' professions.')
        return {device: device, deviceProps: deviceProps, professions: professions, interests: interests,
                user: user, userId: userId}
    }


    constructor(props) {
        super (props)

        RU.device = props.device
        RU.setDeviceProps(props.deviceProps)

        uiStateStore.currentPage = uiStateStore.C1
        if (props.user)
            userStore.setCurrentUser(props.user)

        this.professions = props.professions
        this.interests = props.interests

        this.state = {loading: false, showSuccess: false, showFailure: false}
    }

    loadHome = () => {
        this.setState({loading: true})
    }

    componentWillMount() {
    }

    componentDidMount() {
        let a =1
    }

    render() {
        const {showSuccess, showFailure, loading} = this.state
        const showForm = !showSuccess && !showFailure
        const pageStyle = {background: 'none !important', border: 'unset !important', width: '100%', float: 'left', padding: '2rem',
                           boxShadow: 'none !important'}

        const mobile = RU.getDeviceType() === 'mobile'
        const currentUser = userStore.currentUser

        return (
            <div className="landing ">
                {/*<div className="transparent"></div>*/}
                <div className="topBar centered">
                    <span className='welcome'>Welcome to Pogo.io</span>
                    <Link route='/' >
                        <span className="detail pointer" onClick={this.gotoHome}
                              style={{position: 'absolute', right: '1rem'}}>
                            Visit Dashboard
                        </span>
                    </Link>
                </div>
                <div className="centered ">
                    <div style={{textAlign: 'center'}}>
                        <Dimmer.Dimmable dimmed={loading}>
                            <Dimmer active={loading} inverted>
                                <Loader inverted style={{marginTop: '0', height: '10%'}}>Creating Dashboard...</Loader>
                            </Dimmer>
                            <Container className='loginPage projectViews' style={{position: 'unset'}}>
                                <div className="centered projectViewsWorkPane " style={{height: '92%'}}>
                                    <Segment padded  className="loginBox subtle-scroll-dark" style={pageStyle}>
                                        {showForm &&
                                        <div>
                                            <div className='thinH' style={{fontSize: '2rem', marginBottom: '1rem'}}>Curated Boards</div>
                                            <div className='thinH3 secondary' style={{marginBottom: '1.5rem'}}>You can add a few to get started </div>
                                            <MasonryContainer
                                                activeProjects={this.professions}
                                                inactiveProjects={this.interests}
                                                showTags={false}
                                                currentUser={currentUser}
                                                users={[]}
                                                mobile={mobile}
                                                readOnly={true}
                                                addUserToProject={(projectId, userId, newMemberId, trigger) => {this.addUserToProject(projectId, userId, newMemberId, trigger)}}
                                            />
                                        </div>
                                        }

                                        {showSuccess &&
                                        <div>
                                            <Header>We've created some example Wrangles to get you started.</Header>
                                            <a href='https://pogo.io/login'>
                                                <Button content="Let's Go!" className="go" fluid/>
                                            </a>
                                        </div>
                                        }

                                    </Segment>

                                </div>

                            </Container>
                        </Dimmer.Dimmable>
                    </div>

                </div>
{/*
                <div className="legal bottom ">
                    (c) 2018 Nudge, Inc.
                    <a href='/static/tos.html' target='_blank'>Terms of Service</a>
                </div>
                <div className="credit bottom ">
                    Photo by Joshua Ness
                </div>

*/}
            </div>
        )

    }

}
export default appLayout(withDragDropContext(Customize))