import {Component} from 'react'
import {VanityUrls} from "../config/Constants";
import EmailService from "../services/EmailService";
import RU from "../utils/ResponsiveUtils";
import UserSessionUtils from "../utils/UserSessionUtils";

const jPink = '#DB5197';
const jPinkLight = '#F274B4';
const jYellow = '#FAB900';
const jBlue = '#47C1F1';
const jRed = '#E9504E';
const jItemBG = '#343C46';
const jSectionBG = '#343C46';
const jHoverlayBG = 'rgba(0,0,0, 0.9)';
const jMidBG = '#2B323A';
const jMidHdrBG = '#343C46';

const jFG = '#D8D8D8';
const jSec = '#b7b7b7';
const jThird = '#929292';

export default class Error extends Component {
    static async getInitialProps(ctx) {
        const { req, res, jsonPageRes, query, pathname, asPath } = ctx
        const statusCode = res
            ? res.statusCode
            : jsonPageRes ? jsonPageRes.status : null

        try {
            console.error ('Error page hit. status: ' + statusCode)
            console.log ('Error page hit. path: ' + pathname + ", asPath: " + asPath)
        } catch(error) {
            console.log('Error page hit. Exception during logging.')
        }
        let projectId = query? query.id : 'none'

        const ua = req? req.headers['user-agent'] : null
        const device = RU.getDeviceType(ua)
        const deviceProps = RU.getDeviceProps(ua)
        let userId = UserSessionUtils.retrieveUserId(req)

        const realPath = req.originalUrl

        if (realPath && realPath.length > 0 && realPath !== '/undefined' && realPath !== '/null') {
            console.log ("We've got an error and pathname " +realPath+ ". Can we redirect? ")
            console.log('asPath: '+asPath+ ', pathname: '+pathname+ ', req.url: ' +req.url+ ', req.originalUrl: ' +req.originalUrl)
            const name = realPath.indexOf('/') === 0? realPath.substring(1) : realPath
            if (VanityUrls[name]) {
                console.log ("YES! REdirecting. ")
                res.redirect ('/project/' + VanityUrls[name])
            } else {
                console.log ("NO. VanityUrls["+name+"] = " +VanityUrls[name])
                console.log ("NO. VanityUrls: " +VanityUrls)

            }
        }

        // res.redirect('projectsPage')
        return { statusCode: statusCode, pathname: pathname, projectId: projectId, userId: userId, device: device, deviceProps: deviceProps }
    }

    constructor(props) {
        super(props)

    }

    componentDidMount() {
        const {statusCode, pathname, projectId, device, deviceProps, userId} = this.props
        // if (pathname === '/undefined')
        //     return

        EmailService.sendError({
            thrower: pathname,
            subject: 'error page',
            msg: 'error ' +statusCode+ ', projectId: '+projectId,
            error: {status: statusCode, path: pathname, project: projectId, device: device, userId: userId, deviceProps: deviceProps},
            project: projectId,
            user: userId
        })
    }

    render() {
        const {statusCode, pathname} = this.props

        return (
            <div style={{fontFamily: "'Nunito Sans', sans-serif !important", padding: '2rem',
                        backgroundColor: jMidBG, color: jFG, position: 'fixed', top: 0, left: 0, right: 0, bottom: 0}}>


                <div>Something went wrong. We are working to fix it asap.</div>


            </div>
        )
    }
}
