import {Component} from 'react'
import {appLayout} from '~/_layouts'
import {Button, Container, Dimmer, Form, Header, Loader, Message} from "semantic-ui-react";

import uiStateStore from '/store/uiStateStore'
import UserApiService from "../services/UserApiService";


class ResetPassword extends Component {

    static async getInitialProps(ctx) {
        const {req, pathname, asPath} = ctx
        const isServer = !!req;

        // get reset code
        let resetCode = asPath.replace(/\/reset\/?/,'')
        let qIndex = resetCode.indexOf('?')
        if (qIndex > 0)
            resetCode = resetCode.substring(0,qIndex)

        return {resetCode: resetCode}
    }


    constructor(props) {
        super (props)
        uiStateStore.currentPage = uiStateStore.RP
        this.state = {resetCode: props.resetCode, password: '', passwordConfirm: '', passwordError: false, loading: false, showSuccess: false, showFailure: false}
    }

    handlePWChange = (ev) => {
        const pw = ev.target.value
        this.setState({password: pw})
        if (pw === this.state.passwordConfirm)
            this.setState({passwordError: false})
    }

    handlePW2Change = (ev) => {
        const pw2 = ev.target.value
        this.setState({passwordConfirm: pw2})
        if (pw2 === this.state.password)
            this.setState({passwordError: false})

    }

    handlePW2Blur = (ev) => {
        const {password, passwordConfirm} = this.state
        this.setState({passwordError: (password !== passwordConfirm)})

    }

    handleRetry = (ev) => {
        this.setState({showDefault: true, showFailure: false})
    }

    handleResetSubmit(ev) {
        const {password, passwordConfirm, userId} = this.state
        if (password !== passwordConfirm)
            this.setState({passwordError: true})
        else {
            const apiUser = {user_id: userId, password: password}
            UserApiService.updateUser(apiUser).then( response => {
                this.setState({showSuccess: true})
            }).catch(error => {
                this.setState({showFailure: true})
            })
        }
    }

    componentWillMount() {
    }

    componentDidMount() {
        UserApiService.getUserFromResetCode(this.state.resetCode).then( response => {
            if (response && response.data) {
                this.setState({userId: response.data.user_id, displayName: response.data.display_name})
            } else
                this.setState({error: response.error})
        }).catch( error => {
            // TODO how to handle? Button to retry?
        })
    }

    render() {
        const {showSuccess, showFailure, passwordError} = this.state
        const showForm = !showSuccess && !showFailure
        const pageStyle = {boxShadow: '0 0 40px rgba(0,0,0,0.3) !important', border: '1px solid $gBorder !important', backgroundColor: 'rgba(255,255,255,0.97) !important'}

        return (
            <div className="landing">
                <div className="transparent"></div>
                <div className="topBar centered">
                    <span className='welcome'>Password Reset</span>
                    <span className="detail">Pogo.io Private Beta</span>
                </div>
                <div className="centered loginWrap">
                    <div style={{textAlign: 'center'}}>
                        <Dimmer.Dimmable dimmed={this.state.loading}>
                            <Dimmer active={this.state.loading} inverted>
                                <Loader inverted style={{marginTop: '0', height: '10%'}}>Saving...</Loader>
                            </Dimmer>
                            <Container className='loginPage loginDefaults' >
                                <div className="centered">
                                    <div className="loginBox loginDefaults" style={pageStyle}>
                                        {showForm &&
                                        <div>
                                            <Header>Password Reset</Header>
                                            <Form onSubmit={() => this.handleResetSubmit ()}>
                                                <Form.Field>
                                                    <Form.Input
                                                        name='password'
                                                        value={this.state.password}
                                                        onChange={this.handlePWChange}
                                                        error={this.state.passwordError}
                                                        label='Enter a New Password' type='password'
                                                        placeholder='Password'/>
                                                </Form.Field>
                                                <Form.Field>
                                                    <Form.Input
                                                        name='passwordConfirm'
                                                        value={this.state.passwordConfirm}
                                                        onChange={this.handlePW2Change}
                                                        onBlur={this.handlePW2Blur}
                                                        error={this.state.passwordError}
                                                        label='Retype Password' type='password' placeholder='Password'/>
                                                </Form.Field>
                                                {passwordError &&
                                                <Message>
                                                    <Message.Header>Passwords don't match </Message.Header>
                                                    <p>Please try again. Passwords are case sensitive.</p>
                                                </Message>
                                                }

                                                <Button content='Reset' className="manualLogin" type='submit' fluid disabled={passwordError}/>
                                            </Form>
                                        </div>
                                        }

                                        {showSuccess &&
                                        <div>
                                            <Header>Password Changed Successfully!</Header>
                                            <p>You may now login to wrangle with your new password. </p>
                                            <br/>
                                            <a href='https://pogo.io/login'>
                                                <Button content='Login' className="go" fluid/>
                                            </a>
                                        </div>
                                        }

                                        {showFailure &&
                                        <div>
                                            <Header>Password Change Unsuccessful. </Header>
                                            <p>Something went wrong with your request. Please retry.</p>
                                            <br/>
                                            <Button content='Retry' className="go" fluid onClick={this.handleRetry}/>
                                        </div>
                                        }

                                    </div>

                                </div>

                            </Container>
                        </Dimmer.Dimmable>
                    </div>

                </div>
                <div className="legal bottom ">
                    (c) 2018 Nudge, Inc.
                    <a href='/static/tos.html' target='_blank'>Terms of Service</a>
                </div>
                <div className="credit bottom ">
                    Photo by Joshua Ness
                </div>

            </div>
        )

    }

}
export default appLayout(ResetPassword)