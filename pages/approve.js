import {Component} from 'react'
import {Grid} from 'semantic-ui-react'
import {observer} from 'mobx-react'
// import Router from 'next/router'
import routes, {Link, Router} from '~/routes'
import LeftPane from "/components/common/LeftPane";
// import PDSettings from '/components/ProjectDetail/PDSettings'
import {appLayout} from '~/_layouts'

import cardStore from '/store/cardStore'
import tagStore from '/store/tagStore'
import userStore from '/store/userStore'
import projectStore from '/store/projectStore'
import uiStateStore from '/store/uiStateStore'
import integrationStore from "/store/IntegrationStore";
import UserSessionUtils from '/utils/UserSessionUtils'

import withDragDropContext from '/components/common/WithDragDropContext'

import RU from '/utils/ResponsiveUtils'
import {VIEW_CARD, VIEW_IMAGES} from "/config/Constants"
import {createPreview} from "../utils/ResponsiveUtils";
import ApproveTitlePane from "../components/Approvals/ApproveTitlePane";
import PDWDiscussToolbar from "../components/ProjectDetail/WorkPane/PDWDiscussToolbar";
import Conversations from "../components/Conversations/Conversations";
import APWorkPane from "../components/Approvals/APWorkPane";
import ReactionApiService from "../services/ReactionApiService";
import UserApiService from "../services/UserApiService";
import UserModel from "../models/UserModel";
import JoinPrompt from "../components/Approvals/JoinPrompt";
import APWorkFeed from "../components/Approvals/APWorkFeed";
import PusherService from "../services/PusherService";
import PGMobileSidebar from "../components/common/PGMobileSidebar";

@observer
class approve extends Component {

    static async getInitialProps(ctx) {
        const {req, res, query, pathname, asPath} = ctx
        const SSR = !!req
        const ua = req ? req.headers['user-agent'] : null
        const device = RU.getDeviceType (ua)
        const deviceProps = RU.getDeviceProps (ua)

        let projectId = query.id
        if (projectId === "undefined") {
            throw new Error ("## project.js:getInitialProps: query.id is 'undefined'. Should be during clientside route changes only.")
        } else if (!projectId) {
            res.redirect ('projectsPage')
            return
        }

        console.log ('#### project detail page. Project ID: ' + projectId)

        // First, obtain min info to determine whether anon user can view this project.
        let userId = UserSessionUtils.retrieveUserId (req)

        console.log ("userId: " + (userId || 'Anon'))
/*
        if (!userId) {
            res.redirect ('/login/' + projectId + '?' + projectId)
            return
        }
*/

        // User (or anon) can view this project.
        // Update: Actually, we don't know if user can view it if it's private until we check members list.
        let user = userId ? await userStore.getUserById (userId) : null
        if (SSR === false && user !== null) {
            userStore.setCurrentUser (user, true)
        }

        return {
            userId: userId,
            user: user,
            projectId: projectId,
            device: device,
            deviceProps: deviceProps,
            ssr: SSR
        }
    }

    constructor(props) {
        super (props)

        uiStateStore.currentPage = uiStateStore.AP
        uiStateStore.showSidebar = false
        uiStateStore.voMode = false

        this.projectId = props.projectId

        if (props.user) {
            integrationStore.init (props.user.id, '')
            userStore.setCurrentUser (props.user, true)
        }

        RU.device = props.device
        RU.setDeviceProps (props.deviceProps)

        this.state = {
            view: 'cards',
            importing: false,
            importItems: [],
            importCount: 0,
            // TODO see SSOT note in APWorkPane
            inputCard: null,
            inputCardIndex: null,
            inputVote: null,
            // Moved up from APWorkPane:
            // selectedCard: null,
            // selectedCardIndex: 0,
            autoMsg: null,

            showWelcome: !props.user,
        }

    }

    /**
     * When events are pushed out (votes, card adds, comments), we can trigger updates/refreshes from here.
     * @param event
     * @code see WranglEvent.js
     */
    handleOnlinePushEvent(event, listener) {
        if (!listener)
            return
        let self = listener

        if (event.wrangl.id === projectStore.currentProject.id) {

        } else {

        }
    }

    deleteProject(project) {
        projectStore.deleteProject (project).then (response => {
            uiStateStore.disableUPFetch = true
            Router.pushRoute ('/', {update: false})
        })
    }

    leaveProject(projectId) {
        const userId = userStore.currentUser.id
        projectStore.removeUserFromProject (projectId, userId, userId, false).then (response => {
            uiStateStore.disableUPFetch = false
            Router.pushRoute ('/', {update: true})
        })
    }

    saveCard(card) {
        if (this.saveInProgress) {
            console.log ('Ignoring save already in progress')
            return
        }
        this.saveInProgress = true
        cardStore.saveCard (card).then (card => {
            this.saveInProgress = false
            this.refreshCards ()
        }).catch (error => {
            this.saveInProgress = false
        })
    }

    createCards(cards) {
        cardStore.createCardsFromAssets (cards, projectStore.currentProject).then (response => {
            this.refreshCards () // Note: CardCreate event will eventually come back to us, but could take several seconds. So grab it here, and suppress event reaction.
        })
    }

    importCards(cards) {
        cardStore.cloneCardsIntoProject (cards, projectStore.currentProject).then (response => {
            this.refreshCards ()
        })
    }

    importAssets(assets, source) {
        if (!assets || assets.length === 0)
            throw new Error ("importAssets: no assets .")

        this.setState ({importItems: assets})
        uiStateStore.importingAssets = true
        if (source === 'dropbox') {
            debugger
            // DropboxService.getShareLinks(assets).then( assetsWithLinks => {
            cardStore.createCardsFromFiles (assets, projectStore.currentProject, source).then (response => {
                const cards = response.data.cards
                tagStore.createDropboxFolderTags (cards, assets)
                cardStore.attachDropboxImagesToCards (cards, assets, this.showImportProgress.bind (this))
            })
            // })
        } else
            cardStore.createCardsFromAssets (assets, projectStore.currentProject, source).then (response => {
                // tagStore.autoTagCardsFromSources(response.data.cards, assets, this.onAutoTagComplete) works but disabled.
                this.refreshCards ()
            })
    }

    onAutoTagComplete = (success) => {
        console.log ('Autotagging complete. ')
        debugger
        this.refreshCards ()
    }

    // TODO see SSOT note in APWorkPane
    onSelCard = (card, index) => {
        this.setState({inputCard: card, inputCardIndex: index})
            // selectedCard: card, selectedCardIndex: index})
    }

    // TODO see SSOT note in APWorkPane
    onViewCard = (card, index) => {
        // this.setState({selectedCard: card, selectedCardIndex: index})
    }

    // TODO see SSOT note in APWorkPane
    onVote = (rsum) => {
        // const {inputCard} = this.state
        // if (!inputCard)
        //     debugger

        const me = userStore.currentUser
        const reactions = rsum.card_reactions || []
        const card = cardStore.getCardFromCurrent(rsum.card_id)
        card.reactions = reactions
        this.setState({inputCard: card}) // trigger trickle down props

        // auto generate a voting message
        const isApproved = reactions.find(r => r.user_id === me.id && r.reaction_type_id === ReactionApiService.getThumbsUpId()) !== undefined
        const isRejected = reactions.find(r => r.user_id === me.id && r.reaction_type_id === ReactionApiService.getThumbsDownId()) !== undefined
        const ratingObj = reactions.find(r => r.user_id === me.id && r.reaction_type_id === ReactionApiService.getStarsId())
        const isRated = ratingObj !== undefined
        const action = isApproved? 'approved' : isRejected? 'rejected' : isRated? 'rated' : null
        if (!action)
            return

        const value = isRated && ratingObj? ratingObj.reaction_value : null

        // const card = cardStore.getCardFromCurrent(rsum.card_id)
        // if (card.id !== inputCard.id)
        //     debugger

        let msg = (me.displayName || me.firstName) + ' ' + action + ' "' + card.title +'"'
        if (isRated && value)
            msg += (' ' +value+ ' star' +(value===1?'':'s')+ '.')
        this.setState({autoMsg: msg})
        /*
                setTimeout(() => {
                    this.setState({autoMsg: null})
                }, 1) // Purpose: we want to prop an update, but avoid excess calls. So flip on and off. Would a mbox solution be any better?
        */
    }

    onRemoveItem = (itemId, itemType) => {
        const {inputCard} = this.state
        if (inputCard && inputCard.id === itemId)
            this.setState({inputCard: null})
    }

    showImportProgress(count, msg) {
        if (msg === 'end') {
            this.refreshCards ()
            if (count === this.state.importItems.length) {
                this.setState ({importing: false, importItems: [], importCount: 0})
            }
        }
        else if (count === 'start') {
            this.setState ({importing: true, importCount: Math.max (count, this.state.importCount)})
        }
    }

    uploadAssets() {
        // Upload Component has uploaded assets directly. Just refresh cards.
        this.refreshCards ()

        /*
         cardStore.createCardsFromFiles(cards, projectStore.currentProject).then(response => {
         let images = fileRefs.map ( asset => asset.preview )
         cardStore.attachFilesToCards(response.data.cards, fileRefs, this.showUploadProgress.bind(this))
         })
         */
    }

    refreshCards() {
        projectStore.fetchCurrentProject (projectStore.currentProject.id).then (project => {
            console.log ("Current project has " + projectStore.currentProject.cards.length + ' cards.')
        })
        cardStore.getUserCards (userStore.currentUser.id, false, true).then (response => {
            uiStateStore.importingAssets = false
        })
    }

    refreshMembers = () => {
        projectStore.fetchCurrentProject (projectStore.currentProject.id).then (project => {
        })
    }

    refreshTagBar = () => {
        projectStore.fetchCurrentProject (projectStore.currentProject.id).then (project => {
        })
    }

    viewCards() {
        userStore.setUserPref (null, 'card_view', VIEW_CARD)
        this.setState ({view: 'cards'})
    }

    viewImages() {
        userStore.setUserPref (null, 'card_view', VIEW_IMAGES)
        this.setState ({view: 'images'})
    }

    toggleTags() {
        UserSessionUtils.setSessionPref ('showTags', !this.state.showTags)
        this.setState ({showTags: !this.state.showTags})
    }

    toggleLetters() {
        UserSessionUtils.setSessionPref ('showLetters', !this.state.showLetters)
        this.setState ({showLetters: !this.state.showLetters})
    }

    toggleActions() {
        UserSessionUtils.setSessionPref ('showActions', !this.state.showActions)
        this.setState ({showActions: !this.state.showActions})
    }


    loadComments() {
        // userStore.setUserPref(null, 'card_view', VIEW_COMMENTS)
        cardStore.getAllCardDetailsInCurrentProject ().then (cardDetails => {
            this.setState ({view: 'comments'})
        })
    }

    generatePreview(type, item, style) {
        return createPreview (type, item, style)
    }

    createProject = () => {
        projectStore.createProject().then(project => {
            Router.pushRoute('/project/'+project.id, {id: project.id})
        })
    }

    // anon user has entered name/pass into join prompt.
    onUserJoin = (ev) => {
        // location && location.reload() // last restore, works.
        // return
        // this.getAllTheStuff(this.props.projectId, userStore.currentUser.id)
    }

    // project, convo, connections, prefs
    getAllTheStuff(projectId, userId) {
        projectStore.fetchCurrentProject (projectId, userId, true).then (project => {
            userStore.getUserConnections (userId).then (response => {
                projectStore.getCurrentProjectConvo (projectId)
                    if (project && project.cards && project.cards.length > 0)
                        uiStateStore.setViewingCard(project.cards[0])
            })
        }).catch (error => {
            console.error (error)
            routes.Router.replaceRoute ('/', '/', {shallow: false})
        })
        userStore.getUserPrefs (userId)
    }

    componentDidMount() {
        uiStateStore.showDiscussion = true
        // We must auto-gen empty user for all the upcoming api calls to work. We'll then present them with customization
        if (!userStore.currentUser) {
            UserApiService.createAnonUser().then( response => {
                const newUser = new UserModel(response.data)
                userStore.setCurrentUser(newUser, true)
                this.getAllTheStuff(this.projectId, newUser.id)
            })
        } else {
            const userId = userStore.currentUser.id
            this.getAllTheStuff(this.projectId, userId)
        }
        PusherService.connect() // will (re)connect, and (re)follow our active user
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!userStore.currentUser)
            return

        const {newUserCreated} = this.state
        const userId = userStore.currentUser.id
        const prevProjectId = prevProps.projectId
        const newPropsProjectId = this.props.projectId

        if (prevProjectId !== newPropsProjectId || newUserCreated) {
            this.getAllTheStuff(newPropsProjectId, userId)
        }

    }

    render() {
        console.log ("ssr device: " + this.props.device)
        const project = projectStore.currentProject
        if (!project)
            return null

        const {inputCard, inputCardIndex, /*selectedCard, selectedCardIndex, */ autoMsg, showWelcome,
               newUserName, newUserPass} = this.state
        const workColumns = 12
        const rightColumns = 4
        const user = userStore.currentUser
        const device = RU.getDeviceType ()
        const mobile = ['computer', 'tablet'].indexOf (device) === -1

        return (
            <div>

                {/*Desktop Layout*/}
                {['computer', 'tablet'].indexOf (device) !== -1 &&
                <Grid celled className="project hidden-scroll-light"
                      style={{height: '100%', marginBottom: '0', position: 'fixed'}}>
                    <Grid.Row className="jMidBG framing" style={{boxShadow: '10px 0 10px red !important'}}>
                        <Grid.Column width={16} style={{paddingLeft: '1.5rem', paddingBottom: '1rem'}}
                                     className='jMidBG'>
                            <ApproveTitlePane project={projectStore.currentProject} user={userStore.currentUser} mobile={mobile}/>
                        </Grid.Column>

                    </Grid.Row>

                    <Grid.Row stretched style={{boxShadow: 'none', height: '90%'}}>

                        <Grid.Column width={workColumns} stretched
                                     style={{padding: 0, height: '100%'}}>
                            <APWorkPane selectedCard={uiStateStore.viewingCard}
                                        selectedCardIndex={uiStateStore.viewingCardIndex}
                                        onSelCard={this.onSelCard}
                                        onVote={this.onVote}

                            />

                            <JoinPrompt onUserJoin={this.onUserJoin}/>

                        </Grid.Column>
                        <Grid.Column width={rightColumns} stretched className='borderL discussCol' style={{padding: 0}}>
                            <PDWDiscussToolbar project={project} filterBy={uiStateStore.viewingCard}/>

                            <Conversations project={project}
                                           selectedCard={uiStateStore.viewingCard}
                                           focus={false}
                                           user={user}
                                           onViewCard={this.onViewCard}
                                           onRemoveItem={this.onRemoveItem}
                                           mobile={mobile}
                                           inputCard={inputCard}
                                           inputCardIndex={inputCardIndex}
                                           className='apComments'
                                           autoMsg={autoMsg}
                                           style={{paddingLeft: '0'}}
                                           layout='v'/>

                            <JoinPrompt onUserJoin={this.onUserJoin}/>
                        </Grid.Column>
                    </Grid.Row>

                    <style jsx>{`
                          height: 100%;
                          overflow-y: hidden;
                        `}</style>
                </Grid>
                }

                {/*Mobile Layout is more of a feed*/}
                {device === 'mobile' &&
                <span>
                <div className='fullW fullH' style={{position: 'fixed'}}>
                    <div className="mFraming fullW pad1" >
                        <ApproveTitlePane project={projectStore.currentProject} user={userStore.currentUser} mobile={mobile}/>
                    </div>
                    <div style={{overflowY: 'scroll'}} className='fullW fullH'>
                        <div className='fullW' >
                            <APWorkFeed selectedCard={uiStateStore.viewingCard}
                                        selectedCardIndex={uiStateStore.viewingCardIndex}
                                        onSelCard={this.onSelCard}
                                        onVote={(rs) => this.onVote(rs)}
                                        mobile={true}

                            />
                            <JoinPrompt/>

                        </div>
                    </div>
                </div>
                <PGMobileSidebar
                    component={
                        <LeftPane
                            projects={projectStore.userProjects}
                            totalProjects={projectStore.totalProjects}
                            createProjects={(n,name) => {this.createProjects(n,name)}}
                            user={userStore.currentUser}
                            userCards={cardStore.userCards}
                            connections={userStore.currentConnections}
                            totalConnections={userStore.totalConnections}
                            filterByUser={this.filterByUser}
                            filterByTag={this.filterByTag}
                            mobile
                        />
                    }/>
                <PGMobileSidebar
                    position='right'
                    component={
                        <div>
                            <PDWDiscussToolbar project={project} filterBy={uiStateStore.viewingCard || uiStateStore.editingCard} mobile/>
                            <Conversations project={project}
                                           selectedCard={uiStateStore.viewingCard || uiStateStore.editingCard}
                                           focus={true}
                                           user={user}
                                           mobile
                                           layout='v'/>
                        </div>
                    }
                />

                </span>
                }

            </div>
        )
    }
}

export default appLayout (withDragDropContext (approve))