// framework
import {Component} from 'react'
import PropTypes from 'prop-types'
import {observer} from 'mobx-react'
// inits
import {Initialize} from '~/_getInitialProps'
// stores
// html
// css
// import stylesheet from '~/styles/app.scss'
// or, if you work with plain css
// import stylesheet from 'styles/index.css'
import dynamic from 'next/dynamic'
// ui
// import { BaseLayout } from '../_layouts'
const CSSIMPORT = dynamic(import('../utils/cssimport'))


// icons
@observer
class Index extends Component {
    constructor(props) {
        super(props)
        this.state = {food: 'tacos', droneData: 'none'}

    }

    componentWillMount() {
    }

    componentDidMount() {
        let self = this;
        this.setState({droneData: 'lots'})
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <div>
                index.js
            </div>
        )
    }
}

export default Initialize(Index)

Index.propTypes = {
    document: PropTypes.shape({
        title: PropTypes.string,
        ogImage: PropTypes.string
    })
}

Index.defaultProps = {
    document: {
        title: 'template page',
        ogImage: ''
    }
}