import React, {Component} from "react";
import {Link} from '~/routes'
import {observer} from 'mobx-react'
import uiStateStore from "../store/uiStateStore";
import userStore from "../store/userStore";
import UserSessionUtils from "../utils/UserSessionUtils";
import withDragDropContext from "../components/common/WithDragDropContext";
import {appLayout} from "../_layouts";
import {ProjectType} from "../config/Constants";
import PWAssets from "../components/PSettingsWizard/steps/PWAssets";
import PWSecurity from "../components/PSettingsWizard/steps/PWSecurity";
import PWFinish from "../components/PSettingsWizard/steps/PWFinish";
import PWFolderTitle from "../components/PSettingsWizard/steps/PWFolderTitle";
import PWIntro from "../components/PSettingsWizard/steps/PWIntro";
import PWBoardTitle from "../components/PSettingsWizard/steps/PWBoardTitle";
import PWAPDeadline from "../components/PSettingsWizard/steps/PWAPDeadline";
import PWAPTitle from "../components/PSettingsWizard/steps/PWAPTitle";
import PWAPDesc from "../components/PSettingsWizard/steps/PWAPDesc";
import {Divider} from "semantic-ui-react";

const STEPINTRO = -1

const STEPFOLDERTITLE = 0
const STEPBOARDTITLE = 1
const STEPASSETS = 2
const STEPSECURITY = 3

const STEPAPTITLE = 4
const STEPAPDESC = 5
const STEPAPDEADLINE = 6

const STEPFINISH = 100

const folderSteps = [STEPINTRO, STEPFOLDERTITLE, STEPBOARDTITLE, STEPASSETS, STEPSECURITY, STEPFINISH]
const apSteps = [STEPINTRO, STEPAPTITLE, STEPAPDESC, STEPAPDEADLINE, STEPASSETS, STEPSECURITY, STEPFINISH]


@observer
class projectSettingsWizard extends Component {

    static async getInitialProps(ctx) {
        const {req, res, query, pathname, asPath} = ctx
        const ua = req ? req.headers['user-agent'] : null

/*
        let userId = UserSessionUtils.retrieveUserId (req)
        console.warn ('### user ID: ' + userId)
        if (!userId) {
            res.redirect ('/login/')
            return
        }
*/
        return {userId: null}

    }

    constructor(props) {
        super (props)

        uiStateStore.currentPage = uiStateStore.PS
        uiStateStore.maximizeLeftCol = true
        // this.userId = props.userId
        this.steps = [STEPINTRO]
        this.state = {
            currStep: STEPINTRO,
            currStepIndex: 0,
            showHelp: false,

            // wizard fork based on decisions
            path: 'folder',

            // folders & boards
            folderTitle: null,
            title: null, // board
            desc: null,  // board
            type: ProjectType.NORMAL,

            // approval path
            apTitle: null,
            apDescription: null,
            apDeadlineType: null,
            apDeadline: null,

            assets: []


        }

    }

    toggleHelp = (ev) => {
        this.setState ({showHelp: !this.state.showHelp})
    }

    onUpdate = (propName, propValue) => {
        if (propName === 'path') {
            this.steps = propValue === 'ap' ? apSteps : folderSteps
        }
        this.setState ({[propName]: propValue})
    }

    onBack = () => {
        const nextStepIndex = this.state.currStepIndex - 1
        const nextStep = this.steps[nextStepIndex]
        this.setState ({currStepIndex: nextStepIndex, currStep: nextStep})
        const stepElem = document.getElementById ('step' + nextStep)
        if (stepElem)
            stepElem.scrollIntoView ({block: 'start', behavior: 'smooth'})
    }


    onContinue = (propName, propValue) => {
        if (propName === 'finish') {
            alert ('So at this point, we now create the board with the data collected, and take them to it.')
            return
        }

        if (propName === 'path') {
            this.steps = propValue === 'ap' ? apSteps : folderSteps
        }

        const nextStepIndex = this.state.currStepIndex + 1
        const nextStep = this.steps[nextStepIndex]
        this.setState ({currStepIndex: nextStepIndex, currStep: nextStep, [propName]: propValue})
        // const stepElem = this.refs[currStep] // no ref
        const stepElem = document.getElementById ('step' + nextStep)
        if (stepElem)
            stepElem.scrollIntoView ({block: 'start', behavior: 'smooth'})
    }

    componentDidMount() {
        if (this.userId) {
            userStore.getUserById (this.userId).then (user => {
                userStore.setCurrentUser (user, false)
            })
        }


    }

    render() {
        // const user = userStore.currentUser
        const {path, currStep, showHelp, folderTitle, boardTitle, type, desc} = this.state

        return (
            <div className='fullW fullH jFG pad1 jSideBG'
                 style={{
                     overflowY: 'hidden'
                     // alignItems: 'center'
                 }}>
                <div className='leftGloss abs inline' style={{top: 0, width: '23%'}}>
                    <Link route='projectsPage' data-tip='Home' className='jSideBG'>
                        <img src="/static/img/pogoMulti.png" className='pointer '
                             style={{width: '150px', transition: 'width .2s'}}
                             data-tip="Home"/>
                    </Link>

                    {!showHelp &&
                    <h3 className='pointer jSec ml05' style={{marginTop: '0 !important'}}
                        onClick={this.toggleHelp}>Help</h3>
                    }

                    {showHelp &&
                    <div className='pointer ml05 mt0' onClick={this.toggleHelp}>
                        <div className='jFG mb1 '>
                            <div className='jPinkBG inline mb05 boxRadius pad1'>Folder</div>
                            <p>Contains Boards</p>
                        </div>

                        <div className='jFG mb1 ' style={{marginLeft: '2rem'}}>
                            <div className='jBlueBG inline mb05 boxRadius pad1'>Board</div>
                            <p>Contains Assets</p>
                        </div>

                        <div className='jFG mb2' style={{marginLeft: '4rem'}}>
                            <div className='jYellowBG jFGinv inline mb05 boxRadius pad1'>Asset</div>
                            <p>Images<br/>documents<br/>media</p>
                        </div>

                        <br/><br/>

                        <div className='jFG mb1 '>
                            <div className='jRedBG inline jFGinv mb05 boxRadius pad1'>Approval Request</div>
                            <p>Sends Assets for feedback</p>
                        </div>

                        <div className='jFG mb2' style={{marginLeft: '4rem'}}>
                            <div className='jYellowBG jFGinv inline mb05 boxRadius pad1'>Asset</div>
                            <p>Images<br/>documents<br/>media</p>
                        </div>
                    </div>
                    }

                </div>

                <div className='steps hidden-scroll large'
                     style={{
                         paddingTop: '2rem',
                         paddingLeft: '2rem',
                         margin: 'auto',
                         width: '62%',
                         height: '85%',
                         // border: '1px solid blue',
                         overflowY: 'auto'
                     }}>

                    <PWIntro step={STEPINTRO} active={currStep === STEPINTRO}
                             enableContinue
                             onUpdate={this.onUpdate}
                             onBack={this.onBack}
                             onContinue={this.onContinue}/>

                    {path === 'folder' &&
                    <span>
                    <PWFolderTitle
                        step={STEPFOLDERTITLE} active={currStep === STEPFOLDERTITLE}
                        enableContinue
                        onUpdate={this.onUpdate}
                        onBack={this.onBack}
                        onContinue={this.onContinue}/>
                    <PWBoardTitle
                        step={STEPBOARDTITLE} active={currStep === STEPBOARDTITLE}
                        enableContinue
                        onUpdate={this.onUpdate}
                        onBack={this.onBack}
                        onContinue={this.onContinue}/>
                    </span>
                    }

                    {path === 'ap' &&
                    <span>
                    <PWAPTitle
                        step={STEPAPTITLE} active={currStep === STEPAPTITLE}
                        enableContinue
                        onUpdate={this.onUpdate}
                        onBack={this.onBack}
                        onContinue={this.onContinue}/>

                    <PWAPDesc
                        step={STEPAPDESC} active={currStep === STEPAPDESC}
                        enableContinue
                        onUpdate={this.onUpdate}
                        onBack={this.onBack}
                        onContinue={this.onContinue}/>
                    <PWAPDeadline
                        step={STEPAPDEADLINE} active={currStep === STEPAPDEADLINE}
                        enableContinue
                        onUpdate={this.onUpdate}
                        onBack={this.onBack}
                        onContinue={this.onContinue}/>
                    </span>
                    }


                    <PWAssets step={STEPASSETS} active={currStep === STEPASSETS}
                              onUpdate={this.onUpdate}
                              enableContinue
                              onBack={this.onBack}
                              onContinue={this.onContinue}/>
                    <PWSecurity step={STEPSECURITY} active={currStep === STEPSECURITY}
                                enableContinue
                                onUpdate={this.onUpdate}
                                onBack={this.onBack}
                                onContinue={this.onContinue}/>
                    <PWFinish step={STEPFINISH} active={currStep === STEPFINISH}
                              enableContinue
                              onUpdate={this.onUpdate}
                              onBack={this.onBack}
                              onContinue={this.onContinue}/>

                    <div style={{minHeight: '80%'}}>&nbsp;</div>

                </div>
                <footer className='right'>
                    footer
                </footer>
            </div>
        )
    }
}


export default appLayout (withDragDropContext (projectSettingsWizard))