import {randomIntBetween} from "./solo/randomlntBetween";

export const DROPBOX_COLOR = '#0065fc'

export const COLORS1 = [
    '#4999d8',
    '#8fb7d5',
    '#C4DEF6',
    '#d5cdfb',
    '#aa97d7',
    '#FAD0C3',
    '#EB9694',
    '#e27720',
    '#b83e3a',
    '#a6fca2',
    '#478b45',
    '#4075aa',
    '#343434',
    '#f0f3fc',
]

export const COLORS2 = [
    'B80000', 'DB3E00', 'FCCB00', '008B02', '006B76', '1273DE', '004DCF', '5300EB', 'EB9694', 'FAD0C3', 'FEF3BD', 'C1E1C5', 'BEDADC', 'C4DEF6', 'BED3F3', 'D4C4FB'
]

export const COLORS = [
    '#505491',
    '#517fc4',
    '#7FDBFF',
    '#56cbcc',
    '#5e937b',
    '#6dffa7',
    '#ffdf8a',
    '#d89860',
    '#d35755',
    '#9c5d6c',
    '#f0b5e6',
    '#ac7db5',
    '#545454',
    '#999999',
    '#e8e6ee'
]


// Start with simple dictionary of tags - colors for consistency.
// Future: make it all editable/configurable
export const TagColors = {

}

let colorIndex = 0

class ColorUtils {
    constructor() {
        this.initSequence()
        // DefaultCardTags.map( tag => TagColors[tag] = this.getNextColor() )
        // this.initSequence()
        // DefaultProjectTags.map( tag => TagColors[tag] = this.getNextColor() )
    }

    initSequence() {
        colorIndex = 0
    }

    getNextColor() {
        return COLORS[colorIndex++ % (COLORS.length-1) ]
    }

    getNextColor2() {
        return COLORS2[colorIndex++ % (COLORS.length-1) ]
    }

    randomColor() {
        return randomIntBetween(0, COLORS.length)
    }

    hexToRgb(hex) {
        let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            R: parseInt(result[1], 16),
            G: parseInt(result[2], 16),
            B: parseInt(result[3], 16)
        } : null;
    }

    // 0 = black, 255 = white, 130 = midpoint
    getBrightness(hexColor) {
        const rgb = this.hexToRgb(hexColor)
        return Math.sqrt(
        rgb.R * rgb.R * .241 +
        rgb.G * rgb.G * .691 +
        rgb.B * rgb.B * .068)
    }

    isLight(hexColor) {
        if (!hexColor)
            return false
        if (!hexColor.startsWith('#'))
            hexColor = '#'+hexColor

        return this.getBrightness(hexColor) >= 190
    }

    isDark(hexColor) {
        if (!hexColor)
            return false
        if (!hexColor.startsWith('#'))
            hexColor = '#'+hexColor

        return this.getBrightness(hexColor) < 190
    }
}

export default new ColorUtils()