import {UserRole} from '/config/Constants'

export const getAddUserMsg = (userRole ) => {
    if (!userRole) {
        return '[user] is now in the project.'
    }

    let msg
    switch (userRole) {
        case UserRole.INVITED:
            msg = '[user] has been invited to this project.';
            break
        case UserRole.ACTIVE:
        case UserRole.ADMIN:
            msg = '[user] is a member of this project.';
            break
        case UserRole.INACTIVE_USER:
        case UserRole.INACTIVE_ADMIN:
            msg = '[user] is taking a break from this project.';
            break
        case UserRole.BANNED:
            msg = 'Sorry, but [user] has been banned from this project.';
            break
    }
    return msg
}