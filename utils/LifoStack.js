
export default class LifoStack {
    constructor() {
        this.stack = []
    }

    push(item) {
        this.stack.push(item)
    }

    pop() {
        return this.stack.pop()
    }

    get length() {
        return this.stack.length
    }

    peak() {
        return this.stack[ this.stack.length - 1 ]
    }
}
