import uaParser from 'ua-parser-js'
import {DndItemTypes} from "../config/Constants";
import cardStore from "/store/cardStore";
import userStore from "/store/userStore";
import tagStore from '/store/tagStore'
import uiStateStore from "../store/uiStateStore";
import {Image} from "semantic-ui-react";
import {handleBrokenImage} from "./solo/handleBrokenImage";

/*
    Using react-responsive for client side
    Using ui-parser for a "best guess" on server side
    TBD: if we ever use redux, react-responsive-redux combines this in one package.
    Note: semantic-ui Responsive is problematic. Don't use.
    Note2: react-responsive, or rather, my use of it, is proving problematic. Change to DIY solution.
 */

export const Mobile = { maxWidth: 767 }
export const Tablet = { minWidth: 768, maxWidth: 1024 }
export const Computer = { minWidth: 1025 }
export const LargeScreen = { minWidth: 1200, maxWidth: 1919 }
export const Widescreen = { minWidth: 1920 }

class ResponsiveUtils {
    isUaMobile = false
    isUaTablet = false
    uaString = null
    agent = null
    device = null

    getDeviceProps(uaString) { // for quick transfer from SSR to client
        if (!uaString)
            uaString = this.uaString

        return {isUaMobile: this.uaAppearsMobile(uaString),
            isUaTablet: this.uaAppearsTablet(uaString),
            uaString: this.uaString,
            agent: this.agent,
            browser: this.agent? this.agent.browser : 'unavailable',
            device: this.agent? this.agent.device : 'unavailable',
            os: this.agent? this.agent.os : 'unavailable'
        }
    }

    setDeviceProps(props) { // for quick transfer from SSR to client
        this.isUaMobile = props.isUaMobile
        this.isUaTablet = props.isUaTablet
        this.uaString = props.uaString
        this.agent = props.agent
        this.browser = props.browser
        this.os = props.os
        this.device = props.device
    }

    uaAppearsMobile(uaString) {
        if (!uaString)
            return false

        this.uaString = uaString
        this.agent = uaParser (uaString)

        console.log ('browser: ', this.agent.browser);        // {name: "Chromium", version: "15.0.874.106"}
        console.log ('device: ', this.agent.device);         // {model: undefined, type: undefined, vendor: undefined}
        console.log ('os: ', this.agent.os);             // {name: "Ubuntu", version: "11.10"}

        // mobile device or os, excluding ipad
        this.isUaMobile = (_isMobileDevice(this.agent) || _isMobileOS(this.agent)) && !_isIPad(this.agent)
        return this.isUaMobile
    }

    uaAppearsTablet(uaString) {
        if (!uaString)
            return false

        this.uaString = uaString
        this.agent = uaParser(uaString)

        console.log('browser: ', this.agent.browser);        // {name: "Chromium", version: "15.0.874.106"}
        console.log('device: ', this.agent.device);         // {model: undefined, type: undefined, vendor: undefined}
        console.log('os: ', this.agent.os);             // {name: "Ubuntu", version: "11.10"}

        this.isUaTablet = _isIPad(this.agent) || (this.agent.device && this.agent.device.type === 'tablet')
        return this.isUaTablet
    }

    /*
     console.log('engine: ', agent.engine.name);    // "WebKit"
     console.log('cpu: ', agent.cpu.architecture);   // "amd64"


    mac chrome
     browser:  { name: 'Chrome', version: '62.0.3202.94', major: '62' }
     device:  { vendor: undefined, model: undefined, type: undefined }
     os:  { name: 'Mac OS', version: '10.12.6' }

    iphone
     browser:  { name: 'Mobile Safari', version: '9.0', major: '9' }
     device:  { vendor: 'Apple', model: 'iPhone', type: 'mobile' }
     os:  { name: 'iOS', version: '9.1' }


    android
     { name: 'Chrome', version: '62.0.3202.94', major: '62' }
     { vendor: 'Samsung', model: 'SM-G900P', type: 'mobile' }
     { name: 'Android', version: '5.0' }

     { name: 'Chrome', version: '62.0.3202.94', major: '62' }
     { vendor: 'LG', model: 'Nexus 5', type: 'mobile' }
     { name: 'Android', version: '6.0' }

    desktop chrome
     { name: 'Chrome', version: '62.0.3202.94', major: '62' }
     { vendor: undefined, model: undefined, type: undefined }
     { name: 'Mac OS', version: '10.12.6' }

    desktop safari
     { name: 'Safari', version: '11.0.3', major: '11' }
     { vendor: undefined, model: undefined, type: undefined }
     { name: 'Mac OS', version: '10.12.6' }

    TBD Windows, other major

     */

    // TODO Deprecate
    isMobile() {
        if (typeof window !== 'undefined')
            return window.innerWidth < Mobile.maxWidth
        else if (this.uaString)
            return this.uaAppearsMobile(this.uaString)
        else
            return this.isUaMobile
    }

    // TODO Deprecate
    isTablet() {
        if (typeof window !== 'undefined')
            return window.innerWidth < Tablet.maxWidth && window.innerWidth > Tablet.minWidth
        else if (this.uaString)
            return this.uaAppearsTablet(this.uaString)
        else
            return this.isUaTablet
    }

    // TODO Deprecate
    isComputer() {
        if (typeof window !== 'undefined')
            return window.innerWidth > Computer.minWidth
        else if (this.uaString)
            return !this.uaAppearsMobile(this.uaString) && !this.uaAppearsTablet(this.uaString)
        else
            return !this.isUaMobile && !this.isUaTablet
    }

    getDeviceType(ua) {
        if (!ua)
            ua = this.uaString

        if (typeof window !== 'undefined') {
            if (window.innerWidth <= Mobile.maxWidth)
                this.device = 'mobile'
            else if (window.innerWidth >= Tablet.minWidth && window.innerWidth <= Tablet.maxWidth)
                this.device = 'tablet'
            else if (window.innerWidth >= Computer.minWidth)
                this.device = 'computer'
        }

        if (!this.device && ua) {
            if (this.uaAppearsTablet (ua))
                this.device = 'tablet'
            else if (this.uaAppearsMobile (ua))
                this.device = 'mobile'
            else
                this.device = 'computer'

        } else if (!this.device && !ua)
            this.device = 'computer'

        else if (this.device) {
            // keep existing
        }

        return this.device

    }

    getViewPortWidth() {
        return (typeof window !== 'undefined')? window.innerWidth : 0
    }

    getViewPortHeight() {
        return (typeof window !== 'undefined')? window.innerHeight : 0
    }

    getHeightInUse = () => {
        let height = this.device === 'mobile'? 155 : 206
        if (tagStore.currentTagSet && tagStore.currentTagSet.length > 0)
            height += 50 // tag bar visible
        if ((this.device && this.device === 'mobile') || this.isUaMobile)
            height -= 20 // toolbar labels invisible
        return height
    }


}
export default new ResponsiveUtils()



// Some mobile platforms don't provide dnd previews
// note on platforms that DO provide previews, we don't end up here. To override browser snapshot preview, generate preview in component itself.
export const createPreview = (type, item, style) => {
    console.log('Generating preview for', type, item)
    Object.assign(style, {zIndex: 3})

    if (type === DndItemTypes.CARD) {
        const card = cardStore.getCardFromCurrent (item.id)
        const size = uiStateStore.cardSizeMap['main'] || 200
        Object.assign (style, {width: size + 'px', height: (size * 1.35) + 'px'})

        return (
            <div className='cardDrag' style={style}>
                {card.image &&
                <div className='tOuter' style={{height: (size * 1.35) + 'px'}}>
                    <img className='tInner' src={card.image} onError={handleBrokenImage}/>
                </div>
                }

                {!card.image &&
                <div>{card.title}</div>
                }
            </div>
        )
    } else if (type === DndItemTypes.PERSON) {
        const user = userStore.getUserFromConnections(item.id)
        // Object.assign (style, {borderRadius: '50%' })

        return (
            <div style={style}>
                <Image avatar src={user.image} width="30" style={{boxShadow: '0 0 15px rgba(50,50,50,0.5)'}}/>
                <span className="dontWrap" style={{marginLeft: '0.5rem', textShadow: '0 0 15px rgba(50,50,50,0.5)'}}>
                        {user.displayName || user.email}
                    </span>
            </div>
        )
    } else {
        return (
            <div style={style}>
                Drop to assign
            </div>
        )
    }
}

const _isIPhone = (agent) => {
    return (agent.device && agent.device.model && agent.device.model.toLowerCase().startsWith('iphone'))
}

const _isIPad = (agent) => {
    return (agent.device && agent.device.model && agent.device.model.toLowerCase().startsWith('ipad'))
}

const _isMobileDevice = (agent) => {
    return (agent.device && agent.device.type && agent.device.type.toLowerCase().startsWith('mobile'))
}

const _isMobileOS = (agent) => {
    return (agent.os && agent.os.name && ['ios', 'android'].indexOf (agent.os.name.toLowerCase ()) !== -1)
}
