const ImgDir = '/static/img/3rdparty/'

export const GoogleDocs = 'docs.google.com'
const embeddableSites = [GoogleDocs]

export default class EmbedUtils {

    static isEmbeddable(url) {
        if (!url)
            return false

        const site = embeddableSites.find( site => url.indexOf(site) !== -1) || null
        if (false && site === GoogleDocs)
            return (url.endsWith('/pub'))
        else
            return (site !== null)
    }

    static getIconFromSource(url) {
        // TODO
    }

    static getIconFromUrl(url) {
        if (url.indexOf(GoogleDocs) !== -1)
            return ImgDir + 'googleDoc.png'
        else
            return ImgDir + 'unknown.png'
    }
}

