
class DomUtils {
    closeCallback = null

    showMask(closeCallback) {
        this.closeCallback = closeCallback
        document.querySelector('.mask').style.display = 'block'
    }

    hideMask(ev) {
        document.querySelector('.mask').style.display = 'none'
        if (this.closeCallback !== null) {
            this.closeCallback(ev)
            this.closeCallback = null
        }

    }

}

export default new DomUtils()