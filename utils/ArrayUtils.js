import moment from 'moment'

export default class ArrayUtils {

    // Note: assumes arrays contain objects with ids.
    static arrayUnion = function(a1, a2) {
        return a1.filter( a1Item => a2.find( a2Item => a2Item.id === a1Item.id) !== undefined );
        // return a1.filter( item => a2.indexOf(item) < 0 );
    }

    // Note: assumes arrays contain objects with ids.
    static arrayDifference = function(a1, a2) {
        return a1.filter( a1Item => a2.find( a2Item => a2Item.id === a1Item.id) === undefined );
        // return a1.filter( item => a2.indexOf(item) < 0 );
    }

    static arrayDiffByProp = function(a1, a2, propName) {
        return a1.filter( a1Item => a2.find( a2Item => a2Item[propName] === a1Item[propName]) === undefined );
        // return a1.filter( item => a2.indexOf(item) < 0 );
    }

    // Note: assumes array contains objects with title, image, and url.
    // Update: "matching" images won't necessarily pass equality test, likely due to generated thumbnails. Leaving out of filter for now.
    static arrayUnique(ary) {
        return ary.filter( (origItem, index) => {
            /*const foundIdx =*/ return ary.findIndex( anItem => {
                return anItem.title === ary[index].title && anItem.url === ary[index].url
                }) === index
            // console.log('index: ' +index+ ', foundIndex: ' +foundIdx+ ', origItem: ' +origItem.title+ ', foundItem: ' +ary[foundIdx].title+ '. Include? ' +(index === foundIdx))
            // return index === foundIdx
        })
    }

    // Note: designed for arrays of objects containing a userId
    // TODO take an array of properties to evaluate by
    static uniqueUsers(ary) {
        if (!ary || ary.length === 0)
            return []

        return ary.filter( (origItem, index) => {
            return ary.findIndex( anItem => {
                return anItem.userId === ary[index].userId
            }) === index
        }) || []
    }

    // attempt at generic unique array support
    static removeDupsByProperty(ary, prop) {
        if (!ary || ary.length === 0)
            return []

        return ary.filter( (origItem, index) => {
            return ary.findIndex( anItem => {
                return anItem[prop] === ary[index][prop]
            }) === index
        }) || []
    }

    // sort comparator: return true if all specified props of objects a and b are equal
    static allPropsEqual(a,b, props) {
        let prop
        for (let i=0 ;i<props.length; i++) {
            prop = props[i]
            if (a[prop] !== b[prop])
                return false
        }
        return true
    }

    // return true if ary appears to be sorted in ascending date order. ascendy == truthy. api returns sorted.
    static isAscendyByDate(ary, dateProp) {
        if (!ary || ary.length === 0)
            return false

        const first = ary[0][dateProp]
        const last = ary[ ary.length - 1][dateProp]
        return moment(first).isBefore(last)
    }

    // mobx arrays are objects. This syntax: [].concat.apply([], mobxArrays) fails.
    // let's flatten via reduce
    static flattenMobxArrays( arrayOfArrays ) {
        if (!arrayOfArrays || arrayOfArrays.length === 0)
            return arrayOfArrays

        arrayOfArrays = arrayOfArrays.map( arr => {
            if (arr && arr.slice !== undefined)
                return arr.slice ()
            else {
                console.error('flattenMobx: arr does not have slice. type and object: ', (typeof arr), arr)
                return arr
            }
        })
        return arrayOfArrays.reduce(function (flat, toFlatten) {
            return flat.concat( toFlatten )
        }, [])
    }
}