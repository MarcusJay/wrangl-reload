import {getExt} from "./getExt";

export const imageExts = ['png','jpg','jpeg', 'gif', 'bmp', 'tif']
export const imageMimes = ['image/png','image/jpeg','image/pjpeg', 'image/gif', 'image/bmp', 'image/tifd']

export const isImageFromName = (fileName) => {
    const ext = getExt(fileName)
    return imageExts.indexOf(ext) !== -1
}

export const isImageFromMime = (mime) => {
    return imageMimes.indexOf(mime) !== -1
}
