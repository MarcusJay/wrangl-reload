export const stripHtml = (s) => {
    return String(s).replace(/<.+>/g, '')
}
