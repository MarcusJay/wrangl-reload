export const getShortUrl = (url, extraShort) => {
    if (!url)
        return url;
    let regex = /^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)/im;
    let results = regex.exec(url);
    if (!results)
        return url
    else
        return extraShort ? results[1] : results[0];
}
