const htmlEntityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
};

export const escapeHtml = (html) => {
    return String(html).replace(/[&<>"'`=\/]/g, function (s) {
        return htmlEntityMap[s]
    })
}
