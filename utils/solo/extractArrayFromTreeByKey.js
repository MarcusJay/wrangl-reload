import {getUserByNameFromCollection} from "./getUserByNameFromCollection";
import isGUID from "./isGUID";

export default function({key, tree}) {
    if (!key || !tree)
        return []

    let cardMap = {}

/*
    LEFT OFF:
    IN ORDER OF EFFICIENCY:
    1) UNSORTED ARRAY O(N)
    2) SORTED ARRAY O(LOG N)
    3) MAP O(1)
*/

    function getAryFromTree({key, targetKey, treeOrArray}) {

        // return all items in collection
        if (key === targetKey && typeof treeOrArray === 'object') {
            Object.assign(cardMap, treeOrArray)
            console.log(`EXTRACT Added ${Object.keys(treeOrArray).length} ${key}s!`)
        }

        // we have an obj, not array
        const isArray = Array.isArray (treeOrArray)
        const childKeys = isArray? treeOrArray : Object.keys (treeOrArray)

        // we're at leaf
        if (childKeys.length === 0) {
            console.log(`EXTRACT Hit leaf at key ${key}`)
            return
        }

/*
        // children are all GUIDs. ignore
        if (isGUID (children[0])) {
            console.log(`EXTRACT Hit leaf at key ${key}`)
            return
        }
*/

        // check child keys of interest
        childKeys.forEach(childKey => {
            console.log('EXTRACT Checking child key ' +childKey)
            if (typeof treeOrArray[childKey] === 'object' || Array.isArray(treeOrArray[childKey])) {
                console.log ('EXTRACT recursing...')
                return getAryFromTree ({key: childKey, targetKey: targetKey, treeOrArray: treeOrArray[childKey]})
            }
        })
    }

    Object.keys (tree).forEach (childKey => {
        getAryFromTree ({key: childKey, targetKey: key, treeOrArray: tree[childKey]})
    })

    return cardMap

}