import {getUrlComponents} from "./getUrlComponents";

export const getHostname = (url) => {
    if (!url)
        return null

    const urlObj = getUrlComponents(url)
    let  host = urlObj.hostname         // 'en.wikipedia.org
    // TODO can probably do the following with 1 mad regex.
    let i = host.lastIndexOf('.')
    if (i > 0)
        host = host.substring(0,i)      // 'en.wikipedia
    i = host.indexOf('.')
    if (i >= 0 && host.length > 1)
        host = host.substring(i+1)      // 'wikipedia
    return host
}
