export const truncate = (str, len, ellipsis) => {
    if (!str || !len)
        return str
    const origLen = str.length
    if (len >= origLen)
        return str

    str = str.substring(0,len)
    if (ellipsis)
        str += '...'
    return str
}
