import userStore from '/store/userStore'

// support api swap for user invites 4
export default (event) => {
    if (!event)
        return

    const origUserId = event.userId
    event.userId = event.friendId
    event.friendId = origUserId

    if (event.user !== null)
        event.user = userStore.getUserFromConnections(event.userId)
    if(event.friend !== null)
        event.friend = userStore.getUserFromConnections(event.friendId)

}