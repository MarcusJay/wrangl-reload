export const stripLeadingSlashes = (str) => {
    while (str.charAt(0) ==='/') {
        str = str.substr(1)
    }
    return str
}
