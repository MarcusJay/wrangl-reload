export const randomStr = (len) => {
    if (!len)
        len = 7
    let str = "";
    let range = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";

    for (let i = 0; i < len; i++)
        str += range.charAt(Math.floor(Math.random() * range.length));

    return str;
}
