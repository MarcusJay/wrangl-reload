export function isElementInView (elem, threshold, mode) {
    threshold = threshold || 0;
    mode = mode || 'visible';

    let rect = elem.getBoundingClientRect();
    let viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    let above = rect.bottom - threshold < 0;
    let below = rect.top - viewHeight + threshold >= 0;

    return mode === 'above' ? above : (mode === 'below' ? below : !above && !below);
}
