// get file extension if any
export const getExt = (str) => {
    if (!str)
        return null

    const extIdx = str.lastIndexOf('.')
    if (extIdx <= 0 || extIdx > str.length - 2)
        return null

    return str.substring(extIdx+1)
}
