'use strict';

export const isAtWordStart = (text) => {
    return !text ||
            text === '' ||
            (text.length > 0 && text.endsWith(' '))
}
