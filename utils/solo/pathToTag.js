export const pathToTag = (path) => {
    if (!path)
        return path

    return path.replace(/\//g, ' ').trim()
}
