'use strict';

import {parse, tldExists, isDomain} from 'tldjs'
import normalizeUrl from 'normalize-url'

import { getUrlComponents } from './getUrlComponents'
import isAbsoluteURL from './isAbsoluteURL'
import combineURLs from './combineURLs'
import prependProtocol from './prependProtocol'

const options = {normalizeHttps: true, normalizeProtocol: true, removeTrailingSlash: true}

module.exports = function normalizeAnyUrl (url, allegedHost) {
    if (!url)
        return url

    /*

        if url is absolute, at all, whether same host or not,
            we are done here

        if not, let's consider possibilities
            1. it's relative to current host
            combineUrl host with url

            2. it's relative to some other url
            impossible. info doesn't exist.

            3. it's just missing a scheme/protocol.
            Prepend one.

     */

    // attempt normalization
    let normalizedUrl = null
    try {
        normalizedUrl = normalizeUrl (url, options)
        url = normalizedUrl
    } catch (error) {
        console.warn('Error normalizing url ', error, url)
    }

    if (isAbsoluteURL(url))
        return url

    // attempt parsing
    let props = {}, parts = {}
    try {
        props = parse (url)
        parts = getUrlComponents (url)
    } catch (error) {
        console.warn('Error parsing url ', error, url)
    }
    // parse props:
    //   hostname: 'spark-public.s3.amazonaws.com',
    //   isValid: true, means it's parseable, not necessarily a well known domain name
    //   isIp: false,
    //   tldExists: true,
    //   publicSuffix: 's3.amazonaws.com',
    //   domain: 'spark-public.s3.amazonaws.com',
    //   subdomain: ''
    // parts:
    //     href: url,
    //     protocol: match[1],
    //     host: match[2],
    //     hostname: match[3],
    //     port: match[4],
    //     pathname: match[5],
    //     search: match[6],
    //     hash: match[7]


    // attempt validation
    let domainExists = false, isAbsolute = false
    try {
        domainExists = tldExists (url)
        isAbsolute = isAbsoluteURL (url)
    } catch (error) {
        console.warn('Error validating url ', error, url)
    }

    // best case: absolute url
    if (isAbsolute)
        return normalizeUrl(url, options)

    // url is relative or is missing scheme or protocol
    const protocol = parts.protocol && parts.protocol.length > 0? parts.protocol : 'http:'
    const baseUrl = (parts.host && parts.host.length > 0)? parts.host : (parts.hostname && parts.hostname.length > 0)? parts.hostname : null
    const urlContainsTLD = (props && props.tldExists || !!props.hostname) || (parts && !!parts.host || !!parts.hostname)
    const host = props.hostname || props.domain || allegedHost

    // relative url: missing host
    if (!urlContainsTLD && host) {
        url = combineURLs(host, url)

    // relative url: has host, missing protocol
    } else if (urlContainsTLD) {
        url = prependProtocol(protocol, url)
    }

    // At this point, we hope to have a valid absolute url. So let's check this new url again.

    // Note: This may seem excessive, but I can't tell you how many sites have had issues being scraped.
    // Let's make this bullet proof.
    // BEGIN 2nd CHECK
    normalizedUrl = null
    try {
        normalizedUrl = normalizeUrl (url, options)
        url = normalizedUrl
    } catch (error) {
        console.warn('Error normalizing repaired url ', error, url)
    }

    props = {}, parts = {}
    try {
        props = parse (url)
        parts = getUrlComponents (url)
    } catch (error) {
        console.warn('Error parsing repaired url ', error, url)
    }

    domainExists = false, isAbsolute = false
    try {
        domainExists = tldExists (url)
        isAbsolute = isAbsoluteURL (url)
    } catch (error) {
        console.warn('Error validating repaired url ', error, url)
    }
    // END 2nd Check

    // We've validated, repaired, and revalidated. Now we GOTTA go with what we have.
    try {
        if (isAbsolute)
            return normalizeUrl (url, options)
        else if (parts && parts.href && parts.href.length > 0)
            return normalizeUrl (parts.href, options)
    } catch (error) {
        console.error('Error normalizing final repaired url ', error, url)
        return parts.ref? parts.ref : url
    }

    return url

}

