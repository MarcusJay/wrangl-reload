const DOUBLE_CLICK_THRESHOLD = 400 // ms

export const isDoubleClick = (click1Time, click2Time) => {
    if (!click1Time || !click2Time)
        return false;
    return Math.abs(click2Time - click1Time) < DOUBLE_CLICK_THRESHOLD
}
