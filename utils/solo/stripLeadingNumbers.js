export const stripLeadingNumbers = (str) => {
    while (str.charAt(0) >= '0' && str.charAt(0) <= '9') {
        str = str.substr(1)
    }
    return str
}
