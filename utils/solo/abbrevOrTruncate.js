export const abbrevOrTruncate = (str, len) => {
    if (!str || str.length === 0 || !len)
        return str

    let s = ''
    if (str.indexOf(' ') > 0) {
        let ary = str.split(' ')
        ary.forEach( token => {
            s += token.charAt(0).toUpperCase()
        })
    } else
        s = str
    return s.substring(0,len)
}
