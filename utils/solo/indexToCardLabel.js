// convert a card index into friendly letter label
export const indexToCardLabel = (index) => {
    let charIndex = index % 26
    let labelLen = Math.floor(index / 26) + 1
    let char = String.fromCharCode (65 + charIndex)
    let label = ''

    for (let i=0; i<labelLen; i++) {
        label += char
    }
    return label

}
