
export const getUserByNameFromCollection = (name, users) => {
    if (!name || !users || users.length === 0)
        return null

    if (name.startsWith('@'))
        name = name.substring(1)

    // TODO quick fix. Determine how undefined user is getting into at_users
    return users.find (user => user !== null && user !== undefined && user.displayName === name) || null
}