export const spaceToPlus = (str) => {
    if (!str)
        return str
    return str.replace(/ /g, '+');
}
