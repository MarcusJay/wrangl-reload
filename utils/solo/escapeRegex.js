export const escapeRegex = (regex) => {
    return regex.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}
