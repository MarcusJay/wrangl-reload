export const preventDefault = (event) => {
    event.stopPropagation()
    event.preventDefault()
}
