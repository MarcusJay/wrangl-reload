export const redirect = (ctx, destination) => {
    if (ctx.res) {
        ctx.res.writeHead(302, { Location: destination })
        ctx.res.end()
    } else {
        document.location.pathname = destination
    }
}
