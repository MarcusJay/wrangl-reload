export const generateGUID = () => {
    let uuid = "";
    let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let tokenLengths = [8,4,4,4,12]
    let charIndex

    for (let tIndex=0; tIndex<tokenLengths.length; tIndex++) { // for each segment (token)
        for(let i = 0; i < tokenLengths[tIndex]; i++) { // generate token
            charIndex = Math.floor( Math.random() * chars.length )
            uuid += chars.charAt( charIndex );
        }
        if (tIndex < (tokenLengths.length-1))
            uuid += "-"
    }

    return uuid;

}
