const urlRegex = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/

export const isUrl = (s) => {
    if (!s || s.length === 0)
        return false

    if (!s.startsWith('http'))
        s = 'http://' + s
    return urlRegex.test(s);
}
