export const capitalize = (str) => {
    if (!str || str.length === 0)
        return str
    return str.charAt(0).toUpperCase() + str.substring(1)
}
