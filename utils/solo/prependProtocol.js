'use strict';

export default function prependProtocol(protocol, url) {

  return protocol && url
    ? protocol.replace(/:/, '').replace(/\/+$/, '') + '://' + url.replace(/^\/+/, '')
    : url
};
