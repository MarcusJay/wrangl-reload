import {IMAGE, SITE, DOCUMENT} from "../../config/Constants";

const imageRE = /image\/.+$/
const docRE = /application\/.+$/

export const mimeToUrlType = (mime) => {
    if (typeof mime === 'object' && mime.hasOwnProperty('type'))
        mime = mime.type

    if (imageRE.test(mime))
        return IMAGE

    if (docRE.test(mime))
        return DOCUMENT

    return SITE
}
