export const firstWord = (str) => {
    if (!str || str.length === 0)
        return str
    const firstSpace = str.indexOf(' ')
    return firstSpace !== -1? str.substring(0, firstSpace) : str
}
