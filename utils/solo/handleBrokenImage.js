export const handleBrokenImage = (ev) => {
    if (!ev || !ev.target)
        return
    ev.target.src = '/static/img/imgNotFound.jpg'
}
