export function scaleImage(url, width, height){
    const img = new Image()

    img.src = url
    img.setAttribute('crossOrigin', "Anonymous")

    return new Promise( (resolve, reject) => {

        img.onload = () => {
            const canvas = document.createElement("canvas")
            const ctx = canvas.getContext("2d");

            canvas.width = width
            canvas.height= height

            // draw the img into canvas
            ctx.drawImage(img, 0, 0, width, height)
            const img2 = new Image()
            img2.src = canvas.toDataURL()
            resolve (img2)
        }
    })

}
