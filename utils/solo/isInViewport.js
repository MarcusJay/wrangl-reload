const MAXVP = 5000

export default function (elem) {
    if (!elem)
        return false
    let rect = elem.getBoundingClientRect()
    if (!rect)
        return false

    // SSR
    // Make default a max rather than 0.
    // If a normal elem satisfies first 2, and latter 2 are unavailable, assume fit. We
    // 're not often working with elems larger than viewport
    const height = window && window.innerHeight
        ? window.innerHeight
        : document && document.documentElement.clientHeight
            ? document.documentElement.clientHeight
            : MAXVP

    const width = window && window.innerWidth
        ? window.innerWidth
        : document && document.documentElement.clientWidth
            ? document.documentElement.clientWidth
            : MAXVP

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= height &&
        rect.right <= width
    );
};
