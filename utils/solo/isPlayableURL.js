/*
YouTube videos use the YouTube iFrame Player API
Facebook videos use the Facebook Embedded Video Player API
SoundCloud tracks use the SoundCloud Widget API
Streamable videos use Player.js
Vidme videos are no longer supported
Vimeo videos use the Vimeo Player API
Wistia videos use the Wistia Player API
Twitch videos use the Twitch Interactive Frames API
DailyMotion videos use the DailyMotion Player API
Supported file types are playing using <video> or <audio> elements
HLS streams are played using hls.js
DASH streams are played using dash.js
 */

const regex = /youtube|streamable|vimeo|wistia|twitch|dailymotion/
export const isPlayableURL = (url) => {
    if (!url || url.length === 0)
        return false

    return regex.test(url);
}
