// Base64 encode tools to workaround utf8 problem see
// https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding#The_Unicode_Problem

export const b64DecodeUnicode = (str) => {
    if (typeof window !== 'undefined') {
        return decodeURIComponent (window.atob (str).split ('').map (function (c) {
            return '%' + ('00' + c.charCodeAt (0).toString (16)).slice (-2);
        }).join (''));
    } else {
        let buff = new Buffer(str, 'base64');
        return buff.toString('ascii');
    }
}


