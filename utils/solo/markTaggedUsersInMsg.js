
export const markTaggedUsersInMsg = (msg, users) => {
    if (!msg || msg.length === 0 || !users || users.length === 0)
        return msg

    let userRE, userName, safeName
    let tempScopeCopy = users
    users.forEach( user => {

        if (!user) { // user could be deleted or disconnected
            return
        }
        // Delimit all occurrences. Allows multiple tags of same user.
        userName = user.displayName || user.firstName
        if (userName) {
            safeName = user.displayName.replace (/\s/g, '_') // convert to single token
            userRE = new RegExp ('@' + user.displayName, "g"); // find globally
            msg = msg.replace (userRE, '@' + safeName)               // replace all
        }
    })
    return msg
}
