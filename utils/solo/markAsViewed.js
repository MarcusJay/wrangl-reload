import {MARK_AS_READ_TIMEOUT} from "../../config/EventConfig";
import eventStore from "../../store/eventStore";
import userStore from '/store/userStore'
import cardStore from '/store/cardStore'
import projectStore from '/store/projectStore'

const markAsViewed = (itemId, itemType, callback, timeout = MARK_AS_READ_TIMEOUT) => {
    if (!itemId || !itemType) {
        console.log (`markAsView: missing params: ${itemId}, ${itemType}`)
        return {unreadCommentIds: [], firstNewCommentId: null}
    }

    const evs = eventStore.getInstance ()

    // use flattened dictionaries where available to skip tree traversal
    if (itemType === 'card' && evs.cardBadgesFlat[itemId]) {

        const cardEvent = evs.cardBadgesFlat[itemId]
        if (cardEvent.new === true) {
            setTimeout (() => {
                evs.dismissUserBadges ([itemId], 'item_ids')
            }, timeout)
        }
        return {unreadCommentIds: [], firstNewCommentId: null}
    }

    const cagBadges = evs.currentBadges? evs.currentBadges.categories : null
    if (!cagBadges)
        return {unreadCommentIds: [], firstNewCommentId: null}

    if (itemType === 'project') {
        // Do we need to retrieve the actual object? It's a good safety measure if readily available
        const project = projectStore.getProjectFromUserProjects (itemId)

        // Project: Clear isNew flag. Conversation will clear comments.
        if (project) {
            Object.keys (cagBadges).forEach (cagId => {
                if (cagBadges[cagId] && cagBadges[cagId].projects && cagBadges[cagId].projects[project.id]) {
                    // clear isNew flag
                    const isNew = cagBadges[cagId].projects[project.id].new === true
                    const isNewEventId = isNew ? cagBadges[cagId].projects[project.id].event_id : null
                    if (isNew && isNewEventId) {
                        setTimeout (() => {
                            evs.dismissUserBadges ([isNewEventId])
                        }, timeout)
                    }
                }
            })
        }
        return {unreadCommentIds: [], firstNewCommentId: null}
    }

    if (itemType === 'comments') {
        const project = projectStore.getProjectFromUserProjects (itemId)
        let unreadMsgEventIds, comments, unreadCommentIds = [], firstNewCommentId = null
        if (project) {
            Object.keys (cagBadges).forEach (cagId => {
                if (cagBadges[cagId] && cagBadges[cagId].projects && cagBadges[cagId].projects[project.id]) {
                    comments = cagBadges[cagId].projects[project.id].project_comments
                    if (comments && comments.length > 0) {
                        unreadCommentIds = comments.map( cmt => cmt.item_id).sort( (a,b) => b.event_id - a.event_id)
                        firstNewCommentId = comments[comments.length-1].item_id
                        unreadMsgEventIds = comments.map (cmt => cmt.event_id)

                        setTimeout (() => {
                            evs.dismissUserBadges (unreadMsgEventIds)
                            if (callback)
                                callback(itemId)
                        }, timeout)
                        return ({unreadCommentIds: unreadCommentIds, firstNewCommentId: firstNewCommentId})
                    }
                    return ({unreadCommentIds: [], firstNewCommentId: null})
                }
                return ({unreadCommentIds: [], firstNewCommentId: null})
            })
        }
        return ({unreadCommentIds: [], firstNewCommentId: null})
    }

    if (itemType === 'contact') {
        // update new user badges
        if (evs.currentBadges && evs.currentBadges.direct_messages) {
            const senders = evs.currentBadges.direct_messages.senders
            if (senders && senders[itemId] && senders[itemId].total > 0) {
                const eventIds = senders[itemId].events.map( event => event.event_id) || []
                if (eventIds.length > 0) {
                    setTimeout (() => {
                        evs.dismissUserBadges (eventIds)
                    }, timeout)
                }
            }
        }
    }
}

export default markAsViewed