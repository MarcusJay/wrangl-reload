import {randomIntBetween} from "./randomlntBetween";

const ltrs = 'abcdefghijklmnopqrstuv'
export default (limit) => {
    const i = randomIntBetween(0,limit)
    return ltrs.charAt(i)
}
