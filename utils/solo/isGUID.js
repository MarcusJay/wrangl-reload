const RE = /[A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12}/

export default function isGUID(str) {
    return str && str.length === 36? RE.test(str) : false
}