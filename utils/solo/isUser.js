const userRegex = /^@[\w-_.]+$/

export const isUser = (s) => {
    return false

    if (!s || s.length === 0)
        return false

    return userRegex.test(s);
}
