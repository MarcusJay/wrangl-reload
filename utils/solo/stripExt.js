// strip extension
import {getExt} from "./getExt";

export const stripExt = (str) => {
    const ext = getExt(str)
    if (ext)
        return str.replace('.'+ext, '')
    else
        return str
}
