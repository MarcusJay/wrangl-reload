export default function(s) {
    if (!s || s.length === 0)
        return s

    return s.replace(/-/g, '')
}