import {getUrlComponents} from "./getUrlComponents";
import {DOCUMENT, GOOGLE_DOC, IMAGE, SITE} from "../../config/Constants";

export const getUrlType = (url) => {
    const urlObj = getUrlComponents(url)
    let imageMatch = (/\.(gif|jpg|jpeg|tiff|png|webp)$/i).test(urlObj.pathname)
    let docExtMatch = (/\.(doc|pdf|txt|csv|docx)$/i).test(urlObj.pathname)
    let docPrefixMatch = (/^(docs|drive|doc)/i).test(urlObj.host)
    let gdocPrefixMatch = (/^(docs.google.com)/i).test(urlObj.host)
    let docPathMatch = (/^(\/document)/i).test(urlObj.pathname)

    if (imageMatch)
        return IMAGE
    else if (gdocPrefixMatch)
        return GOOGLE_DOC
    else if (docExtMatch || docPrefixMatch || docPathMatch)
        return DOCUMENT
    else
        return SITE
}
