// TODO can we replace this with boolean comment column is_auto or equiv
export default function isAutoMsg(author, msg) {
    if (!author || !msg)
        return false

    const authorName = author.displayName || author.firstName
    const actions = ['approved', 'rejected', 'rated']

    msg = msg.toLowerCase()
    for (let i=0; i<actions.length; i++) {
        if (msg && (msg.indexOf(authorName.toLowerCase() + ' ' +actions[i]) === 0) ||
                   (msg.indexOf(authorName.toLowerCase() + ': ' +actions[i]) === 0))
            return true
    }
    return false
}