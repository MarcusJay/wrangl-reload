import UserApiService from '../../services/UserApiService'
import UserModel from "../../models/UserModel";

/*
export const getAuthor = (comment) => {
    return new Promise( (resolve, reject) => {
        let author = comment.user
        if (!author)
            author = userStore.getUserFromConnections(comment.comment_creator)

        if (author) {
            resolve(author)
            return
        }

        UserApiService.getPublicUsersById(comment.comment_creator).then( response => {
            const apiUser = response.data.users && response.data.users.length > 0? response.data.users[0] : null
            resolve( apiUser? new UserModel(apiUser) : null)
        })
    }).catch( error => {
        console.error('Error fetching author ' +comment.comment_creator)
        resolve(null)
    })
}
*/

export const hydrateComment = (comment, userFinder, cards) => {
    if (!comment.user && typeof userFinder === 'function')
        comment.user = userFinder(comment.comment_creator)

    if (comment.at_users && userFinder) {
        let user
        comment.at_users = comment.at_users.map (atUserId => {
            if (typeof atUserId === 'string') {
                user = userFinder(atUserId)
                return user ? {id: atUserId, image: user.image, displayName: user.displayName} : null
            }
        })
    }
    return comment

    // at_cards info already provided by api
}

export const hydrateConversation = (convo, userFinder, cards) => {
    if (!convo || convo.length === 0)
        return convo
    convo.forEach( (comment, i) => hydrateComment(comment, userFinder, cards) )
    return convo
}

