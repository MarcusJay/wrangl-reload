export const reverseTruncate = (str, len, ellipsis) => {
    if (!str || !len)
        return str
    const origLen = str.length
    if (len >= origLen)
        return str

    str = str.substring(origLen - len)
    if (ellipsis)
        str = '...' +str
    return str
}
