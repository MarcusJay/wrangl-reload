export const addLineBreaks = (str) => {
    return str.replace(/\. /g, '.<br/>')
}
