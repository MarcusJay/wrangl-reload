export default (str) => {
    if (!str || str.length === 0)
        return str

    return str
        .match (/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
        .map (x => x.toLowerCase ())
        .join ('_')

}