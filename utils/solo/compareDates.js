import moment from 'moment'

export const compareDates = (d1, d2) => {
    if (moment(d1).isBefore(d2))
        return -1
    if (moment(d1).isAfter(d2))
        return 1
    return 0
}
