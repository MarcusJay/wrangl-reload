import ssrHttpCookie from 'cookie'
import jsCookie from 'js-cookie'

class UserSessionUtils {

    /**
     * Get ID of user with session, if one exists, regardless of server or client side render.
     * @param req
     */
    retrieveUserId(req) {
        let userId = null

        if (req) { // server side
            console.log("SSR: Checking for User ID: ")
            const cookies = req.headers.cookie;
            if (typeof cookies === 'string') {
                const cookiesJSON = ssrHttpCookie.parse (cookies);
                console.log("cookies: " +cookies)
                userId = cookiesJSON.userId;
                console.log("SSR: User ID is " +userId+ ". type: " +(typeof userId))
            } else {
                console.log("SSR: no cookies from jsCookie")
            }
        } else {
            userId = jsCookie.get('userId')
            console.log("Clientside render: User ID is " +userId)
        }
        return userId
    }

    endUserSession() {
        // userStore.logoutUser() moved to login
        jsCookie.remove('userId');
    }

    // clientside only
    getSessionPref(name) {
        if (!name)
            throw new Error('getUserPref: no pref name')

        let val = jsCookie.get(name)
        console.log("User pref " +name+ " = " +val)
        return (['true','false'].indexOf(val) >= 0)? val === 'true' : val
    }

    // clientside only
    setSessionPref(name, value) {
        if (!name)
            throw new Error('setUserPref: no pref name')

        if (!value)
            jsCookie.remove(name)

        jsCookie.set(name, value)
        console.log("Set user pref " +name+ " to " +value)
    }

    setDefaultPrefs() {
        this.setSessionPref('cropImages', true)
        this.setSessionPref('showDescriptions', false)
        this.setSessionPref('showTags', true)
        this.setSessionPref('showLetters', true)
        this.setSessionPref('showActions', true)
        this.setSessionPref('showAuthors', true)
        this.setSessionPref('showExtensions', false)
    }

}

export default new UserSessionUtils()