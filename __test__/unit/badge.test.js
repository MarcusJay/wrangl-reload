import React from 'react';
import Enzyme, { shallow, ShallowWrapper} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import DumbBadge from "../../components/common/DumbBadge";
import {randomInt} from "../../utils/solo/randomlnt";
import {randomIntBetween} from "../../utils/solo/randomlntBetween";

// jest.dontMock('.DumbBadge');
Enzyme.configure({ adapter: new Adapter() })

describe("Dumb Badge suite", function() {
    it ("renders parent correctly.", function () {
        const wrapper = shallow (<DumbBadge count={1}/>);
        const badgeDiv = wrapper.find('div.dumbBadge')
        expect(badgeDiv).toHaveLength(1)
    })

    it ("renders a single count.", function () {
        const count = randomIntBetween (1, 20)
        const wrapper = shallow (<DumbBadge count={count}/>);
        const badgeDiv = wrapper.find('div.dumbBadge')
        const counts = badgeDiv.findWhere (node => node.type() !== 'div') // text node has type undefined. Wish we could get 'number' or 'text'
        expect (counts).toHaveLength(1)
    })

})
