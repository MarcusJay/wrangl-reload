import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import {createCategory, deleteCategory} from "../../services/CategoryApiService";
import {APPROVAL_CAT, ARCHIVED_CAT, DEFAULT_CAT, REQUEST_CAT} from "../../config/Constants";
import {randomStr} from "../../utils/solo/randomStr";
import projectStore from "../../store/projectStore";

const testUserId = "1C9DD750-FEC0-93C8-8922-FB70A6B5E640" // Not a Lion
const minCats = 4; // 0 default, 1 ap, 2 reqs, 3 archive

describe('Project Store - Integration', () => {
    test ('getUserProjects expected fields and lengths', () => {
        expect.hasAssertions ()
        const ps = projectStore
        return ps.getUserProjects (testUserId).then (response => {
            expect (response).toBeDefined ()

            // Presence
            expect (response.projects).toBeDefined ()
            expect (response.folio).toBeDefined ()

            // Lengths
            expect(ps.totalProjects === response.projects.length)
            expect (response.folio.length).toBeGreaterThan (3)

            // expect predefined categories
            const folio = response.folio
            expect (folio.find( cat => cat.id === DEFAULT_CAT)).toBeDefined();
            expect (folio.find( cat => cat.id === APPROVAL_CAT)).toBeDefined();
            expect (folio.find( cat => cat.id === REQUEST_CAT)).toBeDefined();
            expect (folio.find( cat => cat.id === ARCHIVED_CAT)).toBeDefined();
        })
    })

    test ('getUserProjects expected fields and lengths', () => {
        expect.hasAssertions ()
        const ps = projectStore
        return ps.getUserProjects (testUserId).then (response => {
            expect (response).toBeDefined ()

            // Presence
            expect (response.projects).toBeDefined ()
            expect (response.folio).toBeDefined ()

            // Lengths
            expect(ps.totalProjects === response.projects.length)
            expect (response.folio.length).toBeGreaterThan (3)

            // expect predefined categories
            const folio = response.folio
            expect (folio.find( cat => cat.id === DEFAULT_CAT)).toBeDefined();
            expect (folio.find( cat => cat.id === APPROVAL_CAT)).toBeDefined();
            expect (folio.find( cat => cat.id === REQUEST_CAT)).toBeDefined();
            expect (folio.find( cat => cat.id === ARCHIVED_CAT)).toBeDefined();
        })
    })

})
