import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import {createCategory, deleteCategory} from "../../services/CategoryApiService";
import {APPROVAL_CAT, ARCHIVED_CAT, DEFAULT_CAT, REQUEST_CAT, UserRole} from "../../config/Constants";
import {randomStr} from "../../utils/solo/randomStr";
import projectStore from "../../store/projectStore";
import ProjectApiService from "../../services/ProjectApiService";
import {EventSource} from "../../config/EventConfig";

const testUserId = "1C9DD750-FEC0-93C8-8922-FB70A6B5E640" // Not a Lion
const friendId = "89E864CE-2428-BF6D-0F70-1D8A848F4FB1" // Thomas Hodson
const projectId = "168B6723-A4E0-3F47-A2F9-0FC891C3438F" // Andy Warhol


describe('Project API - Integration', () => {
    test ('add user to project', () => {
        expect.hasAssertions ()
        const pa = ProjectApiService
        return pa.addUsersToProject (projectId, testUserId, [friendId], EventSource.Test, UserRole.INVITED)
            .then (response => {
                expect (response).toBeDefined ()
                expect (response.data.success).toBe (true)
            })
    })


    test ('Make a user inactive in project (e.g. "leave")', () => {
        expect.hasAssertions ()
        const pa = ProjectApiService
        return pa.removeUserFromProject (projectId, testUserId, friendId, false)
            .then (response => {
                expect (response).toBeDefined ()
                expect(response.data.user_role).toBe(UserRole.INACTIVE_USER)
                expect(response.data.user_id).toBe(friendId)
                expect(response.data.project_id).toBe(projectId)
            })
    })

    test ('Remove a user from project permanently', () => {
        expect.hasAssertions ()
        const pa = ProjectApiService
        return pa.removeUserFromProject (projectId, testUserId, friendId, true)
            .then (response => {
                expect (response).toBeDefined ()
                expect(response.data.success).toBe(true)
            })
    })

})