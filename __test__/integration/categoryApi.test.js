import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import {createCategory, deleteCategory, getUserCategories} from "../../services/CategoryApiService";
import {APPROVAL_CAT, ARCHIVED_CAT, DEFAULT_CAT, REQUEST_CAT} from "../../config/Constants";
import {randomStr} from "../../utils/solo/randomStr";

const testUserId = "1C9DD750-FEC0-93C8-8922-FB70A6B5E640" // Not a Lion
const minCats = 4; // 0 default, 1 ap, 2 reqs, 3 archive

let testCagId

describe('Category Api Service - Integration', () => {
    test ('getUserCategories', () => {
        expect.hasAssertions ()
        return getUserCategories (testUserId).then (response => {
            expect (response).toBeDefined ()
            expect (response.data).toBeDefined ()
            expect (response.data.user_id).toEqual (testUserId)
            expect (response.data.folio_structure).toBeDefined ()
            expect (response.data.folio_structure.length).toBeGreaterThan (3)

            // expect predefined categories
            const folio = response.data.folio_structure
            expect (folio[0].category_id).toBe (DEFAULT_CAT)
            expect (folio[1].category_id).toBe (APPROVAL_CAT)
            expect (folio[2].category_id).toBe (REQUEST_CAT)
            expect (folio[3].category_id).toBe (ARCHIVED_CAT)
        })

    })

    test('createCategories', () => {
        expect.hasAssertions()
        const title = 'test'+randomStr()
        return createCategory(title, testUserId).then( response => {
            expect(response).toBeDefined()
            expect(response.data).toBeDefined()
            expect(response.data.user_id).toEqual(testUserId)
            expect(response.data.title).toBe(title)
            expect(response.data.success).toBe(true)
            testCagId = response.data.category_id
        })

    })

    test('deleteCategory', () => {
        expect(testCagId).toBeTruthy()
        expect(testCagId.length).toBe(36)
        return deleteCategory(testCagId, testUserId).then( response => {
            expect(response).toBeDefined()
            expect(response.data).toBeDefined()
            expect(response.data.user_id).toEqual(testUserId)
            expect(response.data.category_id).toBe(testCagId)
            expect(response.data.success).toBe(true)
        })

    })


})
