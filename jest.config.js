module.exports = {
    verbose: true,
    transform: {
        "^.+\\.js$": "babel-jest"
    },
    globals: {
        "NODE_ENV": "test"
    },
    "setupFilesAfterEnv": ["./__test__/__setup.js"],
    "testEnvironment": "node",
    "roots": [
        "__test__/"
    ],
    moduleFileExtensions: [
        "js",
        "jsx"
    ],
    moduleDirectories: [
        "node_modules",
        "services",
        "store",
        "components"
    ]
}