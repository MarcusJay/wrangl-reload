import ColorUtils from '/utils/ColorUtils'

export const SourceTags = [
    'amazon',
    'pinterest',
    'dropbox',
    'google',
    'youtube'
]

export const DefaultTags = {
    "Favorite": '#f0592b',
    "Important": '#4999d8',
    "Draft": '#8fb7d5',
    "To Do": '#aa97d7',
    "Done": '#5459aa'
}

class TagConfig {
    constructor() {
        // ColorUtils.initSequence()
        // Object.keys(this.DefaultCardTags).forEach( tagName => this.DefaultCardTags[tagName] = ColorUtils.getNextColor() )
        // ColorUtils.initSequence()
        // Object.keys(this.DefaultProjectTags).forEach( tagName => this.DefaultProjectTags[tagName] = ColorUtils.getNextColor() )

        this.tagColorMap = {}
    }

    DefaultCardTags = {
        "Democrat": '#00b8ff',
        "Republican": '#bf0000',
        "Veteran": '#bb7913',
        "Lawyer": '#e0d600',
        "Leadership Now": '#68e01c'
    }

    assignColor(tag) {
        const color = ColorUtils.getNextColor()
        this.tagColorMap[tag] = color
    }

    getTagColor(tag) {
        if (!this.tagColorMap[tag]) {
            this.assignColor(tag)
        }
        return this.tagColorMap[tag]
    }
}

export default new TagConfig()