export const light = {
    bg: '#fff',
    fg: '#555'
}

export const dark = {
    bg: '#000',
    fg: '#bbb'
}

