// api reaction set names
export const STARS = 'Stars'
export const EMOTIONS = 'Emotions'
export const THUMBS = 'Thumbs Up or Thumbs Down'

export const THUMBS_UP = "Thumbs Up"
export const THUMBS_DOWN = "Thumbs Down"


export const ReactionProperties = {
    [THUMBS_UP]: {
        img: '/static/svg/thumbsUp.svg'
    },
    [THUMBS_DOWN]: {
        img: '/static/svg/thumbsDown.svg'
    }
}

export const Types = {
    MutuallyExclusive: 0,
    Rating: 1,
    MultiSelect: 2
}


// Deprecated
/*
 DefaultReactions = {
 "Thumbs Up": {
 iconName: "ThumbsUp"
 },
 "Thumbs Down": {
 iconName: "ThumbsDown"
 }
 // TODO more as needed
 }
 */

