// Populate new users with starting content

export const StartingContacts = [
    'C5155828-71C6-2D3A-FB85-F717F42AC0AD' // Michelle
]


export const StartupProjects = [
    '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD',
    '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD',
    'AF40E193-F5FC-1D45-55F3-16C3DA320B03',
    '0EE7A7CA-6A58-E088-C05A-BEF7E8720270',
    '58B133EF-AFB4-15C2-1CCA-EFDFB22FA8AE',
    '6BFDC014-0E71-4B9B-5873-774781D143FC',
    '10C5417C-08B9-D955-5509-7ADED67A9270',
    'C2591271-CAA2-9B5F-A87A-1700CE699ABF'
]

// TODO when stablized, move to utils or db

// professions
export const Freelancer = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Designer = 'AF40E193-F5FC-1D45-55F3-16C3DA320B03'
export const Planner = '0EE7A7CA-6A58-E088-C05A-BEF7E8720270'
export const Shopper = '58B133EF-AFB4-15C2-1CCA-EFDFB22FA8AE'
export const Artist = '6BFDC014-0E71-4B9B-5873-774781D143FC'
export const Advertiser = '10C5417C-08B9-D955-5509-7ADED67A9270'
export const Consultant = 'C2591271-CAA2-9B5F-A87A-1700CE699ABF'
export const Assistant = '86C449E5-62F3-E4E1-EC95-DB4ADFFD1CEE'
export const Manager = '62B8CEA6-8C50-446C-CA70-3274035DEECA'
export const Executive = ''
export const Professions = [Freelancer, Designer, Planner, Shopper, Artist, Advertiser, Consultant, Assistant, Manager]

// interests
export const Startups = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Design = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Photography = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Film = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Architecture = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Interior = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Science = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Fashion = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Art = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Music = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Gaming = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const GreenThumb = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Lifestyle = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Health = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Sports = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Outdoors = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
export const Automotive = '6B1EA7AB-A7E6-C6D0-25F7-114FAA761EFD'
// export const Interests = [Startups, Design, Photography, Film, Architecture, Interior, Science, Fashion, Art, Music, Gaming,
//     GreenThumb, Lifestyle, Health, Sports, Outdoors, Automotive]
export const Interests = []


// cards
export const StartingCards = [

]

// Starting Tags - see TagConfig.DefaultTags

// help/tours
//export const
