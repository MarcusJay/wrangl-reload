import {Component} from 'react'
import {ThumbsDown, ThumbsUp} from 'react-feather'
import {THUMBS_DOWN, THUMBS_UP} from "./ReactionConfig";
import userStore from "/store/userStore";
import projectStore from "/store/projectStore"
import ReactionApiService from "/services/ReactionApiService";
import {truncate} from "../utils/solo/truncate";
import {ProjectStatus, ProjectType} from "./Constants";
import uiStateStore from "../store/uiStateStore";
import {voteTypeIdToAPName} from "../services/ReactionApiService";

const MAX_EVENT_LEN = 50

export const MARK_AS_READ_TIMEOUT = 5 * 1000

export const EventSource = {
    None: 0,
    EmailInvite: 1,
    ProjectAdd: 2,
    WebSearch: 3,
    ImageFromDisk: 4,
    Dropbox: 5,
    GoogleDocs: 6,
    ICloud: 7,
    Pinterest: 8,
    InApp: 9,
    DirectLink: 10,
    ScrapeUrl: 11,
    ScrapeResults: 12,
    Wrangle: 14,
    OnBoarding: 15,
    Approval: 16
}


/*
 Event Levels
 */
export const EventHigh      = 3
export const EventMedium    = 2

export const DeleteUser     = 32

/*
 Event Types (High)
 */
export const UserIntegrationCreated = 2
export const UpdateReactionInCard = 5
// Project
export const AddCommentToProject = 11
export const AddCardToProject = 18
// Card
export const AddCommentToCard = 20
export const AddReactionToCard = 22
export const DeleteReactionToCard = 23
export const AddTagToCard = 24
export const DeleteTagFromCard = 25
export const CreateCard = 15
export const CreateCards = 33
export const CardMove = 28

export const CardMarkupAdd = 39
export const CardMarkupDelete = 40

export const SendDirectMessage = 13
export const DirectMessageEdited = 38

/*
 Event Types (Medium)
 */
export const UserInvite     = 4
export const ProjectView    = 6
export const ProjectCreate  = 8
export const ApprovalProjectCreate  = 34
export const ProjectUpdate  = 9
export const ProjectDelete  = 10
export const ProjectTakeBreak   = 14
export const ProjectLeaveForever = 31
export const DeleteCommentFromProject  = 12

export const CardUpdate     = 16
export const CardDelete     = 17
export const DeleteCommentFromCard  = 21

export const FolioChanged  = 43

/*
  Additional ignored types
 */
const AddServiceIntegration = 2
const DeleteServiceIntegration = 3

/*
 Client only types
 */

export const PreviewGenerate = 101

export const IgnoredEvents = [CardDelete, /*UserInvite, */ DeleteTagFromCard, ProjectTakeBreak, AddTagToCard, DeleteCommentFromProject, DeleteCommentFromCard, CardMove, DeleteReactionToCard, UserIntegrationCreated, AddServiceIntegration, DeleteServiceIntegration]
export const ToastyEvents = [ProjectView, ProjectCreate, ProjectUpdate, ApprovalProjectCreate, AddCommentToCard, AddCommentToProject, /*SendDirectMessage, */AddReactionToCard, AddCardToProject, CreateCard, CreateCards]
export const CardChangeEvents = [AddCommentToCard, AddReactionToCard, DeleteCommentFromCard, DeleteReactionToCard, CardUpdate, AddTagToCard]
export const CardSetEvents = [AddCardToProject, CreateCard, CreateCards, CardDelete]
export const ProjectChangeEvents = [AddCommentToProject, ProjectUpdate, DeleteCommentFromProject]
export const BadgeWorthyEvents = [ApprovalProjectCreate, SendDirectMessage, AddCommentToCard, AddCommentToProject, AddReactionToCard, AddCardToProject, CreateCard]
/*
  Event Sections for Activity Dashboard
 */
// tbd



export const isToasty = (event) => {
    return !!event && (ToastyEvents.indexOf(event.type) !== -1)
}

// or move to a new EventUtils
export const getIcon = (event, count) => {
    if (!event.reactionTypeId || count > 1)
        return null

    const reaction = ReactionApiService.getReactionByTypeId (event.reactionTypeId)
    if (!reaction) {
        debugger
        return null
    }

    switch (reaction.reaction_name) {
        case THUMBS_UP: return <ThumbsUp size={24} className='eventReactionIcon'/>
        case THUMBS_DOWN: return <ThumbsDown size={24} className='eventReactionIcon'/>
    }
    return null
}

export const getMessage = (event, count, includeProjectInfo) => {

    if (!event.user && event.userId)
        event.user = userStore.getUserFromConnections(event.userId)
    const userName = '[user]' // event.displayName

    if (event.detail && event.detail.indexOf('card_') === 0)
        return null

    // TODO We're going to need to make api calls to retrieve info, or else add additional detail fields.
    // e.g. user added note 'blah' to card 'bleh'. Detail contains blah, not bleh.
    const cardTitle = event.cardId? truncate(event.detail, MAX_EVENT_LEN) : null
    const projectTitle = event.projectTitle? event.projectTitle : (event.project? event.project.title : null)
    const commentText = event.commentId && event.detail? event.detail : null
    const isApproval = event.projectType === ProjectType.APPROVAL // EVENT?
    const isStatusChange = isApproval && event.updatedKey === 'project_status'
    const isAPBroadcast = isStatusChange && event.detail === ProjectStatus.BROADCAST

    if (projectTitle === null) {
        debugger
        return null
    }

    let msg = null

    switch( event.type ) {
        case ApprovalProjectCreate:
            msg = userName +' sent an Approval Request: "' +projectTitle+ '"'
            break

        case CardDelete:
            if (event.detail)
                msg = userName + ' deleted card "' +event.detail+ '"' +(includeProjectInfo? ' from "' +projectTitle+'"' : '')
            break

        case CreateCard:
        case AddCardToProject:
            if (count > 1)
                msg = userName + ' added ' +count+ ' cards' +(includeProjectInfo? ' to "' +projectTitle+'"' : '')
            else if (cardTitle)
                msg = userName + ' added "' +cardTitle+ '"' +(includeProjectInfo? ' to "' +projectTitle+'"' : '')
            break

        case CreateCards:
            msg = userName + ' added ' +event.detail+ ' card' +(event.detail === '1'?'':'s')
            break

        case AddCommentToProject:
            if (count > 1)
                msg = userName + ' added ' + count + ' comments' + (includeProjectInfo ? ' to "' + projectTitle + '"' : '')
            else if (commentText !== null)
                msg = userName + ' "' +commentText+ '"' +(includeProjectInfo && projectTitle? ' in "' +projectTitle+ '"' : '')
            else
                msg = userName + ' posted ' +(includeProjectInfo && projectTitle? ' in "' +projectTitle+ '"' : '')
            break

        case AddCommentToCard:
            if (count > 1)
                msg = userName + ' added ' +count+ ' notes to "' +cardTitle+ '"' +(includeProjectInfo && projectTitle? ' in "' +projectTitle+ '"' : '')
            else if (commentText)
                msg = userName + ' added note "' +commentText+ '" to card "' +cardTitle+ '"' +(includeProjectInfo && projectTitle? ' in "' +projectTitle+ '"' : '')
            // else
            //     debugger
            break

        case AddReactionToCard:
            if (!event.reaction) {
                console.warn('AddReaction event, but no reaction. event.reactionTypeId: ' +event.reactionTypeId)
            }
            const reaction = ReactionApiService.getReactionByTypeId(event.detail)
            if (count > 1)
                msg = userName + ' vote on cards ' +count+ ' times' +(includeProjectInfo && projectTitle? ' in "' +projectTitle+ '"' : '')
            else if (reaction && cardTitle)
                msg = userName + ' ' +voteTypeIdToAPName(event.detail)+ ' ' +cardTitle +(includeProjectInfo && projectTitle? ' in "' +projectTitle+ '"' : '')
            else {
                msg = userName + ' reacted to an item. Missing info. (event.detail: ' +event.detail+')'
            }
            break

        case ProjectView: // hidden
            if (count > 1)
                msg = 'This board has been viewed ' +count+ ' time' +(count === 1? '':'s')
            else
                msg = userName + ' is viewing "' +projectTitle+ '"'
            break

        case ProjectUpdate:
            if (isStatusChange)
                msg = 'Request ' +(event.detail === '1'? 'completed: "' : '(re)opened: "') +projectTitle+ '"'
            else if (isAPBroadcast)
                msg = "You have a new approval request"

/*
                if (includeProjectInfo)
                msg = userName + ' updated board "' +projectTitle+ '"'
            else
                msg = userName + ' updated the board'
*/

            break

        case ProjectDelete:
            msg = userName + ' deleted "' +projectTitle+ '"'
            break

        case SendDirectMessage:
            msg = userName + ': "' +commentText+ '"'
            break
/*
        case ProjectTakeBreak:
            msg = userName + ' is taking a break from "' +projectTitle+ '"'
            break
*/
        case ProjectLeaveForever:
            if (includeProjectInfo)
                msg = userName + ' left "' +projectTitle+ '"'
            else
                msg = userName + ' left'
            break

        case CardMove:
            if (count > 1)
                msg = userName + ' moved ' +count+ ' cards' +(includeProjectInfo && projectTitle? ' in "' +projectTitle+ '"' : '')
            else if (cardTitle)
                msg = userName + ' moved "' +cardTitle+ '"' +(includeProjectInfo && projectTitle? ' in "' +projectTitle+ '"' : '')
            break

        case CardUpdate:
            if (count > 1)
                msg = userName + ' updated ' +count+ ' cards' +(includeProjectInfo && projectTitle? ' in "' +projectTitle+ '"' : '')
            else if (cardTitle)
                msg = userName + ' updated card "' +cardTitle+ '"' +(includeProjectInfo && projectTitle? ' in "' +projectTitle+ '"' : '')
            break
        default:
            msg = 'Type '+event.type+ ', updatedKey: ' +event.updatedKey+ ', detail: ' +event.detail
    }

    return msg
}

// Just state the gist without the details. Useful for papertrail and fees in which each event row is sent the actual objects in qwuestion
export const getMiniMsg = (event, count) => {
  if (!event.user && event.userId)
    event.user = userStore.getUserFromConnections (event.userId)

  if (event.detail && event.detail.indexOf ('card_') === 0)
    return null

  let msg = null
  const commentText = event.commentId ? event.detail : 'No Comment'
  const countFromDetail = Number(event.detail)

  switch (event.type) {
    case ApprovalProjectCreate:
      return 'created approval request'

    case CardMove:
      if (count > 1)
          return 'moved ' +count+ ' cards'
      else if (event.detail)
          return 'moved card to square ' +event.detail+ '. '
      break
    case UserInvite:
        debugger
      return 'invited'

    case CardDelete:
      return 'deleted ' +(event.detail? event.detail : 'a card')

    case CreateCard:
    case AddCardToProject:
      if (count > 1)
        return 'added ' + count + ' cards'
      else // 1
        return 'added card ' +event.detail

    case CreateCards:
      if (countFromDetail > 1)
        return 'created ' +countFromDetail+ ' cards'
      else if (countFromDetail === 1) {
        return 'created ' + (event.card? event.card.title : 'a card')
      }

    case AddCommentToProject:
    case AddCommentToCard:
      if (count > 1)
        return count + ' new messages'
      else
        return 'commented "' +commentText+ '"'

    case AddReactionToCard:
      const reaction = ReactionApiService.getReactionByTypeId(event.detail)
      if (count > 1)
        return 'vote on cards ' + count + ' times'
      else if (reaction)
        return voteTypeIdToAPName(event.detail, reaction.reaction_value) || ('Reaction type id ' +event.detail)
      else
        return 'reacted to (missing info. event.detail: ' +event.detail+')'

    case ProjectView:
      return 'viewed'

    case ProjectUpdate:
      return 'updated the ' +event.detail

    case ProjectDelete:
      return 'deleted'

    case SendDirectMessage:
      return commentText

    case ProjectLeaveForever:
      return 'left'

    case CardUpdate:
      if (count > 1)
        return 'updated ' + count + ' cards'
      else
        return 'updated'

    default:
      return null // msg = 'Type '+event.type+ ', updatedKey: ' +event.updatedKey+ ', detail: ' +event.detail
  }
}

export const getCardUpdateMsg = (event, card, user) => {
    if (!event || !card || !user) {
        console.warn('getCardUpdateMsg missing params')
        return null
    }

    const field = event.updatedKey
    if (!field)
        return null

    const userName = user.displayName

    switch(field) {
        case 'card_title': return userName + ' changed the title to "' +event.detail+ '"'
        case 'card_description': return userName + ' changed the description to "' +event.detail+ '"'
        case 'card_url': return userName + ' changed the url to "' +event.detail+ '"'
        case 'card_image': return userName + ' changed the image to "' +event.detail+ '"'
        default: return null
    }
}

