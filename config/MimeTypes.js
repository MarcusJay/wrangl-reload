export const AcceptedDropTypes = [
    'text/plain',
    'text/html',
    'text/uri-list', // dragging a url from browser

    'image/gif',
    'image/jpeg',
    'image/png',
    'image/svg+xml',
    'image/webp',

    'application/pdf',
    'application/word',
    'application/vnd.ms-excel'

]

export const UrlTypes = [
    'text/uri-list'
]

export const TextTypes = [
    'text/plain',
    'text/html',
    'text/json'
]

export const ImageTypes = [
    'image/gif',
    'image/jpeg',
    'image/png',
    'image/svg+xml',
    'image/webp'
]

export const FileTypes = [
    'application/pdf',
    'application/word',
    'application/vnd.ms-excel'
]