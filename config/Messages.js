// Left off
// Add messages here, for inline hints, ala resource files.
// Just need a way to map key to message body without requiring components to impport entire map

// export const MsgPlToolbar = 'Create new Projects. View projects as: List, Grid, or Large Images, sort by Latest or by Title A-Z, and search for projects by title and description.'
export const MsgPdToolbar = ''
export const MsgProjects = `All projects that you are involved in appear below. You can sort them by most recent (the default) or by title, and they can be viewed as a list, grid, or large images. 

Create new projects with the plus button. Find existing ones using the Search box.`

export const MsgProject = `This is a single project. You can talk with other members in the left pane, and add, edit, move, view, and delete cards in this project on the right pane. Using the Cards toolbar, you can create new cards with the plus button, and find existing cards with the search box. View all card comments with the message icon, and enter the full screen gallery with the image icon. Click Options to adjust how cards appear to you. `

export const MsgLibrary = `This is your global library. It contains all your boards, contacts, cards and more. Drag cards and contacts from here into your boards. When in a board, drag tags onto cards. And if you connect your Dropbox, you can drag your dropbox files into board.`

export const MsgCards = ''
export const MsgContacts = ''
export const MsgComments = ''
