export const env = {
    beta: {
        host: "pogo.io",
        url: "https://pogo.io",
    },

    getProjectUrl(projectId) {
        return env.current.url + '/project/' + projectId
    },

    getAPUrl(projectId) {
        return env.current.url + '/approve/' + projectId
    }

}


env.current = env.beta