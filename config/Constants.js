export const DEFAULT_CAT =  '00000000-0000-0000-0000-000000000000'
export const APPROVAL_CAT = '11111111-1111-1111-1111-111111111111'
export const REQUEST_CAT =  '22222222-2222-2222-2222-222222222222'
export const ARCHIVED_CAT = '33333333-3333-3333-3333-333333333333'

const fiveHundredStrong = '500strong'

// DEMO AND VANITY PROJECT URLS
export const VanityUrls = {
    teampogo: 'EA569B89-90E8-B9C1-8FC2-2C426DD51633',
    michelle: '07AB0865-7556-D247-7599-9788980023FD',
    coffee: 'CB436930-D073-F386-4C60-7BEB395BC5A3',
    ycomb:  '2B0C3707-F105-9590-3AF4-0E92535F299D',
    [fiveHundredStrong]: '4FF5CBE4-0D3F-8B0E-4B9E-D0D30A2ECE49',
    investorupdates: '7AE6DEAB-B2A2-ED3A-DD1F-61FC0DD66ABE'
}


export const EMAIL_REGEX = /[a-z0-9\._%+!$&*=^|~#%'`?{}\-]+@([a-z0-9\-]+\.){1,}([a-z]{2,16})/

// Project security
export const PROJECT_PUBLIC = 0
export const PROJECT_PRIVATE = 1

// Notification Levels
export const NOTIF_NONE = 0
export const NOTIF_ALL = 1

// Notification Levels
export const REACTION_NONE = 0
export const REACTION_UPDOWN = 0
export const REACTION_STARS = 1

// Conversations
export const contactBG = '#a1a5b9'
export const contactBGDrop = '#47C1F1'
export const contactFG = '#D8D8D8'

export const myBG =  '#343C46'
export const myBGDrop = contactBGDrop
export const myFG = contactFG

export const APPROVE = 1
export const RATE = 2
export const REJECT = 3


// Key Codes
/*
export const SHIFT = 16
export const CTRL = 17
export const ALT = 18
export const ENTER = 13
export const ESCAPE = 27
export const TAB = 9
export const BACKSPACE = 8
export const UP = 38
export const DOWN = 40
export const LEFT = 37
export const RIGHT = 39
export const AT = 50
export const HASH = 51
*/

// Key DOMStrings
export const SHIFT = 'Shift'
export const CTRL = 'Control'
export const ALT = 'Alt'
export const ENTER = 'Enter'
export const ESCAPE = 'Escape'
export const TAB = 'Tab'
export const DELETE = 'Delete'
export const BACKSPACE = 'Backspace'
export const UP = 'ArrowUp'
export const DOWN = 'ArrowDown'
export const LEFT = 'ArrowLeft'
export const RIGHT = 'ArrowRight'
export const AT = '@'
export const HASH = '#'

export const ProjectType = {
    NORMAL: 0,
    APPROVAL: 1
}

export const ProjectStatus = {
    OPEN: 0,
    COMPLETED: 1,
    CANCELLED: 2,
    BROADCAST: -1
}

export const ApprovalCompletionType = {
    ALL_USERS_REQUIRED: 0,
    SINGLE_USER_COMPLETES: 1,
    MAJORITY_COMPLETES: 2
}

export const LeftPaneTab = {
    CONTACTS: 0,
    PROJECTS: 1,
    APPROVALS: 2
}

// User props
export const UserRole = {
    INVITED: 0,
    ACTIVE: 1,
    ADMIN: 2,
    INACTIVE_USER: 3,
    INACTIVE_ADMIN: 4,
    BANNED: 5, // (no one can invite him except admin)
    DELETED: 6, // (this will remove the user from the project join table)
    APPROVER: 7,
    REQUESTOR: 8
}
export const UserStatus = {
    ANON: 0,
    NORMAL: 1,
}

export const PaintTool = {
    BRUSH: 0,
    TEXT: 1
}
export const CmtType = {
    TEXT: 1,
    MARKUP: 2
}

export const Rotation = {
    Deg90: 1,
    Deg180: 2,
    Deg270: 3
}

export const MAX_FOLDER_NAME_LEN = 30

// Url types
export const IMAGE = "image"
export const SITE = "site"
export const DOCUMENT = "doc"
export const GOOGLE_DOC = "gdoc"

// Drag & Drop
export const DndItemTypes = {
    CARD: 'card',
    CARD_THUMB: 'card_thumb',
    FOLDER: 'folder', // Category
    STACK: 'stack',
    DROPBOX_THUMB: 'dropbox_thumb',
    PROJECT: 'project',
    PERSON: 'person',
    NON_CONTACT: 'noncontact',
    MEMBER_AVATAR: 'member_av',
    TAG: 'tag',
    ACTIVITY_TILE: 'tile'

}

// MAX LENGTHs of certain fields, as defined in DB
export const FieldLengths = {
    'title' : 512,
    'description' : 1024,
    'url' : 1024
}

// search config
export const SearchConfig = {
    QUERY_DELAY_MS: 500,
    MIN_SEARCH_CHARS: 2,
    MIN_URL_CHARS: 10
}

// tooltips
export const ToolTipConfig = {
    delayShow: '10',
    delayHide: '100',
    effect: 'solid'
}

// toasts
export const ToastConfig = {
    duration: 15000,
    notifDuration: 5000,

}

export const SecurityLevel = {
    Public: 0,
    MembersOnly: 1
}

// User Prefs
export const VIEW_LIST = 1 // 'list'
export const VIEW_GRID = 2 // 'grid'
export const VIEW_COMMENTS = 3 // 'comments'
export const VIEW_IMAGES = 4 // 'images'
export const VIEW_CARD = 5 // 'grid'
export const TAB_LIBRARY = 1
export const TAB_UPLOAD = 2
export const TAB_SEARCH = 3
export const TAB_DROPBOX = 4

export const viewNumToName = (viewNum) => {
    switch (viewNum) {
        case VIEW_GRID: return 'grid'
        case VIEW_LIST: return 'list'
        case VIEW_IMAGES: return 'images'
        case VIEW_CARD: return 'cards'
        case VIEW_COMMENTS: return 'comments'
    }
    return ''
}

export const tabNumToName = (tabNum) => {
    switch (tabNum) {
        case TAB_LIBRARY: return 'library'
        case TAB_DROPBOX: return 'dropbox'
        case TAB_SEARCH: return 'search'
        case TAB_UPLOAD: return 'upload'
    }
    return ''
}
