export const host = 'https://api.pogo.io'
export const baseUrl = 'https://api.pogo.io/wrangl-api/v2/'
export const attmBaseUrl = "https://api.pogo.io/wrangl-api/attachments/"
// export const searchUrl = 'https://search.wran.gl/'

export const ApiTempKeyName = "API_Access_Key"
export const ApiTempKeyValue = "BCE1674E-8E67-C1C9-BB15-D51BAFDB6FF2"

    // authentication
export const socialLogin = baseUrl + 'social_login.php'
export const userVerify = baseUrl + 'user_verify.php'

export const systemStatus = baseUrl + 'system_status_read.php'

    // users
export const userProfileCreate = baseUrl + 'user_profile_create.php'
export const userProfileRead = baseUrl + 'user_profile_read.php'        // returns single profile
export const userProfilePublicRead = baseUrl + 'user_read_public.php'   // accepts and returns collection of profiles
export const userVerifyEmail = baseUrl + 'user_verify_email.php'
export const userProfileUpdate = baseUrl + 'user_profile_update.php'
export const userPrefs = baseUrl + 'user_preferences.php'
export const userProjectSettings = baseUrl + 'user_project_settings_2.php'
export const addUsersToProject = baseUrl + 'project_add_users.php'

    // search!
export const userSearchStart = baseUrl + 'user_search.php'
export const userSearchSelect = baseUrl + 'user_search_select.php'
export const userSearchEnd = baseUrl + 'user_search_end_session.php'

export const userIntegrationCreate = baseUrl + 'service_integration_create.php'
export const userIntegrationsRead = baseUrl + 'service_integration_read.php'
export const userIntegrationsDelete = baseUrl + 'service_integration_delete.php'

export const userResetCodeCreate = baseUrl + 'email_reset_code_create.php'
export const userResetCodeRead = baseUrl + 'email_reset_code_read.php'

export const userAccountCodeCreate = baseUrl + 'account_link_code_create.php'
export const userAccountCodeRead = baseUrl + 'account_link_code_read.php'


    // #################### Old Tags ###
    // 1. User Tags (aka Bag of Tags, Top-Level Tags)
    // Read and Update
export const userTagsRead = baseUrl + 'user_tags_read.php'           // param: user_id.                  response field: tags
export const userTagsUpdate = baseUrl + 'user_tags_update.php'       // params: user_id, add_tags, delete_tags       response field: add_tags. only contains what you added.

    // 2. Project Tags (aka User Project Tags, these are User Tags that have been Assigned to Projects)
    // Read and Update
export const projectTagsRead = baseUrl + 'user_project_tags.php'     // params: user_id, project_id      response field: tags
export const projectTagsUpdate = baseUrl + 'user_project_tags.php'   // params: user_id, project_id, add_tags, delete_tags   response field: tags

    // 3. Cardable Tag Set (aka Project Tag Set, e.g. tags within project that can be applied to cards)
    // Read and Update
// export const cardableTagSetRead = baseUrl + 'project_read.php'       // params: project_id               response field: project_tag_set
    // but in user_projects, it's a | split string, not an array.
export const cardableTagSetUpdate = baseUrl + 'project_update.php'   // params: project_id, add_tags, remove_tags

    // ####################### New Tags (objects) ###
    // 0. Standalone (orphan) Tags
export const tagCreate = baseUrl + 'tag_create.php'                  // param: tag_creator, _text, _color    response: same
export const tagUpdate = baseUrl + 'tag_update.php'                  // param: tag_id, tag_creator, _text, _color    response: same

    // 1. User Library Tags (aka Bag of Tags, Top-Level Tags)
export const userLibTagsRead = baseUrl + 'user_library_tags_read.php'
export const userLibTagsDelete = baseUrl + 'user_library_tag_delete.php'

    // 2. Project Tags (aka User Project Tags, these are User Tags that have been Assigned to Projects)
export const projectTagAdd = baseUrl + 'user_project_tag_add.php'                // TAG A PROJECT
export const projectTagDelete = baseUrl + 'user_project_tag_delete.php'          // UNTAG A PROJECT

    // 3. Cardable Tag Set (aka Project Tag Set, e.g. tags within project that can be applied to cards)
export const cardableTagRead = baseUrl + 'project_tag_set_read.php'
export const cardableTagAdd = baseUrl + 'project_tag_set_add.php'
export const cardableTagDelete = baseUrl + 'project_tag_set_delete.php'

    // Tag Actions: Associations with Cards and Projects
    // 1. Cards
export const cardTagAdd = baseUrl + 'card_tag_add.php'                           // TAG A CARD
export const cardTagDelete = baseUrl + 'card_tag_delete.php'                     // UNTAG A CARD
export const cardTagsRead = baseUrl + 'card_tags_read.php'
export const cardSearchTags = baseUrl + 'card_search_tags.php'
    // ######################## END New Tags

    // Categories
export const userCategoriesRead = baseUrl + 'user_categories.php'
export const categoryCreate = baseUrl + 'category_create.php'
export const categoryUpdate = baseUrl + 'category_update.php'
export const categoryDelete = baseUrl + 'category_delete.php'
// export const categorizeProject = baseUrl + 'category_add_project.php'
// export const uncategorizeProject = baseUrl + 'category_remove_from_project.php'
export const moveCategoryUrl = baseUrl + 'category_move.php'
export const moveProjectCategoryUrl = baseUrl + 'category_move_project.php'

    // relationships
export const userConnect = baseUrl + 'user_connect.php'
export const userDisconnect = baseUrl + 'user_disconnect.php'
export const userConnections = baseUrl + 'user_connections.php'
export const userGroups = baseUrl + 'user_groups.php'

export const groupCreate = baseUrl + 'group_create.php'
export const groupRead = baseUrl + 'group_read.php'
export const groupUpdate = baseUrl + 'group_update.php'
export const groupDelete = baseUrl + 'group_delete.php'

    // projects
export const projectCreate = baseUrl + 'project_create.php'
export const projectCreateBatch = baseUrl + 'project_create_multiple.php'
export const projectRead = baseUrl + 'project_read_new.php'     // singular now, and deep
export const projectsByUser = baseUrl + 'user_projects_new.php'  // get all projects for current user, deep: true is default.
export const projectsInCommon = baseUrl + 'common_projects.php'  // get all projects that array of users hold in common
export const projectsByUserSorted = baseUrl + 'user_projects_sort_read.php'
export const projectsByUserUpdateSorting = baseUrl + 'user_projects_sort_update.php'
export const projectUpdate = baseUrl + 'project_update.php'
export const projectDelete = baseUrl + 'project_delete.php'
export const projectSearch = baseUrl + 'project_search.php'
export const projectSecurity = baseUrl + 'project_security.php'

    // cards
export const cardCreate = baseUrl + 'card_create.php'
export const cardsCreate = baseUrl + 'cards_create.php'
export const cardRead = baseUrl + 'card_read.php'     // accepts and returns multiple card ids
export const cardReadReactions = baseUrl + 'card_read_reactions.php'     // accepts and returns multiple card ids
export const cardSearch = baseUrl + 'card_search.php'
export const cardUpdate = baseUrl + 'card_update.php' // singular
export const cardMove = baseUrl + 'card_move.php' // singular
export const cardDelete = baseUrl + 'card_delete.php'
export const cardsDelete = baseUrl + 'cards_delete.php'
export const cardsByUser = baseUrl + 'user_cards.php'  // get all cards for current user, deep: true is default.
export const cardClone = baseUrl + 'card_clone.php'
export const cardsCloneIntoProject = baseUrl + 'cards_clone.php'

    // api card refactor Sep 18
// export const cardCreate2 = baseUrl + 'card_create_2.php'
// export const cardsCreate2 = baseUrl + 'cards_create_2.php'
// export const cardMove2 = baseUrl + 'card_move_2.php'
// export const cardDelete2 = baseUrl + 'card_delete_2.php'
// export const cardClone2 = baseUrl + 'card_clone_2.php'
// export const cardsCloneIntoProject2 = baseUrl + 'cards_clone_2.php'
export const cardOrderRead = baseUrl + 'card_order.php'
export const approvalProjectAddCards = baseUrl + 'approval_project_add_cards.php'

export const commentCreate = baseUrl + 'comment_create.php'
export const commentCreate2 = baseUrl + 'comment_create_2.php'
export const commentsRead = baseUrl + 'comment_read_2.php'
export const projectCommentsRead2 = baseUrl + 'project_comments_read_2.php'
export const conversationRead = baseUrl + 'conversation_read.php'     // accepts 2 user ids, returns array of comment objs
export const commentUpdate = baseUrl + 'comment_update_2.php' // singular
export const commentsDelete = baseUrl + 'comment_delete.php' // plural, yes. takes an array.

    // Image/File uploads
export const imageUpload = baseUrl + 'image_upload.php'
export const imageUploadToCard = baseUrl + 'image_upload_to_card.php'

export const imageUpload2 = baseUrl + 'image_upload.php'   // 36 x data
export const imageUploadToCard2 = baseUrl + 'image_upload_to_card.php' // 36 x 36 x data

export const imageUploadBase64 = baseUrl + 'image_upload_base64.php'   // 36 x data
export const imageUploadToCardBase64 = baseUrl + 'image_upload_to_card_base64.php' // 36 x 36 x data

export const imageUrlUpload = baseUrl + 'image_url_upload.php'   // params: key, url: image url
export const imageUrlUploadToCard = baseUrl + 'image_url_upload_to_card.php'   // params: key, url: image url

// Annotations / Markup
export const annotationCreate = baseUrl + 'annotation_create.php' // generate image first using ImageFileService, then send here as param
export const annotationRead = baseUrl + 'annotation_read.php'
export const annotationDelete = baseUrl + 'annotation_delete.php'


    // reaction sets
export const reactionSetReadDefaults = baseUrl + 'reaction_set_read_defaults.php'
export const reactonSetRead = baseUrl + 'reaction_set_read.php'
export const reactonSetCreate = baseUrl + 'reaction_set_read.php'

    // reactions
export const reactionCreate = baseUrl + 'reaction_create.php'
export const reactionDelete = baseUrl + 'reaction_delete.php'

export const commentReactionCreate = baseUrl + 'comment_reaction_create.php'
export const commentReactionDelete = baseUrl + 'comment_reaction_delete.php'


    // events
export const userBadges = baseUrl + 'user_badges.php'  // get all badges (new, in prog)
export const userBadgesDismiss = baseUrl + 'user_badges_dismiss.php'  // get all badges (new, in prog)
export const eventsUnseen = baseUrl + 'user_events_unseen.php'  // get all events since last check in
export const eventsUnseen2 = baseUrl + 'user_events_unseen_new.php'
export const eventHistory = baseUrl + 'project_event_history.php'
export const eventsDismiss = baseUrl + 'user_events_dismiss.php'

    // attachments
export const attachmentCreateLink = baseUrl + 'attachment_link.php'
export const attachmentUploadFile = baseUrl + 'attachment_upload.php'
export const attachmentDelete = baseUrl + 'attachment_delete.php'

