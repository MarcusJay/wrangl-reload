/**
 * File, attachment, icon types and support
 */

export const EmbeddableDocs = ['pdf', 'txt', 'js', 'java', 'py', 'php', 'cs']
export const EmbeddableAudio = ['mp3','wav','aif']
export const EmbeddableVideo = ['mov','avi','mp4','flv']
export const EmbeddableTypes = [...EmbeddableAudio, ...EmbeddableVideo, ...EmbeddableDocs]
export const ImageTypes = ['jpg','png','jpeg','gif', 'bmp', 'webp']

export const isImage = (ext) => {
    return ext? ImageTypes.indexOf(ext.toLowerCase()) !== -1 : false
}

export const isEmbeddable = (ext) => {
    return ext? EmbeddableTypes.indexOf(ext.toLowerCase()) !== -1 : false
}


// TODO rename this as it's misleading. Better name: hasNoIcon, useGenericIcon, etc.
export const isPreviewable = (ext) => {
    return !ext || extToIcon(ext) === 'file alternate outline' // ie. it's none of the below doc types
}

export const extToIcon = (ext) => {
    if (!ext)
        return 'file alternate outline'

    switch (ext.toLowerCase()) {
        case 'pdf':
            return 'file pdf outline'
        case 'doc':
        case 'docx':
        case 'docm':
            return 'file word outline'
        case 'txt':
        case 'pages':
        case 'rtf':
            return 'file text outline'
        case 'xls':
        case 'xlsx':
        case 'csv':
            return 'file excel outline'
        case 'ppt':
        case 'pptx':
        case 'pptm':
            return 'file powerpoint outline'
        case 'zip':
            return 'file archive outline'
        case 'mp3':
        case 'wav':
        case 'aif':
            return 'file audio outline'
        case 'mov':
        case 'avi':
        case 'mp4':
        case 'flv':
            return 'file video outline'
        case 'js':
        case 'java':
        case 'py':
        case 'php':
        case 'cs':
            return 'file code outline'
        default:
            return 'file alternate outline'
    }
}

export const sourceToFA = (source) => {
        switch(source) {
            case 'youtube':
                return '/static/img/3rdparty/youtube.png'
            case 'google':
                return '/static/img/3rdparty/google.png'
            case 'linkedin':
                return '/static/img/3rdparty/linkedin.png'
            case 'facebook':
                return '/static/img/3rdparty/facebook.png'
            case 'vimeo':
                return '/static/img/3rdparty/vimeo.png'
            case 'docs.google':
                return '/static/img/3rdparty/googledocs.png'
            case 'pinterest':
                return '/static/img/3rdparty/pinterest.png'
            default:
                return null
        }
    }

export const sourceToIcon = (source) => {
        switch(source) {
            case 'youtube':
                return '/static/img/3rdparty/youtube.png'
            case 'google':
                return '/static/img/3rdparty/google.png'
            case 'linkedin':
                return '/static/img/3rdparty/linkedin.png'
            case 'facebook':
                return '/static/img/3rdparty/facebook.png'
            case 'vimeo':
                return '/static/img/3rdparty/vimeo.png'
            case 'docs.google':
            case 'docs.google.com':
            case 'Google Docs':
                return '/static/img/3rdparty/googleDoc.png'
            case 'pinterest':
                return '/static/img/3rdparty/pinterest.png'
            default:
                return null
        }
    }

