
const routes = require('next-routes')();

//
// Because of awesome Next.js, You don't need to add routes for all pages.
// Every file on Pages folder basically has route as they named.
// (index.js => /, about.js => /about, ...etc.)
//
// Please add your route between of comments
//
// Except that you DO have to add routes here, because we are using Friday's dynamic routing, instead of Next routing.
//
// ------------ ROUTES ---------------
// @RANStartRoutes

routes
    .add('projectsPage', '/', 'projectsPage')
    .add('project', '/project/:id', 'project')
    .add('approve', '/approve/:id', 'approve')
    .add('login', '/login/:redirect', 'login')
    .add('signup', '/signup/:redirect', 'signup')
    .add('reset', '/reset/:code', 'reset')
    .add('profile', '/profile/:id', 'profile')
    .add('dropboxAuth', '/dropboxAuth', 'dropboxAuth')
    .add('code', '/code', 'code')
    .add('cust', '/cust', 'cust')
    .add('new', '/new', 'projectSettingsWizard')

// @RANEndRoutes
// ------------ ROUTES ---------------
//
//

module.exports = routes;
