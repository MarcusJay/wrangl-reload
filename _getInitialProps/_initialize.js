import React, { Component, PropTypes } from 'react';
import Router from 'next/router';
import { Provider } from 'mobx-react';
import { BaseLayout } from '../_layouts'

// css
import stylesheet from '~/styles/app.scss'
// or, if you work with plain css
// import stylesheet from 'styles/index.css'
import dynamic from 'next/dynamic'
const CSSIMPORT = dynamic(import('../utils/cssimport'))

// add Stores
import { initRootStore } from '../store';
import { apiStore} from '../store'
import projectStore from "/store/projectStore";

// import stylesheet from '~/styles/app.scss'

export default function Initialize(UI) {
    return class PageComponent extends Component {

        // ssr async calls
        static async getInitialProps({req, query, pathname}) {

            // url queries are handy
                // console.log('>> params are: ', query)
                // console.log('>> pathname is: ', pathname)

            // call apis

            // req is returned by next js
            const isServer = !!req;
            let initProps = {
                'isServer' : isServer
            }

            // pass to constructor
            return initProps
        }

        constructor(props) {
            super(props);

            // load our stores with initProps
            initRootStore.setData('isServer', props.isServer)

            // prep the stores
            this.store = {
                '_root' : initRootStore,
                apiStore: apiStore
            }

        }

        render() {
            return (
                <Provider appStore={this.store} style={{height: '100%'}}>
                    <BaseLayout {...this.props} style={{height: '100%'}}>
                        <CSSIMPORT style={stylesheet} />
                        <div style={{height: '100%'}}>
                            <style jsx global>{`body { margin: 0; padding: 0; }`}</style>
                            <UI router={Router.router} />
                        </div>
                    </BaseLayout>
                </Provider>
            )
}
    }
}