import PropTypes from 'prop-types'
import Head from 'next/head'
import {Component} from 'react'

// TODO
// - [ ] pass in style as parameter
// - [ ] pass in title as paremeter
// - [ ] pass in og meta tags as parameter

import {
  Button,
  Divider,
  Dropdown,
  Container,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Segment,
  Statistic,
  Sidebar
} from 'semantic-ui-react'


class BaseLayout extends Component {

  constructor (props) {
      super(props)
  }

    render() {

    return(
        <div className="site" style={{height: '100%'}}>
            <Head>
              <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" />
              <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.12/semantic.min.css" />
              { process.env.NODE_ENV === 'production' &&
                <link href="/static/css/app.css" rel="stylesheet" />
              }

              {/* Google Sign In */}
              <meta name="google-signin-client_id" 
                content="862445081010-fonnl3na30sanh6i2hodkn2l5duc67h6.apps.googleusercontent.com" />
              <script src="https://apis.google.com/js/platform.js" async defer></script>

            </Head>

            <div className="base_layout" style={{height: '100%'}}>
                {this.props.children}
            </div>
        </div>
        )
    }


}
export default BaseLayout
