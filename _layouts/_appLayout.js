import NextHead from 'next/head'
import {Component} from 'react'
import {observer} from 'mobx-react'
import ReactTooltip from 'react-tooltip'
import Router from 'next/router';
import ReactGA from 'react-ga';
import Sound from 'react-sound';

// css
import stylesheet from '~/styles/app.scss'
import dynamic from 'next/dynamic'
// dnd
import {DragDropContext} from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'

import {ToastContainer} from 'react-toastify'
import {ToastConfig} from 'config/Constants';
import UserTour from '/components/help/UserTour'
import uiStateStore from '/store/uiStateStore'
import WebPushService from "../services/WebPushService";
import {isBrowser} from "../utils/solo/isBrowser";
import RU from "../utils/ResponsiveUtils";
import projectStore from "../store/projectStore";
import localforage from "localforage";

const CSSIMPORT = dynamic(import('../utils/cssimport'))

// import { Slide, Zoom, Flip, Bounce } from 'react-toastify'

const gaTrackingId = 'UA-113577433-1'
let gaInitialized = false
let pageName

/*
      See https://github.com/zeit/next.js/issues/160
      and https://gist.github.com/trezy/e26cb7feb2349f585d2daf449411d0a4

    This ensures that the first page view gets sent to GA. All subsequent
    page views will be handled by the `Router.onRouteChangeComplete`
    method we set up above.

*/

Router.onRouteChangeComplete = () => {
    if (!gaInitialized)
        ReactGA.initialize(gaTrackingId)
    pageName = window.location.pathname + window.location.search
    if (isBrowser())
        console.warn('~~~~~~ Route Change Complete. Page: ' +window.location.pathname)
    else
        console.warn('~~~~~~ SSR Route Change Complete. Page? ' )

    ReactGA.set({page: pageName})
    ReactGA.pageview(pageName)

    // Hubspot
    const _hsq = window._hsq = window._hsq || [];
    _hsq.push(['setPath', pageName]);
    _hsq.push(['trackPageView']);

    projectStore.isFiltering = false
}


// ** NOTE appLayout takes a (React Component Class)
@DragDropContext(HTML5Backend)
@observer
export default function appLayout(Page) {
    return class PageComponent extends Component {

        static getInitialProps (context) {
            const { req, res, query } = context
            const ua = req? req.headers['user-agent'] : null
            const device = RU.getDeviceType(ua)
            const deviceProps = RU.getDeviceProps(ua)
            return Page.getInitialProps(context)
        }

        constructor(props) {
            super(props)
            this.deviceProps = props.deviceProps
        }

        handleSongLoading = () => {

        }

        handleSongPlaying = () => {

        }

        handleSongFinishedPlaying = () => {
            uiStateStore.soundPlayStatus = Sound.status.STOPPED
        }

        componentDidMount() {
            // hmmm. Necessary? Replace with findDOMNode? causing querySelector issue? Get rid of it? Now? Soon? Like now?
            let root = document? document.querySelector ('body > div') : null
            if (root) {
                root.style.height = '100%'
                root.children[0].style.height = '100%'
            }

            localforage.config({
                driver: [localforage.INDEXEDDB, localforage.WEBSQL, localforage.LOCALSTORAGE],
                name: 'pogoDB',
                storeName: 'pogoStore',
                version: 3
            })


            if (!gaInitialized)
                ReactGA.initialize (gaTrackingId)
            const pageName = window.location.pathname + window.location.search

            if (isBrowser ())
                console.warn ('~~~~~~ Layout did mount. Page: ' + window.location.pathname)
            else
                console.warn ('~~~~~~ SSR Layout did mount. Page? ')

            ReactGA.set ({page: pageName})
            ReactGA.pageview (pageName)


            // Cleanup old Service Workers.
            // TODO Once stable release is out, comment out unregistration below.
            if (typeof navigator !== 'undefined' && typeof navigator.serviceWorker !== 'undefined') {
                navigator.serviceWorker.getRegistrations ().then (function (registrations) {
                    for (let registration of registrations) {
                        registration.unregister ()
                    }
                })
            }

            // Register latest service worker.
            WebPushService.subscribeUserToPush () // offline web push

            // Cleanup pusher connection
            window.onunload = window.onbeforeunload = () => {
                if (uiStateStore.pusherConnection) {
                    console.warn ("Unload: Pusher disconnecting.")
                    uiStateStore.pusherConnection.disconnect ()
                } else {
                    console.warn ("Unload: no pusher reference to disconnect.")

                }
                // PusherService.disconnect()
            }

            window.onfocus = () => {
                uiStateStore.setFaviconAlert(false)
                uiStateStore.enableFavAlert = false
            }

            window.onblur = () => {
                uiStateStore.enableFavAlert = true
            }

        }

        render() {
            // TODO
            // see https://github.com/zeit/next.js/issues/4061
            // see https://github.com/zeit/next.js/issues/4044
            let title = "Pogo.io" // Until we determine why it's always 1 step behind history.

            const evCount = 0 // eventStore.getInstance().getNewEventCount()
            // if (evCount > 0) {
                // title += ' (' + evCount + ')'
            // }
            const faviconPath = '/static/favicon' +(evCount > 0? '/alert':'')
            const isMobile = this.deviceProps && this.deviceProps.device !== 'computer'

            const mobxTrig = uiStateStore.counter + uiStateStore.rand
            const uiss = uiStateStore
            let floatpane, propdesc

            return (
              <div className="site" style={{height: '100%'}} >
                  <NextHead>
                      <title>{title}</title>
                      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113577433-1" />
                      {/*<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet" />*/}
                      <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,600" rel="stylesheet"/>

                      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css" />
                      <link rel="stylesheet" href="https://unpkg.com/emoji-mart@2.11.1/css/emoji-mart.css" />
                      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/react-datepicker/2.7.0/react-datepicker.min.css" />
                      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/react-datepicker/2.7.0/react-datepicker-cssmodules.min.css" />

                      {/* Google Sign In */}
                    {/*<meta name="google-signin-client_id"*/}
                      {/*content="862445081010-fonnl3na30sanh6i2hodkn2l5duc67h6.apps.googleusercontent.com" />*/}
                    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                    <meta name="google-site-verification" content="r-Eg-hG80yLR7_ZtRILZRUA3Be6q2DXAefbtzKBk79I" />
                    {/*<script src="https://apis.google.com/js/platform.js" async defer></script>*/}
                    {/*Twitter intents  */}
                    {/*<script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>*/}

                    {/*Hubspot*/}
                      <script>
                          var _hsq = window._hsq = window._hsq || [];
                          _hsq.push(['setPath', '/home']);
                      </script>
                    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5921871.js"></script>

                    { process.env.NODE_ENV === 'production' &&
                      <link href="/static/css/app.css" rel="stylesheet" />
                    }

                      <link rel="apple-touch-icon" sizes="180x180" href="/static/favicon/apple-touch-icon.png"/>
                      <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon/favicon-32x32.png"/>
                      <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon/favicon-16x16.png"/>
                      <link rel="manifest" href="/static/favicon/site.webmanifest"/>
                      <link rel="mask-icon" href="/static/favicon/safari-pinned-tab.svg" color="#5bbad5"/>
                      <meta name="msapplication-TileColor" content="#da532c"/>
                      <meta name="theme-color" content="#ffffff"/>
                      {/*Favicons*/}

                  </NextHead>
                  <div className="app_layout" style={{height: '100%'}}>
                      {/*<Favicon url={['/static/favicon/Wrangle-512.png']} />*/}
                      { process.env.NODE_ENV !== 'production' &&
                      <div>
                        <CSSIMPORT style={stylesheet} />
                        {/*<DevTools />*/}
                      </div>
                    }

                      <Page router={Router.router} {...this.props} />

                      <div className='dms'>

                          {Object.keys(uiStateStore.openDMs).map( (contactId,i) => {
                              floatpane = uiStateStore.openDMs[contactId].pane
                              return <div key={'dm'+i}>{floatpane}</div>
                          })}

                      </div>
                      <Sound
                          url="/static/audio/beep2.wav"
                          playStatus={uiStateStore.soundPlayStatus}
                          playFromPosition={0}
                          onLoading={this.handleSongLoading}
                          onPlaying={this.handleSongPlaying}
                          onFinishedPlaying={this.handleSongFinishedPlaying}
                      />
                      <ReactTooltip
                          style={{ cursor: 'pointer' }}
                          globalEventOff={ isMobile? 'click' : undefined }
                      />
                      <ToastContainer autoClose={false}
                                      hideProgressBar={true}
                                      draggablePercent={60}
                                      // transition='Zoom'
                                      className='toastOuter'
                                      toastClassName="toastMid"
                                      // progressClassName='progress'
                                      bodyClassName='toastBodyBox'
                                      closeOnClick
                                      pauseOnHover
                      />

                  </div>
              </div>
            )
        }
    }
}