import axios from 'axios'
import {getUrlComponents} from "../../utils/solo/getUrlComponents";
import normalizeUrl from 'normalize-url'
import {IMAGE, SITE} from "../../config/Constants";

const remoteScraper = "https://crawl.pogo.io/"
const localScraper = "http://localhost:8203/"

const remoteUtils = "https://utils.pogo.io/"
const localUtils = "http://localhost:8205/"

const currScraper = remoteScraper // remoteScraper // !!!!!!!!!!!!!!!!!!!!!!! remoteScraper // REMOTE!
const currUtils = remoteUtils // !!!!!!!!!!!!!!!!!!!!!!! // REMOTE!

const siteRE = /(htm|html|asp|aspx|js|jsx|txt|\/|rtf)$/i
const imgRE = /\.(gif|jpg|jpeg|tiff|png|webp)$/i

export default class WebImportService {

    static getUrlType(url) {
        debugger
        if (siteRE.test(url))
            return new Promise((resolve, reject) => { resolve({data: SITE}) })
        else if (imgRE.test(url))
            return new Promise((resolve, reject) => { resolve({data: IMAGE}) })
        else
            return axios({
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                    // 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                url: currUtils + 'urlType',
                data: {
                    "url": url
                }
            })
    }

    // meta tags, images and links, in 1 go to reduce calls
    static fetchEverythingFromUrl(url) {
        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: currScraper + 'combined', // TEMP. REVERT THIS BACK TO COMBINED
            data: {
                "url": url
            }
        })
    }

    static fetchExternalUrl(url) {
        // url = normalize(url)
        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: currScraper + 'url',
            data: {
                "url": url
            }
        })
    }

    static readMetaTags(url) {
        // url = normalize(url)
        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: currScraper + 'meta',
            data: {
                "url": url
            }
        })
    }

    static getNameFromMetaTags(tags) {
        if (!tags)
            return null
        let re = /title|name|og:title|og:name/
        const tag = tags.find ( tag => re.test(tag.key) )
        return tag? tag.value : null
    }

    static getDescFromMetaTags(tags) {
        if (!tags)
            return null
        let re = /description|og:description|desc|og:desc/
        const tag = tags.find ( tag => re.test(tag.key) )
        return tag? tag.value : null
    }

    static getImageFromMetaTags(tags) {
        if (!tags)
            return null
        const imgTag = tags.find ( tag => tag.key === 'og:image')
         if (!imgTag || !imgTag.value)
            return null

        let imgUrl = normalizeUrl (imgTag.value, {stripFragment: false, stripWWW: false})
        if (imgUrl && !imgUrl.startsWith('http') && !imgUrl.startsWith('//')) { // TODO replace with REGEX
            const urlTag = tags.find ( tag => tag.key === 'og:url')
            if (urlTag) {
                let url = normalizeUrl (urlTag.value, {stripFragment: false, stripWWW: false})
                const parts = getUrlComponents (url)
                if (parts.protocol && parts.host)
                    imgUrl = parts.protocol + '//' +parts.host + imgUrl
            }
        }

        return imgUrl
    }

    static getImageDimensionsFromMetaTags(tags) {
        if (!tags)
            return null
        const widthTag = tags.find ( tag => tag.key && tag.key.indexOf('width') !== -1 ) //  og:image:width')
        const heightTag = tags.find ( tag => tag.key && tag.key.indexOf('width') !== -1 ) // og:image:height
        if (widthTag || heightTag)
            return {width: widthTag? widthTag.value : null, height: heightTag? heightTag.value : null}
        else
            return null
    }

    static getIconFromMetaTags(tags) {
        if (!tags)
            return null
        const ogIconPair = tags.find ( tag => tag.key === 'faviconUrl')
        return ogIconPair? ogIconPair.value : null
    }

    static getHostFromMetaTags(tags) {
        if (!tags)
            return null
        let re = /site_name|host|site/
        const tag = tags.find ( tag => re.test(tag.key) )
        return tag? tag.value : null
    }

}