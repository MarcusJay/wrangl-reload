let NodeDecorator = {
    markImportableNodes: function(docElem) {
        docElem.querySelector('*:not(span,p)').filter(function() {
            if (this.currentStyle)
                return this.currentStyle['backgroundImage'] !== 'none';
            else if (window.getComputedStyle)
                return document.defaultView.getComputedStyle(this,null)
        }).addClass('hasBG');
    },

    // nothing below used?

    isWithinBoundingRect(x,y,node) {
        let rect = node.getBoundingClientRect()
        let result = (x < rect.right && x > rect.left && y < rect.bottom && y > rect.top)
        console.log("is within bounds: " +result+ ". x = " +x+ ", y = " +y+ ", nodeRect top|right|bottom|left: " +rect.top + ' ' +rect.right + ' ' +rect.bottom+ ' ' +rect.left )
        return result;
    },

    injectImportBoxes: function() {
        let body = $("body")
        let validNodes = PageAssetService.getImagesAndLinks().validNodes;
        let node, jqNode, madnysNode, nodeOffset, origBorder, importBox, boxId, src, parentNode, parentLink, title, i;

        for (i = 0; i < validNodes.length; i++) {
            node = validNodes[i]
            jqNode = $(node)

            // decorate images only for now
            if (node.name.toLowerCase() !== "img")
                continue;

            jqNode.addClass("wiNode");
            origBorder = jqNode.css("border")
            // console.log("###" +boxId);
        }

        importBox = $(
            "<div class='importBox' data-node-link='' id='" +boxId+ "'" +
            "   data-node-title=''" +
            "   data-node-src=''>" +
            "  <img class='importLogo' src='https://wran.gl/src/svg/site-logo-32.svg'/>" + // id='" +boxId+
            "</div>"
        )

        let selectedNode, selectedTitle, selectedHref, selectedSrc
        $(".wiNode").hover(function() {
            // console.log("--- hover over " +node);
            if ( $(this).hasClass('wiNode') ) {
                $(".importBox").remove()
                selectedNode = this
                selectedTitle = this.wTitle
                selectedHref= this.wHref
                selectedSrc = $(this).attr('src')

                let boxId = (1000 * Math.random()).toString(36).substring(3);
                // console.log("[] boxId: " +boxId)

                let importBox = $(
                    "<div class='importBox' data-node-link='" +selectedHref+ "' id='" +boxId+ "'" +
                    "   data-node-title='" +selectedTitle+ "'" +
                    "   data-node-src='" +selectedSrc+ "'>" +
                    "  <img class='importLogo' src='https://wran.gl/src/svg/site-logo-32.svg'/>" + // id='" +boxId+
                    "</div>"
                )
                let nodeOffset = $(this).offset()
                importBox.offset({top: nodeOffset.top+1, left: (nodeOffset.left + ($(this).width() / 2) - 10)})
                body.append(importBox)

                importBox.click(function(ev) {
                    ev.stopPropagation();
                    let importNodeSrc = $(this).attr('data-node-src');
                    let importNodeLink = $(this).attr('data-node-link');
                    let importNodeTitle = $(this).attr('data-node-title');
                    let thisBox = $(this)
                    // alert('importLogo clicked.');
                    WranglMessenger.getAvailableWrangls().then(function(wrangls) {
                        // TODO use $q when to combine messenger cache and realtime load cases.
                        if (wrangls && Object.keys(wrangls).length > 0) {
                            // alert("Using cached wrangls!");
                            let modal = ModalManager.newModal()
                            let content = ModalManager.buildWranglSelectModal(importNodeSrc, importNodeLink, importNodeTitle, wrangls)
                            thisBox.remove()
                            modal.setContent( content )
                            modal.open();

                        } else {
                            WranglService.loadWrangls ().then (function (wrangls) {
                                // alert('Fetching wrangls now.');
                                let modal = ModalManager.newModal ()
                                let content = ModalManager.buildWranglSelectModal (importNodeSrc, importNodeLink, importNodeTitle, wrangls)
                                thisBox.remove()
                                modal.setContent (content)
                                modal.open ();
                            })
                        }
                    })


                    // let card = {title: $(this).attr('title'), img: $(this).src}
                    return false;
                })

            }

            $(".importBox").show();


            // parentNode = $(this).parent()
            // selectedNode = $(this).detach()
            // nodeWrapper = $("<div class='nodeWrapper'></div>")


            // nodeWrapper.append(selectedNode).append(importBox)
            // parentNode.append(nodeWrapper)
            // importBox.insertAfter( $(this) );

        }, function(ev) {
            // bounding box check must not be working, as it we know mouse clearly lies within
            // TODO determine whether we need originalEvent
            if ( $(this).hasClass('wiNode') && !NodeDecorator.isWithinBoundingRect(ev.clientX, ev.clientY, selectedNode) )
                $(".importBox").remove();
        })


    },


}
