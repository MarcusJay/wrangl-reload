let state = {
    imageMap: {},
    linkMap: {},
    wranglMap: {},

    selectedAssetMap: {},
    selectedWranglMap: {},

    toggleAsset(key) {
        if (state.selectedAssetMap.hasOwnProperty(key))
            delete state.selectedAssetMap[key]
        else {
            let asset = state.getAsset(key)
            if (asset) {
                state.selectedAssetMap[key] = asset
                PopupContentMgr.setStatus("Added asset " +asset.wid+ ". Verify: " +state.selectedAssetMap[asset.wid]);
            }
        }
        PopupContentMgr.refreshSelectedCounts()
    },

    getAsset(key) {
        let asset = state.imageMap[key]
        if (!asset)
            asset = state.linkMap[key]
        return asset
    },

    toggleWrangl(wranglId) {
        if (state.selectedWranglMap.hasOwnProperty(wranglId))
            delete state.selectedWranglMap[wranglId]
        else {
            let wrangl = state.wranglMap[wranglId]
            if (wrangl) {
                state.selectedWranglMap[wranglId] = wrangl
            }
        }
        PopupContentMgr.refreshSelectedCounts()
    },

    countSelectedAssets() {
        let keys = Object.keys(state.selectedAssetMap)
        return keys? keys.length : 0
    },

    countSelectedWrangls() {
        let keys = Object.keys(state.selectedWranglMap)
        return keys? keys.length : 0
    },

    resetSelections() {
        state.selectedAssetMap = {}
        state.selectedWranglMap = {}
    },

}

let DragDropMgr = {
    decorateAssets() {
        let assets = $(".asset")
        let asset
        for (let i=0; i<assets.length; i++) {
            asset = $(assets[i])
            asset.attr('draggable', true)
            asset.attr('ondragstart', DragDropMgr.drag(event))
        }
    },

    decorateWrangls() {
        let wrangls = $(".wrangl")
        let wrangl
        for (let i=0; i<wrangls.length; i++) {
            wrangl = $(wrangls[0])
            wrangl.attr("ondrop", DragDropMgr.dropIntoWrangl(event) )
            wrangl.attr("ondragover", DragDropMgr.allowDrop(event) )
        }
    },

    allowDrop(ev) {
        ev.preventDefault ();
        console.log("### allowDrop")
    },

    drag(ev) {
        console.log("### drag")
        // ev.dataTransfer.setData ("text", ev.target.id);
    },

    dropIntoWrangl(ev) {
        console.log("### dropIntoWrangl")
        ev.preventDefault ();
        // var data = ev.dataTransfer.getData ("text");
        // ev.target.appendChild (document.getElementById (data));
    },
}


let PopupContentMgr = {
    prioritizeAssets(assets) {
        // Score by desirability
    },

    clear(container) {
        container.show();
        container.empty();
    },

    addImages(images, container) {
        PopupContentMgr.setImportEnabled(false)
        if (images && images.length > 0)
            PopupContentMgr.showNoContent(false)

        let image
        for (let i=0; i<images.length; i++) {
            image = images[i]
            state.imageMap[image.wid] = image;
            container.append("<div class='asset' data-key='" +image.wid+ "'><div class='importablePageImage' style='background-image:url(" +image.src+ "); background-size: cover; background-position: 50%'>" +
                "</div></div>"
            )
            // "<img src='" +image.src+ "' alt='" +image.title+ "'/></div></div>"
        }
/*      map based
        let imageKeys = Object.keys(images)
        for (let srcKey in images) {
            container.append("<div class='asset'><div class='importablePageImage'><img src='" +srcKey+ "' alt='" +images[srcKey].title+ "'/></div></div>")
        }
*/
    },

    addLinks(links, container) {
        PopupContentMgr.setImportEnabled(false)
        if (links && links.length > 0)
            PopupContentMgr.showNoContent(false)

        let link, linkHtml
        for (let i=0; i<links.length; i++) {
            link = links[i]
            state.linkMap[link.wid] = link;
            linkHtml =
                "<div class='asset' data-key='" +link.wid+ "'>" +
                    "<div class='importablePageLink'>" +
                "       <span class='linkTitle'>" +
                        (link.title || "Untitled")+
                "       </span>" +
                "       <span class='linkUrl'>" +
                "           <a href='" +link.url+ "'>" +getShortUrl(link.url, true)+ "</a>" +
                "       </span>" +
                "   </div>" +
                "</div>"
            container.append(linkHtml)
        }
    },

    // TODO delete once working inside common class
    addWrangls(wrangls, container) {
        container.empty()
        PopupContentMgr.setImportEnabled(false)
        state.wranglMap = wrangls;
        let wrangl, wranglHtml, wranglImage, wranglTitle, thumbHtml, maxThumbs, choice
        for (let wranglId in wrangls) {
            wrangl = wrangls[wranglId]
            wranglImage = (wrangl.questions[0].choices.length > 0)? wrangl.questions[0].choices[0].image : null
            wranglTitle = wrangl.title;
            maxThumbs = wrangl.questions[0].choices? Math.min(wrangl.questions[0].choices.length, 5) : 5;
            wrangl.questions[0].choices = wrangl.questions[0].choices.reverse()
            thumbHtml = ''
            for (let i=0; i<maxThumbs; i++) {
                choice = wrangl.questions[0].choices[i]
                if (choice.image)
                    thumbHtml += ("<div class='wranglThumbnail'><img src='" +choice.image+ "'/></div>")
                else
                    thumbHtml += ("<div class='wranglThumbnail'><span class='choiceTitle'>" +choice.title+ "</span></div>")
            }

            wranglHtml =
                "<div id='" +wranglId+ "' class='wrangl'>" +
                    "<div class='wranglThumbs'>" +thumbHtml+ "</div>" +
                    "<div class='wranglTitle'>" +wranglTitle+ "</div>" +
                "</div>"
            container.append(wranglHtml)
        }
    },

    bindEvents() {
        $(".asset").click(function(ev) {
            console.log("selected asset.")
            $(this).toggleClass( "assetSelected" )
            state.toggleAsset( $(this).attr('data-key') )
            PopupContentMgr.setImportEnabled( state.countSelectedAssets() > 0 && state.countSelectedWrangls() > 0 )
        })
        $(".wrangl").click(function(ev) {
            console.log("selected wrangl.")
            $(this).toggleClass( "wranglSelected" )
            state.toggleWrangl( $(this).attr('id') )
            PopupContentMgr.setImportEnabled( state.countSelectedAssets() > 0 && state.countSelectedWrangls() > 0 )
        })

        $(".importBtn").click(function(ev) {
            if (state.countSelectedAssets() === 0 || state.countSelectedWrangls() === 0) {
                return;
            }

            WranglService.addCardsToWrangls(state.selectedAssetMap, state.selectedWranglMap).then(function(allResults) {
                PopupContentMgr.setWranglLoading(true)
                WranglService.loadWrangls(true).then(function(wrangls) {
                    PopupContentMgr.addWrangls(wrangls, $(".wranglContainer") )
                    PopupContentMgr.bindEvents()
                    PopupContentMgr.setStatus("Import Successful.")
                    PopupContentMgr.setWranglLoading(false)
                    PopupContentMgr.markUpdatedWrangls()
                    PopupContentMgr.resetSelections()
                    state.resetSelections()
                })

            })

        })

        // TODO temp login
        $(".wglProfilePic").click(function(ev) {
            $(this).hide();
            $(".wglLogin").show();
        });
        $(".wglLogin").keyup( function(ev) {
            if (ev.keyCode === 13) {
                $ (this).hide();
                $ (".wglProfilePic").show()
            }
        });

    },

    refreshSelectedCounts() {
        let wranglMsg = state.countSelectedWrangls() > 0? (state.countSelectedWrangls() + " Wrangls") : "..."
        PopupContentMgr.setStatus("Importing " +state.countSelectedAssets() + " Items into " +wranglMsg)
    },

    resetSelections() {
        $(".asset").removeClass( "assetSelected" )
        $(".wrang").removeClass( "wranglSelected" )
    },

    setStatus(status) {
        $(".status").text( status )
    },

    setWranglLoading(isLoading) {
        $ (".wranglLoading").toggle ( isLoading, 300 )
    },

    setAssetLoading(isLoading) {
        $ (".assetLoading").toggle ( isLoading, 300 )
    },

    markUpdatedWrangls() {
        $(".wranglSelected").addClass("updated");
        setTimeout(function() {
            $(".updated").removeClass("updated");
        }, 3000)
    },

    showNoContent(noContent) {
        $(".assetOuter").toggle( !noContent )
        $(".importBtn").toggle( !noContent )
        $(".noContent").toggle( noContent )
    },

    setImportEnabled(enabled) {
        $(".importBtn").toggle(enabled, 300)
    }

}


$(function() {
    let windowHandler = function(windowInfo) {
        // $(".status").text("window width: " +windowInfo.width+".    ")
        // $("html, body").width( 960 )
    }
    chrome.windows.getCurrent(windowHandler);

/*
    chrome.runtime.getBackgroundPage(function(bkg) {
        // alert(bkg.currentUser)
        $(".assetContainer").text(bkg.currentUser)
    })
*/

    PopupContentMgr.setImportEnabled(false)
    PopupContentMgr.setWranglLoading(true)
    WranglService.loadWrangls().then(function(wrangls) {
        PopupContentMgr.addWrangls(wrangls, $(".wranglContainer") )
        PopupContentMgr.bindEvents()
        // DragDropMgr.decorateWrangls()
        PopupContentMgr.setStatus("")
        PopupContentMgr.setWranglLoading(false)
        PopupContentMgr.setImportEnabled( state.countSelectedAssets() > 0 && state.countSelectedWrangls() > 0 )
    })


    console.log("Popup: asking active tab for content.")
    $("#date").text( getTS() )
    // $(".status").append( "Searching for items...")
    PopupContentMgr.setAssetLoading(true)
    PopupContentMgr.setImportEnabled(false)

    // Ask content script for importable items
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {action: "get", object: "All", domain: Util.getShortUrl(tabs[0].url, true)}, function(response) {
            console.log("Popup: Response from tab: " +response)
            // response = JSON.parse( response.itemsSerialized )
            let assetContainer = $(".assetContainer")

            if (!response) {
                $ (".status").text ("Extension has updated. Please Refresh page.")
            } else if (!response.images || response.images.length === 0) {
                PopupContentMgr.showNoContent(true);
            } else {
                console.log('Popup: Got content from tab. Populating popup with ' +response.images.length+ ' assets.')
                $(".pageAssets .title").text( "Items from this page ")
                $(".pageAssets .info").text( response.images.length+ ' Images and ' +response.links.length+ ' Links')
                // $(".status").append( getTS()+ ": WranglService found " +response.links.length+ ' Links.<br/>')
                // $(".dbg").text( getTS()+ ": Asset dump: ")
                // $(".dbg").append( dumpAssets(response.images) )

/*
                chrome.runtime.getBackgroundPage(function(bkg) {
                    // alert(bkg.currentUser)
                    $(".assetContainer").text(bkg.currentUser)
                })
*/

                PopupContentMgr.clear( assetContainer );
                PopupContentMgr.addImages(response.images, assetContainer )
                // PopupContentMgr.addLinks(response.links, assetContainer )
                PopupContentMgr.setAssetLoading(false)

                let assetSize
                document.querySelector('#sizeSlider').oninput = function() {
                        // alert('slider value: ' +$("#sizeSlider").val() );
                        assetSize = $("#sizeSlider").val()
                        $("#sizeValue").text( assetSize );
                        $(".asset").width( assetSize ).height( assetSize )
                };
/*
                $(".assetOuter .assetContainer .asset").draggable({
                    scope: "wranglAssets"
                });
                $(".wranglOuter .wranglContainer .wrangl").droppable({
                    scope: "wranglAssets",
                    accept: ".asset",
                    tolerance: "touch",
                    activeClass: "droptestActive",
                    hoverClass: 'droptestHover',
                    drop: function( event, ui ) {
                        $( this ).html( "Dropped!" );
                    }
                });
                $(".wranglOuter .wranglContainer .wrangl").addClass( "ui-droppable" )
*/

                /*
                $(".wrangl").droppable({
                    accept: ".asset",
                    activeClass: "ui-state-highlight",
                    hoverClass: "ui-droppable-hover",
                    tolerance: "pointer",
                    drop: function( event, ui ) {
                        $( this )
                            .addClass( "ui-state-highlight" )
                            .html( "Dropped!" );
                    }

                });
        */
/*
                dragula([document.getElementsByClassName('asset'), document.getElementsByClassName('wrangl')], {
                    revertOnSpill: true
                }).on('drop', function(el) {
                    console.error("oh hi: we're good. it's working. ")
                    // $('display').innerHTML = $('drop-target').innerHTML;
                }).on('drag', function(el) {
                    console.error('we are dragging')
                }).on('over', function(el) {
                    console.error('we are hovering')
                });
*/

                /*
                    ,
                    {
                        accepts: function (el, target, source, sibling) {
                            return true; // TODO change this: elements can be dropped in any of the `containers` by default
                        },
                        invalid: function (el, handle) {
                            return false; // TODO change: don't prevent any drags from initiating by default
                        },
                        direction: 'vertical',             // Y axis is considered when determining where an element would be dropped
                        copy: true,
                        // copySortSource: false,             // elements in copy-source containers can be reordered
                        revertOnSpill: true,              // spilling will put the element back where it was dragged from, if this is true
                        removeOnSpill: false,              // spilling will `.remove` the element, if this is true
                        // mirrorContainer: document.body,    // set the element that gets mirror elements appended
                        // ignoreInputTextSelection: true     // allows users to select input text, see details below

                    }
                )
*/
/*
                draco.on("drag", function(ev) {
                    console.log("draco drag");
                })
                draco.on("over", function(ev) {
                    console.log("draco over");
                })
*/

            }
            setLoading(false)
        });
    });
});



let getTS = function() {
    let ts = new Date()
    return ts.toTimeString().substring(0, ts.toTimeString().indexOf('GMT'))
}

let dumpAssets = function(assets) {
    let str = ''
    for (let i=0; i<assets.length; i++) {
        let asset = assets[i]
        for (let field in asset) {
            if (asset.hasOwnProperty(field)) {
                str += ("<br/>asset " +i+": field '" +field+ "': " +asset[field])
            }
        }
    }
    return str
}

let setLoading = function(isLoading) {
    if (isLoading) {  // TODO shorthand? show ( condition )
        $ (".loading").show ()
        $ (".pageAssets").hide ()
    } else {
        $ (".loading").hide ()
        $ (".pageAssets").show ()
    }
}

// TODO this is in Util, but unable to call Util from here, although it is in dependencies.
let getShortUrl = function(url, extraShort) {
    if (!url)
        return ' ';
    let regex = /^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)/im;
    let results = regex.exec(url);
    if (!results)
        return url;
    else
        return extraShort && results.length > 1? results[1] : results[0];
}

/*
function $(id) {
    return document.getElementById(id);
}*/

chrome.runtime.getBackgroundPage(function(bkg) {
    bkg.console('YO from the Popup!');
    // bkg.alert('Hi Popup')
})
