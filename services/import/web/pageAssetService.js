let PageAssetService = {

    MIN_DIMENSION: 100,

    WHITELIST: [],
    SKIP_CLASSES: ["pla-unit-single-clickable-target"],

    isImageValid(node) {
        let viable = true;
        if (!node.height() || (node.height() < PageAssetService.MIN_DIMENSION && node.width() < PageAssetService.MIN_DIMENSION)) {
            viable = false;
            console.warn('Asset Validator: Skipping image node with WxH: ' +node.width()+ ' x ' +node.height());
        }
        else if (!node.is(':visible') || node.css("display") === "none") {
            viable = false;
            console.warn('Asset Validator: Skipping invisible image.');
        } else if (PageAssetService.isBlacklisted( PageAssetService.getParentLinkNode(node)) ) {
            viable = false;
            console.warn('Asset Validator: Skipping blacklisted parent link.');
        }
        return viable;
    },

    isLinkValid(href) {
        if (!href)
            return false;
        let viable = true;
        if (href.endsWith('/') || href.indexOf('.') === -1) {
            viable = false;
            console.warn('Asset Validator: Skipping non-single-entity link: ' +href);
        }
        return viable;
    },

    isBlacklisted(node) {
        if (!node || node.length === 0 || node.get(0).className === "")
            return false;

        let classes = node.get(0).className.split(/\s+/)
        if (!classes)
            return false;

        for (let i=0; i<classes.length; i++) {
            if (PageAssetService.SKIP_CLASSES.indexOf(classes[i]) >= 0)
                return true;
        }
        return false;
    },

    // Return all images and links in current page
    getImagesAndLinks() {
        let images = {}, links = {};
        // Get all the nodes on a page
        let imgNodes = document.querySelectorAll ('img, .hasBG');
        let linkNodes = document.querySelectorAll ('a');
        let validNodes = []

        let node, decoratedNode, existingImage, link, src, href, i, wid, parent;

        // alert ('WranglService found ' + imgNodes.length + ' images and ' + linkNodes.length + ' links on this page.');

        // Collect largest instance of each image
        for (i = 0; i < imgNodes.length; i++) {
            node = imgNodes[i];
            if (!PageAssetService.isImageValid( $(node) ))
                continue;

            src = node.getAttribute ('src');

            // check for multiple image instances
            existingImage = (src && src.length > 0)? imgNodes[src] : null;
            parent = PageAssetService.getParentLinkNode(node)
            link = PageAssetService.getParentHref(node) // TODO or just get from parent directly
            wid = Math.random().toString(36).substring(7)

            if (!existingImage) {
                PageAssetService.decorateAsset (node, wid )
                validNodes.push (node)
            }

            if (!existingImage || (existingImage.width < node.clientWidth && existingImage.height < node.clientHeight)) {
                images[src] = {wid: wid, url: src, src: src, title: node.wTitle, href: node.wHref, width: node.clientWidth, height: node.clientHeight, count: 1};
            } else {
                images[src].count += 1;
            }
        }

        // Gather one of each link
        for (i = 0; i < linkNodes.length; i++) {
            node = linkNodes[i];
            href = node.getAttribute ('href');
            if (!PageAssetService.isLinkValid( href ))
                continue;

            // check for multiple link instances
            link = (href && href.length > 0)? links[href] : null;
            node.wid = Math.random().toString(36).substring(7)

            if (!link) {
                validNodes.push( node )
                links[href] = {wid: node.wid, url: href, title: node.text.trim(), count: 1};
            } else {
                links[href].count += 1;
            }
        }

        // alert ('PageAssetService: returning results: ' +images);
        // Sort by most highly referenced in page
        return {images: images, links: links, validNodes: validNodes}
/*
        return {
            images: Object.getOwnPropertyNames (images).sort (function (a, b) {
                return images[b].count - images[a].count;
            }),
            links: Object.getOwnPropertyNames (links).sort (function (a, b) {
                return links[b].count - links[a].count;
            })
        }
*/
    },

    getParentHref(node) {
        let linkNode = $(node).closest("[href!=''][href]");
        let href = (linkNode && linkNode.length > 0)? linkNode.attr('href') : null;

        // standardize to absolute url
        if (href) {
            let a = document.createElement ('a');
            a.href = href;
            href = a.href;
        }

        return href;
    },

    getParentLinkNode(node) {
        let parentNode = $(node).closest("[href!=''][href]");
        return (parentNode !== node)? parentNode : null;
    },

    decorateAsset(node, wid ) {
        if (!node)
            return;

        let parent = PageAssetService.getParentLinkNode( node )
        node.wTitle = parent? parent.text() : node.getAttribute('alt')
        if (node.wTitle)
            node.wTitle = node.wTitle.trim()
        node.wHref = PageAssetService.getParentHref( node )
        node.wid = wid;
    }

}