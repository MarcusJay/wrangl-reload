
let DomObserver = {
    observeDOM () {
        let MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
            eventListenerSupported = window.addEventListener;

        return function (obj, callback) {
            if (MutationObserver) {
                // define a new observer
                var obs = new MutationObserver (function (mutations, observer) {
                    if (mutations[0].addedNodes.length || mutations[0].removedNodes.length)
                        callback ();
                });
                // have the observer observe foo for changes in children
                obs.observe (obj, {childList: true, subtree: true});
            }
            else if (eventListenerSupported) {
                obj.addEventListener ('DOMNodeInserted', callback, false);
                obj.addEventListener ('DOMNodeRemoved', callback, false);
            }
        };
    }
}

let NodeDecorator = {
    // TODO deprecated?
    markImportableNodes: function() {
        $('*:not(span,p)').filter(function() {
            if (this.currentStyle)
                return this.currentStyle['backgroundImage'] !== 'none';
            else if (window.getComputedStyle)
                return document.defaultView.getComputedStyle(this,null)
                        .getPropertyValue('background-image') !== 'none';
        }).addClass('hasBG');
    },

    injectImportBoxes: function() {
        let body = $("body")
        let validNodes = PageAssetService.getImagesAndLinks().validNodes;
        let node, jqNode, madnysNode, nodeOffset, origBorder, importBox, boxId, src, parentNode, parentLink, title, i;

        for (i = 0; i < validNodes.length; i++) {
            node = validNodes[i]
            jqNode = $(node)

            // decorate images only for now
            if (node.name.toLowerCase() !== "img")
                continue;

            jqNode.addClass("wiNode");
            origBorder = jqNode.css("border")
            // console.log("###" +boxId);
        }

        importBox = $(
            "<div class='importBox' data-node-link='' id='" +boxId+ "'" +
            "   data-node-title=''" +
            "   data-node-src=''>" +
            "  <img class='importLogo' src='https://wran.gl/src/svg/site-logo-32.svg'/>" + // id='" +boxId+
            "</div>"
        )

        let selectedNode, selectedTitle, selectedHref, selectedSrc
        $(".wiNode").hover(function() {
            // console.log("--- hover over " +node);
            if ( $(this).hasClass('wiNode') ) {
                $(".importBox").remove()
                selectedNode = this
                selectedTitle = this.wTitle
                selectedHref= this.wHref
                selectedSrc = $(this).attr('src')

                let boxId = (1000 * Math.random()).toString(36).substring(3);
                // console.log("[] boxId: " +boxId)

                let importBox = $(
                    "<div class='importBox' data-node-link='" +selectedHref+ "' id='" +boxId+ "'" +
                    "   data-node-title='" +selectedTitle+ "'" +
                    "   data-node-src='" +selectedSrc+ "'>" +
                    "  <img class='importLogo' src='https://wran.gl/src/svg/site-logo-32.svg'/>" + // id='" +boxId+
                    "</div>"
                )
                let nodeOffset = $(this).offset()
                importBox.offset({top: nodeOffset.top+1, left: (nodeOffset.left + ($(this).width() / 2) - 10)})
                body.append(importBox)

                importBox.click(function(ev) {
                    ev.stopPropagation();
                    let importNodeSrc = $(this).attr('data-node-src');
                    let importNodeLink = $(this).attr('data-node-link');
                    let importNodeTitle = $(this).attr('data-node-title');
                    let thisBox = $(this)
                    // alert('importLogo clicked.');
                    WranglMessenger.getAvailableWrangls().then(function(wrangls) {
                        // TODO use $q when to combine messenger cache and realtime load cases.
                        if (wrangls && Object.keys(wrangls).length > 0) {
                            // alert("Using cached wrangls!");
                            let modal = ModalManager.newModal()
                            let content = ModalManager.buildWranglSelectModal(importNodeSrc, importNodeLink, importNodeTitle, wrangls)
                            thisBox.remove()
                            modal.setContent( content )
                            modal.open();

                        } else {
                            WranglService.loadWrangls ().then (function (wrangls) {
                                // alert('Fetching wrangls now.');
                                let modal = ModalManager.newModal ()
                                let content = ModalManager.buildWranglSelectModal (importNodeSrc, importNodeLink, importNodeTitle, wrangls)
                                thisBox.remove()
                                modal.setContent (content)
                                modal.open ();
                            })
                        }
                    })


                    // let card = {title: $(this).attr('title'), img: $(this).src}
                    return false;
                })

            }

            $(".importBox").show();


            // parentNode = $(this).parent()
            // selectedNode = $(this).detach()
            // nodeWrapper = $("<div class='nodeWrapper'></div>")


            // nodeWrapper.append(selectedNode).append(importBox)
            // parentNode.append(nodeWrapper)
            // importBox.insertAfter( $(this) );

        }, function(ev) {
                // bounding box check must not be working, as it we know mouse clearly lies within
            // TODO determine whether we need originalEvent
            if ( $(this).hasClass('wiNode') && !NodeDecorator.isWithinBoundingRect(ev.clientX, ev.clientY, selectedNode) )
                $(".importBox").remove();
        })


    },

    isWithinBoundingRect(x,y,node) {
        let rect = node.getBoundingClientRect()
        let result = (x < rect.right && x > rect.left && y < rect.bottom && y > rect.top)
        console.log("is within bounds: " +result+ ". x = " +x+ ", y = " +y+ ", nodeRect top|right|bottom|left: " +rect.top + ' ' +rect.right + ' ' +rect.bottom+ ' ' +rect.left )
        return result;
    }


}

// TODO Delete? Using PageAssetService
let NodeValidator = {

    isViable(node) {
        let viable = true;
        if (!node.height() || (node.height() < PageAssetService.MIN_DIMENSION && node.width() < PageAssetService.MIN_DIMENSION)) {
            viable = false;
            console.warn('Content Asset Validator: Skipping image node with WxH: ' +node.width()+ ' x ' +node.height());
        }
        else if (!node.is(':visible') || node.css("display") === "none") {
            viable = false;
            console.warn('Asset Validator: Skipping invisible image.');
        }
        return viable;
    }
}

// Sends Requests to Background extension script
let WranglMessenger = {

    getAvailableWrangls: function () {
        // motivation for requesting from background rather than wranglService directly is performance- background has already cached wrangls.
        let promise = new Promise (function (resolve, reject) {
            chrome.runtime.sendMessage ({action: "get", object: "wrangls", take: 10}, function (response) {
                console.log (response.wrangls);
                resolve (response.wrangls);
            });
        });
        return promise;
    },

    getDefaultWrangl: function () {
        chrome.runtime.sendMessage ({action: "get", object: "defaultWrangl"}, function (response) {
            console.log (response.defaultWrangl);
        });
    }

    // for this operation, no advantage to passing off to background script, so perform directly.
/*
    addCardToWrangl: function (wrangl, cardData) {
        alert ('sending msg to add card to wrangl.');
        chrome.runtime.sendMessage ({
            action: "add",
            object: "card",
            data: {wranglId: wrangl.id, card: cardData}
        }, function (response) {
            console.log (response.status);
        });
    }
*/

}

// Handles Incoming Requests from Background extension script
let WranglRequestHandler = {
    getAll(sendResponse) {
        // alert('Got getAll request.')

        let items = PageAssetService.getImagesAndLinks();
        items.images = Object.values(items.images)
        items.links = Object.values(items.links)

        sendResponse( items )
        cache = null
    }
}

let ModalManager = {
    defaultStyles: {
        footer: true,
        stickyFooter: false,
        closeMethods: ['overlay', 'button', 'escape'],
        closeLabel: "Close",
        cssClass: ['custom-class-1', 'custom-class-2']
    },

    newModal() {
        let modal = new tingle.modal ({
            footer: true,
            stickyFooter: false,
            closeMethods: ['overlay', 'button', 'escape'],
            closeLabel: "Close",
            cssClass: ['custom-class-1', 'custom-class-2'],
            onOpen: function () {
                console.log ('modal open');
            },
            onClose: function () {
                console.log ('modal closed');
            },
            beforeClose: function () {
                // here's goes some logic
                // e.g. save content before closing the modal
                return true; // close the modal
                return false; // nothing happens
            }
        })
        ModalManager.modal = modal
        return modal
    },

    closeModal() {
        if (ModalManager.modal)
            ModalManager.modal.close()
    },

    buildWranglSelectModal(nodeSrc, nodeLink, nodeDefaultTitle, wrangls) {
        let placeholder = (nodeDefaultTitle && nodeDefaultTitle.length > 0)? nodeDefaultTitle : "Please enter a title..."
        // nodeLink = (nodeLink && nodeLink.length > 0)? nodeLink : null

        let header = $( '<div class="wglHeader">' +
                        '   <img class="wglLogo" src="https://wran.gl/src/img/bluePopupCatTransparent2.png"/>' +
                        '   <span class="wglBrand">Wrangl</span>' +
                        '   <span class="wglProfileName"></span>' +
                        '   <img class="wglProfilePic" src="https://wran.gl/src/svg/site-logo-25.svg"/>' +
                        '   <input class="wglLogin" type="text" placeholder="Enter a user ID" style="display: none;"/>' +

                        '</div>'
        )
        let status = $("<div class='status'></div>")
        let titleRow = $( '<div class="titleRow"></div>' )
        titleRow.append('<span class="previewLabel">Title </span><span class="textPreview"><input id="importTitle" class="wglText" type="text" placeholder="' +placeholder+ '"/></span><br/>')
        if (nodeSrc !== null)
            titleRow.append('<span class="previewLabel">Preview </span><span class="imgPreview"><img id="importImage" src="' +nodeSrc+ '"/></span>')
        if (nodeLink !== null)
            titleRow.append('<span class="previewLabel">&nbsp;</span><span class="linkPreview">' +Util.getShortUrl(nodeLink, true)+ '</span><br/>')
        let topRow = $( '<div class="topRow"></div>' )
        topRow.append(titleRow)


        let wranglImage, wranglTitle
        let wranglHtml = WranglService.createWranglHtml(wrangls, 6)

/*
        for (let wranglId in wrangls) {

            wranglImage = (wrangls[wranglId].questions[0].choices.length > 0)? wrangls[wranglId].questions[0].choices[0].image : null
            wranglTitle = wrangls[wranglId].title;
            if (wranglImage !== null) {
                wranglHtml += "<div class='wrangl' id='" +wranglId+ "'><div class='wranglTitle'>" +wranglTitle+ "</div>"
                wranglHtml += "<img class='wranglImage' src='" + wranglImage + "'/></div>"
            }
        }
*/
        let buttonHtml = "<button id='addLinkBtn' class='tingle-btn tingle-btn--primary'>Add</button>" +
                         "<button id='cancelLinkBtn' class='tingle-btn '>Close</button>"

        let midSection = $('<div class="middle"><div class="middleHeader"><div class="title">Your Wrangls</div><div class="actions">' +buttonHtml+ '</div></div></div>')
        let wranglSection = $('<div class="wranglOuter"><div class="wranglContainer">' +wranglHtml+ '</div></div>')

        let modalContent = $('<div class="importModal"/>')
        modalContent
            .append(header)
            .append(status)
            .append(topRow)
            .append(midSection)
            .append(wranglSection)

        // bind events
        ModalManager.bindWranglEvents(modalContent, true)

        // TODO temp login
        $(".wglProfilePic").click(function(ev) {
            $(this).fadeOut(200)
            $(".wglLogin").fadeIn(200)
        });
        $(".wglLogin").on('keyUp', function(ev) {
            if (ev.keyCode === 13) {
                $ (this).fadeOut (200)
                $ (".wglProfilePic").fadeIn (200)
            }
        });
        midSection.find("#addLinkBtn").click(function(event) {

            let wranglNode = $(".wranglSelected")
            let wranglId = wranglNode[0].id
            let cardTitle = $("#importTitle").val() || nodeDefaultTitle;
            let cardImage = nodeSrc || $("#importImage").src
            let cardData = {title: cardTitle, src: cardImage, link: nodeLink, href: nodeLink}
            wranglNode.html("<div class='loadingContainer'><img class='wLoading' src='https://s3.amazonaws.com/cdn.pradux.com/uploads/1441134990_ellipsis.gif'/></div>")

            WranglService.addCardToWrangl(cardData, wranglId).then(function(result) {
                WranglService.loadWrangls(true).then(function(wrangls) {
                    ModalManager.setStatus("Import Successful", 2500)
                    let wranglContainer = $(".wranglContainer")
                    // wranglContainer.empty()
                    wranglContainer.html( WranglService.createWranglHtml(wrangls, 6) )
                    ModalManager.bindWranglEvents(modalContent, false)
                });
            });
/*
            setTimeout(function() {
                ModalManager.closeModal ()
            }, 2500);
*/
        })
        midSection.find("#cancelLinkBtn").click(function(event) {
            ModalManager.closeModal()
        })

        return modalContent[0];
    },

    bindWranglEvents(container, autoSelect) {
        let wranglBoxes = container.find(".wrangl")
        if (wranglBoxes.length > 0 && autoSelect)
            $(wranglBoxes[0]).addClass('wranglSelected');
        wranglBoxes.click(function(event) {
            wranglBoxes.removeClass("wranglSelected")
            $(this).addClass("wranglSelected")
        })
    },

    setStatus(msg, duration) {
        let status = $(".status")
        status.text( msg )
        status.show()
        if (duration > 0) {
            setTimeout(function() {
                status.fadeOut(1500);
            }, duration)
        }

    }
}

// Begin main thread

// Observe DOM for changes once doc is loaded
$(function() {
    DomObserver.observeDOM (document.getElementById ('body'), function () {
        console.log ('!!! dom changed');
    });

    // Listeners

    chrome.runtime.onMessage.addListener(
        function(request, sender, sendResponse) {
            let command = request.action + request.object;
            // alert("### WranglContent: Received command: " +command);
            console.log("### WranglContent: Received command: " +command);
            let domain = Util.getShortUrl(window.location.href, true)
            if (domain !== request.domain) {
                // sendResponse("Ignoring " +domain)
            }
            else
                WranglRequestHandler[command](sendResponse, request.data)
        })
});


// Get importable nodes if any
// TODO consolidate to PageAssetMananger
NodeDecorator.markImportableNodes()
// PageAssetService.getImagesAndLinks()

// Decorate importable nodes with wrangl import boxes
NodeDecorator.injectImportBoxes()

DomObserver.observeDOM (document.getElementById ('body'), function () {
    console.log ('!!! dom changed');
});




