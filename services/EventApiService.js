import axios from 'axios'
import moment from 'moment'

import userStore from '/store/userStore'

import {ApiTempKeyName, ApiTempKeyValue} from '/config/ApiEndpoints'
import {
    eventHistory, eventsDismiss, eventsUnseen, eventsUnseen2, userBadges,
    userBadgesDismiss
} from "../config/ApiEndpoints";

const DAYS_OF_HISTORY = 356

class EventApiService {
    constructor() {
    }

    getUserBadges() {
        const userId = userStore.currentUser.id

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userBadges,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId
            }
        })
    }

    /**
     *
     * @param theIds - array of 1 or more ids
     * @param idType - event_ids, item_ids
     * @returns {AxiosPromise}
     */
    dismissUserBadges(theIds, idType = 'event_ids') {
        const userId = userStore.currentUser.id
        console.warn("DISMISS")
        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userBadgesDismiss,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId,
                [idType]: theIds
            }
        })
    }


    // Endpoint note: Whether projectId is present or not, results will be in same form: { user_id: 1234, projects: [] }
    getUnseenEventsNew(userId, projectId) {
        if (!userId && userStore.currentUser)
            userId = userStore.currentUser.id

        if (!userId)
            throw new Error ('unseen events: no userId')

        console.log('@@@ %c getUnseenEventsNew pre-axios', 'color: pink')
        let payload = {
            [ApiTempKeyName]: ApiTempKeyValue,
            "user_id" : userId
        }
        if (projectId && projectId !== 'null')
            payload['project_id'] = projectId
        // else
        //     debugger

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: eventsUnseen2,
            data: payload
        })
    }

    getEventHistory(projectId, sinceDate) {
        if (!projectId)
            throw new Error ('eventHistory: no projectId')
        if (!sinceDate)
            sinceDate = moment().subtract(DAYS_OF_HISTORY, 'days')

        const sinceStr = sinceDate.format('YYYY-MM-DD hh:mm:ss')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: eventHistory,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "project_id" : projectId,
                "time_modified" : sinceStr
            }
        })
    }

    dismissEvents(eventIds) {
        const userId = userStore.currentUser.id

        if (!userId || !eventIds || eventIds.length === 0)
            throw new Error ('dismiss: no events')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: eventsDismiss,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId,
                "event_ids" : eventIds
            }
        })
    }

    dismissEventsByType(projectId, cardId, eventTypes) {
        const userId = userStore.currentUser.id

        if (!userId || !eventTypes || (!projectId && !cardId))
            throw new Error ('dismiss: missing params')

        const targetField = projectId? 'project_id' : 'card_id'
        const targetValue = projectId? projectId : cardId;
// debugger
        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: eventsDismiss,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId,
                "event_types" : eventTypes,
                [targetField] : targetValue
            }
        })
    }


}

export default new EventApiService()