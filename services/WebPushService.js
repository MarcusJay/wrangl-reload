import axios from 'axios'

import userStore from '/store/userStore'

const SERVICE_WORKER = "/static/sw/serviceWorker.js"
const WRANGL_PUSH_PUBLIC_KEY = "BPr7ukDq9bTNJwb6HtJh0tBdlJoYvv32jHDBMV8oFQ9rls_12H-MPZNyuIPigqS8gwWcvAU9o4reuBu4wusKWdc"

const WranglPushLocal = "http://mjdev.hopto.org"
const WranglPushRemote = "https://push.pogo.io"

let currWranglPush = WranglPushRemote // WranglPushRemote // DEV REMOTE!!!!!!!!!!!!!!!!

let instance = null

// Handles incoming offline web push notifications
// Singleton, contains instance
class WebPushService {
    constructor() {
        if (!instance) {
            instance = this;
            console.log("WebPushService: Created Singleton.")
        }
        this.registration = null;

        return instance // Singleton
    }

    registerServiceWorker() {
        if (!('serviceWorker' in navigator && 'PushManager' in window))
            return null;
        let promise = new Promise (function (resolve, reject) {
            navigator.serviceWorker.register(SERVICE_WORKER).then(function(registration) {
                // Registration was successful
                console.log('Wrangl ServiceWorker registration success with scope: ', registration.scope);

                let wrangleSW = registration.active || registration.installing;

                // statechange fires every time the ServiceWorker.state changes
                if (wrangleSW) {
                    wrangleSW.onstatechange = function () {
                        // show the message on activation
                        if (wrangleSW.state === 'activated' && !navigator.serviceWorker.controller) {
                            // alert ('we are offline ready')
                            navigator.serviceWorker.addEventListener('message', event => {
                                console.log(event.data.msg)
                                try {
                                    WebMessenger.handleMessage (wrangleSW, event.data.msg)
                                } catch (error) {
                                    console.log(error)
                                }

                            });

                        }
                    };
                }
                resolve(registration)
            }, function(err) {
                console.log('Wrangl ServiceWorker registration failed: ', err);
                // debugger
                reject(err);
            });

        });
        return promise
    }

    askPermissionToNotifyNew() {
        let promise = new Promise (function (resolve, reject) {
            Notification.requestPermission().then(function (result) {
                if (result === 'denied') {
                    console.log ('Permission wasn\'t granted. Allow a retry.');
                    resolve(result);
                }
                else if (result === 'default') {
                    console.log ('The permission request was dismissed.');
                    reject(result);
                }
                // else Success
            });
        });
        return promise

    }

    askPermissionToNotify() {
        return new Promise (function (resolve, reject) {
            const permissionResult = Notification.requestPermission (function (result) {
                resolve (result);
            });

            if (permissionResult) {
                permissionResult.then (resolve, reject);
            }
        }).then (function (permissionResult) {
                if (permissionResult !== 'granted') {
                    throw new Error ('We weren\'t granted permission.');
                }
            });
    }


    // TODO LEFT OFF MAKE SURE THIS IS ONLYH CALLED FROM CLIENT. APPLAYOUT IS INSUFFICIENT, NO GUARANTEE WE'RE IN BROWSER.
    subscribeUserToPush() {

        if (!navigator || !window || !('serviceWorker' in navigator && 'PushManager' in window)) {
            console.error("Failed to subscribe to push. navigator: " +navigator+ ", window: "+window)
            return
        }

        if (!userStore.currentUser) {
            console.error("Did not subscribe to push because no user is logged in.")
            return
        }

        let self = this
        try {
            self.registerServiceWorker ()
                // .then (self.askPermissionToNotifyNew ())
                .then (function (registration) {
                    const subscribeOptions = {
                        userVisibleOnly: true,
                        applicationServerKey: WebPushService.urlBase64ToUint8Array (WRANGL_PUSH_PUBLIC_KEY)
                    };
                    registration.pushManager.subscribe (subscribeOptions).then (
                        function (pushSubscription) {
                            // debugger
                            if (pushSubscription) {
                                self.saveUserSubscription (pushSubscription)
                                console.log ('Received PushSubscription: ', JSON.stringify (pushSubscription));
                            } else {
                                console.error ('Received no PushSubscription from pushManager!')
                            }
                        }, function (error) {
                            // debugger
                            console.error('Error from pushmanager subscribe')
                        }
                    )
                })
        } catch (error) {
            console.error("Error while trying to register service worker and subscribe to push. ", error)
        }
    }

    saveUserSubscription(subscription) {
        if (!userStore.currentUser) {
            debugger
        }

        const subInfo = {subscription: subscription, userId: userStore.currentUser.id}

        axios({
            method: 'POST',
            url: currWranglPush+ '/subscribe-to-offline',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(subInfo ),
            body: JSON.stringify(subInfo )
        }).then(function(response) {
            if (response.status !== 200) {
                console.error('Bad status code from server:', response.status);
            }
        }).catch( error => {
            console.error("Error attempting to subscribe-to-offline", error)

        })
    }



/*
    static urlBase64ToUint8Array(base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/')
        ;
        const rawData = window.atob(base64);
        return Uint8Array.from([...rawData].map((char) => char.charCodeAt(0)));
    }

*/
    static urlBase64ToUint8Array(base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
            .replace(/-/g, '+')
            .replace(/_/g, '/');

        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);

        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    }

}

export default new WebPushService()

class WebMessenger {

    static handleMessage(sw, msg) {
        console.log('!!! WebPushService handleMessage: ' +msg)
        if (!userStore.currentUser)
            debugger

        switch(msg) {
            case 'get currentUser.id':
                const userId = userStore.currentUser? userStore.currentUser.id : 'no userStore avail'
                console.log('!!! WebPushService replying with : ' +userId)
                sw.postMessage(userId);
                break;
            // TODO any further messaging
        }
    }
}