import axios from 'axios'

import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import userStore from '/store/userStore'
import {
    commentCreate2, commentsDelete, commentsRead, commentUpdate, conversationRead,
    projectCommentsRead2
} from "../config/ApiEndpoints";

// class CommentApiService {

// Functions sorted by CRUD


export function createCommentApi(text, projectId, friendId, atCards, atUsers) {
    if (!projectId && !friendId) {
        console.error ('createComment: missing params. pls specify project or dm recipient.')
        return
    }

    let authorId = userStore.currentUser.id

    let payload = {
        [ApiTempKeyName]: ApiTempKeyValue,
        comment_text: text,
        comment_creator: authorId,
    }

    if (projectId)
        payload['project_id'] = projectId
    else if (friendId) // DM
        payload['recipient_id'] = friendId

    if (atCards && atCards.length > 0)
        payload['at_cards'] = atCards
    if (atUsers && atUsers.length > 0)
        payload['at_users'] = atUsers

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: commentCreate2,
        data: payload
    })
}


/**
 *
 * @param commentId
 * @returns {Promise} a single comment object.
 */
export function getCommentByIdApi(commentId) {
    if (!commentId) {
        throw new Error ('getCommentById: missing commentId')
    }
    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: commentsRead,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            "comment_ids": [commentId]
        }
    })
}

/**
 *
 * @param userId
 * @returns {AxiosPromise} format { comments: [commentId, commentId, ... commentId] }
 */
export function getMyCommentsApi(userId) {
    if (!userId) {
        throw new Error ('getUserComments: missing userId')
    }

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        url: commentsRead,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            "user_id": userId
        }
    })
}

/* alias */
export const getUserCommentsApi = getMyCommentsApi

export function getConversationApi(userId, friendId) {
    if (!userId || !friendId) {
        console.error ('conversation: missing 1 or more user ids')
        return
    }

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: conversationRead, // ascending order
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            user_id: userId,
            friend_id: friendId
        }
    })
}

export function getProjectConvoApi(projectId) {
    if (!projectId) {
        console.error ('getProjectConvo: missing project id')
        return
    }

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: projectCommentsRead2,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            project_id: projectId
        }
    })
}

/**
 *
 * @param apiComment
 * @returns {AxiosPromise} a single comment object (see apiComment fields in CommentModel). Does not include comments.
 */
export function updateCommentApi(apiComment) {
    if (!apiComment) {
        throw new Error ('updateComment: missing comment')
    }

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: commentUpdate,
        data: Object.assign (apiComment, {[ApiTempKeyName]: ApiTempKeyValue})
    })

}

export function attachItemToCommentApi(itemId, itemType, commentId) {
    let comment = {comment_id: commentId}
    if (itemType === 'card')
        comment.at_cards = [itemId]
    else if (itemType === 'user')
        comment.at_users = [itemId]

    return updateCommentApi(comment)
}

export function removeItemFromCommentApi(itemId, itemType, commentId) {
    let comment = {comment_id: commentId}
    if (itemType === 'card')
        comment.remove_at_cards = [itemId]
    else if (itemType === 'user')
        comment.remove_at_users = [itemId]

    return updateCommentApi (comment)
}

export function deleteCommentApi(commentId) {
    return deleteCommentsApi ([commentId])
}

/**
 *
 * @param commentIds
 * @returns {AxiosPromise} { success: true }
 */
export function deleteCommentsApi(commentIds) {
    if (!commentIds) {
        throw new Error ('deleteComment: missing parameters')
    }

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            // 'Content-Type': 'application/json',
        },
        url: commentsDelete,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            "comment_ids": commentIds,
            "user_id": userStore.currentUser.id

        }
    })
}


// export default new CommentApiService()