import axios from 'axios'
import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import {EventSource} from '/config/EventConfig'
import userStore from '/store/userStore'
import {attachmentCreateLink, attachmentDelete, attachmentUploadFile} from "../config/ApiEndpoints";

const PART_BOUNDARY = "[END JSON OBJECT BEGIN FILE DATA]"

/**
 * @See also FileService. Keeping separate initially, combine if benefits outweight complications.
 */

export function uploadAttachment (file, ext, cardId, callback, description, count) {

    // Shall we save file preview as well? Nah.

    if (!file || !ext || !cardId)
        throw new Error('uploadAttachment: missing file|ext|cardId')

    if (!description)
        description = 'File attached ' +new Date().toLocaleString()

    const userId = userStore.currentUser? userStore.currentUser.id : 'anon'

    let req = new XMLHttpRequest()
    let hostedUrl = null

    let manifest = {
        [ApiTempKeyName] : ApiTempKeyValue,
        card_id: cardId,
        source: EventSource.Wrangle,
        file_type: ext, // .substring(0,3),// TODO: SEND ENTIRE EXTENSIONS, NOT 3
        description: description,
        user_id: userId
    }

    if (cardId.length !== 36)
        throw new Error('uploadAttachment: card ID length is not 36. ' +cardId)
    const blob = new Blob ([JSON.stringify(manifest), PART_BOUNDARY, file])

    req.open("POST", attachmentUploadFile, true);
    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    req.send(blob)

    req.onloadstart = function (e) {
        callback(count, 'start')
    }
    req.onloadend = function (e) {
        callback(count, 'end', hostedUrl)
    }

    req.onreadystatechange = function(e, arg) {
        console.log("State changed to : " +req.readyState)
        if (req.readyState === XMLHttpRequest.DONE && req.status === 200) {
            hostedUrl = JSON.parse(this.response).url
            console.log("Attachment Upload done. Response url is: " +hostedUrl)
            callback(count, hostedUrl)
        }
    }

}

export function linkAttachment(docUrl, cardId, description) {
    if (!docUrl || !cardId)
        throw new Error('linkAttachment: missing url or card id')

    const userId = userStore.currentUser? userStore.currentUser.id : 'anon'

    const extIdx = docUrl.lastIndexOf('.')
    if (extIdx <= 0)
        throw new Error('linkAttachment: doc url invalid: ' + docUrl)

    let ext = docUrl.substring( extIdx + 1 )
    if (!ext || ext.length < 2 || ext.length > 4) {
        console.warn ('linkAttachment: invalid ext ' + ext + '. Setting to default.')
        ext = 'nil'
    }

    if (!description)
        description = 'File attached on ' +new Date().toLocaleString()

    return axios({
        method: 'POST',
        headers: {
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: attachmentCreateLink,
        data: {
            [ApiTempKeyName] : ApiTempKeyValue,
            url: docUrl,
            card_id : cardId,
            source : 0, // TODO web, scrape, dropbox, what
            file_type: ext, // .substring(0,3), // TODO: SEND ENTIRE EXTENSIONS, NOT 3
            description: description,
            user_id: userId
        }
    })

}

export function deleteAttachment(cardId, attachmentId) {
    if (!attachmentId || !cardId)
        throw new Error('delAttachment: missing attachment id or card id')

    const userId = userStore.currentUser? userStore.currentUser.id : 'anon'

    return axios({
        method: 'POST',
        headers: {
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: attachmentDelete,
        data: {
            [ApiTempKeyName] : ApiTempKeyValue,
            card_id : cardId,
            attachment_id : attachmentId,
            user_id: userId
        }
    })
}

// export default new AttachmentService()