import {UserRole} from "../../config/Constants";
import ReactionApiService from '../ReactionApiService'

export function getProjectReactionSummary(project) {
    if (!project || !project.cards || project.cards.length === 0)
        return {userStats: {}, cardStats: {}}

    let userId, collectionName, cardId, reactionId
    const starsId = ReactionApiService.getStarsId()
    const thumbsUpId = ReactionApiService.getThumbsUpId()
    const thumbsDownId = ReactionApiService.getThumbsDownId()

    // init maps
    let userStats = {}, cardStats = {}
    project.cards.forEach(card => {
        if (!card.id) // TODO likely impossible but for debugging
            return
        cardStats[card.id] = {}
        cardStats[card.id].approvers = new Set([])
        cardStats[card.id].rejectors = new Set([])
        cardStats[card.id].raters = new Set([])
        cardStats[card.id].percentComplete = 0
    })

    // we include requestor as well 1) for testing, and 2) for self-managing where requestor may need voting privs.
    const approvers = project.members.filter(user => user.role === UserRole.APPROVER || user.role === UserRole.REQUESTOR) || []
    approvers.forEach(user => {
        if (!user || !user.id) // TODO likely impossible but for debugging
            return
        userStats[user.id] = {}
        userStats[user.id].cardsApproved = new Set([])
        userStats[user.id].cardsRejected = new Set([])
        userStats[user.id].cardsRated = new Set([])
        userStats[user.id].percentComplete = 0
    })

    // hydrate maps
    project.cards.forEach(card => {
        if (!card.reactions || card.reactions.length === 0)
            return

        // the switch could be replaced with reaection+type+id to propName utils, but it becomes pretty ugly
        card.reactions.forEach(reaction => {
            if (reaction.user_id) {
                userId = reaction.user_id

                // TODO Why no stats for this user? Well, he's not an approver. IGNORE NON_APPROVERS!
                if (!userStats[userId]) {
                    console.warn("Ignoring user " +userId+ " because they are not an approver or requestor.")
                    return
                }

                switch (reaction.reaction_type_id) {
                    case thumbsUpId:
                        userStats[userId].cardsApproved.add(card.id)
                        cardStats[card.id].approvers.add(userId)
                        break
                    case thumbsDownId:
                        userStats[userId].cardsRejected.add(card.id)
                        cardStats[card.id].rejectors.add(userId)
                        break
                    case starsId:
                        userStats[userId].cardsRated.add(card.id)
                        cardStats[card.id].raters.add(userId)
                        break
                }
            }
        })
    })

    // how about some grand totals?
    const totalCards = project.cards.length
    const totalApprovers = approvers.length
    let reactionCount, reactorCount

    approvers.forEach(user => {
        if (!user || !user.id || !userStats[user.id]) { // TODO debugging
            console.error('Bad user in userStats.')
            return
        }
        reactionCount = userStats[user.id].cardsApproved.size + userStats[user.id].cardsRejected.size + userStats[user.id].cardsRated.size
        if (totalCards > 0)
            userStats[user.id].percentComplete = 100 * reactionCount / totalCards
    })

    project.cards.forEach(card => {
        if (!card || !card.id || !cardStats[card.id]) { // TODO debugging
            console.error('Bad card in cardStats.')
            return
        }
        reactorCount = cardStats[card.id].approvers.size + cardStats[card.id].rejectors.size + cardStats[card.id].raters.size
        if (totalApprovers > 0)
            cardStats[card.id].percentComplete = 100 * reactorCount / totalApprovers
    })

    return {userStats: userStats, cardStats: cardStats}
}
