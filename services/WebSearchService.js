import axios from 'axios'
//import { searchUrl } from '/config/ApiEndpoints'
import {getUrlComponents} from "../utils/solo/getUrlComponents";
import {spaceToPlus} from "../utils/solo/spaceToPlus";

const MIN_SEARCH_LEN = 3

const localSearch = 'http://localhost:8201/'
const remoteSearch = 'https://search.pogo.io/'
// const newRemoteSearch = 'http://35.194.41.144:8201'

const currSearch = remoteSearch // REMOTE!!!!!!!!!!!!!!!!!!!!!!



class WebSearchService {
  constructor() {
  }

  search(term, page, engines) {
    if (!term || term.length < MIN_SEARCH_LEN)
      return

    term = spaceToPlus (term);
    if (!page)
      page = 1

    // TODO get engines from params if needed
    // engines = 'google=true&gi=true&amazon=true&yelp=true'
    engines = 'google=true&gi=true'

    let urlString = currSearch + 'search?' +engines + "&page=" +page+ '&q=' + encodeURIComponent( term)

    let self = this;

    return new Promise( (resolve, reject) => {
        axios ({
            method: 'GET',
            url: urlString,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }).then (response => {
/*
            let parts
            response.data.forEach( provider => {
                debugger
                provider.items.map (item => {
                    debugger
                    if (item.img && item.img.url)
                        item.img.url = this.stripQueryParams(item.img.url)
                    if (item.thumbnail && item.thumbnail.url)
                        item.thumbnail.url = this.stripQueryParams(item.thumbnail.url)
                })
            })
*/
            resolve( response )
        })
    })
  }

    stripQueryParams = (url) => {
        let parts = getUrlComponents (url)
        if (parts.href && parts.search)
            url = parts.href.replace (parts.search, '')
        return url
    }

}

export default new WebSearchService ()

