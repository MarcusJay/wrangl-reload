import axios from 'axios'

import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import userStore from '/store/userStore'
import {
    categoryCreate, categoryDelete, categoryUpdate, moveCategoryUrl, moveProjectCategoryUrl,
    projectTagDelete,
    userCategoriesRead
} from "../config/ApiEndpoints";


export function getUserCategories(userId) {
    if (!userId && !userStore.currentUser)
        throw new Error ('getUserTags: no currentUser!')

    if (!userId && userStore.currentUser)
        userId = userStore.currentUser.id

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: userCategoriesRead,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            user_id: userId
        }
    })
}

export function createCategory(title, userId) {
    if (!title)
        throw new Error ('createCategory: no title')

    if (!userId)
        userId = userStore.currentUser.id

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: categoryCreate,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            user_id: userId,
            title: title
        }
    })
}

export function updateCategory(cag, userId) {
    if (!cag)
        throw new Error ('updateCategory: no title')

    if (!userId)
        userId = userStore.currentUser.id

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: categoryUpdate,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            user_id: userId,
            title: cag.title,
            category_id: cag.id
        }
    })
}

export function deleteCategory(categoryId, userId) {
    if (!categoryId)
        throw new Error ('deleteCategory: missing category')

    if (!userId)
        userId = userStore.currentUser.id

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: categoryDelete,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            category_id: categoryId,
            user_id: userId
        }
    })
}

export function moveCategory(catId, destIdx) {
    if (!catId || destIdx === undefined) {
        console.error ('move cat: missing dest cat or project')
    }

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: moveCategoryUrl,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            user_id: userStore.currentUser.id,
            category_id: catId,
            sort_index: destIdx
        }
    })
}

export function moveProjectCategory(toCatId, projectId) {
    if (!toCatId || !projectId) {
        console.error ('move project cat: missing dest cat or project')
    }

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: moveProjectCategoryUrl,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            project_id: projectId,
            destination_category_id: toCatId,
            user_id: userStore.currentUser.id,
            sort_index: 0
        }
    })
}


// export default new CategoryApiService()