import {Component} from 'react'
import {Link} from '~/routes'
import {User} from 'react-feather'
import {toast} from 'react-toastify'

import userStore from "/store/userStore"
import projectStore from "/store/projectStore"
import cardStore from "/store/cardStore"

const ANON = 'No User ID'

class Toaster {
    info(msg, event, count, icon) {
        this.toast (msg, event, count, 'info', icon)
    }

    warn(msg, event, count) {
        this.toast (msg, event, count, 'warn')
    }

    error(msg, event, count) {
        this.toast (msg, event, count, 'error')
    }

    toast(msg, event, count, type, icon) {
        if (!msg) {
            console.error('Missing Toast.')
            return
        }

        if (!event) {
            if (!type || type === 'info')
                toast(msg)
            else
                toast[type](msg)
            return
        }
        let user, card, project
        user = (event.user === undefined && event.userId)? userStore.getUserFromConnections(event.userId) : event.user
        if (!user)
            return

        // debugger
        card = event.card || event.cardId? cardStore.getCardFromCurrent(event.cardId) : null
        project = event.project || event.projectId? projectStore.getProjectFromUserProjects(event.projectId) : null

        const userIdx = msg.indexOf('[user]')
        const postUserMsg = userIdx >=0? msg.substring( userIdx + 6 ) : msg
        const iconIdx = 0 // postUserMsg.indexOf('[icon]')
        icon = null
        // debugger

        const body = (
            <span className='toastBody'>
              <Link route='project' params={{id: project.id}}>
                <span className='event vaTop'>
                    {user && user.image &&
                    <img src={user.image} className='member vaTop'/>
                    }
                    {(!user || (user && !user.image)) &&
                    <User size={28} className='eventIcon' />
                    }

                    <span className="msg">
                        {user && user.id &&
                        <span className='userLink hoh'>
                            {user.displayName}
                        </span>
                        }
                        {(!user || !user.id) &&
                        <span>{ANON}</span>
                        }

                        {icon === null &&
                        postUserMsg +'.'
                        }
                        {icon !== null &&
                        <span >
                        {postUserMsg.substring(0, iconIdx)}
                            {icon}
                        {postUserMsg.substring(iconIdx + 6)}
                        </span>
                        }

                        {project && count === 1 &&
                          <div className='projLink hoh'>{project.title}</div>
                        }

                    </span>
                </span>
              </Link>
            </span>
        )
        if (!type || type === 'info')
            toast(body)
        else
            toast[type](body)
    }
}

export default new Toaster()