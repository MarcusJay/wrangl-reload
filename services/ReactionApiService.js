import axios from 'axios'

import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import userStore from '/store/userStore'
import projectStore from '/store/projectStore'
import {THUMBS_DOWN, THUMBS_UP} from "/config/ReactionConfig"
import {STARS, THUMBS} from "../config/ReactionConfig";
import {
    commentReactionCreate,
    commentReactionDelete, reactionCreate, reactionDelete, reactionSetReadDefaults,
    reactonSetRead
} from "../config/ApiEndpoints";
import {APPROVE, RATE, UserRole} from "../config/Constants";

let thumbsUpId = 0
let thumbsDownId = 0
let starsId = 0

// This service is stateful and contains import constants. Don't deconstruct.
class ReactionApiService {
    defaultReactionSets = []
    currentReactionSet = null
    initialized = false

    getDefaultReactionSets() {
        return new Promise((resolve, reject) => {
            if (this.defaultReactionSets && this.defaultReactionSets.length > 0)
                resolve(this.defaultReactionSets)
            else {
                axios ({
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    },
                    url: reactionSetReadDefaults,
                    data: {
                        [ApiTempKeyName]: ApiTempKeyValue
                    }
                }).then (response => {
                    if (response && response.data) {
                        this.defaultReactionSets = response.data.default_reaction_sets
                        this.currentReactionSet = this.defaultReactionSets.find( rs => rs.reaction_set_title.startsWith('Thumbs') )
                        this.starsRS = this.defaultReactionSets.find( rs => rs.reaction_set_title.startsWith('Stars') )
                        const thumbsUp = this.currentReactionSet.types.find( rt => rt.reaction_name === THUMBS_UP )
                        if (thumbsUp)
                            thumbsUpId = thumbsUp.reaction_type_id
                        const thumbsDown = this.currentReactionSet.types.find( rt => rt.reaction_name === THUMBS_DOWN )
                        if (thumbsDown)
                            thumbsDownId = thumbsDown.reaction_type_id
                        const stars = this.starsRS && this.starsRS.types && this.starsRS.types.length > 0? this.starsRS.types[0] : null
                        if (stars)
                            starsId = stars.reaction_type_id
                        this.initialized = true
                        resolve (this.defaultReactionSets)
                    }
                }).catch (error => {
                    console.error (error)
                    reject (error)
                })
            }
        })
    }

    setDefaultReactionSets(rsets) {
        this.defaultReactionSets = rsets
        this.currentReactionSet = this.defaultReactionSets.find( rs => rs.reaction_set_title.startsWith('Thumbs') )
    }

    getReactionSetById(reactionSetId) {
        if (!reactionSetId) {
            debugger
            console.error('Error getReactionSetById received NO ID')
        }
        return new Promise((resolve, reject) => {
            axios({
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                url: reactonSetRead,
                data: {
                    [ApiTempKeyName]: ApiTempKeyValue,
                    "reaction_set_id": reactionSetId
                }
            }).then(response => {
                if (response && response.data) {
                    resolve (response.data)
                }
            }).catch(error => {
                console.error(error)
                reject(error)
            })
        })
    }

    getStarsReactionTypeId() {
        const stars = this.getReactionSetByTitleFromDefaults(STARS)
        return stars? stars.types[0].reaction_type_id : 0
    }

    registerStarReaction(cardId, value, projectId) {
        return this.registerReaction(cardId, this.getStarsReactionTypeId(), value, projectId)
    }

    registerVoteReaction(cardId, voteType, projectId) {
        const voteTypeId = voteTypeToId(voteType)
        return this.registerReaction(cardId, voteTypeId, null, projectId)
    }

    registerReaction(cardId, reactionTypeId, reactionValue, projectId) {

        if (!projectId && projectStore.currentProject)
            projectId = projectStore.currentProject.id

        let payload = {
            [ApiTempKeyName]: ApiTempKeyValue,
            "card_id": cardId,
            "user_id": userStore.currentUser.id,
            "reaction_type_id": reactionTypeId
        }

        if (reactionValue !== undefined && reactionValue !== null)
            payload['reaction_value'] = reactionValue

        if (projectId)
            payload['project_id'] = projectId

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: reactionCreate,
            data: payload
        })
    }

    unregisterReaction(card, reactionTypeId, reactionId) {

        // if no reactionId avail, find it.
        if (reactionTypeId && !reactionId) {
            reactionId = this.getUserReactionByType(card, reactionTypeId)
        }

        if (!reactionId)
            throw new Error('unregisterReaction: cannot determine reactionId')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: reactionDelete,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "reaction_id": reactionId,
                "user_id": userStore.currentUser.id
            }
        })
    }

    registerReactionExclusive(card, reactionTypeId, reactionValue) {
        let xr = this.getOpposingReaction(reactionTypeId)
        // if no reactionId avail, find it.
        if (reactionTypeId && !reactionId) {
            reactionId = this.getUserReactionByType(card, reactionTypeId)
        }
    }

    // note: non-api calls. Move to ReactionUtils?
    getReactionSetByTitleFromDefaults(rsTitle) {
        return this.defaultReactionSets.find (rs => rs.reaction_set_title === rsTitle) || null
    }

    // note: non-api calls. Move to ReactionUtils?
    getReactionSetByIdFromDefaults(rsId) {
        return this.defaultReactionSets.find (rs => rs.reaction_set_id === rsId) || null
    }

    // note: non-api calls. Move to ReactionUtils?
    getReactionByTypeId(typeId) {
        let reaction = null
        for (let i=0; i<this.defaultReactionSets.length; i++) {
            reaction = this.defaultReactionSets[i].types.find( type => type.reaction_type_id === typeId )
            if (reaction)
                return reaction
        }
        return reaction
    }

    // note: non-api calls. Move to ReactionUtils?
    // type is optional. if no type, return all.
    getCardReactionsByType(card, reactionTypeId) {
        if (!card)
            throw new Error("getCardReactionsByType: missing card")

        if (!card.reactions)
            return []

        if (reactionTypeId)
            return card.reactions.filter( reaction => reaction.reaction_type_id === reactionTypeId ) || []
        else
            return card.reactions
    }

    // note: non-api calls. Move to ReactionUtils?
    countCardReactions(card, reactionTypeId) {
        if (!card)
            throw new Error("getCardReactionsByType: missing card")
        if (!reactionTypeId)
            return card.reactions.length
        else
            return this.getCardReactionsByType(card, reactionTypeId).length
    }

    // note: non-api calls. Move to ReactionUtils?
    // type is optional
    getUserReactionByType(card, reactionTypeId, reactionUser) {
        if (!card)
            return 'none' // throw new Error("getCardReactionsByType: missing card")

        let user = reactionUser || userStore.currentUser
        if (!user)
            return 'none'
        let reactions = this.getCardReactionsByType(card, reactionTypeId)
        reactions.sort( (d1,d2) => d2 - d1 )
        let userReaction
        if (reactionTypeId)
            userReaction = reactions.find( reaction => reaction.user_id === user.id && reaction.reaction_type_id === reactionTypeId) || 'none'
        else
            userReaction = reactions.find( reaction => reaction.user_id === user.id) || 'none'
        return userReaction
    }

    // note: non-api calls. Move to ReactionUtils?
    // if no reactionTypeId, return true if user has reacted at all to card
    hasUserReacted(card, reactionTypeId, user) {
        return (this.getUserReactionByType(card, reactionTypeId, user) !== 'none')
    }

    // parse a reaction summary as returned by card reactions read
    countUpVotesFromSum(reactionSummary) {
        return this.countVotesFromSum(reactionSummary, THUMBS_UP)
    }
    countDownVotesFromSum(reactionSummary) {
        return this.countVotesFromSum(reactionSummary, THUMBS_DOWN)
    }

    countVotesFromSum(reactionSummary, voteType) {
        const voteTypeId = voteTypeToId(voteType)

        if (!reactionSummary || voteTypeId === 0)
            return 0

        const votes = reactionSummary.reaction_types.find (r => r.reaction_type_id === voteTypeId)
        return votes? (votes.count || 0) : 0
    }

    rsIdToReviewType(rsId) {
        const stars = this.getReactionSetByTitleFromDefaults(STARS)
        const thumbs = this.getReactionSetByTitleFromDefaults(THUMBS)
        if (rsId === stars.reaction_set_id)
            return RATE
        else if (rsId=== thumbs.reaction_set_id)
            return APPROVE
        else
            return -1
    }

    // inverse of above
    reviewTypeToRSId(reviewType) {
        if (reviewType === STARS)
            return this.getReactionSetByTitleFromDefaults(STARS).reaction_set_id
        else if (reviewType=== THUMBS)
            return this.getReactionSetByTitleFromDefaults(THUMBS).reaction_set_id
        else
            return null
    }

    // return most recent star rating for card
    getLastStarRating(card) {
        if (!card || !card.reactions || card.reactions.length === 0)
            return null

        const lastReaction = card.reactions[card.reactions.length-1]
        if (lastReaction.reaction_value !== undefined)
            return lastReaction.reaction_value
        else
            return null
    }

    getStarsId = this.getStarsReactionTypeId

    getThumbsUpId() {
        return thumbsUpId
    }
    getThumbsDownId() {
        return thumbsDownId
    }

}

export default new ReactionApiService()

export function voteTypeToId(voteType) {
    if (voteType === THUMBS_UP)
        return thumbsUpId
    else if (voteType === THUMBS_DOWN)
        return thumbsDownId
    else if (voteType === STARS)
        return starsId
    else
        return 0
}

export function voteTypeIdToCollectionName(voteTypeId) {
    if (voteTypeId === thumbsUpId)
        return 'approvers'
    else if (voteTypeId === thumbsDownId)
        return 'rejectors'
    else if (voteTypeId === starsId)
        return 'raters'
    else
        return null
}


export function voteTypeIdToAPName(voteTypeId, starValue) {
    if (voteTypeId === thumbsUpId)
        return 'approved'
    else if (voteTypeId === thumbsDownId)
        return 'rejected'
    else if (voteTypeId === starsId)
        return 'rated' // +starValue pending API support. Event should contain reaction VALUE.
    else
        return null
}

export function countCardVotes(card) {
    if (!card || !card.reactions)
        return {up: 0, down: 0}

    const upVotes = card.reactions.filter (r => r.reaction_type_id === thumbsUpId) || []
    const downVotes = card.reactions.filter (r => r.reaction_type_id === thumbsDownId) || []
    return {up: upVotes.length, down: downVotes.length}
}

export const registerCommentReaction = (commentId, reaction) => {

    if (!commentId || reaction)
        throw new Error('missing params')

    return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: commentReactionCreate,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            comment_id: commentId,
            reaction: reaction
        }
    })
}

export const unregisterCommentReaction = (commentId, reaction) => {

    if (!commentId || reaction)
        throw new Error('missing params')

    return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: commentReactionDelete,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            comment_id: commentId,
            reaction: reaction
        }
    })
}
