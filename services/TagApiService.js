import axios from 'axios'

import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import userStore from '/store/userStore'
import projectStore from '/store/projectStore'
import ColorUtils from '/utils/ColorUtils'
import {truncate} from "../utils/solo/truncate";
import {
    cardableTagAdd, cardableTagDelete, cardableTagRead, cardSearchTags, cardTagAdd, cardTagDelete, cardTagsRead,
    projectTagAdd, projectTagDelete, tagCreate, tagUpdate, userLibTagsDelete,
    userLibTagsRead
} from "../config/ApiEndpoints";

// Mobx store is sole client of this service, therefore singleton class is fine, no need to deconstruct.
class TagApiService {


    // 1. USER TAGS
    createUserTag(name, color, userId) {
        if (!name )
            throw new Error('createUserTag: no tag name')
        if (!color)
            color = ColorUtils.getNextColor()
        if (color.startsWith('#'))
            color = color.substring(1)

        if (!userId)
            userId = userStore.currentUser.id

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: tagCreate,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                tag_creator: userId,
                tag_text: name,
                tag_color: color
            }
        })
    }

    updateUserTag(tag) {
        if (!tag)
            throw new Error('updateUserTag: no tag')
        if (tag.color.startsWith('#'))
            tag.color = tag.color.substring(1)

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: tagUpdate,
            data: Object.assign (this.tagToApiPayload(tag), {[ApiTempKeyName]: ApiTempKeyValue})
        })
    }

    getUserTags(userId) {
        if (!userId && !userStore.currentUser)
            throw new Error('getUserTags: no currentUser!')


        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userLibTagsRead,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                user_id: userId || userStore.currentUser.id
            }
        })
    }

    deleteUserTag(tag, cascadeToProjects) {
        if (!tag)
            throw new Error('deleteUserTags: no tags')
        if (!cascadeToProjects)
            cascadeToProjects = false

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userLibTagsDelete,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                tag_id: tag.id,
                user_id: userStore.currentUser.id,
                delete_from_projects: cascadeToProjects
            }
        })
    }

    // DeleteUserTags PreFlight: Is Tag In Use?
    getProjectsWithTag(tag) {
        if (!tag)
            throw new Error('deleteUserTags: no tags')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userLibTagsDelete,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                tag_id: tag.id,
                user_id: userStore.currentUser.id
            }
        })
    }

    // 2. Project Level Tags
    tagProject(tagId, projectId) {
        if (!tagId || !projectId)
            throw new Error('tagProject: missing tag or project')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectTagAdd,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                project_id: projectId,
                user_id: userStore.currentUser.id,
                tag_id: tagId
            }
        })
    }

    untagProject(tagId, projectId) {
        if (!tagId || !projectId)
            throw new Error('untagProject: missing tag or project')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectTagDelete,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                project_id: projectId,
                user_id: userStore.currentUser.id,
                tag_id: tagId
            }
        })
    }

    getProjectTags(projectId) {
        if (!projectId)
            throw new Error('getProjectTags: missing tag or project')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: cardableTagRead,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                project_id: projectId,
                user_id: userStore.currentUser.id
            }
        })
    }


    // 3. Cardable Sets ('Project Tag Sets')
    addTagToCardableSet(tagId, projectId) {
        if (!tagId || !projectId)
            throw new Error('addTagToCardableSet: missing tag or project')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: cardableTagAdd,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                project_id: projectId,
                tag_id: tagId,
                user_id: userStore.currentUser.id
            }
        })
    }

    deleteTagFromCardableSet(tagId, projectId) {
        if (!tagId || !projectId)
            throw new Error('deleteTagToCardableSet: missing tag or project')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: cardableTagDelete,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                project_id: projectId,
                tag_id: tagId,
                user_id: userStore.currentUser.id
            }
        })
    }

    getProjectTagSet() {
        // get 'project_tag_set_2' from project read
    }

    // 4. Cards
    tagCard(tagId, cardId) {                            // TAG A CARD
        if (!tagId || !cardId) {
            debugger
            throw new Error('tagCard: missing tag or card')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: cardTagAdd,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                card_id: cardId,
                tag_id: tagId,
                user_id: userStore.currentUser.id
            }
        })
    }

    untagCard(tagId, cardId) {
        // cardTagDelete: baseUrl + 'card_tag_delete.php',                     // UNTAG A CARD
        if (!tagId || !cardId)
            throw new Error('untagCard: missing tag or project')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: cardTagDelete,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                card_id: cardId,
                tag_id: tagId,
                user_id: userStore.currentUser.id
            }
        })
    }

    getCardTags(cardId) {
        if (!cardId)
            throw new Error('getCardTags: missing card')

        let payload = {
            [ApiTempKeyName]: ApiTempKeyValue,
            card_id: cardId
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: cardTagsRead,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                card_id: cardId
            }
        })
    }

    findCardsByTags(tagIds, projectId) {
        if (!tagIds || !userStore.currentUser)
            throw new Error('findCardsByTags: missing tagIds or user')

        // const scopeField = projectId? 'project_id' : 'user_id'
        if (!projectId && projectStore.currentProject)
            projectId = projectStore.currentProject.id

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: cardSearchTags,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                tags: tagIds,
                project_id: projectId,
                user_id: userStore.currentUser.id
            }
        })
    }

    tagToApiPayload(tag) {
        let apiTag = {}
        if (tag.id)
            apiTag.tag_id = tag.id
        if (tag.name)
            apiTag.tag_text = truncate( tag.name.trim().replace(/\s+/g, ' ').replace('\n', ''), 63 )
        if (tag.color)
            apiTag.tag_color = tag.color
        return apiTag
    }

}
export default new TagApiService()