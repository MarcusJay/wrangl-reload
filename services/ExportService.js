import axios from 'axios'
import b64toBlob from 'b64-to-blob'

import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import userStore from '/store/userStore'
const utilsRemote = "https://utils.pogo.io/"
const utilsLocal = "http://localhost:8205/"

const currUtils = utilsRemote // !!!!!!!!!!!!!!!!!!!!!!!!!! REMOTE !!!!!!!!!!!!!!!!!!!!!!!!!


class ExportService {

    getPDFBlobUrl(project) {
        return new Promise( (resolve, reject) => {
            this.requestPDF(project).then( response => {
                let blob = b64toBlob(response.data, 'application/pdf')
                const pdfUrl = URL.createObjectURL(blob)
                resolve(pdfUrl)
            }).catch( error => {
                reject(error)
            })
        })
    }

    requestPDF(project) {
        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            url: currUtils + 'exportPDF',
            data: {
                projectId: project.id,
                userId: userStore.currentUser.id
            }
        })
    }


}

export default new ExportService()