import axios from 'axios'

import userStore from '/store/userStore'
import projectStore from '/store/projectStore'
import {env} from "../config/env";

const localEmailer = "http://localhost:8202"
const oldEmailer = "https://smail.wran.gl"
const remoteEmailer = "https://mail.pogo.io"

// Set for current environment
const currentEmailer = remoteEmailer // REMOTE!

const defaultInviteSubject = "Please join my Wrangl."

const EMAIL_INVITE = "/invite"
const EMAIL_AP_REQUEST = "/approve"
const EMAIL_RESET = "/reset"
const EMAIL_WELCOME = "/welcome"
const EMAIL_INTERNAL = "/internal"
// const EMAIL_...

// TODO from my oop days. let go of class
export default class EmailService {
    static sendInvite({recipientEmail, recipientName, recipientPW, senderName, senderId,
                       projectId, imageUrl, isAP, itemCount, apType, apDesc}) {
        if (!recipientEmail) {
            console.error("invite: Missing params. Not enough info to invite. ")
            return
        }

        if (!senderId && userStore.currentUser) {
            senderId = userStore.currentUser.id
            senderName = userStore.currentUser.displayName
        }

        let project = projectId?
            projectStore.getProjectFromUserProjects(projectId)
            : projectStore.currentProject

        if (!imageUrl && project)
            imageUrl = project.image

        const projectUrl = project? (isAP? env.getAPUrl(projectId) : env.getProjectUrl(projectId)) : env.current.url


        let emailParams = {
            sender: senderName,
            senderId: senderId,
            firstname: recipientName,
            password: recipientPW,
            recipient: recipientEmail,
            wranglName: project? project.title : 'Pogo.io',
            wranglUrl: projectUrl,
            imageUrl: imageUrl,
            isAP: isAP === undefined? false : isAP,
            itemCount: itemCount === undefined? 0 : itemCount,
            apType: apType === undefined? 'approval' : apType,
            wranglDesc: apDesc || project? project.description : null
        }

        const action = isAP? EMAIL_AP_REQUEST : EMAIL_INVITE
        return EmailService._sendToEmailServer(emailParams, action)
    }

    static sendWelcome(recipient, firstname) {
        if (!recipient) {
            console.error("Missing recipient info. Not enough info to invite. ")
            return
        }

        let emailParams = {
            firstname: firstname,
            recipient: recipient,
        }

        EmailService._sendToEmailServer(emailParams, EMAIL_WELCOME)
    }

    static sendPasswordReset(userInfo) {
        if (userInfo === null || userInfo === undefined) {
            console.error("Missing params.")
            return
        }

        let emailParams = {
            recipient: userInfo.email,
            code: userInfo.reset_code,
            id: userInfo.user_id,
            name: userInfo.display_name,
            date: userInfo.time_modified
        }

        return EmailService._sendToEmailServer(emailParams, EMAIL_RESET)
    }


    static _sendToEmailServer(emailParams, action) {
        let request = currentEmailer + action

        // TODO should email server use idToken?
        return axios({
            method: 'POST',
            url: request,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            data: emailParams
        })

    }

    static sendError(errorData) {
        axios({
            method: 'POST',
            url: currentEmailer + EMAIL_INTERNAL,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            data: errorData,
        }).then(response => {
            console.log('Response from email server: ' + response)

        }).catch(function(error) {
            console.error("Error from email server: ", error );
        });
    }


}
