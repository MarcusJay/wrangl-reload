import axios from 'axios'

import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import userStore from '/store/userStore'
import projectStore from '/store/projectStore'
import {truncate} from "../utils/solo/truncate";
import {isImageFromMime, isImageFromName} from "../utils/solo/isImageFile";
import {uploadAttachment} from "./AttachmentService";
import {getExt} from "../utils/solo/getExt";
import normalizeUrl from 'normalize-url'
import {
    cardClone,
    cardCreate,
    cardDelete,
    cardMove,
    cardOrderRead,
    cardRead,
    cardReadReactions,
    cardsByUser,
    cardsCloneIntoProject,
    cardsCreate,
    cardsDelete,
    cardSearch,
    cardUpdate
} from "../config/ApiEndpoints";
import {sendImage} from "./ImageFileService";

// class CardApiService {
//   constructor() {
//   }

/**
 *
 * @param cardId
 * @returns {Promise} a single card object.
 */
// TODO you can probably unwrap this since it's been changed to singular card
export function getCardDetails(cardId, projectId) {
    if (!cardId) {
        throw new Error ('getCardById: missing cardId')
    }
    if (!projectId && projectStore.currentProject)
        projectId = projectStore.currentProject.id

    const payload = {
        [ApiTempKeyName]: ApiTempKeyValue,
        "card_id": cardId
    }

    if (projectId)
        payload['project_id'] = projectId

    return new Promise ((resolve, reject) => {
        axios ({
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: cardRead,
            data: payload
        }).then (response => {
            if (response && response.data) {
                resolve (response.data)
            }
        }).catch (error => {
            reject (error)
        })
    })
}

export function getCardReactions(cardId, details) {
    const payload = {
        [ApiTempKeyName]: ApiTempKeyValue,
        "card_id": cardId,
        "details": details
    }

    const projectId = projectStore.currentProject ? projectStore.currentProject.id : null
    if (projectId)
        payload['project_id'] = projectId
    if (userStore.currentUser)
        payload['user_id'] = userStore.currentUser.id

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardReadReactions,
        data: payload
    })
}

/*
export function getCardSortOrder(projectId) {
    if (!userStore.currentUser || !projectId)
        throw new Error ('order endpoint requires userId and projectId! ')

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardOrderRead,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            "project_id": projectId,
            "user_id": userStore.currentUser.id
        }
    })
}
*/

/**
 *
 * @param userId
 * @returns {AxiosPromise} format { cards: [cardId, cardId, ... cardId] }
 */
export function getUserCards(userId) {
    console.log ("GetUserCards: userId = " + userId + ", currentUser.id = " + (userStore.currentUser ? userStore.currentUser.id : 'none'))
    if (!userId) {
        if (!userStore.currentUser)
            throw new Error ('getUserCards: no userId or currentUser')
        else
            userId = userStore.currentUser.id
    }

    return axios ({
        method: 'POST',
        headers: {
            // 'Content-Type': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardsByUser,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            "user_id": userId
            // "card_end": 500
        }
    })
}

/* alias */
// export const getMyCards = getUserCards

/**
 *
 * @param query search term
 * @param projectId. If none, search all of user's cards
 */
export function findCards(query, oldTagNames, projectId) {
    if (!query && !oldTagNames)
        throw new Error ('findCards: missing query or tags')

    let payload = {
        [ApiTempKeyName]: ApiTempKeyValue,
    }

    if (query)
        payload["search_string"] = query.toLowerCase ()
    else if (oldTagNames) {
        payload["tags"] = Array.isArray (oldTagNames) ? oldTagNames : [oldTagNames]
    }
    else
        throw new Error ('findCards: Specify one of: query or tags')

    if (projectId)
        payload['project_id'] = projectId
    else
        payload['user_id'] = userStore.currentUser.id

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardSearch,
        data: payload
    })

}

export const searchCards = findCards

/**
 *
 * @param projectId
 * @param cardModel
 * @returns {AxiosPromise} A single card object (see apiCard fields in CardModel), with the addition of bad members "not_found": [ userId, userId,...]
 */
export function saveCard(projectId, cardModel) {
    // if (!projectId && cardModel.id === 'new')
    //   throw new Error ('createCard: No current project in which to add new card.')

    const isNew = cardModel.id === 'new'
    let endpoint = isNew ? cardCreate : cardUpdate
    let payload = Object.assign (cardToApiPayload (cardModel), {
        [ApiTempKeyName]: ApiTempKeyValue
    })

    if (isNew) { // new cards get extra info that update endpoint rejects
        payload['user_id'] = userStore.currentUser.id
        payload['project_id'] = projectId
    }
    else {
        payload['user_id'] = userStore.currentUser.id
    }

    if (cardModel.url)
        payload['card_url'] = normalizeUrl (cardModel.url)

    return axios ({
        method: 'POST',
        headers: {
            // 'Content-Type': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: endpoint,
        data: payload
    })
}

/**
 *
 * @param cardModel
 * @returns {AxiosPromise} a single card object (see apiCard fields in CardModel). Does not include cards.
 */
/*
export function updateCard(cardModel) {
    if (!cardModel) {
        throw new Error ('updateCard: missing card')
    }

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardUpdate,
        data: Object.assign (this.cardToApiPayload (cardModel), {[ApiTempKeyName]: ApiTempKeyValue})
    })

}
*/

export function deleteImage(cardId) {
    return this.saveCard (projectStore.currentProject.id, {id: cardId, image: 'DELETE'})
}

export function moveCard(cardModel, destIndex) {
    if (!cardModel || destIndex === undefined || destIndex === null) {
        throw new Error ('moveCard: no card or no destination')
    }

    if (!projectStore.currentProject)
        throw new Error ('moveCard: no current project')

    const p = projectStore.currentProject
    if (destIndex > p.cardTotal)
        throw new Error ('moveCard: destIndex out of bounds')

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardMove,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            card_id: cardModel.id,
            user_id: userStore.currentUser.id,
            insert_index: destIndex,
            source_project_id: p.id
        }
    })

}

export function tagCard(cardId, tag) {
    if (!cardId || !tag || tag.length === 0)
        throw new Error ('tagCard: missing tag or card Id')

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardUpdate,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            card_id: cardId,
            add_tags: [tag]
        }
    })
}

// unused
/*
export function cloneCard(cardId, userId, projectId) {
    if (!cardId || !userId)
        throw new Error ('cloneCard: missing params')

    if (!projectId && projectStore.currentProject)
        projectId = projectStore.currentProject.id

    if (!projectId)
        throw new Error ('cloneCard: no project id')

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardClone,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            card_id: cardId,
            user_id: userId
        }
    })

}
*/

export function cloneCardsIntoProject(cardIds, project) {
    if (!cardIds || cardIds.length === 0 || !project)
        throw new Error ('cloneCardsIntoProject: missing cards or project')
    // let cardIds = cardIds.map( card => card.id )

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardsCloneIntoProject,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            card_ids: cardIds,
            user_id: userStore.currentUser.id,
            project_id: project.id
        }
    })
}

export function createCardsFromAssets(assets, project, source) {
    if (!assets || assets.length === 0 || !project)
        throw new Error ('createCardsFromAssets: missing cards or project')

    let apiCards = (source === 'dropbox') ? assets.map (asset => dropboxToApiPayload (asset)) : assets.map (asset => assetToApiPayload (asset))

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardsCreate,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            cards: apiCards,
            user_id: userStore.currentUser.id,
            project_id: project.id
        }
    })
}

// Note: we could definitely combine with above to be more DRY. However, until endpoints and properties are finalized, I'm preferring clean separation.
export function createCardsFromFiles(files, project, source) {
    if (!files || files.length === 0 || !project)
        throw new Error ('missing cards or project')

    let apiCards = files.map (file => fileToApiPayload (file))
    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardsCreate,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            cards: apiCards,
            user_id: userStore.currentUser.id,
            project_id: project.id
        }
    })
}

export function attachFilesToCards(cards, fileRefs, callback) {
    if (!cards || cards.length === 0 || !fileRefs || fileRefs.length === 0)
        throw new Error ('missing cards or images')
    let cardsPayload, matchingFile
    cards.forEach ((card, i) => {

        // TODO replace the following title match with a check for the actual blob url, which needs to be stored in original card field. api needs to support.
        matchingFile = fileRefs.find (fileRef => card.card_title === fileRef.name)
        //

        if (matchingFile) {
            const isImage = isImageFromName (matchingFile.name) || isImageFromMime (matchingFile.type)
            if (isImage)
                sendImage (matchingFile, false, callback, card.card_id, i + 1)
            else {
                uploadAttachment (matchingFile, getExt (matchingFile.name), card.card_id, callback, card.card_description, i + 1)
                /*
                                  // if new card or card has no image, use attm preview
                                  if (!card.image)
                                      ImageFileService.sendBlobUrl(matchingFile.preview, callback, card.card_id, i + 1)
                */
            }
        }
        else
            console.warn ("No matching file found for card: " + card.title)

    })
}

/**
 *
 * @param cardId to delete
 * @returns {Promise} { success: true }
 */
export function deleteCard(cardId, projectId) {
    if (!projectId) {
        console.error ('Delete card: missing params')
        return
    }

    return axios ({
        method: 'POST',
        headers: {
            // 'Content-Type': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardDelete,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            "card_id": cardId,
            "project_id": projectId,
            "user_id": userStore.currentUser.id
        }
    })
}

export function deleteCards(cardIds, projectId) {
    if (!projectId || !cardIds || cardIds.length === 0) {
        console.error ('deleteCards: missing params')
        return
    }

    return axios ({
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: cardsDelete,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            "card_ids": cardIds,
            "project_id": projectId,
            "user_id": userStore.currentUser.id
        }
    })
}

export function cardToApiPayload(card) {
    let apiCard = {}
    if (card.id !== 'new') {
        apiCard.card_id = card.id
    }

    if (card.title)
        apiCard.card_title = truncate (card.title.trim ().replace (/\s+/g, ' ').replace ('\n', ''), 63)
    if (card.description)
        apiCard.card_description = card.description
    if (card.image)
        apiCard.card_image = card.image
    if (card.imageRotation)
        apiCard.card_image_rotation = card.imageRotation
    if (card.indexLtr)
        apiCard.card_letter = card.indexLtr

    if (card.position)
        apiCard.insert_index = card.position

    if (card.url)
        apiCard.card_url = card.url

    if (card.source)
        apiCard.card_source = card.source

    if (card.sourceType)
        apiCard.source_type = card.sourceType

    if (card.sourceIcon)
        apiCard.card_source_icon = card.sourceIcon

    if (card.tags && card.tags.length > 0)
        apiCard.add_tags = card.tags
    return apiCard
}

export function assetToApiPayload(asset) {
    let apiCard = cardToApiPayload (asset)
    delete apiCard.card_id
    return apiCard
}

export function dropboxToApiPayload(item) {
    let apiCard = {}
    apiCard.card_title = item.title
    apiCard.card_description = 'Dropbox ' + item.id
    apiCard.card_url = item.path_display
    apiCard.time_modified = item.server_modified
    apiCard.card_source = 'dropbox'
    return apiCard
}

export function fileToApiPayload(asset) {
    let apiCard = cardToApiPayload (asset)
    delete apiCard.card_id
    // need to store blob url somewhere for post-creation reference. use custom property

    delete apiCard.card_image
    // apiCard.card_url = asset.path
    // TODO Temp using description for relative path urls, as api expects absolute
    if (!asset.description)
        apiCard.card_description = asset.path

    return apiCard
}


// export default new CardApiService ()