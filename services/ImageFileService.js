import axios from 'axios'
import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import cardStore from '/store/cardStore'
import userStore from '/store/userStore'
import {
    annotationCreate, annotationDelete, annotationRead,
    imageUpload2, imageUploadBase64,
    imageUploadToCard2, imageUploadToCardBase64, imageUrlUpload,
    imageUrlUploadToCard
} from "../config/ApiEndpoints";

const utilsRemote = "https://utils.pogo.io/"
const utilsLocal = "http://localhost:8205/"

const currUtils = utilsRemote

const types = [
    'JPG',
    'PNG',
    'GIF',
    'TIF',
    'PDF',
    'PSD'
]

/**
 * @See also AttachmentService. Keeping separate initially, combine if benefits outweigh complications.
 */
// class ImageFileService {

    // no
/*
    sendBlobUrlViaCanvas(blobUrl, callback, cardId, count ) {
        const canvas = document.createElement("canvas")
        const context = canvas.getContext("2d")
        context.drawImage(blobUrl, 0, 0)
        const imgData = canvas.toDataURL()
        this.sendFile(imgData, true, callback, cardId, count)
    }
*/

/*
    sendBlobUrl(blobUrl, callback, cardId, count) {
        const self = this
        const xhr = new XMLHttpRequest;
        xhr.responseType = 'blob';

        xhr.onload = function() {
            const recoveredBlob = xhr.response;
            const reader = new FileReader;

            reader.onload = function() {
                const blobData = reader.result;
                self.sendFile(blobData, true, callback, cardId, count)
            };

            reader.readAsDataURL(recoveredBlob);
        };

        xhr.open('GET', blobUrl);
        xhr.send();

    }
*/

export function sendImage (fileOrBinaryData, isBase64, callback, cardId, count ) {
    let req = new XMLHttpRequest()
    let responseImage = null


    req.onloadstart = function (e) {
        callback(count, 'start')
    }
    req.onloadend = function (e) {
        const json = JSON.parse(this.response)
        responseImage = json.card_image || json.image
        callback(count, 'end', responseImage)
    }

    req.onreadystatechange = function(e, arg) {
        console.log("State changed to : " +req.readyState)
        if (req.readyState === XMLHttpRequest.DONE && req.status === 200) {
            console.log("Upload done. Response url is: " +this.response)
            const json = JSON.parse(this.response)
            responseImage = json.card_image || json.image
            callback(count, responseImage)
        }
    }

    let blob, endpoint
    if (cardId) {
        if (cardId.length !== 36)
            throw new Error('Attempt to call imageUploadToCard but card ID is ' +cardId)
        blob = new Blob ([ApiTempKeyValue, cardId, fileOrBinaryData])
        endpoint = isBase64? imageUploadToCardBase64 :imageUploadToCard2
    }
    else {
        blob = new Blob([ApiTempKeyValue, fileOrBinaryData])
        endpoint = isBase64? imageUploadBase64 : imageUpload2
    }

    req.open("POST", endpoint, true);
    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    req.send(blob)
}

export function sendURL(url, cardId) {
    if (!url)
        throw new Error('sendUrl: missing URL')

    let endpoint = cardId? imageUrlUploadToCard : imageUrlUpload
    let payload = {
        [ApiTempKeyName] : ApiTempKeyValue,
        url: url
    }
    if (cardId) {
        payload['card_id'] = cardId
    }

    return axios({
        method: 'POST',
        headers: {
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: endpoint,
        data: payload
    })

}

    // assumption: the cards received here have image populated, if any, but not attms yet.
export function requestPreviews(apiCards) {
    const self = this
    let utilCalls = [], apiCalls = [], previewBlob

    apiCards.forEach( apiCard => {
        if (!apiCard.card_image) {
            apiCalls.push (cardStore.getCardById(apiCard.card_id))
        }
    })

    Promise.all( apiCalls ).then( cards => {
        cards.forEach( (card,i) => {
            if (card.attachments && card.attachments.length > 0)
                getFilePreview(card.attachments[0].url).then( response => {
                    previewBlob = response.data
                    if (previewBlob !== null)
                        sendImage(previewBlob, true, (count, msg, imageUrl) => updateCard(count, msg, imageUrl, card), card.id, i +1)
                    else
                        console.error("Received empty preview for card " +card.id)
                })
        })
    })


}

export function getFilePreview(fileUrl) {
    return axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        url: currUtils + 'preview',
        data: {
            "fileUrl": fileUrl
        }
    })
}

export function updateCard(count, msg, imageUrl, card) {
    if (msg === 'end') {
        if (card)
            cardStore.refreshCard(card.id)
    }
}

export function saveAnnotation(cardId, imageUrl) {
    if (!cardId || !imageUrl || !userStore.currentUser)
        throw new Error('missing params')

    return axios({
        method: 'POST',
        headers: {
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: annotationCreate,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            "card_id": cardId,
            "user_id": userStore.currentUser.id,
            "image_url": imageUrl
        }
    })
}

export function deleteAnnotation({antId, cardId, userId}) {
    if (!antId && !cardId)
        throw new Error('missing params')

    if (!userId)
        userId = userStore.currentUser.id

    let payload= {
        [ApiTempKeyName]: ApiTempKeyValue,
        "user_id": userId
    }
    if (antId)
        payload.annotation_id = antId
    else if (cardId)
        payload.card_id = cardId

    return axios({
        method: 'POST',
        headers: {
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: annotationDelete,
        data: payload
    })
}

export function getCardAnnotations(cardId) {
    if (!cardId || !userStore.currentUser)
        throw new Error('missing params')

    return axios({
        method: 'POST',
        headers: {
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        url: annotationRead,
        data: {
            [ApiTempKeyName]: ApiTempKeyValue,
            "card_id": cardId,
            "user_id": userStore.currentUser.id,
        }
    })
}

