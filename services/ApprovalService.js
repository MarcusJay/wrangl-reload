import axios from 'axios'
import userStore from '/store/userStore'
import projectStore from '/store/projectStore'
import eventStore from "../store/eventStore";
import ProjectApiService from "./ProjectApiService";
import ProjectModel from "../models/ProjectModel";
import {
    ApprovalCompletionType,
    APPROVE,
    ProjectStatus,
    ProjectType,
    RATE,
    SecurityLevel,
    UserRole
} from "../config/Constants";
import {ApiTempKeyName, ApiTempKeyValue, approvalProjectAddCards} from "../config/ApiEndpoints";
import {EventSource} from "../config/EventConfig";
import ReactionApiService from "./ReactionApiService";
import {STARS, THUMBS} from "../config/ReactionConfig";

// Approval Service, all solos, no state

export const getAPFromUserAPs = (apId) => {
    return projectStore.approvalsSent.find( ap => ap.id === apId )
        || projectStore.approvalsReceived.find( ap => ap.id === apId )
        || null
}

export const getUserApprovals = (userId) => {
    return new Promise ((resolve, reject) => {
        const activeUserId = userStore.currentUser.id
        ProjectApiService.getUserProjects(userId, null, ProjectType.APPROVAL).then( response => {
            let approvals = response.data.projects.map (apiProject => new ProjectModel (apiProject) )

            const sent = approvals.filter( project => project && project.creatorId === activeUserId && project.status !== ProjectStatus.CANCELLED) || []
            const received = approvals.filter( project => project.members.find(member => member.id === activeUserId && member.role === UserRole.APPROVER && project.status !== ProjectStatus.CANCELLED) !== undefined) || []
            projectStore.setApprovals(received, sent) // @action
            resolve (approvals)
        })
    })
}

export const searchApprovals = (query) => {
    if ((!query || query.length === 0) && userStore.currentUser)
        return getUserApprovals(userStore.currentUser.id)

    return new Promise ((resolve, reject) => {
        const activeUserId = userStore.currentUser.id
        ProjectApiService.findProjects(query).then( response => {
            const apiProjects = response.data.projects.filter (project => project.project_type === ProjectType.APPROVAL) || []
            const approvals = apiProjects.map (apiProject => new ProjectModel (apiProject) )

            const sent = approvals.filter( project => project && project.creatorId === activeUserId && project.status !== ProjectStatus.CANCELLED) || []
            const received = approvals.filter( project => project.members.find(member => member.id === activeUserId && member.role === UserRole.APPROVER && project.status !== ProjectStatus.CANCELLED) !== undefined) || []
            projectStore.setFilteredApprovals(received, sent) // @action
            resolve (approvals)
        })
    })
}

export const createApprovalProject = (title, description, cards, requestor, approverIds, reviewType, completionType) => {
    if (!requestor)
        requestor = userStore.currentUser

    if (!title)
        title = "Your feedback is requested!"

    if (!description)
        description = (requestor.displayName || requestor.firstName) + ' would like your feedback on these items.'

    if (!reviewType)
        reviewType = APPROVE

    if (completionType === undefined)
        completionType = (approverIds && approverIds.length > 1)? ApprovalCompletionType.ALL_USERS_REQUIRED : ApprovalCompletionType.SINGLE_USER_COMPLETES

    const stars = ReactionApiService.getReactionSetByTitleFromDefaults(STARS)
    const thumbs = ReactionApiService.getReactionSetByTitleFromDefaults(THUMBS)
    const rs = reviewType === APPROVE? thumbs : reviewType === RATE? stars : null

    if (!cards)
        console.warn("Creating approval project with no cards.")

    if (!approverIds || approverIds.length === 0)
        console.warn("Creating approval project with no approvers.")

    let project = {project_title: title, project_description: description, project_creator: requestor.id,
                   project_type: ProjectType.APPROVAL, security_level: SecurityLevel.Public}
    if (cards && cards.length > 0)
        project.project_image = cards[0].image
    if (rs)
        project.reaction_set = rs.reacetion_set_id

    return new Promise( (resolve, reject) => {
        ProjectApiService.createProject (project).then (response => {
            let apiProject = response.data
            let project = new ProjectModel (apiProject)

            eventStore.getInstance ().observeProject (project)
            // PusherService.getInstance ().followProject (project.id)
            console.log ("Created approval project " + project.id)

            // Add cards
            addCardsToApproval (cards, project.id).then (cardIds => {
                project.cardIds = cardIds
                // Add members
                addMembersToApproval (requestor, approverIds, project.id).then (memberResponse => {
                    resolve (project)
                })
            })
        }).catch (error => {
            console.error (error)
            reject (error)
        })
    })
}


// Approval prep form can now user projectStore.addUsersToProject, which takes array of ids rather than single user calls
export const addCardsToApproval = (cards, apId) => {
    return new Promise( (resolve, reject) => {

        if (!cards || cards.length === 0 || !apId) {
            console.warn('addCardsToApproval: missing params')
            resolve([])
            return
        }

        const activeUserId = userStore.currentUser.id
        const cardIds = typeof cards[0] === 'object'? cards.map (card => card.id || card.card_id) : cards

        axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: approvalProjectAddCards,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                project_id: apId,
                user_id: activeUserId,
                card_ids: cardIds
            }
        }).then (response => {
            resolve(response.data.project_cards)
        }).catch(error => {
            console.error(error)
            resolve([])
        })
    })

}

export const addMembersToApproval = (requestor, approverIds, apId) => {
    return new Promise( (resolve, reject) => {

        if (!approverIds || !apId) {
            console.warn('addMembersToApproval: missing params')
            resolve({data: []})
            return
        }

        let apiCalls = []
        approverIds.forEach( mId => {
            apiCalls.push (addMemberToApproval(mId, apId, UserRole.APPROVER))
        })

        if (apiCalls.length > 0) {
            Promise.all (apiCalls).then (responses => {
                resolve (responses)
            })
        } else {
            resolve ([])
        }
    }).catch(error => {
        console.error(error)
    })

}

export const addMemberToApproval = (memberId, apId, role) => {
    if (!memberId || !apId) {
        console.error('addMemberToApproval: missing params')
        return
    }
    return projectStore.addUserToProject(apId, null, memberId, EventSource.Approval, role)

}

export const completeApproval = (apId) => {
    return projectStore.updateProject({project_id: apId, project_status: ProjectStatus.COMPLETED}) // ignore result. Will generate event we need.
}

export const leaveApproval = (apId) => {
    return projectStore.setUserProjectSettings(null, apId, null, UserRole.INACTIVE_USER) //
}

export const rejoinApproval = (apId) => {
    return projectStore.setUserProjectSettings(null, apId, null, UserRole.ACTIVE) //
}

export const cancelApproval = (apId) => {
    return projectStore.updateProject({project_id: apId, project_status: ProjectStatus.CANCELLED})
}

// I believe this was a workaround to trigger an event, when users and cards had already been added.
// Currently, they'll receive approp events when added etc.
export const broadcastApproval = (ap) => {
    const origStatus = ap.status
    projectStore.updateProject({project_id: ap.id, project_status: ProjectStatus.BROADCAST}).then( response => {
        projectStore.updateProject({project_id: ap.id, project_status: origStatus}).then( response => {
            // DO NOTHING. EVENT HAS BEEN BROADCAST
        })
    })
}

// Rather than change project status, when approver is done voting, they can 'leave' it
// returning true means project won't appear in approvals inbox
export const iHaveCompletedApproval = (ap) => {
    const me = userStore.currentUser.id
    if (!me)
        return true

    if (!ap || ap.cardTotal === 0)
        return true

    const meInProject = ap.members.find( mbr => mbr.user_id === me.id) || null
    if (meInProject)
        return meInProject.role === UserRole.INACTIVE_USER
    else
        return true
}

