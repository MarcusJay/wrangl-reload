// DIY REDUX with mobx: allow lower level react components to *request* to change application state without direclty accessing stores
// In redux this would be a set of reducers
// Ultimately, the costs outweighed the benefits, if any, of this approach. Good experiment and learning experience.

import userStore from '/store/userStore'
import cardStore from '/store/cardStore'
import projectStore from '/store/projectStore'
import tagStore from '/store/tagStore'
import {UserRole} from "../config/Constants";

// project state change requests
export const REQUEST_PROJECT_CREATE = 'projectCreate'
export const REQUEST_PROJECT_FETCH = 'projectFetch'
export const REQUEST_PROJECT_UPDATE = 'projectUpdate'
export const REQUEST_PROJECT_MOVE = 'projectMove'
export const REQUEST_PROJECT_COMMENT = 'projectComment'
export const REQUEST_PROJECT_COMMENT_DELETE = 'projectCommentDelete'
export const REQUEST_PROJECT_ADD_REACTION_SET = 'projectAddReactionSet'
export const REQUEST_PROJECT_ADD_USER = 'projectAddUser'
export const REQUEST_PROJECT_DELETE_USER = 'projectDeleteUser'

// user requests
export const REQUEST_USER_UPDATE = 'userUpdate'


// Tag Operations
// 1. User Tags
export const REQUEST_USER_TAGS_UPDATE = 'userTagsUpdate'
export const REQUEST_USER_SAVE_TAG = 'userSaveTag'
export const REQUEST_USER_DELETE_TAG = 'userDeleteTag'

// 2. Project Level Tags
export const REQUEST_PROJECT_ADD_TAG = 'projectAddTag'
export const REQUEST_PROJECT_DELETE_TAG = 'projectDelTag'
// 3. Cardable Tag Set
export const REQUEST_CARDABLE_SET_SAVE_TAG = 'cardableSetSaveTag'
export const REQUEST_CARDABLE_SET_DELETE_TAG = 'cardableSetDelTag'
// 4. Actual Card Tag assignment
export const REQUEST_CARD_ADD_TAG = 'cardAddTag'
export const REQUEST_CARD_DELETE_TAG = 'cardDelTag'



export const REQUEST_CARD_SELECT = 'cardSelect'
export const REQUEST_CARD_SAVE = 'cardSave'
export const REQUEST_CARDS_CREATE = 'cardsCreate'
export const REQUEST_CARDS_UPLOAD = 'cardsUpload'
export const REQUEST_CARD_MOVE = 'cardMove'
export const REQUEST_CARD_DELETE = 'cardDelete'
export const REQUEST_CARD_COMMENT = 'cardComment'
export const REQUEST_CARD_COMMENT_DELETE = 'cardDeleteComment'
export const REQUEST_CARD_REACT = 'cardReact'
export const REQUEST_CARD_UNREACT = 'cardUnreact'
// export const REQUEST_CLONE_CARD = 'cloneCard'
export const REQUEST_CLONE_CARDS_TO_PROJECT = 'cloneCardsIntoProject'


// Poor Man's Reducer
class StateChangeRequestor {
    // Request state change and move on. No results returned.
    requestStateChange(requestType, requestData) {
        console.log('requestType: ' +requestType+ ', requestData: ' +requestData)
        switch (requestType) {
            // Projects
            case REQUEST_PROJECT_ADD_REACTION_SET:
                projectStore.assignDefaultReactionSet(requestData.project)
                break

            // Cards
            case REQUEST_CARD_SELECT:
                return cardStore.selectCard(requestData.cardId)
                break
        }
    }

    // Request a state change and get back results so you can update accordingly
    awaitStateChange(requestType, requestData) {
        console.log('requestType: ' +requestType+ ', requestData: ' +requestData)
        switch (requestType) {
            // Users
            case REQUEST_USER_UPDATE:
                return userStore.up

            // Projects
            case REQUEST_PROJECT_FETCH:
                return projectStore.fetchCurrentProject()
            case REQUEST_PROJECT_UPDATE:
                return projectStore.updateProject(requestData.project)
            case REQUEST_PROJECT_MOVE:
                return projectStore.updateUserProjectsSorting(requestData.projectIdList)
            // case REQUEST_PROJECT_COMMENT:
            //     return projectStore.addComment(requestData.project, requestData.commentText)
            case REQUEST_PROJECT_COMMENT_DELETE:
                return projectStore.deleteComment(requestData.commentId)
            case REQUEST_PROJECT_CREATE:
                return projectStore.createProject()
            case REQUEST_PROJECT_ADD_USER:
                return projectStore.addUserToProject(requestData.projectId, requestData.userId, requestData.newMemberId, requestData.trigger, requestData.newMemberRole || UserRole.INVITED)
            case REQUEST_PROJECT_DELETE_USER:
                return projectStore.removeUserFromProject(requestData.projectId, requestData.userId, requestData.memberId, requestData.trigger, requestData.memberRole || UserRole.DELETED)

            // Tags
            case REQUEST_USER_SAVE_TAG:
                return userStore.saveUserTag(requestData.tag, requestData.oldTag)
            case REQUEST_USER_DELETE_TAG:
                return userStore.deleteUserTag(requestData.tag)

            case REQUEST_PROJECT_ADD_TAG:
                return tagStore.tagProject(requestData.tagId, requestData.projectId)

            case REQUEST_CARDABLE_SET_SAVE_TAG:
                return projectStore.saveTagToCardableTagSet(requestData.projectId, requestData.tag, requestData.oldTag)
            case REQUEST_CARDABLE_SET_DELETE_TAG:
                return projectStore.deleteTagFromCardableTagSet(requestData.projectId, requestData.tag)

            // Cards
            case REQUEST_CARD_SAVE:
                return cardStore.saveCard(requestData.card)
            case REQUEST_CARDS_CREATE:
                return cardStore.createCardsFromAssets(requestData.card)
            case REQUEST_CARDS_UPLOAD:
                return cardStore.createCardsFromFiles(requestData.cards, requestData.project)
            case REQUEST_CARD_MOVE:
                return cardStore.moveCard(requestData.card, requestData.destination)
            case REQUEST_CARD_DELETE:
                return cardStore.deleteCard(requestData.cardId, requestData.projectId)
            case REQUEST_CARD_COMMENT:
                return cardStore.addComment(requestData.card, requestData.commentText)
            case REQUEST_CARD_COMMENT_DELETE:
                return cardStore.deleteComment(requestData.commentId)
            case REQUEST_CARD_REACT:
                return cardStore.addReaction(requestData.card, requestData.reactionTypeId)
            case REQUEST_CARD_UNREACT:
                return cardStore.deleteReaction(requestData.card, requestData.reactionTypeId, requestData.reactionId)

            case REQUEST_CARD_ADD_TAG:
                return tagStore.tagCard(requestData.tagId, requestData.cardId )
            case REQUEST_CARD_DELETE_TAG:
                return tagStore.untagCard(requestData.tagId, requestData.cardId )

            // Misc and Bulk Actions
            case REQUEST_CLONE_CARDS_TO_PROJECT:
                return cardStore.cloneCardsIntoProject(requestData.cards, requestData.project)
        }
    }
}

export default new StateChangeRequestor ()