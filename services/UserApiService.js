import axios from 'axios'

import {ApiTempKeyName, ApiTempKeyValue} from '/config/ApiEndpoints'
import {EventSource} from '/config/EventConfig'
// utils
import isEmail from 'validator/lib/isEmail';
import {generateGUID} from "../utils/solo/generateGUID";
import {
    socialLogin, userAccountCodeCreate, userConnect, userConnections, userDisconnect,
    userIntegrationCreate, userIntegrationsDelete,
    userIntegrationsRead, userPrefs, userProfilePublicRead, userProfileRead, userProfileUpdate, userResetCodeCreate,
    userResetCodeRead, userTagsRead, userTagsUpdate,
    userVerifyEmail, userVerify, userProfileCreate, userSearchStart, userSearchSelect, userSearchEnd
} from "../config/ApiEndpoints";
import {UserStatus} from "../config/Constants";
import userStore from "../store/userStore";

// TODO Could deconstruct singleton class into modules. Headers are defined in individual calls anyway.
// Only used by a couple classes. userStore and search.
class UserApiService {

    constructor() {

        // axios header
        this.postConfig = {
          headers: {
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
          }
        }
    }

    /**
     *
     * @param userId
     * @returns {Promise} json format { users: [ user1, user2 ], not_found: [ userId1, userId2] }
     */
    getUserById(userId) {
        if (!userId) {
            debugger
            // throw new Error('getUserById: missing userId')
        }
        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userProfileRead,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId
            }
        })
    }


    /**
     *
     * @param userId
     * @returns {AxiosPromise} a single UserProfile object

     */
    getUserProfileById(userId) {
        if (!userId) {
            throw new Error('getUserProfileById: missing userId')
        }
        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userProfileRead,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_ids" : [userId]
            }
        })
    }

    // Get PUBLIC info only for a user
    getPublicUsersById(userIds) {
        if (!userIds || userIds.length === 0) {
            throw new Error('getUserPublicProfilesById: missing userIds')
        }
        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userProfilePublicRead,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_ids" : userIds
            }
        })
    }

    // Note: api only returns users that are not connected.
    // So frontends must combine this search with a filter of userStore.currentConnections
    findUsers(query) {
        return new Promise((resolve, reject) => {
            if (!userStore.currentUser || !query || query.length === 0) {
                resolve([])
                return
            }

            return axios({
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                url: userSearchStart,
                data: {
                    [ApiTempKeyName]: ApiTempKeyValue,
                    "user_id": userStore.currentUser.id,
                    "search_string" : query
                }
            }).then (response => {
                resolve(response)
            }).catch (error => {
                reject(error)
            })
        })
    }

    connectFoundUser(sessionId, userIndex) {
        return new Promise((resolve, reject) => {
            if (!userStore.currentUser || !sessionId || !Number.isInteger(userIndex)) {
                reject('Missing params')
                return
            }

            return axios({
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                url: userSearchSelect,
                data: {
                    [ApiTempKeyName]: ApiTempKeyValue,
                    "user_id": userStore.currentUser.id,
                    "session_id" : sessionId,
                    "item_index": userIndex
                }
            }).then (response => {
                resolve(response)
            }).catch (error => {
                reject(error)
            })
        })
    }

    endFindSession(sessionId) {
        if (!sessionId) {
            console.error('No sessionId to finish')
            return
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userSearchEnd,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id": userStore.currentUser.id,
                "session_id": sessionId
            }
        })
    }



    // Only returns display name, used to verify whether an email is already in db
    verifyUserEmail(email) {
        if (!email) {
            debugger
            throw new Error('verifyUserEmail: missing email')
        }
        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userVerifyEmail,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "email" : email
            }
        })
    }

    getUserPrefs(userId) {

        console.log("###### Calling userPrefs with userId = " +userId)
        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userPrefs,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId
            }
        })
    }

    setUserPrefs(pref) {
        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userPrefs,
            data: Object.assign (pref, {[ApiTempKeyName]: ApiTempKeyValue})
        })
    }

    /**
     *
     */
    createUser(email, name, password, pwOptional, status) {
        if ((!email && !name) || (!password && !pwOptional)) {
            return Promise.reject(new Error('missing display name / email, or password'))
        }

        let payload = {
            [ApiTempKeyName]: ApiTempKeyValue,
        }

        // TODO es6 - use generic args collection, iterate thru

        if (email) {
            payload.email = email
        }
        if (name) {
            payload.display_name = name
        }
        if (status !== undefined) {
            payload.user_status = status
        }
        if (password) {
            payload.password = password
        }

        return axios.post(
                    userProfileCreate,
                    payload,
                    this.postConfig
                )
    }

    createAnonUser() {
        return this.createUser(null, 'Anonymous', '', true, 0)
    }

    getUserIntegrations(userId) {
        if (!userId)
            throw new Error('integrations: missing userId')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userIntegrationsRead,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId
            }
        })
    }

    addUserIntegration(userId, source, token) {
        if (!userId || !source || !token)
            throw new Error('add integration: missing params')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userIntegrationCreate,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId,
                "service": EventSource.Dropbox,
                "token": token
            }
        })
    }

    deleteUserIntegration(userId, integrationId) {
        if (!userId)
            throw new Error('del integration: missing params')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userIntegrationsDelete,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId,
                "integration_id" : integrationId
            }
        })
    }


    /**
     * @returns Promise: a single UserProfile object
     */
    loginUserByOAuth(authToken, provider) {
        return axios({
            method: 'POST',
            headers: {
                //'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: socialLogin,
            transformRequest: [function (data) {
                return JSON.stringify(data);
            }],
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "type" : provider,
                "token" : authToken,
                "access_token" : authToken
            }
        })
    }

    // endpoint requires 1 of either displayName or email. Password is always required.
    /**
     * @returns Promise: a single UserProfile object
     */
    loginUser(identifier, password) {

        if (!identifier) {
            return Promise.reject(new Error('missing display name / email, or password'))
        }

        let payload = {
            [ApiTempKeyName]: ApiTempKeyValue,
            "password" : password
        }

        // check if identifier is email or name
        if(isEmail(identifier)) {
            payload.email = identifier
        } else {
            payload.display_name = identifier
        }

        return  axios.post(
                userVerify,
                payload,
                this.postConfig
        )
    }

    /**
     * @returns Promise: a single UserProfile object
     */
    updateUser(user) {
        if (!user) {
            throw new Error('updateUserProfile: missing profile')
        }
        if (user.id)
            user = this.userToApiPayload(user)

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userProfileUpdate,
            data: Object.assign(user, {[ApiTempKeyName]: ApiTempKeyValue})
        })
    }

/*
    resetUserPassword(apiUser) {
        if (!apiUser) {
            throw new Error('missing user')
        }
        const recoveryUrl = generateGUID()

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userResetPassword,
            data: {
                email: apiUser.email,
                [ApiTempKeyName]: ApiTempKeyValue
            }
        })
    }
*/

    generatePasswordResetCode(email) {
        if (!email) {
            throw new Error('missing email')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userResetCodeCreate,
            data: {
                email: email,
                [ApiTempKeyName]: ApiTempKeyValue
            }
        })
    }

    getUserFromResetCode(code) {
        if (!code) {
            throw new Error('missing reset code')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userResetCodeRead,
            data: {
                reset_code: code,
                [ApiTempKeyName]: ApiTempKeyValue
            }
        })
    }

    createUserAccountCode(userId) {
        if (!userId) {
            throw new Error('missing userId')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userAccountCodeCreate,
            data: {
                user_id: userId,
                [ApiTempKeyName]: ApiTempKeyValue
            }
        })
    }

    getUserTags(userId) {
        if (!userId) {
            throw new Error('getUserTags: missing user')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userTagsRead,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId
            }
        })
    }

    /**
     * Add/Update user tag.
     * @param (new) tag name
     * @param old tag name. If present, this is an edit. Delete it.
     * @param userId
     * @returns {Promise}
     */
    saveUserTag(userId, tag, oldTag) {
        if (!tag || !userId) {
            throw new Error('addUserTag: missing tag or user')
        }

        let payload = {
            [ApiTempKeyName]: ApiTempKeyValue,
            "user_id" : userId,
            "add_tags" : [tag]
        }
        if (oldTag)
            payload["delete_tags"] = [oldTag]

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userTagsUpdate,
            data: payload
        })
    }

    deleteUserTag(userId, tag) {
        if (!tag || !userId) {
            throw new Error('delUserTag: missing tag or user')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userTagsUpdate,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId,
                "delete_tags" : [tag]
            }
        })
    }

    /**
     * "Connect" = Make Them Friends. Nothing to do with login.
     * @param userId
     * @param friendId
     * @param pin
     * @returns Promise: { user_id, friend_id, success: true }
     */
    connectUsers(userId, friendId, pin) {
        return this.setUserRelationship (userId, friendId, true, pin)
    }

    /**
     * "Disconnect" = Make Them Strangers. Nothing to do with login.
     * @param userId
     * @param friendId
     * @returns Promise: { user_id, friend_id, success: true }
     */
    disconnectUsers(userId, friendId) {
        return this.setUserRelationship (userId, friendId, false, false)
    }

    /**
     * Connect or disconnect 2 users.
     * @param userId
     * @param friendId
     * @param makeFriends
     * @param pinFriend
     * @returns Promise: { user_id, friend_id, success: true }
     */
    setUserRelationship(userId, friendId, makeFriends, pinFriend) {
        if (!userId || !friendId || makeFriends === undefined) {
            throw new Error("setUserRelationship: userId or friendId or makeFriends missing!")
        }

        let endpoint = makeFriends? userConnect : userDisconnect
        let payload = {
            [ApiTempKeyName]: ApiTempKeyValue,
            "user_id" : userId,
            "friend_id" : friendId
        }

        if (makeFriends)
            payload["pin_friend"] = pinFriend === true


        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: endpoint,
            data: payload
        })
    }

    /**
     *
     * @param userId
     * @returns {AxiosPromise} format { connections: [userId, userId, ... userId] }
     */
    getUserConnections(userId) {
        console.log('calling api: ' +userId)
        if (!userId) {
            throw new Error('getMyFriends: missing userId')
        }
        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userConnections,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId
            }
        })
    }
    /* alias */
    getMyFriends = this.getUserConnections

    userToApiPayload(user) {
        let apiUser = {}
        if (user.id)
            apiUser.user_id = user.id

        if (user.displayName)
            apiUser.display_name = user.displayName

        if (user.status)
            apiUser.user_status = user.status
        if (user.image)
            apiUser.image = user.image

        if (user.firstName)
            apiUser.first_name = user.firstName

        if (user.lastName)
            apiUser.last_name = user.lastName
        if (user.bio)
            apiUser.user_bio = user.bio

        if (user.email)
            apiUser.email= user.email

        if (user.password)
            apiUser.password = user.password

        if (user.phone)
            apiUser.phone = user.phone


        return apiUser
    }



}

export default new UserApiService()