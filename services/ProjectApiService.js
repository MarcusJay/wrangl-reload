import axios from 'axios'

import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import userStore from '/store/userStore'
import ProjectModel from '/models/ProjectModel'
import {ProjectType, UserRole} from "../config/Constants";
import {
    addUsersToProject,
    cardableTagSetUpdate,
    projectCreate, projectCreateBatch, projectDelete,
    projectRead, projectsByUser, projectsByUserSorted, projectsByUserUpdateSorting, projectSearch, projectSecurity,
    projectsInCommon,
    projectTagsRead, projectTagsUpdate,
    projectUpdate,
    userProjectSettings
} from "../config/ApiEndpoints";
import moment from "moment";
import randomLtr from "../utils/solo/randomLtr";

class ProjectApiService {
    constructor() {
    }

    /**
     *
     * @param projectId
     * @returns {AxiosPromise} a single project object.
     */
    getProjectById(projectId, userId, logEvent) {
        if (!projectId || projectId.length < 10) {
            console.error("bad projectId is: "+projectId)

            throw new Error('getProjectById: missing projectId')
        }

        if (!userId && userStore.currentUser)
            userId = userStore.currentUser.id
        if (!userId) {
            console.error("Project read: USER ID IS NOW REQUIRED")
            debugger
        }


        console.log('@@@ %c getProjectById pre-axios', 'color: pink')
        let payload = {
            [ApiTempKeyName]: ApiTempKeyValue,
            "project_id" : projectId
        }

        payload["user_id"] = userId
        payload['log_event'] = (logEvent === true && !!userId)

        console.log("Requesting project " +projectId+ ' from api')
        return axios({
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/json',
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectRead,
            data: payload
        })
    }

    /**
     *
     * @param userId
     * @returns {AxiosPromise} array of objects: { projects: [project, project, ... project] }
     */
    getUserProjects(userId, sortBy, projectType) {
        if (!userId) {
            if (!userStore.currentUser)
                throw new Error ('getUserProjects: no userId or currentUser')
            else
                userId = userStore.currentUser.id
        }

        if (!sortBy)
            sortBy = "time_modified"

        if (!projectType)
            projectType = ProjectType.NORMAL

        // new option: sort_index

        return axios({
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/json',
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectsByUser,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId,
                // "project_type" : projectType,
                // "sort_type" : sortBy,
                // "member_start": 0,
                // "member_end": 8
            }
        })
    }

    /**
     *
     * @param userIds - array of users that hold this project in common
     * @returns {AxiosPromise} array of objects: { projects: [project, project, ... project] }
     */
    getCommonProjects(userIds, getIdsOnly) {
        if (!userIds || userIds.length ===0) {
            throw new Error('common projects: no users specified!')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectsInCommon,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_ids" : userIds
            }
        })
    }

    countCommonProjects(userIds) {
        if (!userIds || userIds.length ===0) {
            throw new Error('common projects: no users specified!')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectsInCommon,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_ids" : userIds}
        })
    }

    /**
     *
     * @param userId
     * @returns {AxiosPromise} array of ids only: { project_ids: [projectId, projectId, ... projectId] }
     */
    getUserProjectsSorting(userId) {
        if (!userId) {
            userId = userStore.currentUser.id
        }

        return axios({
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/json',
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectsByUserSorted,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId
            }
        })
    }

    getUserProjectSettings(userId, projectId) {
        if (!projectId)
            throw new Error('user project settings missing project id')
        if (!userId) {
            userId = userStore.currentUser.id
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userProjectSettings,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId,
                "project_id" : projectId
            }
        })
    }

    // TODO user roles etc. worth modeling? probably not yet.
    setUserProjectSettings(userId, projectId, notifLevel, role) {
        if (!projectId)
            throw new Error('user project settings missing param(s)')
        if (!userId) {
            userId = userStore.currentUser.id
        }

        let payload = {
            [ApiTempKeyName]: ApiTempKeyValue,
            "user_id" : userId,
            "friend_id" : userId,
            "project_id" : projectId
        }

        if (notifLevel)
            payload['notification_preference'] = notifLevel
        if (role)
            payload['user_role'] = role

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userProjectSettings,
            data: payload
        })
    }

    /**
     *
     * @param query
     * @returns {AxiosPromise}
     */
    findProjects(query) {
        if (!query)
            throw new Error ('findProjects: missing query or tags')

        return axios ({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectSearch,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                search_string: query,
                user_id: userStore.currentUser.id
            }
        })

    }
    searchProjects = this.findProjects


    createProject(projectModel) {
        if (!projectModel)
            projectModel = new ProjectModel()

        // if client model
        if (projectModel.hasOwnProperty('title')) {
            if (!projectModel.creatorId)
                projectModel.creatorId = userStore.currentUser.id
            projectModel = this.projectToApiPayload(projectModel)
        }
        if (!projectModel.time_start)
            projectModel.time_start = moment().format('YYYY-MM-DD hh:mm:ss');  // 2018-12-13 23:58:49

        // if (!projectModel.project_image) default images nixed by MC
        //     projectModel.project_image = 'https://pogo.io/static/img/stocks/' +randomLtr(10)+ '.jpg'


       let payload = Object.assign(projectModel, {[ApiTempKeyName]: ApiTempKeyValue})
/*
        if (!!payload.members) {
            payload.initial_members = payload.members
            delete payload.members
        }
*/
        delete payload.cards

        return axios({
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/json'
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectCreate,
            data: payload
        })
    }

    createProjects(n, name) {
        if (typeof n !== 'number')
            n = parseInt(n)
        if (n < 1 || n > 50)
            throw new Error('too many or too few')
        if (!name)
            name = 'New Project'

        return axios({
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/json'
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectCreateBatch,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "count" : n,
                "auto_increment_title" : true,
                "project_title" : name,
                "project_creator" : userStore.currentUser.id
            }
        })
    }

    /**
     *
     * @param project
     * @returns {AxiosPromise} a single project object (see apiProject fields in ProjectModel). Does not include cards.
     */
    updateProject(project, userId) {
        if (!project) {
            throw new Error('updateProject: missing project')
        }

        if (!userId)
            userId = userStore.currentUser.id
        if (project.hasOwnProperty('id'))
            project = this.projectToApiPayload( project )

        const payload = Object.assign(project, {[ApiTempKeyName]: ApiTempKeyValue, user_id: userId})
        return axios({
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/json',
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectUpdate,
            data: Object.assign(project, {[ApiTempKeyName]: ApiTempKeyValue, user_id: userId})
        })

    }

    deleteImage(project) {
        return this.updateProject({project_id: project.id, project_image: 'DELETE'})
    }

    /**
     *
     * @param projectIds - array of project id's in sorted order
     * @returns {AxiosPromise}
     */
    updateUserProjectsSorting(projectIds) {
        if (!projectIds) {
            throw new Error('updateUserProjectsSorting: missing projects array')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectsByUserUpdateSorting,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                user_id: userStore.currentUser.id,
                project_ids: projectIds
            }
        })

    }

    // userId = currently logged in user
    // memberIds = array of users being added to project
    addUsersToProject(projectId, activeUserId = (userStore.currentUser? userStore.currentUser.id : null), memberIds, eventSource, userRole = UserRole.INVITED) {
        if (!projectId || !activeUserId || !memberIds || memberIds.length === 0) {
            throw new Error('addUserToProject: missing project or user params')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: addUsersToProject,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                project_id: projectId,
                user_id: activeUserId,
                member_ids: memberIds,
                user_role: userRole
            }
        })
    }

    removeUserFromProject(projectId, activeUserId, memberId, isPermanent) {
        if (!projectId || !memberId) {
            throw new Error('removeUserFromProject: missing project or user')
        }

        // TODO check if admin here?
        const newRole = isPermanent? UserRole.DELETED : UserRole.INACTIVE_USER

        if (!activeUserId)
            activeUserId = userStore.currentUser? userStore.currentUser.id : memberId

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: userProjectSettings,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "project_id": projectId,
                "friend_id": memberId,
                "user_id": activeUserId,
                "user_role" : newRole
                // "notification_preference" : NOTIF_NONE
            }
        })
    }

    getProjectTags(projectId) {
        if (!projectId)
            throw new Error('getProjectTags: missing project')

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectTagsRead,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                project_id: projectId
            }
        })
    }

    tagProject(projectId, tag) {
        if (!tag || !projectId) {
            throw new Error('saveProjectTag: missing tag or user')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectTagsUpdate,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userStore.currentUser.id,
                "project_id": projectId,
                "add_tags" : [tag]
            }
        })
    }

    untagProject(projectId, tag) {
        if (!tag || !projectId) {
            throw new Error('delProjectTag: missing tag or user')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectTagsUpdate,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userStore.currentUser.id,
                "project_id" : projectId,
                "delete_tags" : [tag]
            }
        })
    }

    saveTagToCardableTagSet(projectId, tag, oldTag) {
        if (!tag || !projectId) {
            throw new Error('addTagToCardableSet: missing tag or user')
        }

        let payload = {
            [ApiTempKeyName]: ApiTempKeyValue,
            "project_id" : projectId,
            "add_tags" : [tag]
        }

        if (oldTag)
            payload["delete_tags"] = [oldTag] // delete_tags

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: cardableTagSetUpdate,
            data: payload
        })
    }

    deleteTagFromCardableTagSet(projectId, tag) {
        if (!tag || !projectId) {
            throw new Error('delTagToCardableSet: missing tag or user')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: cardableTagSetUpdate,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "project_id" : projectId,
                "delete_tags" : [tag]
            }
        })
    }

    deleteProject(projectId) {
        if (!projectId) {
            throw new Error('deleteProject: missing pr oject')
        }

        return axios({
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectDelete,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "project_id": projectId,
                "user_id": userStore.currentUser.id
            }
        })
    }

    getProjectSecurity(projectId) {
        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: projectSecurity,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "project_id": projectId
            }
        })
    }

    projectToApiPayload(project) {
        if (!project) {
            console.error('projectToApiPayload with no project')
            debugger
            return {}
        }
        let apiProject = {}
        if (project.id !== 'new')
            apiProject.project_id = project.id
        if (project.creatorId)
            apiProject.project_creator = project.creatorId
        if (project.title)
            apiProject.project_title = project.title
        if (project.description)
            apiProject.project_description = project.description
        if (project.image)
            apiProject.project_image = project.image
        if (project.securityLevel !== undefined)
            apiProject.security_level = project.securityLevel
        if (project.reactionSetId)
            apiProject.reaction_set = project.reactionSetId

        return apiProject
    }

}

export default new ProjectApiService()