import Pusher from 'pusher-js'

import userStore from '/store/userStore'
import uiStateStore from '/store/uiStateStore'
import eventStore from '/store/eventStore'
import {isBrowser} from "../utils/solo/isBrowser";
import dehyphenate from "../utils/solo/dehyphenate";
import {truncate} from "../utils/solo/truncate";

// Pusher Servers
const WranglPushLocal = "http://localhost:8200"
const WranglPushOld = "https://push.wran.gl"
const WranglPushRemote = "https://push.pogo.io"

const MAX_PROJECTS = 50

// Pusher Events
const UserTriggeredEvent = "my-event"   // broadcast
const SystemEvent = "system-event"                  // broadcast
const LocalEvent = "local-event"                    // don't broadcast

// Current Env
let currWranglPush = WranglPushRemote // REMOTE!!!

// Handles incoming ONLINE web push notifications (in-app)
class PusherService {

    connect() {
        Pusher.logToConsole = true; // TODO for dev only
        const self = this
        console.warn("PUSHER CONNECT. BROWSER? " +isBrowser())

        // Hot-update is a culprit in excessive pusher connections.
        // Attempt to reuse connection rather than open new one.

        if (uiStateStore.pusherConnection) {
            console.warn ("Reusing existing Pusher connection.")
            self.pusher = uiStateStore.pusherConnection
        } else {
            console.error ("Creating a new Pusher connection.")
            self.pusher = new Pusher ('7ecdbbfa9d6dd52c6ab8', {
                cluster: 'us2',
                encrypted: true
            })
            uiStateStore.pusherConnection = self.pusher
        }

        self.pusher.connection.bind('state_change', function(states) {
            console.warn('Pusher state change: ', states.current)
            self.connected = states.current.indexOf('connected') !== -1 // may contain spaces
        })

        const activeUser = userStore.currentUser
        if (activeUser)
            self.followUser (activeUser.id)
    }

    disconnect() {
        console.warn("Pusher disconnecting. ")
        this.pusher.disconnect()
    }

    followUser(userId) {
        const ps = this

        const userKey = truncate(dehyphenate(userId),10) // TODO hyphens in topics must be fine, as my-channel works
        let testChannel = this.pusher.channel (userKey)
        if (!testChannel) {
            testChannel = this.pusher.subscribe(userKey)
            console.warn("SUBSCRIBED TO " +userKey)
            testChannel.bind(UserTriggeredEvent, (event) => {
                console.warn("DBG my-event: " +JSON.stringify(event))
                ps.handleChannelEvent (event)
            })
        }

/*      Do you like Redundant Notifications? Cus...
        this.pusher.bind (UserTriggeredEvent, (event) => {
            console.warn('USER PUSHER-WIDE event: ' +JSON.stringify(event))
            ps.handleChannelEvent (event)
        })
*/

/*
                        testChannel = this.pusher.channel ('my-channel')
                        if (!testChannel) {
                            testChannel = this.pusher.subscribe('my-channel')
                            console.warn("SUBSCRIBED TO my-channel")
                            testChannel.bind('my-event', (data) => {
                                console.warn("DBG my-event: " +JSON.stringify(data))
                                ps.handleChannelEvent (data)
                            })
                        }
*/


    }

    unfollowUser(userId = userStore.currentUser? userStore.currentUser.id : null) {
        if (!userId)
            return

        console.log ("Pusher: unfollowing user " + userId)
        let channel = this.pusher.channel (userId)
        if (!channel) {
            console.warn('Attempt to unfollow user that is not subscribed. ', userId)
            return
        }

        channel.unbind (UserTriggeredEvent);
        this.pusher.unsubscribe (userId);
    }

    handleChannelEvent(event) {
        eventStore.getInstance().addEvent(event)
    }

}

export default new PusherService()