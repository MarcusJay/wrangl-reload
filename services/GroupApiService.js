import axios from 'axios'

import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import userStore from '/store/userStore'
import {groupCreate, groupRead, groupUpdate, userGroups} from "../config/ApiEndpoints";

export default class GroupApiService {
    static getInstance() {
        if (!GroupApiService.instance) {
            GroupApiService.instance = new GroupApiService ()
        }
        return GroupApiService.instance
    }

    constructor() {
    }

    /**
     *
     * @param groupId
     * @returns {AxiosPromise}
     */
    getGroupById(groupId) {
        if (!groupId) {
            throw new Error('getGroupById: missing groupId')
        }
        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            url: groupRead,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "group_id" : groupId
            }
        })
    }

    /**
     *
     * @param userId
     * @returns {AxiosPromise} format { groups: [groupId, groupId, ... groupId] }
     */
    getMyGroups(userId) {
        if (!userId) {
            throw new Error('getUserGroups: missing userId')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            url: userGroups,
            data: {
                [ApiTempKeyName]: ApiTempKeyValue,
                "user_id" : userId
            }
        })
    }
    /* alias */
    getUserGroups = this.getMyGroups

    /**
     *
     * @param name
     * @param creatorId
     * @param initialMembers
     * @returns {AxiosPromise} A single group object (see apiGroup fields in GroupModel), with the addition of bad members "not_found": [ userId, userId,...]
     */
    createGroup(name, creatorId, initialMembers) {
        if (!name) {
            throw new Error("createGroup: requires name")
        }
        if (!creatorId && userStore.currentUser)
            creatorId = userStore.currentUser.id

        let payload = {
            [ApiTempKeyName]: ApiTempKeyValue,
            "group_name" : name,
            "group_founder" : creatorId,
        }
        if (!!initialMembers) {
            payload.initial_members = initialMembers
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            url: groupCreate,
            data: payload
        })
    }

    /**
     *
     * @param groupModel
     * @returns {AxiosPromise} a single group object (see apiGroup fields in GroupModel)
     */
    updateGroup(groupModel) {
        if (!groupModel) {
            throw new Error('updateGroup: missing group')
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            url: groupUpdate,
            data: Object.assign(groupModel.toApiPayload(), {[ApiTempKeyName]: ApiTempKeyValue})
        })

    }

    addUsersToGroup(groupId, userIds) {
        return this.updateGroupUsers(groupId, userIds, true)
    }

    removeUsersFromGroup(groupId, userIds) {
        return this.updateGroupUsers(groupId, userIds, false)
    }

    /**
     *
     * @param groupId
     * @param userIds
     * @param isAdd - this is an add operation, not a removal.
     * @returns {AxiosPromise} a single group object (see apiGroup fields in GroupModel), with additional member array fields
     */
    updateGroupUsers(groupId, userIds, isAdd) {
        if (!groupId || !userIds || !(Array.isArray(userIds))) {
            throw new Error('updateGroupUsers: incorrect params. ')
        }

        let memberField = isAdd? "add_members" : "remove_members"
        let payload = {
            [ApiTempKeyName]: ApiTempKeyValue,
            "group_id" : groupId,
            memberField : userIds
        }

        return axios({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            url: groupUpdate,
            data: payload
        })
    }

}