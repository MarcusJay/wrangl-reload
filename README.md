# README.md
-----------
creator : @you

## GET STARTED
$ yarn run next

## FRACTURE

    Goals
        Create a Kit that lets you easily create
            Pages
            Incorporate Data Sources
            Has Client Store
            Has Server Side Rendering
            Prototype Services
            Utilize UI Kits
            Tests
            Documentation


### largely based on...

    - nextjs (frontend middleware)
    - micro (http api)
    - semantic ui

### notes on additions

- es6 -> [babel] w root import
    + lets you use import paths that are root based using a tilda '~/'

- ui -> [semantic ui react] https://react.semantic-ui.com/
    + lets you have react ready components

- store -> [mobx]
    + maintain a client store

- testing -> [jest]
    + build test page to display jest results

- code conventions
    + settings -> editorconfig -> via [airbnb](https://github.com/airbnb/javascript/blob/master/.editorconfig)
    + settings -> change log -> via [keepchangelog](http://keepachangelog.com/en/0.3.0/)
    + coding style -> airbnb javascript
    + documentation style -> airbnb javascript
        * code commenting rules [link](https://github.com/airbnb/javascript#comments)
    + use documentation format [link](http://www.davidboyne.co.uk/2016/05/26/automating-react-documentation.html)

- html framework >> semantic ui
    + handle a large set of ui patterns
    + clean documentation

- package manager >> yarn
    + $ yarn install
    + $ yarn add NAMEOFPACKAGE


### how to ?
- add a model
- add a page
- add a react lib
- add external api service to the api
- add external css lib
- add/edit css
- create a layout
- create an es6 class
- find a property in an array of objects
- map thru an array of objects
- preload/fetch data serverside and create a proxy api
- setup a page
- store api keys
- theme our ui framework
- run nextjs in parrellel with a http node client (micro || express || etc)



