console.log('sw')
self.importScripts('EventConfig.js', 'SWConfig.js')

/*
 * Initialize event handlers
 */
self.addEventListener ("install", event => {
    self.skipWaiting ();
    console.log ('@@@ install')
});

self.addEventListener ("activate", event => {
    console.log ('@@@ activate')
    self.currentUserId = null; // 'no user id yet'

    // debugger
    const promiseChain =
        new Promise( function(resolve, reject) {
            self.clients.claim ().then ( function(claimRes) {
                // claim results are nil
                // debugger
                return self.clients.matchAll({includeUncontrolled: true})
            }).then( clients => {
                // debugger
                if (!clients || clients.length === 0) {
                    console.error('Could not find a client to message')
                    resolve(false)
                }
                self.msgClient = clients[0]
                self.msgClient.postMessage({
                    msg: "get currentUser.id"
                });
                resolve(true)
            })
        })

    event.waitUntil(promiseChain)
})


self.addEventListener('message', function(event){
    console.log('!!! SW received msg: ' +event.data)
    self.currentUserId = event.data
})


self.addEventListener ('push', function (event) {
    self.eventData = null
    if (event.data) {
        console.log ('Received push notification: ', event.data.text ());
        self.eventData = event.data.json ()

    } else {
        console.log ('Received push event with no data.');
        return
    }

    // debugger
    // -1. ensure we have a user id and if not, get one for next time.
    console.log("### SERVICe WORKEr HAS A USER ID OF " +self.currentUserId)
    if (!self.currentUserId && self.msgClient !== undefined) {
        self.msgClient.postMessage({
            msg: "get currentUser.id"
        })
    }

    // 0. ensure notify_users is an array and not an object (due to skipping indices)
    let listeners = self.eventData.notify_users
    if (typeof listeners === 'object') {
        listeners = Object.values( listeners )
    }

    // 1. ignore unrelated projects. TODO verify we don't push them out in the first place.
    if (self.currentUserId && listeners.indexOf(self.currentUserId) === -1)
        return

    // 2. ignore ourselves
/*
    if (self.eventData.user_id === self.currentUserId)
        return
*/

    // 3. ignore events deemed to be untoastworthy
    if (IgnoredEvents.indexOf (self.eventData.event_type) >= 0 ) {
        return
    }

    const promiseChain =
        // find currently open (and potentially uncontrolled) tabs with each push
        self.clients.matchAll({includeUncontrolled: true}).then( function(clients) {
            self.myClients = clients
            // debugger
            let sendOfflinePush = false
            var hasFocusedClients = clients.some (function (client) { return client.focused });

            if (hasFocusedClients) {
                // user is active. in-app notif will suffice.
            } else if (clients && clients.length > 0) {
                // wrangl is open somewhere, but it's not the active tab/window.
                sendOfflinePush = true
            } else {
                // wrangl isn't currently open anywhere.
                sendOfflinePush = true
            }

            if (true || sendOfflinePush) { // TODO temp: add true to Always send

                self.hydrateEvent (self.eventData).then (function (hydratedEvent) {

                    const msg = getMessage (hydratedEvent, 1) // || 'Type ' + hydratedEvent.event_type)
                    if (!msg)
                        return {msg: null, options: {}}
                    const msgOptions = {
                        data: hydratedEvent,
                        sound: "https://pogo.io/static/audio/pop_drip.wav"
                    }
                    msgOptions.badge = msgOptions.icon = msgOptions.image = self.determineImage (hydratedEvent)

                    return {msg: msg, options: msgOptions}
                }).then (function (results) {
                    // debugger
                    if (results.msg !== null) {
                        console.log ('Notifying. Self: ' + self + ', reg: ' + self.registration + ', shownotif: ' + self.registration.showNotification)
                        return self.registration.showNotification (results.msg, results.options)
                    }
                })
            }
        })

    event.waitUntil(promiseChain)
})


/*
 * Utils to grab data from api necessary to display descriptive msg
 */
self.hydrateEvent = (event) => {
    let apiCalls = []
    return new Promise ((resolve, reject) => {

        if (event.user_id)
            apiCalls.push (self.getUser (event.user_id))
        if (event.card_id)
            apiCalls.push (self.getCard (event.card_id))
        if (event.project_id)
            apiCalls.push (self.getProject (event.project_id, event.user_id))

        // Awaiting <= 3 api calls
        Promise.all (apiCalls).then (responses => {
            var jsonReads = []
            responses.forEach (response => {
                jsonReads.push (response.json ())
            })

            // Api results received, however reading response bodies = more promises. So now awaiting <= 3 async json read operations.
            Promise.all (jsonReads).then (jsonResponses => {

                event.card = jsonResponses.find (json => json.card_id === event.card_id && json.hasOwnProperty ('card_title'))
                event.project = jsonResponses.find (json => json.project_id === event.project_id && json.hasOwnProperty ('project_title'))
                event.user = jsonResponses.find (json => json.user_id === event.user_id && json.hasOwnProperty ('display_name'))

                resolve (event)
            })
        }).catch (error => {
            debugger
            throw new Error (error)
        })
    })
}


self.getUser = function (userId) {
    if (!userId) {
        debugger
        // throw new Error('getUserById: missing userId')
    }

    var params = {
        [ApiTempKeyName]: ApiTempKeyValue,
        "user_id": userId
    }
    var payload = new FormData ()
    payload.append ("json", JSON.stringify (params))

    return fetch (ApiEndpoints.userRead, {
        method: 'POST',
        cache: 'no-cache',
        mode: 'cors',
        headers: {
            // 'content-type': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: JSON.stringify (params)
    })
}

self.getCard = function (cardId) {
    if (!cardId) {
        debugger
        // throw new Error('getUserById: missing userId')
    }

    var params = {
        [ApiTempKeyName]: ApiTempKeyValue,
        "card_id": cardId
    }
    var payload = new FormData ()
    payload.append ("json", JSON.stringify (params))

    return fetch (ApiEndpoints.cardRead, {
        method: 'POST',
        cache: 'no-cache',
        mode: 'cors',
        headers: {
            // 'Content-Type': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: JSON.stringify (params)
    })
}

self.getProject = function (projectId, userId) {
    if (!projectId || !userId) {
        debugger
        // throw new Error('getUserById: missing userId')
    }

    var params = {
        [ApiTempKeyName]: ApiTempKeyValue,
        "project_id": projectId,
        "log_event": false,
        "user_id": userId
    }
    var payload = new FormData ()
    payload.append ("json", JSON.stringify (params))

    return fetch ("https://api.pogo.io/wrangl-api/v2/project_read_new.php", { //  ApiEndpoints.projectRead,
        method: 'POST',
        cache: 'no-cache',
        mode: 'cors',
        headers: {
            // 'Content-Type': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: JSON.stringify (params)
    })
}

self.determineImage = function( ev ) {
    if (ev.card && ev.card.card_image)
        return ev.card.card_image
    else if (ev.project && ev.project.project_image)
        return ev.project.project_image
    else if (ev.user && ev.user.image)
        return ev.user.image
    else
        return 'https://pogo.io/static/img/Wcc1.png'
}
// end utils

/*
 * Handle events on open notifications
 */
self.addEventListener('notificationclick', function(event) {
    let eventData = event.notification.data
    let action = eventData.project_type === 1? 'approve' : 'project'

    let projectUrl = currEnv.baseUrl + action+ "/" +eventData.project_id

    event.waitUntil(
        self.clients.matchAll({includeUncontrolled: true}).then( function(clientList) {
            if (clientList.length > 0) {
                clientList[0].navigate(projectUrl).then( response => {
                    return clientList[0].focus ();
                }).catch( error => {
                    console.error(error)
                    return self.clients.openWindow(projectUrl);
                })
            }
            return self.clients.openWindow(projectUrl);
        })
    );

});

