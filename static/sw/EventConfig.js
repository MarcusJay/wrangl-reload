var ProjectType = {
    NORMAL: 0,
    APPROVAL: 1
}

var EventSource = {
    None: 0,
    EmailInvite: 1,
    ProjectAdd: 2,
    WebSearch: 3,
    ImageFromDisk: 4,
    Dropbox: 5,
    GoogleDocs: 6,
    ICloud: 7,
    Pinterest: 8,
    InApp: 9,
    DirectLink: 10,
    aScrapeUrl: 11,
    ScrapeResults: 12,
    Wrangle: 14,
    Test: 99
}

/*
 Event Levels
 */
var EventHigh      = 3
var EventMedium    = 2

/*
 Event Types (High)
 */
var UserIntegrationCreated = 2
var UserAcceptInvite = 5
var UserInvited = 4


// Project
var AddCommentToProject = 11
var AddCardToProject = 18
// Card
var AddCommentToCard = 20
var AddReactionToCard = 22
var DeleteReactionToCard = 23
var AddTagToCard = 24
var CreateCard = 15
var CreateCards = 33
var CardMove = 28

var CardMarkupAdd = 39
var CardMarkupDelete = 40

var SendDirectMessage = 13
var DirectMessageEdited = 38

var FolioChanged  = 43

/*
 Event Types (Medium)
 */
var UserInvite     = 4
var ProjectView    = 6
var ProjectCreate  = 8
var ApprovalProjectCreate  = 34
var ProjectUpdate  = 9
var ProjectDelete  = 10
var ProjectTakeBreak   = 14
var ProjectLeaveForever = 31
var DeleteCommentFromProject  = 12

var CardUpdate     = 16
var CardDelete     = 17
var DeleteCommentFromCard  = 21

var AddServiceIntegration = 2
var DeleteServiceIntegration = 3

var IgnoredEvents = [] // [CardDelete, UserInvite, ProjectView, ProjectTakeBreak, AddTagToCard, DeleteCommentFromProject, DeleteCommentFromCard, CardMove, DeleteReactionToCard, UserIntegrationCreated, AddServiceIntegration, DeleteServiceIntegration]
var ToastyEvents = [ApprovalProjectCreate, ProjectUpdate, AddCommentToCard, AddCommentToProject, SendDirectMessage, AddReactionToCard, CreateCard, CreateCards]
var CardChangeEvents = [AddCommentToCard, AddReactionToCard, DeleteCommentFromCard, DeleteReactionToCard, CardUpdate]
var CardSetEvents = [AddCardToProject, CreateCard, CreateCards, CardDelete]
var ProjectChangeEvents = [AddCommentToProject, AddCardToProject, ProjectUpdate, DeleteCommentFromProject, CardDelete]


var isToasty = (event) => {
    return !!event && (ToastyEvents.indexOf(event.event_type) !== -1)
}

var getMessage = (event, count) => {
    if (!event.user)
        debugger
    var userName = event.display_name? event.display_name : event.user? event.user.display_name : event.user.first_name

    var detail = event.event_detail
    var cardTitle = event.card? event.card.card_title : 'a card'
    var projectTitle = event.project_title? event.project_title: (event.project? event.project.project_title: null)

    var commentText = (event.comment_id && detail)? detail : null
    var isStatusChange = event.project_type === ProjectType.APPROVAL && event.updated_key === 'project_status'

    if (!projectTitle) {
        debugger
        return null
    }

    let msg = null

    switch( event.event_type ) {
        case ApprovalProjectCreate:
            msg = userName+ ' sent an Approval Request: "' +projectTitle+ '"'
            break

        case CardDelete:
            if (detail)
                msg = userName + ' deleted file "' +detail+ '"'
            break

        case CreateCard:
        case AddCardToProject:
            if (count > 1)
                msg = userName + ' added ' +count+ ' files'
            else if (cardTitle)
                msg = userName + ' added "' +cardTitle+ '"'
            else if (detail)
                msg = userName + ' added "' +detail+ '"'
            break

        case CreateCards:
            msg = userName + ' added ' +detail+ ' file' +(detail === '1'?'':'s') +' to "' +projectTitle+ '"'
            break

        case AddCommentToProject:
            if (count > 1)
                msg = userName + ' commented ' +count+ ' times on "' +projectTitle+ '"'
            else if (commentText !== null && projectTitle)
                msg = userName + ': "' +commentText+ '" in "' +projectTitle+ '"'
            else if (projectTitle)
                msg = userName + ' posted in "' +projectTitle+ '"'
            break

        case AddCommentToCard:
            if (count > 1)
                msg = userName + ' added ' +count+ ' notes to "' +cardTitle+ '"'
            else if (commentText)
                msg = userName + ' added note "' +commentText+ '"' // on "' +cardTitle+ '"'
            // else
            //     debugger
            break

        case AddReactionToCard:
            if (!event.reaction) {
                console.warn('AddReaction event, but no reaction. event.reactionTypeId: ' +event.reaction_type_id)
            }
            if (count > 1)
                msg = userName + ' reacted to files ' +count+ ' times'
            else {
                msg = userName + ' reacted to "' +cardTitle+ '"'
            }
            break

        case ProjectView:
            if (count > 1)
                msg = 'This board has been viewed ' +count+ ' time' +(count === 1? '':'s')
            else
                msg = userName + ' is viewing "' +projectTitle+ '"'
            break

        case ProjectUpdate:
            if (isStatusChange)
                msg = 'Request ' +(detail === '1'? 'completed: "' : '(re)opened: "') +projectTitle+ '"'
            else
                msg = null
            break

        case ProjectDelete:
            msg = userName + ' deleted "' +projectTitle+ '"'
            break

        case SendDirectMessage:
            msg = userName + ': "' +commentText+ '"'
            break

/*
        case ProjectTakeBreak:
            msg = userName + ' is taking a break from "' +projectTitle+ '"'
            break
*/

        case ProjectLeaveForever:
            msg = userName + ' left "' +projectTitle+ '"'
            break

        case CardMove:
            if (count > 1)
                msg = userName + ' moved ' +count+ ' files'
            else if (cardTitle)
                msg = userName + ' moved "' +cardTitle+ '"'
            break

        case CardUpdate:
            if (count > 1)
                msg = userName + ' updated ' +count+ ' files'
            else if (cardTitle)
                msg = userName + ' updated file "' +cardTitle+ '"'
            break
        default:
            msg = null // '(Event type '+event.type+ ')'
    }

    return msg
}

