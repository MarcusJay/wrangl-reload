var env = {
    local: {
        baseUrl: "http://localhost:8080/"
    },
    dev: {
        // baseUrl: "http://104.197.232.83/"
        baseUrl: "https://pogo.io/"
    },
    prod: {
        baseUrl: "https://pogo.io/"
    }
}

var currEnv = env.dev

// ServiceWorker lives outside scope of our app, so define our own constants here, can't import.
var ANON = 'No User ID'
var baseUrl = 'https://api.pogo.io/wrangl-api/v2/'
var ApiEndpoints = {
    userRead: baseUrl + 'user_profile_read.php',
    cardRead: baseUrl + 'card_read.php',
    projectRead: baseUrl + 'project_read_new.php'
}

var ApiTempKeyName = "API_Access_Key"
var ApiTempKeyValue = "BCE1674E-8E67-C1C9-BB15-D51BAFDB6FF2"

