import React, {Component} from 'react'
import {observer} from 'mobx-react'
import userStore from "../../store/userStore";
import AuthProtector from "../common/AuthProtector";
import ApprovalReactions from "./ApprovalReactions";
import MemberAvatar from "../common/MemberAvatar";
import {firstWord} from "../../utils/solo/firstWord";
// import {DropTarget} from 'react-dnd'

const MAX_MEMBERS = 7

@observer
export default class FeedItemComments extends Component {


    componentDidUpdate() {
        const cmts = this.props.comments

    }


    render() {
        const {card, comments, activeUser, project} = this.props
        if (!card || !comments)
            return <div className='pad1 small jSecinv'>No comments</div>

        let author

        return (
            <div className='jFGinv jBGinv' style={{padding: '0 1rem'}}>
                {comments.map ( (cmt,i) => {
                    author = cmt.user || userStore.getUserFromConnections(cmt.creator_id)

                    return (
                    <div className='small'>
                        {!author &&
                        <img src='/static/svg/v2/listColUser.svg' width='22px' className='mr05'/>
                        }

                        {author &&
                        <span className='bold'>{author.firstName || firstWord(author.displayName)}:&nbsp;</span>
/*
                        <MemberAvatar member={author}
                                      activeUser={activeUser}
                                      enableAdmin={false}
                                      project={project}
                                      key={'ccu' + i}
                                      index={i}
                                      className='mr05'
                        />
*/
                        }

                        <span className='inline'>{cmt.comment_text}</span>
                    </div>
                    )


                })}
            </div>
        )

    }
}