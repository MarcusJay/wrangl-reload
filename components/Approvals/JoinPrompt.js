import {Component} from 'react'
import {observer} from 'mobx-react'
import {Modal} from 'semantic-ui-react'

import projectStore from "/store/projectStore";
import userStore from "../../store/userStore";
import uiStateStore from "../../store/uiStateStore";
import {ProjectType, UserStatus} from "../../config/Constants";

@observer
export default class JoinPrompt extends Component {

    state = {
        newUserName: '',
        newUserPass: '',
        newUserCreated: false,
        showNameError: false,
        showPWError: false
    }

    handleOpen = (ev) => {
        console.log ('handleOpen')
        ev.stopPropagation ()
    }

    handleClose = (ev) => {
        uiStateStore.setJoinPromptState (false)
    }

    onKeyUp = (ev) => {
        if (ev.key === 'Enter') {
            ev.preventDefault()
            ev.stopPropagation()
            this.customizeNewUser(ev)
        }
    }

    onNewUserChange = (ev) => {
        const propName = ev.target.name
        const propValue = ev.target.value
        this.setState ({[propName]: propValue})
    }

    customizeNewUser = (ev) => {
        const {newUserName = '', newUserPass = ''} = this.state
        const project = projectStore.currentProject
        const isApproval = project && project.projectType === ProjectType.APPROVAL

        if (newUserName.length > 0 && (newUserPass.length > 0) || isApproval) {
            this.setState({showNameError: false, showPWError: false})
            const newUser = userStore.currentUser
            newUser.displayName = newUserName
            newUser.password = newUserPass
            newUser.status = UserStatus.NORMAL
            userStore.saveUser (newUser).then (updatedUser => {
                userStore.setCurrentUser (updatedUser, true)
                if (this.props.onUserJoin)
                    this.props.onUserJoin()
                this.handleClose (ev)
            })
        } else {
            if (newUserName.length === 0)
                this.setState({showNameError: true})
            if (newUserPass.length === 0 && !isApproval)
                this.setState({showPWError: true})
        }

    }

    render() {
        const {newUserName, newUserPass, showNameError, showPWError} = this.state

        const project = projectStore.currentProject
        const me = userStore.currentUser
        const isAnon = !me || me.status === UserStatus.ANON
        const showPrompt = uiStateStore.showJoinPrompt
        const forMobxOnly = uiStateStore.rand
        const isApproval = project && project.projectType === ProjectType.APPROVAL

        const pwPlaceholder = 'Password' +(isApproval? ' (optional)':'')

        return (
            <Modal size={'tiny'}
                   open={showPrompt}
                   onClose={this.handleClose}
                   onOpen={this.handleOpen}

            >
                <div className='jFG jSideBG pad1 large'>Please tell us your name or email:</div>
                <div className='pad1 jFGinv jBGinv small'>
                    <input name='newUserName' type='text' placeholder='Name or email'
                           style={{width: '75%', padding: '.55rem 1rem'}}
                           value={newUserName} onChange={this.onNewUserChange} onKeyUp={this.onKeyUp}/>
                    {showNameError &&
                    <div className='small red mt05 ml1 mb1' >
                        Please specify a valid name.
                    </div>
                    }
                    <br/>
                    <input name='newUserPass' type='password' placeholder={pwPlaceholder} className='mt1'
                           style={{width: '75%', padding: '.55rem 1rem'}}
                           value={newUserPass} onChange={this.onNewUserChange} onKeyUp={this.onKeyUp}/>
                    {showPWError &&
                    <div className='small red mt05 ml1 mb1' >
                        Please specify a password.
                    </div>
                    }

                    <div>
                        <button className='button neutral mt1' onClick={this.handleClose}>
                            Cancel
                        </button>
                        <button className='button pink mt1' onClick={this.customizeNewUser}>
                            Get Started!
                        </button>
                        <div className='small mt1'>Already a member?
                            <a href={"/login/" + (project? project.id+"?"+project.id:'')} className="loginLink"> Login</a>
                        </div>
                    </div>
                </div>
            </Modal>


        )
    }
}