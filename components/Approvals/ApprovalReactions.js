import React, {Component} from 'react'
import {observer} from 'mobx-react'
import {ArrowDown, ArrowUp, CheckCircle, ChevronLeft, ChevronRight, Frown, MessageSquare, Smile} from "react-feather";
import ReactionApiService, {voteTypeToId} from "../../services/ReactionApiService";
import {THUMBS, THUMBS_DOWN, THUMBS_UP} from "../../config/ReactionConfig";
import projectStore from "../../store/projectStore";
import userStore from "../../store/userStore";
import {ProjectType, UserStatus} from "../../config/Constants";
import uiStateStore from "../../store/uiStateStore";

const ActiveColor = '#aaa'
const HoverColor = '#bbb'

@observer
export default class ApprovalReactions extends Component {

    state = {reactionSummary: null}

/*
    gotoPreviousCard = (ev) => {
        ev.preventDefault()
        const {selectedIndex} = this.props
        if (selectedIndex >= 0)
            this.props.selIndex(selectedIndex-1)
    }

    gotoNextCard = (ev) => {
        ev.preventDefault()
        const {selectedIndex,cards} = this.props
        if (selectedIndex < cards.length-1)
            this.props.selIndex(selectedIndex+1)
    }
*/

    vote(cardId, voteType, isAnon) {
        if (isAnon)
            uiStateStore.setJoinPromptState(true)
        else {
            ReactionApiService.registerVoteReaction (cardId, voteType).then (response => {
                if (this.props.onVote)
                    this.props.onVote (response.data)
            })
        }
    }

    comment(cardId, isAnon) {
        // TODO
    }

    // TODO pass in reaction set from parent
    componentDidMount() {
        const project = projectStore.currentProject
        if (project && project.projectType === ProjectType.APPROVAL && !project.reactionSetId) {
            project.reactionSet = ReactionApiService.getReactionSetByTitleFromDefaults(THUMBS)
            project.reactionSetId = project.reactionSet? project.reactionSet.reaction_set_id : 0
        } else if (project && project.reactionSetId && !project.reactionSet) {
            project.reactionSet = ReactionApiService.getReactionSetByIdFromDefaults(project.reactionSetId)
        }
        if (project)
            this.setState({rset: project.reactionSet})

        this.thumbsUpId = voteTypeToId(THUMBS_UP)
        this.thumbsDownId = voteTypeToId(THUMBS_DOWN)
        this.starsId = ReactionApiService.getStarsId()

        /*
                if (!this.state.rsum) {
                    getCardReactions (this.props.cardId, false).then (response => {
                        this.setState ({reactionSummary: response.data})
                    })
                }
        */

    }

    render() {
        const {project, cardId, size, rsum, atFirst, atLast, showNav,
               selCard, selIndex, selectedIndex, selectedCard, onVote} = this.props

        // if (!rsum)
        //     return null

        // const {hoverStarNum, showStats} = this.state
        const me = userStore.currentUser
        const isAnon = !me || me.status === UserStatus.ANON

        const reactions = rsum? Array.isArray(rsum)? rsum : (rsum.card_reactions? rsum.card_reactions : []) : []
        const isApproved = !isAnon && reactions.find(r => r.user_id === me.id && r.reaction_type_id === this.thumbsUpId) !== undefined
        const isRejected = !isAnon && reactions.find(r => r.user_id === me.id && r.reaction_type_id === this.thumbsDownId) !== undefined
        const isRated = !isAnon && reactions.find(r => r.user_id === me.id && r.reaction_type_id === this.starsId) !== undefined
        let rating = 0
        // TODO get rating value, and then add stars.

        const downStyle = {backgroundColor: 'unset', border: '1px solid #c80000'}

        return (
            <div className='inline vaTop'>

                {size === 'feed' &&
                <span onClick={() => this.vote (cardId, THUMBS_UP, isAnon)}
                      className={'inline round mr1 animBG vaTop ' +(isApproved? 'jUpvoteBG':'jVoteBG')}
                      data-tip="Approve"
                      style={{padding: '2px', height: '26px', width: '26px'}}
                >
                    <img src='/static/svg/thumbsUp.svg'
                         width={21} height={21}
                         className='apUtReactions pointer hoh '
                         style={{marginBottom: '-1px', marginLeft: '1px'}}/>
                </span>
                }
                {size !== 'feed' &&
                <span className={'inline capsule mr1 vaTop jUpvoteBG ulc ' +(isApproved? 'sel':'')}
                      data-tip="Approve"
                      style={{padding: '2px 18px', height: '32px'}}
                      onClick={() => this.vote (cardId, THUMBS_UP, isAnon)}>
                    <img src='/static/svg/thumbsUp.svg'
                         width={24} height={24}
                         className='apUtReactions pointer hoh '
                         style={{marginBottom: '-1px', marginLeft: '1px'}}/>
                </span>
                }

                {size === 'feed' &&
                <span onClick={() => this.vote (cardId, THUMBS_DOWN, isAnon)}
                      className={'inline round mr1 animBG vaTop ' +(isRejected? 'jDownvoteBG':'jVoteBG')}
                      data-tip="Dislike"
                      style={{padding: '2px', height: '26px', width: '26px'}}
                >
                    <img src='/static/svg/thumbsDown.svg'
                         width={20} height={20}
                         className='pointer hoh '
                         style={{marginTop: '6px', marginLeft: '2px'}}/>
                </span>
                }
                {size !== 'feed' &&
                <span className={'inline capsule mr1 vaTop jDownvoteBG ulc ' +(isRejected? 'sel':'')}
                      data-tip="Dislike"
                      style={{padding: '2px 18px', height: '32px'}}
                      onClick={() => this.vote (cardId, THUMBS_DOWN, isAnon)}>
                    <img src='/static/svg/thumbsDown.svg'
                         width={23} height={23}
                         className='pointer hoh '
                         style={{marginTop: '8px', marginLeft: '2px'}}/>
                </span>
                }

{/*
                {size !== 'feed' && isApproved &&
                <span className='inline jUpvote mt05'>Approved by me.</span>
                }
                {size !== 'feed' && isRejected &&
                <span className='inline jDownvote mt05'>Disliked by me.</span>
                }
*/}

            </div>
        )
    }
}

