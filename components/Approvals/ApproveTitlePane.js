import {Component} from 'react'
import {Link} from '~/routes'
import {DropTarget} from 'react-dnd'
import {Router} from '~/routes'

import {observer} from 'mobx-react'

import HelpButton from "../help/HelpPopup";
import {Check, Menu as FMenu} from 'react-feather'
import ProfileMenu from "/components/common/ProfileMenu";
import UserSessionUtils from '/utils/UserSessionUtils'
import ReactTooltip from 'react-tooltip'

import projectStore from '/store/projectStore'
import cardStore from '/store/cardStore'
import userStore from '/store/userStore'
import uiStateStore from '/store/uiStateStore'
import LoginMenu from "../common/LoginMenu";
import {ARCHIVED_CAT, DndItemTypes, UserRole, UserStatus} from "../../config/Constants";
import RU from "../../utils/ResponsiveUtils";
import EventDetailsPopup2 from "../events/EventDetailsPopup2";
import Toaster from "../../services/Toaster";
import {getUserApprovals, leaveApproval, rejoinApproval} from "../../services/ApprovalService";
import APUserTour from "../help/APUserTour";
import {truncate} from "../../utils/solo/truncate";
import categoryStore from "../../store/categoryStore";

// import store from 'store2'

const MAX_MEMBERS_DISPLAY = 40

const coverImageTarget = {
    drop(props, targetMonitor, component) {
        let dragItem = targetMonitor.getItem ()

        // use card image as cover
        if (dragItem.type === DndItemTypes.CARD) {
            const card = cardStore.getCardFromCurrent(dragItem.id)
            if (card && card.image)
                component.handleImageChange (card.image)
        }
    }
}

@DropTarget([DndItemTypes.CARD], coverImageTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    item: monitor.getItem()
}))
@observer
export default class ApproveTitlePane extends Component {
    constructor(props) {
        super(props)
        this.state = {dropzoneActive: false, largeCover: false, titleIsEditing: false, descIsEditing: false, showInfo: false, showMembers: false, loading: false}
    }

    logout = () => {
        UserSessionUtils.endUserSession()
        window.location.href = '/login'
    }

    showLargeCover = () => this.setState({largeCover: true})
    hideLargeCover = () => this.setState({largeCover: false})

    startEditing() {
        this.setState({titleIsEditing: true, descIsEditing: true})
    }

    toggleMobileSidebar = () => {
        uiStateStore.toggleMobSidebarL()
    }

    showInfo(show) {
        this.setState({showInfo: show})
    }

    showMembers(show) {
        this.setState({showMembers: show})
    }

    onInfoOpen(ev) {
        this.setState({showInfo: true})
        ReactTooltip.rebuild()
    }

    onInfoClose(ev) {
        this.setState({showInfo: false})
    }

    onMembersOpen(ev) {
        this.setState({showMembers: true})
        ReactTooltip.rebuild()
    }

    onMembersClose(ev) {
        this.setState({showMembers: false})
    }

    openConvo = (ev) => {
        uiStateStore.showMobSidebarR = true
    }

    copyAPLink = () => {
        const url = "https://pogo.io/approve/" + projectStore.currentProject.id
        const linkNode = document.querySelector('#hdnLink')
        linkNode.value = url
        linkNode.select()
        document.execCommand('copy')
        Toaster.info('Link to Approval copied: ' +url)
    }

    markAsDone = () => {
        const project = projectStore.currentProject
        if (project) {
            leaveApproval(project.id).then( response => {
                categoryStore.moveProjectCategory(ARCHIVED_CAT, project.id).then( response => {
                // getUserApprovals(userStore.currentUser.id).then( response => {
                    Router.pushRoute('/')
                })
            }).catch(error => {
                console.error(error)
            })
        }
    }

    unmarkAsDone = () => {
        const project = projectStore.currentProject
        if (project) {
            rejoinApproval(project.id).then( response => {
                projectStore.fetchCurrentProject(project.id, null, false)
            }).catch(error => {
                console.error(error)
            })
        }
    }

    componentDidMount() {
        const project = projectStore.currentProject
        if (project) {
            userStore.getUserById(project.creatorId) // sets userStore.currentCreator
        }
    }

    componentDidUpdate() {
        console.log('ap title pane update')
    }

    render() {
        const {loading, largeCover, dropzoneActive} = this.state
        const {project, mobile, user, isOver, item, connectDropTarget} = this.props
        const me = user // userStore.currentUser
        if (!project)
            return null
        const creator = userStore.getUserFromConnections(project.creatorId)
        const meInProject = project.members.find( mbr => mbr.id === me.id) || null
        const imDone = meInProject && meInProject.role === UserRole.INACTIVE_USER

        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON

        const title = project? project.title : 'Loading Project...'
        const titleStyle = {margin: '0.5rem 2rem 0 1rem', fontSize: imDone? '':'1.4rem', color: imDone? 'gray':'white'}
        const startTour = userStore.userPrefs && userStore.userPrefs.autoShowTour

        if (mobile) {
            return (
                <header className="mFraming">
                    <div className='mb1'>
                        <div className='left mr1'>
                            <FMenu className='apUtSB1 pointer' onClick={() => this.toggleMobileSidebar()}/>
                        </div>
                        <div className='dontWrap' style={{maxWidth: '80%'}}>
                            {truncate(title, 20, true)}
                        </div>
                        <div className='absTopRight' style={{marginRight: '-1rem', minWidth: '7rem'}}>
                            {false && !isAnon &&
                            <EventDetailsPopup2 />
                            }

                            <HelpButton createProject={this.create} className='mr1 inline'/>

                            {startTour &&
                            <APUserTour/>
                            }

                            <ProfileMenu user={me} mobile/>
                        </div>
                    </div>
                    <div className='jItemBG relative' style={{marginLeft: '-1rem', width: 'calc(100% + 1rem)', padding: '5px 1rem 5px 1rem'}}>
                        <button className='apUtDone jBtn vaTop tiny lighter pointer mr1 krn1' style={{minWidth: '5rem'}}
                                onClick={this.markAsDone}
                                data-tip="Mark that you're finished approving"
                        >
                            MARK AS DONE
                        </button>

                        <button className='jBtn vaTop tiny lighter pointer mr1 krn1' style={{minWidth: '5rem'}}
                                onClick={this.copyAPLink}
                                data-tip="Copy link to share this approval"
                        >
                            COPY LINK
                        </button>
                        <input id='hdnLink' type='text' className='abs' style={{opacity: 0, top: '-10rem'}}/>

                        <div className='apUtConvo abs' style={{top: '5px', right: '1rem'}}>
                            <img src='/static/svg/v2/messageCircle.svg' className='pointer vaTop' width={30} onClick={this.openConvo}/>
                        </div>
                    </div>
                </header>
            )
        }

        // large
        return connectDropTarget(
            <header className="jMidBG titlePane" >
                <div className="row1 ">
                    <div className="inline left">
                        <Link route='projectsPage'>
                            <img src="/static/img/pogoMulti.png" width="84px" style={{cursor: 'pointer'}} data-tip="Dashboard"/>
                        </Link>
                    </div>
                    <div className="inline" style={{marginLeft: '1rem'}}>
                        <div className="jFG lighter krn1" style={titleStyle}>
                              {title}
                        </div>
                        {false && creator &&
                        <div className='secondary'>
                            From {creator.displayName || creator.firstName}
                        </div>
                        }
                     </div>

                    <div className="absTopRight" style={{textAlign: 'right'}}>

                        <button className='button go vaTop tiny lighter pointer mr1 krn1' style={{minWidth: '5rem', marginTop: 0}}
                                onClick={this.markAsDone}
                                data-tip="Mark that you're finished approving"
                        >
                            I AM DONE APPROVING THIS!
                        </button>

                        <button className='jBtn vaTop tiny lighter pointer mr1 krn1' style={{minWidth: '5rem'}}
                                onClick={this.copyAPLink}
                                data-tip="Copy link to share this approval"
                        >
                            COPY LINK TO THIS APPROVAL
                        </button>
                        <input id='hdnLink' type='text' className='abs' style={{opacity: 0, top: '-10rem'}}/>

                        <EventDetailsPopup2 />

                        <HelpButton createProject={this.create} className='mr1' popupStyle={{right:0}}/>

                        {me &&
                        <ProfileMenu className="right" user={me} />
                        }

                        {!me &&
                        <LoginMenu className="right" project={project}/>
                        }
                    </div>
                </div>
            </header>
        )
    }
}
