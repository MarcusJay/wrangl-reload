import React, {Component} from 'react'
import {Router} from '~/routes'
import RMoment from 'react-moment'
import DumbBadge from "../common/DumbBadge";
import projectStore from "../../store/projectStore";
import {Divider, Loader, Menu, Popup} from "semantic-ui-react";
import MemberAvatar from "../common/MemberAvatar";
import {ProjectStatus, UserRole} from "../../config/Constants";
import uiStateStore, {SendForApproval} from "../../store/uiStateStore";
import AtCard from "../Conversations/AtCard";
import DumbCapsule from "../common/DumbCapsule";

export default class ApprovalThumb extends Component {
    state = {loading: false, menu: false, requestor: null, approvers: []}

    openMenu = (ev) => {
        const {ap, badgeCount} = this.props
        this.setState({loading: true, menu: true})
        const requestor = ap.members? ap.members.find(m => m.role === UserRole.REQUESTOR) : null
        const approvers = ap.members? ap.members.filter(m => m.role === UserRole.APPROVER) : []

        // TODO remove this once userProjects includes cards in approval projects.
        projectStore.getProjectById(ap.id).then( project => {
            ap.cards = project.cards
            ap.cardTotal = project.cardTotal
            this.setState({loading: false, requestor: requestor, approvers: approvers}) // TODO move this out of closure when userProjects fixed
        })

    }

    closeMenu = (ev) => {
        this.setState({menu: false})
    }

    approveNow = () => {
        const {ap} = this.props
        if (ap)
            Router.pushRoute('/approve/'+ap.id, {id: ap.id})
    }

    editRequest = () => {
        const {ap} = this.props
        uiStateStore.currentApprovalRequest = ap
        uiStateStore.rightUnModal = SendForApproval
        this.closeMenu()
    }

    cancelRequest = () => {
        const {ap} = this.props
        this.props.cancelRequest(ap)
        projectStore.deleteProject(ap) // we just changed the status to cancelled. Do we still want to delete?
        this.closeMenu()
    }

    completeRequest = () => {
        const {ap} = this.props
        if (ap) {
            this.props.completeRequest (ap)
            this.closeMenu ()
        }
    }

    leaveApproval = () => {
        const {ap} = this.props
        if (ap) {
            this.props.leaveApproval (ap)
            this.closeMenu ()
        }
    }

    render() {
        const {ap, badgeCount, isNew, received, sent, complete, activeUser} = this.props
        const {menu, requestor, approvers, loading} = this.state
        const image = ap.image || null // TODO default image?
        const status = ap.status === ProjectStatus.OPEN? 'Open' : ap.status === ProjectStatus.COMPLETED? 'Completed' : null
        const statusClass = ap.status === ProjectStatus.OPEN? 'jBlue' : ap.status === ProjectStatus.COMPLETED? 'jPink' : null

        // const yesOrNo = Math.random() > 0.5

        if (false && received) {
            return (
            <div className="row noWrap relative" onClick={this.approveNow}
                 data-tip={'Open "' +ap.title+ '"'} data-place="right">
                <div className="image inline suitcaseM" style={{backgroundImage: 'url(' + (image) + ')'}}> </div>
                {badgeCount > 0 &&
                <DumbBadge count={badgeCount} className='abs' style={{top: '5px', right: '10px'}} />
                }
                <span className="noWrap inline small light ctitle" >{ap.title}</span>
            </div>
            )

        }

        else if (true || sent)
          return (
            <Popup
                trigger={
                    <div className={"row noWrap relative" + (isNew? ' new':'')}
                         data-tip={'Open "' +ap.title+ '"'} data-place="right">
                        <div className="image inline suitcaseM" style={{backgroundImage: 'url(' + (image) + ')'}}> </div>

                        {badgeCount > 0 &&
                        <DumbBadge count={badgeCount} className='abs' style={{top: '2px', left: '23px'}} />
                        }

                        {isNew && badgeCount === 0 &&
                        <DumbCapsule msg='New' className='abs' style={{top: '0', right: '0'}} />
                        }

                        <span className="noWrap inline small light ctitle" >{ap.title}</span>
                    </div>
                }
                on='click'
                open={menu}
                onOpen={(ev) => this.openMenu(ev)}
                onClose={(ev) => this.closeMenu(ev)}
                position='bottom left'
                style={{padding: '1rem', minWidth: '30rem', overflow: 'hidden'}}
                hoverable
            >

                {image &&
                <div className="image cover inline suitcaseM mr1"
                     style={{minHeight: '3rem', minWidth: '3rem', backgroundImage: 'url(' + (image) + ')'}}>&nbsp;</div>
                }
                <div className='inline vaTop bold'
                    style={{margin: '.25rem -.5rem .25rem 0'}}
                >{ap.title}</div>

                <Menu vertical style={{width: '100%', zIndex: '1003 !important'}}>
                    <Menu.Item name="nothing" >
                        {false &&
                        <button className={'jBtn tiny lighter mr05 krn1 vaTop'}
                                style={{minWidth: '5rem', marginTop: '.15rem'}}
                                onClick={this.cancelRequest}
                                data-tip="Cancel this approval request"
                        >
                            CANCEL
                        </button>
                        }

                        {sent && !complete &&
                        <button className={'jBtn tiny lighter mr05 krn1 vaTop'}
                                style={{minWidth: '5rem', marginTop: '.15rem'}}
                                onClick={this.editRequest}
                                data-tip="Edit this approval request"
                        >
                            EDIT
                        </button>
                        }

                        <button className={'jBtn tiny lighter mr05 krn1 vaTop'}
                                style={{minWidth: '5rem', marginTop: '.15rem'}}
                                onClick={this.approveNow}
                                data-tip="Start approving items now"
                        >
                            VIEW
                        </button>

                        {sent && !complete &&
                        <button className={'jBtn tiny lighter mr05 krn1 vaTop'}
                                style={{minWidth: '5rem', marginTop: '.15rem'}}
                                onClick={this.completeRequest}
                                data-tip="Close this approval request as completed"
                        >
                            COMPLETE
                        </button>
                        }

                        {received &&
                        <button className={'jBtn tiny lighter mr05 krn1 vaTop'}
                                style={{minWidth: '5rem', marginTop: '.15rem'}}
                                onClick={this.leaveApproval}
                                data-tip="I'm done. Don't show me this again."
                        >
                            MARK AS DONE
                        </button>
                        }

                    </Menu.Item>

                </Menu>

                {requestor && ap.dateCreated &&
                <div className='small lighter'>
                    <div className='inline dontWrap' style={{width: '50%'}}>Requested by {requestor.displayName || requestor.firstName}:</div>
                    <div className='inline dontWrap' style={{width: '50%'}}>
                        <RMoment subtract={{hours: 7}} format={'MMM Do, h:mm a'}>{ap.dateCreated}</RMoment>.
                    </div>
                </div>
                }

                {requestor && !ap.dateCreated &&
                <div className='small lighter'>
                    Requested by {requestor.displayName || requestor.firstName}
                </div>
                }

                {!requestor && ap.dateCreated &&
                <div className='small lighter'>Requested
                    <RMoment subtract={{hours: 7}} format={'MMM Do, h:mm a'}>{ap.dateCreated}</RMoment>.
                </div>
                }
                {ap.dateModified &&
                <div className='small lighter'>
                    <div className='inline' style={{width: '50%'}}>Last activity:</div>
                    <div className='inline' style={{width: '50%'}}>
                        <RMoment subtract={{hours: 7}} format={'MMM Do, h:mm a'}>{ap.dateModified}</RMoment>.
                    </div>
                </div>
                }

                {status && sent &&
                <div className='small lighter'>
                    <div className='inline' style={{width: '50%'}}>Status:</div>
                    <div className='inline' style={{width: '50%'}}>
                        <span className={statusClass}>{status}</span>
                    </div>
                </div>
                }

                {false && approvers && approvers.length > 0 &&
                <div className='apvrs mt1'>
                    <div className='small lighter secondary' style={{marginBottom: '1rem'}}>Approvers</div>
                    {approvers.map( (apr,i) => {
                    return <MemberAvatar
                                  member={apr}
                                  activeUser={activeUser}
                                  enableAdmin={false}
                                  project={projectStore.currentProject}
                                  refresh={this.refreshMembers}
                                  key={'apr'+i}
                                  index={i}
                                  color="black"
                                  showName={true}
                                  isEnabled={false}
                                  showMenu={false}
                                  showAttmMenu={true}
                    />
                    })}

                </div>
                }

                {approvers && approvers.length > 0 &&
                <div className='small secondary '>
                     {approvers.length} approver{approvers.length !==1? 's':''}
                </div>
                }
                {!approvers || approvers.length === 0 &&
                <div className='small secondary pointer' onClick={this.viewRequest}
                      style={{textDecoration: 'underline'}}>Add approvers
                </div>
                }

                {false && !loading && ap.cards && ap.cards.length > 0 &&
                <div className='apvrs mt1'>
                    <div className='small lighter secondary' style={{marginRight: '1rem'}}>Items</div>
                    {ap.cards.map( (card,i) => {
                        return <AtCard
                            key={'atc'+i}
                            card={card}
                            viewCard={null}
                            showTitle={true}
                            showAttmMenu={false}
                        />
                    })}

                </div>
                }

                {!loading && ap.cards && ap.cards.length > 0 && sent &&
                <div className='small secondary'>
                    {ap.cards.length} item{ap.cards.length!==1?'s':''} to approve
                </div>
                }

                {!loading && (!ap.cards || ap.cards.length === 0) &&
                <div className='small secondary pointer'>
                     No items
                </div>
                }

                <Loader active={loading}></Loader>

            </Popup>

          )
    }
}

