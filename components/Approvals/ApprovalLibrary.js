import {Component} from 'react'
import {observer} from 'mobx-react'
import {Link, Router} from '~/routes'
import {SearchConfig} from '/config/Constants'

import {Form, Icon, Input, Loader} from 'semantic-ui-react'
import projectStore from '/store/projectStore'
import userStore from "/store/userStore";
import eventStore from "/store/eventStore"
import {
    cancelApproval,
    completeApproval,
    getUserApprovals,
    iHaveCompletedApproval, leaveApproval,
    searchApprovals
} from "../../services/ApprovalService";
import ApprovalThumb from "./ApprovalThumb";
import {APPROVAL_CAT, ProjectStatus, REQUEST_CAT, UserRole} from "../../config/Constants";
import {ChevronDown, ChevronUp} from "react-feather";
import DumbBadge from "../common/DumbBadge";
import uiStateStore from "../../store/uiStateStore";

@observer
export default class ApprovalLibrary extends Component {
    constructor(props) {
        super(props)
        this.state = {loading: false, searching: false, query: '', searchResults: projectStore.userApprovals || [],
                      isVirgin: true, isOpen: false,
                      onMeOpen: false, onOthersOpen: false, completedOpen: false,
                      onMeBadge: true, onOthersBadge: true, completedBadge: false
        }
    }

    toggleOpen = (ev) => {
        const newIsOpen = !this.state.isOpen
        if (newIsOpen && !uiStateStore.showSidebar)
            uiStateStore.toggleSidebar()
        this.setState({isOpen: newIsOpen})
    }

    onSearchChange(ev) {
        let query = ev.target.value
        console.log('change: query = ' +query)
        let now = new Date()
        let elapsed = this.state.lastKeyPress === 0? 0 : now - this.state.lastKeyPress
        this.setState({query: ev.target.value, lastKeyPress: now})

        setTimeout( () => {
            if (query.length >= SearchConfig.MIN_SEARCH_CHARS) {
                this.submitSearch (query)
            } else if (!this.state.isVirgin) {
                console.log ('Clearing search')
                this.setState ({searchResults: projectStore.userApprovals})
                projectStore.isFiltering = false
                // this.submitSearch ('')
            } else {
                console.log('Skipping search ' + query + ' at ' +elapsed+ 'ms')
            }
            this.setState ({waiting: false})

        }, SearchConfig.QUERY_DELAY_MS )

    }

    submitSearch(query) {
        console.log("Searching for " +query+ "...")

        this.setState ({searching: true, isVirgin: false})

        searchApprovals(query.toLowerCase()).then (approvals => {
            this.setState ({
                searching: false,
                searchResults: approvals || []
            })
        }).catch (error => {
            console.error ("Error searching for " + query, error)
            this.setState ({searching: false})
        })
    }

    gotoProject(project) {
        this.setState({loading: true})
        Router.pushRoute('approve', {id: project.id}) // Because <Link> appears to be unreliable.
    }

    completeRequest = (ap) => {
        const sent = projectStore.approvalsSent
        const apIndex = sent.findIndex (project => project.id === ap.id)
        if (apIndex)
            sent.splice(apIndex, 1)
        completeApproval (ap.id).then( response => {
            getUserApprovals(userStore.currentUser.id) // trigger mobx refresh, But they're coming back with status 0.
        })
    }

    cancelRequest = (ap) => {
        // delete immediately then sync
        const sent = projectStore.approvalsSent
        const apIndex = sent.findIndex (project => project.id === ap.id)
        if (apIndex)
            sent.splice(apIndex, 1)
        cancelApproval(ap.id).then( response => {
            getUserApprovals(userStore.currentUser.id) // trigger mobx refresh, But they're coming back with status 0.
        })
    }

    leaveApproval = (ap) => {
        const received = projectStore.approvalsReceived
        const apIndex = received.findIndex (project => project.id === ap.id)
        if (apIndex)
            received.splice(apIndex, 1)
        leaveApproval (ap.id).then( response => {
            getUserApprovals(userStore.currentUser.id) // trigger mobx refresh, But they're coming back with status 0.
        })
    }


    toggleSection = (sectionName) => {
        const newOpenState = !this.state[sectionName+'Open']
        const newBadgeState = !this.state[sectionName+'Badge']
        this.setState({[sectionName+'Open']: newOpenState, [sectionName+'Badge']: newBadgeState})
    }

    componentDidMount() {
        const userId = userStore.currentUser.id

        if (projectStore.approvalsReceived.length === 0 || projectStore.approvalsSent.length === 0) {
            this.setState({loading: true})
            getUserApprovals(userId).then ( (approvals) => {

                // TODO In order to determine which received approvals are completed by ME ONLY, we need to READ EACH PROJECT TO GET CARDS
                // expensive. Me no like. Let's determine better rule for marking completion.
/*
                const received = projectStore.approvalsReceived
                if (received && received.length > 0) {
                    let readCalls = []
                    received.forEach( ap => {
                        readCalls.push( projectStore.getProjectById(ap.id))
                    })
                    Promise.all (readCalls).then( projects => {
                        for (let i=0; i<projects.length; i++) {
                            received[i].cards = projects[i].cards
                        }
                    })
                }
*/

                this.setState ({loading: false, searchResults: approvals})
            })
        }
    }

    componentWillReceiveProps() {
        this.setState({thisWillTrigger: 'anUpdate'})
    }

    render() {
        const {badgeTotal, isMini} = this.props
        const {loading, isOpen, query, onMeOpen, onOthersOpen, completedOpen, searchResults} = this.state
        const isFiltering = projectStore.isFiltering
        const sent = isFiltering? projectStore.filteredAPSent : projectStore.approvalsSent
        const received = isFiltering? projectStore.filteredAPReceived: projectStore.approvalsReceived
        const sentOpen = sent.filter (ap => ap.status === ProjectStatus.OPEN) || []
        const receivedOpen = received.filter (ap => ap.status === ProjectStatus.OPEN) || []
        const receivedNotCompleteByMe = receivedOpen.filter( ap => !iHaveCompletedApproval(ap)) || {}
        const receivedAndLeft = receivedOpen.filter( ap => iHaveCompletedApproval(ap)) || []

        const archived = receivedAndLeft.concat(sent.filter (ap => ap.status === ProjectStatus.COMPLETED) || [])
                         .concat( received.filter(ap => ap.status === ProjectStatus.COMPLETED) || [])

        const paneClass = 'pointer relative' +(this.props.mobile? ' m-side-pane':' side-pane')

        const evs = eventStore.getInstance()
        const cagBadges = evs.currentBadges? evs.currentBadges.categories : null
        const onMeBadgeInfo = cagBadges? cagBadges[APPROVAL_CAT] : null
        const onOthersBadgeInfo = cagBadges? cagBadges[REQUEST_CAT] : null
        const onMeBadgeTotal = onMeBadgeInfo? onMeBadgeInfo.total : 0
        const onOthersBadgeTotal = onOthersBadgeInfo? onOthersBadgeInfo.total : 0

        let projectBadgeDetails, apBadgeCount, isAPNew

/*
        if (!isOpen)
            return (
                <section onClick={this.toggleOpen} className='pointer'>
                    <img src='/static/svg/v2/checkCircleInv.svg' width='24px'
                         className='ml1 mr1 mt2 inline ' onClick={this.showApprovals}
                         data-tip="View Approvals"/>

                    <div className='tiny kern1 inline jFG vaTop' style={{marginTop: '2.15rem'}}>
                        {/!*{sentOpen.length + receivedOpen.length} OPEN *!/}APPROVALS
                    </div>
                </section>

            )
*/


        return(
            <section className={paneClass} >
                {/*<Loader active={loading}></Loader>*/}

                {!loading &&
                <div className='thumbLibrary'>
                    <div className={'paneHdr mt2 ' +(isOpen? 'exp':'col')} onClick={this.toggleOpen}>
                        <img src='/static/svg/v2/checkCircleInv.svg' width='24px'
                             className='ml1 mr1 inline animPos'
                             data-tip="View Approvals"/>

                        {badgeTotal > 0 &&
                        <DumbBadge count={badgeTotal} className='abs' style={{top: '-.25rem', left: '2rem'}}/>
                        }

                        {!isMini &&
                        <div className='tiny kern1 inline vaTop' style={{marginTop: '.15rem'}}>
                            {/*{sentOpen.length + receivedOpen.length} OPEN */}APPROVALS
                        </div>
                        }
                    </div>

                    {!isMini &&
                    <div className={'paneContent ' + (isOpen ? 'exp' : 'col')}>
                        <Form inverted className="thumbSearch mt05 mb1">
                            <Input icon={<Icon name="search"/>}
                                   name='Search'
                                   className="sidePaneInput small lighter  mb1"
                                   style={{marginLeft: '.5rem', marginRight: '.5rem', width: '90%'}}
                                   data-tip='Search approvals by name and description'
                                   onChange={(ev) => {
                                       this.onSearchChange (ev)
                                   }}
                                   value={query}
                                   id="apSearch"
                                   placeholder='Search approvals...'
                            />
                        </Form>


                        {/*SECTION HEADER*/}
                        <div className={'tiny relative kern1 jFG mt1 jItemBG secHdr' + (onMeOpen ? ' active' : '')}
                             style={{padding: '1rem .25rem 1rem 1rem'}}
                             onClick={() => this.toggleSection ('onMe')}>
                            NEEDS MY APPROVAL: {receivedNotCompleteByMe.length}
                            {onMeOpen && !isFiltering &&
                            <ChevronUp size={22} className='abs jItemBG round pointer hoh' style={{right: '4px'}}/>
                            }
                            {!onMeOpen && !isFiltering &&
                            <ChevronDown size={22} className='abs jItemBG round pointer hoh' style={{right: '4px'}}/>
                            }
                            {onMeBadgeTotal > 0 && !onMeOpen &&
                            <DumbBadge count={onMeBadgeTotal} className='abs' style={{top: '-5px', right: '-5px'}}/>
                            }
                        </div>

                        <div className={'paneContent ' + (onMeOpen? 'exp' : 'col')} style={{paddingLeft: '1rem'}}>
                            {(onMeOpen || isFiltering) && receivedNotCompleteByMe.map ((project, i) => {
                                //apBadgeCount = evs.unreadPConvoCounts[project.id] || 0
                                projectBadgeDetails = onMeBadgeInfo && onMeBadgeInfo.projects && onMeBadgeInfo.projects[project.id] ? onMeBadgeInfo.projects[project.id] : null
                                apBadgeCount = projectBadgeDetails ? projectBadgeDetails.total : 0
                                isAPNew = projectBadgeDetails ? Boolean (projectBadgeDetails.new) : false
                                return (
                                    <ApprovalThumb ap={project}
                                                   key={project.domId}
                                                   badgeCount={apBadgeCount}
                                                   isNew={isAPNew}
                                                   received={true}
                                                   leaveApproval={this.leaveApproval}
                                    />
                                )
                            })}
                        </div>

                        {/*SECTION HEADER*/}
                        <div className={'tiny kern1 jFG mt05 jItemBG secHdr' + (onOthersOpen ? ' active' : '')}
                             style={{padding: '1rem .25rem 1rem 1rem'}}
                             onClick={() => this.toggleSection ('onOthers')}>
                            WAITING ON OTHERS: {sentOpen.length}
                            {onOthersOpen && !isFiltering &&
                            <ChevronUp size={22} className='abs jItemBG round pointer hoh' style={{right: '4px'}}/>
                            }
                            {!onOthersOpen && !isFiltering &&
                            <ChevronDown size={22} className='abs jItemBG round pointer hoh' style={{right: '4px'}}/>
                            }
                            {onOthersBadgeTotal > 0 && !onOthersOpen &&
                            <DumbBadge count={onOthersBadgeTotal} className='abs' style={{top: '-5px', right: '-5px'}}/>
                            }
                        </div>

                        <div className={'paneContent ' + (onOthersOpen? 'exp' : 'col')} style={{paddingLeft: '1rem'}}>
                            {(onOthersOpen || isFiltering) && sentOpen.map ((project, i) => {
                                // apBadgeCount = evs.unreadPConvoCounts[project.id] || 0
                                projectBadgeDetails = onOthersBadgeInfo && onOthersBadgeInfo.projects && onOthersBadgeInfo.projects[project.id] ? onOthersBadgeInfo.projects[project.id] : null
                                apBadgeCount = projectBadgeDetails ? projectBadgeDetails.total : 0
                                isAPNew = projectBadgeDetails ? Boolean (projectBadgeDetails.new) : false

                                return <ApprovalThumb ap={project} badgeCount={apBadgeCount} sent={true}
                                                      key={project.domId}
                                                      isNew={isAPNew}
                                                      completeRequest={this.completeRequest}
                                                      cancelRequest={this.cancelRequest}
                                />

                            })}
                        </div>

                        {/*SECTION HEADER*/}
                        <div className={'tiny kern1 jFG mt05 jItemBG secHdr' + (completedOpen ? ' active' : '')}
                             style={{padding: '1rem .25rem 1rem 1rem'}}
                             onClick={() => this.toggleSection ('completed')}>
                            COMPLETED: {archived.length}
                            {completedOpen && !isFiltering &&
                            <ChevronUp size={22} className='abs jItemBG round pointer hoh' style={{right: '4px'}}/>
                            }
                            {!completedOpen && !isFiltering &&
                            <ChevronDown size={22} className='abs jItemBG round pointer hoh' style={{right: '4px'}}/>
                            }
                        </div>

                        <div className={'paneContent ' + (completedOpen? 'exp' : 'col')} style={{paddingLeft: '1rem'}}>
                            {(completedOpen || isFiltering) && archived.map ((project, i) => {
                                projectBadgeDetails = onOthersBadgeInfo && onOthersBadgeInfo.projects && onOthersBadgeInfo.projects[project.id] ? onOthersBadgeInfo.projects[project.id] : null
                                apBadgeCount = projectBadgeDetails ? projectBadgeDetails.total : 0
                                isAPNew = projectBadgeDetails ? Boolean (projectBadgeDetails.new) : false

                                return <ApprovalThumb ap={project} key={project.domId} badgeCount={apBadgeCount}
                                                      sent={true} complete={true}/>

                            })}
                        </div>
                    </div>
                    }
                </div>
                }
            </section>
        )
    }
}