import React, {Component} from 'react'
import {observer} from 'mobx-react'
import userStore from "../../store/userStore";
import ReactionApiService, {voteTypeToId} from "../../services/ReactionApiService";
import AuthProtector from "../common/AuthProtector";
import {THUMBS_DOWN, THUMBS_UP} from "../../config/ReactionConfig";
import {getCardReactions} from "../../services/CardApiService";
import ApprovalReactions from "./ApprovalReactions";
import {ProjectType, UserRole, UserStatus} from "../../config/Constants";
import MemberAvatar from "../common/MemberAvatar";
import {Icon, Loader, TextArea} from "semantic-ui-react";
import FeedItemComments from "./FeedItemComments";
import {findDOMNode} from "react-dom";
import {ArrowUpRight, Circle, Edit, Edit2, Link, X} from "react-feather";
import {reverseTruncate} from "../../utils/solo/reverseTruncate";
import {getShortUrl} from "../../utils/solo/getShortUrl";
import ImagePainter3 from "../common/Card/ImagePainter3";
import uiStateStore from "../../store/uiStateStore";
import PaintToolbar from "../ProjectDetail/WorkPane/PaintToolbar";
import {addLineBreaks} from "../../utils/solo/addLineBreaks";
import AttmThumb from "../unmodals/AttachmentsPane/AttmThumb";
import {getExt} from "../../utils/solo/getExt";
import {extToIcon, isPreviewable} from "../../config/FileConfig";

const MAX_MEMBERS = 7

@observer
export default class FeedItem extends Component {

    state = {
        rs: null, saving: false,
        currentCardId: null, newComment: '',
        showConvoArrow: false,
        title: 'Title', description: 'Description', url: null,
        showUrlEditor: false
    }

    onVote = (reactionResponse) => {
        // const atLastCard = this.props.index === this.props.total - 1
        this.getReactions ()
        this.props.onVote (reactionResponse)
    }

    onChange = (ev) => {
        this.setState ({newComment: ev.target.value})
    }

    onKeyPress = (ev) => {
        if (ev.key === 'Enter')
            this.addComment (ev)
    }

    focusCommentInput = (ev) => {
        const elem = findDOMNode (this)
        if (!elem)
            return
        const input = elem.querySelector ('input[type="text"]')
        if (input)
            input.focus ()

        // let's also add indicator that they can open full convo
        this.setState ({showConvoArrow: true})
    }

    addComment = (ev) => {
        const text = this.state.newComment
        const {card} = this.props
        this.props.addCommentOnFeedItem (text, card.id)
        this.setState ({newComment: ''})
    }

    handleTextChange = (ev) => {
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon) {
            uiStateStore.setJoinPromptState(true)
            return
        }

        const {card} = this.props
        const propName = ev.target.name
        this.setState({[propName]: ev.target.value})
        // card[propName] = propValue
    }

    handleTextSave = (ev) => {
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon) {
            uiStateStore.setJoinPromptState(true)
            return
        }
        const {card, onTextSave} = this.props
        const {title, description, url} = this.state
        // const propName = ev.target.name
        // const propValue = ev.target.value
        card.title = title
        card.description = description
        if (url)
            card.url = url
        onTextSave(card)
        ev.target.blur()
    }

    handleTextClick = (ev) => {
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon)
            uiStateStore.setJoinPromptState(true)
    }

    handleTextKeyPress = (ev) => {
        if (ev.key === 'Enter') {
            ev.preventDefault()
            ev.stopPropagation()
            this.handleTextSave (ev)
        }
    }

    onPaintSave = (ev) => this.props.onPaintSave(this.props.card.id)
    onPaintClear = (ev) => this.props.onPaintClear(this.props.card)
    onPaintCancel = () => {}

    gotoLink = (ev) => {
        if (this.props.card.url && window)
            window.open (this.props.card.url)
    }

    openUrlEditor = (ev) => {
        this.setState({showUrlEditor: true})
    }

    closeUrlEditor = (ev) => {
        this.setState({showUrlEditor: false})
    }

    openCardViewer = (ev) => {
        this.props.openCardViewer (this.props.card)
    }

    getReactions = () => {
        const {card, author, index, total, apStats} = this.props
        if (!card)
            return
        getCardReactions (card.id, true).then (response => {
            this.setState ({rs: response.data, currentCardId: card.id})
        })
    }

    togglePaint = (ev) => {
        uiStateStore.paintEnable = !uiStateStore.paintEnable
        uiStateStore.paintCardId = this.props.card.id
    }

    updateDimensions() {
        const img = this.refs.image
        if (img)
            this.setState({imgWidth: img.clientWidth, imgHeight: img.clientHeight})
    }

    componentDidMount() {
        const {card} = this.props
        this.thumbsUpId = voteTypeToId (THUMBS_UP)
        this.thumbsDownId = voteTypeToId (THUMBS_DOWN)
        this.starsId = ReactionApiService.getStarsId ()
        this.setState({title: card.title, description: card.description, url: card.url})
        this.getReactions ()
        this.updateDimensions()
    }

    componentDidUpdate() {
        const {card} = this.props
        const {currentCardId} = this.state
        if (card && currentCardId !== card.id) {
            this.getReactions ()
        }
    }

    render() {
        const {
            project, card, cards, comments, working, atFirst, atLast,
            selectedCard, selectedIndex
        } = this.props
        const {rs, showUrlEditor, newComment, title, description, url, imgWidth, imgHeight} = this.state

        if (!card || !project)
            return null

        const activeUser = userStore.currentUser
        const isAnon = !activeUser

        const memberCount = project ? (project.memberTotal + project.invitedMemberTotal) : 0
        let latestMembers = project.members ? project.members.filter (member => member.role !== UserRole.BANNED).slice (0, MAX_MEMBERS) : []
        const moreCount = memberCount - MAX_MEMBERS
        const shortUrl = card.url ? getShortUrl (card.url, true) : null

        const isApproval = project ? project.projectType === ProjectType.APPROVAL : false
        const requestor = isApproval && memberCount > 0 ? project.members.find (mbr => mbr.role === UserRole.REQUESTOR) : null
        const creator = !isApproval && memberCount > 0 ? project.creator || project.members.find (mbr => mbr.id === project.creatorId) : null
        const markup = card && card.annotations? card.annotations : []
        markup.sort( (a,b) => a.time_modified > b.time_modified? 1 : a.time_modified <  b.time_modified? -1 : 0)

        let ext, iconName, preview
        return (
            <div className='jFGinv jBGinv fullW mb1 relative' style={{borderRadius: '20px'}}>

                <div className='relative fullW'
                     style={{minHeight: '50px'}}
                >
                    <img src={card.image} className='fullW block' ref='image'
                         style={{objectFit: 'contain', borderTopLeftRadius: '20px', borderTopRightRadius: '20px'}}/>

                    {markup.map( (ant, i) =>
                        <img className='tInner' src={ant.image_url} style={{opacity: 0.87}}/>
                    )}


                    {uiStateStore.paintEnable && uiStateStore.paintCardId === card.id &&
                    <ImagePainter3 card={card} image={card.image} width={imgWidth} height={imgHeight}
                        style={{top: 0, left: 0}}
                    />
                    }

                    {(requestor || creator) &&
                    <div className='apUtRequestor abs' style={{top: '1rem', left: '1rem'}}>
                        <MemberAvatar member={requestor || creator}
                                      activeUser={activeUser}
                                      enableAdmin={false}
                                      project={project}
                                      key={'rma'}
                                      index={-1}
                                      color="black"
                                      showName={false}
                                      isEnabled={false}
                                      showMenu={false}
                                      showBorder={true}
                        />
                    </div>
                    }

                </div>

                {!uiStateStore.paintEnable &&
                <div className='fullW' style={{padding: '.5rem 1rem'}}>
                    <AuthProtector isAnon={isAnon}
                                   component={
                                       <ApprovalReactions
                                           showNav={false}
                                           atFirst={atFirst}
                                           atLast={atLast}
                                           project={project}
                                           cardId={card.id}
                                           cards={cards}
                                           selCard={this.props.selCard}
                                           selIndex={this.props.selIndex}
                                           selectedCard={selectedCard}
                                           selectedIndex={selectedIndex}
                                           size='feed'
                                           rsum={rs}
                                           onVote={this.onVote}/>
                                   }
                    />

                    <Icon name='paint brush'
                          data-tip='Draw'
                          className='nav pointer hoh vaTop block mr1'
                          style={{
                              padding: '.25rem',
                              backgroundColor: 'rgba(0,0,0,0.25)',
                              color: 'white',
                              marginBottom: '0',
                              fontSize: '16px',
                              width: '26px',
                              height: '26px'
                          }}
                          onClick={this.togglePaint}
                    />

                    {card.url && card.url.length > 0 &&
                    <span className='capsule mr05 pointer jFourthBG jFG dontWrap inline vaTop' onClick={this.gotoLink}
                          style={{padding: '2px 8px 0 4px', height: '26px'}}
                          data-tip={'Open link at ' + shortUrl}
                    >
                                <ArrowUpRight size={24} className="fg"/>
                                <span className='inline vaTop '
                                      style={{marginTop: '1px'}}> {reverseTruncate (shortUrl, 20, true)}</span>
                    </span>
                    }

                    {card.url && card.url.length > 0 &&
                    <Edit2 size={16} className='jSec' style={{marginTop: '4px'}} onClick={this.openUrlEditor}/>
                    }

                    {/*
                    {card.attachments &&
                    <span className='attmsInCard'>
                            {card.attachments.map ((attm, i) => {
                                ext = getExt (attm.url)
                                iconName = extToIcon (ext)
                                preview = isPreviewable (ext)
                                return (
                                    <AttmThumb file={attm} ext={ext} iconName={iconName}
                                               viewAttm={() => {}}
                                               preview={preview}
                                    />
                                )
                            })}
                        </span>
                    }
*/}
                </div>
                }

                {uiStateStore.paintEnable &&
                <PaintToolbar onPaintSave={this.onPaintSave}
                              onPaintCancel={this.onPaintCancel}
                              onPaintClear={this.onPaintClear}
                              card={card}
                              mobile
                />
                }

                <div className='relative fullW' style={{padding: '0 1rem'}}>
                    <Loader active={working}></Loader>
                    <TextArea className='bold dontWrap fullW'
                        name='title'
                        rows={1}
                        value={title}
                        onClick={this.handleTextClick}
                        onChange={this.handleTextChange}
                        onBlur={this.handleTextSave}
                        onKeyPress={this.handleTextKeyPress}
                        style={{border: 'none', cursor: 'text', padding: '.25rem', overflowY: 'none', resize: 'none'}}
                        />

                    <TextArea
                        rows={1}
                        name='description'
                        value={description}
                        className='small jSec fullW'
                        onClick={this.handleTextClick}
                        onChange={this.handleTextChange}
                        onBlur={this.handleTextSave}
                        onKeyPress={this.handleTextKeyPress}
                        style={{border: 'none', cursor: 'text', padding: '.25rem', overflowY: 'none', resize: 'none'}}
                        />

                </div>

                <div className='fullW'>
                    {/*<Loader active={working}></Loader>*/}
                    <FeedItemComments
                        card={card}
                        comments={comments}
                        activeUser={activeUser}
                        project={project}
                    />
                </div>

                <div className='fullW' style={{padding: '0.5rem 1rem'}}>
                    <input type='text'
                           placeholder='Add comment to this item'
                           value={newComment}
                           className='small fullW jFGInv jBGinv borderAllInv'
                           style={{backgroundColor: 'white', color: 'black'}}
                           onChange={this.onChange}
                           onKeyPress={this.onKeyPress}
                    />
                </div>


                {/*
                <div className='fullW' style={{padding: '0 1rem'}}>
                    {false && latestMembers.map ((member, i) => {
                        return <MemberAvatar member={member}
                                             activeUser={activeUser}
                                             enableAdmin={false}
                                             project={project}
                                             key={'lma' + i}
                                             index={i}
                                             color="black"
                                             showName={true}
                                             isEnabled={false}
                                             showMenu={false}
                        />
                    })}

                    {false && moreCount > 0 &&
                    <span className='inline'>+ {moreCount}</span>
                    }
                </div>
*/}

                {showUrlEditor &&
                    <div className='popup centered abs pad1' style={{bottom: 0, width: '100%'}}>
                        <X size={18} onClick={this.closeUrlEditor} className='abs pointer' style={{top: '.5rem', right: '.5rem'}}/>
                        Link:
                        <input type='text'
                               name='url'
                               value={url}
                               className='ml05 formPane'
                               style={{width: '80%'}}
                               onClick={this.handleTextClick}
                               onChange={this.handleTextChange}
                               onBlur={this.handleTextSave}
                               onKeyPress={this.handleTextKeyPress}
                        />
                    </div>
                }

            </div>
        )

    }
}