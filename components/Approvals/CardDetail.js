import {Component} from 'react'
import RMoment from 'react-moment'
import {observer} from 'mobx-react'
import userStore from "../../store/userStore";
import ReactionApiService, {voteTypeToId} from "../../services/ReactionApiService";
import AuthProtector from "../common/AuthProtector";
import {THUMBS_DOWN, THUMBS_UP} from "../../config/ReactionConfig";
import {getCardReactions} from "../../services/CardApiService";
import {getUserFromMembers} from "../../models/ProjectModel";
import ApprovalReactions from "./ApprovalReactions";
import {Loader} from "semantic-ui-react";
// import {DropTarget} from 'react-dnd'


@observer
export default class CardDetail extends Component {

    state = {rs: null, currentCardId: null}
    isCardInProject = false

    onVote = (reactionResponse) => {
        // const atLastCard = this.props.index === this.props.total - 1
        this.getReactions()
        this.props.onVote(reactionResponse)
    }

    getReactions = () => {
        const { card, author, index, total, apStats } = this.props
        if (!card)
            return
        getCardReactions(card.id, true).then( response => {
            this.setState({rs: response.data, currentCardId: card.id})
        })
    }

    componentDidMount() {
        const { card, project } = this.props
        this.thumbsUpId = voteTypeToId(THUMBS_UP)
        this.thumbsDownId = voteTypeToId(THUMBS_DOWN)
        this.starsId = ReactionApiService.getStarsId()
        this.getReactions()
        if (card && project)
            this.isCardInProject = project.cards && project.cards.findIndex(c => c.id === card.id) !== -1
    }

    componentDidUpdate() {
        const { card, project } = this.props
        const {currentCardId} = this.state
        if (card && currentCardId !== card.id) {
            this.getReactions()
        }
        if (card && project)
            this.isCardInProject = project.cards && project.cards.findIndex(c => c.id === card.id) !== -1
    }

    render() {
        const { project, card, cards, author, index, total, apStats, atFirst, atLast, selectedCard, selectedIndex, loading } = this.props
        const { rs, currentCardId } = this.state

        if (!card)
            return null

        const isAnon = !userStore.currentUser
        let approvals = [], rejections = [], ratings = []
        const reactions = rs? rs.card_reactions : []
        if (reactions && reactions.length > 0) {
            approvals = reactions.filter(r => r.reaction_type_id === this.thumbsUpId) || []
            rejections = reactions.filter(r => r.reaction_type_id === this.thumbsDownId) || []
            ratings = reactions.filter(r => r.reaction_type_id === this.starsId) || []
        }

        // let isApproval = ReactionApiService.getUserReactionByType(card, this.thumbsUpId)
        // let isRejection = ReactionApiService.getUserReactionByType(card, this.thumbsDownId)
        // const rating = ReactionApiService.getLastStarRating(card)

/*
        // TODO approve and reject must be mutually exclusive. FIND MOST RECENT. AND, REGISTERING ONE SHOULD DELETE THE OTHER.
        if (isApproval !== null && isRejection !== null) {
            if (isApproval.time_modified > isRejection.time_modified)
                isRejection = null
            else
                isApproval = null
        }
*/

        let user, vote

        return (
            <div className='apCardDetail vaTop'>
                <div className='apcMain'> {/*row*/}
                    <div className='inline relative centered imgBox borderAll' >
                        {!loading && this.isCardInProject &&
                        <img src={card.image} />
                        }
                        <Loader active={loading || !this.isCardInProject}></Loader>
                    </div>
                    <div className='inline vaTop relative' style={{width: '30%', height: '100%', paddingLeft: '1rem', wordBreak: 'break-word'}}>
                        <p className='fg' style={{fontSize: '1.25rem'}}>
                            {index >= 0? (index+1)+'.':''} {card.title}
                        </p>
                        {author && card.dateCreated &&
                        <div className='secondary small mt1 mb1'>
                            From: {author.displayName || author.email}<br/>
                            <RMoment subtract={{hours: 7}} format={'MMM Do, h:mm a'}>{card.dateCreated}</RMoment>.
                        </div>
                        }

{/*
                        {card.dateCreated &&
                        <div className='secondary small mt1'>
                            Added <RMoment subtract={{hours: 7}} format={'MMM Do, h:mm a'}>{card.dateCreated}</RMoment>
                        </div>
                        }
*/}

                        {card.description && card.description !== 'description' &&
                        <div className={'small secondary'}>{card.description}</div>
                        }

                        {card.url &&
                        <div className='secondary small mt1 link dontWrap'>
                            <a href={card.url} target='_blank'>{card.url}</a>
                        </div>
                        }

                    </div>
                </div>
                <div className='reaction fg centered' style={{marginBottom: '.5rem', marginTop: '1rem'}}>

                    <div className='inline cardReactions'>
                        <AuthProtector isAnon={isAnon}
                                       component={
                                           <ApprovalReactions
                                               showNav={true}
                                               atFirst={atFirst}
                                               atLast={atLast}
                                               project={project}
                                               cardId={card.id}
                                               cards={cards}
                                               selCard={this.props.selCard}
                                               selIndex={this.props.selIndex}
                                               selectedCard={selectedCard}
                                               selectedIndex={selectedIndex}
                                               size={300}
                                               rsum={rs}
                                               onVote={this.onVote}/>
                                       }
                        />
                    </div>

                    <div className='abs voteStats' style={{bottom: '1rem', right: '1rem', width: '28%'}}>
                        {approvals.length > 0 &&
                        <div className='jBlue'>
                            {approvals.map (r => {
                                user = getUserFromMembers(r.user_id, project) || userStore.getUserFromConnections(r.user_id)
                                if (user)
                                    return <div>Approved by {user.displayName || user.firstName}</div>
                                else
                                    return null
                            })}
                        </div>
                        }

                        {rejections.length > 0 &&
                        <div className='red '>
                            {rejections.map (r => {
                                user = getUserFromMembers(r.user_id, project) || userStore.getUserFromConnections(r.user_id)
                                if (user)
                                    return <div>Disliked by {user.displayName || user.firstName}</div>
                                else
                                    return null
                            })}
                        </div>
                        }

                        {ratings.length > 0 &&
                        <div className='fg '>
                            {ratings.map (r => {
                                user = getUserFromMembers(r.user_id, project) || userStore.getUserFromConnections(r.user_id)
                                if (user)
                                    return <div>{r.reaction_value} stars from {user.displayName || user.firstName}</div>
                                else
                                    return null
                            })}
                        </div>
                        }

                        {/*empty cases*/}
                        <div className='jThird lighter tiny'>
                            {approvals.length === 0 &&
                            <div>No approvals</div>
                            }
                            {rejections.length === 0 &&
                            <div>No dislikes</div>
                            }
                            {false && ratings.length === 0 &&
                            <div>No ratings</div>
                            }
                        </div>

                        <div className='secondary small mt1' >
                            Item {index+1} of {total}
                        </div>


                    </div>



                    {/*
                    {reactionType === 'approve' &&
                    <div>
                        <span className='pointer button go' onClick={this.approve}>
                            <ThumbsUp size={24} style={{marginRight: '.5rem', marginBottom: '-4px'}}/>
                            <h2 className='inline'>Approve</h2>
                        </span>
                        <span className='pointer button basic ooh' onClick={this.reject} style={{backgroundColor: 'unset', border: '1px solid #c80000'}}>
                            <ThumbsDown size={24} className='red' style={{marginRight: '.5rem', marginBottom: '-4px'}}/>
                            <h2 className='inline red'>Reject</h2>
                        </span>
                    </div>
                    }

                    {reactionType === 'rate' &&
                    <span>
                        <Star size={20} className={'secondary'} style={{marginBottom: '-2px'}}/>
                        <Star size={20} className={'secondary'} style={{marginBottom: '-2px'}}/>
                        <Star size={20} className={'secondary'} style={{marginBottom: '-2px'}}/>
                        <Star size={20} className={'secondary'} style={{marginBottom: '-2px'}}/>
                        <Star size={20} className={'secondary'} style={{marginBottom: '-2px'}}/>
                    </span>
                    }
*/}
                </div>


            </div>
        )

    }
}