import React, {Component} from 'react'
import {observer} from 'mobx-react'
import {Divider, Grid} from 'semantic-ui-react'
import {Router} from '~/routes'

import UnmodalContainer from '/components/unmodals/UnmodalContainer'
import ReactionApiService from '/services/ReactionApiService'
import projectStore from '/store/projectStore'
import userStore from '/store/userStore'
import uiStateStore from '/store/uiStateStore'
import CardCarousel from "./CardCarousel";
import CardDetail from "./CardDetail";
import {getUserFromMembers} from "../../models/ProjectModel";
import {ProjectStatus, UserRole} from "../../config/Constants";
import {getProjectReactionSummary} from "../../services/solo/getProjectReactionSummary";
import APStats from "./APStats";
import CardViewerPane from "../unmodals/CardViewerPane";
import cardStore from "../../store/cardStore";
// import {DropTarget} from 'react-dnd'


@observer
export default class APWorkPane extends Component {
    constructor(props) {
        super (props)
        this.state = {
            isFiltering: false, isDeleting: false, defaultReactionSets: [],
            author: null, apCompleted: false, loading: false
            // selectedCard: null, selectedCardIndex: 0,
        }
        this.prevProjectId = null
    }

    selCard = (cardId) => {
        const cards = projectStore.currentProject ? projectStore.currentProject.cards : []
        if (!cardId || !cards)
            return

        const card = cards.find (c => c.id === cardId)
        const index = cards.findIndex (c => c.id === cardId)
        _getCardAuthor (card).then (author => {
            this.setState ({author: author})
            // TODO We were SSOT here. Now passing some state up to approve.js, to send down to Convo for tagging, and vice-versa, for at_card views.
            // This is quicker than refactoring approve page to include convo in work pane, which we should've done in the first place.
            // Also quicker than moving state up to approve page, which is equally valid alternative, though not typical with other pages.
            // So when time permits, bring convo into here, so that we can remain SSOT.
            // IN PROGRESS: Moving selectedCard: card, selectedCardIndex: index, state up to approve page
            this.props.onSelCard (card, index)
        })
    }

    selIndex = (index) => {
        const cards = projectStore.currentProject ? projectStore.currentProject.cards : []
        if (!cards || index < 0 || index > cards.length - 1)
            return

        const card = cards[index]
        this.setState ({selectedCard: card, selectedCardIndex: index})
        uiStateStore.setViewingCard (card)
        this.props.onSelCard (card, index)
    }

    handleVote = (reactionSummary) => {
        const ap = projectStore.currentProject
        const cards = ap ? ap.cards : []
        const {selectedCardIndex} = this.state

        if (cards.length > 0 && reactionSummary) {
            cards[selectedCardIndex].reactions = reactionSummary.card_reactions
        }

        const atLastCard = selectedCardIndex === cards.length - 1
        const completed = this.isVotingComplete ()
        if (!completed) {
            // go to first unvoted card
        }
        this.setState ({apCompleted: completed})
        // completeApproval(ap.id) // TODO Chris doesn't want to complete approvals yet. What if multiple approvers?
        // TODO temp solution to keep APStats updated. Refresh after each vote. Better fix: Just update minimum necessary.
        projectStore.fetchCurrentProject (ap.id, userStore.currentUser.id, false)

        // TODO see SSOT note
        this.props.onVote (reactionSummary)
    }

    isVotingComplete() {
        const cards = projectStore.currentProject ? projectStore.currentProject.cards : []
        for (let i = 0; i < cards.length; i++) {
            if (!ReactionApiService.hasUserReacted (cards[i]))
                return false
        }
        return true
    }

    close = (ev) => {
        window && window.close ()
    }

    gotoHome = (ev) => {
        Router.pushRoute ('/', {update: true})
    }

    revisit = (ev) => {
        this.setState ({apCompleted: false})
    }

    msgRequestor = (ev) => {
        const project = projectStore.currentProject
        if (!project)
            return

        const requestor = getUserFromMembers (project.creatorId, project)
        uiStateStore.openDM (requestor.id)
    }

    componentDidMount() {
        const project = projectStore.currentProject
        // this.prevProjectId = project? project.id : null

        if (!this.state.defaultReactionSets) {
            ReactionApiService.getDefaultReactionSets ().then (rs => {
                this.setState ({defaultReactionSets: rs})
            })
        }

        if (project && project.reactionSetId)
            ReactionApiService.getReactionSetById (project.reactionSetId).then (rs => {
                project.reactionSet = rs
            })

        if (project && this.prevProjectId !== project.id) {
            if (project.cards.length > 0) {
                this.selIndex (0)
            }
            this.prevProjectId = project.id
        }

    }

    componentDidUpdate() {
        const project = projectStore.currentProject
        if (project && (this.prevProjectId !== project.id) || !this.state.selectedCard) {
            // this.setState ({loading: true})
            console.log ('APWORKPANE UPDATE: prev project: ' + this.prevProjectId + ', new project: ' + project.id)
            this.prevProjectId = project.id
            if (project.cards.length > 0) {
                this.selIndex (0)
                this.setState ({loading: false})
            }
        }
    }

    render() {
        const {user, mobile, item} = this.props
        const project = projectStore.currentProject
        const {apCompleted, author, loading} = this.state
        const {selectedCard, selectedCardIndex} = this.props

        const me = userStore.currentUser
        const meInProject = project.members.find (mbr => mbr.id === me.id) || null
        const imDone = meInProject && meInProject.role === UserRole.INACTIVE_USER

        if (!project)
            return null

        const cards = project ? project.cards : []
        const atFirstCard = (selectedCardIndex === 0)
        const atLastCard = (selectedCardIndex === cards.length - 1)
        const requestor = getUserFromMembers (project.creatorId, project)
        const apStats = getProjectReactionSummary (project)

        const mainStyle = {height: '100%', opacity: imDone ? 0.7 : 1}

        if (uiStateStore.unModal !== null) {
            return <UnmodalContainer/>
        }

        if (mobile) { // No need for Grid. Retain for style consistency only?
            return (
                <main className="cardViews" style={{width: '100%'}}>
                    <Grid style={{height: '105% !important'}}>
                        <Grid.Row className='borderB' style={{paddingBottom: '0'}}>
                            <Grid.Column width={16}>
                                Toolbar
                            </Grid.Column>
                        </Grid.Row>

                        <Grid.Row style={{paddingTop: '0'}}>
                            <Grid.Column width={16}>
                                Mobile Approvals TBD
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </main>
            )
        }

        // Large
        if (project.status === ProjectStatus.CANCELLED) {
            return (
                <main className="apWork"> {/*style={{height: '100%'}}*/}
                    <h1 className='fg'>This approval request has been cancelled.</h1>
                    <h3>What next?</h3>
                    <div className='mt1 link' onClick={this.gotoHome}>
                        Go to your Dashboard
                    </div>
                    {requestor &&
                    <div className='link' onClick={this.msgRequestor}>
                        Message {requestor.displayName || requestor.firstName}
                    </div>
                    }
                </main>
            )
        }

        else return (
            <main className="apWork" style={mainStyle}>
                <APStats
                    project={project}
                    requestor={requestor}
                    apStats={apStats}
                />

                {/*
                {!apCompleted &&
                <CardCarousel
                    cards={cards}
                    style={{margin: '1rem .5rem 2rem 0'}}
                    reactions={true}
                    selCard={this.selCard}
                    selIndex={this.selIndex}
                    selectedCard={selectedCard}
                    selectedIndex={selectedCardIndex}
                    atFirst={atFirstCard}
                    atLast={atLastCard}
                />
                }
*/}

                {/*
                {!apCompleted &&
                <CardDetail
                    project={project}
                    card={selectedCard}
                    cards={cards}
                    selCard={this.selCard}
                    selIndex={this.selIndex}
                    selectedCard={selectedCard}
                    selectedIndex={selectedCardIndex}
                    author={author}
                    index={selectedCardIndex}
                    total={cards ? cards.length : 0}
                    atFirst={atFirstCard}
                    atLast={atLastCard}
                    onVote={this.handleVote}
                    apStats={apStats}
                    loading={loading}
                />
                }
*/}

                {!uiStateStore.editingCard &&
                <CardViewerPane onInputRequest={this.requestInputFocus}
                                card={selectedCard}
                                index={selectedCardIndex}
                                total={cards ? cards.length : 0}
                                onVote={this.handleVote}
                                apStats={apStats}
                />
                }

{/*
                {apCompleted &&
                <div className='mt1' style={{padding: '10%'}}>
                    <h1 className='fg'>All done. Thanks{user ? (' ' + user.displayName) : ''}!</h1>
                    {requestor &&
                    <h2 className='fg'>We'll let {requestor.displayName || requestor.firstName || 'the requestor'} know
                        immediately.</h2>
                    }
                    <Divider/>
                    <div className='mt1 fg'>
                        <h3>What next?</h3>
                        <div className='link' onClick={this.revisit}>
                            Return to Approval
                        </div>
                        <div className='mt1 link' onClick={this.gotoHome}>
                            Go to your Dashboard
                        </div>
                        <div className='mt1 link' onClick={this.close}>
                            Close pogo.io
                        </div>

                    </div>
                </div>
                }
*/}

            </main>
        )
    }
}

function _getCardAuthor(card) {
    if (!card || !card.creatorId)
        return null

    return new Promise ((resolve, reject) => {
        const connectedAuthor = userStore.getUserFromConnections (card.creatorId)
        if (connectedAuthor)
            resolve (connectedAuthor)

        userStore.getUserById (card.creatorId).then (author => {
            resolve (author)
        })
    })

}