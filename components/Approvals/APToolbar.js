import {Component} from 'react'
import {observer} from 'mobx-react'
import {Link} from '~/routes'
import {Menu} from 'semantic-ui-react'
import uiStateStore from '/store/uiStateStore'
import cardStore from '/store/cardStore'

import 'rc-tooltip/assets/bootstrap.css'
import projectStore from "../../store/projectStore";
import {addCardsToApproval} from "../../services/ApprovalService";
// import UploaderComponent from '/components/common/UploaderComponent'

@observer
export default class APToolbar extends Component {
    state = {
    }

    cancelApproval = () => {
        if (uiStateStore.currentApprovalRequest !== null && uiStateStore.isAPStarterRequest && !uiStateStore.approvalSent) {
            projectStore.deleteProject(uiStateStore.currentApprovalRequest)
        }

        cardStore.selectedCards = []
        uiStateStore.rightUnModal = null
        uiStateStore.currentApprovalRequest = null
        uiStateStore.showUserPopup = false
    }

    selectAllCards = () => {
        const ap = uiStateStore.currentApprovalRequest
        cardStore.selectedCards = this.props.project.cards
        const cardIds = cardStore.selectedCards.map (card => card.id)
        addCardsToApproval (cardIds, ap.id).then(apCardIds => {
            ap.cardIds = apCardIds
        })

    }

    selectNone = () => {
        cardStore.selectedCards = []
        const ap = uiStateStore.currentApprovalRequest
        const cardIds = ap.cardIds && ap.cardIds.length > 0? ap.cardIds : cardStore.selectedCards.map (card => card.id)
        if (cardIds && cardIds.length > 0) {
            cardStore.deleteCards (cardIds, ap.id).then (apCardIds => {
                ap.cardIds = []
            })
        }
    }

    render() {
        const {activeDropdownItem, showLeaveConfirmation, showDeleteConfirmation} = this.state

        const { project } = this.props
        const cards = project? project.cards : []
        const cardCount = cards.length
        const title = project? project.title : 'Project'
        const view = uiStateStore.cardView
        const enableBulkActions = cardStore.selectedCardIds && cardStore.selectedCardIds.length > 0

        return(
            <div className="top-pane workCol" style={{paddingLeft: '1.5rem'}}>
                <Menu attached='top' className='toolbar-menu toolbar-aux' >

                    <Menu.Item style={{paddingLeft: '0', paddingRight: '0'}}>


{/*
                        <Responsive minWidth={767}
                                    data-tip="Filter Cards by Title..." style={{marginLeft: '1rem'}}>
                            <CardSearch {...this.props} handleResults={this.handleCardSearchResults} />
                        </Responsive>
*/}



                    </Menu.Item>


                    <Menu.Item position='right' style={{ paddingRight: '0.1rem', marginRight: '7rem' }} >

                        <button className='toolbar-btn'
                                onClick={this.selectAllCards}
                        >
                            Select All
                        </button>

                        <button className='toolbar-btn'
                                onClick={this.selectNone}
                        >
                            Select None
                        </button>

                        <button className={'toolbar-btn' +(enableBulkActions? '':' disabled')}
                                onClick={this.cancelApproval}
                        >
                            Cancel Approval
                        </button>

                    </Menu.Item>


                </Menu>


            </div>
        )
    }
}