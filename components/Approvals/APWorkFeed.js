import {Component} from 'react'
import {observer} from 'mobx-react'
import {findDOMNode} from "react-dom";
import {Router} from '~/routes'

import UnmodalContainer from '/components/unmodals/UnmodalContainer'
import ReactionApiService from '/services/ReactionApiService'
import projectStore from '/store/projectStore'
import userStore from '/store/userStore'
import uiStateStore from '/store/uiStateStore'
import {getUserFromMembers} from "../../models/ProjectModel";
import {ProjectStatus, UserStatus} from "../../config/Constants";
import {getProjectReactionSummary} from "../../services/solo/getProjectReactionSummary";
import FeedItem from "./FeedItem";
import CardCreator from "../common/Card/CardCreator";
import PaintToolbar from "../ProjectDetail/WorkPane/PaintToolbar";
import cardStore from "../../store/cardStore";
import {deleteAnnotation} from "../../services/ImageFileService";
// import {DropTarget} from 'react-dnd'


@observer
export default class APWorkFeed extends Component {
    constructor(props) {
        super (props)
        this.state = {
            elem: null, isFiltering: false, isDeleting: false, defaultReactionSets: [],
            author: null, initialized: false, apCompleted: false, loading: false, loadingId: null
            // selectedCard: null, selectedCardIndex: 0,

        }
    }

    createCards(cards) {
        if (this.props.createCards)
            this.props.createCards(cards)
    }

    importCards(cards) {
        if (this.props.importCards)
            this.props.importCards(cards)
    }

    importAssets(assets, source) {
        if (this.props.importAssets)
            this.props.importAssets(assets, source)
    }

    uploadAssets(cards, files) {
        if (this.props.uploadAssets)
            this.props.uploadAssets(cards, files)
    }


    selCard = (cardId) => {
        const cards = cardStore.currentCards
        if (!cardId || !cards)
            return

        const card = cards.find (c => c.id === cardId)
        const index = cards.findIndex (c => c.id === cardId)
        _getCardAuthor (card).then (author => {
            this.setState ({author: author})
            // TODO We were SSOT here. Now passing some state up to approve.js, to send down to Convo for tagging, and vice-versa, for at_card views.
            // This is quicker than refactoring approve page to include convo in work pane, which we should've done in the first place.
            // Also quicker than moving state up to approve page, which is equally valid alternative, though not typical with other pages.
            // So when time permits, bring convo into here, so that we can remain SSOT.
            // IN PROGRESS: Moving selectedCard: card, selectedCardIndex: index, state up to approve page
            if (this.props.onSelCard)
                this.props.onSelCard (card, index)
        })
    }

    selIndex = (index) => {
        const cards = cardStore.currentCards
        if (!cards || index < 0 || index > cards.length - 1)
            return

        const card = cards[index]
        this.setState ({selectedCard: card, selectedCardIndex: index, initialized: true})
        if (this.props.onSelCard)
            this.props.onSelCard (card, index)
    }

    handleVote = (reactionSummary) => {
        const ap = projectStore.currentProject
        const cards = ap ? ap.cards : []
        const {selectedCardIndex} = this.state

        if (cards.length > 0 && reactionSummary) {
            cards[selectedCardIndex].reactions = reactionSummary.card_reactions
        }

        const atLastCard = selectedCardIndex === cards.length - 1
        const completed = this.isVotingComplete ()
        if (!completed) {
            // go to first unvoted card
        }
        this.setState ({apCompleted: completed})

        // TODO see SSOT note
        if (this.props.onVote)
            this.props.onVote (reactionSummary)
    }

    isVotingComplete() {
        const cards = cardStore.currentCards
        for (let i = 0; i < cards.length; i++) {
            if (!ReactionApiService.hasUserReacted (cards[i]))
                return false
        }
        return true
    }

    addCommentOnFeedItem(text, cardId) {
        const me = userStore.currentUser
        const isAnon = !me || me.status === UserStatus.ANON

        if (isAnon) {
            uiStateStore.setJoinPromptState (true)
            return
        }

        const project = projectStore.currentProject
        this.setState ({loading: true, loadingId: cardId})
        projectStore.addToConvo (text, project.id, [cardId]).then (response => {
            this.setState ({loading: false, loadingId: false})
        })

    }

    resetInputState = () => {
        this.setState ({
            loading: false,
            loadingId: null,
            newComment: '',
        })
    }

    close = (ev) => {
        window && window.close ()
    }

    gotoHome = (ev) => {
        Router.pushRoute ('/', {update: true})
    }

    revisit = (ev) => {
        this.setState ({apCompleted: false})
    }

    msgRequestor = (ev) => {
        const project = projectStore.currentProject
        if (!project)
            return

        const requestor = getUserFromMembers (project.creatorId, project)
        uiStateStore.openDM (requestor.id)
    }

    openCardViewer = (card) => {

    }

    onTextSave = (card) => {
        const {loading, loadingId} = this.state
        if (loading) {
            console.log('Ignoring save already in progress')
            return
        }
        this.setState({loading: true, loadingId: card.id})
        cardStore.saveCard(card).then( card => {
            this.setState({loading: false, loadingId: null})
        }).catch( error => {
            this.setState({loading: false, loadingId: null})
        })
    }

    onPaintCancel = (ev) => {
    }

    onPaintSave = (cardId) => {
        cardStore.getCardById (cardId).then (card => {
            const cardIdx = cardStore.currentCards.findIndex(c => c.id === cardId)
            cardStore.currentCards[cardIdx] = card
        })
    }

    onPaintClear = (card) => {
        if (card.annotations && card.annotations.length > 0) {
            deleteAnnotation ({cardId: card.id}).then (response => {
                card.annotations = null
                const cardIdx = cardStore.currentCards.findIndex(c => c.id === card.id)
                cardStore.currentCards[cardIdx] = card
            })
        }
    }

    componentDidMount() {
        const project = projectStore.currentProject
        ReactionApiService.getDefaultReactionSets ().then (rs => {
            this.setState ({defaultReactionSets: rs})
        })
        if (project && project.reactionSetId)
            ReactionApiService.getReactionSetById (project.reactionSetId).then (rs => {
                project.reactionSet = rs
            })

        const cards = cardStore.currentCards
        if (cards.length > 0) {
            this.selIndex (0)
        }

        const elem = findDOMNode(this)
        this.setState({elem: elem})

    }

    componentDidUpdate() {
        if (this.state.initialized)
            return

        const cards = cardStore.currentCards
        if (cards.length > 0) {
            this.selIndex (0)
        }
    }

    render() {
        const {user, mobile, item} = this.props
        const project = projectStore.currentProject
        const convo = projectStore.currentProjectConvo

        const {apCompleted, author, loading, loadingId} = this.state
        const {selectedCard, selectedCardIndex} = this.props

        if (!project)
            return null

        const cards = cardStore.currentCards
        const atFirstCard = (selectedCardIndex === 0)
        const atLastCard = (selectedCardIndex === cards.length - 1)
        const requestor = getUserFromMembers (project.creatorId, project)
        const apStats = getProjectReactionSummary (project)

        let cardCmts

        if (uiStateStore.unModal !== null) {
            return <UnmodalContainer/>
        }

        if (project.status === ProjectStatus.CANCELLED) {
            return (
                <main className="apWork" style={{height: '100%'}}>
                    <h1 className='fg'>This approval request has been cancelled.</h1>
                    <h3>What next?</h3>
                    <div className='mt1 link' onClick={this.gotoHome}>
                        Go to your Dashboard
                    </div>
                    {requestor &&
                    <div className='link' onClick={this.msgRequestor}>
                        Message {requestor.displayName || requestor.firstName}
                    </div>
                    }
                </main>
            )
        }

        else return (
            <main className="apUtFeed apWork " style={{height: '105%', padding: '.5rem 5px 0 .5rem'}}>
                {cards.length === 0 &&
                <CardCreator
                    mobile={mobile}
                    showTags={false}
                    showReactions={false}
                    containerElem={this.state.elem}
                    createCards={(cards) => {this.createCards(cards)}}
                    importCards={(cards) => {this.importCards(cards)}}
                    importAssets={(assets, source) => {this.importAssets(assets, source)}}
                    uploadAssets={(cards, files) => {this.uploadAssets(cards, files)}}
                />
                }

                {cards.map ((card, i) => {
                    cardCmts = convo.filter (cmt => cmt.at_cards && (cmt.at_cards.find (c => c.card_id === card.id) !== undefined)) || []
                    return (
                            <FeedItem
                                project={project}
                                card={card}
                                comments={cardCmts}
                                mobile={mobile}
                                selCard={this.selCard}
                                selIndex={this.selIndex}
                                selectedCard={card}
                                selectedIndex={i}
                                author={author}
                                index={i}
                                total={cards ? cards.length : 0}
                                atFirst={atFirstCard}
                                atLast={atLastCard}
                                openCardViewer={this.openCardViewer}
                                onVote={this.handleVote}
                                apStats={apStats}
                                working={loading && loadingId === card.id}

                                onTextSave={this.onTextSave}
                                addCommentOnFeedItem={(text, cardId) => this.addCommentOnFeedItem (text, cardId)}

                                onPaintSave={this.onPaintSave}
                                onPaintCancel={this.onPaintCancel}
                                onPaintClear={this.onPaintClear}


                            />
                    )
                })}
            </main>
        )
    }
}

function _getCardAuthor(card) {
    if (!card || !card.creatorId)
        return null

    return new Promise ((resolve, reject) => {
        const connectedAuthor = userStore.getUserFromConnections (card.creatorId)
        if (connectedAuthor)
            resolve (connectedAuthor)

        userStore.getUserById (card.creatorId).then (author => {
            resolve (author)
        })
    })

}