import {Component} from 'react'
import {observer} from 'mobx-react'
import ReactionApiService from '/services/ReactionApiService'
import projectStore from '/store/projectStore'
import AtCard from "../Conversations/AtCard";
import {ChevronLeft, ChevronRight} from "react-feather";
import {voteTypeToId} from "../../services/ReactionApiService";
import {STARS, THUMBS_DOWN, THUMBS_UP} from "../../config/ReactionConfig";
import {findDOMNode} from "react-dom";
import uiStateStore from "../../store/uiStateStore";
// import {DropTarget} from 'react-dnd'


@observer
export default class CardCarousel extends Component {

    viewer = null

    viewCard = (cardId) => {
        uiStateStore.filterDiscussionByCard = true
        this.props.selCard(cardId)
    }

    gotoPreviousCard = (ev) => {
        ev.preventDefault ()
        const {selectedIndex} = this.props
        if (selectedIndex > 0) {
            this.props.selIndex (selectedIndex - 1)
            if (this.viewer)
                this.viewer.scrollLeft -= 56
        }
    }

    gotoNextCard = (ev) => {
        ev.preventDefault()
        const {selectedIndex,cards} = this.props
        if (selectedIndex < cards.length-1) {
            this.props.selIndex (selectedIndex + 1)
            if (this.viewer)
                this.viewer.scrollLeft += 56
        }
    }

    HandleKeyDown = (ev) => {
        const key = ev.key

        if (uiStateStore.arrowPriority !== 'nav')
            return

        if (key === 'ArrowRight')
            this.gotoNextCard(ev)
        else if (key === 'ArrowLeft')
            this.gotoPreviousCard(ev)
    }

    handleMouseWheel = (ev) => {
        const viewer = this.viewer
        if (viewer)
            viewer.scrollLeft += (ev.deltaY * 5)
    }

    componentDidMount() {
        this.thumbsUpId = voteTypeToId(THUMBS_UP)
        this.thumbsDownId = voteTypeToId(THUMBS_DOWN)

        const elem = findDOMNode(this)
        if (!elem) {
            console.error("Carousel has no element after mount. REALITY VIOLATION CODE 234-0843")
            return
        }
        this.viewer = elem.querySelector('.viewer')
        this.viewer.style.transition = 'scrollLeft 1.2s'

        window.addEventListener('keydown', this.HandleKeyDown)
        // window.addEventListener('wheel', this.handleMouseWheel)


    }

    componentWillUnmount() {
        window.removeEventListener("keydown", this.HandleKeyDown)
        // window.removeEventListener("wheel", this.handleMouseWheel)
    }

    render() {
        const { cards, style, selectedCard, selectedIndex, reactions, atFirst, atLast } = this.props
        const project = projectStore.currentProject
        if (!project)
            return null
        const projectRS = ReactionApiService.getReactionSetByIdFromDefaults(project.reactionSetId)
        const starRS = ReactionApiService.getReactionSetByTitleFromDefaults(STARS)
        const showReactions = reactions && projectRS && starRS && projectRS.reaction_set_id !== starRS.reaction_set_id
        let approved, rejected, rating

        return (
            <div className='ccar dontWrap'>
                <ChevronLeft size={42} className='nav secondary pointer mr05' disabled={atFirst}
                             style={{opacity: atFirst? .3:1, order: 1}}
                             onClick={(ev) => this.gotoPreviousCard (ev)}/>

                <div className='hidden-scroll viewer mr05'
                    style={{order: 2, flexGrow: 10, flexBasis: '50%', overflowY: 'hidden', overflowX: 'auto', scrollBehavior: 'smooth', paddingLeft: '4px'}}>
                {cards.map ((card,i) => {
                    approved = ReactionApiService.hasUserReacted(card, this.thumbsUpId)
                    if (!approved)
                        rejected = ReactionApiService.hasUserReacted(card, this.thumbsDownId)
                    rating = ReactionApiService.getLastStarRating(card)

                    return <AtCard card={card}
                                   key={'cc' + i}
                                   showAttmMenu={false}
                                   sizeRems={4}
                                   style={style}
                                   showMenu={false}
                                   selected={selectedCard && card && card.id === selectedCard.id}
                                   viewCard={() => this.viewCard (card.id)}
                                   showReactions={showReactions}
                                   miniReactions={true}
                                   // TODO Hidden until we can pass in CardReactionsRead or other, rather than rely on unupdated card.
                                   // approved={approved}
                                   // rejected={rejected}
                                   // rating={rating}
                    />
                })}
                </div>
                <ChevronRight size={42} className='nav secondary pointer' disabled={atLast}
                              style={{order: 3, opacity: atLast? .3:1}}
                              onClick={(ev) => this.gotoNextCard (ev)}/>
            </div>
        )

    }
}

