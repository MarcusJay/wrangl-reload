import {Component} from 'react'
import {observer} from 'mobx-react'
import {Link} from '~/routes'
import RMoment from 'react-moment'

import 'rc-tooltip/assets/bootstrap.css'
import {UserRole} from "../../config/Constants";
// import UploaderComponent from '/components/common/UploaderComponent'

@observer
export default class APStats extends Component {
    state = {
    }

    render() {
        const {apStats, project, requestor} = this.props
        const {userStats, cardStats} = apStats

        const totalCards = project.cards.length || 0
        const approvers = project.members.filter(user => user.role === UserRole.APPROVER || user.role === UserRole.REQUESTOR)
        const totalApprovers = approvers? approvers.length : 0

        const userCompletionScores = Object.values(userStats).map (stat => stat.percentComplete)
        const cardCompletionScores = Object.values(cardStats).map (stat => stat.percentComplete)

        const userScoreTotal = (userCompletionScores && userCompletionScores.length > 0)?
            userCompletionScores.reduce((total, amount) => total + amount) : 0
        const cardScoreTotal = (cardCompletionScores && cardCompletionScores.length > 0)?
            cardCompletionScores.reduce((total, amount) => total + amount) : 0

        const totalActiveUsers = (userCompletionScores.filter(pct => pct > 0) || []).length
        const totalActiveCards = (cardCompletionScores.filter(pct => pct > 0) || []).length

        const pctActiveUsers = Math.round(100 * totalActiveUsers / totalApprovers)
        const pctActiveCards = Math.round(100 * totalActiveCards / totalCards)

        // const avgUserScore = Math.round(totalApprovers > 0? (userScoreTotal / totalApprovers) : 0)
        const avgCardScore = Math.round(totalCards > 0? (cardScoreTotal / totalCards) : 0)

        return(
            <div className="top-pane jItemBG jSec lighter small borderB" style={{padding: '1rem'}}>

                {requestor && project.dateCreated &&
                <div className='inline' style={{marginRight: '1rem'}}>
                    <div className='inline' >Requested by {requestor.displayName || requestor.firstName || 'Unknown'} on&nbsp;
                        <RMoment subtract={{hours: 7}} format={'MMM Do, h:mm a'}>{project.dateCreated}</RMoment>.
                    </div>
                </div>
                }

                <span >
                    {totalApprovers > 1 &&
                    <div className='inline' style={{marginRight: '1rem'}}>
                        {totalActiveUsers} of {totalApprovers} members have started voting.
                    </div>
                    }

                    {totalApprovers === 1 &&
                    <div className='inline' style={{marginRight: '1rem'}}>
                        Voting has {totalActiveUsers > 0? '' : 'not'} begun.
                    </div>
                    }

                    {totalActiveCards < totalCards &&
                    <div className='inline' style={{marginRight: '1rem'}}>
                        {totalActiveCards} out of {totalCards} items have been voted on.
                    </div>
                    }

                    {totalCards > 0 && totalActiveCards === totalCards &&
                    <div className='inline' style={{marginRight: '1rem'}}>
                        All items have been voted on.
                    </div>
                    }

                    {false && totalApprovers > 1 &&
                    <div className='right inline' style={{marginRight: '1rem'}}>
                        {pctActiveUsers}% of users have voted.
                    </div>
                    }
                </span>

{/*
                <div className='inline' style={{marginRight: '1rem'}}>
                    {pctActiveCards}% of items received votes.
                </div>
*/}

            </div>
        )
    }
}