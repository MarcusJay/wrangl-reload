import {Component} from 'react'
import {observer} from 'mobx-react'
import {findDOMNode} from 'react-dom'
import projectStore from "../../store/projectStore";
import MemberAvatar from "../common/MemberAvatar";
import {Mail, PlusCircle, X} from "react-feather";
import {ProjectType} from "../../config/Constants";
import isValidEmail from "../../utils/solo/isValidEmail";

const MSG_CHOOSE_USER = "Click the person you'd like to tag..."

@observer
export default class UserPopup extends Component {

    state = {
        displayUser: null,

        showInstructions: true,
        showEmailModal: false,

        searchResults: this.props.allUsers,
        query: null,

        currentInviteeEmail: null,
        currentInviteeName: null,
        allInvitees: []
    }

    close() {
        this.props.close()
    }

    selUser = (u,i) => {
        this.props.selUser(u)
    }

    overUser = (u,i) => {
        this.setState({displayUser: u, showInstructions: false})
        // this.props.overUser(u)
    }

    clearUser = (u,i) => {
        this.setState({displayUser: null})
        this.props.clearUser(u)
    }

    handleSearchChange = (ev) => {
        const query = ev.target.value
        if (!query || query.length === 0) {
            this.setState ({query: null, searchResults: this.props.allUsers})
            return
        }

        const matches = this.props.allUsers.filter( user => {   // using allUsers supports backspaces as well
            return (user.displayName && user.displayName.startsWith (query)) ||
                (user.firstName && user.firstName.startsWith (query)) ||
                (user.lastName && user.lastName.startsWith (query))
        })
        this.setState({query: query, searchResults: matches})
    }

    openEmailModal = (ev) => {
        this.setState({showEmailModal: true, currentInviteeName: null, currentInviteeEmail:null})
    }

    closeEmailModal = (ev) => {
        this.setState({showEmailModal: false})
    }

    handleInviteeChange = (ev) => {
        const propName = ev.target.name
        const propValue = ev.target.value
        this.setState({[propName]: propValue})
    }

    handleKeyDown = (ev) => {
        if (ev.key === 'Enter')
            this.handleAddNewUser(ev)
        else if (ev.key === 'Escape')
            this.close()
    }

    handleAddNewUser = (ev) => {
        const {allInvitees, currentInviteeName, currentInviteeEmail} = this.state
        // const title = this.props.title || projectStore.currentProject.title
        // const senderName = userStore.currentUser.displayName || userStore.currentUser.firstName || userStore.currentUser.email
        if (allInvitees.findIndex( user => user.email === currentInviteeEmail) === -1) {
            allInvitees.push ({name: currentInviteeName, email: currentInviteeEmail})
            this.setState ({allInvitees: allInvitees})
            if (this.props.addNewUser)
                this.props.addNewUser({name: currentInviteeName, email: currentInviteeEmail})
        }
        this.closeEmailModal(ev)
    }

    componentDidMount() {
        const elem = findDOMNode(this)
        this.setState({elem: elem})
        if (elem) {
            elem.focus ()
        }
    }

    componentDidUpdate() {
        console.warn('UserPopup did update')
        const elem = this.state.elem
        if (elem) {
            const selRow = elem.querySelector('.active')
            if (selRow)
                selRow.scrollIntoView ({behavior: 'smooth', block: "start", inline: "nearest"})
        }
    }

    componentWillUnmount() {

    }

    render() {
        const {activeUser, hideActiveUser, selectedUser, selectedIndex, allUsers, bottom} = this.props
        const {displayUser, showInstructions, showEmailModal, query, searchResults, currentInviteeEmail, currentInviteeName, allInvitees} = this.state

        const width = '93%' // uiStateStore.currentApprovalRequest? '33%':'90%'
        const users = searchResults
        if (!users)
            return null

        const project = projectStore.currentProject
        const isApproval = project && project.projectType === ProjectType.APPROVAL
        const isEmailValid = isValidEmail(currentInviteeEmail)

        return (
            <div className='userPopup pad1 subtle-scroll-dark' style={{bottom: bottom, width: width}}>
{/*
                <div className='hdr tiny lighter secondary'>
                    {query && users.length > 0 && 'Contacts matching: "' +query+'"...'}
                    {users.length === 0 && 'No matches found for "' +query+ '"'}
                </div>
*/}
                {users && users.length > 0 &&
                <div className='userSearch mb1'>
                    <input type='text' className='formPane' value={query} onChange={this.handleSearchChange} placeholder='Search Contacts...'/>
                </div>
                }
                <X size={24} onClick={() => this.close()} className='close third abs hoh pointer' style={{top: '1.2rem', right: '1.2rem'}}/>

                <div className='userList subtle-scroll-dark' >
                    <div style={{marginBottom: '1rem'}} onClick={this.openEmailModal} className='pointer'>
                        <PlusCircle size={24} style={{marginBottom: '-6px', marginRight: '.5rem'}} /><i>Add new contact...</i>
                    </div>

                {allInvitees.map( (user,i) => {
                    return (
                        <div style={{marginBottom: '1rem'}} >
                            <Mail size={24} style={{marginBottom: '-6px', marginRight: '.5rem'}} />
                            <span className='secondary'>{user.name}</span>
                        </div>
                    )
                })}

                {users.map( (user,i) => {
                    if (hideActiveUser && activeUser && user && activeUser.id === user.id)
                        return null

                    return (
                        <MemberAvatar member={user}
                                      activeUser={activeUser}
                                      enableAdmin={false}
                                      project={projectStore.currentProject}
                                      key={'up'+i}
                                      index={i}
                                      color="black"
                                      showName={true}
                                      isEnabled={true}
                                      showMenu={false}
                                      overUser={this.overUser}
                                      clearUser={this.clearUser}
                                      clickAction={this.selUser}
                        />
                    )
                })}
                </div>
{/*
                <div className='userName'>
                    {displayUser? displayUser.displayName : null}
                    {showInstructions? <i>{MSG_CHOOSE_USER}</i> : null}
                </div>
                <Divider>Or</Divider>
*/}
{/*
                <div className='userEmails'>
                    <input className='formPane' placeholder='email address...' style={{width: '80%'}}/>
                    <Send size={20} onClick={this.handleEmailInvite}/>
                </div>
*/}
                {showEmailModal &&
                <div className='wModal'>
                    {isApproval &&
                    <h2 className='fg'>Send for approval to:</h2>
                    }

                    {!isApproval &&
                    <h2 className='fg'>Invite new contact to join "{project.title}"</h2>
                    }

                    <div>
                        <input name='currentInviteeEmail' className='sidePaneInput mr1' type='email'
                               placeholder='Email address' style={{width: '60%'}}
                               value={currentInviteeEmail}
                               onChange={this.handleInviteeChange}
                               onKeyDown={this.handleKeyDown}
                        />
                        <input name='currentInviteeName' className='sidePaneInput' type='text'
                               placeholder='Name (optional)' style={{width: '30%', marginRight: '1rem'}}
                               value={currentInviteeName}
                               onChange={this.handleInviteeChange}
                               onKeyDown={this.handleKeyDown}
                        />
                    </div>
                    <div className='mt1'>
                        <button className='button neutral' onClick={this.closeEmailModal}>Cancel</button>
                        <button className={'button' + (isEmailValid? ' go':' disabled')}
                                disabled={!isEmailValid}
                                onClick={this.handleAddNewUser}>
                            Send Email Invitation
                        </button>
                    </div>
                </div>
                }
            </div>
        )
    }
}