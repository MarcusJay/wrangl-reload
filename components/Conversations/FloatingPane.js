import {Component} from 'react'
import {X} from "react-feather";
import {findDOMNode} from "react-dom";
import Draggable from 'react-draggable'

export const FPANE_W = 300
export const FPANE_H = 350
export const FPANE_HDR_H = 40
export const FPANE_BODY_H = FPANE_H - FPANE_HDR_H
const MARGIN = 20
const hasUnits = /[a-zA-Z]+$/i


export default class FloatingPane extends Component {
    constructor(props) {
        super(props)
        this.state = {dragging: false, elem: null}
    }

    close = () => {
        this.props.onClose()
    }

    onClick = () => {
        if (this.props.onClick)
            this.props.onClick()
    }

    onFocus = () => {
        if (this.props.onFocus)
            this.props.onFocus()
    }

    onTitleClick = () => {
        this.props.onTitleClick()
    }

    componentDidMount() {
        const elem = findDOMNode(this)
        this.setState({elem: elem})
    }

    render() {
        let {index, title, component, image, titleHint} = this.props // default dimensions for now. Easy to add custom w/h

        let style = {bottom: 0, right: MARGIN + (index * (FPANE_W + MARGIN)), width: FPANE_W, height: FPANE_H}
        // let style = {width: FPANE_W, height: FPANE_H}

        return (
            <Draggable handle='.dragHandle'>
            <div className='floatingPane z2' style={style} onClick={this.onClick} onFocus={this.onFocus}>
                <div className='dragHandle hdr pointer' style={{height: FPANE_HDR_H}}
                     onClick={this.onFocus}
                     // onMouseDown={this.startDrag}
                     // onMouseUp={this.stopDrag}
                >
                    {image &&
                    <div className='round mr05 inline cover' style={{backgroundImage: 'url('+image+')', width: '1.75rem', height: '1.75rem'}}>&nbsp;</div>
                    }
                    <span className='uoh' onClick={this.onTitleClick} data-tip={titleHint}>{title}</span>
                    <X size={18} onClick={this.close} className='bold right'/>

                </div>
                {component}
            </div>
            </Draggable>
        )
    }

    // Basic draggin
    startDrag = (ev) => {
        console.log('### startDrag')
        ev.preventDefault();
        // get the mouse cursor position at startup:
        this.mouseX = ev.clientX
        this.mouseY = ev.clientY
        this.dragging = true
        // this.state.elem.onmousedown = this.
        this.state.elem.onmousemove = this.dragPane

        // this.setState({dragging: true})
    }

    dragPane = (ev) => {
        console.log('### dragPane')
        ev.preventDefault();
        const elem = this.state.elem

        // calc new position
        this.paneX = this.mouseX - ev.offsetX;
        this.paneY = this.mouseY - ev.offsetY;
        this.mouseX = ev.offsetX;
        this.mouseY = ev.offsetY;
        console.log('### dragging: ' +this.paneX+ ', ' +this.paneY)

        // set position
        if (!elem) {
            console.error ('Attempt to drag pane before element mounted')
            return
        }
        elem.style.bottom = (this.paneY) + "px";
        elem.style.left = (this.paneX) + "px";

    }

    stopDrag = (ev) => {
        console.log('### stopDrag')
        this.dragging = false
        this.state.elem.onmousemove = null
        // this.setState({dragging: false})
    }


}