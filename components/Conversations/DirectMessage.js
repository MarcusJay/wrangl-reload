import {Component} from 'react'
import {Link} from '~/routes'
import {observer} from 'mobx-react'
import {findDOMNode} from 'react-dom'
import {Loader} from 'semantic-ui-react'
import RMoment from 'react-moment'
import moment from 'moment'


import {FPANE_BODY_H} from "./FloatingPane";
import userStore from '/store/userStore'
import eventStore from '/store/eventStore'
import uiStateStore from "../../store/uiStateStore";
import {isUrl} from "../../utils/solo/isUrl";
import {contactBG, contactFG, myBG, myFG} from "../../config/Constants";
import {isBrowser} from "../../utils/solo/isBrowser";

const MAX_ELAPSED = 15 * 60 * 1000
const MAX_TALKERS = 2
const INPUT_H = 42
const WRAP_SIZE = 34

// trying out some css in js
const fg = '#eee'
const border = '#555'

// light theme input
const inputBG = '#F1F0F7' // $jDMBG
const inputFG = '#777' // $jDMFG

// dark theme input inverse
const dInputBG = inputFG
const dInputFG = inputBG

const dmStyleFloating = {
    position: 'relative',
    height: FPANE_BODY_H,
    fontSize: '0.8rem',
    overflow: 'hidden'
}

const dmStyleEmbedded = {
    position: 'relative',
    height: '99%',
    fontSize: '0.8rem'
}

const histStyle = {
    padding: '14px',
    overflowY: 'scroll',
    overflowX: 'hidden',
    position: 'absolute',
    bottom: INPUT_H,
    height: FPANE_BODY_H - INPUT_H - 1,
    width: '100%',
    marginBottom: '1px'
}

const inputPaneStyle = {
    bottom: 0,
    width: '96%',
    height: INPUT_H+'px',
    textAlign: 'center'
}

const inputStyle = {
    color: inputFG,
    background: inputBG,
    border: 'none',
    borderRadius: 5,
    width: '96%',
    padding: '.75rem'
}

const inputStyleEmb = {
    color: dInputFG,
    background: dInputBG,
    border: 'none',
    borderRadius: 5,
    width: '96%',
    padding: '.75rem'
}

const avtStyle = {
    maxWidth: '1.75rem',
    maxHeight: '1.75rem',
    borderRadius: '50%'
}

const timeStyle = {
    verticalAlign: 'top',
    color: '#aaa', // define constants
    marginLeft: '0.5rem'
}

const modules = {
        toolbar: false
}


@observer
export default class DirectMessage extends Component {
    constructor(props) {
        super(props)
        // if (false && document) // temp disabled
        //     this.ReactQuill = require('react-quill')

        this.state = {
            loading: false,
            convo: [],
            inputText:'',
            showAlert: false,
            submitting: false,
            submittedComment: null,
        }
    }

    onClick = (ev) => {
        console.log('click. value: ' +ev.target)
        ev.target.focus()
        eventStore.getInstance().markAsRead(this.props.contactId)
    }

    onKeyPress = (ev) => {
        console.log('key. value: ' +ev.key+ ", val: " +ev.target.value)
        if (ev.key === 'Enter')
            this.sendMsg(ev)
    }

    sendMsg = (ev) => {
        this.setState({submitting: true})
        const {contactId} = this.props
        let text = ev.target.value

        if (contactId) {
            uiStateStore.playSound()
            userStore.addToDM(text, contactId).then( convo => {
                this.setState({convo: convo, submitting: false, submittedComment: text, inputText: ''}) // TODO WHy local state? Why not mobx
            })
        } else {
            console.error('sendMsg: missing user(s). cannot submit ' +text)
        }
    }

    deleteMsg(ev, comment) {
        ev.stopPropagation()
        ev.preventDefault()

    }

    onChange = (ev) => {
        this.setState({inputText: ev.target.value})
    }

    focusInput() {
        const elem = this.state.elem || findDOMNode(this)
        if (elem) {
            const inputElem = elem.querySelector ('#cInput')
            if (inputElem)
                inputElem.focus ()
        }
    }

    scrollToRecent() {
        let elem = this.state.elem
        if (!isBrowser() || !elem)
            return

        const history = elem.querySelector ('#hist')
        if (!history)
            return

        history.scrollTop = history.scrollHeight
    }

    fetchConvo() {
        const {contactId, isPF} = this.props
        console.log('fetching convo with ' +contactId)
        this.setState ({loading: true})
        const isEmbedded = isPF && !uiStateStore.isDMOpen(contactId)

        if (!isEmbedded) {
            uiStateStore.refreshDM (contactId).then (convo => {
                this.scrollToRecent ()
                this.setState ({loading: false})
            })
        } else {
            userStore.fetchConvo(null, contactId).then( convo => {
                uiStateStore.embeddedDM = {contactId: contactId, convo: convo}
                this.scrollToRecent ()
                this.setState({loading: false})
            })
        }
    }

    componentDidMount() {
        const elem = findDOMNode(this)
        this.setState({elem: elem})
        if (userStore.currentUser)
            this.focusInput()
        this.fetchConvo()
    }

    componentDidUpdate() {
        console.warn('DM did update')
        this.scrollToRecent()
    }

    componentWillReact() {
        const {contactId, isPF} = this.props
        console.log('DM react: contact ' +contactId)
    }

    render() {
        const {contactId, isPF, mobile} = this.props
        const {loading} = this.state
        const convo = isPF? uiStateStore.embeddedDM.convo : uiStateStore.openDMs[contactId].convo
        if (!convo) // yet
            return

        const myUserId = userStore.currentUser.id
        const mobxTrigs = uiStateStore.rand + 1

        const dmStyle = isPF? dmStyleEmbedded : dmStyleFloating
        const dmInStyle = isPF? inputStyleEmb : inputStyle
        const histHeight = isPF? 'calc(100% - INPUT_H - 30)' : (FPANE_BODY_H - INPUT_H - 1)
        const histStyleLocal = Object.assign({}, histStyle, {height: histHeight})

        let prevAuthorId = '', prevTimestamp = '' // comments && comments.length > 0? comments[0].comment_creator : '0'
        let elapsed, rowStyle, outerStyle, innerStyle, isMe, showMetadata, isMultiline, words

        console.log('I have ' +convo.length+ ' comments.')

        return (

            <div className="dmsg" style={dmStyle} onFocus={this.onFocus} onClick={this.onClick}>
                    <Loader active={loading}></Loader>
                    <div id='hist' className='hidden-scroll-light' style={histStyleLocal}>
                        {convo.map ( (msg,i) => {
                            if (!msg.user) {
                                msg.user = userStore.getUserFromConnections(msg.comment_creator)
                            }

                            elapsed = prevTimestamp ? moment (msg.time_created).diff (moment (prevTimestamp)) : 100000
                            isMe = msg.user && msg.user.id === myUserId
                            isMultiline = msg.comment_text && msg.comment_text.length > WRAP_SIZE
                            rowStyle = {textAlign: isMe? 'right':'left'}
                            outerStyle = {backgroundColor: isMe? myBG : contactBG, color: isMe? myFG : contactFG}
                            // innerStyle = {textAlign: isMe? 'right':'left'}
                            showMetadata = (prevAuthorId !== msg.comment_creator || elapsed >= MAX_ELAPSED)
                            prevAuthorId = msg.comment_creator
                            prevTimestamp = msg.time_created

                            words = msg.comment_text? msg.comment_text.split(' ') : []

                            return (
                                <div className='msgRow clear' style={rowStyle} key={'msg' + i}>
                                    {showMetadata &&
                                    <div className='clear' style={{textAlign: isMe? 'right':'left', marginBottom: '0.5rem'}}>
                                        {!isMe && msg.user.image &&
                                        <img style={avtStyle} src={msg.user.image}/>
                                        }
                                        <RMoment style={timeStyle} subtract={{hours: 7}} format={'MMM Do, h:mm a'}>{msg.time_created}</RMoment>
                                    </div>
                                    }

                                    {/*<div style={{width: '100%'}}>*/}
                                    <div className='msgOuter clear' style={outerStyle}>   {/*height: '1.9rem',*/}
                                        <span className='msgInner' >
                                            {words.map( (word, i) => {
                                                return isUrl(word)? <a href={word} target='_blank'>{word}</a> : word + (i<words.length-1? ' ':'')
                                            })}
                                        </span>
                                    </div>
                                    {/*</div>*/}
                                </div>
                            )
                        })}
                    </div>

                    <div className='abs' style={inputPaneStyle}>
                        <input placeholder='Type a message...'
                               style={dmInStyle}
                               onChange={this.onChange}
                               onKeyPress={this.onKeyPress}
                               id="cInput"
                               value={this.state.inputText}
                        />

                    </div>
            </div>
        )
    }
}