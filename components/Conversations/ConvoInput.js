import React, {Component} from 'react'
import {observer} from 'mobx-react'
import { Picker } from 'emoji-mart'


import {findDOMNode} from 'react-dom'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import {DropTarget} from 'react-dnd'
import {Icon} from "semantic-ui-react";
import TextArea from 'react-textarea-autosize'
import {DndItemTypes, ENTER, ESCAPE, myBG, myBGDrop, myFG, ProjectType} from "../../config/Constants";

import AuthProtector from "../common/AuthProtector";
import AtCard from "./AtCard";
import projectStore from "../../store/projectStore";
import MemberAvatar from "../common/MemberAvatar";
import ReactionApiService, {voteTypeToId} from "../../services/ReactionApiService";
import {THUMBS_DOWN, THUMBS_UP} from "../../config/ReactionConfig";
import uiStateStore from "../../store/uiStateStore";
import ReactTooltip from "react-tooltip";
import {truncate} from "../../utils/solo/truncate";
import {Smile} from "react-feather";

const inputTarget = {
    drop(props, targetMonitor, component) {
        let dragItem = targetMonitor.getItem()
        const msgId = props.editingMsg? props.editingMsg.comment_id : null

        component.setState({expanded: true})

        // add card reference
/*
        if (dragItem.type === DndItemTypes.CARD) {
            props.onCardDrop(dragItem.id, msgId)
        }
*/

        // add person reference
        if (dragItem.type === DndItemTypes.PERSON || dragItem.type === DndItemTypes.MEMBER_AVATAR) {
            props.onUserDrop(dragItem.id, msgId)
        }

    }
}


// A single message in a Conversation.
@DropTarget([DndItemTypes.PERSON, DndItemTypes.MEMBER_AVATAR], inputTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@observer
export default class ConvoInput extends Component {

    prevEditingMsg = null
    state = {
        localValue: this.props.value, // use local to avoid touching larger collections in parent. per evite.
        atUsers: [],
        showEmojis: false
    }

    expand = (ev) => {
        uiStateStore.expandDiscussionInput = true
        if (this.props.onExpand)
            this.props.onExpand()
    }

    collapse = (ev) => {
        uiStateStore.expandDiscussionInput = false
        if (this.props.onCollapse)
            this.props.onCollapse()
    }

    onChange = (ev) => {
        this.setState({localValue: ev.target.value})
    }

    onEmoji = (emoji) => {
        const emojiCode = emoji.native || emoji.unified
        this.setState({localValue: this.state.localValue + ' ' +emojiCode})
        if (this.props.onEmoji)
            this.props.onEmoji(emojiCode)
        this.closeEmojiPicker()
    }


    onKey = (ev) => {
        if (ev.key === ENTER) {
            ev.preventDefault()
            this.submitMsg (ev)
        }
    }

    cancelMsg = (ev) => {
        // this.setState({atCards: []})
        uiStateStore.expandDiscussionInput = false
        if (this.props.onCancel)
            this.props.onCancel()
    }

    submitMsg = (ev) => {
        const {localValue} = this.state
        const {atCards, atUsers} = this.props
        const enableSend = localValue.length > 0 || atCards.length > 0 || atUsers.length > 0
        if (!enableSend)
            return

        // this.setState({expanded: false})
        uiStateStore.expandDiscussionInput = false
        this.props.onSubmit(ev, localValue)
        this.setState({localValue: '', showEmojis: false})
    }

    removeAttm = (itemId, itemType) => {
        this.props.onRemoveAttm(itemId, itemType)
    }

    handleFocus = (ev) => {
        uiStateStore.arrowPriority = 'text'
    }
    handleBlur = (ev) => {
        uiStateStore.arrowPriority = 'nav'
    }

    showExtras(show) {
        this.setState({showExtras: show})
        ReactTooltip.rebuild()
    }

    showCardPopup = (ev) => {
        if (this.props.showCardPopup)
            this.props.showCardPopup(true)
        // setTimeout(() => {this.showExtras(false)}, 300)
    }

    showUserPopup = (ev) => {
        if (this.props.showUserPopup)
            this.props.showUserPopup(true)
        // setTimeout(() => {this.showExtras(false)}, 300)
    }

    openEmojiPicker = (ev) => {
        if (!this.state.showEmojis)
            this.setState({showEmojis: true})
    }

    closeEmojiPicker = (ev) => { this.setState({showEmojis: false}) }

    handlePickerClick = (event) => {
        try {
            let node = findDOMNode(this.refs.emojiPicker);
            if (!node.contains(event.target)) {
                this.setState({showEmojis: false})
            }
        } catch(error) {
            return null
        }
    }

/*
    handlePickerKey = (ev) => {
        debugger
        if (ev.key === ESCAPE && this.state.showEmojis)
            this.setState({showEmojis: false})
    }
*/

    componentDidMount() {
        this.thumbsUpId = voteTypeToId(THUMBS_UP)
        this.thumbsDownId = voteTypeToId(THUMBS_DOWN)

        const elem = findDOMNode(this)
        this.setState({elem: elem})
        if (!elem) {
            console.error("CONVO INPUT HAS NO ELEMENT AFTER MOUNT. THIS IS A VIOLATION OF REALITY CODE 234097432")
            return
        }

        if (this.textarea) {
            // this.textarea.focus()
            this.setState({input: this.textarea})
            this.textarea.onfocus = () => this.expand()
            // this.textarea.onblur = () => setTimeout(() => this.collapse(), 300)
        }

        // Emoji Mart doesn't close unless you pick an emoji. Workaround.
        // document.addEventListener('keydown', this.handlePickerKey)
        document.addEventListener('mousedown', this.handlePickerClick)

    }

    componentDidUpdate() {
        if (this.props.editingMsg && this.prevEditingMsg !== this.props.editingMsg) {
            this.setState ({localValue: this.props.editingMsg.comment_text})
            this.prevEditingMsg = this.props.editingMsg
        }
    }

    componentWillUnmount() {
        const elem = this.state.elem || findDOMNode(this)
        if (!elem)
            return

        if (this.textarea) {
            this.textarea.removeEventListener('focus', this.expand)
            // this.textarea.removeEventListener('blur', this.collapse)
        }
        // document.removeEventListener('keydown', this.handlePickerKey)
        document.removeEventListener('mousedown', this.handlePickerClick)
    }

    render() {
        const {isAnon, msg, mobile, focus, isOver, connectDropTarget, editingMsg, viewCard, viewUser, /*pleaseExpand,*/
                onInput, onChange, onKeyUp, onKeyDown, onSubmit, onCancel, onCardDrop, onUserDrop, onExpand, onCollapse,
                value, userNamePreview, atCards, atUsers, atCardsLenTest} = this.props
        const {localValue, showEmojis}= this.state
        const expanded = editingMsg || uiStateStore.expandDiscussionInput
        const isPF = uiStateStore.currentPage === uiStateStore.PF

        const containerClass = 'msgEditor ' +(expanded? 'expanded':'collapsed') + (isOver? ' active':'')
        const inputClass = 'convoInput' + (isOver? ' active':'') + (expanded? ' double':'')
        const expHeight = (atCards.length > 0? 6 : 0) + (atUsers.length > 0? 5.5 : 0)
        const containerStyle = {
            backgroundColor: (expanded? (isOver? myBGDrop:myBG) : 'transparent'),
            color: myFG,
            height: expanded? (expHeight+'rem') : '',
            width: mobile? '70%':'23%',
            display: (mobile && !uiStateStore.showMobSidebarR)? 'none':'block'
        }

        let inputStyle = {whiteSpace: 'pre-wrap', width: '100%'}
        if (isOver)
            Object.assign(inputStyle, {borderColor: '#55aace', boxShadow: '0 0 10px #55aace'})

        const rows = expanded? 2 : 1
        const enableSend = value.length > 0 || atCards.length > 0 || atUsers.length > 0

        const text = value // _makeText(value, userNamePreview)
        const displayText = text.replace(/@/g, '')
        const project = projectStore.currentProject
        const isApproval = project && project.projectType === ProjectType.APPROVAL

        const placeholder = uiStateStore.filterDiscussionByCard && uiStateStore.viewingCard? 'Comment on item "' +truncate(uiStateStore.viewingCard.title, 24, true)+ '"' : 'Add a comment...'

        let approved, rejected, rating

        return connectDropTarget(
            <div className={containerClass} style={containerStyle}>

                <AuthProtector isAnon={isAnon} component={
                    <TextArea
                        placeholder={isAnon ? 'Signup to join the conversation!' : placeholder}
                        // autoFocus
                        minRows={rows}
                        maxRows={80}
                        className={inputClass}
                        style={inputStyle}
                        // inverted
                        // onInput={onInput}
                        onChange={this.onChange} /* moving to local state for value for speed */
                        onKeyDown={this.onKey}
                        onFocus={this.handleFocus}
                        // onBlur={this.handleBlur}
                        inputRef={tag => (this.textarea = tag)}
                        id='cInput'
                        value={localValue}
                    />
                }/>

                {expanded &&
                <span className='tiny right '>
                    {!isApproval &&
                    <span className='jSec lighter pointer' style={{marginRight: '.5rem'}} onClick={this.cancelMsg}>
                        Cancel
                    </span>
                    }
                    <span className={'jFG'+(enableSend? ' pointer':' black lighter')} onClick={this.submitMsg}>
                        {editingMsg? 'Save':'Send'}
                    </span>
                </span>
                }

                {!editingMsg &&
                <span className='abs ctrls '>
                    {expanded &&
                    <div className='abs' style={{padding: 0, bottom: '0', right: '2rem', minWidth: '96px', textAlign: 'right'}}>
                        <span onClick={this.openEmojiPicker}
                              className={'jIconBG jFG inline round vaTop centered pointer' +(isPF? '':' mr05 ')}
                              data-tip="Add Emojis"
                              style={{width: '20px', height: '20px', paddingTop: '2px'}}>
                            <Smile size={16}/>
                        </span>

                        {showEmojis &&
                        <Picker set='emojione'
                                ref='emojiPicker'
                                style={{position: 'absolute', bottom: '2rem', right: '26%', cursor: 'pointer'}}
                                onSelect={this.onEmoji}
                            // onClick={this.handlePickerClick}
                            // onKeyDown={this.handlePickerKey}
                                emoji='thumbsup'
                                title='Pogomoji'
                        />
                        }

                        {!isPF &&
                        <img onClick={this.showUserPopup} src='/static/svg/v2/usersCircleFill.svg'
                             width={20} className='mr05 pointer' data-tip="Tag contact"/>
                        }
                        {!isPF &&
                        <span onClick={this.showCardPopup}
                              className='jIconBG jFG bold large inline round vaTop centered pointer'
                              data-tip="Tag item"
                              style={{width: '18px', height: '18px', marginTop: '0'}}>+</span>
                        }

                    </div>
                    }

                    {!expanded &&
                    <Icon name='triangle down' className='secondary pointer' onClick={this.expand}/>
                    }
                    {expanded &&
                    <Icon name='triangle up' className='black pointer' onClick={this.collapse}/>
                    }


                </span>
                }

                {expanded &&
                <div className='attmsPane secondary tiny lighter'>
                    {atCards.length === 0 && !atUsers.length === 0 &&
                    <span >
                        Drop cards or users to attach them to this message
                    </span>
                    }

                    {atCards.length > 0 &&
                    <div className='cardAttms'>
                            {atCards.map ((card,i) => {
                                approved = ReactionApiService.hasUserReacted(card, this.thumbsUpId)
                                if (!approved)
                                    rejected = ReactionApiService.hasUserReacted(card, this.thumbsDownId)
                                rating = ReactionApiService.getLastStarRating(card)

                                return <AtCard card={card}
                                              key={'cattm'+i}
                                              showAttmMenu={editingMsg !== null || isApproval}
                                              removeAttm={() => this.removeAttm(card.card_id || card.id, 'card')}
                                              viewCard={() => viewCard(card.card_id || card.id)}
                                              showVoting={true}
                                              approved={approved}
                                              rejected={rejected}
                                              rating={rating}
                                />
                            })}
                    </div>
                    }

                    {atUsers.length > 0 &&
                    <div className='userAttms' style={{marginTop: '.5rem'}}>
                        {atUsers.map ((user,i) => {
                            if (!user || !user.id)
                                return null

                            return (
                            <div className='inline' onClick={() => viewUser (user.id)}>
                                <MemberAvatar member={user}
                                              enableAdmin={false}
                                              inMsgId={msg? msg.comment_id : null}
                                              project={projectStore.currentProject}
                                              key={'uattm'+i}
                                              index={i}
                                              color="black"
                                              showName={false}
                                              isEnabled={true}
                                              showAttmMenu={true}
                                              removeAttm={() => this.removeAttm(user.id,'user')}
                                />
                            </div>
                            )
                        })}
                    </div>
                    }

                </div>
                }
            </div>
        )
    }
}

function _makeText(text, userName) {
    if (!text || !userName)
        return text

    const atIdx = text.lastIndexOf('@')
    if (atIdx >= 0)
        return text.substring(0,atIdx) + '@' +userName+ ' '
    else
        return text
}

/**
 What determins expansion:
 1. card or user drop onto input.
 2. Edit existing message.

 It's so far a controlled component, so keep it that way.
 But since Expanded state can result from DropTarget here, it should be maintained here.
 It's better than Conversation not need to be aware our state. In this once case.

*/