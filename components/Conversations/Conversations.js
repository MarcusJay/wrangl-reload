import React, {Component} from 'react'
import {DropTarget} from 'react-dnd'
import {
    ALT,
    AT,
    CTRL,
    DndItemTypes,
    ENTER,
    ESCAPE,
    HASH,
    ProjectType,
    SHIFT,
    TAB,
    UserRole,
    UserStatus
} from "../../config/Constants";
import {Link} from '~/routes'
import {observer} from 'mobx-react'
import {findDOMNode} from 'react-dom'
import {Dimmer, Loader} from 'semantic-ui-react'
import {Bell, ChevronLeft, ChevronRight} from 'react-feather'
import moment from 'moment'

import userStore from '/store/userStore'
import cardStore from '/store/cardStore'
import projectStore from '/store/projectStore'
import uiStateStore from '/store/uiStateStore'
import ArrayUtils from "/utils/ArrayUtils";
import {isBrowser} from "../../utils/solo/isBrowser";
import {
    attachItemToCommentApi,
    getCommentByIdApi,
    removeItemFromCommentApi,
    updateCommentApi
} from "../../services/CommentApiService";
import eventStore from "../../store/eventStore";
import ConvoInput from "./ConvoInput";
import ConvoMessage from "./ConvoMessage";
import {hydrateComment, hydrateConversation} from "../../utils/solo/hydrateConvo";
import {getUserFromMembers} from "../../models/ProjectModel";
import UserPopup from "./UserPopup";
import {isAtWordStart} from "../../utils/solo/isAtWordStart";
import UserApiService from "../../services/UserApiService";
import UserModel from "../../models/UserModel";
import CardPopup from "./CardPopup";
import markAsViewed from "../../utils/solo/markAsViewed";
import {truncate} from "../../utils/solo/truncate";
import {sendInvite} from "../../services/EmailService";
import EmailService from "../../services/EmailService";
import {randomStr} from "../../utils/solo/randomStr";

const MAX_ELAPSED = 60 * 1000
const MAX_TALKERS = 3
const MAX_MEMBERS_DISPLAY = 40
const MAX_NAMES =  4

const AUTO_MSG_MIN_ELAPSED = 1 * 1000
/*
 This regex finds any text appearing after the last @ which starts at a word boundary.
    positives:
    @dave           - dave
    @dave and @     - empty string
    @dave and @mary - mary

    negatives:
    dave
    dave@mary


*/
const inputParser = /@([\w\s]*)(?!.*[@\w])/
const pTagRE = /<p>(.+)<\/p>/
const spTagRE = /(?:<p>)(?:<span>)(.+)(?:<\/span>)(?:<\/p>)/ // no need for (?:) anymore


const convoTarget = {
    drop(props, targetMonitor, component) {
        let dragItem = targetMonitor.getItem()

        // add card reference to convo
        if (dragItem.type === DndItemTypes.CARD) {
            uiStateStore.isManualAttm = true
            component.attachItemToMsg(dragItem.id, 'card')
        }

        // add person reference to convo
        else if (dragItem.type === DndItemTypes.PERSON) {
            component.attachItemToMsg(dragItem.id, 'user')
        }

    }
}


@DropTarget([DndItemTypes.CARD, DndItemTypes.PERSON], convoTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@observer
export default class Conversations extends Component {
    constructor(props) {
        super(props)
        this.initialized = false
        this.isConvoPending = false
        this.doScroll = true
        this.participants = []
        this.fetchedUsersForProjId = null
        this.hydrating = false
        this.unreadCommentIds = []
        this.firstNewCommentId = 0
        this.prevProjectId = null
        this.prevFriendId = null // to indicate when to fetch DM convo

        this.state = {
            comments: [],
            loading: false,

            // state of convo input/editor
            inputText:'',
            userNamePreview: '',
            cardNamePreview: '',

            at_cards: [],
            at_users: [],

            editingMsg: null,
            editingMsgPristine: null,

            // state of tagging popups
            filteredUsers: [],
            selectedUser: null,
            selectedUserIndex: 0,
            selectedCard: null,
            selectedCardIndex: 0,

            showAlert: false,
            submitting: false,
            showMembers: false,
            // inputExpanded: false,

            showExtras: false

        }
        this.lastAutoMsg = null
        this.timeOfLastAutoMsg = new Date()

    }

    resetUnread = (projectId) => {
        this.unreadCommentIds = []
        this.firstNewCommentId = 0
        this.setState({update: new Date()})
    }

    hideDiscussion = (ev) => {
        uiStateStore.showDiscussion = false
    }

    showDiscussion = (ev) => {
        uiStateStore.showDiscussion = true
        if (this.props.project) {
            const {unreadCommentIds, firstNewCommentId} =
                markAsViewed (this.props.project.id, 'comments', this.resetUnread)
            this.unreadCommentIds = unreadCommentIds
            this.firstNewCommentId = firstNewCommentId
        }
    }

    areConvosEqual = (convo1, convo2) => {
        if (!convo1 || !convo2 || convo1.length !== convo2.length)
            return false

        const diff = ArrayUtils.arrayDiffByProp(convo1, convo2, 'time_modified')
        return (!diff || diff.length === 0)

    }

/*
    showExtras = (show) => {
        this.setState({showExtras: show})
    }
*/

    onClick = (ev) => {
        console.log('click. value: ' +ev.target)
        ev.target.focus()
    }

/*
    onChange = (ev) => {
        this.setState({inputText: ev.target.value})
    }
*/

    onKeyDown = (ev) => {
        const key = ev.key

        // Prevent TAB from stealing our focus
        if ((uiStateStore.showUserPopup || uiStateStore.showCardPopup) && key === TAB && ev.preventDefault) {
            ev.preventDefault()
        }

/*
        // Ignore meta keys used to compose other chars
        if ([SHIFT, CTRL, ALT].indexOf(key) !== -1)
            return

        this.fullText = this.state.inputText
        console.log ('Key DOWN: ' + key)
        const isPopupTrigger = [AT, HASH].indexOf (key) !== -1

        let atUser

        // initialize popups
        if (isPopupTrigger) {

            if (key === AT) {
                if (isAtWordStart(this.fullText))
                    this.showUserPopup (ev)

            } else if (key === HASH) {
                this.showCardPopup (ev)
            }
        }

        // Interact with open popups
        else if (uiStateStore.showUserPopup) {
            // const fcon = userStore.filteredConnections
            // const selIdx = this.state.selectedUserIndex || 0

            // close it
            if (key === ESCAPE || key === ENTER) {
                this.hideUserPopup (ev)
                if (key === ENTER && this.state.inputText.endsWith ('@'))
                    this.setState ({inputText: this.state.inputText.slice (0, this.state.inputText.length - 2)})

            }
        }
*/

        if (key === ENTER && !ev.shiftKey && this.fullText && this.fullText.length > 0) {
            this.submitComment (ev)
            this.setState({userNamePreview: null, selectedUser: null, selectedUserIndex: -1})
        }

        // else
        //     this.setState({inputText: this.fullText})

        /*
                if (!ev.target.value || ev.target.value.length === 0) {
                    this.hideUserPopup ()
                    this.clearUser ()
                }
        */


    }

    attachItemToMsg = (item, itemType, msgId) => {
        const {editingMsg} = this.state

        const itemModel = (typeof item === 'string')? _getItem(item, itemType) : item
        if (!itemModel) {
            console.error('attachitemToMsg: failed to find itemModel for ' +item)
            debugger
            return
        }
        const itemId = itemModel.id
        const propName = itemType === 'card'? 'at_cards' : itemType === 'user'? 'at_users': null

        // item dropped onto new msg in editor
        if (!msgId) {
            const collection = this.state[propName]
            if (collection.find(item => item.id === itemId) === undefined) {
                collection.push (itemModel)
                this.setState ({[propName]: collection})
            }
            // this.setState({inputExpanded: true})
            uiStateStore.expandDiscussionInput = true
        }

        // item dropped onto existing msg in editor
        else if (editingMsg) {
            if (!editingMsg[propName])
                editingMsg[propName] = [itemModel]
            else
                editingMsg[propName].push(itemModel)
            this.setState({editingMsg: editingMsg})
            // don't auto save while editing
        }

        // item dropped onto existing msg in history. save immediately.
        else {
            this.setState ({submitting: true})
            this.doScroll = false
            attachItemToCommentApi (itemId, itemType, msgId).then (r1 => {
                this.refreshMessage (msgId)
                this.doScroll = true
            })
        }

    }

    removeItemFromMsg = (itemId, itemType) => {
        const {editingMsg} = this.state

        try {
            const itemModel = _getItem (itemId, itemType)
            const propName = itemType === 'card' ? 'at_cards' : itemType === 'user' ? 'at_users' : null

            const collection = editingMsg !== null? editingMsg[propName] : this.state[propName]
            const index = collection.findIndex (item => item.id === itemId || item.card_id === itemId || item.user_id === itemId)
            collection.splice (index, 1)

            if (editingMsg) {
                this.setState({editingMsg: editingMsg})
                removeItemFromCommentApi(itemId, itemType, editingMsg.comment_id)
            } else {
                this.setState({[propName]: collection})
            }
            if (this.props.onRemoveItem)
                this.props.onRemoveItem(itemId, itemType)
        } catch (error) {
            console.error("Error removing item from msg: ", error)
        }
    }

    refreshMessage = (msgId) => {
        this.setState({submitting: true})
        getCommentByIdApi(msgId).then( response => {
            const updatedCmt = hydrateComment(response.data.comments[0], (userId) => userStore.getUserFromConnections(userId))
            let convo = _getMyConvo(this)

            const updateIndex = convo.findIndex( cmt => cmt.comment_id === msgId)

            // TODO REFACTOR: Put convos in 1 store.
            // Call store @action here, not a direct splice.
            convo.splice(updateIndex, 1, updatedCmt)
            this.setState({submitting: false})
        })
    }

    submitComment = (ev, text) => {
        const me = userStore.currentUser
        const isAnon = !me || me.status === UserStatus.ANON

        if (isAnon) {
            uiStateStore.setJoinPromptState(true)
            return
        }

        if (this.state.editingMsg)
            this.updateComment (ev, text)
        else
            this.addComment (ev, text)

        this.hideUserPopup(ev)
        this.hideCardPopup(ev)
        // this.setState({inputExpanded: false})
        uiStateStore.expandDiscussionInput = false
    }

    addComment = (ev, text) => {
        uiStateStore.playSound ()
        this.setState ({submitting: true})
        const {project, user, friend} = this.props
        const {at_cards, at_users} = this.state
        const atCardIds = at_cards? at_cards.map (card => card.id || card.card_id) : null
        const atUserIds = at_users? at_users.map (user => user.id || user.user_id) : null

        if (text.length === 0 && (at_cards.length > 0 || at_users.length > 0))
            text = ' ' // so api will accept "blank" message. Is this still needed? Ask Chris.

        // We're playing with @'s here. You and/or your mom may disapprove of this.
        // It'll change whence we implement text filtering, which was 70% fleshed out. See State Diagram. Where? slack.
        // We're using them behind the scenes to slugify users, but NOT displaying them in text inputs nor messages.
        if (at_users && at_users.length > 0) {
            at_users.forEach( user => {
                const index = text.indexOf(user.displayName)
                if (index === 0 || (index > 0 && text.charAt(index-1) !== '@'))
                    text = text.slice(0,index) + '@' + text.slice(index)
            })
        }

        if (project && !friend) {
            projectStore.addToConvo(text, project.id, atCardIds, atUserIds).then( response => {
                this.refreshMessage(response.msg.comment_id)
                this.resetInputState()
                this.scrollToLatest()
            })

        } else if (user && friend) {
            userStore.addToDM(text, friend.id, atCardIds, atUserIds).then( convo => {
                this.resetInputState()
                this.scrollToLatest()
            })

        } else {
            console.error('submitcomment: no project and no user ids. cannot submit ' +text)
        }
    }

    updateComment = (ev, text) => {
        const {editingMsg, editingMsgPristine} = this.state
        this.setState ({submitting: true})
        const {project, user, friend} = this.props

        const atCardIds = editingMsg.at_cards && editingMsg.at_cards.length > 0? editingMsg.at_cards.map (card => card.id || card.card_id) : null
        const atUserIds = editingMsg.at_users && editingMsg.at_users.length > 0? editingMsg.at_users.map (user => user.id || user.user_id) : null

        // TODO we can simplify this by just sending entire array as replacement.
        const origCardIds = editingMsgPristine.at_cards? editingMsgPristine.at_cards.map (card => card.id || card.card_id) : null
        const origUserIds = editingMsgPristine.at_users? editingMsgPristine.at_users.map (user => user.id || user.user_id) : null
        let addCardIds, addUserIds, removeCardIds, removeUserIds
        // if (atCardIds && origCardIds)
        //     = ArrayUtils.arrayDiffByProp(origCardIds, atCardIds)
        /*
            LEFT OFF
            We'll need to compute deltas in all directions and use cases, eg. had no card before, had some, adding, removing.
            Better to go with Chris idea to pass full array as replacement.
         */

        // copy to slim obj to avoid api rejecting extra fields
        const updMsg = {comment_id: editingMsg.comment_id, comment_text: text}
        if (atCardIds)
            updMsg.at_cards = atCardIds
        if (atUserIds)
            updMsg.at_users = atUserIds

        updateCommentApi(updMsg).then( response => {
            this.refreshMessage(updMsg.comment_id)
            this.resetInputState()
        })
    }

    autoComment = (text, card) => {
        const {project} = this.props
        if (!project || !text || text.length === 0) {
            debugger
            return
        }

        this.setState ({submitting: true})
        const user = userStore.currentUser
        if (!card)
            card = this.props.inputCard

        const atCardIds = card? [card.id] : null

        // don't put these in state, or you risk triggering redundant updates
        this.lastAutoMsg = text
        this.timeOfLastAutoMsg = new Date()

        projectStore.addToConvo(text, project.id, atCardIds, []).then( response => {
            this.refreshMessage(response.msg.comment_id)
            // this.resetInputState()
            // this.scrollToLatest()
        })
    }

    cancelComment = (ev) => {
        const {editingMsg, editingMsgPristine} = this.state
        if (editingMsg) {
            editingMsg.at_cards = editingMsgPristine.at_cards
            editingMsg.at_users = editingMsgPristine.at_users
        }
        this.setState ({editingMsg: null, editingMsgPristine: null, submitting: false, inputText: '', at_cards: [], at_users: []})

        uiStateStore.expandDiscussionInput = false
        uiStateStore.filterDiscussionByCard = false

    }

    editMsg = (msg) => {
        const {project, user, friend} = this.props

        // deep copy, else attms are still referenced.
        const origMsg = {comment_id: msg.comment_id, comment_text: msg.comment_text}
        if (msg.at_cards)
            origMsg.at_cards = [...msg.at_cards]
        if (msg.at_users)
            origMsg.at_users = [...msg.at_users]

        if (project) {
            this.setState({editingMsg: msg, editingMsgPristine: origMsg, inputText: msg.comment_text})

        } else if (user && friend) {
            /*          TODO
                        userStore.deleteFromConvo(text, user.id, friend.id).then( convo => {
                            this.setState({submitting: false, submittedComment: text, inputText: ''})
                        })
            */
        }
    }

    deleteMsg = (msg) => {
        const {project, user, friend} = this.props
        const convo = _getMyConvo(this)

        if (project) {
            projectStore.deleteFromConvo(msg.comment_id, convo, project.id).then( convo => {
                this.setState ({submitting: false})
            })

        } else if (user && friend) {
            /*          TODO
                        userStore.deleteFromConvo(text, user.id, friend.id).then( convo => {
                            this.setState({submitting: false, submittedComment: text, inputText: ''})
                        })
            */
        }
    }

    showCardPopup = (ev) => {
        uiStateStore.showCardPopup = true // this.setState({showUserPopup: true})
    }

    hideCardPopup= (ev) => {
        uiStateStore.showCardPopup = false
        cardStore.filteredCards = userStore.currentCards
    }

    showUserPopup = (ev) => {
        uiStateStore.showUserPopup = true // this.setState({showUserPopup: true})
    }

    hideUserPopup= (ev) => {
        uiStateStore.showUserPopup = false
        userStore.filteredConnections = userStore.currentConnections
    }

    selUser = (u,index) => {
        const elem = findDOMNode(this)
        if (!elem) {
            console.error("Conversation has no element on selUser. REPLACE WITH REF")
            return
        }
        // TODO USE REF
        let value = elem.querySelector('#cInput').value
        if (value.length > 0 && !value.endsWith(' '))
            value += ' '
        value += ('@' + (u.displayName || u.firstName))
        this.attachItemToMsg(u.id, 'user')
        this.hideUserPopup ()

        this.setState({inputText: value, userNamePreview: null, selectedUser: null})
        uiStateStore.expandDiscussionInput = true
    }

    overUser = (user, userIndex) => {
        this.setState ({selectedUser: user, selectedUserIndex: userIndex})
        if (user) {
            this.setState ({userNamePreview: user.displayName})
        }
    }

    clearUser = (user, userIndex) => {
        this.setState ({selectedUser: null, selectedUserIndex: -1, userNamePreview: null})
    }

    inviteUser = ({name, email}) => {
        const activeUser = userStore.currentUser
        const project = projectStore.currentProject
        const isAP = project && project.projectType === ProjectType.APPROVAL

        const defaultPW = randomStr(4)
        userStore.createPendingUser( email, name, defaultPW)

        EmailService.sendInvite({
            recipientEmail: email,
            recipientName: name,
            recipientPW: defaultPW,
            senderName: (activeUser.displayName || activeUser.firstName),
            senderId: activeUser.id,
            isAP: isAP,
            projectId: project? project.id : null,
            imageUrl: project? project.image : 'https://pogo.io/static/img/pogoMulti.png',
            itemCount: project? project.cardTotal : 0,
            apDesc: project? project.description : null
        })
    }


    selCard = (cardId,index) => {
        const card = cardStore.getCardFromCurrent(cardId)
        if (!card) {
            console.error("Can't select card, not in currentCards")
            return
        }

        this.attachItemToMsg(card.id, 'card')
        this.hideCardPopup ()

        this.setState({cardNamePreview: null, selectedCard: null})
        uiStateStore.isManualAttm = true
        uiStateStore.expandDiscussionInput = true
    }

    overCard = (card, cardIndex) => {
        this.setState ({selectedCard: card, selectedCardIndex: cardIndex})
        if (card) {
            this.setState ({cardNamePreview: card.title})
        }
    }

    clearCard = (card, cardIndex) => {
        this.setState ({selectedCard: null, selectedCardIndex: -1, cardNamePreview: null})
    }

    onEmoji = (emojiCode) => {
        const val = this.state.inputText + ' ' +emojiCode
        this.setState({inputText: val})
    }

    onInputExpand = (ev) => {
        // this.setState({inputExpanded: true})
        uiStateStore.expandDiscussionInput = true
    }

    onInputCollapse = (ev) => {
        // this.setState({inputExpanded: false})
        uiStateStore.expandDiscussionInput = false
    }

    focusInput() {
        const elem = this.state.elem
/*      pass prop to convoInput
        if (elem && this.props.focus) {
            const inputElem = elem.querySelector ('#cInput')
            if (inputElem)
                inputElem.focus ()
        }
*/
    }

    scrollToLatest() {
        let elem = null
        try { // repeated window resizing can break findDOMNode
            elem = findDOMNode (this)
        } catch (error) {
            console.error(error)
            return
        }

        if (!isBrowser() || !elem)
            return

        const history = elem.querySelector('.history')
        if (!history)
            return
        history.scrollTop = history.scrollHeight

        const discuss = elem.querySelector('.discuss')
        if (!discuss)
            throw new Error('discuss not found')
        discuss.scrollTop = discuss.scrollHeight
    }

    // if normal project, scroll card into view
    // if approval, go to card detail
    viewCard = (cardId) => {
        // const project = projectStore.currentProject

        const cards = cardStore.currentCards
        const card = cardStore.getCardFromCurrent(cardId)

        const index = cards && cards.length > 0? cards.findIndex (c => c.id === cardId) : -1

        if (!card) {
            console.error('viewCard: cant find card ID: ' +cardId)
            return
        }

        uiStateStore.setViewingCard(card)

        /*
                if (project && project.projectType === ProjectType.APPROVAL) {
                    this.props.onViewCard(card, index)

                } else if (uiStateStore.cardsPane) {
                        const cardElem = uiStateStore.cardsPane.querySelector('#c'+cardId)
                        if (cardElem) {
                            cardElem.scrollIntoView ({behavior: 'smooth', block: "start", inline: "nearest"})
                            uiStateStore.flashCard = cardId

                            // It's mobx or this. Guess which one "just works"
                            cardElem.classList.add('cardHighlight')
                            setTimeout( () => { cardElem.classList.remove('cardHighlight')}, 3000)
                        }
                }
        */
    }

    viewUser(userId) {
    }

    resetInputState = () => {
        this.setState ({
           submitting: false,
           inputText: '',
           editingMsg: null,

           userNamePreview: null,
           selectedUserIndex: 0,
           selectedUser: null,

           cardNamePreview: null,
           selectedCardIndex: 0,
           selectedCard: null,

           at_cards: [],
           at_users: []
       })
    }

/*
    toggleFilterByCard = (ev) => {
        // ignore clicks if there's nothing to filter by
        if (!uiStateStore.viewingCard && !uiStateStore.filterDiscussionByCard)
            return

        uiStateStore.filterDiscussionByCard = !uiStateStore.filterDiscussionByCard

        // if we're stopping filtering, reset input for general board comments
        if (!uiStateStore.filterDiscussionByCard) {
            uiStateStore.expandDiscussionInput = false
            if (this.state.editingMsg)
                this.setState({editingMsg: null})
            else
                this.setState({at_cards: []})
        }

    }
*/

    setConvoToProject = (ev) => {
        uiStateStore.filterDiscussionByCard = false
        // uiStateStore.expandDiscussionInput = false
        if (uiStateStore.viewingCard)
            this.removeItemFromMsg(uiStateStore.viewingCard, 'card')
    }

    setConvoToCard = (ev) => {
        uiStateStore.filterDiscussionByCard = true
        // if (projectStore.currentProject && projectStore.currentProject.projectType === ProjectType.APPROVAL)
        //     return

        uiStateStore.expandDiscussionInput = true
        this.setState({at_cards: []}, () => {
            this.attachItemToMsg (uiStateStore.viewingCard, 'card', null)
        })
    }

    fetchDMConvo() {
        const {friend} = this.props
        userStore.fetchConvo(null, friend.id, true).then( convo => {
            uiStateStore.embeddedDM = {contactId: friend.id, convo: convo}
            this.scrollToLatest()
            // this.setState({loading: false})
        })
    }


    componentDidMount() {
        const elem = findDOMNode(this)
        this.setState({elem: elem, selectedCard: this.props.selectedCard}) // yes this is absolutely essential, allowing us to attach cards via popup, or via auto-select
        this.scrollToLatest()
        if (projectStore.currentProject)
            this.prevProjectId = projectStore.currentProject.id

        window.addEventListener("resize", this.scrollToLatest);

    }

    componentDidUpdate() {
        const {project, focus, friend, inputCard, autoMsg} = this.props
        const {at_cards} = this.state
        console.log('Conversations did update')
        if (this.doScroll)
            this.scrollToLatest()

        if (focus)
            this.focusInput()

        // if we're filtering by card and it's not attached yet, attach it.
        if (inputCard && uiStateStore.filterDiscussionByCard && (at_cards.length === 0 || at_cards.find(card => card.id === inputCard.id) === undefined)) {
            this.setState({at_cards: []}, () => {
                console.log("convo update: attachItem")
                this.attachItemToMsg (inputCard, 'card', null)
            })
        }

        // if we've stopped filtering by card and it was auto-attached, remove it.
        else if (!uiStateStore.isManualAttm && !inputCard && !uiStateStore.filterDiscussionByCard && at_cards.length > 0) {
            // this.setState({at_cards: []}, () => {
            console.log("convo update: removeItem")
            this.removeItemFromMsg(inputCard, 'card')
            // })
        }


        // yes, unconventional: it allows us to select cards from multiple sources, including popups, toolbars, and auto-select for approvals
        // Ok, well, I don't like it.
        if (this.props.selectedCard && (!this.state.selectedCard || this.props.selectedCard.id !== this.state.selectedCard.id)) {
            console.log('convo update: unconventional bit')
            this.setState({selectedCard: this.props.selectedCard})
        }

        const elapsed = new Date() - this.timeOfLastAutoMsg
        if (autoMsg && autoMsg !== this.lastAutoMsg && elapsed > AUTO_MSG_MIN_ELAPSED) {
            console.log('convo update: autocomment')
            this.autoComment(autoMsg, inputCard)
        }

        // Grab convo - from friend DM
        if (!project && friend && friend.id !== this.prevFriendId) {
            this.fetchDMConvo()
            this.prevFriendId = friend.id

        // Grab convo - from project
        } else {

            // grab any unconnected users from api. e.g. anon approval viewer has no connections.
            let convo = projectStore.currentProjectConvo
            if (!this.hydrating && project && this.fetchedUsersForProjId !== project.id
                && convo.length > 0) {

                this.hydrating = true
                const uniqueUserIds = new Set (convo.map (cmt => cmt.comment_creator) || [])
                UserApiService.getPublicUsersById (Array.from (uniqueUserIds)).then (response => {
                    this.participants = response.data.users.map (apiUser => apiUser ? new UserModel (apiUser) : null)
                    this.participants = this.participants.filter (p => p !== null)
                    convo = hydrateConversation (
                        convo,
                        (userId) => this.participants.find (user => user.id === userId) || null,
                        null)
                    this.fetchedUsersForProjId = project.id
                    this.hydrating = false
                })
            }
        }

        // update unread comment badges
        // TODO NOTE: Here's the argument for flat projects in badging. Currently selected category in PLWorkpane may be Cat A/
        // TODO User selects projects in left column, and selects/opens a random project in another category. We have no idea which.
        // TODO project has no ref to its category. So I'd still argue some kind of relationship would be helpful.
        // TODO For now, we will Search thru user_badges.

        if (project && uiStateStore.showDiscussion && this.prevProjectId !== project.id) {
            if (typeof markAsViewed === 'function') { // sporadic markAsView is undefined. Import failure?
                const response = markAsViewed (project.id, 'comments', this.resetUnread)
                this.unreadCommentIds = response.unreadCommentIds
                this.firstNewCommentId = response.firstNewCommentId
            }

        }
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.scrollToLatest)
    }

    componentWillReact() {
        console.log('Conversation will react.')
    }

    render() {
        const comments = _getMyConvo(this)
        if (!comments || uiStateStore.currentPage === uiStateStore.PP) {
            return null
        }

        const {loading, submitting, selectedUser, selectedUserIndex, selectedCard, selectedCardIndex, inputText, editingMsg,
              /*inputExpanded,*/ userNamePreview, at_cards, at_users} = this.state
        const {connectDropTarget, isOver, item, /*expand,*/ focus,
               mobile, layout, style, className, inputCard, inputCardIndex} = this.props
        const project = projectStore.currentProject
        const {totalProjectCommentEvents} = project? eventStore.getInstance().countEventBreakdown(project.id) : 0
        const miniView = !uiStateStore.showDiscussion
        const max = !mobile? MAX_MEMBERS_DISPLAY : MAX_MEMBERS_DISPLAY - 2
        const isApproval = project? project.projectType === ProjectType.APPROVAL : false

        const validMembers = project? project.members.filter( user => user.role !== UserRole.BANNED) : []
        const latestMembers = validMembers.slice(0, max)

        const noUser = !userStore.currentUser // || userStore.currentUser.status === UserStatus.ANON

        // convo input holds either existing or new msg
        const inputAtCards = editingMsg? (editingMsg.at_cards || []) : at_cards
        const inputAtUsers = editingMsg? (editingMsg.at_users || []) : at_users
        const parts = _parseInputText(inputText)
        const userQuery = parts? parts.userQuery : null
        const cardQuery = parts? parts.cardQuery : null

        let prevAuthorId = '', prevTimestamp = '' // comments && comments.length > 0? comments[0].comment_creator : '0'
        let stillTalking, elapsed, author, isUnread, showUnreadMarker
        const paneClass = 'comments hidden-scroll' +(className? (' '+className) : '') + (isOver? ' active':'')
        const historyClass = 'history fullW hidden-scroll' +(uiStateStore.expandDiscussionInput? ' shrink':' full') // shrink
        const historyStyle = {top: '5px', paddingLeft: '1rem', paddingRight: '1rem'} // 3.45rem

        const filterSize = '24px'
        const filterOpacity = uiStateStore.filterDiscussionByCard? 1 : 1 // 0.7
        const filterBorder = uiStateStore.filterDiscussionByCard? '2px solid white' : '2px solid black'
        // const cardOfInterest = selectedCard || inputCard || uiStateStore.viewingCard || uiStateStore.editingCard // in order of priority

        if (miniView && !mobile) {
            return (
                <div className='miniProjectComments' onClick={this.showDiscussion} data-tip="Show Comments">
                    <ChevronLeft size={22} className='utOpenConvo third subtleBtn'/>

                    {totalProjectCommentEvents > 0 &&
                    <div className="miniBadge pointer" >
                        <Bell size={14} style={{marginBottom: '0'}}/>
                    </div>
                    }

                    <div className={'jSec lighter krn1 pointer' +(layout === 'v'? ' rotate90cw':'' )}
                         style={{whiteSpace: 'pre', margin: '12.5rem 0 0 -11.25rem', fontSize: '1.2rem', width: '24rem'}}>
                        COMMENTS
                    </div>
                </div>
            )
        }

        return connectDropTarget(

            <div className={paneClass} style={style}>
                <Loader active={loading}></Loader>

                <Dimmer active={isOver}>
                    <div className='pad1' style={{marginTop: '0', height: '10%', color: '#777', fontSize: '2rem'}}>
                        Tag {item && item.type === DndItemTypes.PERSON? item.name : 'card'} in a new message?
                    </div>
                </Dimmer>

                <div className={"discuss" +(layout === 'h'? ' horiz':'')}>

                    {/*Indicate whether we're commenting on a specific item or the entire board*/}
{/*
                    <div className='msgRow pointer zModal'
                         style={{width: '100%', marginBottom: '.5rem', padding: '1rem'}}>

                        {uiStateStore.filterDiscussionByCard && uiStateStore.viewingCard &&
                        <div className='inline pointer mr05 cover'
                             style={{
                                 backgroundImage: 'url(' + uiStateStore.viewingCard.image + ')',
                                 width: filterSize,
                                 height: filterSize,
                                 opacity: filterOpacity,
                                 border: filterBorder,
                                 borderRadius: '4px',
                                 marginTop: '-8px'
                             }}
                        >&nbsp;</div>
                        }

                        <button className={'jBtn mr05' + (uiStateStore.filterDiscussionByCard? '':' selected')}
                                style={{width: '45%'}}
                                onClick={this.setConvoToProject}>
                            All comments
                        </button>
`
                        <button className={'jBtn' +(uiStateStore.viewingCard? '':' jFourth disabled') +(uiStateStore.filterDiscussionByCard? ' selected':'')}
                                style={{width: '45%'}}
                                onClick={this.setConvoToCard}
                                disabled={!uiStateStore.viewingCard}
                        >
                            The open item only
                            {uiStateStore.viewingCard? uiStateStore.viewingCard.title:'no viewingCard'}
                        </button>

                    </div>

*/}

                    <div className={historyClass} style={historyStyle}>


                        {(!comments || comments.length === 0) &&
                        <div className='small jFG mt05'>
                            {uiStateStore.filterDiscussionByCard && !loading ?
                                  "There are no comments on this item yet."
                                : ""
                            }
                        </div>
                        }

{/*
                        {comments && comments.length > 0 && uiStateStore.filterDiscussionByCard && selectedCard &&
                        <div className='msgRow small fg' style={{width: '99%'}}>
                            <span className='link' onClick={this.toggleFilterByCard}>Show whole conversation.</span>
                        </div>
                        }
*/}


                        {comments.map ( (message,i) => {

                            author = message.user || userStore.getUserFromConnections(message.comment_creator)

                            elapsed = prevTimestamp ? moment (message.time_created).diff (moment (prevTimestamp)) : 100000
                            stillTalking = prevAuthorId === message.comment_creator && elapsed < MAX_ELAPSED

                            prevAuthorId = message.comment_creator
                            prevTimestamp = message.time_created

                            isUnread = this.unreadCommentIds.indexOf(message.comment_id) !== -1
                            showUnreadMarker = this.firstNewCommentId === message.comment_id

                            return <ConvoMessage msg={message}
                                                 key={'m'+i}
                                                 isUnread={isUnread}
                                                 showUnreadMarker={showUnreadMarker}
                                                 author={author}
                                                 activeUser={userStore.currentUser}
                                                 stillTalking={stillTalking}
                                                 refreshMsg={(msgId) => this.refreshMessage(msgId)} /*or message.comment_id*/
                                                 viewCard={this.viewCard}
                                                 viewUser={(userId) => this.viewUser(userId)}
                                                 editMsg={
                                                     uiStateStore.filterDiscussionByCard || editingMsg?
                                                         null : this.editMsg
                                                 }
                                                 isEditing={editingMsg && editingMsg.comment_id === message.comment_id}
                                                 deleteMsg={this.deleteMsg}
                                                 // TODO remove these anon functions. User feedback is these features are unintuitive anyway
                                                 onCardDrop={(cardId) => this.attachItemToMsg(cardId, 'card', message.comment_id)}
                                                 onUserDrop={(userId) => this.attachItemToMsg(userId, 'user', message.comment_id)}
                                                 onCardRemove={(cardId) => this.removeItemFromMsg(cardId, 'card', message.comment_id)}
                                                 onUserRemove={(userId) => this.removeItemFromMsg(userId, 'user', message.comment_id)}
                                    />


                        })}
                        <p></p><p></p><p></p>
                    </div>

                    {uiStateStore.showUserPopup &&
                    <UserPopup query={userQuery}

                               allUsers={project? project.members : []}
                               filteredUsers={userStore.filteredConnections}
                               activeUser={userStore.currentUser}
                               selectedUser={selectedUser}
                               selectedIndex={selectedUserIndex}
                               bottom={uiStateStore.expandDiscussionInput? '8.2rem':'3rem'}

                               selUser={this.selUser}
                               addNewUser={this.inviteUser}
                               overUser={this.overUser}
                               clearUser={this.clearUser}
                               close={this.hideUserPopup}
                    />
                    }

                    {uiStateStore.showCardPopup &&
                    <CardPopup query={cardQuery}

                               allCards={cardStore.currentCards}
                               filteredCards={cardStore.filteredCards}
                               activeUser={userStore.currentUser}
                               selectedCard={selectedCard}
                               selectedCardIndex={selectedCardIndex}
                               bottom={uiStateStore.expandDiscussionInput? '8.2rem':'3rem'}

                               selCard={this.selCard}
                               overCard={this.overCard}
                               clearCard={this.clearCard}
                               close={this.hideCardPopup}
                    />
                    }

{/*
                    {showExtras &&
                    <div className='jPinkBG borderAll'>
                        extras
                    </div>
                    }
*/}

                    {!uiStateStore.filterDiscussionByCard && !uiStateStore.leftUnModal &&
                    <ConvoInput onClick={this.onClick}
                                // onInput={this.onChange}
                                // onChange={this.onChange}
                                onEmoji={this.onEmoji}
                                onSubmit={this.submitComment}
                                onCancel={this.cancelComment}
                                onKeyDown={this.onKeyDown}
                                onExpand={this.onInputExpand}
                                onCollapse={this.onInputCollapse}
                                // showExtras={this.showExtras}
                                showCardPopup={this.showCardPopup}
                                showUserPopup={this.showUserPopup}
                                // pleaseExpand={inputExpanded || expand} // component controls its own expanded state, however, with new userpopup, we ALSO need to instruct it to expand as well
                                viewCard={(cardId) => this.viewCard(cardId)}
                                viewUser={(userId) => this.viewUser(userId)}
                                id="cInput"
                                focus={focus}
                                editingMsg={editingMsg}
                                mobile={mobile}
                                isAnon={noUser}
                                value={noUser? '' : inputText}
                                userNamePreview={userNamePreview}
                                atCards={inputAtCards}
                                atCardsLenTest={inputAtCards.length}
                                atUsers={inputAtUsers}
                                onCardDrop={(cardId, msgId) => this.attachItemToMsg(cardId, 'card', msgId)}
                                onUserDrop={(userId, msgId) => this.attachItemToMsg(userId, 'user', msgId)}
                                onRemoveAttm={(itemId, itemType, msgId) => this.removeItemFromMsg(itemId, itemType, msgId)}
                    />
                    }
                </div>
            </div>
        )
    }
}

function _getMyConvo (component) {
    if (uiStateStore.currentPage === uiStateStore.PF)
        return uiStateStore.embeddedDM.convo // userStore.currentConvo
    else if (uiStateStore.currentPage === uiStateStore.PD || uiStateStore.currentPage === uiStateStore.AP) {
        const card = uiStateStore.viewingCard || component.props.selectedCard
        if (card && uiStateStore.filterDiscussionByCard)
            return projectStore.currentProjectConvo.filter( cmt => cmt.at_cards && (cmt.at_cards.find(c => c.card_id === card.id) !== undefined)) || []
        else
            return projectStore.currentProjectConvo
    }
    else
        return null
}

function _getItem (itemId, itemType) {
    if (itemType === 'card')
        return cardStore.getCardFromCurrent(itemId)
    else if (itemType === 'user') {
        let user = userStore.getUserFromConnections (itemId)
        if (!user && projectStore.currentProject) // project members may no longer be connected
            user = getUserFromMembers(itemId, projectStore.currentProject)
        return user
    }
    else
        return null
}

/*
    find last @
    all chars after that (including space) are query
    if @ is last char, indicate to keep popup up
    if there is no @, or if last @ user is already in atUsers, indicate to close popup.

    when a user is accepted/selected, add them to atUsers. Use that to check for last @ already used.
    we'll need to update atUsers to contain users tagged here.
    we might want to keep track of start/end index within inputText for each tagged user.

 */
function _getUserQueryFromInput(txt) {
    if (!txt || txt.length === 0)
        return null

    const startIdx = txt.lastIndexOf ('@')
    if (startIdx < 0)
        return null

    let phrase = txt.substring(startIdx)    // eg. @, @mar, @marc ja

    if (phrase === '@')                     // @
        return ''

    if (phrase.length > 1)                  // @mar, @mar ja
        return phrase.substring(1)

    return null
}

function _parseInputText(txt) {
    if (!txt || txt.length === 0)
        return {userQuery: null, cardQuery: null}

    let matches
/*
    if (spTagRE.test(txt)) {
        matches = spTagRE.exec (txt)
        txt = matches[1]
    } else if (pTagRE.test(txt)) {
        matches = pTagRE.exec (txt)
        txt = matches[1]
    }
*/

    matches = inputParser.exec(txt) // find any chars after last '@' if any of either
    let userQuery = null, cardQuery = null

    if (matches && matches.length > 1) {
        userQuery = matches[1]
    }
    return {userQuery: userQuery, cardQuery: null}
}

function _normalizeUserNames(text, users) {
    if (!users)
        return text

    users.forEach( user => {
        text = _normalizeUserName(text, user.displayName)
    })
    return text
}

function _normalizeUserName(text, rawName) {
    if (!text || !rawName)
        return text

    if (text.indexOf(rawName) >= 0) {
        let safeName = rawName.replace(/\s/g, '_')
        text = text.replace(rawName, safeName)
        return text
    } else
        return text
}