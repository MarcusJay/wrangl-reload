import React, {Component} from 'react'
import {Dropdown} from "semantic-ui-react";
import MoreHoriz from "../common/MoreHoriz";

export default class AuthorActions extends Component {
    render() {
        const {open, editMessage, deleteMessage, cancelActions} = this.props

        return (
            <Dropdown
                icon={<span className='fg abs subtleShadow aActions hoh boxRadius'
                            style={{padding: '.1rem .3rem 0.2rem .3rem', top: '0', right: '0rem'}}>
                        <MoreHoriz size={24} className='jFG pointer' style={{verticalAlign: 'middle'}}/>
                    </span>
                    }
                className='abs'
                style={{right: '1rem'}}
                // open={open}
                // onOpen={this.onDropdownOpen.bind (this)}
                // onClose={this.onDropdownClose.bind (this)}
                pointing='top right'
            >
                <Dropdown.Menu className='hioh bg fg'>
                    {editMessage !== null &&
                    <Dropdown.Item name='open' onClick={editMessage} >
                        Edit Message...
                    </Dropdown.Item>
                    }
                    <Dropdown.Item as='div' name='open' onClick={deleteMessage} className='red'>
                        Delete Message...
                    </Dropdown.Item>
                    <Dropdown.Item as='div' name='open' onClick={cancelActions} >
                        Cancel
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
            )
    }
}