import React, {Component} from 'react'
import {Dropdown} from "semantic-ui-react";
import MoreHoriz from "../common/MoreHoriz";
import {Check, Smile} from "react-feather";
import {findDOMNode} from "react-dom";
import {Picker} from "emoji-mart";

export default class ReaderActions extends Component {
    state = {showEmojis: false}

    openEmojiPicker = (ev) => {
        if (!this.state.showEmojis)
            this.setState ({showEmojis: true})
    }

    closeEmojiPicker = (ev) => {
        this.setState ({showEmojis: false})
    }

    addEmoji = (emoji) => {
        if (this.props.onEmoji)
            this.props.onEmoji (emoji.native || emoji.unified)
        this.closeEmojiPicker ()
    }

    handlePickerClick = (event) => {
        try {
            let node = findDOMNode (this.refs.emojiPicker);
            if (!node.contains (event.target)) {
                this.setState ({showEmojis: false})
            }
        } catch (error) {
            return null
        }
    }

    render() {
        const {showEmojis} = this.state

        return (
            <Dropdown
                icon={
                    <span className='fg subtleShadow aActions inline hoh boxRadius'
                          data-tip="Add Emoji"
                          style={{padding: '.2rem .3rem 0.3rem .3rem', top: '0', right: '0rem'}}>
                    <Smile size={24} className='jFG pointer' style={{verticalAlign: 'middle'}}/>
                </span>
                }
                className='abs'
                style={{right: '1rem'}}
                pointing='top right'
            >
                <Dropdown.Menu className='hioh bg fg'>
                    <Dropdown.Item as='div' name='open' >
{/*
                        <Picker set='emojione'
                                ref='emojiPicker'
                                style={{position: 'absolute', bottom: '2rem', right: '26%', cursor: 'pointer'}}
                                onSelect={this.addEmoji}
                                emoji='thumbsup'
                                title='Pogomoji'
                        />
*/}
Reactions coming shortly
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        )

    }
}