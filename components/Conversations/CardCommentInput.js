import React, {Component} from 'react'
import {observer} from 'mobx-react'
import {findDOMNode} from 'react-dom'
import localforage from 'localforage'
import projectStore from "../../store/projectStore";
import userStore from "../../store/userStore";
import {voteTypeToId} from "../../services/ReactionApiService";
import {THUMBS_DOWN, THUMBS_UP} from "../../config/ReactionConfig";
import uiStateStore from "../../store/uiStateStore";
import {Picker} from "emoji-mart";
import {Smile} from "react-feather";

@observer
export default class CardCommentInput extends Component {

    state = {text: '', isDirty: false, saving: true, showEmojis: false}

    prevCardId = null

    cancelMsg = (ev) => {
        this.clearInput()
        if (this.props.onCancel)
            this.props.onCancel()
    }

    togglePaint = (ev) => {
        uiStateStore.paintEnable = !uiStateStore.paintEnable
    }

    handleChange = (ev) => {
        console.warn('CardComment change. ')
        const val = ev.target.value
        this.setState({text: val, isDirty: val && val.length > 0})
    }

    handleKeyDown = (ev) => {
        console.warn('CardComment key down')
        if (ev.key === 'Enter') {
            ev.stopPropagation()
            this.saveComment (ev)
        }
    }

    handleFocus = (ev) => {
        uiStateStore.arrowPriority = 'text'
    }
    handleBlur = (ev) => {
        uiStateStore.arrowPriority = 'nav'
    }

    clearInput() {
        const {text} = this.state
        const {card} = this.props

        const activeUser = userStore.currentUser

        if (text && text.length > 0) {
            try {
                localforage.removeItem (`draft-${activeUser.id}-${card.id}`).then (response => {
                    // success
                })
            } catch (error) {
                console.log ('Error removing draft', error)
            }
        }
        this.setState({text: '', isDirty: false})

    }

    // populate with unsent draft if any
    initInput(card) {
        const activeUser = userStore.currentUser
        if (!card || !activeUser)
            return this.clearInput()

        localforage.getItem(`draft-${activeUser.id}-${card.id}`).then( draft => {
            const initText = draft || ''
            this.setState ({text: initText, isDirty: draft !== null})
        })
    }

    saveDraft(card) {
        const {text} = this.state
        const activeUser = userStore.currentUser
        if (!text || !card || !activeUser)
            return

        try {
            localforage.setItem(`draft-${activeUser.id}-${card.id}`, text).then(response => {
                // success
            })
        } catch (error) {
            console.log('Error saving draft', error)
        }
    }

    saveComment = (ev) => {
        const {text, isDirty} = this.state
        if (!isDirty)
            return

        this.setState ({saving: true})
        uiStateStore.playSound ()
        const project = projectStore.currentProject
        const {card} = this.props

        projectStore.addToConvo(text, project.id, [card.id]).then( response => {
            this.clearInput()
            this.setState ({saving: false})
            if (this.props.onSubmit)
                this.props.onSubmit()
            // this.refreshMessage(response.msg.comment_id)
        })

    }

    openEmojiPicker = (ev) => { this.setState({showEmojis: true}) }
    closeEmojiPicker = (ev) => { this.setState({showEmojis: false}) }

    addEmoji = (emoji) => {
        if (this.props.onEmoji)
            this.props.onEmoji(emoji.native || emoji.unified)
        const {text} = this.state
        const delimiter = text.length > 0? ' ' :''
        this.setState({isDirty: true, text: text + delimiter +(emoji.native || emoji.unified)})
        this.closeEmojiPicker()
    }

    handlePickerClick = (event) => {
        try {
            let node = findDOMNode(this.refs.emojiPicker);
            if (!node.contains(event.target)) {
                this.setState({showEmojis: false})
            }
        } catch(error) {
            return null
        }
    }

    componentDidMount() {
        this.thumbsUpId = voteTypeToId(THUMBS_UP)
        this.thumbsDownId = voteTypeToId(THUMBS_DOWN)
        this.prevCardId = this.props.card? this.props.card.id : null

        const elem = findDOMNode(this)
        this.setState({elem: elem})

        const input = this.refs.input
        if (input) {
            this.setState({input: input})
        }
        document.addEventListener('mousedown', this.handlePickerClick)

    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handlePickerClick)
    }

    componentDidUpdate(prevProps) {
        const oldCard = prevProps.card
        const {card} = this.props
        if (!oldCard || !card || oldCard.id !== card.id) {
            // this.prevCardId = card? card.id : null
            this.saveDraft(oldCard)
            this.initInput(card)
        }
    }

    render() {
        const {text, isDirty, showEmojis} = this.state
        const {isAnon, mobile, card, onChange, value} = this.props

        const project = projectStore.currentProject
        // const isApproval = project && project.projectType === ProjectType.APPROVAL

        let approved, rejected, rating

        return (
            <React.Fragment>
                <input className='commentInput dontWrap' type='text'
                       style={{flexBasis: '88%', marginRight: '1.5rem', lineHeight: '1.25rem !important'}} /*semantic*/
                       placeholder={'Comment on this item...'}
                       onChange={this.handleChange}
                       onKeyDown={this.handleKeyDown}
                       onFocus={this.handleFocus}
                       onBlur={this.handleBlur}
                       value={text}
                />

                <span onClick={this.openEmojiPicker}
                      className='inline round vaTop centered pointer mr1 '
                      data-tip="Add Emojis"
                      style={{marginLeft: '-3.75rem', width: '28px', height: '28px', paddingTop: '3px'}}>
                            <Smile size={26} className='jFGinv'/>
                        </span>

                {showEmojis &&
                <Picker set='emojione'
                        ref='emojiPicker'
                        style={{position: 'absolute', bottom: '2rem', right: '16%', cursor: 'pointer'}}
                        onSelect={this.addEmoji}
                    // onClick={this.handlePickerClick}
                    // onKeyDown={this.handlePickerKey}
                        emoji='thumbsup'
                        title='Pogomoji'
                />
                }


                <button className={'jBtn mr05'}
                        style={{flexBasis: '7%', color: isDirty? '#d8d8d8':'#999'}}
                        disabled={!isDirty}
                        onClick={this.cancelMsg}
                >
                    Reset
                </button>

                <button className={'jBtn mr05'}
                        disabled={!isDirty}
                        style={{flexBasis: '7%', color: isDirty? '#d8d8d8':'#999'}}
                        onClick={this.saveComment}
                >
                    Save
                </button>

            </React.Fragment>
        )
    }
}


