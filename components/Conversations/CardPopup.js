import {Component} from 'react'
import {observer} from 'mobx-react'
import {findDOMNode} from 'react-dom'
import projectStore from "../../store/projectStore";
import cardStore from '../../store/cardStore'
import {Mail, PlusCircle, X} from "react-feather";
import {ProjectType} from "../../config/Constants";
import AtCard from "./AtCard";

const MSG_CHOOSE_USER = "Click the person you'd like to tag..."

@observer
export default class CardPopup extends Component {

    state = {
        displayCard: null,

        showInstructions: true,
        showEmailModal: false,

        searchResults: this.props.allCards || (cardStore.currentCards? cardStore.currentCards : []),
        query: null,

        currentInviteeEmail: null,
        currentInviteeName: null,
        allInvitees: []
    }

    close() {
        this.props.close()
    }

    selCard = (cardId,i) => {
        this.props.selCard(cardId)
        this.close()
    }

    overCard = (card,i) => {
        this.setState({displayCard: card, showInstructions: false})
    }

    clearCard = (u,i) => {
        this.setState({displayCard: null})
        this.props.clearCard(u)
    }

    handleSearchChange = (ev) => {
        const query = ev.target.value
        if (!query || query.length === 0) {
            this.setState ({query: null, searchResults: this.props.allCards})
            return
        }

        const matches = this.props.allCards.filter( card => {   // using allCards supports backspaces as well
            return (card.title && card.title.startsWith (query))
        })
        this.setState({query: query, searchResults: matches})
    }

    viewCard = (card) => {
        // meh
    }

    componentDidMount() {
        const elem = findDOMNode(this)
        this.setState({elem: elem})
        if (elem) {
            elem.focus ()
        }
    }

    componentDidUpdate() {
        const elem = this.state.elem
        if (elem) {
            const selRow = elem.querySelector('.active')
            if (selRow)
                selRow.scrollIntoView ({behavior: 'smooth', block: "start", inline: "nearest"})
        }
    }

    componentWillUnmount() {

    }

    render() {
        const {activeCard, hideActiveCard, selectedCard, selectedIndex, allCards, bottom} = this.props
        const {displayCard, showInstructions, showEmailModal, query, searchResults, currentInviteeEmail, currentInviteeName, allInvitees} = this.state

        const width = '93%' // uiStateStore.currentApprovalRequest? '33%':'90%'
        const cards = searchResults
        if (!cards)
            return null

        const project = projectStore.currentProject
        const isApproval = project && project.projectType === ProjectType.APPROVAL

        return (
            <div className='userPopup pad1 subtle-scroll-dark' style={{maxHeight: '75%', bottom: bottom, width: width}}>

                {false && cards && cards.length > 0 &&
                <div className='cardSearch mb1'>
                    <input type='text' className='formPane' value={query} onChange={this.handleSearchChange} placeholder='Search Cards...'/>
                </div>
                }
                <X size={24} onClick={() => this.close()} className='close third abs hoh pointer' style={{top: '1.2rem', right: '1.2rem'}}/>

                <div className='userList subtle-scroll-dark' >

                {cards.map( (card,i) => {
                    return <AtCard card={card}
                                   key={'cpop'+i}
                                   showAttmMenu={false}
                                   removeAttm={null}
                                   viewCard={this.selCard}
                                   showVoting={false}
                                   approved={false}
                                   rejected={false}
                                   rating={0}
                    />

                })}
                </div>
            </div>
        )
    }
}