import React from 'react'
import {observer} from 'mobx-react'
import {X} from "react-feather";
import {abbrevOrTruncate} from "../../utils/solo/abbrevOrTruncate";
import {sourceToIcon} from "../../config/FileConfig";
import CardReactions from "../common/Card/CardReactions";
import {Popup} from "semantic-ui-react";
import {voteTypeIdToAPName} from "../../services/ReactionApiService";
import userStore from '../../store/userStore'
import {capitalize} from "../../utils/solo/capitalize";

const DefaultSize = 3 // TODO remove from css once safe

@observer
export default class AtCard extends React.Component {

    removeAttm = (ev) => {
        ev.preventDefault()
        ev.stopPropagation()
        if (this.props.removeAttm)
            this.props.removeAttm(this.props.card.id, 'card')
    }

    viewCard = (ev) => {
        const {card} = this.props
        this.props.viewCard(card.id || card.card_id)
    }

    render() {
        const {card, showReactions, miniReactions, showAttmMenu,
            removeAttm, showVoting, showTitle, selected, approved, rejected, rating, sizeRems, style} = this.props
        if (!card)
            return null

        // be able to support both CardModel (from a drop) or an api card (from a read)
        const image = card.image || card.card_image
        const icon = card.source? sourceToIcon(card.source) : null
        const id = card.id || card.card_id
        const title = abbrevOrTruncate(card.title || card.card_title, image? 4 : 3)
        const size = (sizeRems? sizeRems : DefaultSize) + 'rem'
        let coreStyle = {backgroundImage: 'url(' + image + ')', width: size, height: size}
        const combinedStyle = style? Object.assign(coreStyle, style) : coreStyle

        const atContents = image?
            <div className={'imgCardRef pointer inline relative' +(selected? ' sel':'')}
                        data-tip={card.title}
                        style={combinedStyle}
                        onClick={this.viewCard}>
                {showAttmMenu &&
                <X size={16} className='abs fg round hidden pointer attmAction centered'
                   style={{top: '-5px',right:'-5px'}}
                   data-tip='Remove'
                   onClick={this.removeAttm}
                />
                }&nbsp;

                {showTitle &&
                <span className='secondary tiny abs' style={{bottom: '-1.2rem', left: '1px'}}>{title}</span>
                }

                {showReactions &&
                <span className='abs ' style={{bottom: '-1rem', left: '-2px', width: '105%', backgroundColor: '#20222Bb0'}}>
                    <CardReactions card={card} hideZeros={true} mini={miniReactions}/>
                </span>
                }
            </div>

        :
            <div className={'imgCardRef inline relative' +(selected? ' sel':'')}
                        onClick={this.viewCard}>
                {icon &&
                <img src={icon} className='abs source' />
                }

                {icon &&
                <span className='subtitle abs dontWrap'>{title}</span>
                }

                {!icon &&
                <span className='title inline dontWrap'>{title}</span>
                }

                {showAttmMenu &&
                <X size={16} className='abs fg bg round pointer attmAction'
                   style={{top: '-5px',right:'-5px'}}
                   onClick={this.removeAttm}
                />
                }
            </div>

        let user, action
        const popup = showReactions?
            <Popup
                inverted
                trigger={atContents}
                position='bottom left'
                >
                {card.reactions && card.reactions.map( reaction => {
                    user = userStore.getUserFromConnections(reaction.user_id)
                    action = voteTypeIdToAPName(reaction.reaction_type_id)
                    return <div className='fg small lighter'>
                        {capitalize(action)} by {user? (user.displayName || user.email || user.firstName) : user}.
                    </div>
                })}
            </Popup>
        : null

        if (showReactions && card.reactions && card.reactions.length > 0)
            return popup
        else
            return atContents
    }
}