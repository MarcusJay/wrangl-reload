import {Component} from 'react'
import {DropTarget} from 'react-dnd'
import RMoment from 'react-moment'
import cardStore from "../../store/cardStore";
import {User} from "react-feather";
import AuthorActions from "./AuthorActions";
import ReaderActions from "./ReaderActions";
import {isUrl} from "../../utils/solo/isUrl";
import {CmtType, DndItemTypes, myFG} from "../../config/Constants";
import projectStore from "../../store/projectStore";
import MemberAvatar from "../common/MemberAvatar";
import AtCard from "./AtCard";
import {markTaggedUsersInMsg} from '/utils/solo/markTaggedUsersInMsg'
import {isUser} from "../../utils/solo/isUser";
import MemberAvatarPopup from "../common/MemberAvatarPopup";
import {getUserByNameFromCollection} from "../../utils/solo/getUserByNameFromCollection";
import ReactionApiService from "../../services/ReactionApiService";
import isAutoMsg from "../../utils/solo/isAutoMsg";
import {Divider, Icon} from "semantic-ui-react";
import uiStateStore from "../../store/uiStateStore";

const MAX_ELAPSED = 60 * 1000

const messageTarget = {
    drop(props, targetMonitor, component) {
        let dragItem = targetMonitor.getItem ()

        // add card reference to message
        if (dragItem.type === DndItemTypes.CARD) {
            props.onCardDrop (dragItem.id)
        }

        // add person reference to message
        else if (dragItem.type === DndItemTypes.PERSON) {
            props.onUserDrop (dragItem.id)
        }

    }
}


// A single message in a Conversation.
@DropTarget ([DndItemTypes.CARD, DndItemTypes.PERSON], messageTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget (),
    isOver: monitor.isOver (),
    canDrop: monitor.canDrop (),
    item: monitor.getItem ()
}))
export default class ConvoMessage extends Component {

    state = {showAuthorActions: false}

    unattachUser = (msgId) => {
        this.props.onUserRemove (msgId)
    }

    unattachCard = (cardId) => {
        this.props.onCardRemove (cardId)
    }

    editMessage = () => {
        if (this.props.editMsg)
            this.props.editMsg (this.props.msg)
        this.closeActions ()
    }

    deleteMessage = () => {
        if (this.props.deleteMsg)
            this.props.deleteMsg (this.props.msg)
        this.closeActions ()
    }

    handleConfirm = (ev) => {

    }

    closeActions = (ev) => {
        this.setState ({showAuthorActions: false})
    }

    viewCard(cardId) {
        this.props.viewCard (cardId)
    }

    render() {
        const {
            isOver, isEditing, connectDropTarget, msg, author, activeUser, stillTalking,
            viewUser, onCardDrop, onUserDrop, isUnread, showUnreadMarker
        } = this.props

        let msgMeta = null, msgBody = null, actions = null, atCards = null, atUsers = null, unreadMarker = null
        const isMe = activeUser && author && author.id === activeUser.id

        const isAuto = isAutoMsg (author, msg.comment_text)
        let bubbleStyle
        if (isAuto)
            bubbleStyle = {backgroundColor: 'transparent', color: myFG}
        else
            bubbleStyle = {} //{backgroundColor: isMe? (isOver? myBGDrop : myBG) : (isOver? contactBGDrop : contactBG),
        // color: isMe? myFG : contactFG}


        let approved, rejected, rating

        // Actions for message author [or reader?]
        if (isMe) {
            actions = this.props.editMsg? // hold off any other message actions while one message is being edited
                <AuthorActions editMessage={this.props.editMsg? this.editMessage : null}
                             deleteMessage={this.deleteMessage}
                             cancelActions={this.closeActions}
                             open={this.state.showAuthorActions}
                /> : null

        } else {
            actions = null // <ReaderActions onConfirm={this.handleConfirm}/>
        }

        // Optional unread messages timestamp line
        if (showUnreadMarker) {
            unreadMarker =
                <Divider horizontal>
                    <RMoment className='tiny jSec' subtract={{hours: 7}}
                             format={'MMM Do, h:mm a'}>{msg.time_created}</RMoment>
                </Divider>
        }


        // Metadata that marks beginning of new message
        if (!stillTalking) {
            msgMeta =
                <div className='msgMeta'>

                    {/* 1. Avatar */}
                    {msg.user && msg.user.image &&
                    <div className='userImg' style={{backgroundImage: 'url(' + msg.user.image + ')'}}>&nbsp;</div>
                    }
                    {(!msg.user || !msg.user.image) &&
                    <User size={24}/>
                    }

                    {/* Author name */}
                    {author &&
                    <div className='secondary small' style={{marginBottom: '-4px'}}>{author.displayName}</div>
                    }

                    {/* Timestamp */}
                    <RMoment subtract={{hours: 7}} format={'MMM Do, h:mm a'}
                             className="third tiny lighter">{msg.time_created}</RMoment>
                </div>
        }

        // Card attachments
        if (msg.at_cards && msg.at_cards.length > 0) { // && !uiStateStore.filterDiscussionByCard) {
            let card
            atCards = (
                <span className='cardAttms'>
                    {msg.at_cards.map ((card, i) => {
                        card = cardStore.getCardFromCurrent (card.card_id)
                        if (!card || !card.image)
                            return null
                        /*
                                                        <div className='imgCardRef micro' style={{marginBottom: '0', paddingTop: '.25rem'}}>
                                                        Item deleted
                                                    </div>
                        */

                        approved = ReactionApiService.hasUserReacted (card, this.thumbsUpId, author)
                        if (!approved)
                            rejected = ReactionApiService.hasUserReacted (card, this.thumbsDownId, author)
                        rating = ReactionApiService.getLastStarRating (card, author)

                        return <AtCard card={card}
                                       sizeRems={2}
                                       style={{border: '2px solid white', margin: '.5rem 1rem .25rem 0'}}
                                       key={'cattm' + i}
                                       viewCard={() => this.viewCard (card.id)}

                        />
                    })}
                </span>
            )
        }

        // User attachments. TODO Will these 2 be similar enough to combine here? Probably not.
        if (msg.at_users && msg.at_users.length > 0) {
            atUsers = (
                <span className='userAttms'>
                    {msg.at_users.map ((user, i) => {
                        if (!user || !user.id)
                            return null

                        return (
                            <div className='inline' onClick={() => viewUser (user.id)}>
                                <MemberAvatar member={user}
                                              activeUser={activeUser}
                                              enableAdmin={false}
                                              inMsgId={msg.comment_id}
                                              project={projectStore.currentProject}
                                              key={'uattm' + i}
                                              index={i}
                                              color="black"
                                              showName={false}
                                              isEnabled={true}
                                              showMenu={true}
                                              showBorder={true}
                                              style={{marginRight: '.25rem', marginBottom: 0}}
                                              size={'2rem'}
                                              unattachUser={() => this.unattachUser (msg.comment_id)}
                                />

                                {/*
                                <span className='round' style={{backgroundImage: 'url('+user.image+')'}}>
                                </span>
                                <span className='userRef tiny pointer dontWrap'>
                                    @{user.name}
                                </span>
*/}
                            </div>
                        )
                    })}
                </span>
            )
        }

        // Message body, everything in the bubble
        msg.comment_text = markTaggedUsersInMsg (msg.comment_text, msg.at_users)
        const lines = msg.comment_text ? msg.comment_text.split (/\n/) : []
        // const lineWords = lines.map (line => line.split (/[\s\t]/))

        let userName, user, words

        msgBody = (
            <div className={'msgOuter' + (isEditing ? ' editing' : '') + (isUnread ? ' unread' : '')}
                 style={bubbleStyle}>
                {!isEditing && (atCards || atUsers) &&
                <div className='allAttms'>
                    {!isEditing && atCards}
                    {msg.comment_type === CmtType.MARKUP &&
                    <Icon name='paint brush'
                          className='abs'
                          style={{
                              fontSize: '18px',
                              paddingRight: '1rem',
                              margin: '-0.75rem 0 0 -.75rem',
                              color: '#47C1F1'
                          }} // jBlue
                    />
                    }
                    {!isEditing && atUsers}

                </div>
                }
                <span className='msgInner '>
                    {isEditing &&
                    <span className='italic'>Editing below...</span>
                    }

                    {!isEditing && msg.comment_type !== CmtType.MARKUP && lines.map(line => {
                        words = line.split (/[\s\t]/)
                        return (
                            <p>
                                {words.map( (word,i) => {
                                    if (isUser(word)) {
                                        userName = word.replace(/_/g,' ').replace('@','')
                                        user = getUserByNameFromCollection(userName, msg.at_users)

                                        // NOTE This is a @username inside a Popup, not an Avatar.
                                        return (
                                            <MemberAvatarPopup
                                                trigger={<span className='atUser inline pointer'>{userName}</span>}
                                                member={user} activeUser={activeUser}
                                                inMsgId={msg.comment_id}
                                                enableAdmin={false} /*TODO*/
                                            />
                                        )
                                    }
                                    else if (isUrl (word))
                                        return <span><a href={word} target='_blank'>{word}</a> </span>
                                    else
                                        return word + (i < words.length - 1 ? ' ' : '')                                })}
                            </p>
                        )
                    })}
                </span>


                {/*
                {!isEditing && (atCards || atUsers) &&
                <span className='allAttms'>
                    {!isEditing && atCards}
                    {!isEditing && atUsers}
                </span>
                }
*/}

            </div>
        )

        return connectDropTarget (
            <div className='msgRow' style={{marginTop: '.5rem'}}>
                {unreadMarker}
                {msgMeta}

                {msgBody}
                {actions}

            </div>
        )
    }
}

