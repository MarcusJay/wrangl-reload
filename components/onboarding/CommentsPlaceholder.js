import {Component} from 'react'
import {Comment} from 'semantic-ui-react'

export default class CommentsPlaceholder extends Component {
    render() {
        const comments = [1,2,3,4,5,6,7]
        return(
            <Comment.Group className='abs' style={{bottom: '3rem'}}>
                {comments.map( (comment,i) =>
                    <Comment key={'cgpl'+i}>
                        <span className='placeholder cmtPH' style={{width: '2rem', lineHeight: '2rem'}}>&nbsp;</span>
                        <Comment.Content style={{display: 'inline', verticalAlign: 'text-top'}}>
                            <span className='placeholder textPH' style={{width: '30%', lineHeight: '1rem'}}>&nbsp;</span>

{/*
                            <Comment.Metadata>
                                <span className='placeholder' style={{width: '20%', lineHeight: '1rem'}}>&nbsp;</span>
                            </Comment.Metadata>
*/}
                            <Comment.Text className='cText'>
                                <span className='placeholder textPH' style={{width: '90%'}}>&nbsp;</span>
                            </Comment.Text>
                        </Comment.Content>
                    </Comment>
                )}
            </Comment.Group>
        )
    }
}