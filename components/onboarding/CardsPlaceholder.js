import {Component} from 'react'
import {Icon} from 'semantic-ui-react'
import {File, Image, Search} from 'react-feather'
import ImportModal, {TAB_DROPBOX, TAB_SEARCH, TAB_UPLOAD} from "../modals/ImportModal";
import CardPlaceholder from "./CardPlaceholder";


export default class CardsPlaceholder extends Component {

    importCards(cards) {
        this.props.importCards(cards)
    }

    importAssets(assets, source) {
        // this.props.importAssets(assets, source)
    }

    uploadAssets(cards, files) {
        // this.props.uploadAssets(cards, files)
    }

    render() {
        const count = this.props.count
        const cards = []
        for (let i=0; i<count; i++)
            cards.push( 'placeholder ')

        // const titles = ['Images', 'Documents', 'Dropbox Files', 'Web Search Results', 'Links', 'Attachments']

        return(
            <div>
                <ImportModal project={this.props.project} importCards={this.importCards}
                             importAssets={this.importAssets} uploadAssets={this.uploadAssets}
                             tab={TAB_UPLOAD}
                             trigger={
                                 <CardPlaceholder title='Images' tab={TAB_SEARCH} icon={<Image size={36}/>}/>
                             }
                />
                <ImportModal project={this.props.project} importCards={this.importCards}
                             importAssets={this.importAssets} uploadAssets={this.uploadAssets}
                             tab={TAB_UPLOAD}
                             trigger={
                                 <CardPlaceholder title='Documents' tab={TAB_SEARCH} icon={<File size={36}/>}/>
                             }
                />
                <ImportModal project={this.props.project} importCards={this.importCards}
                             importAssets={this.importAssets} uploadAssets={this.uploadAssets}
                             tab={TAB_SEARCH} query='random'
                             trigger={
                                 <CardPlaceholder title='Search' tab={TAB_SEARCH} icon={<Search size={36}/>}/>
                             }
                />
                <ImportModal project={this.props.project} importCards={this.importCards}
                             importAssets={this.importAssets} uploadAssets={this.uploadAssets}
                             tab={TAB_DROPBOX}
                             trigger={
                                 <CardPlaceholder title='Dropbox' tab={TAB_SEARCH} icon={<Icon name='dropbox' size='huge'/>}/>
                             }
                />


            </div>
        )
    }
}