import {Component} from 'react'
import {observer} from 'mobx-react'
import {Link, Router} from '~/routes'
import {randomIntBetween} from "../../utils/solo/randomlntBetween";

import projectStore from '/store/projectStore'
import userStore from '/store/userStore'

const brakePoints = [250,350,450];

const COUNT = 35
const titles = ["Oh Hello!", "I'm a Project", "Aka a Smart Folder", "I Contain Cards", "Cards Can Be Anything", "Event Planning", "Prop Houses", "Onboarding Docs", "Head Shots", "Set Designs", "Team Links", "Private Chats", "Public Boards" ]


@observer
class MasonryPlaceholder extends Component{
    constructor(props) {
        super(props)
        let projects = []
        for (let i=0; i<COUNT; i++)
            projects.push( {height: randomIntBetween(100,500), width: randomIntBetween(10, 99)} )

        this.state = {projects: projects, brakePoints: brakePoints}

    }

    createBoard = (ev) => {
        projectStore.createProject().then( project => {
            Router.pushRoute('/project/'+project.id, {id: project.id})
        })
    }

    render() {
        const {loading} = this.props

        if (!loading)
            return null

        return (
            <div className="container">
                <div className="masonry-container">
                    <Masonry brakePoints={this.state.brakePoints} >
                        {this.state.projects.map((project, idx) => {
                            return (
                                <ProjectTile
                                    height={project.height}
                                    width={project.width}
                                    i={idx}
                                    key={'k'+idx}/>
                            )
                        })}
                    </Masonry>
                </div>
            </div>
        )
    }
}

class ProjectTile extends Component {

    render() {

        const imgBoxClass = 'imgBox placeholder'
        const idx = this.props.i % titles.length
        const r = randomIntBetween(220, 240)
        const g = randomIntBetween(150, 200)
        const b = randomIntBetween(200, 250)
        const color = 'rgba('+255+','+(b-20)+','+b+', 0.75)'

        const sat = randomIntBetween(50, 90)
        const hsl = 'hsla(33, ' +sat+ '%, 61%, 0.75)'

        return (
            <div className='tile placeholder tilePH' style={{height: this.props.height}}>
                <div className={imgBoxClass}  style={{width: this.props.width+'%', height: this.props.height}}>
                </div>
                {/*<h1>{titles[idx]}</h1>*/}
            </div>
        )
    }
}

class Masonry extends Component{
    state = {columns: 3}

    getColumns(width){
        const initialValue = this.props.brakePoints.length

        const result = this.props.brakePoints.reduceRight( function(numBrakePoints, brakePoint, idx) {
            return brakePoint < width ? numBrakePoints : idx;
        }, initialValue); // yo
        console.log('getColumns. result: ' +result)
        return result
    }

    onResize(){
        const columns = this.getColumns(this.refs.Masonry.offsetWidth);
        if(columns !== this.state.columns){
            this.setState({columns: columns});
        }

    }

    distributeTilesToColumns(){
        let columns = []; // yo
        const numC = this.state.columns;
        for(let i = 0; i < numC; i++){
            columns.push([]); // array of empty arrays
        }
        const tiles = this.props.children

        const result = tiles.reduce( function(columns, tile, idx) {
            columns[ idx % numC ].push(tile);
            return columns;
        }, columns);

        return result // yes
    }

    componentWillReact() {
        console.log("Masonry will react.")
    }

    render(){
        const columns = this.distributeTilesToColumns()

        return (
            <div className="masonry" ref="Masonry">
                {columns.map( (column, columnIdx) => {
                    return (
                        <div className="column" key={'col'+columnIdx} >
                            {column.map( (child, idx) => {
                                return (
                                    <div key={'cc'+idx}>
                                        {child}
                                    </div>
                                )
                            })}
                        </div>
                    )
                })}
            </div>
        )
    }
}


export { MasonryPlaceholder }