import {Component} from 'react'
import {X} from 'react-feather'
import {observer} from 'mobx-react'

import USU from '/utils/UserSessionUtils'
import uiStateStore from "/store/uiStateStore";

@observer
export default class InlineHint extends Component {
    closeHint() {
        uiStateStore[this.props.name] = false
        USU.setSessionPref(this.props.cookie, false)
    }

    endAllHints() {
        uiStateStore.showInlineHints = false
        USU.setSessionPref('showInlineHints', false)
    }

    render() {
        const body = this.props.body // or Messages.js, but that would require importing entire map?
        const title = this.props.title || null
        const extraClass = this.props.place || ''
        const showEndAll = this.props.place !== 'side'

        return (

            <div className={'inlineHint ' + extraClass}>
                {title &&
                <div className='title'>{title}</div>
                }
                {body &&
                <div className='body'>{body}</div>
                }
                <div className='actions'>
                    {showEndAll &&
                    <span onClick={() => this.endAllHints ()}>(End All Hints)</span>
                    }
                    <X size={18} onClick={() => this.closeHint()}>X</X>
                </div>
            </div>
        )
    }
}