import {Component} from 'react'
import {PlusSquare} from 'react-feather'

export default class CardsPlaceholder extends Component {

    render() {
        const title = this.props.title
        const icon = this.props.icon || <PlusSquare />
        const tab = this.props.tab

        return (
            <div className='placeholder cardPH LeCard' >
                <div className='top'>
{/*
                    <h1>{title}</h1>
                    <div>
                        {icon}
                    </div>
*/}
                </div>
                <div className='bottom'>
                    <span className='placeholder textPH'
                          style={{width: '90%', lineHeight: '0.9rem', margin: '.5rem 0 0 .5rem'}}>&nbsp;</span>
                    <span className='placeholder textPH'
                          style={{width: '70%', lineHeight: '0.9rem', margin: '.5rem 0 0 .5rem'}}>&nbsp;</span>
                    <span className='placeholder textPH'
                          style={{width: '50%', lineHeight: '0.9rem', margin: '.5rem 0 0 .5rem'}}>&nbsp;</span>
                </div>
            </div>
        )
    }
}
