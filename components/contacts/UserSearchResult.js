import MemberAvatar from "../common/MemberAvatar";
import React from "react";
import MemberAvatarPopup from "../common/MemberAvatarPopup";

const UserSearchResult = (props) => {
    const {user, activeUser, isConnected, contactAdded, sessionId, onSelect} = props

    const selectUser = (ev) => {
        if (props.user)
        onSelect(props.user)
    }

    const userRow =
        <div className='fullW dontWrap pad1 hbgoh pointer'>
            <span className='inline round coverBG mr05 vaTop'
                  style={{backgroundImage: 'url(' + (user.image) + ')', width: '1.5rem', height: '1.5rem'}}>&nbsp;
            </span>

            <span className='inline jFG hoh dontWrap vaTop tiny' style={{marginTop: '0'}}>
                {user.displayName || user.display_name}
            </span>

        </div>

    if (true) // isConnected)
        return (
            <MemberAvatarPopup
                trigger={userRow}
                sessionId={sessionId}
                member={user}
                activeUser={activeUser}
                contactAdded={contactAdded}
                enableAdmin={true}
            />
        )
    // else
    //     return userRow
}

export default UserSearchResult