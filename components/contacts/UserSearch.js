import React, {Component} from 'react'
import {Form, Icon, Input, Loader} from "semantic-ui-react";
import ProjectModel from "../../models/ProjectModel";
import {SearchConfig, UserRole} from "../../config/Constants";
import projectStore from "../../store/projectStore";
import userStore from "../../store/userStore";
import UserApiService from "../../services/UserApiService";
import UserSearchResult from "./UserSearchResult";
import {X} from "react-feather";
import {EventSource} from "../../config/EventConfig";
import ProjectApiService from "../../services/ProjectApiService";

export default class extends React.Component {

    state = {query: null, isVirgin: true, searching: false,
             results: [], showResults: false, showError: false,
             sessionId: null, selectedUser: null, selectedIndex: -1}

    onSearchChange = (ev) => {
        let query = ev.target.value
        console.log('change: query = ' +query)
        let now = new Date()
        let elapsed = this.state.lastKeyPress === 0? 0 : now - this.state.lastKeyPress
        this.setState({query: ev.target.value, lastKeyPress: now})

        setTimeout( () => {
            if (query.length >= SearchConfig.MIN_SEARCH_CHARS) {
                this.submitSearch (query)
            } else if (!this.state.isVirgin) {
                console.log ('Clearing search')
                this.setState ({results: []})
            } else {
                console.log('Skipping search ' + query + ' at ' +elapsed+ 'ms')
            }
            this.setState ({waiting: false})

        }, SearchConfig.QUERY_DELAY_MS )

    }

    submitSearch(query) {
        console.log("Searching for " +query+ "...")
        const {sessionId} = this.state
        query = query.toLowerCase()

        this.setState ({searching: true, isVirgin: false})

        // Note: api only returns users that are not connected.
        // So we must combine api search with our own userStore.currentConnections results
        const connectedUsers = userStore.currentConnections.filter (u =>
            u.displayName? u.displayName.toLowerCase().startsWith(query) :
                u.firstName? u.firstName.toLowerCase().startsWith(query) :
                    u.lastName? u.lastName.toLowerCase().startsWith(query) : false) || []

        UserApiService.findUsers(query).then (response => {
            if (!response.data)
                throw new Error ('user search api failed')

            const unconnectedUsers = response.data.found_users || []
            this.setState ({
                searching: false,
                sessionId: response.data.session_id,
                results: connectedUsers.concat(unconnectedUsers),
                showResults: query && query.length >= SearchConfig.MIN_SEARCH_CHARS
            })
        }).catch (error => {
            if (error && error.response && error.response.status === 403) {
                if (sessionId) {
                    UserApiService.endFindSession (sessionId).then (response => {
                        if (response.data.success === 'ok') {
                            this.submitSearch (query)
                        }
                    })
                } else {
                    this.setState ({showError: true, searching: false})
                }
            } else {
                console.error ("Error searching for " + query, error)
                this.setState ({searching: false})
                throw new Error (error)
            }
        })
    }

    closeResults = (ev) => {
        this.setState({showResults: false})
    }

    closeError = (ev) => {
        this.setState({showError: false})
    }

    contactAdded = (ev) => {
        this.setState({showResults: false})
        userStore.getUserConnections()
    }

    render() {
        const {placeholder = 'Search for people...'} = this.props
        const {query, results, showResults, showError, sessionId, searching} = this.state
        const activeUser = userStore.currentUser
        return (
            <div className='fullW relative' style={{padding: '0 .5rem'}}>
                <Loader active={searching}>Searching</Loader>

                {/*Search input*/}
                <Form inverted className="thumbSearch mt05" style={{width: 'calc(100% - 2rem) !important'}}>
                    <Input icon={<Icon name="search" />}
                           name='Search'
                           data-tip='Search for people'
                           className="sidePaneInput small lighter"
                           style={{paddingRight: '.5rem'}}
                           onChange={this.onSearchChange}
                           value={query}
                           id="userSearch"
                           placeholder={placeholder}
                    />
                </Form>


                {showResults && query && query.length >= SearchConfig.MIN_SEARCH_CHARS &&
                <div className='abs popup' style={{top: '2.1rem', width: '90%', minHeight: '100px', maxHeight: '500px', overflow: 'auto' }}>
                    <X size={18} onClick={this.closeResults} className='close third abs hoh pointer' style={{top: '1rem', right: '1rem'}}/>
                    <div className='jSec tiny italic pad1'>Found {results? results.length : 0} result{results.length === 1?'':'s'}</div>
                    {results.map( (user, i) => {
                        return (
                            <UserSearchResult user={user}
                                              activeUser={activeUser}
                                              isConnected={!!user.id}
                                              contactAdded={this.contactAdded}
                                              sessionId={sessionId} onSelect={this.onSelectUser}/>
                        )
                    })}
                </div>
                }

                {!searching && showError &&
                    <div className='abs popup pad1' style={{top: 0, width: '86%', minHeight: '200px', paddingTop: '48px'}}>
                        <X size={18} onClick={this.closeError} className='close third abs hoh pointer' style={{top: '1.2rem', right: '1.2rem'}}/>
                        Sorry, you already have an open search session. Please close it first.
                    </div>
                }

            </div>
        )
    }
}