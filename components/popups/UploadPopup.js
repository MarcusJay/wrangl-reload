import {Component} from 'react'
import {Popup} from 'semantic-ui-react'
import projectStore from '/store/projectStore'
// import FileService from '/services/FileService'
import UploaderComponent from '/components/common/UploaderComponent'

export default class UploadTab extends Component {
    state = {showPopup: false}

    onPopupOpen(ev) {
        this.setState({showPopup: true})
    }

    onPopupClose(ev) {
        this.setState({showPopup: false})
    }

    render() {
        const innerTrig = this.props.trigger || <Camera size={24} />
        const trig = <span className={this.props.className}>{innerTrig}</span>

        return (
            <Popup style={{padding: 0}}
                   inverted
                   trigger={trig}
                   open={this.state.showPopup}
                   onOpen={(ev) => this.onPopupOpen(ev)}
                   onClose={(ev) => this.onPopupClose(ev)}
                   on='click'
                   position='bottom center'
            >
                <UploaderComponent project={projectStore.currentProject} card={this.props.card} uploadAssets={this.uploadAssets}/>
            </Popup>
        )
    }
}