import {Component} from 'react'
import {observer} from 'mobx-react'
import {Dimmer, Grid} from 'semantic-ui-react'
import PDWToolbar from './WorkPane/PDWToolbar'
import PDWCardsPane from './WorkPane/PDWCardsPane'
import PDWStatusbar from './WorkPane/PDWStatusbar'
import PDWTagToolbar from "./WorkPane/PDWTagToolbar";
import UnmodalContainer from '/components/unmodals/UnmodalContainer'

import TagApiService from '/services/TagApiService'
import ReactionApiService from '/services/ReactionApiService'

import CardModel from "/models/CardModel";
import cardStore from '/store/cardStore'
import projectStore from '/store/projectStore'
import tagStore from '/store/tagStore'
import uiStateStore from '/store/uiStateStore'
// import PDWProjectComments from "./WorkPane/PDWProjectComments";
import PDWDiscussToolbar from "./WorkPane/PDWDiscussToolbar";
import Conversations from "../Conversations/Conversations";
import RightUnmodalContainer from "../unmodals/RightUnmodalContainer";
import APToolbar from "../Approvals/APToolbar";
import {SendForApproval} from "../../store/uiStateStore";
import CardViewerPane from "../unmodals/CardViewerPane";
import CardEditorPane from "../unmodals/CardEditorPane";
import userStore from "../../store/userStore";
import PGMobileSidebar from "../common/PGMobileSidebar";
import APWorkFeed from "../Approvals/APWorkFeed";
import {findCards} from "../../services/CardApiService";
import {ProjectType} from "../../config/Constants";
// import {DropTarget} from 'react-dnd'

const MIN_DISCUSSION_WIDTH = 994


/*
const paneTarget = {
    drop(props, targetMonitor, component) {
        let dragItem = targetMonitor.getItem()
        const project = projectStore.currentProject

        // add user
        if (dragItem.type === DndItemTypes.PERSON) {
            let newMemberId = dragItem.id

            const existingUser = project.members.find(member => member.id === newMemberId )
            if (existingUser !== undefined) {
                let msg = getAddUserMsg( existingUser.role )
                Toaster.info(msg, {user: existingUser, project: project}, 1)
                return
            }

            // This pattern works, but would require refreshing entire workpane to update. No return values from props functions.
            // props.addUserToProject(props.project.id, userId)
            // end

            StateChangeRequestor.awaitStateChange(REQUEST_PROJECT_ADD_USER,
                {projectId: project.id, userId: userStore.currentUser.id, newMemberId: newMemberId, trigger: EventSource.InApp, newMemberRole: UserRole.INVITED}).then (response => {
                const newMember = userStore.getUserFromConnections(newMemberId)
                addMember( newMember, project)
                // let msg = getAddUserMsg()
                // Toaster.info(msg, {user: newMember, project: props.project}, 1)
                component.forceUpdate()
            })

        }
    }
}
*/
/*
@DropTarget([DndItemTypes.PERSON], paneTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    item: monitor.getItem()
}))
*/
@observer
export default class PDWorkPane extends Component {
    constructor(props) {
        super(props)
        this.onCardCreate = this.onCardCreate.bind(this)
        // this.onCardFilter = this.onCardFilter.bind(this)
        this.onCardsChange = this.onCardsChange.bind(this)

        this.state = {
            isFiltering: false,
            isDeleting: false,
            selTab: 'cards',
            defaultReactionSets: [],
            autoMsg: null,
            requestInputFocus: true
        }
    }

    // TODO use this pattern in projectsPage, which has multiple sources of truth.
    viewCards() {
        this.props.viewCards()
    }

    viewComments() {
        this.props.loadComments()
        // this.setState({view: 'comments'})
    }

    viewImages() {
        this.props.viewImages()
    }

    toggleTags() {
        this.props.toggleTags()
    }

    toggleLetters() {
        this.props.toggleLetters()
    }

    toggleActions() {
        this.props.toggleActions()
    }

    onCardCreate(card) {
        //this.props.onCardCreate
    }

    filterCards(query, tagIds) {
        const project = projectStore.currentProject

        if (query && tagIds)
            throw new Error ('filter cards: choose query or tags, not both(yet).')

        if (!query && (!tagIds || tagIds.length === 0)) {
            cardStore.stopFiltering()
            return
        }

        if (query)
            findCards (query, null, project.id).then (response => {
                let filteredCards = response.data.cards.map (apiCard => {
                    return new CardModel (apiCard)
                })
                debugger
                cardStore.setFilteredCards(filteredCards, true)
            })
        else // tag ids
            TagApiService.findCardsByTags(tagIds, project.id).then (response => {
                let card
                let filteredCards = response.data.cards.map (apiCard => {
                    card = new CardModel (apiCard)
                    card.tags2 = tagStore.tagIdsToTags(card.tagIds) // Ideally, card tag search would give us same tags as other endpoints
                    return card
                })
                // this.setState ({filteredCards: filteredCards, isFiltering: true})
                debugger
                cardStore.setFilteredCards(filteredCards, true)
            })

    }

/*
    onCardFilter(filteredCards) {
        this.setState({filteredCards: filteredCards})
    }
*/

    onCardsChange() {
        this.props.onCardsChange()
    }

    onSettings() {

    }

    saveCard(card) {
        this.props.saveCard(card)
    }

    createCards(cards) {
        this.props.createCards(cards)
    }

    importCards(cards) {
        this.props.importCards(cards)
    }

    importAssets(assets, source) {
        this.props.importAssets(assets, source)
    }

    uploadAssets(cards, files) {
        this.props.uploadAssets(cards, files)
    }

    createProject() {
        this.props.createProject()
    }

    deleteProject() {
        this.setState({isDeleting: true})
        this.props.deleteProject(projectStore.currentProject)
    }

    leaveProject() {
        this.setState({isDeleting: true})
        this.props.leaveProject(projectStore.currentProject.id)
    }

    showMsgs = (ev) => {
        this.setState({selTab: 'msgs'})
    }

    showCards = (ev) => {
        this.setState({selTab: 'cards'})
    }

    // TODO see SSOT note in APWorkPane
    // TODO worth combining with other parent of APWorkFeed, approve.js? Or different use cases?
    onVote(rsum) {
        const {inputCard} = this.state
        if (!inputCard)
            debugger

        const me = userStore.currentUser
        const reactions = rsum.card_reactions || []
        inputCard.reactions = reactions
        this.setState({inputCard: inputCard}) // trigger trickle down props

        // auto generate a voting message
        const isApproved = reactions.find(r => r.user_id === me.id && r.reaction_type_id === ReactionApiService.getThumbsUpId()) !== undefined
        const isRejected = reactions.find(r => r.user_id === me.id && r.reaction_type_id === ReactionApiService.getThumbsDownId()) !== undefined
        const isRated = reactions.find(r => r.user_id === me.id && r.reaction_type_id === ReactionApiService.getStarsId()) !== undefined
        const action = isApproved? 'approved' : isRejected? 'rejected' : isRated? 'rated' : null
        if (!action)
            return

        const card = cardStore.getCardFromCurrent(rsum.card_id)
        if (card.id !== inputCard.id)
            debugger // impossible as of 1.11.2019

        // const msg = (me.displayName || me.firstName) + ' ' + action + ' "' + card.title +'"'
        // this.setState({autoMsg: msg})
        /*
                setTimeout(() => {
                    this.setState({autoMsg: null})
                }, 1) // Purpose: we want to prop an update, but avoid excess calls. So flip on and off. Would a mbox solution be any better?
        */
    }

    onPaintSave = (cardTitle) => {
        const me = userStore.currentUser
        if (!cardTitle || !me)
            return null

        // Backend now generates project_comment that references markup. No longer need to autogen here.
        // this.setState({autoMsg: (me.displayName || me.firstName) + ' added markup to "' +truncate(cardTitle, 25, true)+ '"'
        // })
    }

    requestInputFocus = (ev) => {
        this.setState({requestInputFocus: true})
    }

    treatAsAP = (isOnMeAP) => {
        const project = projectStore.currentProject
        if (isOnMeAP && project.cardTotal > 0)
            uiStateStore.setViewingCard(project.cards[0])
    }

    componentDidMount() {
        const project = projectStore.currentProject
        ReactionApiService.getDefaultReactionSets().then (rs => {
            this.setState({defaultReactionSets: rs})
        })
        if (project && project.reactionSetId)
            ReactionApiService.getReactionSetById (project.reactionSetId).then( rs => {
                project.reactionSet = rs
            })


    }

    componentDidUpdate() {
        const activeUser = userStore.currentUser
        const project = projectStore.currentProject
        const isOnMeAP = project && project.projectType === ProjectType.APPROVAL && (!activeUser || project.creatorId !== activeUser.id)
        // this.treatAsAP(isOnMeAP)
    }

    render() {
        const { user, mobile, item, showTags } = this.props
        const project = projectStore.currentProject
        const { autoMsg, selTab, requestInputFocus } = this.state

        const cards = project? project.cards : []
        const tagSet = project? project.tagSet2 : []
        const cmts = project? project.comments : []

        const initialShowTags = showTags && cards.length > 0

        const availWidth = uiStateStore.voMode? '100%' : !uiStateStore.showSidebar || uiStateStore.minimizeLeftCol? '95%' : '81.25%'
        const discussColumns = uiStateStore.showDiscussion? 5 : 1
        const workColumns = 16 - discussColumns

        // const hdrClass = mobile? 'thinH':'thinH2'
        // const hdrSpacing = mobile? '1rem':'2rem'
        // const isLeft = uiStateStore.discussPosition === 'left'
        const isApproval = uiStateStore.rightUnModal === SendForApproval && uiStateStore.approvalSent === false && uiStateStore.isAPStarterRequest
        const discussClass = 'discussCol discussBG discussBody' // +(isLeft? ' borderL' : ' borderR')
        const discussStyle = isApproval? {padding: 0, height: '99%', overflowY: 'auto', overflowX: 'hidden'}:{padding: 0}

        const overflowMode = uiStateStore.paintEnable? 'hidden':'auto'

        if (uiStateStore.unModal !== null) {
            return <UnmodalContainer />
        }

        // I hate to split up the DOM tree, but there are simply too many problems with conditional Grid.Columns within a single DOM.
        // TODO: if we move more state into mobx, it'll mean less repetition here.
        // TODO: Also, let's replace semantic Grid with flex layout.
        const pdwToolbar =
        <PDWToolbar user={user}
                    cards={cards}
                    project={project}
                    mobile={mobile}

            // views
                    view={this.props.view}
                    viewCards={() => {this.viewCards()}}
                    viewComments={() => {this.viewComments()}}
                    viewImages={() => {this.viewImages()}}
                    toggleTags={() => {this.toggleTags()}}
                    toggleLetters={() => {this.toggleLetters()}}
                    toggleActions={() => {this.toggleActions()}}
                    showTags={this.props.showTags}
                    showLetters={this.props.showLetters}
                    showActions={this.props.showActions}

            // event handlers (event has already occurred)
                    onCardCreate={this.onCardCreate}
                    onCardFilter={this.onCardFilter}
            // onCardsChange={this.onCardsChange}
                    onSettings={this.onSettings}

            // action requests (action has yet to occur)
                    saveCard={(card) => {this.saveCard(card)}}
                    createCards={(cards) => {this.createCards(cards)}}
                    importCards={(cards) => {this.importCards(cards)}}
                    importAssets={(assets, source) => {this.importAssets(assets, source)}}
                    uploadAssets={(cards, files) => {this.uploadAssets(cards, files)}}
                    createProject={() => {this.createProject()}}
                    leaveProject={() => this.leaveProject()}
                    deleteProject={() => this.deleteProject()}
                    handleCardSearchResults={(query) => {this.filterCards(query, null)}}
        />

        const apToolbar =
            <APToolbar  cards={cards}
                        project={project}
            />

        const pdwCardsPane =
            <PDWCardsPane project={project}
                          mobile={mobile}
                          isFiltering={cardStore.isFiltering}
                          user={user}
                          onCardsChange={this.onCardsChange}
                          view={this.props.view}
                          showTags={this.props.showTags}
                          showLetters={this.props.showLetters}
                          showActions={this.props.showActions}

                // action requests (action has yet to occur)
                          createCards={(cards) => {this.createCards(cards)}}
                          importCards={(cards) => {this.importCards(cards)}}
                          importAssets={(assets, source) => {this.importAssets(assets, source)}}
                          uploadAssets={(cards, files) => {this.uploadAssets(cards, files)}}

                          saveCard={(card) => {this.saveCard(card)}}
                          refreshCards={() => {this.props.refreshCards()}}
                          refreshMembers={() => {this.props.refreshMembers()}}
            />

        const tagToolbar =
            <PDWTagToolbar  user={user}
                            cards={cards}
                            mobile={mobile}
                            project={project}
                            tags={tagSet}
                            showTags={this.props.showTags}
                            filterByTags={(tags) => this.filterCards(null, tags)}
            />

        const conversations =
            <Conversations project={project}
                           selectedCard={uiStateStore.viewingCard || uiStateStore.editingCard}
                           focus={requestInputFocus}
                           inputCard={uiStateStore.viewingCard}
                           user={user}
                           autoMsg={autoMsg}
                           mobile={mobile}
                           layout='v'/>

        if (mobile) { // No need for Grid. Retain for style consistency only?
            return (
            <main className="cardViews hidden-scroll hidden-scroll-dark" style={{width: 'calc(100% + 10px)', height: '100%', overflowY:overflowMode}}>
                <Grid style={{height: '105% !important'}}>
                    <Grid.Row style={{paddingBottom: '0', paddingTop: '0'}}>
                        <Grid.Column width={16}>
                            {Boolean(!isApproval && !uiStateStore.viewingCard) && pdwToolbar}
                            {isApproval && apToolbar}
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row style={{paddingTop: '0'}}>
                        <Grid.Column width={16}>
                            <div style={{marginLeft: '1rem', marginBottom: '1rem'}}>
                                {/*<span className={'small pointer'} style={{marginRight: hdrSpacing}} onClick={this.showCards}>Cards</span>*/}
                                {/*<span className={'small pointer'} style={{marginRight: hdrSpacing}} onClick={this.showMsgs}>Comments</span>*/}
                            </div>

                            <APWorkFeed selectedCard={null}
                                        selectedCardIndex={0}
                                        onSelCard={null}
                                        onVote={null}
                                        mobile={true}
                                        createCards={(cards) => {this.createCards(cards)}}
                                        importCards={(cards) => {this.importCards(cards)}}
                                        importAssets={(assets, source) => {this.importAssets(assets, source)}}
                                        uploadAssets={(cards, files) => {this.uploadAssets(cards, files)}}

                            />

                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <PGMobileSidebar
                    position='right'
                    component={
                        <div>
                            <PDWDiscussToolbar project={project} filterBy={uiStateStore.viewingCard || uiStateStore.editingCard} mobile/>
                            {conversations}
                        </div>
                    }
                />

            </main>
            )
        }

        // Large
        else return (
            <main className="cardViews" style={{width: availWidth, height: 'calc(100% - 5rem)'}} >

                <Grid style={{height: '100%'}}>
                    <Grid.Row style={{paddingTop: '0', height: '100%'}}>

                        <Grid.Column className='workCol' computer={workColumns} tablet={workColumns} mobile={16} style={{height: '100%'}}>
                            <Dimmer active={uiStateStore.currentApprovalRequest !== null && !uiStateStore.isAPStarterRequest} >
                                <div className='absFull' style={{backgroundColor: 'rgba(0,0,0,.94) !important'}}>&nbsp;</div>
                            </Dimmer>

                            {Boolean(!isApproval && !uiStateStore.viewingCard) && pdwToolbar}
                            {isApproval && apToolbar}

                            {Boolean(!uiStateStore.viewingCard && !uiStateStore.editingCard && showTags) && tagToolbar}

                            {!uiStateStore.viewingCard && !uiStateStore.editingCard && pdwCardsPane}

                            {!uiStateStore.editingCard && uiStateStore.viewingCard &&
                            <CardViewerPane onInputRequest={this.requestInputFocus} onPaintSave={this.onPaintSave}/>
                            }

                            {!uiStateStore.viewingCard && uiStateStore.editingCard &&
                            <CardEditorPane />
                            }

                            {!uiStateStore.editingCard &&
                            <PDWStatusbar cardCount={cardStore.isFiltering? cardStore.filteredCards.length : cardStore.currentCards.length}
                                          isFiltering={cardStore.isFiltering}
                                          memberCount={project? project.memberTotal : 0}
                                          cardTagCount={tagSet.length}
                                          projectCommentTotal={project? project.projectCommentTotal : 0}
                                          project={project}
                            />
                            }
                        </Grid.Column>

                        <Grid.Column width={discussColumns} className={discussClass} style={discussStyle} >

                            {!uiStateStore.rightUnModal &&
                            <PDWDiscussToolbar project={project}
                                               filterBy={uiStateStore.viewingCard || uiStateStore.editingCard}/>
                            }

                            {!uiStateStore.rightUnModal &&
                            conversations
                            }

                            {uiStateStore.rightUnModal &&
                                <RightUnmodalContainer />
                            }
                        </Grid.Column>

                    </Grid.Row>
                </Grid>

            </main>
        )
    }
}