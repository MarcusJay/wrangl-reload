import React, {Component} from 'react'
import {observer} from 'mobx-react'
import {Link, Router} from '~/routes'
import DumbBadge from "../../common/DumbBadge";
import DumbCapsule from "../../common/DumbCapsule";
import categoryStore from "../../../store/categoryStore";


export default class ProjectThumb extends React.Component {

    gotoProject = (ev) => {
        Router.prefetchRoute('project', {id: this.props.project.id}) // Because <Link> appears to be unreliable.
    }


    render() {
        const {project, isSelected, badgeCount, isNew} = this.props

        return (
            <Link prefetch route='project' params={{id: project.id}}>
                <div className={"row noWrap relative" + (isSelected ? ' active' : '')}
                     // onClick={() => this.gotoProject (project)}
                     data-tip={'Open "' + project.title + '"'} data-place="right">
                    <div className="image inline suitcaseM"
                         style={{backgroundImage: 'url(' + (project.image) + ')'}}>

                    </div>

                    {badgeCount > 0 &&
                    <DumbBadge count={badgeCount} className='abs' style={{top: '2px', left: '23px'}}/>
                    }

                    {isNew && badgeCount === 0 &&
                    <DumbCapsule msg='New' className='abs' style={{top: '0', right: '0'}}/>
                    }

                    <span className="noWrap inline small light ctitle">{project.title}</span>
                </div>
            </Link>
        )
    }
}
