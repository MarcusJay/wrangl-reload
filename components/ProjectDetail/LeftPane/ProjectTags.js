import {Component} from 'react'
import {observer} from 'mobx-react'

import {Dimmer, Loader} from 'semantic-ui-react'
// import TagConfig from '/config/TagConfig'
import PDTagCreator from "/components/common/TagCreator";
import tagStore from '/store/tagStore'
import ProjectTag from "./ProjectTag";
import {X} from "react-feather";
import {GithubPicker} from "react-color"; // trying simpler pattern of allowing smart components to access lesser stores.

@observer
export default class ProjectTags extends Component {
    constructor(props) {
        super(props)
        this.state = {loading: false, showTagEditor: false, editingTag: null, tagName: null, tagColor: null}
    }

    closeTagEditor = () => {
        this.setState({showTagEditor: false})
    }

    editTag = (tag) => {
        this.setState({showTagEditor: true, editingTag: tag, tagColor: tag.color, tagName: tag.name})
    }

    editTagColor = (tag) => {
        this.setState({editingTag: tag, tagColor: tag.color, tagName: tag.name})
        this.saveTag()
    }

    onNameChange = (ev) => {
        this.setState ({tagName: ev.target.value})
    }

    onKeyPress = (ev) => {
        if (ev.key !== 'Enter')
            return

        this.saveTag(ev)
    }

    saveTag = (ev) => {
        const {editingTag, tagName, tagColor} = this.state
        editingTag.name = tagName
        editingTag.color = tagColor
        tagStore.updateTag(editingTag).then(response => {
            this.setState({editingTag: null})
            this.closeTagEditor()
        })
    }

    deleteTag = (tagObj) => {
        this.setState({loading: true})
        tagStore.deleteTagFromCardableSet(this.props.project.id, tagObj.id).then( r => this.setState({loading: false}) )

    }

    componentWillReact() {
        console.log('PDLTags will react.')
    }

    render() {
        const { project, editEnabled, mobile } = this.props
        const {tagColor, tagName, showTagEditor, showColorPicker } = this.state
        const tags = tagStore.currentTagSet // this.state.tagSet
        // const paneClass = mobile? 'm-side-pane':'side-pane'

        return(
            <div className='toolbar-tags'>
{/*
                    {editEnabled &&
                    <PDTagCreator createTag={(newTag) => this.updateTag(newTag)} />
                    }
*/}
                    {tags.map( tagObj => {

                        return(
                            <ProjectTag
                                tag={tagObj} key={'k'+tagObj.id}
                                editEnabled={true}
                                editTag={this.editTag}
                                editTagColor={this.editTagColor}
                                updateTag={this.updateTag}
                                deleteTag={this.deleteTag}
                                project={project}/>
                        )
                    })}

                {showTagEditor &&
                <div className='abs formPane zModal modalShadow' style={{padding: '1rem', backgroundColor: '#333'}}> {/*crime*/}
                    <X size={24} onClick={this.closeTabEditor} className='vaTop pointer abs' style={{right: '1rem'}}/>
                    <span className='infoLabel'>
                        Edit tag name
                    </span>
                    <input type='text'
                           name='new'
                           placeholder='Enter name...'
                           value={tagName}
                           onChange={this.onNameChange}
                           onKeyPress={this.onKeyPress}
                           className='jSec jItemBG mr1'
                           style={{minWidth: '26rem', padding: '.5rem'}}
                    />

                    <div className='inline jBtn pointer mr1' onClick={this.closeTagEditor}>
                        Cancel
                    </div>
                    <div className='inline jBtn pointer ' onClick={this.saveTag}>
                        Save
                    </div>
                </div>
                }


            </div>
        )
    }
}