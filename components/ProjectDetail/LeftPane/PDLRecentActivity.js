import {Component} from 'react'

const mockActivity = [
    {projectName: 'Project A', activityCount: 5},
    {projectName: 'Project B', activityCount: 7},
    {projectName: 'Project C', activityCount: 14},
    {projectName: 'Project D', activityCount: 3},
]

export default class PDLRecentActivity extends Component {
    constructor(props) {
        super(props)
        this.createActivityHtml = this.createActivityHtml.bind(this)
    }

    createActivityHtml(mockActivity) {
        let html = []
        for (let i=0; i<mockActivity.length; i++) {
            html.push( <div key={mockActivity[i].projectName} className="projectActivityBox">{mockActivity[i].projectName} new activity: {mockActivity[i].activityCount}</div> )
        }
        return html
    }

    render() {
        return(
            <div className="side-pane" >
                {this.createActivityHtml(mockActivity)}

                <style jsx>{`
                  height: 30%;
                `}</style>
            </div>
        )
    }
}