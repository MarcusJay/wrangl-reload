import {Component} from 'react'
import {observer} from 'mobx-react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import {Comment, Input} from 'semantic-ui-react'
import {Trash, User} from 'react-feather'

import StateChangeRequestor, {REQUEST_PROJECT_COMMENT} from '/services/StateChangeRequestor'
import Moment from "react-moment"
import userStore from "/store/userStore"
import projectStore from '/store/projectStore'

@observer
export default class PDLNotes extends Component {
    constructor(props) {
        super(props)
        this.state = {project: this.props.project, comments: this.props.comments || [], commentValue: ''}
    }

    handleChange(ev) {
        this.setState({commentValue: ev.target.value})
    }

    handleInput(ev, data) {
        // console.log(ev)
        if (ev.key === 'Enter') {
            StateChangeRequestor.awaitStateChange(REQUEST_PROJECT_COMMENT, {project: this.props.project, commentText: ev.target.value})
                .then( comment => {
                    console.log("Received back: " +comment.comment_text)
                    this.setState({comments: [comment, ...this.state.comments], commentValue: '' })
                })
        } 
    }

    deleteComment(ev, comment) {
        ev.stopPropagation()
        ev.preventDefault()
        projectStore.deleteComment(comment.comment_id).then( response => {
            if (response.success === true) {
                let index = this.state.comments.findIndex( c => c.comment_id === comment.comment_id)
                this.state.comments.splice(index,1)
                this.setState({comments: this.state.comments})
                this.props.project.comments = this.state.comments // mutating parent project. TODO just reload comments from store
            }

        })

    }

    componentWillMount() {
        this.state.comments.forEach( comment => {
            comment.user = userStore.getUserFromConnections(comment.comment_creator)
        })
    }

    render() {
        const self = this

        return(
            <div className="hidden-scroll-light discuss">
                <Input icon='add' inverted value={this.state.commentValue} fluid placeholder='Add a comment...'
                       style={{marginBottom: '7px'}}
                       onKeyPress={(ev) => {this.handleInput(ev)}}
                       onChange={(ev) => this.handleChange(ev)}/>
                <div className='history'>
                    {this.state.comments.length === 0 &&
                    <div className="secondary" style={{marginTop: '1rem'}}>No project comments</div>
                    }
                    {this.state.comments.length > 0 &&
                    <Comment.Group>
                        <ReactCSSTransitionGroup
                            transitionName="cardfx"
                            transitionEnterTimeout={300}
                            transitionLeaveTimeout={200}>

                        {this.state.comments.map (function (comment, i, comments) {
                            return (
                                <Comment key={'pdlc'+i}>
                                    {comment.user.image &&
                                    <img className='userImg' src={comment.user.image}/>
                                    }
                                    {!comment.user.image &&
                                    <User size={24}/>
                                    }
                                    <Comment.Content>
                                        <Comment.Author as='a'>{comment.user.displayName}</Comment.Author>
                                        <Comment.Metadata>
                                            <Moment subtract={{hours: 8}} className="darkSecondary small"
                                                    fromNow>{comment.time_modified}</Moment>
                                            {comment.user && self.props.user && comment.user.id === self.props.user.id &&
                                            <Trash color="#777" size={16}
                                                   onClick={(ev) => self.deleteComment (ev, comment)}
                                                   className="deleteIcon"/>
                                            }
                                        </Comment.Metadata>
                                        <Comment.Text className='cText'>{comment.comment_text}</Comment.Text>
                                    </Comment.Content>
                                </Comment>
                            )
                        })}
                        </ReactCSSTransitionGroup>
                    </Comment.Group>
                    }
                    </div>
                {/*<TextArea placeholder='Project-level discussion' style={{ minHeight: 150, width: '95%', backgroundColor: '#f0f0f0' }} />*/}
            </div>
        )
    }
}