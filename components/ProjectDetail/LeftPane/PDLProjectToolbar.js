import {Component} from 'react'
import {Confirm, Menu} from 'semantic-ui-react'
import {Copy, Settings, Trash} from 'react-feather';
import ShareModal from '/components/modals/ShareModal'
import uiStateStore from '/store/uiStateStore'

export default class PDLProjectToolbar extends Component {
    constructor(props) {
        super(props)
        this.state = {showDeleteConfirmation: false}
    }

    handleCreateProject() {

    }

    openSettings() {
        this.props.settings()
    }

    confirmDelete = () => {
        this.setState({showDeleteConfirmation: true})
    }

    onDeleteCancel = () => {
        this.setState({showDeleteConfirmation: false})
    }

    onDeleteConfirm = () => {
        this.props.deleteProject(this.props.project)
    }

    render() {
        return(
            <div className="side-pane">
                <Menu className="toolbar-menu darkBack" style={{ color: '#999', margin: '.5rem 0 0 0' }}>
                    
                    <Menu.Item style={{marginLeft: '0 !important', paddingLeft: '0'}}>
                        <ShareModal project={this.props.project}
                                    connections={this.props.connections}
                                    user={this.props.user}
                                    style={{ paddingRight: '0.4rem', marginBottom: '-7px' }}  data-tip="Share this project" data-type="light"
                        />

                        <Copy color="#e0e0e0" className='dark' style={{ paddingRight: '0.1rem', marginBottom: '0' }}  data-tip="Copy this project (coming soon)" data-type="light"/>
                        <Settings color="#e0e0e0" className='dark' onClick={() => {this.gotoProjectSettings()}} style={{ paddingRight: '0.1rem' }} data-tip="Board settings (coming soon)" data-type="light"/>
                        {!uiStateStore.voMode && this.props.user && this.props.project && this.props.project.creatorId === this.props.user.id &&
                        <Trash color="#e0e0e0" onClick={this.confirmDelete} className='dark' style={{paddingRight: '0.1rem', marginBottom: '0'}} data-tip="Delete this project" data-type="light"/>
                        }
                    </Menu.Item>

                    <Confirm
                        content={'Are you sure you want to permanently delete Project "' +this.props.project.title+ '"?'}
                        open={this.state.showDeleteConfirmation}
                        onCancel={this.onDeleteCancel}
                        onConfirm={this.onDeleteConfirm}
                        confirmButton='Delete Project'
                        style={{height: '25% !important'}}
                    />
                </Menu>
            </div>
        )
    }
}