import {Component} from 'react'
import PropTypes from 'prop-types'

import {DragSource} from 'react-dnd'
import {DndItemTypes} from '~/config/Constants'
import {GithubPicker, SketchPicker} from "react-color";

import {Dropdown, Icon, Popup} from 'semantic-ui-react'
import {XCircle} from 'react-feather'
import uiStateStore from "../../../store/uiStateStore";
import ColorUtils from "../../../utils/ColorUtils";
import {DEFAULT_CAT} from "../../../config/Constants";
import React from "react";

export default class ProjectTag extends Component {
    state = {showColorPicker: false}

    editTag = () => {
        this.props.editTag(this.props.tag)
    }

    editColor = (ev) => {
        this.setState({showColorPicker: true})
    }

    handleColorChangeComplete = (color, ev) => {
        this.props.tag.color = color.hex
        this.props.editTagColor(this.props.tag)
        this.setState({showColorPicker: false})
    }

    deleteTag = () => {
        this.props.deleteTag (this.props.tag)
    }

    render() {
        const {tag, editEnabled, monitorTagEdit, updateTag, deleteTag, project} = this.props
        const {showColorPicker} = this.state
        const textColorClass = ColorUtils.isDark (tag.color) ? ' lightOnDark' : ' darkOnLight'

        return (
            <span
                style={{
                    backgroundColor: '#'+tag.color,
                    overflow: 'visible'
                }}
                className={'tagBtn ' + textColorClass}
            >
                {tag.name}

                <Dropdown
                    icon={<Icon name="dropdown" size="large" className='ml05 vaTop'
                                style={{fontSize: '1.5rem', margin: '0 0 0 0'}}/>}
                >
                        <Dropdown.Menu className='hioh bg fg'>
                            <Dropdown.Item name='open' onClick={this.editTag}>
                                Edit Name...
                            </Dropdown.Item>
                            <Dropdown.Item name='open' onClick={this.editColor}>
                                Edit Color...
                            </Dropdown.Item>

                            <Dropdown.Item name='open' onClick={this.deleteTag}>
                                Delete Tag
                            </Dropdown.Item>
                        </Dropdown.Menu>
                </Dropdown>

                {showColorPicker &&
                <div className='abs' style={{top: '3rem'}}>
                    <GithubPicker disableAlpha presetColors={['#B80000', '#DB3E00', '#FCCB00', '#008B02', '#006B76', '#1273DE', '#004DCF', '#5300EB', '#EB9694', '#FAD0C3', '#FEF3BD', '#C1E1C5', '#BEDADC', '#C4DEF6', '#BED3F3', '#D4C4FB']}
                                  width={220} color={'#'+tag.color} onChangeComplete={ this.handleColorChangeComplete }/>
                </div>
                }

            </span>
        )
    }
}