import {Component} from 'react'
import {Confirm} from 'semantic-ui-react'
import {Camera, Copy, Share} from 'react-feather';

export default class PDLProjectToolbar extends Component {
    constructor(props) {
        super(props)
        this.state = {showDeleteConfirmation: false}
    }

    handleCreateProject() {

    }

    openSettings() {
        this.props.settings()
    }

    confirmDelete = () => {
        this.setState({showDeleteConfirmation: true})
    }

    onDeleteCancel = () => {
        this.setState({showDeleteConfirmation: false})
    }

    onDeleteConfirm = () => {
        alert("Project Delete needs wiring up.")
        this.setState({showDeleteConfirmation: false})
    }

    render() {
        return(
            <div>
                        <Share color="#777"/>
                        <Camera color="#777"/>
                        <Copy color="#777"/>
                        {/*<Feather.Trash color="#777" onClick={this.confirmDelete}/>*/}
                    <Confirm
                        content={'Are you sure you want to delete "' +this.props.project.title+ '"?'}
                        open={this.state.showDeleteConfirmation}
                        onCancel={this.onDeleteCancel}
                        onConfirm={this.onDeleteConfirm}
                        confirmButton='Delete Project'
                    />
            </div>
        )
    }
}