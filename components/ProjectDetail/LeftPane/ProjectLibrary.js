import {Component} from 'react'
import {observer} from 'mobx-react'
import {Link, Router} from '~/routes'
import {SearchConfig} from '/config/Constants'

import {Form, Icon, Input, Loader} from 'semantic-ui-react'
import projectStore from '/store/projectStore'
import uiStateStore from '/store/uiStateStore'
import ProjectModel from "/models/ProjectModel";
import ProjectApiService from '/services/ProjectApiService'
import userStore from "/store/userStore";
import eventStore from "/store/eventStore"
import {APPROVAL_CAT, ARCHIVED_CAT, DEFAULT_CAT, REQUEST_CAT} from "../../../config/Constants";
import ProjectThumb from "./ProjectThumb";
import {truncate} from "../../../utils/solo/truncate";
import {ChevronDown, ChevronUp, Plus} from "react-feather";
import DumbBadge from "../../common/DumbBadge";
// import {getUserApprovals} from "../../../services/ApprovalService";

@observer
export default class ProjectLibrary extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            isOpen: false,
            searching: false,
            query: '',
            searchResults: projectStore.userProjects || [] }
        this.folioSize = projectStore.userFolio? projectStore.userFolio.length : 0
    }

    toggleOpen = (ev) => {
        const newIsOpen = !this.state.isOpen
        if (newIsOpen && !uiStateStore.showSidebar)
            uiStateStore.toggleSidebar()
        this.setState({isOpen: newIsOpen})
    }

    toggleSection = (catId) => {
        const newOpenState = this.state[catId+'Open'] === undefined? true : !this.state[catId+'Open'] // lazy init
        const newBadgeState = this.state[catId+'Badge'] === undefined? true : !this.state[catId+'Badge']

        // We could have initialized each catId state to closed, but we can lazily set state here when needed.
        this.setState({[catId+'Open']: newOpenState, [catId+'Badge']: newBadgeState})
    }

    onSearchChange = (ev) => {
        let query = ev.target.value
        if (!query)
            query = ''
        console.log('change: query = ' +query)
        let now = new Date()
        let elapsed = this.state.lastKeyPress === 0? 0 : now - this.state.lastKeyPress
        this.setState({query: ev.target.value, lastKeyPress: now})

        setTimeout( () => {
            if (query.length >= SearchConfig.MIN_SEARCH_CHARS) {
                this.submitSearch (query)
            } else {
                projectStore.isFiltering = false
                this.setState ({searchResults: projectStore.userProjects})
            }
            this.setState ({waiting: false})

        }, SearchConfig.QUERY_DELAY_MS )

    }

    submitSearch(query) {
        console.log("Searching for " +query+ "...")

        this.setState ({searching: true, isVirgin: false})

        projectStore.filterProjectsByQuery(query.toLowerCase()).then (matchingProjects => {
            this.setState ({
                searching: false,
                searchResults: matchingProjects || []
            })
        }).catch (error => {
            console.error ("Error searching for " + query, error)
            this.setState ({searching: false})
        })
    }

    createProject = () => {
        this.props.createProject()
    }

/*
    gotoProject(project) {
        // this.setState({loading: true})
        Router.pushRoute('project', {id: project.id}) // Because <Link> appears to be unreliable.
    }
*/

    componentDidMount() {
        const userId = userStore.currentUser.id

        if (uiStateStore.currentPage === uiStateStore.PD && (!projectStore.userProjects || projectStore.userProjects.length === 0)) {
            this.setState({loading: true})
            projectStore.getUserProjects(userId).then (({projects, folio}) => {
                this.setState ({loading: false, searchResults: projects})

            })
        }

/*
        if (!projectStore.userApprovals) {
            this.setState({loading: true})
            getUserApprovals(userId).then ( (approvals) => {
                this.setState ({loading: false, searchResults: approvals})
            })
        }
*/

    }

    componentDidUpdate() {
/*
        const {searching, searchResults} = this.state
        const projects = projectStore.userProjects || []
        if (!searching && (!searchResults || searchResults.length === 0) && projects.length > 0) {
        // if (!searching && projectStore.userProjects.length > 0) { infinite
        //     console.log('Project Lib did update')
        //     this.setState({searchResults: projectStore.userProjects})
        }
*/

        // folio is populated, time to initialize expand/collapsed state of categories
/*
        if (projectStore.userFolio && projectStore.userFolio.length !== this.folioSize) {
            projectStore.userFolio.forEach( (cat, i) => {

            })
        }
*/
    }

    componentWillReceiveProps() {
        this.setState({thisWillTrigger: 'anUpdate'})
    }

    componentWillReact() {
        console.log('Project Lib will react')
        // this.setState({searchResults: projectStore.userProjects})
    }

    render() {
        const {badgeTotal, isMini} = this.props
        const {searching, isOpen, searchResults} = this.state
        const allProjects = searchResults || projectStore.userProjects
        if (!allProjects)
            return null

        // Wait for it. Loaded by either PD or PL, not by us.
        if (!projectStore.userFolio || projectStore.userFolio.length === 0) {
            return null
        }

        const folio = projectStore.userFolio.slice? projectStore.userFolio.slice() : []

        const ffolio = folio.filter( cat => cat && cat.id !== APPROVAL_CAT && cat.id !== REQUEST_CAT) || []

        const isFiltering = projectStore.isFiltering
        const showSections = ffolio.length > 1

        const evs = eventStore.getInstance()
        const cagBadges = evs.currentBadges? evs.currentBadges.categories : null
        const defaultBadgeInfo = cagBadges? cagBadges[DEFAULT_CAT] : null
        // const badgedProjectIds = defaultBadgeInfo? Object.keys(defaultBadgeInfo.projects) : []
        // const badgedProjects = typeof allProjects.filter === 'function'? (allProjects.filter (project => badgedProjectIds.indexOf(project.id) !== -1) || []) : []
        // const remainingProjects = typeof allProjects.filter === 'function'? (allProjects.filter (project => badgedProjectIds.indexOf(project.id) === -1) || []) : []
        const projectCount = allProjects? allProjects.length : 0

        const paneClass = 'pointer relative' +(this.props.mobile? ' m-side-pane':' side-pane')
        let project, badgeCount, isSelected, isNew, projectBadgeDetails

/*
        if (!isOpen)
            return (
                <section onClick={this.toggleOpen} className='pointer'>
                    <img src='/static/svg/v2/briefcase.svg' width='26px'
                         className='ml1 mt2' onClick={this.showBoards}
                         data-tip="View Boards"/>

                    <div className='tiny kern1 inline jFG vaTop' style={{paddingLeft: '1rem', marginTop: '2.3rem'}}>BOARDS</div>
                </section>
            )
*/

        return(
            <div className={paneClass} style={{marginBottom: '8rem'}}>
                <Loader active={this.state.loading}></Loader>

                <div className={'paneHdr mt2 ' +(isOpen? 'exp':'col')} onClick={this.toggleOpen}>
                    <img src='/static/svg/v2/briefcase.svg' width='26px'
                         className='ml1 animPos'
                         data-tip="View Boards"/>

                    {badgeTotal > 0 &&
                    <DumbBadge count={badgeTotal} className='abs' style={{top: '-.25rem', left: '2rem'}}/>
                    }

                    {!isMini &&
                    <div className='tiny kern1 inline vaTop '
                         style={{paddingLeft: '1rem', marginTop: '.3rem'}}>
                        BOARDS
                    </div>
                    }

                    {isOpen && !isMini &&
                    <Plus size={22} className='ml1 boh pointer' onClick={this.createProject}
                        data-tip="Create a new board" />
                    }
                </div>

                {!isMini &&
                <div className={'paneContent ' + (isOpen ? 'exp' : 'col')}>
                    <Form inverted className="thumbSearch mt05" style={{paddingLeft: '.5rem'}}>
                        <Input icon={<Icon name="search"/>}
                               name='Search'
                               data-tip='Search boards by name and description'
                               className="sidePaneInput small lighter"
                               style={{paddingRight: '.5rem'}}
                               onChange={this.onSearchChange}
                               value={this.state.query}
                               id="cardSearch"
                            // style={{color: '#777'}}
                               placeholder='Search boards...'
                        />
                    </Form>

                    <div className='projectLib thumbLibrary'>
                        {isFiltering &&
                        allProjects.map ((project, i) => {
                            projectBadgeDetails = defaultBadgeInfo && defaultBadgeInfo.projects && defaultBadgeInfo.projects[project.id] ? defaultBadgeInfo.projects[project.id] : null
                            badgeCount = projectBadgeDetails ? projectBadgeDetails.total : 0
                            isNew = projectBadgeDetails ? Boolean (projectBadgeDetails.new) : false
                            isSelected = projectStore.currentProject && project.id === projectStore.currentProject.id

                            return <ProjectThumb project={project} badgeCount={badgeCount} isNew={isNew}
                                                 key={project.domId}
                                                 isSelected={isSelected}/>
                        })
                        }


                        {!isFiltering && ffolio.map ((cat, i) => {
                            badgeCount = cat.id !== ARCHIVED_CAT && cagBadges && cagBadges[cat.id] !== undefined ? cagBadges[cat.id].total : 0
                            return cat.projectIds && cat.projectIds.length > 0?
                            (
                                <div key={'c'+cat.id}>

                                    {/*SECTION HEADER*/}
                                    {showSections &&
                                    <div
                                        className={'tiny kern1 jFG mt05 jMidBG secHdr' + (this.state[cat.id + 'Open'] ? ' active' : '')}
                                        style={{padding: '1rem .25rem 1rem 1rem'}}
                                        onClick={() => this.toggleSection (cat.id)}>
                                        {truncate (cat.title, 20, true)}: {cat.projectIds ? cat.projectIds.length : 0}
                                        {this.state[cat.id + 'Open'] && !isFiltering &&
                                        <ChevronUp size={22} className='abs jItemBG round pointer hoh'
                                                   style={{right: '4px'}}/>
                                        }
                                        {!this.state[cat.id + 'Open'] && !isFiltering &&
                                        <ChevronDown size={22} className='abs jItemBG round pointer hoh'
                                                     style={{right: '4px'}}/>
                                        }
                                        {badgeCount > 0 &&
                                        <DumbBadge count={badgeCount} className='abs'
                                                   style={{top: '-5px', right: this.state[cat.id + 'Open']? '2px':'-5px', transition: 'right .2s'}}/>
                                        }
                                    </div>
                                    }

                                    <div className={'pinList paneContent ' + (this.state[cat.id + 'Open'] || isFiltering? 'exp' : 'col')} style={{paddingLeft: '1rem'}}>
                                        {(this.state[cat.id + 'Open'] || isFiltering) && cat.projectIds.map ((projectId, i) => {
                                            if (!projectId)
                                                return null

                                            project = projectStore.getProjectFromUserProjects (projectId)
                                            if (!project) // it may have been deleted, without refreshing user projects
                                                return null

                                            projectBadgeDetails = defaultBadgeInfo && defaultBadgeInfo.projects && defaultBadgeInfo.projects[projectId] ? defaultBadgeInfo.projects[projectId] : null
                                            badgeCount = projectBadgeDetails ? projectBadgeDetails.total : 0
                                            isNew = projectBadgeDetails ? Boolean (projectBadgeDetails.new) : false
                                            isSelected = projectStore.currentProject && projectId === projectStore.currentProject.id

                                            return <ProjectThumb project={project} badgeCount={badgeCount} isNew={isNew}
                                                                 key={project.domId}
                                                                 isSelected={isSelected}/>
                                        })}
                                    </div>
                                </div>
                            ) : null
                        })}
                    </div>
                </div>
                }
            </div>
        )
    }
}