import {Component} from 'react'
import {observer} from 'mobx-react'
import PDWTagFilter from "./PDWTagFilter";
import TagCreator from "/components/common/TagCreator";
import {findDOMNode} from 'react-dom'

import tagStore from '/store/tagStore'
import uiStateStore from '/store/uiStateStore'

@observer
export default class PDWTagToolbar extends Component {

    constructor(props) {
        super (props)
        let activeTags = {}
        // this.props.tags.forEach (tag => activeTags[tag] = false)
        this.state = {activeTags: activeTags}
    }

    // When user edits a cardable tag within its project tagset, update the project's cardable tag set.
    saveTag(newName, newColor, tagObj) {
        if (!tagObj || newName !== tagObj.name)
            this.setState({loading: true})
        if (!tagObj) {
            tagStore.addTagToCardableSet(this.props.project.id, newName, newColor).then( r => this.setState({loading: false}) )
        }
/*      creation only for starters
        else {
            tagObj.name = newName
            tagObj.color = newColor
            tagStore.updateTagInCardableSet(this.props.project.id, tagObj).then( r => this.setState({loading: false}) )
        }
*/

    }
    getActiveTagIds() {
        return Object.keys(this.state.activeTags)
    }

    countActiveTags() {
        return Object.keys(this.state.activeTags).length
    }

    toggleTag(tagObj) {
        const isActive = this.state.activeTags[tagObj.id] !== undefined
        if (isActive) {
            delete this.state.activeTags[tagObj.id]
        } else
            this.state.activeTags[tagObj.id] = true
        this.setState ({activeTags: this.state.activeTags}) // to render

        this.props.filterByTags (this.getActiveTagIds())
    }

    clearFilters = () => {
        this.props.filterByTags (null)
        this.setState({activeTags: {}})
    }

    componentDidMount() {
        const elem = findDOMNode(this)
        elem.onmousewheel = (ev) => {
            elem.scrollLeft += (ev.deltaY * 5)
        }
    }

    componentWillUnmount() {
        const elem = this.state.elem || findDOMNode(this)
        if (elem)
            elem.onmousewheel = null
    }

    componentWillReact() {
        console.log('Tag Toolbar will react.')
    }

    render() {
        let trimmedTags = tagStore.currentTagSet // this.props.tags //.filter( tag => TagConfig.DefaultProjectTags[tag] !== undefined )
        let availTags = trimmedTags.length
        const tagWidth = 100 / (availTags+2)
        // const charsPerTag = 100 / n
        const isFilteringByTags = Object.keys(this.state.activeTags).length > 0

        const activeTagCount = this.countActiveTags()
        // if (availtags === 0 || (!tagStore.areTaggedCardsOnDisplay() && !isFilteringByTags))
        //     return null
        if (uiStateStore.editingCard !== null)
            return null

        return(
            <div className="top-pane toolbar-tags small dontWrap subtle-scroll-dark" style={{overflowX: 'auto'}}>
                {activeTagCount === 0 && trimmedTags.length > 0 &&
                <span className="tagBtn header">Show Only: </span>
                }
                {activeTagCount > 0 &&
                <span className=" tagBtn header reset" style={{marginLeft: '.5rem'}} onClick={this.clearFilters}>Show All</span>
                }

                {trimmedTags.map ( (tagObj, index) =>
                    <PDWTagFilter tag={tagObj} width={tagWidth}
                                  key={tagObj.name+index}
                                  index={index+1}
                                  filteringIsActive={activeTagCount > 0}
                                  active={this.state.activeTags[tagObj.id] !== undefined}
                                  // nameLength={charsPerTag}
                                  toggleTag={(tagObj) => this.toggleTag(tagObj)}/>
                )}
                <TagCreator createTag={(newTag) => this.saveTag(newTag)}
                            project={this.props.project} defaultOpenForm={trimmedTags.length === 0}
                />

            </div>
        )
    }
}