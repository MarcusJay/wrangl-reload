import projectStore from '/store/projectStore'
import {ProjectType, UserRole, UserStatus} from "../../../config/Constants";
import RU from "../../../utils/ResponsiveUtils";
import userStore from "../../../store/userStore";
import MemberAvatar from "../../common/MemberAvatar";

const MAX_MEMBERS_DISPLAY = 5

const refreshMembers = (ev) => {
    if (projectStore.currentProject)
        projectStore.fetchCurrentProject(null, null, false).then( response => {

        })
}

const BoardMembers = () => {

    const max = RU.getDeviceType() === 'computer'? MAX_MEMBERS_DISPLAY : MAX_MEMBERS_DISPLAY - 2
    const activeUser = userStore.currentUser
    const project = projectStore.currentProject
    const requestor = project && project.projectType === ProjectType.APPROVAL?
        userStore.getUserFromConnections(project.creatorId) : null
    const validMembers = project? project.members.filter( user =>
        [UserRole.BANNED, UserRole.INACTIVE_USER, UserRole.INACTIVE_ADMIN].indexOf(user.role) === -1 &&
         user.status !== UserStatus.ANON) : []
    let latestMembers = validMembers.slice(0, MAX_MEMBERS_DISPLAY)
    if (requestor !== null)
        latestMembers = latestMembers.filter (user => user.role === UserRole.APPROVER)

    const memberCount = project? (project.memberTotal + project.invitedMemberTotal) : 0
    const moreCount = (memberCount > max)? memberCount - max : 0

    return (
        <div className='ml1 mt05' >
        {latestMembers
            .sort ((a,b) => { return a.displayName.toLowerCase().localeCompare(b.displayName.toLowerCase())})
            .map( (mbr,i) => {
                return (
                    <MemberAvatar member={mbr}
                                  activeUser={activeUser}
                                  enableAdmin={true}
                                  project={project}
                                  size='32px'
                                  refresh={refreshMembers}
                                  key={mbr.id}
                                  index={i}
                                  showName={false}
                                  isEnabled={true}
                                  showMenu={true}
                    />
                )
            })
        }
        {moreCount > 0 &&
        <span className='jSec mr05'>+ {moreCount}</span>
        }

        </div>
    )
}

export default BoardMembers