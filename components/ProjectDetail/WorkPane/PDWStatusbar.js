import {Component} from 'react'
import {observer} from 'mobx-react'
import uiStateStore from '/store/uiStateStore'
import CardSizeSlider from "/components/common/CardSizeSlider"
import {isBrowser} from "../../../utils/solo/isBrowser";
import TileSizeSlider from "../../common/TileSizeSlider";

@observer
export default class PDWStatusbar extends Component {
    constructor(props) {
        super(props)
        this.state = {a:1}
    }

    componentDidMount() {
        const self = this
        if (isBrowser()) {
            window.addEventListener ("orientationchange", function () {
                console.log('Orientation change')
                self.forceUpdate()
            });
        }

    }

    render() {
        const leftCols = uiStateStore.voMode? 13 : 10
        const rightCols = uiStateStore.voMode? 3 : 6

        if (uiStateStore.editingCard || uiStateStore.viewingCard)
            return null

        return(
            <footer className="bottom-pane jItemBG" >
                {uiStateStore.cardView === 'cards' && !uiStateStore.leftUnModal &&
                <CardSizeSlider cardContainer="main"/>
                }
                {uiStateStore.cardView === 'tiles' &&
                <TileSizeSlider cardContainer="main"/>
                }
            </footer>
        )
    }
}