import {Component} from 'react'
import PropTypes from 'prop-types'

import {DragSource} from 'react-dnd'
import {DndItemTypes} from '~/config/Constants'
import ColorUtils from '/utils/ColorUtils'
import uiStateStore from "../../../store/uiStateStore";

const tagSource = {
    beginDrag(props) {
        uiStateStore.enableFileDrops = false
        return {
            tag: props.tag.name,
            id: props.tag.id
        }
    },
    endDrag(props, monitor) {
        uiStateStore.enableFileDrops = true
        return {
            tag: props.tag.name,
            id: props.tag.id
        }
    }
}

@DragSource(DndItemTypes.TAG, tagSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
}))
export default class PDWTagFilter extends Component {
    static propTypes = {
        connectDragSource: PropTypes.func,
        isDragging: PropTypes.bool,
        id: PropTypes.any,
        tag: PropTypes.any,
        tagColor: PropTypes.any
    }

    constructor(props) {
        super(props)
        this.state = {showColor: false, showFull: false}
    }

    toggleTag() {
        this.props.toggleTag(this.props.tag)
    }

    showColor(show) {
        this.setState({showColor: show, showFull: show})

    }

    render() {
        const { isDragging, connectDragSource } = this.props
        const tagClass = 'tagBtn ' + (this.props.active? 'active' : isDragging? 'dragging' : 'inactive')
        const tagColor = '#' +this.props.tag.color
        const opacity = (this.props.filteringIsActive && !this.props.active)? 0.3 : 1
        const filter = (this.props.filteringIsActive && !this.props.active)? 'saturate(0.5) brightness(0.8)' : ''
        const textColorClass = ColorUtils.isDark(tagColor)? 'lightOnDark' : 'darkOnLight'
        const showColor = true // this.state.showColor || this.props.active || isDragging
        const showFull = this.state.showFull

        return connectDragSource(
            <span
                style={{
                opacity: opacity,
                filter: filter,
                backgroundColor: showColor? tagColor : 'transparent'
            }}
                  className={tagClass +(showColor? ' '+textColorClass:'')}
                  onMouseEnter={() => this.showColor(true)} onMouseLeave={() => this.showColor(false)}
                  onClick={() => this.toggleTag()}
                  /*data-tip={this.props.tag.name} data-offset="{'top':-48}" data-class="tagTip"*/

            >
                {this.props.tag.name}
                {/*<div className='underStrip' style={{backgroundColor: tagColor}}>&nbsp;</div>*/}
            </span>
        )
    }
}

{/*
<span style={{width: this.props.width+'%', backgroundColor: tagColor}} className={tagClass +' '+textClass}
*/}
