import {Component} from 'react'
import {observer} from 'mobx-react'
import ReactTooltip from 'react-tooltip'
import {Link} from '~/routes'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import {UserRole} from '/config/Constants'

import StateChangeRequestor, {REQUEST_PROJECT_ADD_USER} from '/services/StateChangeRequestor'

import Toaster from '/services/Toaster'
import ArrayUtils from '/utils/ArrayUtils'
import projectStore from '/store/projectStore'
import userStore from '/store/userStore'
import uiStateStore from '/store/uiStateStore'
import MemberAvatar from "../../common/MemberAvatar";
// import {DropTarget} from 'react-dnd'
import {getAddUserMsg} from "/utils/MessageUtils";
import {EventSource} from "/config/EventConfig";
import {addMember} from "/models/ProjectModel";
import RU from "/utils/ResponsiveUtils";
import DraggableMemberAvatar from "../../common/DraggableMemberAvatar";
import {DndItemTypes, ProjectType, UserStatus} from "../../../config/Constants";
import {Icon, Popup} from "semantic-ui-react";
import {ArrowRight, ChevronDown, ChevronRight} from "react-feather";
import UserSearchResult from "../../contacts/UserSearchResult";
import UserSearch from "../../contacts/UserSearch";
import ProjectApiService from "../../../services/ProjectApiService";

const MAX_MEMBERS_DISPLAY = 10

const paneTarget = {
    drop(props, targetMonitor, component) {
        let dragItem = targetMonitor.getItem()

        if (!props.project)
            return null

        // add user
        if (dragItem.type === DndItemTypes.PERSON) {
            let newMemberId = dragItem.id

            const existingUser = props.project.members.find(member => member.id === newMemberId )
            if (existingUser !== undefined) {
                let msg = getAddUserMsg( existingUser.role )
                Toaster.info(msg, {user: existingUser, project: props.project}, 1)
                return
            }

            // This pattern works, but would require refreshing entire workpane to update. No return values from props functions.
            // props.addUserToProject(props.project.id, userId)
            // end

            StateChangeRequestor.awaitStateChange(REQUEST_PROJECT_ADD_USER,
                {projectId: props.project.id, userId: userStore.currentUser.id, newMemberId: newMemberId, trigger: EventSource.InApp, newMemberRole: UserRole.INVITED}).then (response => {
                const newMember = userStore.getUserFromConnections(newMemberId)
                addMember( newMember, props.project)
                // let msg = getAddUserMsg()
                // Toaster.info(msg, {user: newMember, project: props.project}, 1)
                component.forceUpdate()
            })

        }
    }
}

/*
@DropTarget([DndItemTypes.PERSON], paneTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    item: monitor.getItem()
}))
*/
@observer
export default class PDWToolbar extends Component {
    constructor(props) {
        super(props)
        this.state = { showMembers: false, showMemberName:null }
    }

    overUser = (user) => {
        this.setState({showMemberName: user.displayName || user.firstName})
    }

    clearUser = () => {
        this.setState({showMemberName: null})
    }

    addUserToBoard = (user) => {
        const project = projectStore.currentProject
        ProjectApiService.addUsersToProject(project.id, null, [user.id], EventSource.ProjectAdd, UserRole.INVITED)
            .then(response => this.refreshMembers())
    }

    attachUser = (user) => {
        // TODO
    }

    unattachUser = (msgId, user) => {
        // TODO
    }

    refreshMembers = (ev) => {
        if (projectStore.currentProject)
            projectStore.fetchCurrentProject(null, null, false).then( response => {

            })
    }

    filterCommentsByCard = (ev) => {
        uiStateStore.filterDiscussionByCard = !uiStateStore.filterDiscussionByCard
    }

    toggleDiscussion = (ev) => {
        uiStateStore.showDiscussion = !uiStateStore.showDiscussion
    }


    closeMobSidebarR = (ev) => {
        uiStateStore.showMobSidebarR = false
    }

    // Although this component uses projectStore directly, we want to compare incoming props to old props to animate changes.
    // TODO refactor to componentDidUpdate or similar safe lifecycle call
    componentWillReceiveProps(nextProps) {
        console.log('PDWDiscuss Toolbar will receive props')
        const newProject = nextProps.project
        const oldProject = this.props.project
        if (!oldProject || !newProject)
            return

        const membersChanged =
            (newProject.memberTotal !== oldProject.memberTotal) ||
            (newProject.invitedMemberTotal !== oldProject.invitedMemberTotal)

        const changedMembers = ArrayUtils.arrayDifference(newProject.members, oldProject.members) || []
        changedMembers.forEach( member => member.isChanged = true)

        this.setState({
            membersChanged: membersChanged
        })
    }

    componentDidUpdate() {
        ReactTooltip.rebuild();
    }

    render() {
        const project = projectStore.currentProject
        const miniView = !uiStateStore.showDiscussion
        const isPF = uiStateStore.currentPage === uiStateStore.PF
        const {showMemberName} = this.state
        const { isOver, item, filterBy, friend, connectDropTarget, mobile} = this.props
        const showMembers = uiStateStore.showMembers && !miniView
        const max = RU.getDeviceType() === 'computer'? MAX_MEMBERS_DISPLAY : MAX_MEMBERS_DISPLAY - 2
        // if (!project)
        //     return null

        const requestor = project && project.projectType === ProjectType.APPROVAL?
              userStore.getUserFromConnections(project.creatorId) : null
        const validMembers = project? project.members.filter( user =>
            [UserRole.BANNED, UserRole.INACTIVE_USER, UserRole.INACTIVE_ADMIN].indexOf(user.role) === -1 &&
             user.status !== UserStatus.ANON) : []
        let latestMembers = validMembers.slice(0, max)
        if (requestor !== null)
            latestMembers = latestMembers.filter (user => user.role === UserRole.APPROVER)

        const memberCount = project? (project.memberTotal + project.invitedMemberTotal) : 0
        const moreCount = (memberCount > max)? memberCount - max : 0

        const iconFlash = (!showMembers && this.state.membersChanged)? ' flashAndFade':''

        const newMember = (isOver && item)? userStore.getUserFromConnections(item.id) : null
        const activeUser = userStore.currentUser
        const activeUserInProject = project && activeUser? project.members.find( member => member.id === activeUser.id) : null
        const enableAdmin = activeUserInProject? activeUserInProject.role === UserRole.ADMIN : false

        const isApproval = project && project.projectType === ProjectType.APPROVAL
        const paneClass = 'utOpenConvo top-pane' + (isApproval? ' borderB' : '')
        const filterSize = '24px'
        const filterOpacity = uiStateStore.filterDiscussionByCard? 1 : 0.7
        const filterBorder = uiStateStore.filterDiscussionByCard? '2px solid #333' : '2px solid #888'
        const barColorClass = uiStateStore.filterDiscussionByCard? 'jYellowBG':'jBlueBG'


        if (miniView) {
            return (
                <div className="top-pane tool-pane mini-toolbar" style={{paddingLeft: '1.5rem'}}>
                        {/*<User size={24} className='third subtleBtn absToolBtn' data-tip="Show Discussion" onClick={this.toggleDiscussion}/>*/}
                </div>
            )
        }

        return ( // connectDropTarget(
            <div className={paneClass} style={{maxHeight: isApproval? '44px' : ''}}>
                <div className={'toolbar-menu relative toolbar-aux hidden-scroll ' +barColorClass}
                     style={{padding:'0.3rem 0.5rem .4rem 1.5rem', overflowX: 'auto',
                         transition: 'background-color 0.2s ease-out'}}>
                    {!mobile && !isApproval && !uiStateStore.showUserPopup && !uiStateStore.showCardPopup &&
                    <div className='chdr pointer' >
                        <div className='abs paneCtrls'>
                            <ChevronRight size={22} className='utCloseConvo'
                                          color={uiStateStore.filterDiscussionByCard? '#333':'white'}
                                          onClick={this.toggleDiscussion}
                                          data-tip='Hide Comments'/>
                        </div>
                    </div>
                    }

                    {uiStateStore.filterDiscussionByCard && uiStateStore.viewingCard &&
                    <div className="discussAvatars " style={{marginTop: '.25rem'}}>
                        <div className='inline pointer cover'
                             style={{backgroundImage: 'url(' + uiStateStore.viewingCard.image+ ')',
                                 width: filterSize,
                                 height: filterSize,
                                 opacity: filterOpacity,
                                 border: filterBorder,
                                 borderRadius: '4px',
                                 margin: '4px .5rem 4px 0'
                             }}
                        >&nbsp;</div>
                        <span className='jFGinv ml1 bold'>Item Comments</span>
                    </div>
                    }

                    {!uiStateStore.filterDiscussionByCard &&
                    <div className="discussAvatars " style={{marginTop: '.25rem', opacity: uiStateStore.showMembers ? 1 : 0}}>
                        {false && !isPF &&
                            <Popup on='click'
                                   position='bottom right'
                                   trigger={
                                       <span className="inline hoh pointer bold small jFG">
                                           <Icon name="dropdown" size="large" className='dropdownArrow'/>
                                       </span>
                                   }
                            >
                                <div className='jFG jBG small'>
                                    <div className='italic jSec' style={{marginBottom: '-1rem'}}>
                                        Add new members
                                    </div>
                                    <div className='mb1'>
                                        <UserSearch placeholder='Find people...' addToBoard/>
                                    </div>

                                    <p className='small mb1'>Current Members:</p>
                                    {validMembers
                                        .sort ((a,b) => { return a.displayName.toLowerCase().localeCompare(b.displayName.toLowerCase())})
                                        .map( (mbr,i) => {
                                            return (
                                                <div className='mb05'>
                                                    <MemberAvatar member={mbr}
                                                                  activeUser={activeUser}
                                                                  enableAdmin={false}
                                                                  project={project}
                                                                  refresh={this.refreshMembers}
                                                                  key={'allm'+i}
                                                                  style={{opacity: '1 !important', margin: '0 .5rem .5rem 0'}}
                                                                  index={i}
                                                                  color="black"
                                                                  showName={false}
                                                                  isEnabled={false}
                                                                  showMenu={false}
                                                    />
                                                    <span>
                                                        {mbr.displayName || mbr.firstName || mbr.email}
                                                    </span>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </Popup>
                        }

                        <span className='inline jFG'
                              style={{height: '24px', margin: '4px .5rem 4px 0'}}>
                            All Comments
                        </span>
                        </div>
                    }

{/*
                    {filterBy &&
                    <div className='abs' style={{top: '.75rem', right: filterCardRight}}>
                        <div className='inline pointer cover'
                             style={{backgroundImage: 'url(' + filterBy.image+ ')',
                                 width: filterSize,
                                 height: filterSize,
                                 opacity: filterOpacity,
                                 border: filterBorder,
                                 borderRadius: '4px',
                                 marginTop: '-8px'
                             }}
                             onClick={this.filterCommentsByCard}
                        >&nbsp;</div>
                    </div>
                    }
*/}

                    {mobile &&
                    <span onClick={this.closeMobSidebarR} >
                        <ArrowRight size={26} className='jFG abs hoh pointer' style={{top: '.5rem', right: 0}}/>
                    </span>
                    }


                </div>

            </div>
        )
    }
}