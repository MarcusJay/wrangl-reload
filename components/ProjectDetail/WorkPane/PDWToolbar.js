import {Component} from 'react'
import {observer} from 'mobx-react'
import {Link, Router} from '~/routes'
import {findDOMNode} from 'react-dom'
import {Menu, Popup} from 'semantic-ui-react'
import Responsive from 'react-responsive'

import ReactTooltip from 'react-tooltip'

import CardModel from '/models/CardModel'
import ImportModal from '/components/modals/ImportModal'
import CardSearch from '/components/common/CardSearch'

import UserSessionUtils from '/utils/UserSessionUtils'
import uiStateStore, {ProjectSettings} from '/store/uiStateStore'
import projectStore from '/store/projectStore'
import cardStore from '/store/cardStore'

import 'rc-tooltip/assets/bootstrap.css'
import ShareModal from "../../modals/ShareModal";
import ExportService from "../../../services/ExportService";
import {CardViewer, ExportPreview, ImportContent, SendForApproval} from "../../../store/uiStateStore";
import Toaster from "../../../services/Toaster";
import MoreHoriz from "../../common/MoreHoriz";
import userStore from "../../../store/userStore";
import {ProjectType, UserStatus} from "../../../config/Constants";
import {Info, Settings} from "react-feather";
import {Moment} from "react-moment";
import {getUserApprovals, leaveApproval} from "../../../services/ApprovalService";
// import UploaderComponent from '/components/common/UploaderComponent'

@observer
export default class PDWToolbar extends Component {
    constructor(props) {
        super (props)
        this.openNewCardEditor = this.openNewCardEditor.bind (this)
        this.closeNewCardEditor = this.closeNewCardEditor.bind (this)

        this.saveCard = this.saveCard.bind (this)
        this.importCards = this.importCards.bind (this)
        this.createCards = this.createCards.bind (this)
        this.importAssets = this.importAssets.bind (this)
        this.uploadAssets = this.uploadAssets.bind (this)
        this.handleCardSearchResults = this.handleCardSearchResults.bind (this)

        this.state = {
            // cardView: uiStateStore.cardView, // this.props.view || 'cards',
            card: null,
            cardQuery: '',
            searchResults: false,
            showEditor: false,
            activeDropdownItem: '',
            showLeaveConfirmation: false,
            showDeleteConfirmation: false,
            pdfError: null,
            showPdfError: false
        }

    }

    viewCards() {
        uiStateStore.setCardView ('cards')
        ReactTooltip.rebuild ()
    }

    toggleNotes() {
        if (uiStateStore.cardView === 'comments') {
            this.viewCards ()
            return
        }

        uiStateStore.loadingNotes = true
        cardStore.getAllCardDetailsInCurrentProject ().then (cards => {
            uiStateStore.setCardView ('comments')
            ReactTooltip.rebuild ()
            uiStateStore.loadingNotes = false
        })
    }

    viewTiles() {
        uiStateStore.setCardView ('tiles')
    }

    toggleInfo() {
        uiStateStore.toggleStickyInfo ()
    }

    viewImages() {
        uiStateStore.setCardView ('images')
    }

    // TODO Migrate most of these to mobx
    // Note this also means move UserSessionPrefs here
    toggleAuthors() {
        uiStateStore.showAuthors = !uiStateStore.showAuthors
        UserSessionUtils.setSessionPref ('showAuthors', uiStateStore.showAuthors)
    }

    toggleDescriptions() {
        uiStateStore.showDescriptions = !uiStateStore.showDescriptions
        UserSessionUtils.setSessionPref ('showDescriptions', uiStateStore.showDescriptions)
    }

    toggleExtensions() {
        uiStateStore.showExtensions = !uiStateStore.showExtensions
        UserSessionUtils.setSessionPref ('showExtensions', uiStateStore.showExtensions)
    }

    toggleTags = (ev) => {
        uiStateStore.showTags = !uiStateStore.showTags
        // uiStateStore.showTagBar = !uiStateStore.showTagBar
        this.props.toggleTags ()
    }

    toggleStacks = (ev) => {
        uiStateStore.showStacks = false // !uiStateStore.showStacks
        if (uiStateStore.showStacks)
            uiStateStore.showTags = true
    }

    toggleLetters() {
        this.props.toggleLetters ()
    }

    toggleActions() {
        uiStateStore.showActions = !uiStateStore.showActions
        UserSessionUtils.setSessionPref ('showActions', uiStateStore.showActions)
    }

    toggleCrop = () => {
        // uiStateStore.cropImages = !uiStateStore.cropImages
        // UserSessionUtils.setSessionPref ('cropImages', uiStateStore.cropImages)
    }

    toggleVO() {
        uiStateStore.voMode = !uiStateStore.voMode
        ReactTooltip.rebuild ()
    }

    toggleDiscussion() {
        // uiStateStore.showDiscussion = !uiStateStore.showDiscussion
        ReactTooltip.rebuild ()
    }

    openNewCardEditor() {
        this.setState ({showEditor: true, card: new CardModel ()})
    }

    closeNewCardEditor() {
        this.setState ({showEditor: false, card: null})
    }

    projectSettings = () => {
        projectStore.settingsProject = this.props.project
        uiStateStore.unModal = ProjectSettings
    }

    saveCard(card) {
        // console.log('new card title: ' +card.title)
        this.props.saveCard (card)
        this.closeNewCardEditor ()

    }

    createCards(cards) {
        this.props.createCards (cards)
    }

    importCards(cards) {
        this.props.importCards (cards)
    }

    importAssets(assets, source) {
        this.props.importAssets (assets, source)
    }

    uploadAssets(cards, files) {
        this.props.uploadAssets (cards, files)
    }

    sendforApproval = () => {
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon)
            uiStateStore.setJoinPromptState(true)

        else if (uiStateStore.rightUnModal === null) {
            userStore.selectedUserIds = []
            cardStore.selectedCardIds = []
            uiStateStore.showDiscussion = true
            uiStateStore.rightUnModal = SendForApproval
        }
        else {
            uiStateStore.rightUnModal = null
            uiStateStore.approvalSent = false
        }
    }

    onOptionsMenuOpen = (ev) => {
    }

    onBoardMenuOpen = (ev) => {
    }

    handleMenuItemSelect(ev, {name}) {
        ev.stopPropagation ()
        ev.preventDefault ()
        this.setState ({activeDropdownItem: name})
    }

    handleCardSearchResults(results) {
        this.props.handleCardSearchResults (results)
    }


    openDefaultCardViewer = () => {
        uiStateStore.setViewingCard (null)
        uiStateStore.unModal = CardViewer
    }

    openImportUnmodal = () => {
        projectStore.importProject = this.props.project
        uiStateStore.unModal = ImportContent
    }

    openConvo = (ev) => {
        uiStateStore.showMobSidebarR = true
    }

    markAsDone = () => {
        const project = projectStore.currentProject
        const isApproval = project && project.projectType === ProjectType.APPROVAL

        if (!isApproval)
            return

        leaveApproval(project.id).then( response => {
            getUserApprovals(userStore.currentUser.id).then( response => {
                Router.pushRoute('/')
            })
        }).catch(error => {
            console.error(error)
        })
    }

    render() {
        const {activeDropdownItem, showLeaveConfirmation, showDeleteConfirmation} = this.state
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON

        const {project, mobile} = this.props
        if (!project)
            return null
        const isApproval = project && project.projectType === ProjectType.APPROVAL
        const isOwner = userStore.currentUser && project.creatorId === userStore.currentUser.id
        const showTags = Boolean(this.props.showTags)
        const cards = project ? project.cards : []
        const cardCount = cards.length
        const title = project ? project.title : 'Project'
        const view = uiStateStore.cardView
        const enableBulkActions = cardStore.selectedCardIds && cardStore.selectedCardIds.length > 0

        const creator = project? userStore.getUserFromConnections(project.creatorId) : null
        const created = project? project.dateCreated : null
        const modified = project? project.dateModified : null

        return (
            <div className="utHelpEnd top-pane workCol jItemBG" style={{paddingLeft: '1.5rem'}}>
                <div className='toolbar-menu toolbar-aux relative'>

                    {!uiStateStore.voMode &&
                    <ImportModal project={project} importCards={this.importCards} createCards={this.createCards}
                                 importAssets={this.importAssets} uploadAssets={this.uploadAssets}/>
                    }


{/*
                    {!mobile && cardCount > 0 && !uiStateStore.viewingCard &&
                    <img className='boh pointer mr05' src='/static/svg/v2/viewGallery.svg' width={30}
                         data-tip={uiStateStore.cropImages? 'Images fit into card':'Images fill card'}
                         onClick={this.toggleCrop}
                    />
                    }
*/}

                    {cardCount > 0 &&
                    <Responsive minWidth={767}
                                data-tip="Filter Cards by Title..." style={{marginLeft: '1rem'}}>
                        <CardSearch {...this.props} handleResults={this.handleCardSearchResults}/>
                    </Responsive>
                    }

                    <div className='abs' style={{top: '.5rem', right: '0.1rem'}}>

                        {cardCount > 0 &&
                        <span data-tip="Share this board">
                            <ShareModal project={project}

                                        connections={this.props.connections}
                                        user={this.props.user}
                            />
                        </span>
                        }

                        {!mobile && cardCount > 0 && !isApproval &&
                        <button className={'utRequest jBtn jFG micro lighter mr1 krn1 vaTop' + (enableBulkActions ? '' : ' disabled')}
                                style={{minWidth: '5rem', marginTop: '.15rem'}}
                                onClick={this.sendforApproval}
                                data-tip="Request approval on items from this board"
                        >
                            REQUEST FEEDBACK
                        </button>
                        }

                        {isApproval && 
                        <button className={'utRequest jBtn micro lighter mr05 krn1 vaTop' + (enableBulkActions ? '' : ' disabled')}
                                style={{minWidth: '5rem', marginTop: '.15rem'}}
                                onClick={this.markAsDone}
                                data-tip="Mark this Approval as Done / Completed."
                        >
                            MARK AS DONE
                        </button>
                        }



                        {mobile &&
                            <img src='/static/svg/v2/messageCircle.svg' className='pointer mr05' width={30} onClick={this.openConvo}/>
                        }

                        {!isAnon && !mobile &&
                        <Settings size={22} className='jSec pointer' style={{marginBottom: '.25rem', marginTop: '4px'}}
                                  onClick={this.projectSettings}
                                  data-tip='More actions'/>
                        }
                    </div>

                </div>

            </div>
        )
    }
}