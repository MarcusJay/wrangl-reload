import {Component} from 'react'
import PropTypes from 'prop-types'
import {findDOMNode} from 'react-dom'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import {observer} from 'mobx-react'
import Dropzone from 'react-dropzone'

import {Confirm, Dimmer, Loader} from 'semantic-ui-react'

import Card from '../../common/Card'
// import UploaderComponent from '/components/common/UploaderComponent'
import CardModel from '/models/CardModel'
import cardStore from '/store/cardStore'
import projectStore from '/store/projectStore'
import ProjectApiService from '/services/ProjectApiService'
import uiStateStore from '/store/uiStateStore'
import eventStore from '/store/eventStore'
import {getShortUrl} from '/utils/solo/getShortUrl'

import {DropTarget} from 'react-dnd'
import {DndItemTypes} from '/config/Constants'
import UserSessionUtils from '/utils/UserSessionUtils'
import {AddCardToProject, CardDelete, CreateCard} from '/config/EventConfig'
import {indexToCardLabel} from "../../../utils/solo/indexToCardLabel";
import ResponsiveUtils from "../../../utils/ResponsiveUtils";
import {AcceptedDropTypes, FileTypes, ImageTypes} from "../../../config/MimeTypes";
import {isUrl} from "../../../utils/solo/isUrl";
import {requestPreviews} from "../../../services/ImageFileService";
import CardCreator from "../../common/Card/CardCreator";
import {SendForApproval} from "../../../store/uiStateStore";
import {DEFAULT_CAT, UserRole, UserStatus} from "../../../config/Constants";
import {addCardsToApproval} from "../../../services/ApprovalService";
import userStore from "../../../store/userStore";
import {EventSource} from "../../../config/EventConfig";
import TourFirstProject from "../../help/TourFirstProject";
import tagStore from "../../../store/tagStore";
import CardStack from "../../common/Card/CardStack";
import categoryStore from "../../../store/categoryStore";
import isInViewport from "../../../utils/solo/isInViewport";

const FIVE_MB = 5242880
const TEN_MB = 10485760
const TWENTY_MB = 20971520

const paneTarget = {
    drop(props, targetMonitor, component) {
        let dragItem = targetMonitor.getItem()

        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon) {
            uiStateStore.setJoinPromptState(true)
            return
        }

        // add card to project
        if (dragItem.type === DndItemTypes.CARD_THUMB) {
            props.importCards([dragItem.id])
        }

        // add user to project
        if (dragItem.type === DndItemTypes.PERSON) {
            component.addMember(dragItem.id)
        }

        // add dropbox item to project
        else if (dragItem.type === DndItemTypes.DROPBOX_THUMB) {
            props.importAssets([dragItem.asset], 'dropbox')
        }

        // add user
/*
        else if (dragItem.type === DndItemTypes.PERSON) {
            let newMemberId = dragItem.id

            const existingUser = props.project.members.find(member => member.id === newMemberId )
            if (existingUser !== undefined) {
                let msg = getAddUserMsg( existingUser.role )
                Toaster.info(msg, {user: existingUser, project: props.project}, 1)
                return
            }

            // This pattern works, but would require refreshing entire workpane to update. No return values from props functions.
            // props.addUserToProject(props.project.id, userId)
            // end

            StateChangeRequestor.awaitStateChange(REQUEST_PROJECT_ADD_USER,
                {projectId: props.project.id, userId: userStore.currentUser.id, newMemberId: newMemberId, trigger: EventSource.InApp, newMemberRole: UserRole.INVITED}).then (response => {
                const newMember = userStore.getUserFromConnections(newMemberId)
                addMember( newMember, props.project)
                let msg = getAddUserMsg()
                Toaster.info(msg, {user: newMember, project: props.project}, 1)
                component.forceUpdate()
            })

        }
*/
    }
}

@DropTarget([DndItemTypes.CARD_THUMB, DndItemTypes.PERSON, DndItemTypes.DROPBOX_THUMB], paneTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    item: monitor.getItem()
}))
@observer
export default class PDWCardsPane extends Component {
    static propTypes = {
        connectDropTarget: PropTypes.func,
        isOver: PropTypes.bool,
        item: PropTypes.any
    }

    constructor(props) {
        super(props)

        let fileRefs

        this.state = {
            // mutating mobx cards on drag is slow. Let's modify local copy, then persist when drag done.
            // localCards: cardStore.isFiltering? cardStore.filteredCards : cardStore.currentCards,
            isHovering: false,
            isReactingToPush: false,
            dropzoneActive: false,
            elem: null,
            prefCardSize: uiStateStore.defaultCardSize, // UserSessionUtils.getSessionPref('cardSize.cardViewsWorkPane') || null,
            expandedCardId: null,
            uploadCards: [],
            openCard: null,
            openCardIndex: -1,
            showEditor: false,
            loading: false,
            uploading: false,
            uploadCount: 0,
            completedCount: 0,
            uploadSize: 0,
            showSizeWarning: false,
            showSizeExceeded: false,
            showUploadError: false,
            showUnseenEvents: true
        }
        console.log("PDWCardsPane: I've got " +cardStore.currentCards.length+ ' cards.')

        this.updateDimensions = this.updateDimensions.bind(this)
        this.moveCard = this.moveCard.bind(this)
        this.hoverCard = this.hoverCard.bind(this)
        this.saveCard = this.saveCard.bind(this)
        this.deleteCard = this.deleteCard.bind(this)
        this.onExpand = this.onExpand.bind(this)
        this.onShrink = this.onShrink.bind(this)
        this.uploadFiles = this.uploadFiles.bind(this)

    }

    closeSize = () => {
        this.setState({showSizeExceeded: false})
    }
    closeError = () => {
        this.setState({showUploadError: false})
    }

    addMember(newMemberId) {
        const projectId = this.props.project? this.props.project.id : null
        ProjectApiService.addUsersToProject(projectId, userStore.currentUser.id, [newMemberId], EventSource.ProjectAdd, UserRole.INVITED).then( response => {
            this.props.refreshMembers()
        })

    }

    // TODO consider moving this and comment's version of this into parent PDWorkPane
    updateDimensions() {
        return

/*
        if (!isBrowser())
            return

        const elem= this.state.elem
        const dz = elem.firstChild
        if (!elem) {
            console.warn('UpdateDimensions before dom element is set.')
            return
        }

        let height = (window.innerHeight - RU.getHeightInUse() )
        elem.style.height = height+'px'
        dz.style.height = height+300+'px' // elem.scrollHeight +'px' <-- this results in enormous height, stretching out pane. TODO consider an overlay.
*/

    }

    // React-Dropzone next 3
    onDrop(files, unsupportedTypes) {
        this.fileRefs = files
        let urlsDropped = false

        if (unsupportedTypes.length > 0) {
            unsupportedTypes.forEach( (file, i) => {
                if (file instanceof DataTransferItem && file.type === 'text/uri-list') {
                    urlsDropped = true
                    file.getAsString((url) => {
                        this.setState({uploadCount: i, dropzoneActive: false, uploading: true})
                        cardStore.createCardFromUrl(url).then( cardResponse => {
                            this.props.refreshCards ()
                            this.setState ({uploading: false, uploadCards: []})
                        })
                    })
                }
            })

        }
        if (!urlsDropped && (files.length === 0 || unsupportedTypes.length > 0)) {
            this.setState({dropzoneActive: false, showUploadError: true})
            return
        }

        if (files.length === 0)
            return

        const maxSize = files.reduce((max, f) => f.size > max ? f.size : max, files[0].size);
        const totalSize = files.reduce((sum, f) => sum + f.size, files[0].size);
        this.setState({dropzoneActive: false, uploadSize: totalSize})
        if (maxSize > FIVE_MB || totalSize > TEN_MB)
            this.setState({showSizeWarning: true})
        if (maxSize > TEN_MB) {
            this.setState({showSizeExceeded: true})
            return
        }

        let card, fileCards = []
        files.forEach(file => {
            card = CardModel.FromFileUpload (file)
            uiStateStore.fileCardMap[file] = card
            fileCards.push(card)
        })

        this.setState({uploadCards: fileCards}, () => {
            this.uploadFiles(fileCards)
        })
    }

    onDragEnter = () => {
        this.setState({dropzoneActive: true})
    }

    onDragLeave = () => {
        this.setState({dropzoneActive: false})
    }

    onExpand(cardId) {
        this.setState({expandedCardId : cardId})
    }

    onShrink(cardId) {
        this.setState({expandedCardId : null})
    }

    onTextChange = (cardId, propName, propValue) => {
        const cardUpdate = {id: cardId, [propName]: propValue}
        const cardInCollection = cardStore.currentCards.find(card => card.id === cardId)
        const cardIndex = cardStore.currentCards.findIndex(card => card.id === cardId)
        if (cardInCollection && cardIndex >= 0) {
            cardInCollection[propName] = propValue
            cardStore.currentCards[cardIndex] = cardInCollection
        }


    }

    onTextSave = (card) => {
        // const cardUpdate = {id: cardId, [propName]: propValue}
        this.saveCard(card)
    }

    // Clipboard notes
    // A paste of file(s) will produce both items and files.
    // items have name specifics via getAsString, files don't.
    // Options
    // 1. If files exist, ignore items. no name though.
    // 2. use item.getAsFile.
    // 3. get both and matchup.
    // 4.

    onPaste = (ev) => {
        return
        const clipboard = ev.clipboardData
        if (!clipboard || (!clipboard.items && !clipboard.files)) // invalid paste
            return


        // text or ...? types
        let filename
        if (clipboard.items && clipboard.items instanceof DataTransferItemList) {
            for (let i=0; i<clipboard.items.length; i++) { // no forEach, not array
                let item = clipboard.items[i]
                if (item.type === 'text/plain') {
                    item.getAsString((txt) => {
                        if (clipboard.files.length > 0) {
                            filename = txt
                        } else {
                            this.setState ({uploadCount: i, dropzoneActive: false, uploading: true})
                            if (isUrl (txt)) {
                                cardStore.createCardFromUrl (txt).then (cardResponse => {
                                    this.props.refreshCards ()
                                    this.setState ({uploading: false, uploadCards: []})
                                })
                            } else {
                                cardStore.createCardFromText (txt).then (cardResponse => {
                                    this.setState ({uploading: false, uploadCards: []})
                                })
                            }
                        }
                    })
                } else if (item.kind === 'file') {
                    item.getAsFile((file) => {
                        let a = 1
                    })
                }
            }
        }

        // file types
        if (false && clipboard.files && clipboard.files instanceof FileList) {
            let acceptedFiles = []
            for (let i=0; i<clipboard.files.length; i++) { // no forEach or filter, not array
                let file = clipboard.files[i]
                if ([...FileTypes, ...ImageTypes].indexOf (file.type) !== -1)
                    acceptedFiles.push (file)
            }

            let card, fileCards = []
            this.fileRefs = acceptedFiles
            acceptedFiles.forEach( file => {
                card = CardModel.FromFileUpload (file)
                uiStateStore.fileCardMap[file] = card
                fileCards.push(card)
            })

            this.setState({uploadCards: fileCards}, () => {
                this.uploadFiles(fileCards)
            })

        }
    }

    uploadFiles(filesUnusedUsingStateVar) {
        this.setState ({uploading: true})
        cardStore.createCardsFromFiles(this.state.uploadCards, this.props.currentProject).then(response => {
            this.newCards = response.data.cards
            cardStore.attachFilesToCards(this.newCards, this.fileRefs, this.showUploadProgress.bind(this))
        })
    }

    // Upload progresses will arrive asynchronously
    showUploadProgress(uploadNumber, msg) {
        const totalUploading = this.state.uploadCards.length
        if (msg === 'start') {
            const count = Math.min(this.state.uploadCount + 1, totalUploading)
            this.setState({uploadCount: count, uploading: true})
        }
        if (msg === 'end') {
            const count = Math.min(this.state.completedCount+1, totalUploading)
            this.setState({completedCount: count})

            if (this.state.completedCount === this.state.uploadCards.length) {
                requestPreviews(this.newCards)
                this.setState ({uploading: false, uploadCards: []}, () => {
                    this.props.refreshCards ()
                    if (this.fileRefs)
                        this.fileRefs.forEach (file => window.URL.revokeObjectURL (file.preview))
                })
            }
        }
    }


    hoverCard(dragIndex, hoverIndex) {
        cardStore.hoverCard(dragIndex, hoverIndex)
    }

    // commit drag operation to backend.
    moveCard(dragId, dragIndex, hoverIndex) {
        if (cardStore.isFiltering)
            return

        // done with localCards copy. Persist its state to mobx.
        // let localCards = this.state.localCards.slice()
        let storeCards = cardStore.currentCards
        const dragCard = cardStore.getCardFromCurrent(dragId)
        const hoverCard = cardStore.getPreDragCard(hoverIndex) // local copy of cards has already mutated during hover, so grab card originally at position from mobx.
/*
        if (dragCard.index === hoverIndex) {
            console.warn("Moving card to same position. Ignoring")
        }
*/
        let destIndex

        // if (hoverIndex === cardStore.currentCards.length - 1)
        //     destIndex = cardStore.currentCards.length
        if (hoverIndex > dragIndex)    // if dragging toward tail
            destIndex = hoverIndex // +1 //hoverCard.nextId  // insert AFTER hover card
        else                                // if dragging toward head
            destIndex = hoverIndex // insert BEFORE hover card

        cardStore.moveCard(dragCard, destIndex).then( response => {
            // TODO rather than fetch project, how's about reorder cards according to response.data.destination_project_cards array
            projectStore.fetchCurrentProject(this.props.project.id)
            this.setState({isHovering: false})
        })
    }

    saveCard(card) {
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon) {
            uiStateStore.setJoinPromptState(true)
            return
        }

        this.props.saveCard(card)
    }

    deleteCard(cardId, cardIndex) {
        const {project} = this.props
        if (project && project.id) {
            cardStore.deleteCard (cardId,project.id).then (response => {
                projectStore.fetchCurrentProject (project.id)
            })
        }
    }

    clickCard = (targetElem, card) => {
        const ap = uiStateStore.currentApprovalRequest
        const isApproval = uiStateStore.rightUnModal === SendForApproval && ap !== null

        // Deselect/Delete
        if (cardStore.isCardSelected(card.id)) {
            cardStore.deselectCard (card.id)

            // Persist only if this is an existing AP
            if (isApproval && !uiStateStore.isAPStarterRequest)
                cardStore.deleteCard(card.id, ap.id).then( response => {
                    ap.cardIds = ap.cardIds? ap.cardIds.filter( cardId => cardId !== card.id) : []
                })

        }

        // Select/Add
        else {
            cardStore.selectCard (card.id)
            if (isApproval && !uiStateStore.isAPStarterRequest)
                addCardsToApproval ([card.id], ap.id).then(apCardIds => {
                    ap.cardIds = apCardIds
                })
        }
    }

    clickPane() {

    }

/*
    clearUnseenEvents() {
        this.setState({showUnseenEvents: false})
    }
*/

    handlePushEvents() {
        const project = projectStore.currentProject
        if (!project)
            return

        const rtEvents = eventStore.getInstance().rtEvents[project.id] || []
        const unseenEvents = eventStore.getInstance().unseenEventHistory.filter(event => event.projectId === project.id) || []
        const allEvents = rtEvents.concat(unseenEvents)

        const cardAdds = allEvents.filter( event => event.type === AddCardToProject || event.type === CreateCard) || []
        cardAdds.forEach( event => this.handleRemoteCardAdd( event.cardId ))

        const cardDeletions = allEvents.filter( event => event.type === CardDelete) || []
        cardDeletions.forEach( event => this.handleRemoteCardDelete( event.cardId ))

        const cardMoves = allEvents.filter( event => event.type === CardDelete) || []
        cardMoves.forEach( event => this.handleRemoteCardMove( event.cardId ))

    }

    handleRemoteCardAdd( cardId ) {
        // TODO
        // fetch new card
        // add it to our rendered cards without propagating
    }

    handleRemoteCardDelete( cardId ) {
        // TODO
        // find card in our rendered cards
        // delete card without propagating
    }

    handleRemoteCardMove( cardId ) {
        // TODO
        // refresh all cards
        // find card's new index within refreshed cards.
        // move card without propagating
    }

    createCards(cards) {
        this.props.createCards(cards)
    }

    importCards(cards) {
        this.props.importCards(cards)
    }

    importAssets(assets, source) {
        this.props.importAssets(assets, source)
    }

    uploadAssets(cards, files) {
        this.props.uploadAssets(cards, files)
    }

    openCardViewer(card, attmId) {
        const elem = findDOMNode(this)
        if (elem)
            uiStateStore.cardsPaneSP = elem.scrollTop
        uiStateStore.setViewingCard(card, attmId)
        // uiStateStore.unModal = CardViewer
    }

    openCardEditor(card) {
        const elem = findDOMNode(this)
        if (elem)
            uiStateStore.cardsPaneSP = elem.scrollTop
        uiStateStore.setEditingCard(card)
        // uiStateStore.unModal = CardEditor
    }

    componentWillMount() {
        uiStateStore.showAuthors = UserSessionUtils.getSessionPref('showAuthors')
        uiStateStore.showActions = UserSessionUtils.getSessionPref('showActions')
        uiStateStore.showDescriptions = UserSessionUtils.getSessionPref('showDescriptions')

    }

    componentDidMount() {
        const elem = findDOMNode(this)
        if (elem) {
            uiStateStore.cardsPane = elem
            if (uiStateStore.cardsPaneSP > 0)
                elem.scrollTo (0, uiStateStore.cardsPaneSP)
        }

        if (window) {
            window.addEventListener ("resize", this.updateDimensions)
            // window.addEventListener ("paste", this.onPaste)
        }
        const device = ResponsiveUtils.getDeviceType()
        if (device !== 'computer') {
            uiStateStore.setStickyInfo (true)
            // uiStateStore.cropImages = false
        }
        this.setState({elem: elem})

    }

    componentDidUpdate() {
        console.warn('Cards pane did update')
        this.updateDimensions()
        if (userStore.userPrefs) {
            if (uiStateStore.showStacks) {
                this.origCrop = userStore.userPrefs.cropImages
                userStore.userPrefs.cropImages = true
            } else {
                userStore.userPrefs.cropImages = this.origCrop
            }
        }

    }

    componentWillReact() {
        console.log('CardsPane will react')
        // this.handlePushEvents()
        // this.forceUpdate()
        // alert('cards pane reacting')
    }

    render() {
        const { mobile, isOver, item, connectDropTarget} = this.props
        const project = projectStore.currentProject
        const forMobxOnly = project? eventStore.getInstance ().oldRtEventCounts[project.id] : null
        if (!project)
            return null

        let trimmedTags = tagStore.currentTagSet || []
        let cards = []
        cards = this.state.isHovering? cardStore.currentCards :    // drag or push notif in progress. Use local copy for speed.
                       this.props.isFiltering? cardStore.filteredCards : // no drag, use store. Filtering.
                       cardStore.currentCards                            // no drag, use store. Not filtering.
        const cardCount = cards? cards.length : 0
        const isApproval = uiStateStore.rightUnModal === SendForApproval

        const cardEvents = project? eventStore.getInstance().getCardBreakdowns(project.id) : []
        const uploadFileTotal = this.state.uploadCards.length
        // const percentUploaded = 100 * this.state.completedCount / uploadFileTotal
        const dropTarget = !uiStateStore.editingCard? connectDropTarget : function(elem) {return elem}

        const dzClass = this.state.dropzoneActive? ' dropzone abs' : ''
        let paneClass = 'cardViewsWorkPane main hidden-scroll' + dzClass
        if (isApproval)
            paneClass += ' apPane'

        let size = uiStateStore.getCardSize('main') || uiStateStore.defaultCardSize
        let sizeStyle = {}, sizeVal, hasUnits, units
        const ratio = project.reactionSetId !== null? 1.5 : 1.2;
        const re = /([0-9]+)(px|\%?)/
        const matches = re.exec(size)
        if (matches && matches.length > 1) {
            sizeVal = matches[1]
            hasUnits = matches.length > 2 && matches[2] !== ''
            units = hasUnits? matches[2] : 'px'
        }
        if (!hasUnits)
            size += units
        sizeStyle = {width: (sizeVal + units), height: (sizeVal * ratio) + units}
        let allowImageChange

        // tag-based stacks organization
        const stackMap = {} // new Map()
        if (uiStateStore.showStacks && trimmedTags.length > 0) {
            trimmedTags.forEach( tag => {
                // stackMap.set(tag, cards.filter(card => card.tags2 && card.tags2.findIndex(cardTag => cardTag.id === tag.id) !== -1) )
                stackMap[tag.id] = cards.filter(card => card.tags2 && card.tags2.findIndex(cardTag => cardTag.id === tag.id) !== -1) || []
            })
        }
        const stackCount = Object.keys(stackMap).length // stackMap.size

        // new card badging
        const evs = eventStore.getInstance()
        const selCatId = categoryStore.currCatId || '0'
        const cagBadges = evs.currentBadges? evs.currentBadges.categories : null
        const catBadgeInfo = cagBadges? cagBadges[selCatId] : null
        const projectBadgeDetails = catBadgeInfo && catBadgeInfo.projects && catBadgeInfo.projects[project.id]? catBadgeInfo.projects[project.id] : null
        const badgedCards = projectBadgeDetails? projectBadgeDetails.cards : {}
        const badgedCardTotal = badgedCards? Object.keys(badgedCards).length : 0

        let tag, isNew

        return dropTarget(

            <div className={paneClass} onPaste={this.onPaste}>

            <Dropzone id='dzcp'
                      className={dzClass} style={{width: '100%', height: '100%'}} disableClick
                      // accept={AcceptedDropTypes}
                      maxSize={TEN_MB}
                      disabled={uiStateStore.enableFileDrops === false}
                      onDrop={this.onDrop.bind(this)}
                      onDragEnter={this.onDragEnter}
                      onDragLeave={this.onDragLeave}
            >

                <Dimmer active={this.state.uploading} inverted>
                    <Loader style={{marginTop: '0', height: '10%'}}>
                        <span className='jFG'>Uploading: {this.state.completedCount+1} of {uploadFileTotal}</span>
                    </Loader>
                </Dimmer>

                <Dimmer active={uiStateStore.importingAssets} inverted>
                    <Loader inverted style={{marginTop: '0', height: '10%'}}>Importing...</Loader>
                </Dimmer>

                <Dimmer active={uiStateStore.exporting} inverted>
                    <Loader style={{marginTop: '0', height: '10%'}}>
                    </Loader>
                      <div className='fg' style={{marginTop: '6rem', lineHeight: '2'}}>Generating PDF.</div>
                      {project && project.cardTotal > 25 &&
                      <div className='fg' style={{lineHeight:'2'}}>Larger projects may take up to 2 minutes to assemble. <br/>Please do not hit refresh.</div>
                      }

                </Dimmer>

                <Dimmer active={isOver}>
                    <div style={{marginTop: '0', height: '10%', color: '#777', fontSize: '2rem'}}>
                        Add {item && item.type === DndItemTypes.PERSON? item.name : 'card'} to this project?
                    </div>
                </Dimmer>

                <Dimmer active={this.state.dropzoneActive} inverted>
                    <div style={{marginTop: '0', height: '10%', color: '#aaa', fontSize: '2rem', fontWeight: 300}}>
                        Add to Board
                    </div>
                </Dimmer>

                <Loader active={uiStateStore.loadingNotes} inverted style={{marginTop: '0', height: '10%'}}></Loader>

                {!uiStateStore.showStacks && cardCount > 0 && !mobile && ['cards','comments'].indexOf(uiStateStore.cardView) !== -1 &&
                <ReactCSSTransitionGroup
                    transitionName="cardfx"
                    transitionEnterTimeout={300}
                    transitionLeaveTimeout={200}>

                    {cards.map ((cardModel, i) => {

                        allowImageChange = !isGoogleDoc(cardModel)
                        if (cardModel.description && cardModel.description.startsWith('Create a new document'))
                            cardModel.description = ''

                        isNew = Boolean(badgedCards && badgedCards[cardModel.id] && badgedCards[cardModel.id].new)

                        return (
                            <Card
                                card={cardModel}
                                isNew={isNew}
                                size={size}
                                style={sizeStyle}
                                containerName='main'
                                containerElem={this.state.elem}
                                previous={this.previousCard}
                                next={this.nextCard}
                                key={cardModel.domId}
                                wkey={'c'+i}
                                index={i}
                                prefSize={this.state.prefCardSize}
                                user={this.props.user}
                                view={uiStateStore.cardView}
                                showTags={this.props.showTags}
                                showLetters={this.props.showLetters}
                                showReactions={project.reactionSetId !== null}
                                openCardViewer={(card, attmId) => this.openCardViewer(card, attmId)}
                                openCardEditor={(card) => this.openCardEditor(card)}
                                ltr={indexToCardLabel (i)}
                                id={cardModel.id}
                                text={cardModel.title}
                                reactionSet={project.reactionSet}
                                allowImageChange={allowImageChange}
                                isApproval={isApproval}

                                mobile={mobile}

                                events={cardEvents[cardModel.id]}
                                showUnseenEvents={true || this.state.showUnseenEvents}

                                moveCard={this.moveCard}
                                hoverCard={this.hoverCard}
                                selectCard={this.clickCard}
                                selected={cardStore.isCardSelected(cardModel.id)}

                                saveCard={(card) => this.saveCard (card)}
                                deleteCard={this.deleteCard}
                                expanded={this.state.expandedCardId === cardModel.id}

                                onExpand={this.onExpand}
                                onShrink={this.onShrink}
                                onTextChange={this.onTextChange}
                                onTextSave={this.onTextSave}
                            />
                        )
                    })}

                </ReactCSSTransitionGroup>
                }

                {uiStateStore.showStacks && stackCount > 0 && !mobile && ['cards', 'comments'].indexOf (uiStateStore.cardView) !== -1 &&
                <div>
                    {/*<div className='jSec tiny'>{stackCount} stacks</div>*/}
                    {Object.keys(stackMap).map( (tagId, i) => {
                        tag = trimmedTags.find(tag => tag.id === tagId)
                        return (
                            <CardStack tag={tag} cards={stackMap[tagId]} size={size}/>
                        )
                    })}
                </div>
                }


                {cardCount === 0 &&
                <CardCreator
                    mobile={mobile}
                    showTags={this.props.showTags}
                    showReactions={project.reactionSetId !== null}
                    containerElem={this.state.elem}
                    createCards={(cards) => {this.createCards(cards)}}
                    importCards={(cards) => {this.importCards(cards)}}
                    importAssets={(assets, source) => {this.importAssets(assets, source)}}
                    uploadAssets={(cards, files) => {this.uploadAssets(cards, files)}}
                />
                }

                {Boolean(userStore.userPrefs && userStore.userPrefs.autoShowTour) &&
                <TourFirstProject cardCount={project.cards ? project.cards.length : 0}/>
                }

            </Dropzone>
            <Confirm
                open={this.state.showSizeExceeded}
                header={"File size exceeded"}
                content='We accept files as large as 10Mb. For larger files, please contact support@pogo.io'
                onCancel={this.closeSize}
                onConfirm={this.closeSize}
            />

            <Confirm
                open={this.state.showUploadError}
                header={"Aw Snap"}
                content='We had a problem with this upload. Please try a different file, or contact support@pogo.io'
                onCancel={this.closeError}
                onConfirm={this.closeError}
            />

            </div>
        )
    }
}

function isGoogleDoc(cardModel) {
    if (['docs.google.com', 'Google Docs'].indexOf(cardModel.source) !== -1)
        return true

    if (cardModel.url) {
        const domain = getShortUrl (cardModel.url, true)
        return (domain === 'docs.google.com')
    }
    return false
}