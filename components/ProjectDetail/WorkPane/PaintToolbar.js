import React from 'react'
import {observer} from 'mobx-react'

import {Icon, Popup} from "semantic-ui-react";
import ReactTooltip from "react-tooltip";
import uiStateStore from "../../../store/uiStateStore";
import {PaintTool} from "../../../config/Constants";
import userStore from "../../../store/userStore";
import {saveAnnotation, sendImage} from "../../../services/ImageFileService";
import {CirclePicker, GithubPicker} from "react-color";
import {X} from "react-feather";

@observer
export default class PaintToolbar extends React.Component {

    state = {saving: false, saved: false}

    handleColorChangeComplete = (color, ev) => {
        uiStateStore.paintColor = color.hex
    }

    cancel = (ev) => {
        uiStateStore.paintEnable = false
        if (this.props.onPaintCancel)
            this.props.onPaintCancel()
    }

    undo = (ev) => {
        if (uiStateStore.paintRef) {
            uiStateStore.paintRef.undo()
            uiStateStore.paintCount -= 1
        }
    }

    clear = (ev) => {
        if (uiStateStore.paintRef) {
            uiStateStore.paintRef.clear()
            uiStateStore.paintCount = 0
        }
        if (this.props.onPaintClear)
            this.props.onPaintClear()
    }

    saveChanges = (ev) => {
        if (uiStateStore.paintRef) {
            // const desc = 'Markup' + (userStore.currentUser ? ` by ${userStore.currentUser.displayName} by Marc Jacobs` : '')
            this.drawing = uiStateStore.paintRef.canvas.drawing.toDataURL ()
            this.drawing = this.drawing.replace('data:image/png;base64,','')
            this.setState({saving: true})
            sendImage (this.drawing, true, this.showUploadProgress, null, 1)
        }

    }

    // Upload progresses will arrive asynchronously
    showUploadProgress = (count, msg, imageUrl) => {
        if (msg === 'end') {
            if (imageUrl) {
                saveAnnotation(this.props.card.id, imageUrl).then(response => {
                    // cardStore.refreshCard(this.props.card.id)
                    // projectStore.fetchCurrentProject()
                    this.setState({saving: false})
                    // this.showSaved()
                    // tell parent to refresh its card to obtain latest annotations
                    if (this.props.onPaintSave)
                        this.props.onPaintSave()
                }).catch(error => {
                    console.error(error)
                }).finally( () => {
                    uiStateStore.paintEnable = false
                })
            } else {
                // TODO fail
                this.setState({saving: false})
            }

            // RELEASE RESOURCES
            window.URL.revokeObjectURL (this.drawing)

        }
        else if (msg === 'start') {
            this.setState({saving: true})
        } else {
            console.log("Progress: count = " +count+ ", msg: " +msg)
        }
    }

    showSaved = (ev) => {
        this.setState({saved: true})
        setTimeout(() => {
            this.setState({saved: false})
        }, 2000)
    }

    componentDidMount() {
        ReactTooltip.rebuild()
    }

    render() {
        const {mobile} = this.props
        const {saving, saved} = this.state
        const color = uiStateStore.paintColor

        return (
            <div className={'fullW jFG centered relative' +(mobile? ' pad03':' pad1')}>
                {!mobile &&
                <div className='inline mt05 mb1' style={{height: '2rem', width: 0}}>
                    &nbsp;
                </div>
                }

                {!mobile &&
                <span className='inline mr1'>
                    Markup
                </span>
                }

                <Popup style={{padding: 0}}
                       inverted
                       trigger={<Icon name="circle" size="large" className='inline mr1' style={{color: color}}/>}
                       on='click'
                       position='bottom left'
                >
                    <GithubPicker disableAlpha presetColors={['#B80000', '#DB3E00', '#FCCB00', '#008B02', '#006B76', '#1273DE', '#004DCF', '#5300EB', '#EB9694', '#FAD0C3', '#FEF3BD', '#C1E1C5', '#BEDADC', '#C4DEF6', '#BED3F3', '#D4C4FB']}
                                  width={220} color={ color } onChangeComplete={ this.handleColorChangeComplete }/>
                </Popup>

                <button className={'inline mr1' +(mobile? ' jBtnInv':' jBtn')}
                      style={{opacity: uiStateStore.paintCount > 0? 1:0.5, cursor: uiStateStore.paintCount > 0? 'pointer': 'not-allowed'}}
                      data-tip='Clear' onClick={this.clear}>
                    Clear
                </button>

                <button className={'inline mr1' +(mobile? ' jBtnInv':' jBtn')}
                        style={{opacity: uiStateStore.paintCount > 0? 1:0.5, cursor: uiStateStore.paintCount > 0? 'pointer': 'not-allowed'}}
                        disabled={uiStateStore.paintCount === 0}
                        onClick={this.undo}>
                    Undo
                </button>


                <button className={'inline mr1' +(mobile? ' jBtnInv':' jBtn')}
                        style={{opacity: uiStateStore.paintCount > 0? 1:0.5, cursor: uiStateStore.paintCount > 0? 'pointer': 'not-allowed'}}
                        data-tip='Save changes' onClick={this.saveChanges}>
                    {saving? 'Saving...':'Save'}
                </button>

                <span className='pointer jFG abs'
                      style={{right: mobile? '1rem':'2rem', top: mobile? '.2rem':'1.25rem'}}
                      data-tip='Close' onClick={this.cancel}>
                    <X size={32} />
                </span>

                {saved &&
                <span className='jBlueBG pad1 jFG capsule inline bold'>
                    Saved!
                </span>
                }

            </div>
        )
    }
}