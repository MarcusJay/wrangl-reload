import {Component} from 'react'
import {Link} from '~/routes'
import {DropTarget} from 'react-dnd'
import Dropzone from 'react-dropzone'

import {Dimmer, Loader} from 'semantic-ui-react'

import {observer} from 'mobx-react'

import {ChevronLeft, Chrome, Menu as FMenu} from 'react-feather'
import StateChangeRequestor, {REQUEST_PROJECT_UPDATE} from '/services/StateChangeRequestor'
import ProfileMenu from "/components/common/ProfileMenu";
import UserSessionUtils from '/utils/UserSessionUtils'
import SystemStatus from "../common/SystemStatus";
import ReactTooltip from 'react-tooltip'

import projectStore from '/store/projectStore'
import cardStore from '/store/cardStore'
import userStore from '/store/userStore'
import categoryStore from '/store/categoryStore'
import uiStateStore from '/store/uiStateStore'
import LoginMenu from "../common/LoginMenu";
import {DndItemTypes, UserStatus} from "../../config/Constants";
import RU from "../../utils/ResponsiveUtils";
import {sendImage} from "../../services/ImageFileService";
import EventDetailsPopup2 from "../events/EventDetailsPopup2";
import HelpButton from "../help/HelpPopup";
import Breadcrumb from "../common/Breadcrumb";
import BoardMembers from "./WorkPane/BoardMembers";

// import store from 'store2'

const MAX_MEMBERS_DISPLAY = 40

const coverImageTarget = {
    drop(props, targetMonitor, component) {
        let dragItem = targetMonitor.getItem ()

        // use card image as cover
        if (dragItem.type === DndItemTypes.CARD) {
            const card = cardStore.getCardFromCurrent(dragItem.id)
            if (card && card.image)
                component.handleImageChange (card.image)
        }
    }
}

@DropTarget([DndItemTypes.CARD], coverImageTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    item: monitor.getItem()
}))
@observer
export default class PDTitlePane extends Component {
    constructor(props) {
        super(props)
        this.handleImageChange = this.handleImageChange.bind(this)
        this.state = {
            dropzoneActive: false,
            largeCover: false,
            loading: false,
            showInfo: false,
            showMembers: false,
        }

    }

    logout = () => {
        UserSessionUtils.endUserSession()
        window.location.href = '/login'
    }

    showLargeCover = () => this.setState({largeCover: true})
    hideLargeCover = () => this.setState({largeCover: false})

    handleTextChange = (ev) => {
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon) {
            uiStateStore.setJoinPromptState(true)
            return
        }
        const project = projectStore.currentProject
        const propName = ev.target.name
        const propValue = ev.target.value
        project[propName] = propValue
    }

    handleTextClick = (ev) => {
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon)
            uiStateStore.setJoinPromptState(true)

    }

    handleTextSave = (ev) => {
        if (this.state.loading) // prevent double save via Enter and blur
            return

        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon) {
            uiStateStore.setJoinPromptState(true)
            return
        }

        const project = projectStore.currentProject
        const propName = ev.target.name || ev.target.id // former works with form field, latter works with content editable
        const propValue = ev.target.value || ev.target.innerText // former works with form field, latter works with content editable
        const payload = {id: project.id, [propName]: propValue}
        ev.target.blur()

        if (project[propName] === propValue) // no change
            return

        this.setState({loading: true})
        projectStore.updateProject(payload).then(response => {
            this.setState({loading: false})
        })
    }

    handleKeyPress = (ev) => {
        if (ev.key === 'Enter') {
            ev.preventDefault()
            ev.stopPropagation()
            this.handleTextSave (ev)
        }
    }

    // Image may have been uploaded or specified by url. Either way, we have its new hosted url.
    handleImageChange(image) {
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon) {
            uiStateStore.setJoinPromptState(true)
            return
        }

        projectStore.currentProject.image = image
        if (image)
            StateChangeRequestor.awaitStateChange(REQUEST_PROJECT_UPDATE,
                {project: {project_id: projectStore.currentProject.id, project_image: image}}).then( response => {
                // projectStore.currentProject.image = response
                // debugger
                this.setState(this.state) // force update
            })
    }

    toggleMobileSidebar = () => {
        uiStateStore.toggleMobSidebarL()
    }

    showInfo(show) {
        this.setState({showInfo: show})
    }

    showMembers(show) {
        this.setState({showMembers: show})
    }

    onInfoOpen(ev) {
        this.setState({showInfo: true})
        ReactTooltip.rebuild()
    }

    onInfoClose(ev) {
        this.setState({showInfo: false})
    }

    onMembersOpen(ev) {
        this.setState({showMembers: true})
        ReactTooltip.rebuild()
    }

    onMembersClose(ev) {
        this.setState({showMembers: false})
    }

    toggleVO() {
        uiStateStore.voMode = !uiStateStore.voMode
        ReactTooltip.rebuild()
    }

    onDrop(files) {
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon) {
            uiStateStore.setJoinPromptState(true)
            return
        }
        this.fileRefs = files
        if (files.length > 0) {
            let fileRef = files[0]
            sendImage(fileRef, false, this.updateProgress.bind(this))
            // see updateProgress for results handling
        }
    }

    onDragEnter = () => {
        this.setState({dropzoneActive: true})
    }

    onDragLeave = () => {
        this.setState({dropzoneActive: false})
    }

    updateProgress(count, msg, uploadedImageUrl) {
        if (msg === 'start') {
            this.setState({loading: true})
        } else if (msg === 'end') {
            const project = projectStore.currentProject
            if (uploadedImageUrl)
                project.image = uploadedImageUrl
            // else if (this.fileRefs && this.fileRefs.length > 0) {
            //     project.image = this.fileRefs[0].preview
            // }
            projectStore.updateProject({id: project.id, image: uploadedImageUrl}).then( response => {
                this.setState ({loading: false, project: project})
                if (this.fileRefs)
                    this.fileRefs.forEach (file => window.URL.revokeObjectURL (file.preview))
            })

        }
    }

    // Although this component uses projectStore directly, we want to compare incoming props to old props to animate changes.
    // deprecated
/*
    componentWillReceiveProps(nextProps) {
        console.log('Project Title Pane will receive props')
        const newProject = nextProps.project
        const oldProject = this.props.project
        if (!oldProject)
            return

        const imgChanged = newProject.image !== oldProject.image
        const titleChanged = newProject.title !== oldProject.title
        const descChanged = newProject.description !== oldProject.description

        if (imgChanged || titleChanged || descChanged)
            debugger

        this.setState({
            imgChanged: imgChanged,
            titleChanged: titleChanged,
            descChanged: descChanged
        })
    }
*/

    componentDidMount() {
        const project = projectStore.currentProject
        if (project) {
            userStore.getUserById(project.creatorId) // sets userStore.currentCreator
        }

    }

    componentDidUpdate(prevProps) {
        const prevProject = prevProps.project
        const newProject = projectStore.currentProject

        if (newProject && newProject.title === 'Untitled Board') {
            const titleElem = this.refs.ptitle
            if (titleElem)
                titleElem.focus()
        }

        if (prevProject && newProject && prevProject.id !== newProject.id) {
            console.log('Old proj desc: ' +prevProject.description+ ', newProject desc: ' +newProject.description)
        }
    }
    render() {
        const {loading, largeCover, dropzoneActive} = this.state
        const {mobile, user, isOver, item, connectDropTarget} = this.props
        let project = projectStore.currentProject
        if (!project)
            return null

        const creator = userStore.getUserFromConnections(project.creatorId)

        // const workColumns = uiStateStore.voMode? 12 : 10
        const isEditable = !uiStateStore.voMode
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        const amICreator = project && userStore.currentUser && userStore.currentUser.id === project.creatorId

        // onboarding & loading
        const placeholder = project && project.image? '' : ' placeholder'
        const title = project? project.title : 'Loading Project...'
        const desc = project? project.description : 'Add a description'
        const image = project && project.image? project.image : '/static/img/pattern1.png'

        // animate changes
        // Nah, people don't like flashing anymore. And we'd need to differentiate between changes due to remote event vs routing
        const imgGlow = false // isOver || this.state.imgChanged? ' glow' : ''
        const titleFlash = false // this.state.titleChanged? ' flashAndFade' : ''
        const descFlash = false // this.state.descChanged? ' flashAndFade' : ''

        const dzClass = dropzoneActive? 'dropzone' : ''
        const viewWidth = RU.getViewPortWidth()
        const coverW = Math.min(500, viewWidth * .5)
        const coverH = Math.min(500, viewWidth * .5)
        const shouldDim = uiStateStore.currentApprovalRequest !== null && !uiStateStore.isAPStarterRequest

        const selCat = categoryStore.getCurrentCategory()


        if (mobile) {
            return (
                <header className="mFraming">
                    <div className='left'>
                        <FMenu className='pointer' onClick={() => this.toggleMobileSidebar()}/>
                        <Link prefetch route='projectsPage' data-tip='Home' >
                            <ChevronLeft size={24} className='ml1 jSec pointer mr1'/>
                        </Link>
                    </div>
                    <div className='inline dontWrap mr1' style={{maxWidth: '80%', marginTop: '3px'}}>
                        {title}
                    </div>
                    <div className='absTopRight' style={{marginRight: '-1rem'}}>
                        <HelpButton createProject={this.create} className='mr1'/>
                        <ProfileMenu className="right" user={user} mobile/>
                    </div>
                </header>
            )
        }

        // large
        return connectDropTarget(
            <header className="jMidBG titlePane mb1 relative" >
                <Dimmer active={shouldDim} >
                    <div className='absFull' style={{backgroundColor: 'rgba(0,0,0,.94) !important'}}>&nbsp;</div>
                </Dimmer>
                <Loader active={loading}></Loader>

                <div style={{margin: '.25rem 0 -.25rem .5rem'}}>
                    <Breadcrumb catId={categoryStore.currCatId} project={project} />
                </div>

                <div className={"utProjDesc projectImage mr1 inline" +imgGlow +placeholder +(amICreator? ' creator':'')}
                     style={{backgroundImage: 'url(' +image+ ')'}}
                     onMouseEnter={this.showLargeCover}
                     onMouseLeave={this.hideLargeCover}>
                    <Dropzone onDrop={this.onDrop.bind(this)}
                              multiple={false}
                              disabled={uiStateStore.enableFileDrops === false || !amICreator}
                              onDragEnter={this.onDragEnter}
                              onDragLeave={this.onDragLeave}
                              style={{width: '100%', height: '100%', padding: 0}}
                    >
                    </Dropzone>
                </div>

                <div className='inline' style={{width: '50%'}}>
                    <div >

                        <SystemStatus/>
                    </div>

                    <div >


                    </div>
                </div>

                <div className='abs flex' style={{left: '5rem', right: '32%', bottom: '-1rem'}}>

                    <div style={{flexBasis: '60%', flexShrink: 3}}>
                        <p   className={"jFG large krn1 " +titleFlash}
                             data-tip={amICreator? "Click to edit board title":''}
                             contentEditable={isEditable && amICreator && !isAnon}
                             id='title'
                             ref='ptitle'
                             suppressContentEditableWarning={true}
                             onClick={this.handleTextClick}
                             onBlur={this.handleTextSave}
                             onKeyPress={this.handleKeyPress}
                        >
                            {title}
                        </p>

                        <p className='formPaneTrns inline tiny jSec'
                           data-tip={amICreator? "Click to edit board description":''}
                           contentEditable={isEditable && amICreator && !isAnon}
                           id='description'
                            // placeholder='Project Description'
                            // rows={1}
                            // disabled={!isCreator}
                           onClick={this.handleTextClick}
                           onChange={this.handleTextChange}
                           onBlur={this.handleTextSave}
                           onKeyPress={this.handleKeyPress}
                           style={{border: 'none', cursor: 'text', padding: 0, resize: 'none', backgroundColor: 'transparent !important'}}
                        >
                            {desc}
                        </p>
                    </div>

                    {/* Move members here. No more in left pane. Chris */}
                    {!uiStateStore.viewingCard && !uiStateStore.editingCard &&
                    <div className='noWrap' style={{flexBasis: '30%', flexGrow: 2, textAlign: 'right'}}>
                        <BoardMembers/>
                    </div>
                    }
                </div>

                <div className="absTopRight right secondary" style={{textAlign: 'right'}}>

                    <HelpButton className='mr1'  createProject={this.createProject}/>

                    <div className='inline vaTop'>
                        <EventDetailsPopup2 />
                    </div>

                    <div className='utChrome inline vaTop mr1'>
                        <a href='https://chrome.google.com/webstore/detail/pogoio/pnonlpnonheokmhpjmjnonhkijehjdcf' target='_blank' className='jSec'
                           data-tip="Our chrome extension allows you to pull images and links from other sites directly into your boards. Get it now! ">
                            {/*<img src='/static/svg/v2/chromeCircle.svg' width={26} />*/}
                            {/*<Chrome size={24} />*/}
                            <img src='/static/img/chrome32.png' width='26'/>
                        </a>
                    </div>

                    {user &&
                    <ProfileMenu user={user} />
                    }

                    {!user &&
                    <LoginMenu project={project}/>
                    }
                </div>

            </header>
        )
    }
}
