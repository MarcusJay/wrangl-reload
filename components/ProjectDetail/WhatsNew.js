import {Component} from 'react'
import {observer} from 'mobx-react'
import {Bell} from "react-feather"
import uiStateStore from '/store/uiStateStore'

@observer
export default class WhatsNew extends Component {

    constructor(props) {
        super(props)
    }

    flashyflashy() {
        uiStateStore.showWhatsNew = true
        uiStateStore.nWhatsNew++ // using counter rather than bool as bool wasn't triggering a single reaction anywhere
        // debugger
        // setTimeout(() => uiStateStore.showWhatsNew = false, 5000)
    }

    render() {
        return(
            <span className="whatsNew" onClick={() => this.flashyflashy()}>
                <Bell size={16} style={{marginRight: '3px', marginBottom: '-4px'}}/>
            </span>
        )
    }
}