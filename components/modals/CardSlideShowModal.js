import React, {Component} from 'react'
import {Icon, Modal} from 'semantic-ui-react'
import {ChevronLeft, ChevronRight, Image as FImage, X, ZoomIn} from 'react-feather'
import Moment from 'react-moment'

import uiStateStore from '/store/uiStateStore'
import {ToastConfig} from 'config/Constants';
import {ToastContainer} from 'react-toastify'
import ReactTooltip from 'react-tooltip'
import CardFullView from "../common/Card/CardFullView";
import userStore from "/store/userStore"
import cardStore from "/store/cardStore"
import {extToIcon, isEmbeddable, isImage} from "../../config/FileConfig";
import {indexToCardLabel} from "../../utils/solo/indexToCardLabel";

const LEFT = 1
const RIGHT = 2

export default class CardSlideShowModal extends Component {
    constructor(props) {
        super(props)
        // this.props.cards
        this.state = { modalOpen: false, currentCard: null, currentIndex: 0, currentAttm: null, transitionToggle: true }
    }

    handleUnmount = (ev, props) => {
        this.setState({modalOpen: false})
    }

    handleMount = (ev, props) => {
        // this.setState ({modalOpen: true})
        // uiStateStore.isFullViewOpen = true  // need for doc-wide key event catching
        // uiStateStore.forcePopupOpen = true;
        let selectedIndex = 0, selectedCard = cardStore.currentCards[0]
        let selectedAttm = null

        if (this.props.cardId) {
            selectedCard = cardStore.currentCards.find( card => card.id === this.props.cardId)
            selectedIndex = cardStore.currentCards.indexOf( selectedCard ) // find( card => card.id === this.props.cardId)
        }
        if (selectedCard) {
            if (this.props.attmId) {
                selectedAttm = this.selectAttm(this.props.attmId, selectedCard)
            }

            this.fetchCard (selectedCard.id)
            this.setState ({modalOpen: true, currentIndex: selectedIndex, currentAttm: selectedAttm}) // fetchCard will retrieve fully populated currentCard
        } else {
            this.setState ({modalOpen: true, currentIndex: selectedIndex, currentCard: selectedCard})
        }
    }

    handleOpen = (ev, props) => {
        this.setState ({modalOpen: true})
        uiStateStore.isFullViewOpen = true  // need for doc-wide key event catching
        uiStateStore.forcePopupOpen = true;
    }

    handleClose = (ev, props) => {
        this.setState({ modalOpen: false })
        uiStateStore.isFullViewOpen = false
        uiStateStore.forcePopupOpen = false;
    }

    handleKeyPress = (ev) => {
        console.log('key: ', ev.key)
        // if (ev.key === 'ArrowLeft')
        //     this.gotoPreviousCard(ev)
        // else if (ev.key === 'ArrowRight')
        //     this.gotoNextCard(ev)

    }

    fetchCard = (cardId) => {
        cardStore.getCardById(cardId).then( card => {
            this.setState({currentCard: card })
        })
    }

    atFirstSlide() {
        return this.state.currentIndex === 0
    }

    atLastSlide() {
        return this.state.currentIndex === cardStore.currentCards.length - 1
    }

    gotoPreviousCard(ev) {
        ev.stopPropagation()
        // ev.preventDefault()

        if (this.atFirstSlide()) {
            console.warn('gotoPrevious - already at 0, ignoring.')
            return
        }

        const newIdx = this.state.currentIndex - 1
        const newCard = cardStore.currentCards[newIdx]
        this.setState({ currentIndex: newIdx, currentCard: newCard} )
        this.fetchCard(newCard.id)
    }

    gotoNextCard(ev) {
        ev.stopPropagation()
        ev.preventDefault()

        if (this.atLastSlide()) {
            console.warn ('gotoNext - already at end, ignoring.')
            return
        }

        const newIdx = this.state.currentIndex + 1
        const newCard = cardStore.currentCards[newIdx]
        this.setState({ currentIndex: newIdx, currentCard: newCard, currentAttm: null})
        this.fetchCard(newCard.id)
    }

    triggerTransition() {
        this.setState({transitionToggle: !this.state.transitionToggle})
    }

    selectAttm(attmId, card) {
        if (!card)
            card = this.state.currentCard

        if (!card) {
            console.log ('SelectAttm: no card. Skipping. Consider setState() completed function')
            this.clearAttm()
            return
        }

        const attm = card.attachments.find( attm => attm.attachment_id === attmId )
        console.log('selectAttm: selecting ' +(attm? attm.url : 'not found'))
        if (attm)
            this.setState({currentAttm: attm})
        return attm
    }

    clearAttm() {
        console.log('Select main: Erasing currentAttm.')
        this.setState({currentAttm: null})
    }

    componentWillMount() {
        if (!this.state.modalOpen)
            return

/*
        let selectedIndex = 0, selectedCard = cardStore.currentCards[0]
        let selectedAttm = null

        if (this.props.cardId) {
            selectedCard = cardStore.currentCards.find( card => card.id === this.props.cardId)
            selectedIndex = cardStore.currentCards.indexOf( selectedCard ) // find( card => card.id === this.props.cardId)
        }

        if (selectedCard) {
            if (this.props.attmId) {
                selectedAttm = this.selectAttm(this.props.attmId, selectedCard)
            }

            this.fetchCard (selectedCard.id)
            this.setState ({currentIndex: selectedIndex, currentAttm: selectedAttm}) // fetchCard will retrieve fully populated currentCard
        } else {
            this.setState ({currentIndex: selectedIndex, currentCard: selectedCard})
        }
*/
    }

    componentWillUnmount() {
        uiStateStore.forcePopupOpen = false;
    }

    componentDidMount() {
        if (typeof document === 'undefined')
            return

        // TODO REPLACe WITH REFS
        const modalContent = document.querySelector ('#mc')
        if (modalContent) // hidden in vo mode
            modalContent.focus ()

        if (this.state.modalOpen) {

            // TODO replace with react synthetic event once working and determined which component best suited
            document.onkeyup = (ev) => {
                const key = ev.charCode
                console.log('Key: ' +key)
                if (key === LEFT)
                    this.gotoPreviousCard (ev)
                else if (key === RIGHT)
                    this.gotoNextCard (ev)
            }

        } else {
            //document.removeEventListener('keyup', this.)
        }
    }

/*
    static getDerivedStateFromProps(props, state) {
        debugger
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        debugger
    }

    componentWillUpdate(a,b) {
        debugger
    }

    componentDidUpdate(prevProps, prevState, snapshop) {
        // this.setState({modalOpen: b.modalOpen})
        debugger
    }
*/

    render() {
        const card = this.state.currentCard
        if (!card)
            return null

        const isTriggerInCard = !!this.props.cardId

        const attm = this.state.currentAttm
        const attmCount = card && card.attachments? card.attachments.length : 0
        let iconName, isAttmEmbeddable, isAttmViewable

        const ltr = indexToCardLabel (this.state.currentIndex)
        const creator = card? userStore.getUserFromConnections(card.creatorId) : null
        const reactionCount = card? (card.reactions || []).length : 0
        const link = card? card.url : null

        const prevColor = this.atFirstSlide()? '#555' : 'white'
        const nextColor = this.atLastSlide()? '#555' : 'white'

        // several triggers
        let triggerIcon
        const iconStyle = isTriggerInCard? {marginBottom: '0.35rem', marginRight: '0.75rem', maxWidth: '1.35rem'} : {} // marginBottom: '0.3rem', marginLeft: '0.5rem'

        if (this.props.triggerIconSrc)
            triggerIcon = <img src={this.props.triggerIconSrc} className='embedIcon pointer' style={iconStyle}/>
        else if (this.props.triggerIconName === 'zoom') {
            triggerIcon = <ZoomIn size={20} style={iconStyle}/>
        } else if (this.props.trigger)
            triggerIcon = this.props.trigger
        else
            triggerIcon = <FImage stroke-width='2' size={24} className="subtleBtn" style={{marginBottom: '-3px', marginRight: '0.75rem'}}/>

        const tip = (this.props.tip === false)? null : isTriggerInCard? "Open Full Screen" : "View Full Screen Gallery"

        return (
            <Modal className='slideShowModal'
                trigger={<span className="zoom" onClick={this.handleOpen} data-tip={tip}>{triggerIcon}</span>}
                open={this.state.modalOpen}
                dimmer="blurring"
                onClose={this.handleClose}
                onOpen={this.handleOpen}
                onMount={this.handleMount}
                closeOnDimmerClick={true}
                closeOnDocumentClick={true}
            >
                <h1 className='noWrap'>{ltr}. {card? card.title : this.props.projectTitle}</h1>
                <X className='absTopRight' color='white' size={36} onClick={this.handleClose}/>
                {card &&
                <Modal.Content id='mc' onKeyUp={this.handleKeyPress} onClick={this.clickBG}>
                    <div className='navigation'>
                        <ChevronLeft size={48} color={prevColor} className={'nav left '}
                                     onClick={(ev) => this.gotoPreviousCard (ev)}/>
                        <ChevronRight size={48} color={nextColor} className='nav right' disabled={this.atLastSlide()}
                                      onClick={(ev) => this.gotoNextCard (ev)}/>
                    </div>

                    {attmCount > 0 &&
                    <span className='thumbs'>
                        <div className='thumb pointer' style={{backgroundImage: 'url(' +card.image+ ')'}}
                             onClick={() => this.clearAttm()}>&nbsp;</div>
                        {card.attachments.map ((attm, i) => {
                            isAttmEmbeddable = isEmbeddable (attm.file_type)
                            isAttmViewable = isImage (attm.file_type)
                            iconName = extToIcon (attm.file_type)

                            if (isAttmViewable)
                                return <div className='thumb pointer' style={{backgroundImage: 'url(' +attm.url+ ')'}} onClick={() => this.selectAttm( attm.attachment_id )}>&nbsp;</div>
                            else if (isAttmEmbeddable && iconName)
                                return <Icon name={iconName} className='pointer' onClick={() => this.selectAttm( attm.attachment_id )}/>
                            else
                                return null
                        })}
                    </span>
                    }

                    <CardFullView card={card} attm={attm} handleNav={this.handleKeyPress} handleClose={this.handleClose}/>

                    <ToastContainer autoClose={ToastConfig.duration}
                        // type="success"
                                    closeOnClick
                                    pauseOnHover
                    />
                    <ReactTooltip effect="solid"/>
                </Modal.Content>
                }

                {/*Could make this a CardStats component I spose meh*/}
                {card && false &&
                <div className='bottomStats'>

                    <label>Title:</label><span>{card.title}</span>
                    <label>Description:</label><span>{card.description},</span>
                    {creator &&
                    <span>
                        <label>Created By:</label><span>{creator.displayName},</span>
                    </span>
                    }
                    <label>Last Modified:</label>
                    <span>
                        <Moment subtract={{hours: 8}} fromNow>{card.dateModified}</Moment>
                    </span>
                    {link &&
                    <span>
                        <label>Link:</label><span>{card.url},</span>
                    </span>
                    }
                    {reactionCount > 0 &&
                    <span>
                        <label>Reactions:</label><span>{(card.reactions || []).length}</span>
                    </span>
                    }
                </div>
                }

                {card &&
                <input type='text' onKeyDown={this.handleKeyPress} className='hidden' autoFocus/>
                }
            </Modal>
        )
    }
}
