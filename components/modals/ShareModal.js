import React, {Component} from 'react'
import {Modal} from 'semantic-ui-react'
import {Facebook, Link, Send, Twitter, X} from 'react-feather'

import Toaster from '/services/Toaster';

import {env} from '/config/env'
import EmailService from "../../services/EmailService";
import {randomStr} from "/utils/solo/randomStr";
import userStore from "../../store/userStore";
import isValidEmail from "../../utils/solo/isValidEmail";
import {ProjectType} from "../../config/Constants";

export default class ShareModal extends Component {
    constructor(props) {
        super(props)
        this.state = { modalOpen: false, showCopyConfirmation: false,
            email: null, name: null, sendingInvite: false, sentInvite: false }
        this.defaultShareMsg = "Please join me at '" +(props.project? props.project.title : 'Pogo.io')+ "': "
    }

    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => this.setState({ modalOpen: false })

    copyLink = () => {
        const {project} = this.props
        if (!project)
            return
        const url = "https://pogo.io/project/" + project.id
        const linkNode = document.querySelector('#hdnLink')
        linkNode.value = url
        linkNode.select()
        document.execCommand('copy')
        Toaster.info('Link to board copied: ' +url)
    }

    shareToFacebook = (ev) => {
        let projectUrl = env.getProjectUrl(this.props.project.id)

        FB.ui(
            {
                method: 'feed',
                redirect_uri: projectUrl,
                link: projectUrl,
                caption: this.props.project.title
            },
            function(response){
                if (response && !response.error_message){
                    Toaster.info ("Post Successful!")
                }
                else {
                    console.error('ERROR: Error sharing project to feed.');
                    Toaster.error ("We were unable to post to your feed. '" +response.error_message+ "'")
                }
            })
    }

    handleChange = (ev) => {
        const propName = ev.target.name
        const propValue = ev.target.value
        this.setState({[propName]: propValue})
    }

    handleKeyPress = (ev) => {
        if (ev.key === 'Enter')
            this.sendInvite()
    }

    sendInvite = () => {
        const {email, name} = this.state
        const {project, user} = this.props

        if (email === null)
            throw new Error('sendInvite with missing email')
        const projectUrl = project? env.getProjectUrl(project.id) : env.current.url
        // const shareProject = project // || {title: 'Pogo.io',  image: ''}

        // create user immediately.
        const defaultPW = randomStr(4)
        userStore.createPendingUser( email, name, defaultPW)
        const isAP = project && project.projectType === ProjectType.APPROVAL
        this.setState({sendingInvite: true})

        EmailService.sendInvite({
            recipientEmail: email,
            recipientName: name,
            recipientPW: defaultPW,
            senderName: (user.displayName || user.firstName),
            senderId: user.id,
            isAP: isAP,
            projectId: project? project.id : null,
            imageUrl: project? project.image : 'https://pogo.io/static/img/pogoMulti.png',
            itemCount: project? project.cardTotal : 0,
            apDesc: project? project.description : null
        }).then( response => {
            // Toaster.info('Shared shareProject.title+ " successfully sent to " +(name || email)+ ". Please let them know.")
            this.setState({email: '', name: '', sendingInvite: false, sentInvite: true})
            setTimeout(() => {
                this.setState({sentInvite: false})
            }, 2000)
        }).catch(error => {
            Toaster.error("Board not shared. Error: " +error)
            this.setState({sendingInvite: false})
        })
    }
    // End Email Invites


    componentDidMount() {
        if (typeof window === 'undefined' || window.fbAsyncInit !== undefined)
            return

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1742126849378172',
                xfbml      : true,
                cookie     : true,
                status     : true,
                version    : 'v2.10'
            });
        };
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            //#xfbml=1&version=v2.10&appId=1742126849378172
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    }

    render() {
        if (!this.props.project)
            return null

        const {showCopyConfirmation, email, name} = this.state
        const isInviteEnabled = email !== null
        const isEmailValid = isValidEmail(email)

        return (
            <Modal className='shareModal pad0'
                trigger={
                    <button className='utShare jBtn micro mr05 krn1 vaTop '
                            style={{minWidth: '5rem', marginTop: '.15rem'}}
                            onClick={this.handleOpen}
                    >
                        SHARE
                    </button>
                }
                open={this.state.modalOpen}
                onClose={this.handleClose}
                   size='tiny'
                // size='large'
            >
                <div className='large bold jBG jFG pad1'>Share this board</div>
                <X size={16} onClick={this.handleClose} className='close third abs' style={{top: '0', right: '1rem'}}/>
                <div className='jBGinv jFGinv pad1 small'>

                    <img className="shareImage inline" src={this.props.project.image}/>
                    <span className="inline large ml1 vaTop mt05" >Share Board "{this.props.project.title}"</span>

                    <div className="mt1" >
                        {/*<Mail size={18} />*/}
                        <div className="inline mr1 mb1" >Invite:</div>
                        <input name="email"
                               value={email}
                               className='mr1 mb05 '
                               style={{width: '40%', padding: '.5rem', color: 'black !important', backgroundColor: 'white !important'}}
                               placeholder="Email address" type='text'
                               onChange={this.handleChange}
                               onKeyPress={this.handleKeyPress}
                        />
                        <input name="name" value={name}
                               className='mr1 '
                               style={{width: '25%', padding: '.5rem', color: 'black !important', backgroundColor: 'white !important'}}
                               placeholder="Name (optional)" type='text'
                               onChange={this.handleChange} onKeyPress={this.handleKeyPress}
                        />

                        <button disabled={!isEmailValid}
                                className={'jBtn' +(isEmailValid? '':' disabled')}
                                onClick={this.sendInvite}
                                style={{color: '#333'}}>
                            Send
                        </button>
{/*
                        <Send size={26} disabled={!isInviteEnabled}
                              className='hoh pointer'
                              onClick={this.sendInvite}/>
*/}

                        {this.state.sendingInvite &&
                        <div className='right pad1 large'>Sending invitation...</div>
                        }
                        {this.state.sentInvite &&
                        <div className='right mt1 pad1 large boxRadius jBlueBG jFG'>Sent √</div>
                        }
                    </div>
                    <div className=''>
                        <button className='jBtn mt05 mb1 block' onClick={this.copyLink}
                                style={{color: '#333'}}
                             data-tip="Copy the link to this project and share it with others">
                            Copy Board Link
                        </button>
                        <input id='hdnLink' type='text' className='abs' style={{opacity: 0, bottom: 0}}/>

                        <button onClick={this.shareToFacebook} className='jBtn mb1 pointer block'
                                style={{color: '#333'}}
                             data-tip="Share this board to Facebook">
                            Post Board to Facebook
                        </button>

                        <button className='jBtn mb1 pointer block'
                                style={{color: '#333'}}
                             data-tip="Share this board to Twitter">
                            <a target='_blank' href={"https://twitter.com/intent/tweet?text=" +this.defaultShareMsg+ "&url=" +env.getProjectUrl(this.props.project.id+ "&display_url=binder.cards")}>
                                Post Board to Twitter
                            </a>
                        </button>
                    </div>
                    <div>
                        <button className='button neutral' onClick={this.handleClose} >
                            Close
                        </button>
                    </div>
                </div>
            </Modal>
        )
    }
}
