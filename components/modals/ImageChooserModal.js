import React, {Component} from 'react'
import {Form, Header, Modal, Progress} from 'semantic-ui-react'
import {Camera} from 'react-feather'

import uiStateStore from '/store/uiStateStore'
import ProjectApiService from "../../services/ProjectApiService";
import {sendImage, sendURL} from "../../services/ImageFileService";
import {deleteImage} from "../../services/CardApiService";

export default class ImageChooserModal extends Component {
    constructor(props) {
        super(props)
        this.state = { modalOpen: false }
        this.uploadFile = this.uploadFile.bind(this)
        this.updateProgress = this.updateProgress.bind(this)
    }

    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => this.setState({ modalOpen: false })

    suppressDefault = (ev) => {
        ev.preventDefault()
        ev.stopPropagation()
    }

    // Hitting Enter in Web URL is interpreted as a click of the Import button. So let's determine user's intention.
    uploadFile(ev, data) {
        const elem = document.querySelector( '#uploader1' )
        let self = this

        // Intention is to upload a blob
        if (elem.files.length > 0) {
            let fileRef = elem.files[0]
            let fileType = fileRef.type
            sendImage( fileRef, false, this.updateProgress )
            // see updateProgress for results handling

        // Intention is to send an image url
        } else {
            let elem = document.querySelector('#imageUrl')
            let imageUrl = elem.value
            sendURL(imageUrl, this.props.cardId).then( response => {
                console.log("Response: "+response)
                // self.props.project.image = response.data.image
                this.props.handleImageChange(response.data.image)
                this.handleClose()

            }).catch( error => {
                console.error(error)
                this.handleClose()
                throw new Error(error)
            })
        }
        this.handleClose()
    }

    deleteImage = () => {
        if (this.props.cardId)
            deleteImage(this.props.cardId).then(response => {
                this.props.handleImageChange(null)
                this.handleClose()
            })
        else if (this.props.project)
            ProjectApiService.deleteImage(this.props.project).then(response => {
                this.props.handleImageChange(null)
                this.handleClose()
            })
    }

    updateProgress(count, msg, imageUrl) {
        if (msg === 'start') {
            this.setState({uploading: true})
        } else if (msg === 'end') {
            this.setState ({uploading: false})
            debugger
            this.props.handleImageChange(imageUrl)
        } else {
            this.setState({uploadProgress: Math.round((count/msg)*100)})
        }
    }

    render() {
        if (uiStateStore.voMode)
            return (
                <span></span>
            )

        const {cardId, trigger, triggerText, showTrigger} = this.props
        const iconColor = cardId? '#777' : 'black' // ''#e0e0e0'
        const iconStyle = cardId? {marginBottom: '-3px', marginLeft: '0.25rem'} : this.props.project? {marginTop: '2rem'} : {} // -6px

        const tip = cardId? "Choose an Image" : null
        const trigClass = triggerText? 'pointer' : 'imageTrigger'
        const trigInner = triggerText? <span>{triggerText}</span> : <Camera color={iconColor} size={20} style={iconStyle}/>
        const trigOuter = <span className={trigClass} onClick={this.handleOpen} data-tip={tip} data-place="bottom">{trigInner}</span>
        const trig = trigger || trigOuter

        return (
            <Modal className='imageModal'
                   trigger={trig}
                   open={this.state.modalOpen}
                   onOpen={this.handleOpen}
                   onClose={this.handleClose}
                    size='small' style={{top: '1rem !important'}}
            >
                <Header icon='image' content={"Choose an image" }/>
                <Modal.Content onClick={this.suppressDefault}>
                    <div className="upload-box" >
                        <div className="browse-files">
                            <div>Choose an Image from your computer</div><br/>
                            <input id='uploader1' className="browse-input" type="file" name="files"/>
                            {this.state.uploading &&
                            <Progress id='progress' percent={this.state.uploadProgress} indicating style={{marginTop: '50px', width: '90%', marginLeft: '10%'}}/>
                            }
                            <div><br/>Or enter an Image URL</div><br/> {/*// redo*/}
                            {/*<input placeholder='Image URL...' onChange={this.handleImageInputChange}/>*/}
                            <Form.Input id='imageUrl' name='image' style={{ fontSize: '1rem', width: '90%'}}
                                        placeholder={'Image URL...'} />
                            {!this.state.uploading &&
                            <span>
                                <button id='importBtn' className="button go" style={{marginTop: '20px'}} title="Save Image"
                                        onClick={this.uploadFile}>Save Image</button>
                                <button id='deleteBtn' className="button stop" style={{marginTop: '20px'}} title="Delete Image"
                                    onClick={this.deleteImage}>Delete Image</button>
                            </span>
                            }
                        </div>
                    </div>
                </Modal.Content>
{/*
                <Modal.Actions centered>
                    <Button color='blue' onClick={this.handleClose} >
                        Done
                    </Button>
                </Modal.Actions>
*/}
            </Modal>
        )
    }
}
