import React, {Component} from 'react'
import {observer} from 'mobx-react'
import {Confirm, Divider, Input} from 'semantic-ui-react'
import {PlusCircle, Send, X} from 'react-feather'

import EmailService from '/services/EmailService'
import {env} from '/config/env'
import userStore from '/store/userStore'
import projectStore from '/store/projectStore'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import Toaster from '/services/Toaster';
import {ToastContainer} from 'react-toastify'
import {ToastConfig} from 'config/Constants'

import ReactTooltip from 'react-tooltip'
import ArrayUtils from '/utils/ArrayUtils'
import {randomStr} from "/utils/solo/randomStr";
import {SearchConfig} from '/config/Constants'
// import PersonCard from "../common/PersonCard";
import {EMAIL_REGEX, NOTIF_ALL, ProjectType, UserRole} from "../../config/Constants";
import Person from "../common/PersonInline" // PersonInModal";
import uiStateStore from "../../store/uiStateStore";
import ResponsiveUtils from "../../utils/ResponsiveUtils";
import isValidEmail from "../../utils/solo/isValidEmail";

const inputBG = '#2B323A!important'

// TODO Is it preferable to combine this with PLPeople and handle changes with styles, or safer to keep separate functionality
@observer
export default class PeopleUnmodal extends Component {
    state = {
        modalOpen: false, email: null, name: null, sendingInvite: false, query: '', showEmailWarning: false,
        openPersonId: null, searchResults: [], lastKeyPress: 0, searching: false, isVirgin: true,
        showDeleteConfirmation: false, deletingUserId: null, dmUserIds: []
    }

    close() {
        uiStateStore.maximizeLeftCol = false
        uiStateStore.leftUnModal = null
        userStore.getUserConnections ().then (connections => {
            // mobx will cause PLPeople to react
        })
    }

    setOpenPersonId = (personId) => {
        this.setState ({openPersonId: personId})
    }

    // too many friends?
    confirmDisconnect(contactId) {
        this.setState ({showDeleteConfirmation: true, deletingUserId: contactId})
    }

    onDeleteCancel = () => {
        this.setState ({showDeleteConfirmation: false, deletingUserId: null})
    }

    onDeleteConfirm = () => {
        this.disconnectUser (this.state.deletingUserId)
    }

    // playin favorites?
    pinUser(contactId) {
        userStore.toggleUserPin (contactId, true)
    }

    unpinUser(contactId) {
        userStore.toggleUserPin (contactId, false)
    }

    directMessage(contactId) {
        uiStateStore.openDM (contactId)
    }

    // Email Invites
    emailChange = (ev) => {
        const value = ev.target.value
        this.setState ({email: value, showEmailWarning: !isValidEmail (value)})
    }

    nameChange = (ev) => {
        const value = ev.target.value
        this.setState ({name: value})
    }

    handleKeyPress = (ev) => {
        if (ev.key === 'Enter')
            this.sendInvite ()
    }

    // users can be invited to a specific project or to site in general
    sendInvite = () => {
        const email = this.state.email
        if (email === null || email.length === 0) {
            return
        }
        else if (email.length < 4 || !EMAIL_REGEX.test (email.toLowerCase ())) {
            console.error ('sendInvite attempt with bad email of ' + email)
            Toaster.error ('Invalid email format. Please try again.')
            return
        }

        const activeUser = userStore.currentUser
        let project = projectStore.currentProject
        // const projectUrl = project ? env.getProjectUrl (project.id) : env.current.url
        // if (!project)
        //     project = {title: 'Pogo.io', image: ''}

        // create user immediately.
        const defaultPW = randomStr (4)
        userStore.createPendingUser (this.state.email, this.state.name, defaultPW)

        this.setState ({sendingInvite: true})

        EmailService.sendInvite({
            recipientEmail: this.state.email,
            recipientName: this.state.name,
            recipientPW: defaultPW,
            senderName: (activeUser.displayName || activeUser.firstName),
            senderId: activeUser.id,
            isAP: project && project.projectType === ProjectType.APPROVAL,
            projectId: project? project.id : null,
            imageUrl: project? project.image : 'https://pogo.io/static/img/pogoMulti.png',
        }).then (response => {
            Toaster.info ((this.state.name || this.state.email) + " will receive an email invitation to join. Please let them know.")
            this.setState ({email: '', name: '', sendingInvite: false})
        }).catch (error => {
            Toaster.error ("Invitation not sent. Error: " + error)
            this.setState ({sendingInvite: false})
        })
    }
    // End Email Invites

    // Search Contacts
    resetSearch() {
        this.setState ({isLoading: false, searchResults: [], value: ''})
    }

    handleSearchChange(e, {value}) {
        let allegedValue = value;
        let actualDomValue = document.querySelector ('#contactSearch').value
        let now = new Date ()
        let elapsed = this.state.lastKeyPress === 0 ? 0 : now - this.state.lastKeyPress
        this.setState ({query: actualDomValue, lastKeyPress: now})

        setTimeout (() => {
            if (this.state.query.length >= SearchConfig.MIN_SEARCH_CHARS) {
                this.submitSearch (this.state.query)
            } else if (!this.state.isVirgin) {
                console.log ('Clearing search')
                this.setState ({searchResults: []})
            } else {
                console.log ('Skipping search ' + query + ' at ' + elapsed + 'ms')
            }
            this.setState ({waiting: false})

        }, SearchConfig.QUERY_DELAY_MS)

    }

    submitSearch(query) {
        console.log ("Searching for " + query + "...")
        this.setState ({searching: true, isVirgin: false})
        query = query.toLowerCase ()

        // TODO implement a custom renderer instead.
        this.props.connections.map (user => {
            user.title = user.displayName;
            user.description = user.email || user.socialProvider
        })

        // TODO no user search endpoint yet.
        // For now, search within connections.
        this.setState ({
            searching: false,
            searchResults: this.props.connections.filter (user => {
                return (user.displayName && user.displayName.toLowerCase ().indexOf (query) === 0) || (user.email && user.email.toLowerCase ().indexOf (query) === 0)
            })
        })
    }

    handleResultSelect(e, {value}) {
        // this.setState ({value: result.title})
        console.log ('result selected: ' + e)
        debugger
    }

    // End Search Contacts

    // Contact Actions
    // only called by clicks on inline actions (√, +, invited).
    // Implication: user is viewing current project.
    // TODO consolidate this and same in PLPeople to 1 place eg service
    setMembership(userId, newRole) {
        if (!userId)
            throw new Error ('toggleMembership: missing user')

        if (!projectStore.currentProject)
            throw new Error ('toggleMembership: no current project')

        const project = projectStore.currentProject
        const member = project.members.find (member => member.id === userId)
        const isMember = member ? member.role === UserRole.ACTIVE : false
        const isInvited = member ? member.role === UserRole.INVITED : false

        if (newRole !== undefined)
            projectStore.setUserProjectSettings (userId, project.id, (newRole === UserRole.INVITED ? NOTIF_ALL : null),
                newRole).then (updatedProject => {
                this.props.refreshMembers ()
            })

        else if (isMember || isInvited) { // toggle if unspecified
            projectStore.setUserProjectSettings (userId, project.id, null, UserRole.DELETED).then (updatedProject => {
                this.props.refreshMembers ()
            })
        } else {
            projectStore.setUserProjectSettings (userId, project.id, NOTIF_ALL, UserRole.INVITED).then (updatedProject => {
                this.props.refreshMembers ()
            })
        }
    }

    // playin favorites?
    pinUser(contactId) {
        userStore.toggleUserPin (contactId, true)
    }

    unpinUser(contactId) {
        userStore.toggleUserPin (contactId, false)
    }

    disconnectUser(contactId) {
        const userId = userStore.currentUser.id
        userStore.disconnectUsers (userId, contactId)
        this.setState ({showDeleteConfirmation: false, deletingUserId: null})
    }


    render() {
        const activeUser = userStore.currentUser
        // const everyone = userStore.currentConnections
        const everyone = userStore.currentConnections.slice () // this.props.connections.slice()
        const project = projectStore.currentProject
        const mobile = ResponsiveUtils.isMobile ()

        const {openPersonId, email, showEmailWarning} = this.state
        const activeUserInProject = project ? project.members.find (member => member.id === activeUser.id) : null
        const enableAdmin = activeUserInProject ? activeUserInProject.role === UserRole.ADMIN : false
        const isInviteEnabled = email !== null && email.length >= 4 && EMAIL_REGEX.test (email.toLowerCase ())

        // pinned users next
        const pinnedUsers = everyone.filter (user => user.pinned === true) || []

        // members next
        let members = !project ? [] : project.members.filter (user => user.role === UserRole.ACTIVE || user.role === UserRole.ADMIN) || []
        members = members.filter (member => member.id !== activeUser.id && !pinnedUsers.find (user => user.id === member.id)) || []
        let invited = !project ? [] : project.members.filter (user => user.role === UserRole.INVITED) || []
        if (members.length > 0) {
            members.forEach (member => {
                if (!member.displayName) member.displayName = "Hidden"
            })
        }

        // All other contacts last
        let nonMembers = project ? ArrayUtils.arrayDifference (everyone, project.members) : everyone
        let isMember

        return (
            <div className='leftUnModal'>
                <X size={24} onClick={() => this.close ()} className='close'/>
                <nav className='lpTabs'
                     style={{marginTop: '-0.5rem'}}> {/*accounts for unmodal pad, keeps tabs from moving*/}
                    <div className='inline hoh pointer relative ml1'>
                        <img src='/static/svg/v2/contacts.svg' width={26}/>
                    </div>

                    <div className='utDM jFG tiny kern1 inline'
                         style={{paddingLeft: '1rem'}}
                         onClick={this.toggleOpen}>
                        CONTACTS
                    </div>
                </nav>

                {/*
                {everyone.length > 0 &&
                <h3 className='jFG' style={{marginBottom: '1rem'}}>You have {everyone.length} Contacts in Pogo.io</h3>
                }
*/}

                {everyone.length === 0 &&
                <div>
                    <h3 className='secondary' style={{marginBottom: '1rem'}}>You don't have any contacts yet.</h3>
                    <div className='small secondary' style={{marginBottom: '1rem'}}>
                        To create contacts on Pogo.io, you can:
                        <ul>
                            <li>
                                Send someone an email invitation to join you
                            </li>
                            <li>
                                Ask a Pogo.io user to invite you to one of their boards
                            </li>
                            <li>
                                Ask a Pogo.io user to copy and paste you the direct link to their board
                            </li>
                        </ul>
                    </div>
                </div>
                }

                <h2 className='jFG mt1'>Invite new members to join Pogo.io</h2>
                    <div style={{marginBottom: '1rem'}}>
                        <input className="sidePaneInput small lighter"
                               type='text'
                               style={{width: '46%'}}
                               name="emailInput"
                               value={this.state.email}
                               placeholder="email address..."
                               onChange={this.emailChange}
                               onKeyPress={this.handleKeyPress}/>

                        <input className="sidePaneInput small lighter"
                               type='text'
                               style={{width: '46%'}}
                               name="nameInput"
                               placeholder="Full name (optional)"
                               value={this.state.name}
                               onChange={this.nameChange}
                               onKeyPress={this.handleKeyPress}/>

                        <X size={20} className='jSec mt05 inline'/>
                        {showEmailWarning &&
                        <div className='red'>
                            Please enter a valid email
                        </div>
                        }

                    </div>

{/*
                <p className='jFG vaTop'><PlusCircle size={20} className='mr1 large' style={{marginBottom: '-4px'}}/>
                    Add more people</p>
                <p className='jFG'><input type='checkbox' className='mr1 large'/>Connect everyone together</p>
                <p className='jFG'><input type='checkbox' className='mr1 large'/>Add everyone to a new Board
                </p>
*/}

                <div className='mt2 mb2'>
                    <button disabled={!isInviteEnabled} className={'button ' + (isInviteEnabled ? ' jFG go hoh' : ' disabled jThird')}
                            onClick={this.sendInvite}>
                        {this.state.sendingInvite ? "Sending..." : isInviteEnabled ? "Send Invitation" : 'Send Invitation'}
                    </button>
                </div>

                <div className="section peoplePane">

                    {/*
                    <div>
                        <Search id="contactSearch"
                                loading={this.state.searching}
                                onResultSelect={this.handleResultSelect}
                                onSearchChange={this.handleSearchChange}
                                results={this.state.searchResults}
                                placeholder='Search Contacts...'
                                value={this.state.query}
                        />
                    </div>
*/}

<Divider/>
                    <div className="people">
                        <div className="members">
                            <h2 className='jFG mt1'>Existing Contacts</h2>
                            <ReactCSSTransitionGroup
                                transitionName="cardfx"
                                transitionEnterTimeout={500}
                                transitionLeaveTimeout={300}>
                                {[...pinnedUsers, ...members, ...nonMembers].map ((person, i) => {

                                    isMember = project && project.members.find (member => member.id === person.id) !== undefined
                                    return (
                                        <Person
                                            person={person}
                                            activeUser={activeUser}
                                            enableAdmin={enableAdmin}
                                            showName={true}
                                            key={'pup' + i}
                                            id={person.id}
                                            pinned={person.pinned}
                                            mobile={mobile}
                                            name={person.displayName || person.email}
                                            project={project}
                                            isMember={isMember}
                                            pinUser={() => this.pinUser (person.id)}
                                            unpinUser={() => this.unpinUser (person.id)}
                                            openPersonId={openPersonId}
                                            setOpenPersonId={this.setOpenPersonId}
                                            showMenu={2}
                                            disconnectUser={() => this.confirmDisconnect (person.id)}
                                            setMembership={() => this.setMembership (person.id)}
                                            directMessage={() => this.directMessage (person.id)}
                                        />
                                    )

                                })}
                            </ReactCSSTransitionGroup>
                        </div>

                    </div>
                </div>

                <Confirm
                    content={'Permanently remove from your Contacts?'}
                    open={this.state.showDeleteConfirmation}
                    onCancel={this.onDeleteCancel}
                    onConfirm={this.onDeleteConfirm}
                    confirmButton='Remove'
                    style={{height: '25% !important'}}
                />
                <ToastContainer autoClose={ToastConfig.duration}
                    // type="success"
                                closeOnClick
                                pauseOnHover
                />
                <ReactTooltip effect="solid"/>
            </div>
        )
    }
}
