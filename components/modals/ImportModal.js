import React, {Component} from 'react'
import {observer} from 'mobx-react'
import {findDOMNode} from 'react-dom'
import {Header, Modal, Tab} from 'semantic-ui-react'
import {PlusSquare, X} from 'react-feather'
import UserSessionUtils from '/utils/UserSessionUtils'
// import SearchTab from "./ImportTabs/SearchTab"
import CombinedSearchTab from "./ImportTabs/CombinedSearchTab"
// import WebTab from "./ImportTabs/WebTab";
import CardsTab from "./ImportTabs/CardsTab";
import UploaderComponent from "/components/common/UploaderComponent"
import DropboxTab from "./ImportTabs/DropboxTab";
import BlankTab from "./ImportTabs/BlankTab";
import uiStateStore from "../../store/uiStateStore";

export const TAB_LIBRARY = 0
export const TAB_UPLOAD = 1
export const TAB_SEARCH = 2
export const TAB_DROPBOX = 3

const DEFAULT_TITLE = "Add new items to board"

@observer
export default class ImportModal extends Component {
  constructor(props) {
    super (props)
    this.state = {modalOpen: false, modalHeight: 0}
    this.handleClose = this.handleClose.bind (this)
    this.handleOpen = this.handleOpen.bind (this)
    this.createCards = this.createCards.bind (this)
    this.importCards = this.importCards.bind (this)
    this.importAssets = this.importAssets.bind (this)
    this.uploadAssets = this.uploadAssets.bind (this)

    this.state.activeTab = this.props.tab || UserSessionUtils.getSessionPref('selectedImportTab') || TAB_SEARCH
    const {imageOnly, card} = this.props

    this.panes = [
      {menuItem: 'Library',
          render: () => <Tab.Pane style={{height: this.state.modalHeight}} attached={false} >
            <CardsTab project={this.props.project} onClose={this.handleClose}
                      imageOnly={imageOnly}
                      card={uiStateStore.viewingCard}
                      importCards={this.importCards}/></Tab.Pane>
      },
      {menuItem: 'Upload',
          render: () => <Tab.Pane style={{height: this.state.modalHeight}} attached={false} >
            <UploaderComponent project={this.props.project} onClose={this.handleClose}
                               imageOnly={imageOnly}
                               card={uiStateStore.viewingCard}
                               uploadAssets={this.uploadAssets}/></Tab.Pane>
      }
/*
      {menuItem: 'Dropbox',
          render: () => <Tab.Pane style={{height: this.state.modalHeight}} attached={false} active={true} selected={true}>
            <DropboxTab project={this.props.project} onClose={this.handleClose} importAssets={this.importAssets}/></Tab.Pane>
      },
*/
    ]

    if (!imageOnly) {
        this.panes.push(
            {menuItem: 'Search',
              render: () => <Tab.Pane style={{height: this.state.modalHeight}} attached={false} >
              <CombinedSearchTab project={this.props.project} onClose={this.handleClose}
                               imageOnly={imageOnly} card={card}
                               importAssets={this.importAssets} query={this.props.query}/></Tab.Pane>
        })

        this.panes.push(
            {menuItem: 'Blank',
              render: () => <Tab.Pane style={{height: this.state.modalHeight, backgroundColor: 'red'}} attached={false}>
              <BlankTab project={this.props.project} onClose={this.handleClose} createCards={this.createCards}/></Tab.Pane>
        })
    }

  }

  handleOpen = () => this.setState ({modalOpen: true, modalHeight: findDOMNode(this).offsetHeight * 0.8})

  handleClose = () => {
    if (this.props.onClose)
        this.props.onClose()
    this.setState ({modalOpen: false})
  }

  handleTabChange = (e,tabInfo) => {
    this.setState({activeTab: tabInfo.activeIndex})
    UserSessionUtils.setSessionPref('selectedImportTab', tabInfo.activeIndex)
  }

  importCards(cards) {
    this.props.importCards (cards)
    this.handleClose ();
  }

  createCards(cards) {
      this.props.createCards (cards)
      this.handleClose ();
  }

  importAssets(assets, source) {
    this.props.importAssets (assets, source)
    this.handleClose ();
  }

  uploadAssets(cards, files) {
      this.props.uploadAssets (cards, files)
      this.handleClose ();
  }

  render() {
    const {imageOnly, card, title} = this.props
    if (imageOnly && !card)
      throw new Error("ImageOnly mode requires a Card")
    const defaultTrigger = <img className='pointer utNewCard2 mr05' src='/static/svg/NewCardLabelled.svg' height={30} />
    const trigger = <span onClick={this.handleOpen} data-tip={title || DEFAULT_TITLE}>{this.props.trigger || defaultTrigger}</span>

    return (
      <Modal trigger={trigger}
             open={this.state.modalOpen}
             onClose={this.handleClose}
             closeOnDimmerClick={false}
             size='large'
      >
        <X size={36} onClick={this.handleClose} className='absTopRight'/>
        <Header icon={<PlusSquare size={30} style={{margin: '-0.3rem 1rem 0 0'}}/>} content={title || DEFAULT_TITLE}/>
        <Modal.Content scrolling>
          <Tab className='tab-aux'
               style={{marginBottom: 0}}
               activeIndex={this.state.activeTab}
               grid={{ paneWidth: 12, tabWidth: 3 }}
               menu={{ attached: false, tabular: false }}
               panes={this.panes}
               onTabChange={this.handleTabChange}/>
        </Modal.Content>
      </Modal>
    )
  }
}
