import React, {Component} from 'react'
import {Header, Modal} from 'semantic-ui-react'
import {Paperclip} from 'react-feather'

import uiStateStore from '/store/uiStateStore'
import AttachmentComponent from "../unmodals/AttachmentsPane";

export default class ImageChooserModal extends Component {
    state = { modalOpen: false }

    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => this.setState({ modalOpen: false })

    render() {
        if (uiStateStore.voMode)
            return (
                <span></span>
            )

        const iconColor = this.props.cardId? '#555' : '#e0e0e0'
        const iconStyle = this.props.cardId? {marginBottom: '0.3rem', marginLeft: '0.5rem'} : {} // -6px

        const tip = "Change Card Image"

        return (
            <Modal className='attmModal'
                    // trigger={<Menu.Item name='comments' />}
                    trigger={<span>Add Attachments...</span>}
                    open={this.state.modalOpen}
                    onOpen={this.handleOpen}
                    onClose={this.handleClose}
                    style={{height: '70%'}}
            >
                <Header icon={<Paperclip size={30} style={{margin: '-0.3rem 1rem 0 0'}}/>} content={"Attach document(s) to card" }/>
                <Modal.Content>
                    modal content <AttachmentComponent card={this.props.card}/>
                </Modal.Content>
{/*
                <Modal.Actions centered>
                    <Button color='blue' onClick={this.handleClose} >
                        Done
                    </Button>
                </Modal.Actions>
*/}
            </Modal>
        )
    }
}
