import React, {Component} from 'react'
import {Header, Icon, Modal} from 'semantic-ui-react'
import {ZoomIn} from 'react-feather'
import {ToastContainer} from 'react-toastify'
import {ToastConfig} from 'config/Constants';
import ReactTooltip from 'react-tooltip'

export default class ImageModal extends Component {
    constructor(props) {
        super(props)
        this.state = { modalOpen: false }
    }

    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => this.setState({ modalOpen: false })

    // TODO experiment with fade
    fadeIn = () => {
        let modalElem = document.querySelector('.imageModal') // findDOMNode only returns trigger
        if (!modalElem)
            return
        let opacity = 0
        let fader = setInterval( () => {
            opacity += 0.1
            modalElem.style.opacity = opacity
            if (opacity >= 1)
                clearInterval(fader)
        }, 300 )
    }

    // trigger={<span className="zoom" onClick={this.handleOpen}><Icon name="zoom in " style={{color: "#555"}} /></span>}

    render() {
        const iconStyle = {marginBottom: '-6px', marginLeft: '0.5rem'}

        return (
            <Modal className='imageModal' basic large
                   trigger={<span className="zoom" onClick={this.handleOpen}><ZoomIn size={24} style={iconStyle}/></span>}
                closeOnDocumentClick={true}
                open={this.state.modalOpen}
                dimmer="blurring"
                onMount={this.fadeIn}
                onClose={this.handleClose}
                // size='large'
            >
                <Header content={this.props.title}/>
                <Icon name="close" color='white' size="big" style={{float: 'right'}} onClick={this.handleClose}/>
                <Modal.Content onClick={this.handleClose}>
                    <img src={this.props.image} className="image" style={{width: '100%', cursor: 'zoom-out'}}/>

                    <ToastContainer autoClose={ToastConfig.duration}
                                    // type="success"
                                    closeOnClick
                                    pauseOnHover
                    />
                    <ReactTooltip effect="solid" />
                </Modal.Content>
            </Modal>
        )
    }
}
