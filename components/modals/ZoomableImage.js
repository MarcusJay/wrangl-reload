import React, {Component} from 'react'
import {Header, Icon, Modal} from 'semantic-ui-react'
import {ToastConfig} from 'config/Constants';
import {ToastContainer} from 'react-toastify'
import ReactTooltip from 'react-tooltip'

export default class ZoomableImage extends Component {
    constructor(props) {
        super(props)
        this.state = { modalOpen: false }
    }

    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => this.setState({ modalOpen: false })

    previousImage = () => this.props.previousImage()

    nextImage = () => this.props.nextImage()

    onKeyPress = (ev) => {
        if (ev.key === 'rightArrow')
            this.nextImage()
        else if (ev.key === 'leftArrow')
            this.previousImage()
    }

    // TODO experiment with fade
    fadeIn = () => {
        let modalElem = document.querySelector('.imageModal') // findDOMNode only returns trigger
        if (!modalElem)
            return
        let opacity = 0
        let fader = setInterval( () => {
            opacity += 0.1
            modalElem.style.opacity = opacity
            if (opacity >= 1)
                clearInterval(fader)
        }, 300 )
    }

    render() {
        // const imageClass = "cardImage " + this.props.size || ''
        let trigger
        if (this.props.size)
            trigger = <div className={'cardImage' +' ' +this.props.size} style={{backgroundImage: 'url(' + (this.props.image) + ')'}}>&nbsp;</div>
        else if (this.props.height)
            trigger = <div className='cardImage' style={{height: this.props.height, backgroundImage: 'url(' + (this.props.image) + ')'}}>&nbsp;</div>
        else {
            trigger = <div>&nbsp;</div>
        }

        if (this.props.readonly === true) {
            return trigger
        }
        return (
            <Modal className='imageModal' basic
                   trigger={trigger}
                closeOnDocumentClick={true}
                open={this.state.modalOpen}
                onMount={this.fadeIn}
                onOpen={this.handleOpen}
                onClose={this.handleClose}
                size='large'

            >
                <Header content={this.props.title}/>
                <Icon name="close" color='white' size="big" style={{float: 'right'}} onClick={this.handleClose}/>
                <Modal.Content className="imageDetail" >
                    <img src={this.props.image} style={{width: '100%'}} onClick={this.handleClose}/>

                    <ToastContainer autoClose={ToastConfig.duration}
                                    // type="success"
                                    closeOnClick
                                    pauseOnHover
                    />
                    <ReactTooltip effect="solid" />
                </Modal.Content>
            </Modal>
        )
    }
}
