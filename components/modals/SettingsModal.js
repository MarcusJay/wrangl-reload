import React, {Component} from 'react'
import {Header, Modal} from 'semantic-ui-react'
import {Settings, X} from 'react-feather'


export default class SettingsModal extends Component {
    constructor(props) {
        super(props)
        this.state = { modalOpen: false }
    }

    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => this.setState({ modalOpen: false })

    render() {
        return (
            <Modal className='imageModal import-modal'
                trigger={<div onClick={this.handleOpen}><Settings className="third" size={24} data-tip="Board Settings" data-place="left"/></div>}
                open={this.state.modalOpen}
                onClose={this.handleClose}
                dimmer="blurring"
                size='small'
            >
                <Header icon={<Settings style={{marginRight: '1rem'}} />} content={"Board Settings for " +this.props.project.title}/>
                <Modal.Content>
                    <X onClick={() => this.handleClose()} style={{color: '#777', float: 'right', pointer: 'cursor'}}/>
                    <div>Coming soon...</div>
                    <div>Privacy settings...</div>
                    <div>Notification settings...</div>
                    <div>Reaction settings...</div>
                    <div>Set Admins...</div>
                    <br/>
                    {/*<div>Purge Cards..</div>*/}
                    {/*<div>Manage Members</div>*/}
                </Modal.Content>
            </Modal>
        )
    }
}
