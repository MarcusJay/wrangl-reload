import React, {Component} from 'react'
import {findDOMNode} from 'react-dom'
import {Button, Dimmer, Form, Grid, Icon, Loader} from 'semantic-ui-react'

import ReactTooltip from 'react-tooltip'
import {DOCUMENT, IMAGE, SearchConfig} from '/config/Constants'
import {getShortUrl} from '/utils/solo/getShortUrl'
import normalizeUrl from 'normalize-url'

import WebImportService from '/services/import/WebImportService'
import WebSearchService from '/services/WebSearchService'
import PreviewCard from '/components/common/PreviewCard'
import CardModel from '/models/CardModel'
import CardSizeSlider from "/components/common/CardSizeSlider";
import uiStateStore from '/store/uiStateStore'
import ResponsiveUtils from '/utils/ResponsiveUtils'
import {EventSource} from "/config/EventConfig";
import {getUrlComponents} from "../../../utils/solo/getUrlComponents";
import {getUrlType} from "../../../utils/solo/getUrlType";
import {isUrl} from "../../../utils/solo/isUrl";
import {randomIntBetween} from "../../../utils/solo/randomlntBetween";
import RU from "../../../utils/ResponsiveUtils";
import {GOOGLE_DOC} from "../../../config/Constants";
import Toaster from "../../../services/Toaster";
import cardStore from "../../../store/cardStore";
import projectStore from "../../../store/projectStore";

const searches = ['art deco', 'gothic', 'victorian', 'wedding bouquets', 'tropical', 'beach', 'industrial', 'sushi', 'pizza', 'star wars', 'parks and recreation', 'marvel']

export default class CombinedSearchTab extends Component {
    constructor(props) {
        super (props)
        this.loadingMsg = ''
        this.state = {
            modalOpen: false,
            inputText: uiStateStore.previousSearch || this.props.query || '',
            searchResults: [],
            lastKeyPress: 0,
            importableAssets: [],
            selectedAssets: {},
            isLoading: false,
            importing: false,
            elem: null,
            showShareWarning: false
        }

        // this.handleClose = this.handleClose.bind (this)
        // this.handleOpen = this.handleOpen.bind (this)
        this.toggleSelectCard = this.toggleSelectCard.bind (this)
        this.importSelectedAssets = this.importSelectedAssets.bind (this)
        this.handleInputChange = this.handleInputChange.bind (this)
    }

    // see parent importmodal
    // handleOpen = () => this.setState ({modalOpen: true})

    // handleClose = () => this.setState ({modalOpen: false})

    showError = () => {
        this.setState({revealError: true})
    }

    handleInputChange(ev) {
        const text = ev.target.value
        this.setState({inputText: text})
        console.log('onCHANGE: ' +text)
    }

    handleInputSubmit(ev) {
        // const now = new Date ()
        // const elapsed = this.state.lastKeyPress === 0 ? 0 : now - this.state.lastKeyPress

        this.setState({searchResults: []})
        const text = this.state.inputText
        console.log('input Submit: ' +text)
        // const isUrl = isUrl (text)
        this.setState({searchResults: [], isUrl: isUrl(text)})

        if (!isUrl(text) && text.length < SearchConfig.MIN_SEARCH_CHARS) {
            return
        }

        console.log('change. value: ' +text)

        if (isUrl(text))
            this.handleUrl (ev, text.trim())
        else
            this.handleSearch (ev, text.trim())

    }

    handleKeyPress(ev) {
        console.log('input Submit: ' +this.state.inputText)

        if (ev.key === 'Enter') {
            this.handleInputSubmit(ev)
        }
    }

    handleUrl(ev, url) {
        this.loadingMsg = 'Reading url...'

        url = normalizeUrl(url)
        const urlParts = getUrlComponents (url)
        const urlType = getUrlType (url)
        this.setState ({isLoading: true, urlType: urlType})
        let urlCard = new CardModel()
        let scraperCards = []

        // 0. Check google sharing
        if (urlType === GOOGLE_DOC && (!urlParts.search || !urlParts.search.includes('sharing'))) {
            this.setState({showShareWarning: true})
        }


        // 1. URL is an image
        if (urlType === IMAGE) {
            urlCard.type = IMAGE
            // urlCard.selected = true // auto-select in pane if only item.
            if (!urlCard.image) {
                urlCard.image = urlParts.href
            }
            this.state.searchResults.push (urlCard)
            this.toggleSelectCard(null, urlCard)
            this.setState ({searchResults: this.state.searchResults, isLoading: false, disableZoom: true})
        } else {

            WebImportService.fetchEverythingFromUrl (url).then (response => {
                if (!response.data.success) {
                    this.setState ({searchResults: [], isLoading: false, error: response.data.error})
                }

                const metaTags = response.data.metaTags
                const scrapedImages = response.data.images
                const date = response.data.date

                urlCard.title = WebImportService.getNameFromMetaTags (metaTags) || getShortUrl (url, true)
                urlCard.image = WebImportService.getImageFromMetaTags (metaTags)
                const dim = WebImportService.getImageDimensionsFromMetaTags (metaTags)
                if (dim) {
                    urlCard.imageWidth = dim.width
                    urlCard.imageHeight = dim.height
                }

                urlCard.url = urlParts.href
                urlCard.description = WebImportService.getDescFromMetaTags (metaTags)
                urlCard.source = WebImportService.getHostFromMetaTags (metaTags) || urlParts.host.toLowerCase ()
                urlCard.sourceIcon = WebImportService.getIconFromMetaTags (metaTags)
                urlCard.sourceType = EventSource.ScrapeUrl
                urlCard.type = urlType
                urlCard.domId = 'siteCard'

                // scale image if needed
                const vwWidth = ResponsiveUtils.getViewPortWidth ()
                if (urlCard.imageWidth && vwWidth > 0 && Number (urlCard.imageWidth) > (vwWidth * 0.7)) {
                    urlCard.imageWidth = '40vw'
                    urlCard.imageHeight = '50vw'
                }

                // 2. URL is a document (eg pdf).
                if (urlType === DOCUMENT || urlType === GOOGLE_DOC) {
                    this.state.searchResults.push (urlCard)
                    this.toggleSelectCard(null, urlCard)
                    this.setState ({searchResults: this.state.searchResults, isLoading: false})

                    // 3. URL has contents
                } else if (scrapedImages) {
                    // TODO rather than filter out inline image data assets, support them using same methods as file uploads.
                    let validImages = response.data.images.filter (img => !img.src || !img.src.startsWith ('data:'))
                    console.log ("Found " + validImages.length + " importable items.")

                    if (validImages && validImages.length > 0) {
                        scraperCards = validImages.map (asset => {
                            return new CardModel.FromImportableAsset (asset, urlParts.hostname)
                        })
                    }
                    this.state.searchResults.push (urlCard)
                    this.setState ({searchResults: this.state.searchResults.concat (scraperCards), isLoading: false})
                }
            }).catch (error => {
                this.setState ({searchResults: [], isLoading: false, error: error})
            })
        }

    }

    handleSearch(ev, query) {
        this.loadingMsg = 'Searching...'
        const self = this

        if (!ev || ev.key === 'Enter' || ev.target.id === 'sicon') {
            query = query.toLowerCase ()
            let results, webResults, imageResults
            this.setState ({isLoading: true, inputText: query})

            // Fetch 2 pages of results from google and images for starters. TODO infinite scroll, etc.
            WebSearchService.search (query, 1).then (response => {
                if (!response.data || response.data.length === 0)
                    throw new Error ('search failed')

                webResults = response.data.find (results => results.src === 'Google').items
                imageResults = response.data.find (results => results.src === 'Google_Images').items
                WebSearchService.search (query, 2).then (response => {
                    if (!response.data || response.data.length === 0)
                        throw new Error ('search failed')
                    webResults = webResults.concat (response.data.find (results => results.src === 'Google').items)
                    imageResults = imageResults.concat (response.data.find (results => results.src === 'Google_Images').items)
                    webResults = webResults.map (item => new CardModel.FromWebSearch (item, 'google'))
                    imageResults = imageResults.map (item => new CardModel.FromWebSearch (item, 'google images'))
                    results = imageResults.concat(webResults)
                    // TODO options

                    self.setState ({searchResults: self.state.searchResults.concat(results), isLoading: false})
                    ReactTooltip.rebuild()
                    uiStateStore.previousSearch = query
                })

            }).catch (error => {
                console.error ("Error searching for " + query, error)
                this.setState ({isLoading: false})
                throw new Error (error)
            })
        }
    }

    toggleSelectCard(target, card) {
        console.log ("selectCard: target: " + target + ", card: " + card.title);
        if (!this.state.selectedAssets[card.id])
            this.state.selectedAssets[card.id] = card
        else
            delete this.state.selectedAssets[card.id]
        this.setState (this.state)
    }

    importSelectedAssets() {
        this.setState ({importing: true})
        let assetArray = Object.values (this.state.selectedAssets)
        if (assetArray.length === 0) {
            console.error ("importSelectedAssets: 0 cards selected.")
            return
        }
        const self = this
        cardStore.createCardsFromAssets (assetArray, projectStore.currentProject).then (response => {
            self.props.refreshCards && this.props.refreshCards ()
            if (self.props.onClose)
                self.props.onClose()
        }).catch( error => {
            console.error(error)
            Toaster.error("There was a problem with the image upload. Please try a different image.")
        }).finally ( function() {
            self.setState({importing: false, isLoading: false, selectedAssets: []})
        })
    }

    componentDidMount() {
        this.setState ({elem: findDOMNode (this)})
        document.querySelector('#webSearch').focus() // TODO get child of our elem instead

        if (this.props.query === 'random') {
            const index = randomIntBetween(0, searches.length-1)
            const search = searches[index]
            this.handleSearch(null, search)
        }
    }

    render() {
        let selectedCardCount = Object.keys (this.state.selectedAssets).length
        const { isUrl, error, revealError, showShareWarning, disableZoom } = this.state
        const url = isUrl && this.state.inputText? this.state.inputText.trim() : 'search'
        const resultCount = this.state.searchResults.length

        const device = RU.getDeviceType()
        const mobile = ['computer','tablet'].indexOf(device) === -1

        return (
            <div className="tabWrap">
                <Dimmer.Dimmable dimmed={this.state.isLoading}>
                    <Dimmer active={this.state.isLoading} inverted>
                        <Loader inverted style={{marginTop: '0', height: '10%'}}>
                            <span className='jFG'>{this.loadingMsg}</span>
                        </Loader>
                    </Dimmer>

                    <div className='mb1'>
                        <span className="mr1">Search or URL</span>
                            <button disabled={!isUrl && selectedCardCount === 0} className='jBtn mr05'
                                    onClick={this.importSelectedAssets}>

                                Add {selectedCardCount} Item{selectedCardCount === 1?'':'s'}
                            </button>
                            <button className='jBtn mt05' onClick={this.props.onClose}>
                                cancel
                            </button>
                            {!mobile &&
                            <CardSizeSlider cardContainer="combinedSearchTab" disabled={resultCount === 0 || disableZoom}/>
                            }
                    </div>

                    <div className="web-import-box ">
                        <input name='Search'
                               type='text'
                                onChange={(ev) => {             // TODO handles keys, but ignores Enter. Better way?
                                    this.handleInputChange (ev)
                                }}
                                onKeyPress={(ev) => {           // TODO IGNORES keys??, but HANDLES Enter. Better way?
                                    this.handleKeyPress (ev)
                                }}
                                value={this.state.inputText}
                                id="webSearch"
                                className='jInputBG jFG'
                                style={{width: mobile? '90%':'50%'}}
                                placeholder='Enter a search term or a URL'
                        />
{/*
                        <span className='abs' style={{right: '1rem', top: '-10px'}}>
                            <Icon name="search" className='jSec' onClick={(ev) => { this.handleInputSubmit(ev)}}/>
                        </span>
*/}


                        <div className="browse-page-assets">
{/*
                                <div className="itemCount">{resultCount} Result{resultCount !== 1 ? 's' : ''}:
                                </div>
*/}
                                {resultCount > 0 && !isUrl &&
                                <div className='mb1 mbt1 itemCount'>{resultCount} result{resultCount ===1? '':'s'}. Click to select the items you'd like to import.</div>
                                }
                                {error && isUrl &&
                                <div>
                                    We're having trouble reaching {getShortUrl(url, true)} for images. Please try again shortly.
                                    <p>
                                    {!revealError &&
                                    <a href='' onClick={this.showError}>Show error</a>
                                    }
                                    {revealError &&
                                    <span>{error.toString()}</span>
                                    }
                                    </p>
                                </div>
                                }

                                <div className='mt1'>
                                {resultCount > 0 && this.state.searchResults.map ((card, i) => {
                                    return (
                                        <PreviewCard
                                            card={card}
                                            mobile={mobile}
                                            containerName='combinedSearchTab'
                                            containerElem={this.state.elem}
                                            key={'pcc'+i}
                                            index={i}
                                            assetSize={card.imageWidth ? card.imageWidth : card.type === IMAGE? '60%' : null}
                                            id={card.id}
                                            text={card.title}
                                            view={card.type === IMAGE? IMAGE : 'card'}
                                            selectCard={this.toggleSelectCard}
                                            selected={this.state.selectedAssets[card.id] !== undefined || card.selected || isUrl}
                                            // deleteCard={null}
                                        />

                                    )
                                })}
                                </div>
                            </div>

                            {showShareWarning &&
                            <div className='jPink pad1'>
                                Note: This google doc link does not appear to be shared. <br/><br/>
                                The best way to be sure that your doc will be viewable to everyone, is to copy the link from the 'Share' button
                                at the top right of your google document, rather than from the browser's address bar.
                            </div>
                            }


                    </div>
                </Dimmer.Dimmable>

            </div>
        )
    }
}
