import {Component} from 'react'

export default class PinterestTab extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return(
            <div>
                <div className="tab-description">Import items from your pinterest boards:</div>
                    <div className="pinterest-import-box">
                        <div className="center-msg">Pinterest Import Coming Soon...</div>
                    </div>
                </div>

        )
    }
}