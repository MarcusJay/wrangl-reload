import {Component} from 'react'
import {findDOMNode} from 'react-dom'
import {Button, Grid, Input} from 'semantic-ui-react'

export default class BlankTab extends Component {
    constructor(props) {
        super(props)
        this.state = {modalOpen: false, count: 1, elem: null}
    }

/*
    handleOpen = () => this.setState ({modalOpen: true})

    handleClose = () => this.setState ({modalOpen: false})
*/

    handleCountChange(ev) {
        this.setState({count: ev.target.value})

    }

    createCard = (ev) => {
        const blank = _createCardData(1)
        this.props.createCards([blank])

    }

    componentWillReceiveProps(newProps) {

    }

    componentDidMount() {
        this.setState ({elem: findDOMNode (this)})
    }

    render() {
        const cardCount = this.state.count

        return(
            <div className="tabWrap">
                <p className="tab-description mb1">Create a new empty item </p>
                <button className='button go mt1'
                        onClick={this.createCard}>
                    Create!
                </button>

            </div>
        )
    }
}

function _createCardData(index) {

    return {
        title: 'Untitled ' +index,
    }
}