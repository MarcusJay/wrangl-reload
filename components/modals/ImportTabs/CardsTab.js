import {Component} from 'react'
import {findDOMNode} from 'react-dom'
import {Dimmer, Form, Grid, Icon, Input, Loader} from 'semantic-ui-react'

import cardStore from '/store/cardStore'
import projectStore from '/store/projectStore'
import CardModel from '/models/CardModel'
import PreviewCard from '/components/common/PreviewCard'
import CardSizeSlider from "/components/common/CardSizeSlider";
import {SearchConfig} from '/config/Constants'
import userStore from "../../../store/userStore";
import RU from "../../../utils/ResponsiveUtils";
import {findCards} from "../../../services/CardApiService";

export default class CardsTab extends Component {
    constructor(props) {
        super(props)
        this.moveCard = this.moveCard.bind(this)
        this.deleteCard = this.deleteCard.bind(this)
        this.toggleSelectCard = this.toggleSelectCard.bind(this)
        this.importSelectedCards = this.importSelectedCards.bind(this)
        // this.handleSearchChange = this.handleSearchChange.bind(this)
        this.state = {userCards: [], selectedCards: {}, searching: false, importing: false, loading: false,
                      query: '', searchResults: [], lastKeyPress: 0, isVirgin: true, waiting: false, elem: null}
    }

    // onKeyPress(ev) {
    // }

    onChange(ev) {
        let query = ev.target.value
        console.log('change: query = ' +query)
        let now = new Date()
        let elapsed = this.state.lastKeyPress === 0? 0 : now - this.state.lastKeyPress
        this.setState({query: ev.target.value, lastKeyPress: now})

/*
        if (elapsed < SearchConfig.MIN_DELAY_MS) {
            this.setState({waiting: true, query: ev.target.value, lastKeyPress: now})
            console.log('Skipping rapid typing')
            setTimeout( () => {
                this.setState({waiting: false})
            }, SearchConfig.MIN_DELAY_MS)
        }
*/

        setTimeout( () => {
            if (query.length >= SearchConfig.MIN_SEARCH_CHARS) {
                this.submitSearch (query)
            } else if (!this.state.isVirgin) {
                console.log ('Clearing search')
                this.setState ({searchResults: this.state.userCards})
            } else {
                console.log('Skipping search ' + query + ' at ' +elapsed+ 'ms')
            }
            this.setState ({waiting: false})

        }, SearchConfig.QUERY_DELAY_MS )

    }

/*
    submitSearch(ev) {
        let query = ev.target.value

        StateChangeRequestor.awaitStateChange(REQUEST_CARD_COMMENT, {card: this.props.card, commentText: query})
            .then( comment => {
                console.log("Received back: " +comment.comment_text)
                this.setState({comments: [comment, ...this.state.comments], inputText: '' })
            })
    }
*/

    submitSearch(query) {
        console.log("Searching for " +query+ "...")

        this.setState ({searching: true, isVirgin: false})

        findCards (query.toLowerCase()).then (response => {
            if (!response.data || response.data.length === 0)
                throw new Error ('Card search failed')

            this.setState ({
                searching: false,
                searchResults: response.data.cards.map (apiCard => {
                    return new CardModel (apiCard)
                })
            })
        }).catch (error => {
            console.error ("Error searching for " + query, error)
            this.setState ({searching: false})
            throw new Error (error)
        })
    }

    toggleSelectCard(target, card) {
        let selCards = this.state.selectedCards
        const {imageOnly} = this.props

        console.log("selectCard: target: " +target+ ", card: " +card.title);

        if (!selCards[ card.id ]) {
            if (imageOnly) {
                selCards = {} // no multi-select
            }

            selCards[card.id] = card
        }
        else
            delete selCards[ card.id ]
        this.setState({selectedCards: selCards})

/*      this approach works too, but it's useful to have a map collection of selected cards to operate on
        if (!this.state[ card.domId ])
            this.setState( {[card.domId]: true })
        else
            this.setState( {[card.domId]: false })
*/
    }

    importSelectedCards() {
        this.setState({importing: true})
        let cardArray = Object.keys(this.state.selectedCards)
        if (cardArray.length === 0) {
            console.error("importSelectedCards: 0 cards selected.")
            return
        }

        this.props.importCards(cardArray)
    }

    assignImage = () => {
        const {imageOnly, card} = this.props
        const {selectedCards} = this.state
        if (!imageOnly || !card || !selectedCards || selectedCards.length === 0)
            return

        this.setState({importing: true})
        let imageCardId = Object.keys(selectedCards)[0]
        let image = selectedCards[imageCardId].image
        if (!image)
            throw new Error('assignImage not founod. In progress')
        cardStore.saveCard ({id: card.id, image: image}).then (card => {
            card.image = image
            this.setState ({importing: false})
        }).catch (error => {
            this.setState ({importing: false, error: error})
        }).finally( () => {
            if (this.props.onClose)
                this.props.onClose()

        })
    }


    moveCard() { /*No-op*/ }
    deleteCard() { /*No-op*/ }

    componentDidMount() {
        const userId = userStore.currentUser.id
        this.setState({loading: true})
        cardStore.getUserCards(userId, false, true).then( userCards => { // no caching, use cardmodel
            const nonProjectApiCards = cardStore.userCards.filter( card => card.projectId !== this.props.project.id )
            this.setState ({ elem: findDOMNode(this), userCards: nonProjectApiCards, searchResults: nonProjectApiCards, loading: false })
        })
    }

    render() {
        const {imageOnly} = this.props
        const { importing, loading, query, searchResults } = this.state
        const project = projectStore.currentProject
        let selectedCardCount = Object.keys(this.state.selectedCards).length
        const device = RU.getDeviceType()
        const mobile = ['computer','tablet'].indexOf(device) === -1

        return(
            <div className="tabWrap">
                <Dimmer.Dimmable dimmed={importing}>
                {/*<Dimmer active={this.state.searching} inverted>*/}
                    {/*<Loader active={this.state.searching} style={{marginTop: '0', height: '10%'}}>Searching cards...</Loader>*/}
                {/*</Dimmer>*/}
                <Dimmer active={importing} inverted>
                    <Loader inverted className='secondary' style={{marginTop: '0', height: '10%'}}>Importing {selectedCardCount} items into '{project.title}'</Loader>
                </Dimmer>
                <Dimmer active={loading} inverted>
                    <Loader inverted className='secondary' style={{marginTop: '0', height: '10%', color: '#aaa'}}>Loading your items...</Loader>
                </Dimmer>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={6}>
                            <div className="tab-description">
                                {imageOnly? "Choose an image from your other boards" : "Import items from your other boards:"}
                            </div>
                        </Grid.Column>
                        <Grid.Column width={6} >
                            {imageOnly &&
                            <button disabled={selectedCardCount === 0}
                                    className={'jBtn mr05' +(selectedCardCount === 0? ' jFourth disabled':'')}
                                    onClick={this.assignImage}>
                                Use image
                            </button>
                            }
                            {!imageOnly &&
                            <button disabled={selectedCardCount === 0} className='jBtn mr05'
                                    onClick={this.importSelectedCards}>
                                Add {selectedCardCount} item{selectedCardCount === 1? '':'s'}
                            </button>
                            }
                            <button className='jBtn mt05' onClick={this.props.onClose}>
                                cancel
                            </button>
                        </Grid.Column>
                        {!mobile &&
                        <Grid.Column width={4}>
                            <CardSizeSlider cardContainer="cardsTab" disabled={!searchResults || searchResults.length === 0}/>
                        </Grid.Column>
                        }
                    </Grid.Row>
                </Grid>
                <div className="card-import-box scroller">
                    <Form>
                        <Input icon={<Icon name="search" />}
                                    name='Search'
                                    onChange={(ev) => {this.onChange (ev)}}
                                    // onKeyPress={(ev) => {this.onKeyPress (ev)}}
                                    value={query}
                                    id="cardSearch"
                                    className='jInputBG jFG'
                                    style={{width: mobile? '90%':'50%'}}
                                    placeholder='Filter...'
                        />
                        <div className="browse-cards">
                        {searchResults.map( (cardModel, i) => {
                            return (
                                <PreviewCard
                                    card={cardModel}
                                    mobile={mobile}
                                    containerName='cardsTab'
                                    containerElem={this.state.elem}
                                    key={'csr'+i}
                                    index={i}
                                    ltr={String.fromCharCode(65+i)}
                                    id={cardModel.id}
                                    text={cardModel.title}
                                    readOnly={true}
                                    selectCard={this.toggleSelectCard}
                                    // selected={this.state[ cardModel.domId ] === true}
                                    selected={this.state.selectedCards[ cardModel.id ] !== undefined}
                                    moveCard={null}
                                    deleteCard={null}
                                />
                            )
                        })}
                    </div>
                    </Form>
                </div>
                </Dimmer.Dimmable>
            </div>
        )
    }
}

{/*
 */}
