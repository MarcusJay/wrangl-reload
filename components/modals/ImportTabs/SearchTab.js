import React, {Component} from 'react'
import {findDOMNode} from 'react-dom'
import {Button, Dimmer, Form, Grid, Icon, Loader} from 'semantic-ui-react'

import WebSearchService from '/services/WebSearchService'
import Card from '/components/common/Card'
import CardModel from '/models/CardModel'
import CardSizeSlider from "/components/common/CardSizeSlider";
import {SearchConfig} from '/config/Constants'
import {getShortUrl} from '/utils/solo/getShortUrl'
import {isUrl} from "../../../utils/solo/isUrl";

export default class SearchTab extends Component {
  constructor(props) {
    super (props)
    this.state = {modalOpen: false, webQuery: '', searchResults: [], lastKeyPress: 0, isLoading: false, selectedAssets: [], elem: null}
    this.handleClose = this.handleClose.bind (this)
    this.handleOpen = this.handleOpen.bind (this)
    this.toggleSelectCard = this.toggleSelectCard.bind (this)
    this.importSelectedAssets = this.importSelectedAssets.bind (this)
    this.handleSearchChange = this.handleSearchChange.bind(this)
  }

  handleOpen = () => this.setState ({modalOpen: true})

  handleClose = () => this.setState ({modalOpen: false})

  handleSearchChange(ev) {
    let now = new Date()
    let elapsed = this.state.lastKeyPress === 0? 0 : now - this.state.lastKeyPress
    let allegedSearchTerm = ev.target.value;
    let actualSearchTerm = document.querySelector('#webSearch').value

    if (actualSearchTerm < SearchConfig.MIN_SEARCH_LEN) {
      return
    }

    if (ev.key === 'Enter') {

      // TODO combine searchTab with webTab.
      let siteCard = null
      if (isUrl(actualSearchTerm)) {
          siteCard = new CardModel ()
          siteCard.title = getShortUrl (actualSearchTerm, true)
          siteCard.description = actualSearchTerm
          siteCard.image = 'https://pogo.io/static/img/site.png'
          siteCard.url = actualSearchTerm
      }

      let searchStr = actualSearchTerm.toLowerCase ()
      let results
      this.setState ({isLoading: true, webQuery: searchStr})

      // Let's fetch 2 pages of results from google and images for starters. TODO infinite scroll, etc.
      WebSearchService.search (searchStr, 1).then (response => {
          if (!response.data || response.data.length === 0)
              throw new Error ('search failed')

          results = response.data.find( results => results.src === 'Google' ).items
          results = results.concat( response.data.find( results => results.src === 'Google_Images' ).items )
          WebSearchService.search (searchStr, 2).then( response => {
              if (!response.data || response.data.length === 0)
                  throw new Error ('search failed')
              results = results.concat( response.data.find( results => results.src === 'Google' ).items )
              results = results.concat( response.data.find( results => results.src === 'Google_Images' ).items )
              results = results.map (item => new CardModel.FromWebSearch (item) )
              if (siteCard)
                  results = [siteCard].concat(results)
              debugger
              this.setState ({
                  isLoading: false,
                  searchResults: results
              })
          })

      }).catch (error => {
          console.error ("Error searching for " + searchStr, error)
          this.setState ({isLoading: false})
          throw new Error (error)
      })
    }
  }

  toggleSelectCard(target, card) {
    console.log ("selectCard: target: " + target + ", card: " + card.title);
    if (!this.state.selectedAssets[card.id])
      this.state.selectedAssets[card.id] = card
    else
      delete this.state.selectedAssets[card.id]
    this.setState (this.state)
  }

  importSelectedAssets() {
    this.setState ({importing: true})
    let assetArray = Object.values(this.state.selectedAssets)
    if (assetArray.length === 0) {
      console.error ("importSelectedAssets: 0 cards selected.")
      return
    }
    this.props.importAssets( assetArray )
    this.handleClose ();
  }

  componentDidMount() {
    this.setState({elem: findDOMNode(this)})
  }

  render() {
    let selectedCardCount = Object.keys (this.state.selectedAssets).length
    return (
          <div className="tabWrap">
            <Dimmer.Dimmable dimmed={this.state.isLoading}>
              <Dimmer active={this.state.isLoading} inverted>
                <Loader inverted style={{marginTop: '0', height: '10%'}}>Searching...</Loader>
              </Dimmer>

              <Grid>
                <Grid.Row>
                  <Grid.Column width={6}>
                    <div className="tab-description">Search the Web for items:</div>
                  </Grid.Column>
                  <Grid.Column width={6}>
                    <Button disabled={Object.keys (this.state.selectedAssets).length === 0} color="blue"
                            style={{margin: '-5px 0 8px 0'}} onClick={this.importSelectedAssets}>
                      Add {selectedCardCount > 1 ? selectedCardCount + ' Items' : selectedCardCount === 1 ? selectedCardCount + ' Item' : 'Items'}
                    </Button>
                  </Grid.Column>
                  <Grid.Column width={4}>
                    <CardSizeSlider cardContainer="searchTab" disabled={selectedCardCount === 0}/>
                  </Grid.Column>
                </Grid.Row>
              </Grid>

              <div className="web-import-box scroller">
                <Form>
{/*
                <Search id="webSearch"
                        loading={this.state.searching}
                        onResultSelect={this.handleResultSelect}
                        onSearchChange={this.handleSearchChange}
                        results={this.state.searchResults}
                        placeholder='Search Web...'
                        value={this.state.webQuery}
                        {...this.props}
                />
*/}

                <Form.Input icon={<Icon name="search" onClick={(ev) => { this.handleSearchChange (ev)}}/>}
                            name='Search'             onKeyPress={(ev) => {this.handleSearchChange (ev)}}
                            id="webSearch"
                            style={{color: '#777'}}
                            placeholder='google search...'
                />

{/*
                  <Button disabled={Object.keys (this.state.selectedAssets).length === 0} color="blue"
                          style={{margin: '-5px 0 8px 0', float: 'right'}} onClick={this.importSelectedAssets}>
                    Import {selectedCardCount > 1 ? selectedCardCount + ' Items' : selectedCardCount === 1 ? selectedCardCount + ' Item' : 'Items'}
                  </Button>
*/}

                <div className="browse-page-assets">
                  {this.state.searchResults.length > 0 &&
                    <div className="itemCount">{this.state.searchResults.length} Search Results:</div>
                  }
                  {this.state.searchResults.map ((asset, i) => {
                    return (
                      <Card
                        card={asset}
                        containerName='searchTab'
                        containerElem={this.state.elem}
                        key={'sast'+i}
                        index={i}
                        ltr={String.fromCharCode (65 + i)}
                        id={asset.id}
                        text={asset.title}
                        readOnly={true}
                        selectCard={this.toggleSelectCard}
                        selected={this.state.selectedAssets[asset.id] !== undefined}
                        moveCard={null}
                        deleteCard={null}
                      />

                      /*
                       <div>Asset: <br/>
                       id: {asset.wid}<br/>
                       title: {asset.title}<br/>
                       src: {asset.src}<br/>
                       link: {asset.href}<br/>
                       count: {asset.count}<br/>
                       </div>
                       */
                    )
                  })}
                </div>
                {/*
                 <div>
                 <Form.Button onClick={this.props.handleSubmit}>
                 <Feather.Download/>Import
                 </Form.Button >

                 </div>
                 */}
              </Form>
            </div>
            </Dimmer.Dimmable>

          </div>
    )
  }
}
