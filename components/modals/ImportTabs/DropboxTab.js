import {Component} from 'react'
import {findDOMNode} from 'react-dom'
import {observer} from 'mobx-react'

import {Button, Dimmer, Grid, Icon, Loader} from 'semantic-ui-react'
import DropboxService from '/integrations/DropboxService'
import integrationStore from "/store/IntegrationStore"
import PreviewCard from "/components/common/PreviewCard"
import PreviewFolder from "/components/common/PreviewFolder"
import CardSizeSlider from "/components/common/CardSizeSlider";

@observer
export default class DropboxTab extends Component {
    constructor(props) {
        super(props)
        this.state = {selectedAssets: {}, searching: false, connecting: true, importing: false, modalOpen: false,
            query: '', searchResults: [], lastKeyPress: 0, isVirgin: true, waiting: false, elem: null}
    }

    handleOpen = () => this.setState ({modalOpen: true})

    handleClose = () => this.setState ({modalOpen: false})

    drillDown(folderPath) {
        this.setState({connecting: true})
        integrationStore.dropboxDrilldown(folderPath)
    }

    drillUp() {
        this.setState({connecting: true})
        if (!integrationStore.isRoot)
            integrationStore.dropboxDrillup()
    }

    toggleSelectCard(target, card) {
        console.log ("selectCard: target: " + target + ", card: " + card.title);
        if (!this.state.selectedAssets[card.id])
            this.state.selectedAssets[card.id] = card
        else
            delete this.state.selectedAssets[card.id]
        this.setState (this.state)
    }

    importSelectedCards() {
        this.setState ({importing: true})
        let assetArray = Object.values (this.state.selectedAssets)
        if (assetArray.length === 0) {
            console.error ("importSelectedAssets: 0 cards selected.")
            return
        }
        debugger
        this.props.importAssets (assetArray, 'dropbox')
        this.handleClose ();
    }

    resetSelectedCards() {
        this.setState({selectedAssets: {}, importing: false, searchResults: this.props.userCards})
    }

    openAuthInNewTab() {
        const dropboxAuthUrl = DropboxService.getAuthorizationUrl()
        const authWindow = window.open(dropboxAuthUrl, '_blank', 'top=200,left=200,height=500,width=600,menubar=no,location=no,resizable=no,scrollbars=no,status=no')
    }

    componentWillReceiveProps(newProps) {
    }

    componentDidMount() {
        this.setState ({elem: findDOMNode (this)})
    }

    render() {
        if (!this.state.searchResults)
            return null
        const connected = integrationStore.isDropboxAuthorized() // && integrationStore.dropboxCurrentFiles !== null
        const loading = integrationStore.dropboxLoading

        const projectId = this.props.projectId
        const fullScreen = this.props.mode === 'full'

        const selectedCardCount = Object.keys(this.state.selectedAssets).length
        const isRoot = integrationStore.dropboxNavStack.length === 0
        const containerClass = fullScreen? 'main-pane' : 'side-pane'

        // Dropbox returns folders first
        const fileStartIndex = connected? integrationStore.dropboxCurrentFiles.findIndex( item => item.type === 'file' ) : -1
        const folders = connected? integrationStore.dropboxCurrentFiles.slice(0, fileStartIndex) : []
        const resultCount = integrationStore.dropboxCurrentFiles.length

        if (!connected) {
            return (
                <div onClick={() => this.openAuthInNewTab()}>
                    <Icon name="dropbox" size="massive" style={{display: 'inline-block', marginLeft: '0.5rem', color: '#777'}}/>
                    <div className="pointer" style={{marginLeft: '0.5rem', fontSize: '2rem', display: 'inline', color: '#777'}}>
                        Connect to your Dropbox
                    </div>
                </div>
            )
        }

        return(
            <div className="tabWrap">
                <Dimmer.Dimmable dimmed={loading}>
                    <Dimmer active={loading} inverted>
                        <Loader inverted style={{marginTop: '0', height: '10%'}}>Waiting for Dropbox...</Loader>
                    </Dimmer>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={6}>
                                <div className="tab-description">Add files from your Dropbox:</div>
                            </Grid.Column>
                            <Grid.Column width={6}>
                                <Button disabled={Object.keys (this.state.selectedAssets).length === 0} color="blue"
                                        style={{margin: '-5px 0 8px 0'}} onClick={() => this.importSelectedCards()}>
                                    Import {selectedCardCount > 1 ? selectedCardCount + ' Items' : selectedCardCount === 1 ? selectedCardCount + ' Item' : 'Items'}
                                </Button>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                <CardSizeSlider cardContainer="dropboxTab" disabled={resultCount === 0}/>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>

                    <div className="dropbox-import-box">
                        <div className='currDir' onClick={() => this.drillUp()}>
                            <span>Current Folder: </span>
                            <span>{integrationStore.dropboxCurrentFolder}</span>
                        </div>

                        <div className="browse-files">
                            {integrationStore.dropboxCurrentFiles.map( (card, i) => {
                                if (card.type === 'folder')
                                    return (
                                    <PreviewFolder
                                        folder={card}
                                        containerName='dropboxTab'
                                        containerElem={this.state.elem}
                                        id={card.id}
                                        key={'dbf'+i}
                                        index={i}
                                        openFolder={(folder) => this.drillDown(folder)}
                                    />
                                    )
                                else
                                    return (
                                    <PreviewCard
                                        card={card}
                                        containerName='dropboxTab'
                                        containerElem={this.state.elem}
                                        id={card.id}
                                        key={'dbc'+i}
                                        index={i}
                                        text={card.title}
                                        view={'card'} // TODO
                                        selectCard={(target, card) => this.toggleSelectCard(target, card)}
                                        openFolder={(folder) => this.drillDown(folder)}
                                        selected={this.state.selectedAssets[ card.id ] !== undefined}
                                    />
                                )
                            })}
                        </div>
                    </div>
                </Dimmer.Dimmable>
            </div>
        )
    }
}