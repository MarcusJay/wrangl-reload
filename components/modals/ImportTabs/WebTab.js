import {Component} from 'react'
import {findDOMNode} from 'react-dom'
import {Button, Dimmer, Form, Grid, Icon, Loader} from 'semantic-ui-react'
import {getShortUrl} from '/utils/solo/getShortUrl'
import WebImportService from '/services/import/WebImportService'
import WebSearchService from '/services/WebSearchService'
import CardModel from '/models/CardModel'
import Card from '/components/common/Card'
import CardSizeSlider from "/components/common/CardSizeSlider";

import {IMAGE, SITE} from "/config/Constants"
import {getUrlComponents} from "../../../utils/solo/getUrlComponents";
import {getUrlType} from "../../../utils/solo/getUrlType";

export default class WebTab extends Component {
  constructor(props) {
    super (props)
    this.handleUrlChange = this.handleUrlChange.bind (this)
    this.toggleSelectCard = this.toggleSelectCard.bind (this)
    this.importSelectedAssets = this.importSelectedAssets.bind (this)
    this.state = {importableAssets: [], fetchingAssets: false, selectedAssets: {}, importing: false, elem: null}
  }

  handleUrlChange(ev, data) {

      // console.log(ev)
      if (ev.key === 'Enter') {
          // validate url
          // TODO

          let url = ev.target.value.trim()
          let urlParts = getUrlComponents (url)
          let baseStr = urlParts.protocol + '//' + urlParts.host
          let urlType = getUrlType (url)
          this.setState ({fetchingAssets: true, urlType: urlType})

          // User pasted an image url, so don't scrape, just grab image
          if (urlType === IMAGE) {
              let imgCard = new CardModel()
              urlCard.title = getShortUrl(url, true)
              urlCard.image = urlParts.href
              urlCard.url = urlParts.href
              this.setState({importableAssets: [urlCard], fetchingAssets: false})

          // User pasted a site url, and they may want the site itself, or items in the site.
          // NOTE: Site or DOCUMENT
          } else {
              // Start with google results
              WebSearchService.search (url, 1).then (response => {
                  let googleCard, giCard
                  if (!response.data || response.data.length === 0) {
                      googleCard = null
                      giCard = null
                  } else {
                      let gResponse = response.data.find (results => results.src === 'Google')
                      let gImageResponse = response.data.find (results => results.src === 'Google_Images')
                      let gResults = gResponse? gResponse.items : null
                      let gImageResults = gImageResponse? gImageResponse.items : null

                      googleCard = gResults && gResults.length > 0? new CardModel.FromWebSearch (gResults[0]) : null
                      giCard = gImageResults && gImageResults.length > 0? new CardModel.FromWebSearch (gImageResults[0]) : null
                  }

                  let siteCard = new CardModel ()
                  siteCard.title = getShortUrl (url, true)
                  siteCard.description = "The Entire Web Page"
                  siteCard.image = 'https://pogo.io/static/img/site.png'
                  siteCard.url = urlParts.href

                  // scrape
                  WebImportService.fetchExternalUrl (urlParts.href).then (response => {
                      if (!response.data || !response.data.success || !response.data.images)
                          throw new Error ('external url returned problem or no importable images')

                      // TODO rather than filter out inline image data assets, support them using same methods as file uploads.
                      let filteredImages = response.data.images.filter (img => !img.src || !img.src.startsWith ('data:'))
                      console.log ("Found " + filteredImages.length + " importable items.")

                      // Assemble results
                      let cards = []
                      if (googleCard)
                          cards.push(googleCard)
                      if (giCard)
                          cards.push(giCard)
                      if (!googleCard && !giCard)
                          cards.push(siteCard)
                      if (filteredImages && filteredImages.length > 0) {
                          let assetCards = filteredImages.map (asset => {
                              return new CardModel.FromImportableAsset (asset, urlParts.hostname)
                          })
                          // cards.push(siteCard)
                          cards.concat(assetCards)
                      }
                      this.setState ({importableAssets: cards, fetchingAssets: false})

                  }).catch (error => {
                      console.error ("Error fetching url: ", error)
                      this.setState ({fetchingAssets: false})
                      throw new Error (error)
                  })
              }).catch(error => {
                  console.error ("Error searching for " + url, error)
                  this.setState ({fetchingAssets: false})
                  throw new Error (error)
              })

          }
      }
  }

  toggleSelectCard(target, card) {
    console.log ("selectCard: target: " + target + ", card: " + card.title);
    if (!this.state.selectedAssets[card.id])
      this.state.selectedAssets[card.id] = card
    else
      delete this.state.selectedAssets[card.id]
    this.setState (this.state)
  }

  importSelectedAssets() {
    this.setState ({importing: true})
    let assetArray = Object.values(this.state.selectedAssets)
    if (assetArray.length === 0) {
      console.error ("importSelectedAssets: 0 cards selected.")
      return
    }
    this.props.importAssets( assetArray )

  }

  componentDidMount() {
    this.setState({elem: findDOMNode(this)})
  }

  render() {
    const { importing, urlType, importableAssets, selectedAssets} = this.state
    const selectedCardCount = Object.keys (selectedAssets).length

    return (
      <div className="tabWrap">
        <Dimmer.Dimmable dimmed={importing}>
        <Dimmer active={importing} inverted>
          <Loader inverted style={{marginTop: '0', height: '10%'}}>Importing {selectedCardCount} Item(s) into
            '{this.props.project.title}'</Loader>
        </Dimmer>
        <Dimmer active={this.state.fetchingAssets} inverted>
            {urlType === IMAGE &&
            <Loader inverted style={{marginTop: '0', height: '10%'}}>Loading image...</Loader>
            }
            {urlType === SITE &&
            <Loader inverted style={{marginTop: '0', height: '10%'}}>Searching web page for items...</Loader>
            }
        </Dimmer>
        <Grid>
          <Grid.Row>
            <Grid.Column width={6}>
              <div className="tab-description">Enter a single Web Page for items:</div>
            </Grid.Column>
            <Grid.Column width={6}>
              {/*<span className="sel-card-count">{Object.keys(selectedAssets).length} Selected</span>*/}
              <Button disabled={Object.keys (selectedAssets).length === 0} color="blue"
                      style={{margin: '-5px 0 8px 0'}} onClick={this.importSelectedAssets}>
                Import {selectedCardCount > 1 ? selectedCardCount + ' Items' : selectedCardCount === 1 ? selectedCardCount + ' Item' : 'Items'}
              </Button>
            </Grid.Column>
            <Grid.Column width={4}>
              <CardSizeSlider cardContainer="webTab" disabled={!importableAssets || importableAssets.length === 0}/>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <div className="web-import-box scroller">
          <Form>
            <Form.Input icon={<Icon name="search" onClick={(ev) => {
              this.handleUrlChange (ev)
            }}/>}
                        name='Web Address' onKeyPress={(ev) => {
              this.handleUrlChange (ev)
            }} style={{color: '#777'}}
                        placeholder='www.example.com/something/interesting'/>
            {/*<Feather.PlusCircle style={{color: '#777', float: 'left'}}/> <label style={{color: '#777', float: 'left', margin: '2px 0 0 10px'}}>Add More Web Items</label>*/}
            {/*<Loader content='Loading' active={this.state.fetchingAssets}/>*/}
            <div className="browse-page-assets">
              {importableAssets.length > 0 &&
              <div className="itemCount">{importableAssets.length} Items Found</div>
              }
              {importableAssets.map ((asset, i) => {
                return (
                  <Card
                    card={asset}
                    containerName='webTab'
                    containerElem={this.state.elem}
                    key={'ast'+i}
                    index={i}
                    ltr={String.fromCharCode (65 + i)}
                    id={asset.id}
                    text={asset.title}
                    readOnly={true}
                    selectCard={this.toggleSelectCard}
                    selected={selectedAssets[asset.id] !== undefined}
                    moveCard={null}
                    deleteCard={null}
                  />

                  /*
                   <div>Asset: <br/>
                   id: {asset.wid}<br/>
                   title: {asset.title}<br/>
                   src: {asset.src}<br/>
                   link: {asset.href}<br/>
                   count: {asset.count}<br/>
                   </div>
                   */
                )
              })}
            </div>
            {/*
             <div>
             <Form.Button onClick={this.props.handleSubmit}>
             <Feather.Download/>Import
             </Form.Button >

             </div>
             */}
          </Form>
        </div>
        </Dimmer.Dimmable>
      </div>
    )
  }
}