/**
 * Deprecated. Safe to Delete
 */
import {Component} from 'react'
import FileService from '/services/ImageFileService'
import UploaderComponent from '/components/common/UploaderComponent'

export default class UploadTab extends Component {
    constructor(props) {
        super(props)
        this.sendFile = this.sendFile.bind(this)
    }

    sendFile() {
        const elem = document.querySelector( '#sendFile1' );
        let fileRef = elem.files[0]
        let fileType = fileRef.type
        FileService.sendFile( fileRef, false )
    }

    render() {
        return(
            <div>
                <UploaderComponent project={this.props.project}  />
            </div>
        )
    }
}