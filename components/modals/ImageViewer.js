import React, {Component} from 'react'
import {findDOMNode} from "react-dom";
import {observer} from 'mobx-react'
import uiStateStore from '/store/uiStateStore'
import userStore from '/store/userStore'
import {handleBrokenImage} from '/utils/solo/handleBrokenImage'
import {Edit2, RotateCw} from "react-feather";
import ImagePainter3 from "../common/Card/ImagePainter3";

const rotateRE = /rotate\(([0-9]+)deg\)/

@observer
export default class ImageViewer extends Component {
    constructor(props) {
        super(props)
        this.state = { modalOpen: false, myWidth: 0, myHeight: 0}
    }

    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => this.setState({ modalOpen: false })

    handleRotate = (ev) => {
        ev.preventDefault()
        ev.stopPropagation()

        const elem = findDOMNode(this)
        const imgNode = elem.querySelector('#'+this.props.wkey)
        if (!imgNode)
            return
        const rotationMatch = rotateRE.exec(imgNode.style.transform)
        const currRotation = (rotationMatch && rotationMatch.length > 1)? Number(rotationMatch[1]) : 0
        imgNode.style.transform = 'rotate(' +(currRotation+90) + 'deg)'
        // TODO Persist rotation cardStore.saveCard({id: card.id, imageRotation: degree to constant})
    }

    updateDimensions() {
        // const {enablePaint, wkey} = this.props

        const elem = findDOMNode(this)
        const imgBox = this.refs.imgBox // elem.querySelector('#'+wkey)
        if (imgBox)
            this.setState({myWidth: imgBox.clientWidth, myHeight: imgBox.clientHeight})
    }

    componentDidMount() {
        const elem = findDOMNode(this)
        this.setState({elem: elem})
        this.updateDimensions()
    }

    componentDidUpdate() {
        // const {enablePaint, wkey} = this.props
        const {elem, myWidth, myHeight} = this.state
        const imgBox = this.refs.imgBox // elem.querySelector('#'+wkey)

        return // TODO work on drawing later
        if (imgBox && imgBox.clientWidth !== myWidth || imgBox.clientHeight !== myHeight)
            this.updateDimensions()
    }

    render() {
        const {card, handleClick, enablePaint, showRotation, isHover, isAP, imgStyle, hideBG, image, wkey, style, mobile, inverted} = this.props
        const {myWidth, myHeight} = this.state
        const outerHeight = mobile? '' : this.props.size === 'preview'? '77%' : '100%'
        const width = mobile? '100%':''
        const glow = '' // this.props.glow? ' insetGreen':''
        const borderBottom = inverted? '1px solid #e0e0e0' : '1px solid #404040'
        let combinedStyle = {height: outerHeight, width: width}
        if (style)
            combinedStyle = Object.assign(combinedStyle, style)

        const onClick = !enablePaint && !isAP? handleClick : null
        const cropImages = uiStateStore.cropImages // follow the observable. userStore.userPrefs? userStore.userPrefs.cropImages : true
        const markup = card && card.annotations? card.annotations : []
        markup.sort( (a,b) => a.time_modified > b.time_modified? 1 : a.time_modified <  b.time_modified? -1 : 0)

        return (
            <div className='tOuter' style={combinedStyle} onClick={onClick}>
                {/*CROP TO FILL*/}
                {cropImages && !mobile &&
                <div id={wkey} className={'tCover' + glow} style={{backgroundImage: 'url(' + (image) + ')'}}>
                    &nbsp;
                </div>
                }

                {/*DON'T CROP. BLUR OPTIONAL*/}
                {(mobile || !cropImages) &&
                <div style={{height: '100%'}}>
                    {/*
                    {!hideBG &&
                    <div className='tBlur' style={{backgroundImage: 'url(' + (image) + ')'}}>&nbsp;</div>
                    }
*/}

                    {image &&
                    <img id={wkey} ref='imgBox'
                         className={'tInner' + glow}
                         style={imgStyle}
                         src={image}
                         onError={handleBrokenImage}/>
                    }

                    {uiStateStore.paintVisible && markup.map ((ant, i) =>
                        <img className='tInner' src={ant.image_url} style={{opacity: 0.97}}/>
                    )}

                    {enablePaint &&
                    <ImagePainter3 card={card} image={image} className='tInner' width={myWidth} height={myHeight}/>
                    }

                </div>
                }

                {isHover &&
                <span className="abs roundIcon" data-tip='Rotate image' style={{bottom: '.5rem', right: '3rem'}}>
                    <RotateCw size={16} className='secondary' onClick={this.handleRotate}/>
                </span>
                }

            </div>
        );

    }
}

