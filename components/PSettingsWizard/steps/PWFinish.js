import React, {useState} from 'react'

export default function ({step, enableContinue, onUpdate, onContinue, onBack, active}) {

    const submit = (ev) => {
        onContinue('finish')
    }

    return (
        <div id={'step'+step} className={'mt2 fullW' +(active? ' active':' inactive')}>
            <h1 className='mt2 mb1'>
                Ready to Create?
            </h1>

            <button className='mt1 mr1 jBtn'
                    // style={{border: 'none'}}
                    onClick={onBack}>
                Back
            </button>

            {enableContinue &&
            <button className='mt1 button go' onClick={submit}>
                Get Started
            </button>
            }

        </div>
    )
}




