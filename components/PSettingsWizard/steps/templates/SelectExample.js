import React from 'react'
import {Select} from "semantic-ui-react";

const options = [
    {text: 'North Pole'},
    {text: 'Mount Doom'},
    {text: 'Halengrath'},
    {text: 'Virginia'},
    {text: '1234 BoogieWoogie Avenue'}
]

export default function ({step, enableContinue, onContinue, active}) {
    return (
        <div id={'step'+step} className={'mt1' +(active? ' active':' inactive')}>
            <header className='mt2 mb1'>
                {step}. Where do you live?
            </header>

            <div>
                <Select options={options}/>
            </div>

            {enableContinue &&
            <button className='mt1 jBtn' onClick={onContinue}>
                Continue
            </button>
            }
        </div>
    )
}




