import React from 'react'

export default function ({step, enableContinue, onContinue, active}) {

    return (
        <div id={'step'+step} className={active? 'active':'inactive'}>
            <header className='mb1'>
                {step}. Which of these best describes your karate fighting technique?
            </header>
            <div >
                < div
                    className='pad1 inline'
                    style={{maxWidth: '30%'}}>
                    Pokemon
                    <img className='mt1' src='/static/img/stocks/a.jpg' width='100%'
                         style={{}}/>
                </div>
                <div className='pad1 inline' style={{maxWidth: '30%'}}>
                    Water Planet
                    <img className='mt1' src='/static/img/stocks/e.jpg' width='100%'
                         style={{}}/>
                </div>
                <div
                    className='pad1 inline'
                    style={{maxWidth: '30%'}}>
                    Briene of Tarth
                    <img className='mt1' src='/static/img/stocks/c.jpg' width='100%'
                         style={{}}/>
                </div>
            </div>

            {enableContinue &&
            <button className='jBtn' onClick={onContinue}>
                Continue
            </button>
            }

        </div>
    )
}




