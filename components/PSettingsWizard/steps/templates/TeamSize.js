import React from 'react'

export default function ({step, enableContinue, onContinue, active}) {

    return (
        <div id={'step'+step} className={'mt1' +(active? ' active':' inactive')}>
            <header className='mt2 mb1'>
                {step}. How many people are on your team?
            </header>

            <div className='mb05'>
                <input className='mr05' type="radio" name="teamSize"/>I am an army of one.
            </div>
            <div className='mb05'>
                <input className='mr05' type="radio" name="teamSize"/>2-5 people
            </div>
            <div className='mb05'>
                <input className='mr05' type="radio" name="teamSize"/>6-10 people
            </div>
            <div className='mb05'>
                <input className='mr05' type="radio" name="teamSize"/>11+ people
            </div>
            <div className='mb05'>
                <input className='mr05' type="radio" name="teamSize"/>347 people exactly.
            </div>

            {enableContinue &&
            <button className='mt1 jBtn' onClick={onContinue}>
                Continue
            </button>
            }
        </div>
    )
}




