import React, {useState} from 'react'
import {ProjectType} from "../../../config/Constants";
import {Check} from "react-feather";


export default function ({step, enableContinue, onUpdate, onContinue, onBack, active}) {

    const [pType, setPType] = useState(ProjectType.NORMAL);

    const chooseBoard = (ev) => {
        setPType(ProjectType.NORMAL)
        onUpdate('type', pType)
    }

    const chooseAP = (ev) => {
        setPType(ProjectType.APPROVAL)
        onUpdate('type', pType)
    }

    const submit = (ev) => {
        onContinue('type', pType)
    }

    const back = (ev) => {
        onBack()
    }

    return (
        <div id={'step'+step} className={'mt1 fullW' +(active? ' active':' inactive')}>
            <header className='mt2 mb1'>
                {step}. What would you like to create?
            </header>


            <div >
                < div
                    className={'pad1 loh pointer mr1 centered cardRadius inline' +(pType === ProjectType.NORMAL? '  jBlueBG':' jBlueSecBG')}
                    style={{width: '44%', height: '200px'}}
                    onClick={chooseBoard}
                >
                    Collaboration Board
                    <span className='fullW inline'>
                        <Check size={48} style={{opacity: pType === ProjectType.NORMAL? 1:0, marginTop: '50px'}}/>
                    </span>
                </div>
                < div
                    className={'pad1 loh pointer mr1 centered cardRadius inline' +(pType === ProjectType.APPROVAL? ' jPinkBG':' jPinkSecBG')}
                    style={{width: '44%', height: '200px'}}
                    onClick={chooseAP}
                >
                    Approval Request
                    <span className='fullW inline'>
                        <Check size={48} style={{opacity: pType === ProjectType.APPROVAL? 1:0, marginTop: '50px'}}/>
                    </span>
                </div>
            </div>

            {enableContinue &&
            <button className='mt1 jBtn' onClick={submit}>
                Continue
            </button>
            }
        </div>
    )
}




