import React, {useState} from 'react'
import {ProjectType, SecurityLevel} from "../../../config/Constants";
import {Check, Lock} from "react-feather";


export default function ({step, enableContinue, onUpdate, onContinue, onBack, active}) {

    const [secType, setSecType] = useState(ProjectType.NORMAL);

    const choosePublic = (ev) => {
        setSecType(SecurityLevel.Public)
        onUpdate('type', secType)
    }

    const choosePrivate = (ev) => {
        setSecType(SecurityLevel.MembersOnly)
        onUpdate('type', secType)
    }

    const submit = (ev) => {
        onContinue('secType', secType)
    }

    const back = (ev) => {
        onBack()
    }

    return (
        <div id={'step'+step} className={'mt1 fullW' +(active? ' active':' inactive')}>
            <header className='mt2 mb1'>
                {step}. Choose a Security Level
            </header>


            <div >
                < div
                    className={'pad1 loh pointer mr1 centered cardRadius inline borderAll'}
                    style={{width: '44%', height: '200px'}}
                    onClick={choosePublic}
                >
                    Public
                    <span className='fullW inline'>
                        <Check size={48} style={{opacity: secType === SecurityLevel.Public? 1:0, marginTop: '50px'}}/>
                    </span>
                </div>
                < div
                    className={'pad1 loh pointer mr1 centered cardRadius inline borderAll'}
                    style={{width: '44%', height: '200px'}}
                    onClick={choosePrivate}
                >
                    Private (members only)
                    <span className='fullW inline'>
                        <Check size={48} style={{opacity: secType === SecurityLevel.MembersOnly? 1:0, marginTop: '50px'}}/>
                    </span>
                </div>
            </div>

            <button className='mt1 mr1 jBtn'
                // style={{border: 'none'}}
                    onClick={onBack}>
                Back
            </button>

            {enableContinue &&
            <button className='mt1 jBtn' onClick={submit}>
                Continue
            </button>
            }
        </div>
    )
}




