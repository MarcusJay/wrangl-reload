import React, {useState} from 'react'
import Dropzone from 'react-dropzone'
import {sendImage} from "../../../services/ImageFileService";
import {Loader} from "semantic-ui-react";
import {ProjectType} from "../../../config/Constants";
import uiStateStore from "../../../store/uiStateStore";
import UploaderComponent from "../../common/UploaderComponent";

export default function ({step, enableContinue, onContinue, onUpdate, onBack, active}) {

    let fileRefs
    const [image, setImage] = useState ('/static/img/pattern1.png');
    const [loading, setLoading] = useState (false);

    const chooseUpload = (ev) => {

    }

    const chooseLibrary = (ev) => {

    }

    const chooseSearch = (ev) => {

    }


    const submit = (ev) => {
        onContinue ('image', image)
    }

    return (
        <div id={'step' + step} className={'mt1' + (active ? ' active' : ' inactive')}>
            <header className='mt2 mb1'>
                <div className='boxRadius vaTop mr1 inline jYellowBG'
                     style={{width: '2rem', height: '2rem', marginTop: '-.3rem'}}>&nbsp;</div>

                Upload Assets
            </header>
            <Loader active={loading}></Loader>

            <div>
                <UploaderComponent imageOnly={true} wizMode={true}/>
{/*
                < div
                    className='pad1 loh pointer mr1 centered cardRadius inline jBlueBG'
                    style={{width: '30%', height: '100px'}}
                    onClick={chooseLibrary}
                >
                    Use Existing Assets
                </div>

                < div
                    className='pad1 loh pointer mr1 jFGinv centered cardRadius inline jYellowBG'
                    style={{width: '30%', height: '100px'}}
                    onClick={chooseSearch}
                >
                    Search Web for Assets
                </div>
*/}

            </div>


            <button className='mt1 mr1 jBtn'
                // style={{border: 'none'}}
                    onClick={onBack}>
                Back
            </button>

            {enableContinue &&
            <button className='mt1 jBtn' onClick={submit}>
                Continue
            </button>
            }
        </div>
    )
}




