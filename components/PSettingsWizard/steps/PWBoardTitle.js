import React, {useState} from 'react'

export default function ({step, enableContinue, onUpdate, onContinue, onBack, active}) {

    const [title, setTitle] = useState('');

    const submit = (ev) => {
        onContinue('title', title)
    }

    const keyPress = (ev) => {
        if (ev.key === 'Enter')
            submit(ev)
    }

    return (
        <div id={'step'+step} className={'mt1 fullW' +(active? ' active':' inactive')}>
            <header className='mt2 mb1'>
                <div className='boxRadius vaTop mr1 inline jBlueBG'
                     style={{width: '2rem', height: '2rem', marginTop: '-.3rem'}}>&nbsp;</div>
                Name of stage or topic of project (Board)
            </header>

            <input className='formPane'
                   type='text'
                   placeholder='Board name...'
                   onChange={(ev) => setTitle(ev.target.value)}
                   onKeyPress={keyPress}
                   value={title}
                   />

            <button className='mt1 mr1 jBtn'
                    // style={{border: 'none'}}
                    onClick={onBack}>
                Back
            </button>

            {enableContinue &&
            <button className='mt1 jBtn' onClick={submit}>
                Continue
            </button>
            }

        </div>
    )
}




