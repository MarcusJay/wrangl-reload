import React from 'react'

export default function ({step, enableContinue, onContinue, active}) {

    return (
        <div id={'step'+step} className={'mt1' +(active? ' active':' inactive')}>
            <header className='mt2 mb1'>
                {step}. At this point, we have their contact basics.
            </header>


            {enableContinue &&
            <button className='mt1 jBtn' onClick={onContinue}>
                Continue
            </button>
            }
        </div>
    )
}




