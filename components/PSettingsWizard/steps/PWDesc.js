import React, {useState} from 'react'

export default function ({step, enableContinue, onContinue, onBack, active}) {

    const [desc, setDesc] = useState('');

    const submit = (ev) => {
        onContinue('desc', desc)
    }

    const keyPress = (ev) => {
        if (ev.key === 'Enter')
            submit(ev)
    }

    return (
        <div id={'step'+step} className={'mt1' +(active? ' active':' inactive')}>
            <header className='mt2 mb1'>
                {step}. Description
            </header>

            <input className='formPane'
                   type='text'
                   onChange={(ev) => setDesc(ev.target.value)}
                   onKeyPress={keyPress}
                   placeholder='This board contains items for the new campaign for our favorite client.'
                   value={desc}
                   />

            <button className='mt1 mr1 jBtn'
                    // style={{border: 'none'}}
                    onClick={onBack}>
                Back
            </button>

            {enableContinue &&
            <button className='mt1 jBtn' onClick={submit}>
                Continue
            </button>
            }
        </div>
    )
}




