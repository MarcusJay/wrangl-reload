import React, {Component} from 'react'
import {observer} from 'mobx-react'
import {Loader} from "semantic-ui-react";
import userStore from "../../../store/userStore";
import Person from "../../common/PersonInline";

@observer
export default class PWMembers extends Component {

    submit = (ev) => {
        this.props.onContinue ('members', [])
    }

    render() {
        const {step, enableContinue, onContinue, onUpdate, onBack, active} = this.props

        const everyone = userStore.currentConnections.slice ()
        const activeUser = userStore.currentUser

        return (
            <div id={'step' + step} className={'mt1' + (active ? ' active' : ' inactive')}>
                <header className='mt2 mb1'>
                    {step}. Add Members
                </header>

                <div>
                    {everyone.map ((person, i) => {
                        return (
                            <Person
                                person={person}
                                activeUser={activeUser}
                                enableAdmin={false}
                                key={person.domId}
                                id={'plp' + i}
                                badgeCount={0}
                                showBadge={false}
                                pinned={false}
                                mobile={false}
                                name={person.displayName || person.email}
                                isMember={false}
                                isConnected={true}
                            />
                        )
                    })}
                </div>


                <button className='mt1 mr1 jBtn'
                    // style={{border: 'none'}}
                        onClick={onBack}>
                    Back
                </button>

                {enableContinue &&
                <button className='mt1 jBtn' onClick={this.submit}>
                    Continue
                </button>
                }
            </div>
        )
    }
}




