import React, {useState} from 'react'

export default function ({step, enableContinue, onUpdate, onContinue, onBack, active}) {

    const [folderTitle, setFolderTitle] = useState('');

    const submit = (ev) => {
        onContinue('folderTitle', folderTitle)
    }

    const keyPress = (ev) => {
        if (ev.key === 'Enter')
            submit(ev)
    }

    return (
        <div id={'step'+step} className={'mt1 fullW' +(active? ' active':' inactive')}>
            <header className='mt2 mb1'>
                <div className='boxRadius vaTop mr1 inline jPinkBG'
                     style={{width: '2rem', height: '2rem', marginTop: '-.3rem'}}>&nbsp;</div>
                <div className='inline'>Name of your project or client (Folder)</div>
            </header>

            <input className='formPane'
                   type='text'
                   placeholder='Folder name...'
                   onChange={(ev) => setFolderTitle(ev.target.value)}
                   onKeyPress={keyPress}
                   value={folderTitle}
                   />

            <button className='mt1 mr1 jBtn'
                    // style={{border: 'none'}}
                    onClick={onBack}>
                Back
            </button>

            {enableContinue &&
            <button className='mt1 jBtn' onClick={submit}>
                Continue
            </button>
            }

        </div>
    )
}




