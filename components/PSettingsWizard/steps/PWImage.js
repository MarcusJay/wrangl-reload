import React, {useState} from 'react'
import Dropzone from 'react-dropzone'
import {sendImage} from "../../../services/ImageFileService";
import {Loader} from "semantic-ui-react";

export default function ({step, enableContinue, onContinue, onUpdate, onBack, active}) {

    let fileRefs
    const [image, setImage] = useState('/static/img/pattern1.png');
    const [loading, setLoading] = useState(false);

    const updateProgress = (count, msg, uploadedImageUrl) => {
        if (msg === 'start') {
            setLoading(true)
        } else if (msg === 'end') {
            if (uploadedImageUrl) {
                setImage (uploadedImageUrl)
                onUpdate ('image', uploadedImageUrl)
            }

            setLoading(false)
            if (fileRefs)
                fileRefs.forEach (file => window.URL.revokeObjectURL (file.preview))
        }
    }

    const onDrop = (files) => {
        fileRefs = files
        if (files.length > 0) {
            let fileRef = files[0]
            sendImage(fileRef, false, updateProgress)
        }
    }


    const submit = (ev) => {
        onContinue('image', image)
    }

    return (
        <div id={'step'+step} className={'mt1' +(active? ' active':' inactive')}>
            <header className='mt2 mb1'>
                {step}. Choose a Cover Image
            </header>
            <Loader active={loading}></Loader>

            <div className="mr1 ml1 coverBG cardRadius borderAll centered pointer boh"
                 style={{width: '10rem', height: '10rem', backgroundImage: 'url(' +image+ ')'}}>
                <Dropzone onDrop={onDrop}
                          multiple={false}
                          style={{width: '100%', height: '100%', padding: 0}}
                >
                </Dropzone>
            </div>



            <button className='mt1 mr1 jBtn'
                    // style={{border: 'none'}}
                    onClick={onBack}>
                Back
            </button>

            {enableContinue &&
            <button className='mt1 jBtn' onClick={submit}>
                Continue
            </button>
            }
        </div>
    )
}




