import React, {useState} from 'react'
import {ProjectType} from "../../../config/Constants";
import {Check} from "react-feather";


export default function ({step, enableContinue, onUpdate, onContinue, onBack, active}) {

    const [path, setPath] = useState('folder');

    const chooseFolder = (ev) => {
        // redundant variable is because setPath state changer isn't synchronous. We'd need to use effect. whatevs.
        const newPath = 'folder'
        setPath(newPath)
        onUpdate('path', newPath)
    }

    const chooseAP = (ev) => {
        const newPath = 'ap'
        setPath(newPath)
        onUpdate('path', newPath)
    }

    const submit = (ev) => {
        onContinue('path', path)
    }

    const back = (ev) => {
        onBack()
    }

    return (
        <div id={'step'+step} className={'mt1 fullW' +(active? ' active':' inactive')}>
            <header className='mt2 mb1'>
                Would you like to create a folder, or do you just want to send files for approval?
            </header>

            <div >
                < div
                    className={'pad1 loh pointer mr1 centered cardRadius inline' +(path === 'folder'? '  jPinkBG':' jPinkSecBG')}
                    style={{width: '44%', height: '200px'}}
                    onClick={chooseFolder}
                >
                    First Folder & Board
                    <span className='fullW inline'>
                        <Check size={48} style={{opacity: path === 'folder'? 1:0, marginTop: '50px'}}/>
                    </span>
                </div>
                < div
                    className={'pad1 loh pointer mr1 centered cardRadius inline' +(path === 'ap'? ' jRedBG':' jRedSecBG')}
                    style={{width: '44%', height: '200px'}}
                    onClick={chooseAP}
                >
                    Approval Request
                    <span className='fullW inline'>
                        <Check size={48} style={{opacity: path === 'ap'? 1:0, marginTop: '50px'}}/>
                    </span>
                </div>
            </div>

{
            <div className='mt1'>
            {enableContinue &&
            <button className='mt1 jBtn' onClick={submit}>
                Continue
            </button>
            }
            </div>
}
        </div>
    )
}
