import React, {useState, useEffect} from 'react'
import {Check} from "react-feather";
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";

export default function ({step, enableContinue, onUpdate, onContinue, onBack, active}) {

    const now = new Date()
    const [apDeadlineType, setAPDeadlineType] = useState('asap');
    const [apDeadline, setAPDeadline] = useState(now);

    useEffect(() => {
        const picker = document.getElementById('datePicker')
        // if (picker)
        //     picker.focus()
    });

    const chooseASAP = (ev) => {
        setAPDeadlineType('asap')
        onUpdate('apDeadline', 'asap')
    }

    const chooseDate = (ev) => {
        setAPDeadlineType('date')
        const picker = document.getElementById('datePicker')
        if (picker)
            picker.focus()
        onUpdate('apDeadline', 'date')
    }

    const onChange = (date) => {
        setAPDeadline(date)
        setAPDeadlineType('date')
        onUpdate('apDeadline', date)
    }

    const submit = (ev) => {
        onContinue('apDeadline', apDeadline)
    }

    return (
        <div id={'step'+step} className={'mt1 fullW' +(active? ' active':' inactive')}>
            <header className='mt2 mb1'>
                <div className='boxRadius vaTop mr1 inline jPinkBG'
                     style={{width: '2rem', height: '2rem', marginTop: '-.3rem'}}>&nbsp;</div>
                <div className='inline'>What is the deadline?</div>
            </header>

            <div>
                <div
                    className='pad1 vaTop loh pointer mr1 centered cardRadius inline borderAll'
                    style={{height: '120px'}}
                    onClick={chooseASAP}
                >
                    <h1>ASAP</h1>
                    <span className='fullW inline'>
                            <Check size={48} style={{opacity: apDeadlineType === 'asap'? 1:0}}/>
                        </span>
                </div>

                <div className='vaTop inline' style={{width: '4rem', marginTop: '1rem'}}>
                    or by
                </div>

                <div
                    className='vaTop loh pointer mr1 centered cardRadius borderAll inline'
                    style={{paddingTop: '0.75rem', height: '120px'}}
                    onClick={chooseDate}
                >
                    <DatePicker
                        id='datePicker'
                        selected={apDeadline}
                        showTimeSelect
                        // timeFormat="H:mm aa"
                        timeIntervals={60}
                        dateFormat="MMMM d, yyyy h:mm aa"
                        timeCaption="Time Due"
                        onChange={onChange}
                        className='wDatePickerTrigger vaTop '
                        calendarClassName='wDatePicker'
                        // placeholderText="Click to select a due date"
                    />
                    <span className='fullW inline'>
                            <Check size={48} style={{opacity: apDeadlineType === 'date'? 1:0}}/>
                        </span>
                </div>
            </div>

            <button className='mt1 mr1 jBtn'
                    // style={{border: 'none'}}
                    onClick={onBack}>
                Back
            </button>

            {enableContinue &&
            <button className='mt1 jBtn' onClick={submit}>
                Continue
            </button>
            }

        </div>
    )
}




