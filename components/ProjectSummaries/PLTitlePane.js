import {Component} from 'react'
import {observer} from 'mobx-react'
import {Chrome, Menu as FMenu} from 'react-feather'

import UserSessionUtils from '/utils/UserSessionUtils'
import SystemStatus from "/components/common/SystemStatus"
import ProfileMenu from "/components/common/ProfileMenu";
import uiStateStore from "/store/uiStateStore"
import categoryStore from "/store/categoryStore"
import HelpButton from "../help/HelpPopup";

// import store from 'store2'

@observer
export default class PLTitlePane extends Component {
    constructor(props) {
        super(props)
    }

    logout = () => {
        UserSessionUtils.endUserSession()
        window.location.href = '/login'
    }

    toggleMobileSidebar = () => {
        uiStateStore.toggleMobSidebarL()
    }

    openSearch() {
        uiStateStore.toggleSidebar()
    }

    showHelp() {
        debugger
        uiStateStore.showInlineHints = true
        uiStateStore.hintProjects = true
        uiStateStore.hintPlToolbar = true
    }

    createProject = () => {
        this.props.createProjects(1)
    }

    render() {
        const {user, mobile} = this.props
        const frameClass = mobile? 'mFraming':'framing'

        if (mobile) {
            return (
            <header className="mFraming" style={{paddingTop: '0.5rem'}}>
                <div className='left'>
                    <FMenu className='pointer' onClick={() => this.toggleMobileSidebar()}/>
                </div>

                <div className='large centered' style={{marginRight: '1rem', maxWidth: '90%'}}>
                    Pogo.io
                </div>

                <div className='abs' style={{top: '.5rem', right: 0}}>
                    <HelpButton createProject={this.create} className='mr1'/>
                    <ProfileMenu className="right" user={user} mobile/>
                </div>
            </header>
            )

        // desktop or large
        } else {
            return (
            <header className="jMidBG">
                <div>
                    <div className="jSec lighter krn1" style={{ fontSize: '1.4rem', margin: '.5rem 2rem 1rem 1rem' }}>
                        {user? user.displayName : 'Dashboard'}
                    </div>
                    <SystemStatus/>
                </div>

                <div className="absTopRight right secondary" style={{textAlign: 'right'}}>

{/*
                    <button className='jBtn tiny lighter pointer mr1 krn1' style={{minWidth: '5rem'}} onClick={this.createProject}
                            data-tip="Create a new project"
                    >
                        NEW
                    </button>
*/}


                    <span className='inline'>
                        <HelpButton createProject={this.create} className='mr1'/>
                    </span>

                    <div className='utChrome inline vaTop mr1'>
                        <a href='https://chrome.google.com/webstore/detail/pogoio/pnonlpnonheokmhpjmjnonhkijehjdcf' target='_blank' className='jSec'
                           data-tip="Our chrome extension allows you to pull images and links from other sites directly into your boards. Get it now! ">
                            {/*<Chrome size={24} />*/}
                            <img src='/static/img/chrome32.png' width='26'/>
                        </a>
                    </div>

                    <ProfileMenu user={user} />

                </div>

            </header>
            )
        }
    }
}