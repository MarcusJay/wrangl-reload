import {Component} from 'react'

import {Link, Router} from '~/routes'
import {ArrowLeft} from 'react-feather'

export default class PFTitlePane extends Component {
    constructor(props) {
        super(props)
    }

    setCoverImage() {
    }

    goBack = () => {
        Router.back()
    }

    render() {
        return(
            <div className='pointer topLeftPane darkFraming borderB' style={{paddingLeft: '1.3rem', paddingTop: '1.1rem'}}>
                <div onClick={this.goBack}>
                    <ArrowLeft className='darkSecondary' size={24} style={{margin: '0 0.7rem -7px 0.3rem'}}/>
                    <span className='darkSecondary' >Back</span>
                </div>
            </div>
        )
    }
}