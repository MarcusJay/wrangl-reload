import {Component} from 'react'
import {Plus} from 'react-feather'

export default class CategoryCreator extends Component {
    constructor(props) {
        super(props)
        this.state = {newCategory: ''}
    }

    handleKeyPress(ev) {
        if (ev.key === 'Enter') {
            ev.preventDefault()
            this.createCategory()
        }
    }

    handleChange(ev) {
        this.setState({newCategory: ev.target.value})
    }

    createCategory() {
        const newCategory = this.state.newCategory
        this.setState({newCategory: ''})
        this.props.createCategory( newCategory )
    }

    render() {

        return (
            <div className="tag">
                <Plus size={24} onClick={() => this.createCategory()} style={{margin: '0 4px -7px 0'}} />
                <input type="text" onKeyPress={(ev)=>{this.handleKeyPress(ev)}} placeholder="New folder title..." onChange={(ev) => this.handleChange(ev)} value={this.state.newCategory}
                      style={{margin: '0 0 0.5rem 0.5rem', border: '1px solid #e0e0e0', padding: '0.1rem 0.3rem', width: '75%', display: 'inline-block', lineHeight: '1.42em'}}/>
            </div>
        )
    }
}