import {Component} from 'react'

import {DragSource} from 'react-dnd'
import {DndItemTypes} from '~/config/Constants'
import {Circle, XCircle} from 'react-feather'
import {DEFAULT_CAT} from "../../../config/Constants";
import uiStateStore from "../../../store/uiStateStore";

const categorySource = {
    beginDrag(props) {
        uiStateStore.enableFileDrops = false
        return {
            category: props.category.title,
            id: props.category.id
        }
    },
    endDrag(props, monitor) {
        uiStateStore.enableFileDrops = true
        return {
            category: props.category.title,
            id: props.category.id
        }
    }
}

@DragSource(DndItemTypes.TAG, categorySource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
}))
export default class PLCategory extends Component {

    handleKeyPress(ev) {
        if (ev.key === 'Enter') {
            ev.preventDefault()
            const newCategoryName = ev.target.textContent
            ev.target.blur()
            this.props.createCategory(newCategoryName, this.state.color, this.props.category )
        } else if (ev.key === 'Escape') {
            debugger
        }
    }

    deleteCategory() {
        this.props.deleteCategory(this.props.category)
    }

    render() {
        const { isDragging, connectDragSource, category } = this.props
        const opacity = 1 // isDragging ? 0.5 : 1
        const title = category.id === DEFAULT_CAT? 'All' : category.title

        return connectDragSource(
            <div className="tag" style={{marginLeft: '1rem'}}>
                <Circle size={18} className='secondary' style={{marginBottom: '-0.2rem'}}/>
                <span style={{color: '#555', margin: '0 0 0.5rem 0.5rem', fontSize: '1.1rem', fontWeight: 'bold'}}>{title}</span>
                <XCircle size={18} color='#999' style={{float: 'right'}} className="rowHoverActions" onClick={() => this.deleteCategory()}/>
            </div>
        )
    }
}