import {Component} from 'react'
import {observer} from 'mobx-react'

import {Dimmer, Loader} from 'semantic-ui-react'
import PLCategory from './PLCategory'
import PLCategoryCreator from "/components/ProjectSummaries/LeftPane/PLCategoryCreator"
import categoryStore from '/store/categoryStore'

@observer
export default class PLCategories extends Component {
    constructor(props) {
        super(props)
        this.state = {selectedCategory: null, loading: false}
    }

    toggleCategory(categoryName) {
        console.log("toggling category: " +categoryName);
        this.setState({ selectedCategory: categoryName})
        // this.props.filterByCategory(categoryName)
    }

    createCategory(title) {
        this.setState({loading: true})
        categoryStore.createCategory(title).then( r => {
            this.setState ({loading: false})
        }).catch( error => {
            console.error('Error creating and then gettng user cats. ', error)
        })
    }

    deleteCategory(category) {
        this.setState({loading: true})
        categoryStore.deleteCategory(category.id).then( r => {
            this.setState ({loading: false})
        })
    }

    render() {
        let categories = categoryStore.userCategories
        if (!categories)
            return null
        const paneClass = this.props.mobile? 'm-side-pane':'side-pane'

        return(
            <div className={paneClass + ' tags'}>
                <Dimmer.Dimmable dimmed={this.state.loading}>
                    <Dimmer inverted active={this.state.loading} >
                        <Loader inverted active={this.state.loading}> </Loader>
                    </Dimmer>
                    {this.props.editEnabled &&
                    <PLCategoryCreator createCategory={(newCategory) => this.createCategory(newCategory)} key='categoryCreator'/>
                    }
                    {categories.map( (categoryObj,i) => {
                        return(
                            <PLCategory category={categoryObj} key={'cat'+i}
                                   editEnabled={false}
                                   deleteCategory={(category) => this.deleteCategory(category)}
                                   toggleCategory={(categoryName) => {this.toggleCategory(categoryName)}}/>
                        )
                    })}
                </Dimmer.Dimmable>
            </div>
        )
    }
}