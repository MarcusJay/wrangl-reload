import {Component} from 'react'
import {observer} from 'mobx-react'

import Person from '/components/common/PersonInline'
import projectStore from '/store/projectStore'
import userStore from "../../../store/userStore";
import {SearchConfig, UserRole} from "../../../config/Constants";
import eventStore from "/store/eventStore";
import uiStateStore, {ManageContacts} from "../../../store/uiStateStore";
import {Confirm, Form, Icon, Input, Loader} from "semantic-ui-react";
import {isBrowser} from "../../../utils/solo/isBrowser";
import {EventSource} from "../../../config/EventConfig";
import ProjectApiService from "../../../services/ProjectApiService";
import DumbBadge from "../../common/DumbBadge";
import {ChevronDown, ChevronUp, Plus} from "react-feather";
import toSnakeCase from "../../../utils/solo/toSnakeCase";
import UserApiService from "../../../services/UserApiService";

@observer
export default class ContactLibrary extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedUser: null,
            showMembers: true,
            showNonMembers: true,
            loading: false,
            isOpen: false,
            openPersonId: null,
            showPinned: true,
            showDeleteConfirmation: false,
            deletingUserId: null,
            dmUserIds: [],

            // search
            query: null,
            isVirgin: true,
            searching: false,
            results: [],
            showResults: false,
            showError: false,
            sessionId: null,

        }
            // badgedOpen: false, pinnedOpen: false, membersOpen: true, othersOpen: false}
    }

    onSearchChange = (ev) => {
        let query = ev.target.value
        console.log('change: query = ' +query)
        let now = new Date()
        let elapsed = this.state.lastKeyPress === 0? 0 : now - this.state.lastKeyPress
        this.setState({query: ev.target.value, lastKeyPress: now})

        if (this.timerId)
            clearTimeout (this.timerId)

        // negative case: clear query immediately
        if (!query || query.length < SearchConfig.MIN_SEARCH_CHARS) {
            this.setState ({searching: false, showResults: false})

        // positive case: (re)start timer to query
        } else {
            this.timerId = setTimeout( () => {
                this.submitSearch (query)
                this.timerId = null
            }, SearchConfig.QUERY_DELAY_MS)

        }

    }

    submitSearch(query) {
        console.log("Searching for " +query+ "...")
        const {sessionId} = this.state
        query = query.toLowerCase()

        this.setState ({searching: true, isVirgin: false})

        // Note: api only returns users that are not connected.
        // So we must combine api search with our own userStore.currentConnections results
        const connectedUsers = userStore.currentConnections.filter (u =>
            u.displayName? u.displayName.toLowerCase().startsWith(query) :
                u.firstName? u.firstName.toLowerCase().startsWith(query) :
                    u.firstName? u.firstName.toLowerCase().startsWith(query) :
                        u.email? u.email.toLowerCase().startsWith(query) : false) || []

        UserApiService.findUsers(query).then (response => {
            if (!response.data)
                throw new Error ('user search api failed')

            const unconnectedUsers = response.data.found_users || []
            this.setState ({
                searching: false,
                sessionId: response.data.session_id,
                results: connectedUsers.concat(unconnectedUsers),
                showResults: query && query.length >= SearchConfig.MIN_SEARCH_CHARS
            })
        }).catch (error => {
            if (error && error.response && error.response.status === 403) {
                if (sessionId) {
                    UserApiService.endFindSession (sessionId).then (response => {
                        if (response.data.success === 'ok') {
                            this.submitSearch (query)
                        }
                    })
                } else {
                    this.setState ({showError: true, searching: false})
                }
            } else {
                console.error ("Error searching for " + query, error)
                this.setState ({searching: false})
                throw new Error (error)
            }
        })
    }

    contactAdded = (ev) => {
        userStore.getUserConnections().then( connections => {
            // render gets em from store. just refresh.
            this.setState({showResults: false, query: null, searching: false, isVirgin: true, results: [], sessionId: null})
            // query doesn't clear, most likely due to Semantic. You love Semantic.
            // this.refs.searchInput.value = '' won't work since Semantic's <Input> actually wraps a native input inside
            // however it thankfull assigns the id to the inner native input, not its outer wrapper
            const input = document.getElementById('userSearch')
            if (input)
                input.value=''
        })
    }

    toggleOpen = (ev) => {
        const newIsOpen = !this.state.isOpen
        if (newIsOpen && !uiStateStore.showSidebar)
            uiStateStore.toggleSidebar()
        this.setState({isOpen: newIsOpen})
    }

    // too many friends?
    confirmDisconnect(contactId) {
        this.setState({showDeleteConfirmation: true, deletingUserId: contactId})
    }

    onDeleteCancel = () => {
        this.setState({showDeleteConfirmation: false, deletingUserId: null})
    }

    onDeleteConfirm = () => {
        this.disconnectUser(this.state.deletingUserId)
    }


    disconnectUser(contactId) {
        const userId = userStore.currentUser.id
        userStore.disconnectUsers(userId, contactId)
        this.setState({showDeleteConfirmation: false, deletingUserId: null})
    }

    // playin favorites?
    pinUser = (contactId) => {
        userStore.toggleUserPin(contactId, true)
    }
    unpinUser = (contactId) => {
        userStore.toggleUserPin(contactId, false)
    }

    setOpenPersonId = (personId) => {
        this.setState({openPersonId: personId})
    }

    // only called by clicks on menu actions (√, +, invited).
    // Implication: user is viewing current project.
    setMembership(newMemberId, newRole) {
        const projectId = this.props.project? this.props.project.id : null

        ProjectApiService.addUsersToProject(projectId, userStore.currentUser.id, [newMemberId], EventSource.ProjectAdd, newRole).then( response => {
            this.props.refreshMembers()
        })
    }

    directMessage = (contactId) => {
        uiStateStore.openDM(contactId)
    }

    toggleEditContacts() {
        // uiStateStore.unModal = ManageContacts
        uiStateStore.maximizeLeftCol = !uiStateStore.maximizeLeftCol
        setTimeout(() => {
            uiStateStore.leftUnModal = ManageContacts
        },250)
    }

    getConnections() {
        const userId = userStore.currentUser.id
        this.setState({loading: true})

        // Display cached (and unobserved) connections immediately, then fetch real observables.
        // No. Let's just stop caching for a bit.
        userStore.getUserConnections(userId).then( response => {
            this.setState ({loading: false})
            if (isBrowser()) {
                // Restore DM state: open previously open DM panes
                uiStateStore.restoreDMState().then( openDMUserIds => {
                    this.setState({dmUserIds: openDMUserIds})
                })

                // NOTE: We should already be following all connections.
                // But for testing, make sure we follow DM users. Re-following just means it finds existing subscription channel.
                // PusherService.getInstance().followUsers(eventStore.getInstance().unreadMsgCounts.map( msgCount => msgCount.friend_id))
            }
        }).catch( error => {
            console.error(error)
            debugger
        })
    }

    toggleSection = (sectionName) => {
        const newOpenState = !userStore.userPrefs[sectionName+'Open']
        // Justification: mobx will force open board members first time project is opened. After that, we'll leave it up to user.
        if (sectionName === 'projectMembers' && !newOpenState)
            uiStateStore.showMembers = false


        const snakeName = toSnakeCase(sectionName)
        console.warn('setting ' +snakeName+'_open to ' +newOpenState)
        userStore.setUserPref(null, snakeName+'_open', newOpenState)
    }


    componentDidMount() {
        this.getConnections()
    }

    componentDidUpdate() {
        const inProject = uiStateStore.currentPage === uiStateStore.PD
        this.contentHeight = 2 + 4 + 4 + (inProject? 4 : 0)

    }

    render() {
        const {openPersonId, query, results, showResults, showError, sessionId, searching} = this.state

        // mobx faux arrays don't sort(). slice to convert.
        let everyone = userStore.currentConnections.slice() // this.props.connections.slice()

        const { badgeTotal, isMini } = this.props
        const project = projectStore.currentProject // this.props.project //
        const inProject = project && uiStateStore.currentPage === uiStateStore.PD
        const isOpen = Boolean(uiStateStore.tourInProgress || (this.state.isOpen && uiStateStore.showSidebar)) // force open once so we can see board members

        // 0. new user badges. will replace unseen events
        const evs = eventStore.getInstance()
        const dms = evs.currentBadges? evs.currentBadges.direct_messages : null
        const dmTotal = dms? dms.total : 0
        const senders = dms && dms.total > 0? dms.senders : {}
        const senderIds = Object.keys(senders)

        // 1. users with badges first.
        // IF USER_BADGES INCLUDES PEOPLE NOT IN ADDRESS BOOK? WE'LL OPEN DM WINDOW, BUT DON'T ADD TO CONTACTS, SO NOT HERE.
        const badgedUsers = senderIds? everyone.filter( user => senderIds.indexOf(user.id) !== -1) : []

        // 2. users with open DMs next
        // alert(dmUserIds.length + ' open DMS found')
        // const dmUsers = everyone.filter (user => openDMUserIds.indexOf(user.id) !== -1) || []

        // 2. pinned users next
        const pinnedUsers = everyone.filter(user => user.pinned === true && senderIds.indexOf(user.id) === -1) || []

        // 3. Project members, back from the dead
        // const members = project? (project.members.filter( user => !user.pinned && senderIds.indexOf(user.id) === -1) || []) : []

        // 4. All other contacts last. TODO could use array diff util?
        // let otherContacts = everyone.filter( user => members.findIndex(mbr => mbr.id === user.id) === -1 && !user.pinned) || []
        let otherContacts = everyone.filter( user => !user.pinned) || []
        // remainingContacts.sort( (a,b) => { return b.dateModified - a.dateModified })

        const activeUser = userStore.currentUser
        const activeUserInProject = project? project.members.find( member => member.id === activeUser.id) : null
        const enableAdmin = activeUserInProject? activeUserInProject.role === UserRole.ADMIN : false

        const paneClass = 'slideDown thumbLibrary relative pointer ' +(this.props.mobile? 'side-pane' : uiStateStore.maximizeLeftCol? 'expanded-pane' : 'side-pane')

        // const showBadgedUsers = badgedOpen && badgedUsers.length > 0
        const pinnedOpen = Boolean(userStore.userPrefs && userStore.userPrefs.favoritesOpen && pinnedUsers.length > 0)
        // const membersOpen = inProject // userStore.userPrefs? Boolean(userStore.userPrefs.projectMembersOpen && members.length > 0) : inProject
        const othersOpen = uiStateStore.tourInProgress || Boolean(userStore.userPrefs && userStore.userPrefs.contactsOpen)


        let isMember, dmCount, isConnected

        return(
            <div className={paneClass + ' subtle-scroll-light utContacts'}>

                <div className={'paneHdr mt2 ' +(isOpen? 'exp':'col')} onClick={this.toggleOpen}>
                    <img src="/static/svg/v2/contacts.svg" width='26px'
                         className='ml1'/>

                    {badgeTotal > 0 &&
                    <DumbBadge count={badgeTotal} className='abs' style={{top: '-.4rem', left: '2rem'}}/>
                    }

                    {!isMini &&
                    <div className='utDM tiny kern1 inline'
                         style={{paddingLeft: '1rem'}}
                         onClick={this.toggleOpen}>
                        CONTACTS
                    </div>
                    }

                    {isOpen && !isMini &&
                    <div className='utContacts utAddContact inline' >
                        <Plus size={22} className='ml1 pointer' onClick={this.toggleEditContacts}
                             data-tip="Add new contacts" style={{verticalAlign: 'bottom'}}
                        />
                    </div>
                    }
                </div>

                {!isMini &&
                <div className={'paneContent ' + (isOpen ? 'exp' : 'col')}> {/*style={{height: isOpen? this.contentHeight+'rem': 0}}*/}
                    <Loader active={searching}>Searching</Loader>
                    {/*Search*/}
                    <Form inverted className="thumbSearch mt05" style={{width: 'calc(100% - 2rem) !important'}}>
                        <Input icon={<Icon name="search" />}
                               name='Search'
                               ref='searchInput'
                               data-tip='Search for people'
                               className="sidePaneInput small lighter"
                               style={{paddingRight: '.5rem'}}
                               onChange={this.onSearchChange}
                               value={query}
                               id="userSearch"
                               placeholder='Search for people...'
                        />
                    </Form>

                    {showResults && // show search results only
                    <div className='results'>
                        {/* SECTION HEADER */}
                        <div className={'tiny kern1 jFG  mb05 jMidBG secHdr active'}
                             style={{padding: '1rem .25rem 1rem 1rem'}}>
                            SEARCH RESULTS
                        </div>

                        <div className={'othersList paneContent exp'}
                             style={{paddingLeft: '1rem'}}>
                            {results && results.map ((person, i) => {
                                dmCount = senders[person.id] !== undefined ? senders[person.id].total : 0
                                isConnected = everyone && everyone.find (u => person.id && u.id === person.id) !== undefined
                                isMember = project && project.members.find (member => member.id === person.user_id) !== undefined
                                return (
                                    <Person
                                        person={person}
                                        activeUser={activeUser}
                                        enableAdmin={enableAdmin}
                                        key={person.domId}
                                        id={'plp' + i}
                                        sessionId={sessionId}
                                        badgeCount={dmCount}
                                        showBadge={true}
                                        pinned={person.pinned}
                                        mobile={this.props.mobile}
                                        name={person.displayName || person.email}
                                        project={project}
                                        isMember={isMember}
                                        isConnected={isConnected}
                                        contactAdded={this.contactAdded}
                                        pinUser={this.pinUser}
                                        unpinUser={this.unpinUser}
                                        openPersonId={openPersonId}
                                        setOpenPersonId={this.setOpenPersonId}
                                        setMembership={(newRole) => this.setMembership (person.id, newRole)}
                                        directMessage={this.directMessage}/>
                                )
                            })}
                        </div>
                    </div>
                    }

                    {!showResults && // show usual sections
                    <div className='sections'>

                        {/*badged users*/}
                        <div className='pinList' style={{paddingLeft: '1rem'}}>
                            {badgedUsers.map ((person, i) => {

                                // isMember = project && project.members.find( member => member.id === person.id) !== undefined
                                dmCount = senders[person.id] !== undefined ? senders[person.id].total : 0
                                return (
                                    <Person
                                        person={person}
                                        activeUser={activeUser}
                                        enableAdmin={enableAdmin}
                                        key={person.domId}
                                        id={'plp' + i}
                                        badgeCount={dmCount}
                                        showBadge={true}
                                        pinned={person.pinned}
                                        mobile={this.props.mobile}
                                        name={person.displayName || person.email}
                                        project={project}
                                        isMember={false} /*we don't care*/
                                        isConnected={true}
                                        pinUser={() => this.pinUser (person.id)}
                                        unpinUser={() => this.unpinUser (person.id)}
                                        openPersonId={openPersonId}
                                        setOpenPersonId={this.setOpenPersonId}
                                        setMembership={(newRole) => this.setMembership (person.id, newRole)}
                                        directMessage={() => this.directMessage (person.id)}/>
                                )
                            })}
                        </div>

                        {/* SECTION HEADER */}
                        <div className={'tiny kern1 jFG mb05 jMidBG secHdr' + (pinnedOpen ? ' active' : '')}
                             style={{padding: '1rem .25rem 1rem 1rem'}}
                             onClick={() => this.toggleSection ('favorites')}>
                            FAVORITES
                            {pinnedOpen &&
                            <ChevronUp size={22} className='abs jItemBG round pointer hoh' style={{right: '4px'}}/>
                            }
                            {!pinnedOpen &&
                            <ChevronDown size={22} className='abs jItemBG round pointer hoh' style={{right: '4px'}}/>
                            }
                        </div>

                        <div className={'pinList paneContent ' + (pinnedOpen ? 'exp' : 'col')}
                             style={{paddingLeft: '1rem'}}>
                            {pinnedOpen && pinnedUsers.map ((person, i) => {

                                // isMember = project && project.members.find( member => member.id === person.id) !== undefined
                                dmCount = senders[person.id] !== undefined ? senders[person.id].total : 0
                                return (
                                    <Person
                                        person={person}
                                        activeUser={activeUser}
                                        enableAdmin={enableAdmin}
                                        key={person.domId}
                                        id={'plp' + i}
                                        badgeCount={dmCount}
                                        showBadge={true}
                                        pinned={person.pinned}
                                        mobile={this.props.mobile}
                                        name={person.displayName || person.email}
                                        project={project}
                                        isMember={false} /*we don't care*/
                                        isConnected={true}
                                        pinUser={() => this.pinUser (person.id)}
                                        unpinUser={() => this.unpinUser (person.id)}
                                        openPersonId={openPersonId}
                                        setOpenPersonId={this.setOpenPersonId}
                                        setMembership={(newRole) => this.setMembership (person.id, newRole)}
                                        directMessage={() => this.directMessage (person.id)}/>
                                )
                            })}
                        </div>

                        {/* SECTION HEADER */}
                        <div className={'tiny kern1 jFG  mb05 jMidBG secHdr' + (othersOpen ? ' active' : '')}
                             style={{padding: '1rem .25rem 1rem 1rem'}}
                             onClick={() => this.toggleSection ('contacts')}>
                            ALL CONTACTS
                            {othersOpen &&
                            <ChevronUp size={22} className='abs jItemBG round pointer hoh' style={{right: '4px'}}/>
                            }
                            {!othersOpen &&
                            <ChevronDown size={22} className='abs jItemBG round pointer hoh' style={{right: '4px'}}/>
                            }
                        </div>

                        <div className={'othersList paneContent ' + (othersOpen ? 'exp' : 'col')}
                             style={{paddingLeft: '1rem'}}>
                            {othersOpen && otherContacts.map ((person, i) => {

                                // isMember = project && project.members.find( member => member.id === person.id) !== undefined
                                dmCount = senders[person.id] !== undefined ? senders[person.id].total : 0
                                return (
                                    <Person
                                        person={person}
                                        activeUser={activeUser}
                                        enableAdmin={enableAdmin}
                                        key={person.domId}
                                        id={'minm' + i}
                                        badgeCount={dmCount}
                                        showBadge={true}
                                        mobile={this.props.mobile}
                                        name={person.displayName || person.email}
                                        project={project}
                                        isMember={false}
                                        isConnected={true}
                                        openPersonId={openPersonId}
                                        setOpenPersonId={this.setOpenPersonId}
                                        setMembership={(newRole) => this.setMembership (person.id, newRole)}
                                        pinUser={() => this.pinUser (person.id)}
                                        unpinUser={() => this.unpinUser (person.id)}
                                        disconnectUser={() => this.confirmDisconnect (person.id)}
                                        directMessage={() => this.directMessage (person.id)}/>
                                )
                            })}
                        </div>
                    </div>
                    }

                </div>
                }

                <Confirm
                    content={'Permanently remove from your Contacts?'}
                    open={this.state.showDeleteConfirmation}
                    onCancel={this.onDeleteCancel}
                    onConfirm={this.onDeleteConfirm}
                    confirmButton='Remove'
                    style={{height: '25% !important'}}
                />

            </div>

        )
    }
}