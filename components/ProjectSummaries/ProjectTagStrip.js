import {Component} from 'react'
import {observer} from 'mobx-react'
import PropTypes from 'prop-types'
import ColorUtils from '/utils/ColorUtils'
import tagStore from '/store/tagStore'
import {truncate} from "../../utils/solo/truncate";

@observer
export default class ProjectTagStrip extends Component {
    static propTypes = {
        tags: PropTypes.any
    }

    constructor(props) {
        super(props)
    }

    render() {
        const tags = this.props.tags
        console.log("TAG STRIP: " +this.props.tags.length+ " tags")
        let n = tags.length
        const tagWidth = 100 / n
        const charsPerTag = 20 / n
        let userLibTag

        return (
            <div className="projectTagStrip">
                {tags.map ( projectTag => {
                    userLibTag = tagStore.userTags.find( userTag => userTag.id === projectTag.id) || projectTag // TODO should we allow tags that aren't in lib?
                    return userLibTag? <span style={{
                        width: tagWidth + '%',
                        backgroundColor: '#' + userLibTag.color,
                        color: ColorUtils.isLight (userLibTag.color) ? '#777' : 'white'
                    }} key={'t' + userLibTag.id} data-tip={userLibTag.name}>
                    {truncate (userLibTag.name, charsPerTag)}
                    </span> : null
                })}
            </div>
        )
    }
}