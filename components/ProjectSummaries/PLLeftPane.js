import {Component} from 'react'
import {Accordion, Container, Icon} from 'semantic-ui-react'
import {observer} from 'mobx-react'
import ContactLibrary from '/components/ProjectSummaries/LeftPane/ContactLibrary'
// import PeopleModal from '/components/modals/PeopleModal'
import UserSessionUtils from '/utils/UserSessionUtils'
import DropboxLibrary from "../common/DropboxLibrary"
import ProjectLibrary from "../ProjectDetail/LeftPane/ProjectLibrary";
import projectStore from "../../store/projectStore";
import {MsgLibrary} from "../../config/Messages";
import uiStateStore from "../../store/uiStateStore";
import InlineHint from "../onboarding/InlineHint";
import {isBrowser} from "../../utils/solo/isBrowser.js";

@observer
export default class PLLeftPane extends Component {
    constructor(props) {
        super(props)
        this.state = {showPeople: true, showTags: false, showCardLib: false, showDropboxLib: false, showProjectLib: false, catEditEnabled: false, showContactEdit: false}
        this.filterByUser = this.filterByUser.bind(this)
    }

    handleProjectCreate() {
        // StateChangeRequestor.requestStateChange(REQUEST_PROJECT_CREATE)

    }

    togglePeople() {
        UserSessionUtils.setSessionPref('contacts', !this.state.showPeople)
        this.setState({showPeople: !this.state.showPeople})
    }

    toggleTags() {
        UserSessionUtils.setSessionPref('tagLib', !this.state.showTags)
        this.setState({showTags: !this.state.showTags})
    }

    showContactEdit(showOrHide) {
        this.setState({showContactEdit: showOrHide})
    }

    filterByUser(userId) {
        this.props.filterByUser(userId)
    }

    refreshMembers = () => {
        projectStore.fetchCurrentProject(projectStore.currentProject.id).then( project => {
        })
    }

    toggleTagEdit(ev) {
        ev.preventDefault()
        ev.stopPropagation()
        this.setState({catEditEnabled: !this.state.catEditEnabled})
    }

    toggleCardLib() {
        UserSessionUtils.setSessionPref('cardLib', !this.state.showCardLib)
        this.setState({showCardLib: !this.state.showCardLib})
    }

    toggleProjectLib() {
        UserSessionUtils.setSessionPref('projectLib', !this.state.showProjectLib)
        this.setState({showProjectLib: !this.state.showProjectLib})
    }

    toggleDropboxLib() {
        const shouldShowDropbox = !this.state.showDropboxLib
        UserSessionUtils.setSessionPref('dropboxLib', shouldShowDropbox)
        this.setState({showDropboxLib: shouldShowDropbox})
        if (shouldShowDropbox) {

        }
    }

    componentDidMount() {
/*      start everything closed
        this.setState({
            showPeople: UserSessionUtils.getSessionPref ('contacts'),
            showTags: UserSessionUtils.getSessionPref ('tagLib'),
            showCardLib: UserSessionUtils.getSessionPref ('cardLib'),
            showProjectLib: UserSessionUtils.getSessionPref ('projectLib'),
            showDropboxLib: UserSessionUtils.getSessionPref ('dropboxLib')
        })
*/
    }

    render() {
        const { mobile } = this.props
        const fgClass = mobile? 'mSecondary' : 'darkSecondary'
        const scrollClass = mobile? ' subtle-scroll-dark': ' subtle-scroll-dark'

        return(
            <Container className={'tool-pane darkFraming' +scrollClass+ (mobile? ' mobileBG':'')}
                       style={{ overflowY: 'auto' }}>
                <Accordion styled className={'darkFraming' +(mobile? ' mobileBG':'')}>
{/*
                    <Accordion.Content active={true} style={{height:'44px', marginTop: '-2px', color: "#e0e0e0"}}>
                        <h5 className={fgClass} style={{marginTop: '0.5rem'}}>Library</h5>
                        <Divider/>
                    </Accordion.Content>
*/}
                    {isBrowser() && uiStateStore.showInlineHints && uiStateStore.hintLibrary &&
                    <InlineHint name='hintLibrary' cookie='hlib' title='Global Library'
                                body={MsgLibrary} place='side'
                    />
                    }

                    <Accordion.Title active={this.state.showPeople} index={0} style={{paddingLeft: 0}}>
                         <Icon name='dropdown' onClick={() => {this.togglePeople()}} className={fgClass}/>
                         <span onClick={() => {this.togglePeople()}} className={fgClass} onMouseOver={() => this.showContactEdit(true)} onMouseOut={() => this.showContactEdit(false)}>
                             Contacts
                         </span>
                         <span className='hiddenAnimReady' style={{opacity: this.state.showPeople || this.state.showContactEdit? 1 : 0}}>
{/*
                             <PeopleModal connections={this.props.connections}
                                          user={this.props.user}
                                          mobile={mobile}
                                          refreshMembers={this.refreshMembers}

                             />
*/}
                         </span>
                     </Accordion.Title>

                    <Accordion.Content active={this.state.showPeople}>
                        <ContactLibrary project={this.props.project}
                                        mobile={mobile}
                                        refreshMembers={this.refreshMembers}
                                        filterByUser={this.filterByUser} />
                    </Accordion.Content>

                    <Accordion.Title active={this.state.showProjectLib} onClick={() => {this.toggleProjectLib()}} style={{paddingLeft: 0}} className={fgClass}>
                        <Icon  className={fgClass} name='dropdown' />
                        <span className={fgClass}>Boards</span>
                    </Accordion.Title>

                    <Accordion.Content active={this.state.showProjectLib}>
                        <ProjectLibrary userProjects={this.props.projects}
                                        mobile={mobile}
                        />
                    </Accordion.Content>

{/*
                    <Accordion.Title active={this.state.showCardLib} onClick={() => {this.toggleCardLib()}} style={{paddingLeft: 0}} className={fgClass}>
                        <Icon  className={fgClass} name='dropdown' />
                        <span className={fgClass}>My Files</span>
                    </Accordion.Title>

                    <Accordion.Content active={this.state.showCardLib}>
                        <CardLibrary importCards={(cards) => {this.importCards(cards)}}
                                     mobile={mobile}
                        />
                    </Accordion.Content>
*/}

{/*
                    <Accordion.Title active={this.state.showTags} index={0} onClick={() => {this.toggleTags()}} style={{paddingLeft: 0}} >
                        <Icon className={fgClass} name='dropdown' />
                        <span className={fgClass}>Categories</span>
                        {!this.state.catEditEnabled && this.state.showTags &&
                        <Edit2 size={16} className={fgClass} onClick={(ev)=>this.toggleTagEdit(ev)} style={{float: 'right', marginTop: '-2px'}}/>
                        }
                        {this.state.catEditEnabled &&
                        <span onClick={(ev)=>this.toggleTagEdit(ev)} style={{float: 'right'}}>Done&nbsp;
                            <Check size={24} color='#d0d0d0' style={{float: 'right', marginTop: '-2px'}}/>
                        </span>
                        }

                    </Accordion.Title>

                    <Accordion.Content active={this.state.showTags}>
                        <PLCategories editEnabled={this.state.catEditEnabled}
                                      mobile={mobile}
                        />
                    </Accordion.Content>
*/}

                    <Accordion.Title active={this.state.showDropboxLib} onClick={() => {this.toggleDropboxLib()}} style={{paddingLeft: 0}}>
                        <Icon  className={fgClass} name='dropdown' />
                        <span className={fgClass}>Dropbox</span>
                    </Accordion.Title>

                    <Accordion.Content active={this.state.showDropboxLib}>
                        <DropboxLibrary open={this.state.showDropboxLib}
                                        importCards={(cards) => {this.importCards (cards)}}
                                        project={this.props.project}
                                        mobile={mobile}
                        />
                    </Accordion.Content>

                </Accordion>

            </Container>
        )
    }
}