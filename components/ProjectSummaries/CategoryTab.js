import React, {Component} from 'react'
import {X} from "react-feather";
import {DropTarget, DragSource} from 'react-dnd'
import {
    APPROVAL_CAT, ARCHIVED_CAT, DEFAULT_CAT, DndItemTypes, MAX_FOLDER_NAME_LEN,
    REQUEST_CAT
} from "../../config/Constants";
import projectStore from "../../store/projectStore";
import categoryStore from "../../store/categoryStore";
import Toaster from "../../services/Toaster";
import {Dropdown, Icon, Popup} from "semantic-ui-react";
import DumbBadge from "../common/DumbBadge";
import eventStore from "../../store/eventStore";
import uiStateStore from "../../store/uiStateStore";
import {findDOMNode} from "react-dom";
import {truncate} from "../../utils/solo/truncate";

let origIndex = -1
const tabSource = {
    beginDrag(props) {
        origIndex = props.index
        return {
            id: props.cat.id,
            type: DndItemTypes.FOLDER,
            index: props.index,
            origIndex: origIndex // seems redundant, but isn't: index will be overwritten by final props.index, origIndex will not.
        }
    },
    endDrag(props, monitor) {
        let finalIndex = props.index
        return {
            id: props.cat.id,
            type: DndItemTypes.FOLDER,
            index: props.index,
        }
    }
}

const tabTarget = {
    hover(props, monitor, component) {
        let draggedItem = monitor.getItem()
        if (draggedItem.type !== DndItemTypes.FOLDER)
            return

        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            // return
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            // return
        }

        props.hoverTab(dragIndex, hoverIndex)
        monitor.getItem().index = hoverIndex
    },

    drop(props, targetMonitor, component) {
        let dragItem = targetMonitor.getItem()

        // folder sorting
        if (dragItem.type === DndItemTypes.FOLDER) {
            const dragIndex = dragItem.index
            const hoverIndex = props.index
            if (dragItem.origIndex !== hoverIndex)
                props.moveTab(dragItem.id, dragItem.origIndex, hoverIndex)
            // else
            //     props.cancelMove()
        }

        // add project
        else if (dragItem.type === DndItemTypes.PROJECT) {
            let projectId = dragItem.id
            // let index = dragItem.index
            // let fromCatId = dragItem.catId
            let project = projectStore.getProjectFromUserProjects(projectId)
            if (!project) {
                console.error('Category Drop: cant find project')
                debugger
            }

            const projectExists = projectStore.isProjectInCategory(project, props.cat)
            if (projectExists) {
                let msg = "Project is already in this category."
                Toaster.info(msg, 1)
                return
            }

            props.setCatMoveInProgress(true)
            categoryStore.moveProjectCategory(props.cat.id, project.id).then (response => {
                props.setCatMoveInProgress(false)
            })

        }
    }
}

@DropTarget([DndItemTypes.PROJECT, DndItemTypes.FOLDER], tabTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@DragSource(DndItemTypes.FOLDER, tabSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
}))
export default class CategoryTab extends Component {

    state = {toolTip: null, stats: null, isHover: false}
    showCagName = () => {
        const cat = this.props.cat
        const count = cat && cat.projectIds && cat.projectIds.length
        const stats = count + ' boards'
        const tip = <div className='tiny'>{count} boards</div>
        this.setState({toolTip: tip})

    }

    clearCagName = () => {
        this.setState({toolTip: null, stats: null})
    }

    hoverTab(isHover) {
        this.setState({isHover: isHover})
    }

    editCag = (ev) => {
        this.props.editCag(this.props.cat)
    }

    deleteCag = (ev) => {
        const {cat, deleteCag} = this.props
        if (deleteCag)
            deleteCag(cat.id)
    }

    render() {
        const {cat, selected, selectCat, index, loading, onChange, onKeyPress, deleteCag,
            isOver, item, badgeCount, isAP, icon,
            connectDropTarget, connectDragSource
        } = this.props
        if (!cat)
            return null

        const {toolTip, stats, isHover} = this.state
        const isSortTarget = isOver && cat && item && item.type === DndItemTypes.FOLDER && cat.id !== item.catId
        const isProjectTarget = isOver && cat && item && item.type === DndItemTypes.PROJECT
        const isDefault = cat.id === DEFAULT_CAT

        let hdrClass = 'tiny vaTop inline relative hoh' // +(isDropTarget? ' over':'')
        if (isDefault)
            hdrClass += ' utCats'

        const title = cat.title? truncate(cat.title.replace('Project', 'Board'), MAX_FOLDER_NAME_LEN, true) : 'Untitled'
        const titleClass = 'small inline ml05 mr05 kern1' +(selected? ' jFG':' jThird')
        // const projectCount = cat && cat.projects && cat.projects.length
        const enableModify = selected && [APPROVAL_CAT, REQUEST_CAT, ARCHIVED_CAT].indexOf(cat.id) === -1

        const paneClass = 'inline ctab'
            +(selected ? ' active':'')
            +(isSortTarget? ' sorting':'')
            +(isProjectTarget? ' over':'')

        return connectDragSource ( connectDropTarget(
            <span className={paneClass} style={{margin: '0 .15rem 0 0'}}
                  onMouseEnter={() => { this.hoverTab(true) }}
                  onMouseLeave={() => { this.hoverTab(false) }}
            >
                <span className={hdrClass}
                      key={'cat' + index}
                      onMouseEnter={this.showCagName}
                      onMouseLeave={this.clearCagName}
                      onClick={() => selectCat (cat.id)}
                      style={{marginTop: '-3px'}}>

                    {icon &&
                    <span className='jFG inline' style={{verticalAlign: 'bottom', marginBottom: '-4px'}}>{icon}</span>
                    }
{/*
                    <img src={'/static/svg/v2/foldersCircleFill' + (selected ? 'Sel' : '') + '.svg'} width={26}
                         height={26}
                         style={{marginBottom: '-9px'}}/>
*/}


                    <div className={titleClass} style={{lineHeight: '1.8rem'}}>
                        {title}
                    </div>

                    {badgeCount > 0 &&
                    <span className='inline mr05' style={{marginTop: '2px'}}>
                        <DumbBadge count={badgeCount} className='tiny' style={{width:'20px', height: '20px'}}/>
                    </span>
                    }

                    {!loading && enableModify &&
                    <Dropdown
                        icon={<Icon name="dropdown" size="large" className={'vaTop' +(selected? ' jFG':' jThird')}
                                    style={{fontSize: '1.5rem', margin: '0 0 0 0'}}/>}
                        className='jThird'
                        // pointing='top right'
                    >
                        <Dropdown.Menu className='hioh bg fg'>
                            <Dropdown.Item name='open' onClick={this.editCag}>
                                Edit Name...
                            </Dropdown.Item>

                            {cat.id !== DEFAULT_CAT &&
                            <Dropdown.Item name='open' onClick={this.deleteCag}>
                                Delete Folder
                            </Dropdown.Item>
                            }
                        </Dropdown.Menu>
                    </Dropdown>
                    }


{/*

                    {cat.id !== DEFAULT_CAT && !loading &&
                    <X size={12} onClick={() => deleteCag (cat.id)} className='cagActions jSec'
                       data-tip='Delete Folder'
                       style={{marginLeft: '.25rem', marginBottom: '-2px'}}/>
                    }
*/}

                </span>
            </span>
        )
        )
    }
}


/**

 • remaining event types: add card, add annotation
 • new project and new card: blue outline
 • auto-open convo pane when opening project if new comments
 • add a "new content below" indicator like chris app
 • dismiss new event types once added.
 • chris hates black on white thing. hates "New". Michelle likes.


 */