import {Component} from 'react'
import {observer} from 'mobx-react'
import Moment from "react-moment";

import {Link} from '~/routes'
import {ArrowLeft, ArrowRight, ChevronLeft, ChevronRight, Home, Info, X} from "react-feather";

import uiStateStore from '/store/uiStateStore'
import projectStore from "../../store/projectStore";
import userStore from "../../store/userStore";
import {Popup} from "semantic-ui-react";

// TODO Rename to TopLeftPane as it's used for all views now
@observer
export default class PLTopLeftPane extends Component {
    state = {showInfo: false}
    setCoverImage() {
    }

    toggleLP = (ev) => {
        uiStateStore.toggleSidebar()
    }

    openInfo = (ev) => {
        this.setState({showInfo: true})
    }
    closeInfo = (ev) => {
        this.setState({showInfo: false})
    }

    render() {
        const {profile, readOnly} = this.props
        const {showInfo} = this.state
        const isPD = uiStateStore.currentPage === uiStateStore.PD

        let project = projectStore.currentProject
        const inProject = project && uiStateStore.currentPage === uiStateStore.PD

        const creator = project? userStore.getUserFromConnections(project.creatorId) : null
        const created = project? project.dateCreated : null
        const modified = project? project.dateModified : null

        // const friendName = profile? userStore.getUserFromConnections(profile.id) : null

        const isOpen = uiStateStore.showSidebar
        const infoClass = isOpen? 'jSec abs hoh':'jSec hoh'
        const infoStyle = isOpen? {top: '0', right: '4rem'} : {margin: '2.75rem 1.25rem .5rem 0'}

        // const infoRight = isOpen? '3rem':'1rem'
        const logoSize = isOpen? '84px': '48px'

        return(
            <div className={'topLeftPane relative' +(isOpen? '':' centered')} style={{color: '#20222B'}}>
                <Link route='projectsPage' data-tip='Home' className='jSideBG'>
                    <img src="/static/img/pogoMulti.png" className='pointer abs jSideBG'
                         style={{width: logoSize, top: '0', left: '0', transition: 'width .2s'}}
                         data-tip="Home"/>
                </Link>

{/*
                <div className='jFG'>
                    senderId: {this.props.senderId}<br/>
                    recipEmail: {this.props.recipientEmail}<br/>
                </div>
*/}

                {isOpen &&
                <span onClick={this.toggleLP} >
                    {/*<ChevronLeft size={26} className='jSec abs hoh pointer' style={{right: '1rem', top: '0'}}/>*/}
                    <ChevronLeft size={26} className='jSec hoh pointer' style={{marginTop: '3.5rem', marginBottom: '0'}}/>
                </span>
                }

                {!isOpen &&
                <span onClick={this.toggleLP} >
                    <ChevronRight size={26} className='jSec hoh pointer' style={{marginTop: '2rem', marginRight: '2rem'}}/>
                </span>
                }

                {false && (project || profile) &&
                <Popup
                    inverted
                    trigger={
                        <Info size={26} className={infoClass} style={infoStyle} onClick={this.toggleLP}
                              data-tip={"View " +(project? 'board' : profile? 'contact' : '')+ " info"}/>
                    }
                    on='click'
                    open={showInfo}
                    onOpen={this.openInfo}
                    onClose={this.closeInfo}
                >
                    <div className='jFG'>
                        <X size={24} onClick={this.closeInfo} className='vaTop pointer abs' style={{top: '1rem', right: '1rem'}}/>
                        {project &&
                        <div className='tiny bold'>
                            <div>
                                <span className='infoLabel'></span>
                                {/*<span className='infoLabel' style={{textAlign: 'left'}}>About:</span>*/}
                            </div>

                            {creator &&
                            <div>
                                <span className='infoLabel'> Owner:</span>
                                <span className="secondary lighter">{creator.displayName}</span>
                            </div>
                            }

                            {created &&
                            <div>
                                <span className='infoLabel'> Created: </span>
                                <Moment format='LL' className="secondary lighter">{created}</Moment>
                            </div>
                            }

                            {modified &&
                            <div>
                                <span className='infoLabel'> Last Updated:</span>
                                <Moment format='LL' className="secondary lighter">{modified}</Moment>
                            </div>
                            }

                        </div>
                        }

                        {profile &&
                        <div className='tiny bold'>
                            <div>
                                <span className='infoLabel'></span>
                                <span className='infoLabel' style={{textAlign: 'left'}}>About Contact:</span>
                            </div>

                            <div>
                                <span className='infoLabel'>Display name:</span>
                                <span className="secondary lighter">{profile.displayName}</span>
                            </div>

                            {profile.firstName && profile.lastName &&
                            <div>
                                <span className='infoLabel'>Full name:</span>
                                <span className="secondary lighter">{profile.firstName} {profile.lastName}</span>
                            </div>
                            }

                            {profile.firstName && profile.lastName &&
                            <div>
                                <span className='infoLabel'>Full name:</span>
                                <span className="secondary lighter">{profile.firstName} {profile.lastName}</span>
                            </div>
                            }

                            {profile.lastActive &&
                            <div>
                                <span className='infoLabel'> Last Active:</span>
                                <Moment format='LL' className="secondary lighter">{profile.lastActive}</Moment>
                            </div>
                            }

                        </div>
                        }
                    </div>
                </Popup>
                }

            </div>
        )
    }
}