import React, {Component} from 'react'
import {Dropdown} from "semantic-ui-react";
import {ChevronDown} from "react-feather";

export default class AuthorActions extends Component {
    state = {open: false}

    openMenu = () => {
        this.setState({open: true})
    }
    closeMenu = () => {
        this.setState({open: false})
    }

    render() {
        const {open} = this.state
        const {renameCag, deleteCag} = this.props

        return (
            <Dropdown
                icon={<ChevronDown className='cagActions secondary' style={{marginLeft: '1.5rem', verticalAlign: 'middle'}}/>
                    }
                pointing='top right'
                // open={open}
                // onOpen={this.openMenu}
            >
                <Dropdown.Menu className='hioh bg fg'>
                    <Dropdown.Item name='open' onClick={renameCag} >
                        Rename...
                    </Dropdown.Item>
                    <Dropdown.Item as='div' name='open' onClick={deleteCag} className='red'>
                        Delete...
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
            )
    }
}