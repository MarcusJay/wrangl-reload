import {Component} from 'react'
import {observer} from 'mobx-react'
import {Loader, Menu, Popup} from 'semantic-ui-react'
import {Bell} from 'react-feather'
import moment from 'moment'

import eventStore from '/store/eventStore'
import projectStore from '/store/projectStore'
import userStore from '/store/userStore'
import cardStore from '/store/cardStore'
import uiStateStore from "/store/uiStateStore";

import {getIcon, getMessage} from '/config/EventConfig'
import EventDetail from "/components/events/EventDetail";
import EventCollection from "../../models/EventCollection";
import {ProjectView} from "../../config/EventConfig";
import ArrayUtils from "../../utils/ArrayUtils";
import {isBrowser} from "../../utils/solo/isBrowser";

const MAX_EVENTS = 100
const DAYS_PER_FETCH = 2
const MAX_PROJECTS = 15

@observer
export default class GlobalEventDetailsPopup extends Component {

    constructor(props) {
        super (props)
        // this.state = {queue: [], currentEvent: null}
        this.state = {allHistory: [], allNewEvents: [], unseenEvents: [], showPopup: false, showTrigger: true, visible: true, loading: false}
        // this.newEvents = []
    }

    onPopupOpen(ev) {
        this.setState({showPopup: true, loading: false})
    }

    onPopupClose(ev) {
        this.setState({showPopup: false, showTrigger: false, loading: false})
    }

    startLoading = () => {
        this.setState({loading: true})
    }

    gotoEventTarget(eventInfo) {
        // TODO
        // if added/updated cards, highlight cards.
        // if added/updated 1 card, open card in expand or gallery.
        // if user rleft, go to their profile.
    }

    isEventNew( event, unseenEvents ) {
        return unseenEvents.find( ue => ue.id === event.id ) !== undefined
    }

    // No api calls required. Unseen events were loaded in ssr init props.
    fetchNewEvents = () => {
        this.setState({loading: true})
        const evs = eventStore.getInstance()

        // Unseen
        const unseenEventArrays = Object.values(evs.unseenEventHistoryNew).map( eventInfo => eventInfo.events )
        console.log('FNE: unseenEventArrays: ' +unseenEventArrays)
        console.log('FNE: typeof unseenEventArrays: ' +(typeof unseenEventArrays))
        console.log('FNE: unseenEventArrays.length: ' +(unseenEventArrays.length))

        const unseenEvents = ArrayUtils.flattenMobxArrays( unseenEventArrays )
        const unseenTotal = unseenEvents.length

        // Realtime
        // debugger
        let newEvents = ArrayUtils.flattenMobxArrays( Object.values(evs.rtEvents) )
        if (!newEvents || newEvents.length === 0)
            newEvents = []
        const allNewEvents = newEvents.concat(unseenEvents)
        this.setState({allNewEvents: allNewEvents, unseenEvents: unseenEvents, loading: false})
        return allNewEvents
    }

    // api fetch. Potentially costly.
    fetchHistory = (ev, since) => {
        this.setState({loading: true})
        if (!since)
            since = moment().subtract(DAYS_PER_FETCH, 'days')

        const evs = eventStore.getInstance()
        const limit = Math.min(MAX_PROJECTS, projectStore.userProjects.length)

        // populate allEventHistory, update component once all calls are complete
        let apiCalls = []
        for (let i=0; i<limit; i++) {
            apiCalls.push( evs.fetchProjectHistory( projectStore.userProjects[i].id, since) ) // TODO don't we already have this from getinitialprops
        }
        Promise.all( apiCalls ).then( results => {
            let allHistory = ArrayUtils.flattenMobxArrays( Object.values(evs.allEventHistory) )
            // TODO
            // LEFT OFF
            // 1. I'm not seeing a "New Activity" section anymore. It was working, must've killed it during the factoring out of fetchNewEvents.
            // Get it back. 
            // 2. Is this component receiving realtime events? It doesn't appear to be. In-projects does though, yes?
            // 3. This component doesn't have access to cards in each project. Is it worth fetching them for the sake of direct access?

            if (this.state.allNewEvents && this.state.allNewEvents.length > 0) {
                allHistory = ArrayUtils.arrayDifference(allHistory, this.state.allNewEvents)
            }
            this.setState({allHistory: allHistory, loading: false})
        })

    }

    componentWillMount() {
        const allNewEvents = this.fetchNewEvents() || []
        // debugger
        if (allNewEvents.length === 0)
            this.fetchHistory()
    }

    render() {
        if (!isBrowser())
            return null

        const forMobxOnly = eventStore.getInstance().oldRtEventCounts
        const evs = eventStore.getInstance()
        let sections = []

/*
        // Unseen
        const unseenEventArrays = Object.values(evs.unseenEventHistoryNew).map( eventInfo => eventInfo.events )
        const unseenEvents = ArrayUtils.flattenMobxArrays( unseenEventArrays )
        const unseenTotal = unseenEvents.length

        // Realtime
        let newEvents = ArrayUtils.flattenMobxArrays( Object.values(evs.rtEvents) )
        if (!newEvents || newEvents.length === 0)
            newEvents = []
        const allNewEvents = newEvents.concat(unseenEvents)
*/

        // Combined = 'new activity'
        let allNewEvents = this.state.allNewEvents || []
        if (allNewEvents.length > 0) {
            const ec = new EventCollection (allNewEvents)
            allNewEvents = ec.getMergedEvents ()
        }
        const newEventTotal = allNewEvents.length
        // debugger
        if (newEventTotal > 0)
            sections.push({title: 'New Activity', events: allNewEvents, count: newEventTotal})

        // Only fetch old history if requested, or if no new events.
        let oldHistory = this.state.allHistory || []
        if (oldHistory.length > 0) {
            const histEC = new EventCollection( oldHistory )
            oldHistory = histEC.getMergedEvents()
        }
        sections.push({title: 'Activity History', link: 'Get More Activity', events: oldHistory, count: oldHistory.length})

        const bgColor = newEventTotal > 0? '#f0592b' : '#999' // gOrange and third
        const trig = (
            <span className="pointer eventPopupTrig" style={{marginRight: '1rem', backgroundColor: bgColor}} onClick={this.startLoading}>
                <Loader active={this.state.loading}></Loader>
                {!this.state.loading &&
                <Bell size={16} style={{marginRight: '3px', marginBottom: '-4px'}}/>
                }
            </span>
        )

        let user, card, msg, icon, className, isNew
        return(
            <Popup
                   className='eventDetails subtle-scroll-dark'
                   style={{padding: 0}}
                   trigger={trig}
                   open={uiStateStore.forcePopupOpen || this.state.showPopup}
                   onOpen={(ev) => this.onPopupOpen(ev)}
                   onClose={(ev) => this.onPopupClose(ev)}
                   on='click'
                   hoverable
                   wide="very"
                   position='bottom right'
            >
                {sections.map( (section, i) => {
                    // if (section.count === 0 && i === 0)
                    //     return 'nothing in ' +section.title

                    return (
                    <div key={'section'+i}>
                        {section.count > 0 &&
                        <div className='menuHdr centered secondary'>{section.title}</div>
                        }
                        {section.count === 0 &&
                        <div>
                            {this.state.loading &&
                            <div className='menuHdr centered secondary'>Loading History...</div>
                            }
                            {!this.state.loading &&
                            <div className='menuHdr centered link' onClick={this.fetchHistory}>{section.link || section.title}</div>
                            }
                        </div>
                        }
                        {section.count > 0 &&
                        <Menu vertical
                              style={{padding: '0.5rem', width: '100%', marginTop: '0.5rem', marginBottom: '0.5rem'}}>

                            {section.events.map ((eventInfo, i) => {
                                if (i > MAX_EVENTS || eventInfo.event.type === ProjectView)
                                    return null

                                user = userStore.getUserFromConnections (eventInfo.event.userId)
                                // if (!user)
                                //     debugger
                                card = eventInfo.event.card || eventInfo.event.cardId ? cardStore.getCardFromCurrent (eventInfo.event.cardId) : null
                                isNew = this.isEventNew (eventInfo.event, this.state.unseenEvents)
                                // if (isNew)
                                //     debugger
                                msg = getMessage (eventInfo.event, eventInfo.count, true)
                                icon = getIcon (eventInfo.event, eventInfo.count)
                                className = isNew ? 'event new' : 'event old'

                                if (!user)
                                    debugger

                                if (!msg)
                                    return null

                                return (
                                    <EventDetail
                                        user={user}
                                        card={card}
                                        project={eventInfo.event.project}
                                        showProjectThumb={true}
                                        isNew={isNew}
                                        className={className}
                                        msg={msg}
                                        icon={icon}
                                        count={eventInfo.count}
                                        date={eventInfo.event.dateModified}
                                        key={'evm' + i}
                                    />
                                )
                            })}
                        </Menu>
                        }
                    </div>
                    )
                })}
            </Popup>
        )
    }
}

