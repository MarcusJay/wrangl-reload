import {Component} from 'react'
import {observer} from 'mobx-react'
import PLToolbar from './WorkPane/PLToolbar'
import ProjectSummary from './WorkPane/ProjectSummary'
import UnmodalContainer from '/components/unmodals/UnmodalContainer'

import userStore from '/store/userStore'
import projectStore from '/store/projectStore'
import uiStateStore from '/store/uiStateStore'
import {MasonryContainer} from "/components/ProjectSummaries/WorkPane/masonry";
import {UserRole, VIEW_GRID, VIEW_IMAGES, VIEW_LIST} from "/config/Constants"
import {MasonryPlaceholder} from "../onboarding/masonryPlaceholder";
import {Dimmer, Loader} from "semantic-ui-react";
import categoryStore from "../../store/categoryStore";
import RightUnmodalContainer from "../unmodals/RightUnmodalContainer";
import {ARCHIVED_CAT, DEFAULT_CAT, viewNumToName} from "../../config/Constants";
import CategoryTabBar from "./CategoryTabBar";
import eventStore from "../../store/eventStore";
import UserSessionUtils from "../../utils/UserSessionUtils";
import OnboardFirstProject from "../help/OnboardFirstProject";

@observer
export default class PLWorkPane extends Component {
    constructor(props) {
        super(props)
        this.state = {//selCatId: null, // projectStore.userFolio && projectStore.userFolio.length > 0? projectStore.userFolio[0].id : null,
                      view: userStore.userPrefs? viewNumToName(userStore.userPrefs.project_view) : 'grid',
                      projectsLoaded: false, loading: false, loadingCats: false, isProjectMovingCats: false  }
    }

    // let's create projects directly in the selected "folder". I
    createProjects = (n=1,name) => {
        // this.props.createProjects(n,name, this.state.selCatId)
        this.props.createProjects(n,name, categoryStore.currCatId)

    }

    deleteProject(project) {
        this.props.deleteProject(project)
    }

    leaveProject(projectId) {
        this.props.leaveProject(projectId)
    }

    addUserToProject(projectId, userId, newMemberId, trigger) {
        return this.props.addUserToProject(projectId, userId, newMemberId, trigger, UserRole.INVITED)
    }

    viewAsList = () => {
        userStore.setUserPref(null, 'project_view', VIEW_LIST)
        this.setState({view: 'list'})
    }

    viewAsGrid = () => {
        userStore.setUserPref(null, 'project_view', VIEW_GRID)
        this.setState({view: 'grid'})
    }

    viewAsImages = () => {
        userStore.setUserPref(null, 'project_view', VIEW_IMAGES)
        this.setState({view: 'images'})
    }

    selectCat = (catId) => {
        // this.setState({selCatId: catId})
        categoryStore.currCatId = catId
        UserSessionUtils.setSessionPref('selCatId', catId)
    }

    createCag = (cagName) => {
        categoryStore.createCategory(cagName).then( userCats => {
            // userFolio has been updated and will trigger props trickle
        })
    }

    renameCag = (cag) => {
        categoryStore.updateCategory(cag).then( userCats => {
            // userFolio has been updated and will trigger props trickle
        })
    }

    deleteCag = (cagId) => {
        categoryStore.deleteCategory(cagId).then( userCats => {
            // userFolio has been updated and will trigger props trickle
            // this.setState({selCatId: DEFAULT_CAT})
            categoryStore.currCatId = DEFAULT_CAT
        })
    }

    setCatMoveInProgress = (moving) => {
        this.setState({isProjectMovingCats: moving})
    }

    sortBy(key) {
        this.props.sortBy(key)
    }

    toggleTags() {
        this.props.toggleTags()
    }

    importCards(cards, project) {
        this.props.importCards(cards, project)
    }

    importAssets(assets, project, source) {
        this.props.importAssets(assets, project, source)
    }

    filterProjects(query) {
        if (!query) {
            projectStore.stopFiltering()
            return
        }

        projectStore.filterProjectsByQuery(query)
    }

    showMsgs = (ev) => {
        // this.setState({selTab: 'msgs'})
    }

    showProjects = (ev) => {
        // this.setState({selTab: 'projects'})
    }

    closeRightCol = (ev) => {
        uiStateStore.rightUnModal = null
    }

    loadProjects() {
        const userId = userStore.currentUser.id
        this.setState({loading: true, loadingCats: true})

        // Display cached (and unobserved) projects immediately, then fetch and init real observables.
        projectStore.getCachedUserProjects(userId).then( upc => {
            // const showLoading = (!upc || !upc.projects || upc.projects.length === 0)
            this.setState({loading: true})

            projectStore.getUserProjects(userId).then( response => {
                this.setState ({loading: false, loadingCats: false, projectsLoaded: true})
            })
        }).catch( error => {
            console.error(error)
            debugger
        })
    }

    componentDidMount() {
        const userId = userStore.currentUser.id
        if (this.props.device !== 'mobile') {
            userStore.getUserPrefs (userId).then (prefs => {

                // TODO TEMP
                if (prefs.projectView === 'list') // disable list until draggable
                    prefs.projectView = 'images'

                this.setState ({view: prefs.projectView || 'images'})

            })
        }

        if (uiStateStore.disableUPFetch) {
            uiStateStore.disableUPFetch = false
            return
        }

        this.loadProjects()


    }

    componentDidUpdate() {
        console.warn('plworkpane did update')
        if (this.state.projectsLoaded === false && (!projectStore.userProjects || projectStore.length === 0)) // extra cautious in update
            this.loadProjects()
    }

    render() {
        const {loading, loadingCats, /*selCatId, */ isProjectMovingCats} = this.state
        const {mobile, readOnly, showTags, currentUser, users} = this.props
        let view = mobile? 'images' : this.state.view
        const mobxTrig = uiStateStore.counter + uiStateStore.rand


        const folio = projectStore.userFolio // categoryStore.userCategories //
        const showCats = true // folio && folio.length > 1
        const projects = uiStateStore.currentPage === uiStateStore.PF? projectStore.projectsInCommon :
                         projectStore.isFiltering? projectStore.filteredProjects: projectStore.userProjects

        const selCatId = categoryStore.currCatId || DEFAULT_CAT
        const evs = eventStore.getInstance()
        const selCat = (selCatId && folio && folio.length > 0)? folio.find (cat => cat && cat.id === selCatId) : null
        const cagBadges = evs.currentBadges? evs.currentBadges.categories : null
        const selCatBadgeInfo = cagBadges? cagBadges[selCatId] : null

        const allActiveProjects =
            ARCHIVED_CAT === selCatId? projects
            : projects && typeof projects.filter === 'function'? projects.filter( project => project.isActive === true) : [] // projects
        // Don't show them - MC 7/13/18. Show them - MC 5/27/19
        const allInactiveProjects = [] // projects && typeof projects.filter === 'function'? projects.filter( project => !project.isActive) : []

        let selProjects = selCat && !projectStore.isFiltering && typeof allActiveProjects.filter === 'function'? (allActiveProjects.filter (p => selCat.projectIds.indexOf (p.id) !== -1) || []) : allActiveProjects

        const projectCount = projects? projects.length : 0

        let badgeCount, isNew, projectBadgeDetails

        if (uiStateStore.unModal !== null) {
            return <UnmodalContainer />
        }
        else
            return(
            <main className="projectViews">

                {showCats && !mobile && !projectStore.isFiltering &&
                <CategoryTabBar folio={folio} mobile={mobile} selCatId={selCatId} selectCat={this.selectCat}
                                createCag={this.createCag}
                                renameCag={this.renameCag}
                                deleteCag={this.deleteCag}
                                loadingCats={loadingCats}
                                setCatMoveInProgress={this.setCatMoveInProgress}
                />
                }

                <PLToolbar
                    projects={projects}
                    friend={this.props.friend} /*profilePage*/
                    readOnly={readOnly}
                    createProjects={(n,name) => {this.createProjects(n,name)}}
                    mobile={mobile}
                    view={view}
                    showLoading={loading}
                    viewAsList={this.viewAsList}
                    viewAsGrid={this.viewAsGrid}
                    viewAsImages={this.viewAsImages}
                    sortBy={(key) => this.sortBy(key)}
                    toggleTags={() => {this.toggleTags()}}
                    showTags={showTags}
                    showProjects={() => this.showProjects()}
                    showMsgs={() => this.showMsgs()}
                    handleProjectQuery={(query) => {this.filterProjects(query)}}
                />

                <div className="projectViewsWorkPane relative hidden-scroll" style={{paddingTop: mobile? '0':'', height: '88%', overflow: 'auto'}}>
                    {/*<h2>{uiStateStore.counter}. rand: {uiStateStore.rand}, # of DMs: {Object.keys(uiStateStore.openDMs).length}</h2>*/}

                    <div className='mainCol' style={{width: uiStateStore.rightUnModal !== null? '70%':'100%'}}>

                        {projectStore.isFiltering &&
                        <div className='secondary italic small' style={{marginBottom: '1rem'}}>Search results from all folders:</div>
                        }

                        {/* All 3 Projects views dimmer/loader handling plz */}
                        <Dimmer active={uiStateStore.creatingProjects} >
                            <Loader style={{marginTop: '0', top: '5%'}}>Creating...</Loader>
                        </Dimmer>


{/*
                        <Dimmer active={isProjectMovingCats} >
                            <Loader style={{marginTop: '0', top: '35%'}}>
                            <span className='secondary'>Updating Categories...</span>
                            </Loader>
                        </Dimmer>
*/}

                        {!loading && projects.length === 0 && !projectStore.isFiltering &&
                        <OnboardFirstProject createProject={this.createProjects}/>
                        }


                        {/*1. List*/}
{/*
                        {(view === 'list' && selTab === 'projects') &&
                        <ProjectList activeProjects={selProjects}
                                     inactiveProjects={allInactiveProjects}
                                     readOnly={readOnly}
                                     mobile={mobile}
                                     catId={selCatId}
                                     addUserToProject={(projectId, userId, newMemberId, trigger) => {this.addUserToProject(projectId, userId, newMemberId, trigger)}}
                        />
                        }
*/}

                        {/*2. Grid*/}
                        {view === 'grid' &&
                        <div className='mt1'>

                            {selProjects.map ((project, i) => {
                                projectBadgeDetails = selCatBadgeInfo && selCatBadgeInfo.projects && selCatBadgeInfo.projects[project.id]? selCatBadgeInfo.projects[project.id] : null
                                badgeCount = projectBadgeDetails? projectBadgeDetails.total : 0
                                isNew = projectBadgeDetails? Boolean(projectBadgeDetails.new) : false

                                return project? (
                                        <ProjectSummary project={project}
                                                           key={project.domId}
                                                           index={i}
                                                           mobile={mobile}
                                                           badgeCount={badgeCount}
                                                           isNew={isNew}
                                                           showTags={showTags}
                                                           catId={selCatId}
                                                           currentUser={currentUser}
                                                           creator={userStore.getUserFromConnections (project.creatorId)}
                                                           users={users}
                                                           moveProject={(dragIndex, hoverIndex) => this.moveProject (dragIndex, hoverIndex)}
                                        // prepareMove={this.prepareMove}
                                                           cancelMove={() => this.cancelMove ()}
                                                           readOnly={readOnly}


                                                           addUserToProject={(projectId, userId, newMemberId, trigger) => {
                                                               this.addUserToProject (projectId, userId, newMemberId, trigger)
                                                           }}
                                                           leaveProject={(projectId) => {
                                                               this.leaveProject (projectId)
                                                           }}
                                                           deleteProject={(project) => {
                                                               this.deleteProject (project)
                                                           }}
                                        />
                                    ) : null
                            })}
                        </div>
                        }

                        {/*3. Masonry*/}
                        {projectCount === 0 && view === 'images' && !projectStore.isFiltering &&
                            <MasonryPlaceholder loading={loading}/>
                        }

                        {projectCount > 0 && view === 'images' &&
                        <MasonryContainer
                            activeProjects={selProjects}
                            inactiveProjects={allInactiveProjects}
                            showTags={showTags}
                            currentUser={currentUser}
                            users={users}
                            mobile={mobile}
                            catId={selCatId}
                            addUserToProject={(projectId, userId, newMemberId, trigger) => {this.addUserToProject(projectId, userId, newMemberId, trigger)}}
                            importCards={(cards, project) => {this.importCards(cards, project)}}
                            importAssets={(assets, project, source) => {this.importAssets(assets, project, source)}}
                            leaveProject={(projectId) => {this.leaveProject(projectId)}}
                            deleteProject={(project) => {this.deleteProject(project)}}
                        />
                        }
                    </div>
                    <div className='rightCol' style={{paddingRight: mobile? 0: '1rem'}}>
                        <RightUnmodalContainer/>
                    </div>
                </div>
{/*
                <div className='dms'>
                    {Object.keys(uiStateStore.openDMs).map( (contactId,i) => {
                        return <div key={'dm'+i}>{uiStateStore.openDMs[contactId].pane}</div>
                    })}
                </div>
*/}

            </main>
        )
    }
}