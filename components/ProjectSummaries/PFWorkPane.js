import {Component} from 'react'
import {observer} from 'mobx-react'
import ProjectList from './WorkPane/ProjectList'
import UnmodalContainer from '/components/unmodals/UnmodalContainer'

import userStore from '/store/userStore'
import projectStore from '/store/projectStore'
import uiStateStore from '/store/uiStateStore'
import {UserRole, VIEW_GRID, VIEW_IMAGES, VIEW_LIST} from "/config/Constants"
import {Dimmer, Grid, Loader} from "semantic-ui-react";
import DirectMessage from "../Conversations/DirectMessage";
import PDWDiscussToolbar from "../ProjectDetail/WorkPane/PDWDiscussToolbar";
import Conversations from "../Conversations/Conversations";

@observer
export default class PLWorkPane extends Component {
    constructor(props) {
        super(props)
        this.state = {selCatId: projectStore.userFolio && projectStore.userFolio.length > 0? projectStore.userFolio[0].id : null,
                      view: this.props.view, dark: false,
                      selTab: userStore.currentConvo && userStore.currentConvo.length > 0? 'msgs':'projects',
                      prevProfileId: null,
                      loading: false
        }
    }

    createProject(n, baseName) {
        this.props.createProject(n, baseName)
    }

    createProjects(n,name) {
        this.props.createProjects(n,name)
    }

    deleteProject(project) {
        this.props.deleteProject(project)
    }

    leaveProject(projectId) {
        this.props.leaveProject(projectId)
    }

    addUserToProject(projectId, userId, newMemberId, trigger) {
        return this.props.addUserToProject(projectId, userId, newMemberId, trigger, UserRole.INVITED)
    }

    viewAsList() {
        userStore.setUserPref(null, 'project_view', VIEW_LIST)
        this.setState({view: 'list'})
    }

    viewAsGrid() {
        userStore.setUserPref(null, 'project_view', VIEW_GRID)
        this.setState({view: 'grid'})
    }

    viewAsImages() {
        userStore.setUserPref(null, 'project_view', VIEW_IMAGES)
        this.setState({view: 'images'})
    }

    selectCat(catId) {
        this.setState({selCatId: catId})
    }

    sortBy(key) {
        this.props.sortBy(key)
    }

    toggleTags() {
        this.props.toggleTags()
    }

    importCards(cards, project) {
        this.props.importCards(cards, project)
    }

    importAssets(assets, project, source) {
        this.props.importAssets(assets, project, source)
    }

    filterProjects(query) {
        if (!query) {
            projectStore.stopFiltering()
            return
        }

        projectStore.filterProjectsByQuery(query)
    }

    fetchProjects() {
        const {friend, currentUser} = this.props
        const {loading} = this.state

        if (loading)
            return

        // TODO Research why nextjs can't save state between pages. If we have to re-fetch business data on every route change,
        // we're behaving like a multi-page stateless application.
        if (projectStore.userProjects.length > 0) {
            this.setState({loading: true})
            projectStore.getCommonProjects([currentUser.id, friend.id]).then( projects => {
                this.setState ({loading: false, prevProfileId: friend.id})
            })
        }

        else if (projectStore.userProjects.length === 0) {
            this.setState ({loading: true})
            projectStore.getUserProjects (currentUser.id).then (response => {
                projectStore.getCommonProjects ([currentUser.id, friend.id]).then (projects => {
                    this.setState ({loading: false, prevProfileId: friend.id})
                })
            })
        }
    }

    componentDidMount() {
        if (uiStateStore.disableUPFetch) {
            uiStateStore.disableUPFetch = false
        }
        this.fetchProjects()
    }

    componentDidUpdate() {
        const {prevProfileId, loading} = this.state
        const {friend} = this.props
        if (friend && friend.id !== prevProfileId && !loading)
            this.fetchProjects()
    }

    render() {
        const {loading, view, selCatId, selTab} = this.state
        const {mobile, readOnly, friend, showTags, currentUser, users} = this.props
        const mobxTrig = uiStateStore.counter + uiStateStore.rand

        const projects = projectStore.projectsInCommon || []
        const folio = projectStore.userFolio

        const allActiveProjects = projects.filter( project => project.isActive === true) || []
        const allInactiveProjects = projects.filter( project => !project.isActive) || []

        const isDMOpen = uiStateStore.isDMOpen(friend.id)
        const discussColumns = isDMOpen? 0 : uiStateStore.showDiscussion? 5 : 1
        const workColumns = 16 - discussColumns
        const discussClass = 'discussCol discussBG discussBody borderR'

        const testProject = projectStore.userProjects[0]

        if (uiStateStore.unModal !== null) {
            return <UnmodalContainer />
        }
        else
            return(
            <main className="projectViews">
                <div className="projectViewsWorkPane hidden-scroll-light" style={{padding: 0, overflowY: 'unset', height: '94%'}}>
                        <Dimmer active={loading} >
                            <Loader active={loading} style={{marginTop: '0', top: '35%'}}>Loading...</Loader>
                        </Dimmer>

                    <Grid style={{height: '100%', width: '100%'}}>
                        <Grid.Row style={{padding: '0', height: '100%'}}>

                            <Grid.Column className='workCol borderL borderR' computer={workColumns} tablet={workColumns} mobile={16} style={{height: '100%'}}>
                                <div className='hidden-scroll' style={{height: '100%', overflowY: 'auto'}}>
                                    <ProjectList activeProjects={allActiveProjects}
                                             inactiveProjects={allInactiveProjects}
                                             readOnly={readOnly}
                                             mobile={mobile}
                                             isPF={true}
                                             addUserToProject={(projectId, userId, newMemberId, trigger) => {this.addUserToProject(projectId, userId, newMemberId, trigger)}}
                                    />
                                </div>
                            </Grid.Column>

                            {!isDMOpen &&
                            <Grid.Column width={discussColumns} className={discussClass} style={{padding: 0, height: '100%'}} >
                                <div className='hidden-scroll-dark' style={{height: '95%', overflowY: 'auto'}}>
                                    {/*<DirectMessage contactId={friend.id} isPF={true}/>*/}
                                    <PDWDiscussToolbar friend={friend} filterBy={uiStateStore.viewingCard || uiStateStore.editingCard}/>
                                    <Conversations friend={friend}
                                                   selectedCard={uiStateStore.viewingCard || uiStateStore.editingCard}
                                                   focus={true}
                                                   user={userStore.currentUser}
                                                   mobile={mobile}
                                                   layout='v'/>

                                </div>
                            </Grid.Column>
                            }
                        </Grid.Row>
                    </Grid>

                </div>
            </main>
        )
    }
}