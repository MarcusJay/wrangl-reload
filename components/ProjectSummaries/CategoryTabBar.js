import {Component} from 'react'
import {observer} from 'mobx-react'
import {Clock, FolderPlus, Plus, X} from "react-feather";
import {APPROVAL_CAT, ARCHIVED_CAT, DEFAULT_CAT, REQUEST_CAT} from "../../config/Constants";
import CategoryTab from "./CategoryTab";
import eventStore from "../../store/eventStore";
import categoryStore from "../../store/categoryStore";
import UserSessionUtils from "../../utils/UserSessionUtils";
import cardStore from "../../store/cardStore";
import projectStore from "../../store/projectStore";

@observer
export default class CategoryTabBar extends Component {

    state = {
        showCagEditor: false,
        showDeleteConfirmation: false,
        editingCag: null,
        deletingCagId: 0,
        cagName: '',
        newCagColor: '#000000',
        showCagActions: false
    }

/*
    onClick = (ev) => {
        const catId = ev.target.name
        const cat = this.props.folio.find (cat => cat.id === catId)
        if (cat)
            this.setState({editingCag: cat})
    }
*/

    onNameChange = (ev) => {
        const {editingCag, cagName} = this.state
        const name = ev.target.name
        const val = ev.target.value
        this.setState ({cagName: val})

/*
        else {
            const cat = this.props.folio.find (cat => cat.id === name)
            if (cat) {
                cat.title = val
                this.setState ({editingCag: cat})
            }
        }
*/
    }

    onKeyPress = (ev) => {
        if (ev.key !== 'Enter')
            return

        this.saveCag(ev)
    }

    saveCag = (ev) => {
        const {editingCag} = this.state
        if (editingCag !== null)
            this.renameCag(editingCag)
        else
            this.submitCagCreation()
    }

    toggleCagCreator = () => {
        const show = !this.state.showCagEditor
        this.setState({showCagEditor: show})
        if (show) {
            this.setState({cagName: ''})
        }
    }

    closeTabEditor = () => {
        this.setState({showCagEditor: false})
    }

/*
    openCagActions = () => {
        this.setState({showCagActions: true})
    }

    closeCagActions = () => {
        this.setState({showCagActions: false})
    }

    cancelCagCreation = () => {

    }
*/

    submitCagCreation = (ev) => {
        this.props.createCag(this.state.cagName)
        this.closeTabEditor()
    }

    renameCag = (editingCag) => {
        editingCag.title = this.state.cagName
        this.props.renameCag(editingCag)
        this.setState({editingCag: null})
        this.closeTabEditor()
    }

    hoverTab(dragIndex, hoverIndex) {
        categoryStore.hoverCategory(dragIndex, hoverIndex)
    }

    // commit drag operation to backend.
    moveTab = (dragId, dragIndex, hoverIndex) => {
        const dragCard = categoryStore.getCategoryFromUser(dragId)
        let destIndex

        if (hoverIndex > dragIndex)    // if dragging toward tail
            destIndex = hoverIndex // +1 //hoverCard.nextId  // insert AFTER hover card
        else                                // if dragging toward head
            destIndex = hoverIndex // insert BEFORE hover card

        categoryStore.moveCategory(dragId, destIndex).then( response => {
            // categoryStore.getUserCategories()
            projectStore.getUserProjects()
            this.setState({isHovering: false})
        })
    }

    handleColorChangeComplete = (color, ev) => {
        this.setState({newCagColor: color.hex})
        // this.props.updateTag(this.props.tag.name, color.hex, this.props.tag )
    }

    editCag = (cag) => {
        this.setState({showCagEditor: true, editingCag: cag, cagName: cag.title})
    }

    deleteCag = () => {
        const cagId = this.state.deletingCagId
        this.props.deleteCag(cagId)
    }

    confirmDelete = (cagId) => {
        this.setState ({deletingCagId: cagId, showDeleteConfirmation: true})
    }

    onDeleteConfirm = (ev) => {
        this.setState ({showDeleteConfirmation: false})
        this.deleteCag()
    }

    onDeleteCancel = (ev) => {
        this.setState({showDeleteConfirmation: false})
    }

    componentDidMount() {
        // restore state between routes thx nextjs
        const sessionCatId = UserSessionUtils.getSessionPref('selCatId')
        if (sessionCatId)
            categoryStore.currCatId = sessionCatId
    }

    render() {
        const {folio, mobile, selectCat, loadingCats, setCatMoveInProgress} = this.props
        const {showCagEditor, editingCag, showDeleteConfirmation, showCagActions, cagName, newCagColor} = this.state
        const selCatId = categoryStore.currCatId

        if (typeof folio.filter !== 'function' || typeof folio.map !== 'function') {
            debugger
            console.error('type error')
            return null
        }

        const ffolio = folio // folio.filter( cat => cat && cat.id !== APPROVAL_CAT && cat.id !== REQUEST_CAT) || [] // LET THEM IN
        // const hdrSize = mobile? '0.9rem':'1.2rem'
        // const hdrClass = 'vaTop inline relative hoh'
        // const hdrSpacing = mobile? '.5rem':'.5rem'

        // const isDefaultCatOnly = ffolio.length === 1 && ffolio[0].id === DEFAULT_CAT

        const evs = eventStore.getInstance()
        const cagBadges = evs.currentBadges? evs.currentBadges.categories : null
        let badgeCount, isAP, isArchived

        return (
        <div className='relative'>
            <div className='cagHeader pointer utNewFolder' >
                {/*<div className='inline tiny kern1 jFG mr1'>FOLDERS</div>*/}
                {ffolio && typeof ffolio.map === 'function' && ffolio.map( (cat,i) => {
                    badgeCount = cat.id !== ARCHIVED_CAT && cagBadges && cagBadges[cat.id] !== undefined ? cagBadges[cat.id].total : 0
                    isAP = cat.id === APPROVAL_CAT || cat.id === REQUEST_CAT
                    isArchived = cat.id === ARCHIVED_CAT
                    // if ((isArchived || isAP) && (!cat.projectIds || cat.projectIds.length === 0))
                    //     return null


                    return (
                        <CategoryTab selected={selCatId === cat.id}
                                     selectCat={selectCat}
                                     loading={loadingCats}
                                     key={'cat' + i}
                                     cat={cat}
                                     isAP={isAP}
                                     icon={null}

                                     moveTab={this.moveTab}
                                     hoverTab={this.hoverTab}
                                     editCag={this.editCag}
                                     deleteCag={this.confirmDelete}

                                     badgeCount={badgeCount}
                                     setCatMoveInProgress={setCatMoveInProgress}
                                     index={i}
                                     onChange={this.onNameChange}
                                     onKeyPress={this.onKeyPress}
                                     size={cat.title ? cat.title.length : 10}
                                     maxLength={20}/>
                    )

                })}

                <img className='pointer ml1' src='/static/svg/NewFolderLabelled.svg' height={28}
                     style={{marginBottom: '-7px', marginTop: '0'}}
                     onClick={this.toggleCagCreator}
                     data-tip="Create New Folder"/>


            </div>

            {showCagEditor &&
            <div className='abs formPane zModal modalShadow' style={{padding: '1rem', backgroundColor: '#333'}}> {/*crime*/}
                <X size={24} onClick={this.closeTabEditor} className='vaTop pointer abs' style={{right: '1rem'}}/>
                <span className='infoLabel'>
                    {(editingCag? 'Edit':'New') + ' folder name'}
                </span>
                <input type='text'
                       name='new'
                       placeholder='Enter name...'
                       value={cagName}
                       onChange={this.onNameChange}
                       onKeyPress={this.onKeyPress}
                       className='jSec jItemBG mr1'
                       style={{minWidth: '26rem', padding: '.5rem'}}
                />

                <div className='inline jBtn pointer mr1' onClick={this.closeTabEditor}>
                    Cancel
                </div>
                <div className='inline jBtn pointer ' onClick={this.saveCag}>
                    Save
                </div>
            </div>
            }

            {showDeleteConfirmation &&
            <div className='abs wModal' style={{top:'30%',left:'15%'}}>
                <div className='bold large'>Are you sure you want to delete this folder? The projects inside it will return to your default folder.</div>
                <div className='btns'>
                    <button className='button neutral' onClick={this.onDeleteCancel}>Cancel</button>
                    <button className='button go' onClick={this.onDeleteConfirm}>Delete Folder</button>
                </div>
            </div>
            }

        </div>
        )
    }
}
