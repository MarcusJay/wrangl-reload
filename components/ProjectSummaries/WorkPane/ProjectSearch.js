import React, {Component} from 'react'
import {Icon, Input} from 'semantic-ui-react'
import {SearchConfig} from '/config/Constants'

export default class ProjectSearch extends Component {
    constructor(props) {
        super (props)
        this.state = {query: '', searchResults: [], lastKeyPress: 0, searching: false, isVirgin: true, waiting: false}
    }

    handleOpen = () => this.setState ({modalOpen: true})

    handleClose = () => this.setState ({modalOpen: false})

    resetSearch() {
        this.setState ({isLoading: false, searchResults: [], value: ''})
    }

    onChange(ev) {
        let query = ev.target.value
        console.log('change: query = ' +query)
        let now = new Date()
        let elapsed = this.state.lastKeyPress === 0? 0 : now - this.state.lastKeyPress
        this.setState({query: ev.target.value, lastKeyPress: now})

        setTimeout( () => {
            if (query.length >= SearchConfig.MIN_SEARCH_CHARS) {
                this.submitSearch (query)
            } else if (!this.state.isVirgin) {
                console.log ('Clearing search')
                this.props.handleProjectQuery()
            } else {
                console.log('Skipping search ' + query + ' at ' +elapsed+ 'ms')
            }
            this.setState ({waiting: false})

        }, SearchConfig.QUERY_DELAY_MS )

    }

    submitSearch(query) {
        console.log("Searching for " +query+ "...")

        this.setState ({searching: true, isVirgin: false})
        this.props.handleProjectQuery(query)
    }


  render() {
    // let selectedProjectCount = Object.keys (this.state.selectedAssets).length
    return (
        <div className='relative inline' style={{margin: this.props.mobile? 'auto':''}}>
            <input name='Search'
                   onChange={(ev) => {this.onChange (ev)}}
                   value={this.state.query}
                   id="projectSearch"
                   className='jSideBG jFG'
                   placeholder='Search...'
                   style={{height: '2.1rem'}}
            />
            <span className='abs' style={{right: '.5rem', top: '.75rem'}}>
                <Icon name="search" className='jSec'/>
            </span>
        </div>
    )
  }
}
