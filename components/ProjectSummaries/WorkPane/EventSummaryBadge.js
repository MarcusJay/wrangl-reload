import {Component} from 'react'
import {observer} from 'mobx-react'
import {FilePlus, MessageCircle} from 'react-feather'

import eventStore from '/store/eventStore'
import {findDOMNode} from "react-dom";
import uiStateStore from "../../../store/uiStateStore";

const ANIM_CLASS = 'flash'

@observer
export default class EventSummaryBadge extends Component {

    constructor(props) {
        super(props)
        this.state = {showPopup: false}
    }

    onOpen(ev) {
        this.setState({showPopup: true})
    }

    onClose(ev) {
        this.setState({showPopup: false})
    }

    resetAnimation() {
        const elem = findDOMNode(this)
        if (!elem)
            return

        elem.classList.remove(ANIM_CLASS)
        setTimeout(() => {
            elem.classList.add(ANIM_CLASS)
        }, 100)
    }

    componentDidMount() {
        const evs = eventStore.getInstance()
        const project = this.props.project
        if (!project || uiStateStore.currentPage === uiStateStore.PF)
            return

        // NOTE: this.count is only used for animation at this point
        this.count = (evs.unseenEventCounts[project.id] || 0) + (evs.rtEvents[project.id]? evs.rtEvents[project.id].length : 0)
    }

    componentDidUpdate() {
        console.warn('event summary badge did update')
        const evs = eventStore.getInstance()
        const project = this.props.project

        let newCount = 0
        if (project && evs.rtEvents && evs.rtEvents[project.id])
            newCount = (evs.unseenEventCounts[project.id] || 0) + (evs.rtEvents[project.id].length || 0)

        // NOTE: this.count is only used for animation at this point
        if (newCount !== this.count) {
            this.resetAnimation()
            this.count = newCount
        }
    }

    render() {
        let {totalUnseen, totalViews, totalCardAddEvents, totalCardUpdateEvents,
               totalCardCommentEvents, totalProjectCommentEvents, totalReactionEvents }
            = eventStore.getInstance().countEventBreakdown(this.props.project.id)

        if (totalProjectCommentEvents === 0 && totalCardAddEvents === 0)
            return null

        // totalUnseen -= totalProjectCommentEvents

        const isMixed = ([totalCardAddEvents > 0, totalCardCommentEvents > 0, /*totalProjectCommentEvents > 0,*/ totalReactionEvents > 0].filter( bool => bool === true ) || []).length > 1 // could use some()
        if (totalUnseen === 0 || totalUnseen === undefined) // why would it be undefined?
            return null

        console.log('%c totalUnseen: ' +totalUnseen+ ', totalViews: ' +totalViews, 'color: purple')
        // console.log('TOTAL UNSEEN: ' +totalUnseen)
        const totalSansViews = totalUnseen - totalViews

        const isViewsOnly = totalUnseen === totalViews
        const viewsBG = '#548fb3'
        const importantBG = '#f0592b'
        const absPos = this.props.abs === true? ' abs': this.props.abs === 2? ' abs':'' // badgeUpTR

        console.log('count: ['+totalUnseen+']')

        return (
            <span className={"eventTotal " + absPos}>
                {totalProjectCommentEvents > 0 &&
                <span className='newMsgBG'>
                    <MessageCircle className='flipHoriz' color='white' fill='white' style={{width: '15px', height: '15px', marginBottom: '-2px', marginRight: '0.25rem'}}/>
                    {totalProjectCommentEvents}
                </span>
                }

                {totalCardAddEvents > 0 &&
                <span className='newCardBG'>
                    <FilePlus color='white ' style={{width: '15px', height: '15px', marginBottom: '-2px', marginRight: '0.25rem'}}/>
                    {totalCardAddEvents}
                </span>
                }


            </span>
        )
    }
}

{/*
 <Popup style={{display: 'block', textAlign: 'left'}}
 className='eventStats'
 trigger={popupTrigger}
 onOpen={(ev) => this.onOpen(ev)}
 onClose={(ev) => this.onClose(ev)}
 on='hover'
 position='top center'
 >
 <div className='stat' data-tip={cardAddEvents.length + ' new cards in this folder.'} data-place="right">
 <Layers color="#777" size={24} />
 <span className="count"> {cardAddEvents.length}</span>
 </div>
 <div className='stat' data-tip={commentEvents.length + ' new comments inside.'} data-place="right">
 <MessageSquare color="#777" size={24}/>
 <span className="count"> {commentEvents.length}</span>
 </div>
 <div className='stat' data-tip={reactionEvents.length + ' new card reactions inside.'} data-place="right">
 <ThumbsUp color="#777" size={24}/>
 <span className="count">{reactionEvents.length}</span>
 </div>
 </Popup>
 */}
