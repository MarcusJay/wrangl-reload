import {Component} from 'react'
import {observer} from 'mobx-react'
import * as Feather from 'react-feather';

import {REQUEST_PROJECT_OPEN, StateChangeRequestor} from '/services/StateChangeRequestor'
// import Link from 'next/link'
import {Link} from '~/routes'

@observer
export default class ProjectSummary extends Component {
    constructor(props) {
        super(props)
        this.state = {expanded: false}
    }

    toggleExpanded() {
        this.setState({expanded: !this.state.expanded})
    }

    handleLink(ev) {
        ev.preventDefault()
        ev.stopPropagation()
    }

    render() {
        console.log("Expanded: " +this.state.expanded)

        // const img2 = this.props.project.cardTotal > 0? this.props.project.cards.find( card => card.image !== undefined ).image : null

        return(
               <div className={this.state.expanded? "coverView expanded" : "coverView"}>
                   <Link route='project' params={{id: this.props.project.id}} onClick={(ev) => this.handleLink(ev)}>
                       <div>
                           <div className="imgBox">
                               {this.props.project.image &&
                               <img src={this.props.project.image} width='100%' className="previewImg"/>
                               }
                               {!this.props.project.image &&
                               <div className="sad">No cover. Sad!</div>
                               }
                               <div className="hoverlay">
                                   <Feather.Link size={64} className="action"/>

                               </div>
                           </div>
                           <div className="title"
                                data-tip={"Open this project"}   >
                               {this.props.project.title}
                               <span className="stats">
                                    <Feather.Layers color="#999" style={{width: '15px',height:'15px', marginBottom: '-2px', marginLeft: '3px'}}/>
                                    <div style={{display: 'inline-block', color: '#999', marginLeft: '0.3rem'}}>{this.props.project.cardTotal}</div>
                               </span>
                           </div>
                       </div>
                   </Link>
               </div>
        )
    }
}