import React, {Component} from 'react'
import {findDOMNode} from "react-dom";
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import {observer} from 'mobx-react'
import {Link, Router} from '~/routes'
import {Loader} from 'semantic-ui-react'
import userStore from '/store/userStore'
import categoryStore from '/store/categoryStore'
import projectStore from '/store/projectStore'
import eventStore from '/store/eventStore'
import {DndItemTypes} from '/config/Constants'

import {DragSource, DropTarget} from 'react-dnd'

import Toaster from '/services/Toaster'

import StateChangeRequestor, {REQUEST_PROJECT_ADD_USER} from '/services/StateChangeRequestor'
import EventSummaryBadge from "./EventSummaryBadge";
import uiStateStore, {ProjectSettings} from "/store/uiStateStore";
import {DEFAULT_CAT, ProjectType, UserRole} from "../../../config/Constants";
import {EventSource} from "../../../config/EventConfig";
import {getAddUserMsg} from "../../../utils/MessageUtils";
import {addMember} from "../../../models/ProjectModel";
import Moment from "react-moment";
import MoreHoriz from "../../common/MoreHoriz";
import {stripHtml} from "../../../utils/solo/stripHtml";
import DumbCapsule from "../../common/DumbCapsule";
import DumbBadge from "../../common/DumbBadge";
import ProjectApiService from "../../../services/ProjectApiService";
import ProjectMenu from "./ProjectMenu";
import {AlertTriangle} from "react-feather";

// let brakePoints = [350, 500, 750];
const brakePoints = [350,400,350,250];
const MAX_MEMBERS = 15

@observer
class MasonryContainer extends Component{
    /*

     projects={this.props.projects}
     showTags={this.props.showTags}
     currentUser={this.props.currentUser}
     users={this.props.users}

     */
    constructor(props) {
        super(props)
        this.state = {
            brakePoints: brakePoints,
            showLeaveConfirmation: false,
            showDeleteConfirmation: false,
            leavingProjectId: null,
            deletingProjectId: null,
            width: 0
        }
    }

    confirmLeave = (projectId) => {
        this.setState ({showLeaveConfirmation: true, leavingProjectId: projectId})
    }

    confirmDelete = (projectId) => {
        this.setState ({showDeleteConfirmation: true, deletingProjectId: projectId})
    }

    onLeaveConfirm = (ev) => {
        this.props.leaveProject(this.state.leavingProjectId)
        this.setState({showLeaveConfirmation: false, leavingProjectId: null})
    }

    onLeaveCancel = (ev) => {
        this.setState({showLeaveConfirmation: false, leavingProjectId: null})
    }

    onDeleteConfirm = (ev) => {
        this.props.deleteProject(this.state.deletingProjectId)
        this.setState({showDeleteConfirmation: false, deletingProjectId: null})
    }

    onDeleteCancel = (ev) => {
        this.setState({showDeleteConfirmation: false, deletingProjectId: null})
    }

    setContainerWidth() {
        const elem = findDOMNode(this)
        const width = elem? elem.offsetWidth : 0
        this.setState({width: width})
    }

    onResize = (ev) => {
        if (this && this.setContainerWidth)
            this.setContainerWidth()
    }

    componentDidMount() {
        this.setContainerWidth()
        window.addEventListener("resize", this.onResize)

    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.onResize)
    }


    render() {
        const categories = categoryStore.userCategories
        const {mobile, activeProjects, inactiveProjects} = this.props
        const {width, showLeaveConfirmation, showDeleteConfirmation} = this.state

        const selCatId = categoryStore.currCatId || DEFAULT_CAT

        const evs = eventStore.getInstance()
        const cagBadges = evs.currentBadges? evs.currentBadges.categories : null
        const selCatBadgeInfo = cagBadges? cagBadges[selCatId] : null

        let badgeCount, isNew, projectBadgeDetails

        return (
            <div className="container">
                <div className="masonry-container">
                    {/*<Dimmer.Dimmable dimmed={uiStateStore.creatingProjects}>*/}
                        {/*<Dimmer inverted active={uiStateStore.creatingProjects} >*/}
                            {/*<Loader inverted style={{marginTop: '0', top: '5%'}}>Creating Projects...</Loader>*/}
                        {/*</Dimmer>*/}

                        <Masonry brakePoints={this.state.brakePoints}
                                 parentWidth={width}
                        >
                            {[...activeProjects, ...inactiveProjects].map((project, idx) => {
                                if (!project)
                                    return null

                                projectBadgeDetails = selCatBadgeInfo && selCatBadgeInfo.projects && selCatBadgeInfo.projects[project.id]? selCatBadgeInfo.projects[project.id] : null
                                badgeCount = projectBadgeDetails? projectBadgeDetails.total : 0
                                isNew = projectBadgeDetails? Boolean(projectBadgeDetails.new) : false

                                return (
                                    <ProjectTile
                                        project={project}
                                        mobile={mobile}
                                        isNew={isNew}
                                        badgeCount={badgeCount}
                                        readOnly={this.props.readOnly}
                                        showTags={this.props.showTags}
                                        currentUser={this.props.currentUser}
                                        creator={userStore.getUserFromConnections(project.creatorId)}
                                        users={this.props.users}
                                        addUserToProject={this.props.addUserToProject}
                                        importCards={(cards) => {this.props.importCards(cards, project)}}
                                        importAssets={(assets, project, source) => {this.props.importAssets(assets, project, source)}}
                                        confirmLeave={this.confirmLeave}
                                        confirmDelete={this.confirmDelete}
                                        leaveProject={this.props.leaveProject}
                                        deleteProject={this.props.deleteProject}
                                        key={project.domId}
                                    />
                                )
                            })}
                        </Masonry>
                    {/*</Dimmer.Dimmable>*/}
                </div>

                {showLeaveConfirmation &&
                <div className='abs wModal ' style={{top:'30%',left:'15%'}}>
                    <div>Leave this project? You can always rejoin it later.</div>
                    <div className='btns'>
                        <button className='button neutral' onClick={this.onLeaveCancel}>Cancel</button>
                        <button className='button go' onClick={this.onLeaveConfirm}>Leave Board</button>
                    </div>
                </div>
                }

                {showDeleteConfirmation &&
                <div className='abs wModal ' style={{top:'30%',left:'15%'}}>
                    <div>Are you sure you want to permanently delete this board? This cannot be undone.</div>
                    <div className='btns'>
                        <button className='button neutral' onClick={this.onDeleteCancel}>Cancel</button>
                        <button className='button go' onClick={this.onDeleteConfirm}>Delete Board</button>
                    </div>
                </div>
                }

            </div>
        )
    }
}

const projectTarget = {
    drop(props, targetMonitor, component) {
        let projectId = props.project.id
        let project = projectStore.getProjectFromUserProjects(projectId)
        let dragItem = targetMonitor.getItem()

        // add card to project
        if (dragItem.type === DndItemTypes.CARD_THUMB) {
            props.importCards([dragItem.id], props.project)
        }

        // add dropbox item to project
        else if (dragItem.type === DndItemTypes.DROPBOX_THUMB) {
            props.importAssets([dragItem.asset], props.project, 'dropbox')
        }

        // add user
        else if (dragItem.type === DndItemTypes.PERSON) {
            let newMemberId = dragItem.id

            const existingUser = project.members.find(member => member.id === newMemberId )
            if (existingUser !== undefined) {
                let msg = getAddUserMsg( existingUser.role )
                Toaster.info(msg, {user: existingUser, project: project}, 1)
                return
            }

            ProjectApiService.addUsersToProject(projectId, userStore.currentUser.id, [newMemberId], EventSource.ProjectAdd, UserRole.INVITED).then( response => {
                // TODO refresh trigger needed?
            })
        }
    }
}

let origIndex = -1
const projectSource = {
    beginDrag(props) {
        origIndex = props.index
        // props.prepareMove()

        return {
            id: props.project.id,
            type: DndItemTypes.PROJECT,
            index: props.index,
            catId: props.catId,
            origIndex: origIndex // seems redundant, but isn't: index will be overwritten by final props.index, origIndex will not.
        }
    },
    endDrag(props, monitor) {
        // let finalIndex = props.index
        return {
            id: props.id,
            catId: props.catId,
            type: DndItemTypes.PROJECT,
            index: props.index,
        }
    }
}

@DragSource(DndItemTypes.PROJECT, projectSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
}))
@DropTarget([DndItemTypes.PERSON, DndItemTypes.CARD_THUMB, DndItemTypes.DROPBOX_THUMB], projectTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@observer
class ProjectTile extends Component {
    constructor(props) {
        super(props)
        const proj = props.project
        const memberCount = proj? proj.memberTotal : 0
        let moreCount = (memberCount > MAX_MEMBERS)? memberCount - MAX_MEMBERS : 0
        this.state = {memberCount: memberCount, moreCount: moreCount, loading: false, adding: false,
                      showDeleteConfirmation: false, showLeaveConfirmation: false, added: false}
        this.gotoProject = this.gotoProject.bind (this)
    }

    loadProject() {
        this.setState({loading: true})
        // Router.pushRoute('project', {id: this.props.project.id}) Replaced with Link Prefetch. Still using loader.
    }

    joinProject = () => {
        const { currentUser, project } = this.props
        this.setState({adding: true})
        projectStore.addUserToProject(project.id, currentUser.id, currentUser.id, EventSource.OnBoarding, UserRole.ACTIVE)
            .then( response => {
                this.setState({adding: false, added: true})
            })
    }

    gotoProject(ev) {
        // prevent container from stealing event.
        // TODO ProjectMenu stops propagation, and sits above on z plane, and works fine on ProjectSummary. Why/how is masonry stealing event?
        if (ev.target.tagName === 'svg')
            return


        ev.stopPropagation ()
        const isApproval = this.props.project && this.props.project.projectType === ProjectType.APPROVAL
        const route = isApproval? 'approve':'project'

        // No hoverlay on mobile, just OPEN.
        // if (this.props.mobile)
        Router.push ({
            pathname: '/' +route+ '/' + this.props.project.id
        })
    }


    gotoProjectSettings = (ev) => {
        ev.stopPropagation ()
        ev.preventDefault ()
        projectStore.settingsProject = this.props.project
        uiStateStore.unModal = ProjectSettings
    }

    onDropdownOpen(ev) {
        // ev.stopPropagation()
        // ev.preventDefault()
        this.setState({showDropdown: true})
    }
    onDropdownClose(ev) {
        // ev.stopPropagation()
        // ev.preventDefault()
        this.setState({showDropdown: false})
    }

    handleMenuItemSelect(ev, { name }) {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({ activeDropdownItem: name, showDropdown: false })
    }

    confirmLeave = (ev) => {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({showDropdown: false}, () => this.props.confirmLeave(this.props.project.id))
    }

    confirmDelete = (ev) => {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({showDropdown: false}, () => this.props.confirmDelete(this.props.project.id))
    }

    onDeleteConfirm = (ev) => {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({showDropdown: false, showDeleteConfirmation: false})
        this.props.deleteProject(this.props.project)
    }

    onDeleteCancel = (ev) => {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({showDropdown: false, showDeleteConfirmation: false})
    }

    rejoinProject = (ev) => {
        ev.stopPropagation()
        ev.preventDefault()
        const userId = userStore.currentUser.id
        this.props.addUserToProject(this.props.project.id, userId, userId, EventSource.InApp)
    }

    showMemberName(member) {
        if (member)
            this.setState({memberName: member.displayName || member.firstName || member.email})
    }

    clearMemberName() {
        this.setState({memberName: null})
    }

    generatePreview() {
        const {project, connectDragPreview, index} = this.props
        if (!document || !project)
            return


//      Solution 1 - works. use a static image. Size is ignored.
        const img = new Image(50,50)
        img.src = '/static/img/card1.png'
        connectDragPreview (img)
        img.onload = () => {
            connectDragPreview (img)
        }
    }

    componentDidMount() {
        this.generatePreview()
    }

    componentDidUpdate() {
        console.warn('masonry did update')
        this.generatePreview()
    }

    componentWillReact() {
        // console.log("$$$$$$ : Project " +this.props.project.title+ " will react.")
    }

    render() {
        const { added, adding, loading, memberName } = this.state
        const { canDrop, isOver, project, isNew, badgeCount, readOnly, item, mobile, connectDragSource, connectDropTarget, connectDragPreview } = this.props
        if (!project)
            return null

        const isActiveTarget = canDrop && isOver
        // const { activeDropdownItem } = this.state
        let imgBoxClass = 'imgBox'
        if (isActiveTarget)
            imgBoxClass += ' dropTarget'
        if (!project.image)
            imgBoxClass += ' noImage'

        if (!project.isActive && readOnly)
            project.isActive = true // don't apply inactive styles if we're just browsing

        if (!project.isActive)
            imgBoxClass += ' inactive'

        const projectTitle = project.title? project.title.trim().replace('&nbsp;', '') : 'Untitled Project'
        const popupTrigger = ( // wrap ellipse to allow imprecise clicking
            <div className="popupTrigger" style={{width: '18px', display: 'inline-block'}}><MoreHoriz size={24} /></div>
        )

        const hasDesc = project.description && project.description.length > 0 && project.description !== 'No Description'
        const cleanDesc = hasDesc ? stripHtml (project.description.trim ().replace ('&nbsp;', '')) : ''

        const isCreator = project.creatorId === userStore.currentUser.id

        const latestMembers = project.members? project.members.filter( member => member.role !== UserRole.BANNED).slice(0, MAX_MEMBERS) : []

        const activeUser = userStore.currentUser
        const openAsAP = project.projectType === ProjectType.APPROVAL && (!activeUser || project.creatorId !== activeUser.id)
        const route = openAsAP? 'approve':'project'

        const dragWrapper = mobile? (elem) => elem : connectDragSource
        const dropWrapper = mobile? (elem) => elem : connectDropTarget
        const previewWrapper = mobile? (elem) => elem : connectDragPreview


        return previewWrapper ( dropWrapper ( dragWrapper (
            <div className='relative' onClick={this.gotoProject}>
                <div className='tile jProj' >

                    {isNew &&
                    <DumbCapsule msg='New' className='abs' style={{top: '.5rem', right: '.5rem'}} />
                    }

                    {badgeCount > 0 &&
                    <DumbBadge count={badgeCount} className='abs' style={{top: isNew? '2.5rem':'.5rem', right: '.5rem'}} />
                    }

                    <Loader inverted active={loading}>Opening...</Loader>

                    {/* TODO can we factor out hoverlay into component    */}
                    {!mobile &&
                    <div className="hoverlay pointer"  >
                        <Loader active={loading}>Opening...</Loader>
                        <Loader active={adding}>Adding...</Loader>

                        <ProjectMenu project={project}
                                     activeUser={activeUser}
                                     className='abs cardBG borderAll boxRadius'
                                     style={{right: '.75rem', top: '.75rem', height: '1.8rem', zIndex:2}}
                                     confirmLeave={this.confirmLeave}
                                     confirmDelete={this.confirmDelete}
                                     rejoinProject={this.rejoinProject}/>

                        <div className='jFG small noWrap'>
                            {projectTitle}
                        </div>

                        {hasDesc &&
                        <div className="jSec tiny">
                            {cleanDesc}
                        </div>
                        }

                        {this.props.creator &&
                        <div className="jSec tiny">
                            By {this.props.creator.displayName}
                        </div>
                        }

                        {openAsAP &&
                        <div className='tiny mt1 jSec'>{project.cardTotal} items, {project.memberTotal} members.</div>
                        }

                        <div className="projectStats abs" style={{bottom: '1rem'}}>
                            {memberName &&
                            <div className='popup abs tiny dontWrap'
                                 style={{top: '-3rem', padding: '.25rem 1.5rem', minWidth: '8rem'}}>
                                {memberName}
                            </div>
                            }

                            {latestMembers.map ((member, i) =>
                                <img className="roundIconBdr" key={'mem' + member.id} src={member.image}
                                     data-tip={member.displayName}
                                     style={{marginRight: '-.2rem'}}
                                     onMouseEnter={() => this.showMemberName (member)}
                                     onMouseLeave={() => this.clearMemberName ()}
                                />
                            )}

                        </div>

                    </div>
                    }

                    {project.image &&
                    <img src={project.image} className="coverImg"/>
                    }

                    <div style={{padding: '.75rem .75rem 0 .75rem'}}>
                        <div className='bold jFGinv'>
                            {projectTitle}
                        </div>
                        <div className='micro jSecInv' style={{textTransform: 'upperCase'}}>
                            UPDATED <Moment subtract={{hours: 8}} fromNow>{project.dateModified}</Moment>
                        </div>
                    </div>

                </div>
            </div>
        )
        )
        )
    }
};

@observer
class Masonry extends Component{
    constructor(props){
        super(props);
/*
        if (typeof window !== 'undefined') {
            this.state = {columns: window.innerWidth < 700? 2 : 3}
        } else
            this.state = {columns: 3}; // not used? see brakepoints
*/

    }

    getNumOfCols = (containerWidth) => {
        if (this.props.mobile || (containerWidth > 300 && containerWidth < 380))
            return 2

        const {brakePoints} = this.props
        if (containerWidth === 0)
            return brakePoints.length

        let sum = 0, colCount = 1
        while (colCount <= (brakePoints.length)) {
            sum += brakePoints[colCount-1]
            if (sum >= containerWidth)
                break
            colCount++
        }

        console.log('getNumOfCols. result: ' +colCount)
        return colCount
    }

    distributeTilesToColumns(){
        let columns = [];
        const {parentWidth} = this.props
        const numC = this.getNumOfCols(parentWidth)
        for(let i = 0; i < numC; i++){
            columns.push([]); // array of empty arrays
        }
        const tiles = this.props.children

        const result = tiles.reduce( function(columns, tile, idx) {
            columns[ idx % numC ].push(tile);
            return columns;
        }, columns);

        return result
    }

    componentWillReact() {
        console.log("Masonry will react.")
    }

    render(){
        const columns = this.distributeTilesToColumns()

        return (
            <div className="masonry" ref="Masonry">
                {columns.map( (column, columnIdx) => {
                    return (
                        <div className="column" key={'col'+columnIdx} >
                            <ReactCSSTransitionGroup
                                transitionName="cardfx"
                                transitionEnterTimeout={300}
                                transitionLeaveTimeout={200}>

                            {column.map( (child, idx) => {
                                return (
                                    <div key={'cc'+idx}>
                                        {child}
                                    </div>
                                )
                            })}
                            </ReactCSSTransitionGroup>
                        </div>
                    )
                })}
            </div>
        )
    }
}


export { MasonryContainer }