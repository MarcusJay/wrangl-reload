import {Component} from 'react'
import {findDOMNode} from "react-dom";
import {observer} from 'mobx-react'
import {DndItemTypes, UserRole} from '/config/Constants'
import {getEmptyImage} from 'react-dnd-html5-backend'
import {Link, Router} from '~/routes'
import domToImage from 'dom-to-image'

import PropTypes from 'prop-types'
import {Icon} from 'semantic-ui-react'

import {DragSource, DropTarget} from 'react-dnd'

import StateChangeRequestor, {REQUEST_PROJECT_ADD_USER} from '/services/StateChangeRequestor'

import Toaster from '/services/Toaster'
// Read only
import projectStore from '/store/projectStore'
import userStore from '/store/userStore'
import Moment from 'react-moment'
import {EventSource} from "/config/EventConfig";
import EventSummaryBadge from "./EventSummaryBadge";
import {getAddUserMsg} from "/utils/MessageUtils";
import {addMember} from "/models/ProjectModel";
import {randomIntBetween} from "../../../utils/solo/randomlntBetween";
import {stripHtml} from "../../../utils/solo/stripHtml";
import uiStateStore, {ProjectSettings} from "../../../store/uiStateStore";
import {scaleImage} from "../../../utils/solo/scaleImage";
import DumbBadge from "../../common/DumbBadge";
import DumbCapsule from "../../common/DumbCapsule";
import ProjectApiService from "../../../services/ProjectApiService";
import {ARCHIVED_CAT, ProjectType} from "../../../config/Constants";
import ProjectMenu from "./ProjectMenu";
import categoryStore from "../../../store/categoryStore";

const MAX_MEMBERS = 12

let origIndex = -1
const projectSource = {
    beginDrag(props) {
        origIndex = props.index
        // props.prepareMove()

        return {
            id: props.project.id,
            type: DndItemTypes.PROJECT,
            index: props.index,
            catId: props.catId,
            origIndex: origIndex // seems redundant, but isn't: index will be overwritten by final props.index, origIndex will not.
        }
    },
    endDrag(props, monitor) {
        // let finalIndex = props.index
        return {
            id: props.id,
            catId: props.catId,
            type: DndItemTypes.PROJECT,
            index: props.index,
        }
    }
}


const projectTarget = {
    drop(props, targetMonitor, component) {
        let projectId = props.project.id
        let project = projectStore.getProjectFromUserProjects (projectId)
        let dragItem = targetMonitor.getItem ()

        // add user
        if (dragItem.type === DndItemTypes.PERSON) {
            debugger
            let newMemberId = dragItem.id

            const existingUser = project.members.find (member => member.id === newMemberId)
            if (existingUser !== undefined) {
                let msg = getAddUserMsg (existingUser.role)
                Toaster.info (msg, {user: existingUser, project: project}, 1)
                return
            }

            ProjectApiService.addUsersToProject(projectId, userStore.currentUser.id, [newMemberId], EventSource.ProjectAdd, UserRole.INVITED).then( response => {
                // TODO refresh trigger needed?
            })
        }
        // sort projects
/*
        } else if (dragItem.type === DndItemTypes.PROJECT) {
            debugger
            const dragIndex = dragItem.index
            const hoverIndex = props.index
            // Time to actually perform the action
            if (dragItem.origIndex !== hoverIndex)
                props.moveProject (dragItem.origIndex, hoverIndex)
            else
                props.cancelMove ()
        }
*/

        // return { name: 'Project' }
    }
}

@DragSource(DndItemTypes.PROJECT, projectSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
}))
@DropTarget([DndItemTypes.PERSON, DndItemTypes.CARD_THUMB, DndItemTypes.DROPBOX_THUMB], projectTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget (),
    isOver: monitor.isOver (),
    canDrop: monitor.canDrop (),
    item: monitor.getItem ()
}))
@observer
export default class ProjectSummary extends Component {
    static propTypes = {
        // connectDragSource: PropTypes.func,
        connectDropTarget: PropTypes.func,
        isOver: PropTypes.bool,
        canDrop: PropTypes.bool,
        // item: PropTypes.any,
        moveProject: PropTypes.func,
        cancelMove: PropTypes.func
    }

    constructor(props) {
        super (props)
        // this.handleDelete = this.handleDelete.bind(this)
        this.handleMenuItemSelect = this.handleMenuItemSelect.bind (this)
        this.gotoProject = this.gotoProject.bind (this)
        this.gotoProjectSettings = this.gotoProjectSettings.bind (this)
        this.mockActivityCount = randomIntBetween (1, 20)
        this.state = {
            loading: false,
            activeMenuItem: 'open',
            hoverProject: false,
            showDeleteConfirmation: false,
            showLeaveConfirmation: false,
            tags: this.props.project.tags2
        }
    }

    loadProject() {
        this.setState ({loading: true})
        // Router.prefetchRoute('project', {id: this.props.project.id}) Replaced with Link Prefetch. Still using loader.
    }

    hoverProject(isHover) {
        this.setState({isHover: isHover})
    }

    onDropdownOpen(ev) {
        ev.stopPropagation ()
        ev.preventDefault ()
        this.setState ({showDropdown: true})
    }

    handleMenuItemSelect(ev, {name}) {
        ev.stopPropagation ()
        ev.preventDefault ()
        this.setState ({activeDropdownItem: name, showDropdown: false})
    }

    confirmLeave = (ev) => {
        ev.stopPropagation ()
        ev.preventDefault ()
        this.setState ({showDropdown: false}, () => {
            this.setState ({showLeaveConfirmation: true})
        })
    }

    confirmDelete = (ev) => {
        ev.stopPropagation ()
        ev.preventDefault ()
        this.setState ({showDropdown: false}, () => {
            this.setState ({showDeleteConfirmation: true})
        })
    }

    onDeleteCancel = (ev) => {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({showDropdown: false, showDeleteConfirmation: false})
    }

    onDeleteConfirm = (ev) => {
        ev.stopPropagation ()
        ev.preventDefault ()
        this.setState ({showDropdown: false, showDeleteConfirmation: false})
        this.props.deleteProject (this.props.project.id)
    }

    onLeaveConfirm = (ev) => {
        ev.stopPropagation ()
        ev.preventDefault ()
        this.setState ({showDropdown: false, showLeaveConfirmation: false})
        this.props.leaveProject (this.props.project.id)
    }

    /*
        onLeaveCancel = (ev) => {
            ev.stopPropagation()
            ev.preventDefault()
            this.setState({showDropdown: false, showLeaveConfirmation: false})
        }
    */

    rejoinProject = (ev) => {
        ev.stopPropagation ()
        const userId = userStore.currentUser.id
        this.props.addUserToProject (this.props.project.id, userId, userId, EventSource.InApp)
    }

    gotoProject(ev) {
        // prevent container from stealing event.
        // TODO ProjectMenu stops propagation, and sits above on z plane, and works fine on ProjectSummary. Why/how is masonry stealing event?
        if (ev.target.tagName === 'svg')
            return

        const isApproval = this.props.project && this.props.project.projectType === ProjectType.APPROVAL
        const route = isApproval? 'approve':'project'

        // No hoverlay on mobile, just OPEN.
        Router.push ({
            pathname: '/' +route+ '/' + this.props.project.id
        })
    }

    gotoProjectSettings(ev) {
        ev.stopPropagation ()
        ev.preventDefault ()
        projectStore.settingsProject = this.props.project
        uiStateStore.unModal = ProjectSettings
    }

    showMemberName(member) {
        if (member)
            this.setState({memberName: member.displayName || member.firstName || member.email})
    }

    clearMemberName() {
        this.setState({memberName: null})
    }

    generatePreview() {
        const {project, connectDragPreview, index} = this.props
        if (!document || !project)
            return


//      Solution 1 - works. use a static image. Size is ignored.
        const img = new Image(50,50)
        img.src = '/static/img/card1.png'
        connectDragPreview (img)
        img.onload = () => {
            connectDragPreview (img)
        }

        // Solution 2 - scale project image using canvas, results in tainted canvas error.

        // Solution 3 - domToImage
        // TODO CROSS DOMAIN ISSUE
/*
        const n2 = document.getElementById('ps'+index)

        domToImage.toPng(n2)
            .then(function (dataUrl) {
                const img = new Image();
                img.src = dataUrl;
                connectDragPreview(img);
                img.onload = () => connectDragPreview(img);
            })
            .catch(function (error) {
                console.error('something went wrong!', error);
            });

*/

    }

    componentDidMount() {
        this.generatePreview()
    }

    componentDidUpdate() {
        console.warn('profile.js did update')
        this.generatePreview()
    }

    render() {
        const {canDrop, isDragging, isOver, isNew, item, index, badgeCount, project, mobile, readOnly, connectDragSource, connectDropTarget, connectDragPreview} = this.props
        const isActiveTarget = canDrop && isOver
        const {isHover, memberName, showLeaveConfirmation, showDeleteConfirmation} = this.state
        let projectClass = 'gridView relative pointer'

        if (!project) {
            console.error('ProjectSummary missing project')
            return null
        }

        const activeUser = userStore.currentUser
        const openAsAP = project.projectType === ProjectType.APPROVAL && (!activeUser || project.creatorId !== activeUser.id)
        const route = openAsAP? 'approve':'project'

        if (isActiveTarget) {
            if (item.type === 'project')
                projectClass += ' sortTarget'
            else
                projectClass += ' dropTarget'
        }

        if (!project.isActive && readOnly)
            project.isActive = true // don't apply inactive styles if we're just browsing

/*
        if (!project.isActive)
            projectClass += ' inactive'
*/

        const popupTrigger = ( // wrap ellipse to allow imprecise clicking
            <div style={{width: '22px', display: 'inline-block'}} className='more'>
                <Icon name="ellipsis horizontal" style={{margin:'0 0 0 0.5rem'}}/></div>
        )

        // cleaning
        const hasDesc = project.description && project.description.length > 0 && project.description !== 'No Description'
        const cleanDesc = hasDesc ? stripHtml (project.description.trim ().replace ('&nbsp;', '')) : ''
        // const showDesc = cleanDesc.length > 0

        const latestMembers = project.members? project.members.filter( member => member.role !== UserRole.BANNED).slice(0, MAX_MEMBERS) : []
        const projectTitle = project.title ? project.title.trim ().replace ('&nbsp;', '') : 'Untitled Project'

        return connectDragPreview ( connectDropTarget ( connectDragSource (
            <div className='inline' style={{opacity: (isDragging || isOver)? 0.3 : 1}}
                 onMouseEnter={() => { this.hoverProject(true) }}
                 onMouseLeave={() => { this.hoverProject(false) }}
                 onClick={this.gotoProject}
            >

                <div className="gridViewOuter jProj relative">

                        <div className={projectClass} id={'ps'+index}>
                            <div className="coverImage" style={{backgroundImage: 'url(' + project.image + ')'}}>&nbsp;

                                {isNew &&
                                <DumbCapsule msg='New' className='abs' style={{top: '.5rem', right: '.5rem'}} />
                                }

                                {badgeCount > 0 &&
                                <DumbBadge count={badgeCount} className='abs' style={{top: isNew? '2.5rem':'.5rem', right: '.5rem'}} />
                                }
                            </div>

                            {/*<Link prefetch route={route} params={{id: project.id}} >*/}
                            <div className='hoverlay'>
                                {categoryStore.currCatId !== ARCHIVED_CAT &&
                                <ProjectMenu project={project}
                                             activeUser={activeUser}
                                             className='abs cardBG borderAll boxRadius'
                                             style={{right: '.75rem', top: '.75rem', height: '1.8rem'}}
                                             confirmLeave={this.confirmLeave}
                                             confirmDelete={this.confirmDelete}
                                             rejoinProject={this.rejoinProject}/>
                                }

                                <div className='jFG small noWrap ' style={{width: 'calc(100% - 2rem)'}}>
                                    {projectTitle}
                                </div>

                                {hasDesc &&
                                <div className="jSec tiny">
                                    {cleanDesc}
                                </div>
                                }

                                {this.props.creator &&
                                <div className="jSec tiny">
                                    By {this.props.creator.displayName}
                                </div>
                                }

                                {openAsAP &&
                                <div className="tiny jSec mt1" >
                                    {project.cardTotal} items, {project.memberTotal} members.
                                </div>
                                }

                                <div className="projectStats fullW abs pad1" style={{bottom: 0, left: 0}}>

                                    {memberName &&
                                    <div className='popup abs tiny dontWrap'
                                         style={{top: '-3rem', padding: '.25rem 1.5rem', minWidth: '8rem'}}>
                                        {memberName}
                                    </div>
                                    }
                                    {latestMembers.map( (member, i) =>
                                        <img className="roundIconBdr" key={'mem'+member.id} src={member.image} data-tip={member.displayName}
                                             style={{marginRight: '-.2rem'}}
                                             onMouseEnter={() => this.showMemberName(member)}
                                             onMouseLeave={() => this.clearMemberName()}
                                        />
                                    )}

                                </div>
                            </div>
                            {/*</Link>*/}


                            {/* bottom */}
                            <div style={{padding: '.75rem .75rem 0 .75rem'}}>
                                <div className='bold jFGinv noWrap'>
                                    {projectTitle}
                                </div>
                                <div className='micro jSecInv' style={{textTransform: 'upperCase', marginTop: '-2px'}}>
                                    UPDATED <Moment subtract={{hours: 8}} fromNow>{project.dateModified}</Moment>
                                </div>
                            </div>
                        </div>

                </div>
                {showLeaveConfirmation &&
                <div className='abs wModal' style={{top:'30%',left:'15%'}}>
                    <div className='bold large'>Leave from this project? You can always rejoin it later.</div>
                    <div className='btns'>
                        <button className='button neutral' onClick={this.onLeaveCancel}>Cancel</button>
                        <button className='button go' onClick={this.onLeaveConfirm}>Leave Board</button>
                    </div>
                </div>
                }

                {showDeleteConfirmation &&
                <div className='abs wModal' style={{top:'30%',left:'15%'}}>
                    <div className='bold large'>Are you sure you want to permanently delete this board? This cannot be undone.</div>
                    <div className='btns'>
                        <button className='button neutral' onClick={this.onDeleteCancel}>Cancel</button>
                        <button className='button go' onClick={this.onDeleteConfirm}>Delete Board</button>
                    </div>
                </div>
                }
            </div>
        )
        )
        )
    }
}