import {Component} from 'react'
// import Link from 'next/link'
import {Link, Router} from '~/routes'
import {observer} from 'mobx-react'
import {Dimmer, Grid, Loader} from 'semantic-ui-react'
import {Layers, MessageCircle, Trash, User} from 'react-feather'
import userStore from '/store/userStore'

import {REQUEST_PROJECT_OPEN, StateChangeRequestor} from '/services/StateChangeRequestor'
import {EventSource} from "../../../config/EventConfig";
import Moment from "react-moment";
import EventSummaryBadge from "./EventSummaryBadge";
import WEditableField from "../../common/WEditableField";
import projectStore from "../../../store/projectStore";
import uiStateStore from "../../../store/uiStateStore";

@observer
export default class ProjectList extends Component {
    constructor(props) {
        super (props)
        this.state = {
            column: null,
            // data: this.props.projects,
            direction: null,
            loading: false,
            loadingId: null,
            showTools: false,
            selIds: new Set([])
        }
    }

    loadProject(ev, projectId) {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState ({loading: true, loadingId: projectId})
        Router.pushRoute('project', {id: projectId})
    }

    saveProject(project) {
        projectStore.updateProject (project).then (updProject => {

        })
    }

    bulkDelete = () => {
        const selIds = this.state.selIds
        if (selIds.length === 0)
            return

        uiStateStore.deletingProjects = true
        projectStore.deleteProjects(selIds).then( r1 => {
            projectStore.getUserProjects(userStore.currentUser.id).then ( r2 => {
                uiStateStore.deletingProjects = false
                this.setState({showTools: false, selIds: new Set()})
            })
        })
    }

    handleSort = (clickedColumn) => () => {
        const {column, data, direction} = this.state

        if (column !== clickedColumn) {
            this.setState ({
                column: clickedColumn,
                // data: _.sortBy(data, [clickedColumn]),
                data: data.sort (function (a, b) {
                    if (clickedColumn === 'image')
                        return a.image ? a : b;
                    else
                        return b[clickedColumn] - a[clickedColumn]
                }),
                direction: 'ascending',
            })

            return
        }

        this.setState ({
            data: data.reverse (),
            direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    }

    rejoinProject(projectId) {
        const userId = userStore.currentUser.id
        this.props.addUserToProject (projectId, userId, userId, EventSource.InApp)
    }

    toggleSelect(projectId) {
        const selIds = this.state.selIds

        if (selIds.has(projectId))
            selIds.delete(projectId)
        else
            selIds.add(projectId)

        this.setState({showTools: selIds.size > 0, selIds: selIds})
    }

    render() {
        const {column, loading, loadingId, selIds, showTools, direction} = this.state
        const {isPF, mobile, activeProjects, inactiveProjects } = this.props

        return (
            <div className="listView" style={{marginTop: isPF? 0:'', minHeight: isPF? '2.85rem':'', overflowY: isPF? 'auto':''}}>
                <Dimmer.Dimmable dimmed={loading || uiStateStore.creatingProjects || uiStateStore.deletingProjects}>
                  <Dimmer inverted active={loading || uiStateStore.creatingProjects || uiStateStore.deletingProjects} >
                      <Loader inverted style={{marginTop: '0', top: '5%'}}><span className='jFG'>Opening...</span></Loader>
                  </Dimmer>

                   <Grid>
                   {!showTools &&
                   <Grid.Row key={'ghdr'} className='hdrRow'>
                       <Grid.Column width={2}>

                       </Grid.Column>
                       <Grid.Column width={9} sorted={column === 'title' ? direction : null}
                                    onClick={this.handleSort ('title')}>
                           Name
                       </Grid.Column>
                       {!mobile &&
                       <Grid.Column width={2} sorted={column === 'dateModified' ? direction : null}
                                    onClick={this.handleSort ('dateModified')}>
                           Activity
                       </Grid.Column>
                       }
                       <Grid.Column width={1} sorted={column === 'memberTotal' ? direction : null}
                                    onClick={this.handleSort ('memberTotal')}>
                           <User className='secondary' size={20}/>
                       </Grid.Column>
                       <Grid.Column width={1} sorted={column === 'cardTotal' ? direction : null}
                                    onClick={this.handleSort ('cardTotal')}>
                           <Layers className='secondary' size={20}/>
                       </Grid.Column>
                       <Grid.Column width={1} sorted={column === 'commentTotal' ? direction : null}
                                    onClick={this.handleSort ('commentTotal')}>
                           <MessageCircle className='secondary' size={20}/>
                       </Grid.Column>
                   </Grid.Row>
                   }

                   {showTools &&
                   <Grid.Row key={'ghdr'} className='hdrRow'>
                       <div onClick={this.bulkDelete} style={{padding: '0'}}>
                        <Trash size={24} className='secondary' />
                        <span className='secondary' style={{marginTop: '0.1rem'}}> Delete {selIds.size} project{selIds.size !== 1?'s':''}?</span>
                       </div>
                   </Grid.Row>
                   }

                   {[...activeProjects, ...inactiveProjects].map ( (project,i) => {

                        const rowClass = project.isActive ? '' : 'inactive'

                        return (
                            <Grid.Row key={'plp'+i} className={rowClass} onClick={(ev) => this.loadProject (ev, project.id)}
                                      style={{opacity: loading && loadingId !== project.id ? 0.4 : 1}}>
                                <Grid.Column width={2} className='c1'>
                                    {/*<input type='checkbox' onChange={() => this.toggleSelect(project.id)} />*/}
                                    <Link prefetch route='project' params={{id: project.id}}>
                                        <div className="coverImage" onClick={(ev) => this.loadProject (ev, project.id)}
                                             style={{
                                                 backgroundImage: 'url(' + project.image + ')',
                                                 height: '3rem',
                                                 width: '4.4rem',
                                                 borderRadius: '5px'
                                             }}>
                                        </div>
                                    </Link>
                                </Grid.Column>
                                <Grid.Column width={9} style={{position: 'relative'}}>
                                    {loadingId !== project.id &&
                                    <div>
                                        <h4 style={{fontWeight: 'normal', marginBottom: '0'}}>
                                            <WEditableField name="title" value={project.title} parent={project}
                                                            style={{textOverflow: 'ellipsis'}}
                                                            saveParent={(project) => this.saveProject (project)}
                                            />

                                        </h4>
                                        <span className='small third lighter nowrap abs'> {project.description}</span>
                                    </div>
                                    }

                                    <EventSummaryBadge project={project} abs/>

                                </Grid.Column>
                                {!mobile &&
                                <Grid.Column width={2}>
                                    {project.isActive &&
                                    <Link prefetch route='project' params={{id: project.id}}>
                                        <span onClick={(ev) => this.loadProject (ev, project.id)}>
                                            <Moment format='MMM D, YYYY'
                                                    subtract={{hours: 8}}>{project.dateModified}</Moment>
                                        </span>
                                    </Link>
                                    }

                                    {!project.isActive &&
                                    <span>
                                        On break
                                        <button className="button neutral" title="Rejoin"
                                                onClick={() => this.rejoinProject (project.id)}>Rejoin</button>
                                    </span>
                                    }
                                </Grid.Column>
                                }
                                <Grid.Column width={1}>
                                    {project.memberTotal}
                                </Grid.Column>
                                <Grid.Column width={1}>
                                    {project.cardTotal}
                                </Grid.Column>
                                <Grid.Column width={1}>
                                    {project.projectCommentTotal}
                                </Grid.Column>
                            </Grid.Row>
                        )
                    })}
                </Grid>
              </Dimmer.Dimmable>
            </div>
        )
    }
}

{/*
<List celled>
    {this.props.projects.map( project => {
        return(
            <Link route='project' params={{id: project.id}}>
                <List.Item>
                    <div className="coverImage" style={{backgroundImage: 'url('+project.image+')'}}>
                        {!project.image &&
                        <span>
                                            <Feather.Layers color="#777"/>
                                        </span>
                        }
                    </div>
                    <List.Content>
                        <List.Header>{project.title}</List.Header>
                        {project.description || project.cards.length+ ' cards and ' +project.members.length+ ' members.'}
                    </List.Content>
                </List.Item>
            </Link>
        )
    })}
</List>
*/
}
