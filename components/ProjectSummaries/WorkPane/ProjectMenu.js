import MoreHoriz from "../../common/MoreHoriz";
import {Menu, Popup} from "semantic-ui-react";

const ProjectMenu = ({project, className, style, activeUser,
                      confirmLeave, confirmDelete, rejoinProject}) => {
    let trigStyle = {width: '18px', display: 'inline-block'}
    if (style)
        trigStyle = Object.assign({}, style)

    return (
        <Popup
            trigger={
                <div style={trigStyle} className={className}>
                    <MoreHoriz size={24}/>
                </div>
            }
            on='click'
            position='bottom center'
            style={{padding: 0}}
        >

            <div className="tiny jFG kern1">
                <Menu vertical className='hioh' style={{width: 'unset'}}>
                    {project.isActive &&
                    <Menu.Item onClick={confirmLeave} className=''>
                        Leave Board
                    </Menu.Item>
                    }

                    {!project.isActive &&
                    <Menu.Item onClick={rejoinProject} className=''>
                        Rejoin Board
                    </Menu.Item>
                    }

                    {project.creatorId === activeUser.id &&
                    <Menu.Item onClick={confirmDelete} className=''>
                        Delete Board
                    </Menu.Item>
                    }
                </Menu>

            </div>
        </Popup>
    )
}

export default ProjectMenu