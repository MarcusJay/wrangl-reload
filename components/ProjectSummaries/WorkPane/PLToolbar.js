import React from 'react'
import {observer} from 'mobx-react'

import {Loader, Menu} from 'semantic-ui-react'
import {PlusCircle} from 'react-feather';
import {Link} from '~/routes'
import Responsive from 'react-responsive'
import PropTypes from 'prop-types'
import ProjectSearch from "./ProjectSearch";
import {MsgProjects} from "../../../config/Messages";
import uiStateStore from "../../../store/uiStateStore";
import InlineHint from "../../onboarding/InlineHint";
import {isBrowser} from "../../../utils/solo/isBrowser";
import projectStore from "../../../store/projectStore";
import userStore from "../../../store/userStore";

const SORT_DATE = 'time_modified'
const SORT_TITLE = 'project_title'

@observer
export default class PLToolbar extends React.Component {
    static propTypes = {
        projects: PropTypes.any,
        readOnly: PropTypes.bool,
        createProject: PropTypes.func,
        viewAsList: PropTypes.func,
        viewAsGrid: PropTypes.func,
        viewAsImages: PropTypes.func,
        sortBy: PropTypes.func
    }

    constructor(props) {
        super(props)
        this.createProject = this.createProject.bind(this)
        this.state = {selectedView: this.props.mobile? this.props.view : 'images',
                      projectQuery: '', searchResults: false,
                      creatingBoard: false,
                      activeDropdownItem: '', sortBy: SORT_DATE, loading: false, commonProjectsCount: 0}

    }

    createProject() {
        this.setState({creatingBoard: true})
        this.props.createProjects(1)
    }

    viewAsList = () => {
        this.props.viewAsList()
        this.setState({selectedView: 'list'})
    }

    viewAsGrid = () => {
        this.props.viewAsGrid()
        this.setState({selectedView: 'grid'})
    }

    viewAsImages = () => {
        this.props.viewAsImages()
        this.setState({selectedView: 'images'})
    }

    toggleTags() {
        this.props.toggleTags()
    }

    toggleSort() {
        const sortBy = this.state.sortBy === SORT_DATE? SORT_TITLE : SORT_DATE
        this.setState({sortBy: sortBy})
        this.props.sortBy(sortBy)
    }

    resetSearch() {
        this.setState ({loading: false, searchResults: [], value: ''})
    }

    onDropdownOpen(ev) {
        ev.stopPropagation()
        ev.preventDefault()
    }

    onDropdownClose(ev) {
        ev.stopPropagation()
        ev.preventDefault()
    }

    handleMenuItemSelect(ev, { name }) {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({ activeDropdownItem: name })
    }

    handleProjectQuery(results) {
        this.props.handleProjectQuery(results)
    }

    showMsgs = (ev) => {
        this.props.showMsgs()
    }

    showProjects = (ev) => {
        this.props.showProjects()
    }

    componentDidMount() {
        const {friend} = this.props
        if (userStore.currentUser && friend) {
            projectStore.countCommonProjects ([userStore.currentUser.id, friend.id]).then (count => {
                this.setState ({commonProjectsCount: count})
                if (count === 0)
                    this.showMsgs ()
            })
        }
    }

    render() {
        const {mobile, showLoading} = this.props
        const { creatingBoard, selectedView } = this.state
        const isPF = uiStateStore.currentPage === uiStateStore.PF

        const projects = uiStateStore.currentPage === uiStateStore.PF? projectStore.projectsInCommon : projectStore.userProjects
        const projectCount = projects? projects.length : 0
        return(
            <div className="">

                {isBrowser() && uiStateStore.showInlineHints === true && uiStateStore.hintProjects === true &&
                <InlineHint name='hintProjects' cookie='hps' title='Welcome to your dashboard!'
                            body={MsgProjects}
                />
                }

                <Menu attached='top' className='toolbar-menu toolbar-aux' style={{paddingLeft: mobile? '0':''}}>
                    {false && !this.props.readOnly &&
                    <div>
                        <Responsive maxWidth={1020} >
                            <Menu.Header style={{color: "#999", marginTop: '0.8rem'}}>New</Menu.Header>
                        </Responsive>
                        <Responsive minWidth={1021} >
                            <Menu.Header style={{color: "#999", marginTop: '0.8rem'}}>New</Menu.Header>
                        </Responsive>
                    </div>
                    }
                    {!this.props.readOnly &&
                    <Menu.Item data-tip="Create New Board" >
                        {!creatingBoard &&
                        <div className="utNewProject topLeftNav pointer" style={{marginRight: '0.25rem'}}
                             onClick={this.createProject}>
                            <img src='/static/svg/NewBoardLabelled.svg' height={33} style={{marginTop: '2px'}}/>
                            {/*<button className="mui-btn mui-btn--primary mui-btn--raised round">+</button>*/}
                        </div>
                        }
                        {creatingBoard &&
                            <div className='pad1 jBlue italic working'>Creating Board...</div>
                        }
                    </Menu.Item>
                    }

                    {!mobile && projectCount > 0 &&
                    <Menu.Item >
{/*
                        <img src='/static/svg/v2/viewList.svg' onClick={this.viewAsList} data-tip="View as List"
                             className={'jIcon inline mr05' +(selectedView === 'list'? ' active':' inactive')}/>
*/}
                        <img src='/static/svg/v2/viewGrid.svg' onClick={this.viewAsGrid} data-tip="View as Grid"
                             className={'jIcon inline mr05' +((selectedView === 'grid'? ' active':' inactive'))}/>
                        <img src='/static/svg/v2/viewGallery.svg' onClick={this.viewAsImages} data-tip="View as Masonry"
                             className={'jIcon inline mr05' +(selectedView === 'images'? ' active':' inactive')}/>
                    </Menu.Item>
                    }

                    {projectCount > 0 &&
                    <Menu.Item >
                        <ProjectSearch {...this.props} handleProjectQuery={(query) => this.handleProjectQuery(query)}/>
                    </Menu.Item>
                    }

                    {showLoading &&
                    <div className='ml1 mt1 inline jBlue italic working'>
                        Fetching latest activity...
                    </div>
                    }



                </Menu>

            </div>
        )
    }
}