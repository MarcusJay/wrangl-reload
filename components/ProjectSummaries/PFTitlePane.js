import {Component} from 'react'
import {Image} from 'semantic-ui-react'
import {Chrome, Menu as FMenu, Star} from 'react-feather'
import ProfileMenu from "/components/common/ProfileMenu";
import SystemStatus from "/components/common/SystemStatus";
import uiStateStore from "/store/uiStateStore";
import projectStore from "/store/projectStore";
import {firstWord} from "../../utils/solo/firstWord";
import RU from '/utils/ResponsiveUtils'
import LoginMenu from "../common/LoginMenu";
import HelpButton from "../help/HelpPopup";

export default class PFTitlePane extends Component {

    state = {largeAvatar: false}

    showLargeAvatar = () => this.setState ({largeAvatar: true})
    hideLargeAvatar = () => this.setState ({largeAvatar: false})

    toggleSidebar() {
        uiStateStore.toggleSidebar ()
    }

    render() {
        const {mobile, user, friend, totalProjects} = this.props
        const project = projectStore.currentProject
        const {largeAvatar} = this.state
        const viewWidth = RU.getViewPortWidth ()
        const viewHeight = RU.getViewPortHeight ()
        const avatarW = Math.min (500, viewWidth * .5)
        const avatarH = Math.min (500, viewWidth * .5)


        if (mobile) {
            return (
                <header className="mFraming">
                    <div className='left'>
                        <FMenu onClick={() => this.toggleSidebar ()}/>
                    </div>
                    <div className='centered dontWrap' style={{marginRight: '1rem', maxWidth: '80%'}}>
                        <Image avatar src={friend.image}
                               style={{width: '1.5rem', height: '1.5rem', marginRight: '1rem'}}/>
                        {friend.displayName}
                    </div>
                    <div className='absTopRight' style={{marginRight: '-1rem'}} data-tip="Open profile menu">
                        <ProfileMenu className="right" user={user} mobile/>
                    </div>
                </header>
            )
        }

        // large
        return (

            <header className="jMidBG titlePane">
                <div className="row1">
                    <div className={"projectImage inline"}>
                            <span className='left round avatar'
                                  style={{
                                      backgroundImage: 'url(' + friend.image + ')',
                                      width: '4rem',
                                      height: '4rem',
                                      marginRight: '1rem'
                                  }}
                                  onMouseEnter={this.showLargeAvatar}
                                  onMouseLeave={this.hideLargeAvatar}
                            >
                            </span>
                    </div>
                    <div className="inline" style={{paddingLeft: '2rem', width: '60%'}}>
                        <SystemStatus/>
                        <h1 className='thinH fg' style={{marginBottom: '.5rem'}}>{friend.displayName}</h1>
                        {friend.pinned &&
                        <div style={{marginBottom: '0.5rem'}}>
                            <Star size={18} style={{marginRight: '0.5rem'}}/>
                            <span className='vaTop small secondary'>In your favorites.</span>
                        </div>
                        }

                        {friend.bio &&
                        <div className='small lighter' style={{marginBottom: '0.5rem'}}>{friend.bio}</div>
                        }

                        {friend.email && friend.isEmailPublic &&
                        <div style={{marginBottom: '0.5rem'}}>{friend.email}</div>
                        }

                        <div className='small lighter'>
                            You
                            and {firstWord (friend.displayName)} have {totalProjects} project{totalProjects !== 1 ? 's' : ''} in
                            common
                        </div>
                    </div>


                    <div className="absTopRight right secondary" style={{textAlign: 'right'}}>

                        <HelpButton className='mr1' />

                        <div className='utChrome inline vaTop mr1'>
                            <a href='https://chrome.google.com/webstore/detail/pogoio/pnonlpnonheokmhpjmjnonhkijehjdcf' target='_blank' className='jSec'
                               data-tip="Our chrome extension allows you to pull images and links from other sites directly into your boards. Get it now! ">
                                {/*<Chrome size={24} />*/}
                                <img src='/static/img/chrome32.png' width='26'/>
                            </a>
                        </div>

                        {user &&
                        <ProfileMenu user={user}/>
                        }

                        {!user &&
                        <LoginMenu project={project}/>
                        }

                    </div>
                </div>
            </header>
        )
    }
}