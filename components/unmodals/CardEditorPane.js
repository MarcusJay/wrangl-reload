import {Component} from 'react'
import {observer} from 'mobx-react'
import Dropzone from 'react-dropzone'
import {Grid, Loader} from 'semantic-ui-react'
import {Image as FImage, X} from 'react-feather'
import userStore from "../../store/userStore";
import cardStore from "../../store/cardStore";
import projectStore from "../../store/projectStore";
import CardTagStrip from "../common/Card/CardTagStrip";
import uiStateStore from "../../store/uiStateStore";
import AttachmentsPane from "./AttachmentsPane";
import {sendImage} from "../../services/ImageFileService";
import CardCarousel from "../Approvals/CardCarousel";

@observer
export default class CardEditModal extends Component {

    constructor(props) {
        super (props)
        this.state = {saving: false, uploading: false, error: null}
        this.updateProgress = this.updateProgress.bind (this)
        this.prevCardId = null
    }

    handleClose = (ev) => {
        this.handleSave (ev, true).then( response => {
            uiStateStore.setEditingCard(null)
            uiStateStore.unModal = null
            window.removeEventListener('keydown', this.handleKeyDown)
        })
    }

    handleViewer = (ev) => {
        uiStateStore.setViewingCard(uiStateStore.editingCard)
        this.handleClose(ev)
        // uiStateStore.unModal = CardEditor
        // uiStateStore.unModal = CardViewer
    }

    selCard = (cardId) => {
        const cards = cardStore.currentCards || []
        if (!cardId || !cards)
            return

        const index = cards.findIndex (c => c.id === cardId)
        this.selIndex(index)
    }

    selIndex = (index) => {
        uiStateStore.editingCardIndex = index
        uiStateStore.setEditingCard(cardStore.currentCards[index])
    }

    atFirstSlide() {
        return uiStateStore.editingCardIndex === 0
    }

    atLastSlide() {
        return uiStateStore.editingCardIndex === cardStore.currentCards.length - 1
    }


    handleChange = (ev) => {
        // ev.preventDefault ()
        // ev.stopPropagation ()

        const targetElem = ev.target;
        let propValue = targetElem.type === 'checkbox' ? targetElem.checked : targetElem.value;
        const propName = targetElem.name;

        this.setState({[propName]: propValue})
        // reflecting this react state change back to mobx, since the former works.
        uiStateStore.updateEditingCard(propName, propValue)
    }

    handleKeyDown = (ev) => {
        if (ev.key === 'Enter')
            this.handleSave(ev)
        else if (ev.key === 'Escape')
            this.handleClose(ev)
    }

    handleSave = (ev, saveAll) => {
        console.trace("HandleSave")
        const card = uiStateStore.editingCard
        const targetElem = ev.target

        return new Promise ((resolve, reject) => { // for resolving to other handlers like close

            if (saveAll) {
                this.setState ({saving: true})
                cardStore.saveCard (card).then (cardResponse => {
                    this.setState ({saving: false})
                    resolve (true)
                }).catch (error => {
                    this.setState ({error: error})
                })
            } else {
                const propValue = targetElem.type === 'checkbox' ? targetElem.checked : targetElem.value;
                const propName = targetElem.name;
                if (this.hasCardChanged(propName)) {
                    this.setState ({saving: true})
                    cardStore.saveCard ({id: card.id, [propName]: propValue}).then (card => {
                        this.setState ({saving: false, [propName]: propValue})
                        resolve (true)
                    }).catch (error => {
                        this.setState ({error: error})
                    })
                } else {
                    resolve(true)
                }
            }
        })
    }

    hasCardChanged(propName) {
        if (!this.origCard || !uiStateStore.editingCard)
            return false

        if (propName !== undefined)
            return this.origCard[propName] !== uiStateStore.editingCard[propName]

        let changed = false
        ['title','description','url'].forEach( prop => {
            if (this.origCard[prop] !== uiStateStore.editingCard[prop])
                changed = true
        })
        return changed
    }

    onDrop(files) {
        this.fileRefs = files
        if (files.length > 0) {
            let fileRef = files[0]
            sendImage (fileRef, false, this.updateProgress, uiStateStore.editingCard.id, 1)
            // see updateProgress for results handling
        }
    }

    updateProgress(count, msg, uploadedImageUrl) {
        if (msg === 'start') {
            this.setState({uploading: true})
        } else if (msg === 'end') {
            const card = uiStateStore.editingCard
            if (card) {
                if (uploadedImageUrl)
                    card.image = uploadedImageUrl
                else if (this.fileRefs && this.fileRefs.length > 0) {
                    card.image = this.fileRefs[0].preview
                }
            }
            this.setState ({uploading: false})
            if (this.fileRefs)
                this.fileRefs.forEach (file => window.URL.revokeObjectURL (file.preview))

        }
    }

    initCardEditing () {
        const ec = uiStateStore.editingCard
        this.origCard = Object.assign({}, ec)
        uiStateStore.filterDiscussionByCard = true
        this.prevCardId = ec.id
    }

    componentDidMount() {
        // transfering state from mobx to here while editing, because one of these 2 works.
        if (uiStateStore.editingCard) {
            this.initCardEditing()
        }
        window.addEventListener('keydown', this.handleKeyDown)

    }

    componentDidUpdate() {
        if (uiStateStore.editingCard && this.prevCardId !== uiStateStore.editingCard.id) {
            this.initCardEditing()
        }
    }

    componentWillUnmount() {
        window.removeEventListener('keydown', this.handleKeyDown)
    }

    render() {
        const {saving, uploading, error} = this.state
        let card = uiStateStore.editingCard
        if (!card)
            card = cardStore.currentCards[0]

        const forMobx = uiStateStore.editingCard? uiStateStore.editingCard.title : null

        // const card = cardStore.editingCard
        const project = projectStore.currentProject

        const authorId = card? card.creatorId : null
        const author = authorId ? userStore.getUserFromConnections (authorId) : null

        const atFirst = this.atFirstSlide()
        const atLast = this.atLastSlide()

        return (
            <div className='cardEditor hidden-scroll' style={{width: '100%', height: '99%'}}>
                <Loader active={saving || uploading}>Saving</Loader>

                <CardCarousel
                    cards={cardStore.currentCards}
                    reactions={true}
                    selCard={this.selCard}
                    selIndex={this.selIndex}
                    selectedCard={uiStateStore.editingCard}
                    selectedIndex={uiStateStore.editingCardIndex}
                    atFirst={atFirst}
                    atLast={atLast}
                />

                <div className='cardBG jFG card relative' style={{height:'88%', overflowY: 'auto'}}>
                    <Grid className='fullW'>
                        <Grid.Row className='fullW'>
                            <Grid.Column width={6}>

                                <Dropzone onDrop={this.onDrop.bind(this)}
                                          multiple={false}
                                          className='upload-box'
                                          style={{width: '100%', height: '100%', padding: 0}}
                                >
                                    <img name='image' src={card.image}
                                         style={{maxWidth: '100%', maxHeight: '18rem'}}/>
                                </Dropzone>

                            </Grid.Column>
                            <Grid.Column width={9} className='form fullW small'>
                                <div className='frow fullW'>
                                    <label className='block' style={{margin: '1rem 0 0 0'}}>Title</label>
                                    <input type='text' name='title' onChange={this.handleChange}
                                                onBlur={this.handleSave} onKeyDown={this.handleKeyDown}
                                                className='formPane fullW mt05'
                                                value={card.title} placeholder='Enter a title'/>
                                </div>

                                <div className='frow fullW'>
                                    <label className='block' style={{margin: '1rem 0 0 0'}}>Description</label>
                                    <textarea name='description' onChange={this.handleChange}
                                            onBlur={this.handleSave} onKeyDown={this.handleKeyDown}
                                            placeholder='Enter a description...'
                                            rows={3}
                                            className='formPane fullW mt05'
                                            style={{resize: 'none'}}
                                            value={card.description}>
                                        {card.description}
                                    </textarea>
                                </div>

                                <div className='frow fullW'>
                                    <label className='block' style={{margin: '1rem 0 0 0'}}>URL</label>
                                    <input type='url' name='url' placeholder='Enter a URL...'
                                            onChange={this.handleChange} onBlur={this.handleSave} onKeyDown={this.handleKeyDown}
                                            className='formPane fullW mt05'
                                            value={card.url}/>
                                </div>

                                <div className='secondary fullW mt05'>
                                    {author &&
                                    'Added by ' + author.displayName + (card.dateCreated ? (' on ' + new Date (card.dateCreated).toLocaleDateString ()) : '')
                                    }
                                    {!author && card.dateCreated &&
                                    'Added on ' + new Date (card.dateCreated).toLocaleDateString ()
                                    }
                                </div>


                            </Grid.Column>
                        </Grid.Row>
                    </Grid>

                    <div className='fullW' style={{padding: '0 1rem'}}>
                        <CardTagStrip tags={card.tags2}
                                      card={card}
                                      availTags={project.tagSet2}
                                      editMode={true}
                                      tagCard={(tagId) => this.tagCard (tagId)}
                                      untagCard={(tagId) => this.unTagCard (tagId)}
                        />
                    </div>

                    <div style={{width: '100%'}}>
                        <AttachmentsPane embedded editMode={true}/>
                        <p></p><p></p>
                    </div>

                    <div className='abs' style={{top: '.5rem', right: '.5rem'}}>
{/*
                        <FImage size={24}
                                onClick={this.handleViewer}
                                data-tip='Preview this Item'
                                className='hoh pointer secondary mr05' />
*/}
                        <button onClick={this.handleClose} className='button go close'
                                data-tip='Close Advanced Editor'>Done</button>
                    </div>
                </div>
            </div>
        )
    }
}
