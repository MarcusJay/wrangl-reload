import {Component} from 'react'
import {observer} from 'mobx-react'
import {AlertTriangle, Lock} from 'react-feather'
import {Loader, Radio} from "semantic-ui-react";
import {Link, Router} from '~/routes'

import {NOTIF_ALL, NOTIF_NONE, PROJECT_PRIVATE, PROJECT_PUBLIC} from '/config/Constants'

import uiStateStore from "/store/uiStateStore"
import projectStore from '/store/projectStore'
import ReactionApiService from "../../services/ReactionApiService";
import {STARS, THUMBS} from "../../config/ReactionConfig";
import userStore from "../../store/userStore";
import ExportService from "../../services/ExportService";
import {ExportPreview} from "../../store/uiStateStore";
import Toaster from "../../services/Toaster";
import ProjectTags from "../ProjectDetail/LeftPane/ProjectTags";

@observer
export default class ProjectSettingsPane extends Component {
    constructor(props) {
        super (props)
        this.state = {
            saving: false,
            loading: false,
            showLeaveConfirmation: false,
            showDeleteConfirmation: false,
        }
    }

    close = () => {
        projectStore.settingsProject = null
        uiStateStore.unModal = null
    }

    setNoVoting = (ev) => {
        this.setVotingType('DELETE')
    }

    setThumbVoting = (ev) => {
        const thumbs = ReactionApiService.getReactionSetByTitleFromDefaults(THUMBS)
        this.setVotingType(thumbs.reaction_set_id)
    }

    setStarVoting = (ev) => {
        const stars = ReactionApiService.getReactionSetByTitleFromDefaults(STARS)
        this.setVotingType(stars.reaction_set_id)
    }

    setVotingType(reactionTypeId) {
        const project = projectStore.settingsProject
        this.setState({saving: true})
        if (!project)
            return

        projectStore.updateProject({id: project.id, reactionSetId: reactionTypeId}).then( updatedProject => {
            project.reactionSetId = reactionTypeId
            // Correct disparity between what we must send to nullify a reactionSet ('DELETE') and what we receive on reads (null)
            if (reactionTypeId === 'DELETE')
                project.reactionSetId = null
            this.setState({saving: false})
        })
    }

    setPrivacy = (level) => {
        const project = projectStore.settingsProject
        this.setState({saving: true})
        projectStore.updateProject({id: project.id, securityLevel: level}).then( updateProject => {
            project.securityLevel = level
            this.setState({saving: false})
        })
    }

    submitNotifLevel = (event) => {
        const targetElem = event.target
        let propValue = targetElem.checked? NOTIF_ALL : NOTIF_NONE

        this.setState({saving: true})
        projectStore.setUserProjectSettings(null, projectStore.settingsProject.id, propValue).then( response => {
            this.setState({saving: false})
        })
    }

    confirmLeave = () => {
        this.setState ({showLeaveConfirmation: true})
    }

    confirmDelete = () => {
        this.setState ({showDeleteConfirmation: true})
    }


    onDeleteCancel = () => {
        this.setState ({showDeleteConfirmation: false})
    }

    onLeaveCancel = () => {
        this.setState ({showLeaveConfirmation: false})
    }

    onDeleteConfirm = () => {
        this.setState ({showDeleteConfirmation: false, loading: true})
        const project = projectStore.settingsProject
        projectStore.deleteProject(project).then( response => {
            this.close()
            uiStateStore.disableUPFetch = true
            Router.pushRoute('/', {update: false})
        })

    }

    onLeaveConfirm = () => {
        this.setState ({showDeleteConfirmation: false, loading: true})
        const project = projectStore.settingsProject
        const userId = userStore.currentUser.id
        projectStore.removeUserFromProject(project.id, userId, userId, false).then( response => {
            this.close()
            uiStateStore.disableUPFetch = false
            Router.pushRoute('/', {update: true})
        })

    }

    exportPDF() {
        uiStateStore.exporting = true
        console.log ("Requesting PDF")
        const project = projectStore.settingsProject
        ExportService.getPDFBlobUrl (project).then (pdfUrl => {
            uiStateStore.unModal = ExportPreview
            uiStateStore.exportPreviewUrl = pdfUrl
            uiStateStore.exporting = false
        }).catch (error => {
            uiStateStore.unModal = null
            uiStateStore.exportPreviewUrl = null
            uiStateStore.exporting = false
            const details = error.name + ': ' + error.message
            console.error (details)
            Toaster.error ("There was a problem saving this Board as a PDF. For support: " + details)
        })
    }


    toggleTags = (ev) => {
        userStore.userPrefs.showTags = !userStore.userPrefs.showTags
        userStore.setUserPref(null, 'show_tags', userStore.userPrefs.showTags)
    }

    toggleStacks = (ev) => {
        uiStateStore.showStacks = false // !uiStateStore.showStacks // TODO no userPref for this yet
        // if (uiStateStore.showStacks)
        //     uiStateStore.showTags = true
    }

    toggleCrop = () => {
        // update observable ui prop
        uiStateStore.cropImages = !uiStateStore.cropImages

        // now persist it
        userStore.userPrefs.cropImages = uiStateStore.cropImages
        userStore.setUserPref(null, 'crop_images', userStore.userPrefs.cropImages)
    }

    componentDidMount() {
        projectStore.getUserProjectSettings(null, projectStore.settingsProject.id).then( project => {
            this.forceUpdate() // Should not be necessary
        })
    }

    componentWillReact() {
        const test = projectStore.settingsProject
        console.log('project settings will REACT')
    }

    render() {
        const {showDeleteConfirmation, showLeaveConfirmation, saving, loading} = this.state
        const project = projectStore.settingsProject
        if (!project)
            return null

        const activeUser = userStore.currentUser
        const secLev = project.securityLevel
        const notifLevel = project.notifLevel
        const stars = ReactionApiService.getReactionSetByTitleFromDefaults(STARS)
        const thumbs = ReactionApiService.getReactionSetByTitleFromDefaults(THUMBS)


        const amICreator = project && userStore.currentUser && userStore.currentUser.id === project.creatorId
        const test = projectStore.settingsProject.id

        return (
            <div className='unModal'>
                <Loader active={saving}>Saving...</Loader>
                <Loader active={loading}>Loading...</Loader>
                <img src={project.image} height='60' className='boxRadius mr1'/>
                <h2 className='inline vaTop mt1'>{project.title} settings</h2>
                <button onClick={this.close} className='button go close' >Done</button>
                <div className='content projectSettings'>
                    <p></p>
                    <section className='mb2'>
                        <h3 className='mb05'>Voting</h3>
                        <div className='row'>
                            <button
                                name='reactionSetId'
                                className={project.reactionSetId === thumbs.reaction_set_id? 'button go':'jBtn'}
                                style={{minWidth: '20%'}}
                                onClick={this.setThumbVoting}
                            >Approve or reject items
                            </button>

                            <button
                                className={project.reactionSetId === stars.reaction_set_id? 'button go':'jBtn'}
                                onClick={this.setStarVoting}
                                style={{minWidth: '20%'}}
                            >Rate items from 1 to 5 stars
                            </button>

                            <button
                                className={project.reactionSetId === null || project.reactionSetId === 'DELETE'?
                                    'button go':'jBtn'}
                                onClick={this.setNoVoting}
                                style={{minWidth: '20%'}}
                            >No voting
                            </button>

                        </div>
                    </section>

                    <section className='mb2'>
                        <h3>Tags</h3>

                        <ProjectTags project={project} enableEdit={true}/>
{/*
                        <p>
                            <Radio toggle checked={userStore.userPrefs.showTags} onChange={this.toggleTags}/>
                            <span className='vaTop ml1 jFG'>Tags</span>
                            <img className='ml1' src='/static/img/tags.png'
                                 height='26' style={{opacity: userStore.userPrefs.showTags? .8:.2}}/>
                        </p>
*/}
{/*
                        <p>
                            <Radio toggle checked={uiStateStore.showStacks} onChange={this.toggleStacks}/>
                            <span className='vaTop ml1 jFG'>Enable Stacks</span>
                            <img className='ml1' src='/static/img/stacks.png'
                                 height='26' style={{opacity: uiStateStore.showStacks? .8:.2}}/>
                        </p>
*/}

                        <h3>Images</h3>
                        <p>
                            <Radio toggle checked={uiStateStore.cropImages} onChange={this.toggleCrop}/>
                            <span className='vaTop ml1 jFG'>
                                {!uiStateStore.cropImages? 'Images fit into card':'Images fill card'}
                            </span>
                        </p>
                    </section>

                    <section className='mb2'>
                        <h3>Privacy</h3>
                        <button
                            className={secLev === PROJECT_PUBLIC? 'button go':'jBtn'}
                            onClick={() => this.setPrivacy(PROJECT_PUBLIC)}
                            style={{minWidth: '30%'}}
                        >
                            Public. Anyone with the link can view.
                        </button>

                        <button
                            className={secLev === PROJECT_PRIVATE? 'button go':'jBtn'}
                            onClick={() => this.setPrivacy(PROJECT_PRIVATE)}
                            style={{minWidth: '30%'}}
                        >
                            <Lock className='jSec mr05' size={16} style={{marginBottom: '-2px'}}/>
                            Private. Only members can view.
                        </button>

                    </section>

                    <section className='mb2'>
                        <h3>Actions</h3>

                        <button className='button neutral' name="PDF" onClick={this.exportPDF}
                                data-tip="Exports the entire board as a PDF file"
                        >
                            Save Board As a PDF
                        </button>

                        <button className='button neutral' name='leave' onClick={this.confirmLeave}
                                data-tip="You will no longer be a member of this board. It will be moved to your Archive"
                        >
                            Leave Board
                        </button>

                        {(!uiStateStore.voMode && activeUser && project && project.creatorId === activeUser.id) &&
                        <button className='button stop' name="delete" onClick={this.confirmDelete}
                                data-tip="Call Kenny Loggins, because you're in the Danger Zone. This will permanently delete the board. This cannot be undone."
                        >
                            <AlertTriangle className='mr05' size={18} style={{marginBottom: '-4px'}}/>
                            Delete Board Forever
                        </button>
                        }

                    </section>

                </div>

                {showLeaveConfirmation &&
                <div className='abs wModal inverted' style={{top: '30%', left: '15%'}}>
                    <div>Leave this board? It will be moved to your Archive folder. </div>
                    <div className='btns'>
                        <button className='button neutral' onClick={this.onLeaveCancel}>Cancel</button>
                        <button className='button go' onClick={this.onLeaveConfirm}>Leave Board</button>
                    </div>
                </div>
                }

                {showDeleteConfirmation &&
                <div className='abs wModal inverted' style={{top: '30%', left: '15%'}}>
                    <div>Are you sure you want to permanently delete this board? This cannot be undone.</div>
                    <div className='btns'>
                        <button className='button neutral' onClick={this.onDeleteCancel}>Cancel</button>
                        <button className='button go' onClick={this.onDeleteConfirm}>Delete Board</button>
                    </div>
                </div>
                }
            </div>
        )
    }
}