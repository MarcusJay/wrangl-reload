import {Component} from 'react'
import {observer} from 'mobx-react'
import {X} from 'react-feather'
import uiStateStore from "/store/uiStateStore"
import userStore from "/store/userStore"
import {Form, Loader} from "semantic-ui-react";
import Dropzone from 'react-dropzone'
import {sendImage} from "../../services/ImageFileService";

@observer
export default class UserSettingsPane extends Component {
    constructor(props) {
        super (props)
        this.state = {saving: false, user: userStore.currentUser, dropzoneActive: false}
    }

    close() {
        uiStateStore.unModal = null
    }

    handleKey = (event) => {
        if (event.key === 'Enter')
            this.handleChange(event)
    }

    handleChange = (event) => {
        const user = this.state.user
        // const user = userStore.currentUser

        const targetElem = event.target
        const propValue = targetElem.type === 'checkbox' ? targetElem.checked : targetElem.value
        const propName = targetElem.name
        user[propName] = propValue
        this.setState({user: user})

        if (event.key === 'Enter' || event.type === 'blur') {
            this.submitChange (event, propName, propValue)
        }
        // else
        //     userStore.updateCurrentUserClientOnly(propName, propValue)


    }

    submitChange = (event, propName, propValue) => {
        // const user = userStore.currentUser
        const user = this.state.user

        this.setState({saving: true})
        userStore.saveUser({id: user.id, [propName]: propValue}).then( user => {
            this.setState({saving: false})
        })
    }

    onDrop(files) {
        this.fileRefs = files
        if (files.length > 0) {
            let fileRef = files[0]
            sendImage (fileRef, false, this.updateProgress.bind(this))
            // see updateProgress for results handling
        }
    }

    onDragEnter = () => {
        this.setState({dropzoneActive: true})
    }

    onDragLeave = () => {
        this.setState({dropzoneActive: false})
    }

    updateProgress(count, msg, uploadedImageUrl) {
        if (msg === 'start') {
            this.setState({saving: true})
        } else if (msg === 'end') {
            const user = userStore.currentUser
            if (uploadedImageUrl)
                user.image = uploadedImageUrl
            userStore.saveUser({id: user.id, image: uploadedImageUrl}).then( response => {
                this.setState ({saving: false})
                if (this.fileRefs)
                    this.fileRefs.forEach (file => window.URL.revokeObjectURL (file.preview))
            })
        }
    }

    /*
        handleImageChange = (imageUrl) => {
            // const user = userStore.currentUser
            const user = this.state.user
            user.image = imageUrl

            this.setState({saving: true})
            userStore.saveUser({id: user.id, image: imageUrl}).then( user => {
                this.setState({user: user, saving: false})
            })
        }
    */

    render() {
        // const user = userStore.currentUser
        const user = this.state.user

        return (
            <div className='unModal'>
                <h1>Edit Profile</h1>
                <X size={36} onClick={() => this.close()} className='close'/>
                <div className='content userSettings'>
                    <div className="imgBox image"
                         style={{backgroundImage: 'url(' +user.image+ ')'}}>
                        <Dropzone onDrop={this.onDrop.bind(this)}
                                  multiple={false}
                                  disabled={false}
                                  onDragEnter={this.onDragEnter}
                                  onDragLeave={this.onDragLeave}
                                  style={{width: '100%', height: '100%', padding: 0}}
                        >
                        </Dropzone>
                    </div>

                    <div className='form'>
                        <Loader active={this.state.saving}>Saving...</Loader>
                        <Form inverted>
                            <Form.Input name='displayName' label='Display Name' onChange={this.handleChange} onKeyPress={this.handleKey} onBlur={this.handleChange}
                                        value={user.displayName} placeholder={user.displayName || 'Your display name (for public)'} />

                            <Form.Input name='firstName' label='First Name' onChange={this.handleChange} onKeyPress={this.handleKey} onBlur={this.handleChange}
                                        value={user.firstName} placeholder={user.firstName || 'Your first name (optional)'} />

                            <Form.Input name='lastName' label='Last Name' onChange={this.handleChange} onKeyPress={this.handleKey} onBlur={this.handleChange}
                                        value={user.lastName} placeholder={user.lastName || 'Your last name (optional)'} />

                            {!user.socialProvider &&
                            <Form.Input name='email' label='Email' type='email' onChange={this.handleChange}
                                        onKeyPress={this.handleKey} onBlur={this.handleChange}
                                        value={user.email} placeholder={user.email || 'Email (optional)'}/>
                            }

                            {!user.socialProvider &&
                            <Form.Input name='password' label='Password' onChange={this.handleChange} type='password'
                                        onKeyPress={this.handleKey} onBlur={this.handleChange}/>
                            }

                            <Form.Input name='phone' label='Phone Contact' type='tel' onChange={this.handleChange} onKeyPress={this.handleKey} onBlur={this.handleChange}
                                        value={user.phone} placeholder={user.phone || 'Phone number (optional)'} />

{/*
                            <Form.Input name='bio' label='Bio' onChange={this.handleChange} onKeyPress={this.handleKey} onBlur={this.handleChange}
                                        value={user.bio} placeholder='Bio (optional)' />
*/}

                        </Form>

                    </div>

                </div>
            </div>
        )
    }
}