import {Component} from 'react'
import {observer} from 'mobx-react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import {findDOMNode} from 'react-dom'
import {Button, Dimmer, Form, Loader} from 'semantic-ui-react'
import Dropzone from 'react-dropzone'

import uiStateStore from '/store/uiStateStore.js'
import cardStore from "/store/cardStore"
import CardModel from "/models/CardModel";
import {extToIcon, isPreviewable} from '/config/FileConfig'
import AttmThumb from "./AttachmentsPane/AttmThumb";
import {getExt} from "../../utils/solo/getExt";
import {isUrl} from "../../utils/solo/isUrl";
import {deleteAttachment, linkAttachment, uploadAttachment} from "../../services/AttachmentService";

const MAX_ATTM = 4

@observer
export default class AttachmentsPane extends Component {
    constructor(props) {
        super(props)

        let fileRefs

        this.state = {
            elem: null,
            uploadCount: 1,
            uploadProgress: 0,
            importing: false,
            files: [],
            url: null,
            desc: null
        }

        this.isFinalUpload = false // out of state, because requires synchronous set
        this.showUploadProgress = this.showUploadProgress.bind(this)
    }

    close() {
        uiStateStore.setEditingCard(null)
        uiStateStore.unModal = null
    }

    focusInput() {
        // no
    }

    onDrop(files) {
        this.fileRefs = files
        let attm, attms = []
        files.forEach(file => {
            attm = CardModel.FromFileUpload (file)
            uiStateStore.fileCardMap[file] = attm
            attms.push(attm)
        })

        this.setState({
            files: attms
        }, () => {
            // !! release object urls only after api upload is complete. See progress below
            // files.forEach( file => window.URL.revokeObjectURL(file.preview))

        });
    }

    onChange(ev) {
        const target = ev.target
        const propName = target.name
        this.setState({[propName]: target.value, urlError: false})
    }

    onKeyPress(ev) {
        console.log('key. value: ' +ev.key+ ", val: " +ev.target.value)
        if (ev.key === 'Enter')
            this.attachUrl(ev)
    }

    attachUrl(ev) {
        const fileUrl = this.state.url
        const desc = this.state.desc
        debugger
        linkAttachment(fileUrl, uiStateStore.editingCard.id, desc).then( response => {
            console.log("Response: "+response)
            this.setState({url: null, desc: null})
            cardStore.getCardById(uiStateStore.editingCard.id).then( card => {
                uiStateStore.setEditingCard(card)
            })

        }).catch( error => {
            const msg = error.response.data.error
            console.error(msg)
            this.setState({urlError: true, urlErrorMsg: msg})
        })
    }

    attachFiles() {
        this.setState ({importing: true})
        if (!this.fileRefs || this.fileRefs.length === 0)
            debugger

        this.fileRefs.forEach( (fileRef, i) => {
            debugger
            const ext = getExt(fileRef.name)
            // TODO we should save preview image as well
            uploadAttachment( fileRef, ext, uiStateStore.editingCard.id, this.showUploadProgress, fileRef.name, i+1 )

        })
    }

    openAttm = (attm) => {
        if (attm && attm.url && window)
            window.open(attm.url)
    }

    deleteAttachment = (attmId) => {
        if (!attmId)
            debugger

        deleteAttachment(uiStateStore.editingCard.id, attmId).then( response => {
            cardStore.getCardById(uiStateStore.editingCard.id).then( card => {
                uiStateStore.setEditingCard(card)
            })
        })
    }

    cancel() {
        this.setState({files: [], importing: false})
    }

    // Upload progresses will arrive asynchronously
    showUploadProgress(count, msg) {
        if (msg === 'end') {
            this.setState ({uploadCount: Math.max (count, this.state.uploadCount)})
            if (count === this.state.files.length) {
                this.setState({importing: false, files: []})
                cardStore.getCardById(uiStateStore.editingCard.id).then( card => {
                    uiStateStore.setEditingCard(card)
                })

                // RELEASE RESOURCES
                if (this.fileRefs)
                    this.fileRefs.forEach (file => window.URL.revokeObjectURL (file.preview))

            }
        }
        else if (msg === 'start') {
            this.setState({importing: true})
        } else {
            console.log("Progress: count = " +count+ ", msg: " +msg)
        }
    }

    componentDidMount() {
        this.setState({elem: findDOMNode(this)})
        this.focusInput()

    }

    render() {
        const {cardId, editMode, viewerMode} = this.props
        const selectedFileCount = this.state.files.length
        const card = editMode? uiStateStore.editingCard : cardStore.getCardFromCurrent(cardId)
        if (!card)
            return null

        const formatValid = isUrl(this.state.url)
        const urlClass = (formatValid && !this.state.urlError)? 'valid' : (this.state.urlError)? 'invalid' : ''
        const attmCount = card.attachments? card.attachments.length : 0
        const blankCount = Math.max(MAX_ATTM - attmCount, 0)
        let blanks = []
        for (let i=0 ; i<blankCount; i++) // for jsx iteration
            blanks.push('blank')

        const attmsLabel = editMode? 'Currently attached' : 'Attachments'
        const padding = editMode? '1rem' : '.5rem 0 0 0'

        let ext, iconName, preview

        return (
            <div className='unModal' style={{backgroundColor: 'unset', border: 'none', padding: padding, marginTop: 0}}>
                <div className='umContent' style={{marginTop: 0}}>
                    <Dimmer.Dimmable dimmed={this.state.importing}>
                        <Dimmer active={this.state.importing} inverted>
                            <Loader inverted style={{marginTop: '0', height: '10%'}}>Uploading Attachments
                                ({this.state.uploadCount} of {this.state.files.length})...</Loader>
                        </Dimmer>

                        <div className='existing'>

                            {editMode &&
                            <div className='secondary small' style={{margin: '.5rem 0'}}>{attmsLabel}</div>
                            }

                            <div style={{display: 'flex', alignContent: 'bottom'}}>
                            <ReactCSSTransitionGroup
                                transitionName="cardfx"
                                transitionEnterTimeout={300}
                                transitionLeaveTimeout={200}>

                                {card.attachments && card.attachments.map ((attm, i) => {
                                    attm.title = attm.url
                                    ext = getExt (attm.url)
                                    iconName = extToIcon (ext)
                                    preview = isPreviewable (ext)

                                    return (
                                        <AttmThumb file={attm} ext={ext} iconName={iconName} preview={preview}
                                                   size={48} className='mr05 jSec'
                                                   enableRemoval={true}
                                                   openAttm={()=> this.openAttm(attm)}
                                                   deleteAttm={(id) => this.deleteAttachment(id)}/>
                                    )
                                })}
                            </ReactCSSTransitionGroup>
                            {editMode && blanks.map( (blank, i) => {
                                return <div className='blank' data-tip="Enter a URL or Upload files below (4 Max)">{i + 1 + attmCount}</div>
                            })}
                            </div>
                        </div>

                      {editMode &&
                      <div className="cardEditor attms" style={{padding: 0, border: 'none'}}>

                        {attmCount < MAX_ATTM &&
                        <div className='secondary small' style={{margin: '.5rem 0'}}>Attach document URL</div>
                        }

                        {this.state.urlError && attmCount < MAX_ATTM &&
                        <div className='error'>Please check the attachment URL.</div>
                        }

                          {attmCount < MAX_ATTM &&
                          <input
                              id='aUrl' type='url' name='url' style={{fontSize: '1rem', width: '100%'}}
                              disabled={attmCount === MAX_ATTM}
                              placeholder={attmCount < MAX_ATTM ? 'Attachment URL...' : 'Max number of files attached.'}
                              onChange={(ev) => this.onChange (ev)}
                              onKeyPress={(ev) => this.onKeyPress (ev)}
                              className={urlClass}
                          />
                          }

                        {this.state.url && this.state.url.length > 0 &&
                        <Form.Input name='desc' style={{fontSize: '1rem', width: '100%', marginTop: '1rem'}}
                                    placeholder={'Description (Optional)...'}
                                    onChange={(ev) => this.onChange (ev)}
                                    onKeyPress={(ev) => this.onKeyPress (ev)}
                        />
                        }

                          {attmCount < MAX_ATTM &&
                          <div className='secondary small' style={{margin: '.5rem 0'}}>
                              Attach from your Desktop or Phone
                          </div>
                          }

                        {this.state.files.length > 0 &&
                        <span>
                                <Button className='button neutral'
                                        style={{margin: '-5px 0 8px 1rem'}}
                                        onClick={() => this.cancel ()}>
                                    Cancel
                                </Button>
                                <Button className='button go'
                                        style={{margin: '-5px 0 8px 1rem'}}
                                        onClick={() => this.attachFiles ()}>
                                    Attach
                                </Button>
                            </span>
                        }
                        {this.state.files.length === 0 && attmCount < MAX_ATTM &&
                        <div className="formPane">
                          <Dropzone onDrop={this.onDrop.bind (this)} className="upload-box">
                            <div className="browse-cards">
                              <h1 className="thinH" style={{color: '#555'}}>Drop files here</h1>
                              <div className="thinH2">Or click to choose files...</div>
                            </div>
                          </Dropzone>
                        </div>
                        }

                        {this.state.files.length === 0 && attmCount === MAX_ATTM &&
                        <div className="mt05">
                          You've attached the maximum number of items to this card.
                        </div>
                        }


                        {this.state.files.length > 0 &&
                        <div className="pad1 card-import-boxHIDE">
                          <div className="browse-files">
                            {this.state.files.map ((file, i) => {

                              ext = getExt (file.title)
                              iconName = extToIcon (ext)
                              preview = isPreviewable (ext)

                              return (
                                <AttmThumb file={file} ext={ext} iconName={iconName} preview={preview}
                                           size={36}
                                           readOnly={true}/>
                              )
                            })}
                          </div>
                        </div>
                        }
                      </div>
                      }
                    </Dimmer.Dimmable>

                </div>
            </div>
        )
    }
}

