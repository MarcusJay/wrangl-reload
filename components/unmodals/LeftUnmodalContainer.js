import {Component} from 'react'
import {observer} from 'mobx-react'
import uiStateStore, {ManageContacts} from '/store/uiStateStore'
import PeopleUnmodal from "../modals/PeopleUnmodal";

@observer
export default class LeftUnmodalContainer extends Component {

    render() {
        if (!uiStateStore.leftUnModal)
            debugger

        const panelName = uiStateStore.leftUnModal
        let panel
        switch (panelName) {
            case ManageContacts:
                panel = <PeopleUnmodal />
                break
            default:
                debugger
                panel = <span className='leftUnModal'>{panelName}</span>
        }
        return (
            <div >
                {panel}
            </div>
        )

    }
}