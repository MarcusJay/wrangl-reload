import {Component} from 'react'
import {Icon} from 'semantic-ui-react'
import {X, XCircle} from 'react-feather'

import {attmBaseUrl} from '/config/ApiEndpoints'
import {truncate} from "../../../utils/solo/truncate";
import React from "react";

export default class AttmThumb extends Component {

    openAttm = (ev) => {
        const opener = this.props.openAttm || this.props.viewAttachment
        if (opener)
            opener(this.props.file)
    }

    deleteAttm = (ev) => {
        ev.stopPropagation()
        const {file, deleteAttm} = this.props
        if (file && deleteAttm)
            deleteAttm(file.attachment_id)
    }

    render() {
        const { file, ext, size, enableRemoval, className, color, padIcon, circle, preview, iconName, source, sourceIcon, iTitle, viewAttm } = this.props
        let title = this.props.readOnly? file.title : file.description || file.title.replace(attmBaseUrl, '')
        title = this.props.trunc > 0? truncate(title, this.props.trunc) : title
        const style = this.props.readOnly? {overflow: 'hidden', display: 'block', maxWidth: '90%'} : {display: 'inline-block', overflow: 'hidden'}
        const image = file.image || file.url
        const attmClass = 'hoh relative ' + (className || '')
        const tip = 'Open ' +ext+ ' file'
        const icon = source || iconName

        const iconColor = color || 'white'
        const circleClass = '' //circle? 'roundIcon' :''
        const iconSize = size+'px'
        const fontSize = (size - 14) + 'px'
        const iconMarginTop = enableRemoval || padIcon? '12px':''

        return (
            <div className={attmClass} data-tip={tip} data-place='top' style={style} onClick={this.openAttm}>
                {enableRemoval &&
                <X size={18} className='abs fg round pointer attmAction centered'
                   style={{top: '0',right:'0'}}
                   data-tip='Remove attachment'
                   onClick={this.deleteAttm}
                />
                }

{/*
                {preview && image &&
                <div className='inline coverBG'
                     style={{border: '2px solid white', minWidth: iconSize, minHeight: iconSize, backgroundImage: 'url(' +image+ ')'}}>&nbsp;
                </div>
                }
*/}

                {icon &&
                <div className='centered cardBG'
                     style={{display: 'flex', alignItems: 'center', justifyContent: 'center',
                         width: iconSize, height: iconSize, padding: '1px 0 0 5px'}}>
                    <Icon
                        name={icon}
                        style={{fontSize: fontSize, color: iconColor, marginTop: iconMarginTop}}

                    />
                </div>
                }
                {!icon && sourceIcon &&
                <img src={sourceIcon} className="icon"/>
                }
                {iTitle &&
                <span className='title '>{title}</span>
                }
            </div>
        )
    }
}
