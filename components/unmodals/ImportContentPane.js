import {Component} from 'react'
import {observer} from 'mobx-react'
import {X} from 'react-feather'

import {NOTIF_ALL, NOTIF_NONE} from '/config/Constants'

import uiStateStore from "/store/uiStateStore"
import projectStore from '/store/projectStore'
import ReactionApiService from "../../services/ReactionApiService";
import {STARS, THUMBS} from "../../config/ReactionConfig";
import UploaderComponent from "../common/UploaderComponent";
import CombinedSearchTab from "../modals/ImportTabs/CombinedSearchTab";
import CardsTab from "../modals/ImportTabs/CardsTab";

@observer
export default class ImportContentPane extends Component {

    state = {saving: false, loading: false, query: ''}

    close() {
        projectStore.importProject = null
        uiStateStore.unModal = null
    }

/*
    handleKey = (event) => {
        if (event.key === 'Enter')
            this.handleChange(event)
    }

*/
    handleChange = (event, type) => {
        const project = projectStore.importProject

        const targetElem = event.target
        let propValue = targetElem.value
        const propName = targetElem.name
        if (!/[a-zA-Z]+/.test(propValue))
            propValue = Number(propValue)

        project[propName] = propValue
        this.submitChange(event, propName, propValue)
    }

    submitChange = (event, propName, propValue) => {
        const project = projectStore.importProject
        this.setState({saving: true})
        projectStore.updateProject({id: project.id, [propName]: propValue}).then( project => {
            // project[propName] = propValue
            const test = projectStore.importProject
            this.setState({saving: false})
            // Correct disparity between what we must send to nullify a reactionSet ('DELETE') and what we receive on reads (null)
            if (project && propName === 'reactionSetId' && propValue === 'DELETE')
                project.reactionSetId = null

        })
    }

    submitNotifLevel = (event) => {
        const targetElem = event.target
        let propValue = targetElem.checked? NOTIF_ALL : NOTIF_NONE

        this.setState({saving: true})
        projectStore.setUserProjectSettings(null, projectStore.importProject.id, propValue).then( response => {
            this.setState({saving: false})
        })
    }

    componentDidMount() {
        projectStore.getUserProjectSettings(null, projectStore.importProject.id).then( project => {
            this.forceUpdate() // Should not be necessary
        })
    }

    componentWillUpdate() {
        projectStore.getUserProjectSettings(null, projectStore.importProject.id).then( project => {
            // this.forceUpdate()
        })
    }

    componentWillReact() {
        const test = projectStore.importProject
        console.log('project settings will REACT')
    }

    render() {
        const {query} = this.state
        const project = projectStore.importProject
        console.log('settings for project: ' +project)
        const secLev = project.securityLevel
        const notifLevel = project.notifLevel
        const stars = ReactionApiService.getReactionSetByTitleFromDefaults(STARS)
        const thumbs = ReactionApiService.getReactionSetByTitleFromDefaults(THUMBS)



        const test = projectStore.importProject.id

        return (
            <div className='unModal'>
                <h1>Add Stuff!</h1>
                {/*<h2>{project.title}</h2>*/}
                <X size={36} onClick={() => this.close()} className='close'/>
                <div className='content importContent'>
                    <div className='leftHalf borderR'>
                        <CombinedSearchTab project={project} importAssets={this.importAssets} query={query}/>
                        <UploaderComponent project={project}  />
                    </div>
                    <div className='rightHalf'>
                        <CardsTab project={project} importCards={this.importCards}/>
                    </div>


                </div>
            </div>
        )
    }
}