import {Component} from 'react'
import {observer} from 'mobx-react'
import {X} from 'react-feather'
import uiStateStore from "/store/uiStateStore"
import userStore from "/store/userStore"
import projectStore from '/store/projectStore'

@observer
export default class ExportPane extends Component {
    constructor(props) {
        super (props)
        this.state = {saving: false}
    }

    close() {
        uiStateStore.unModal = null
    }

    handleKey = (event) => {
        if (event.key === 'Enter')
            this.handleChange(event)
    }

    handleChange = (event) => {
        const user = userStore.currentUser

        const targetElem = event.target
        const propValue = targetElem.type === 'checkbox' ? targetElem.checked : targetElem.value
        const propName = targetElem.name
        user[propName] = propValue

        if (event.key === 'Enter' || event.type === 'blur') {
            debugger
            this.submitChange (event, propName, propValue)
        }
        else
            userStore.updateCurrentUserClientOnly(propName, propValue)


    }

    submitChange = (event, propName, propValue) => {
        const user = userStore.currentUser

        this.setState({saving: true})
        userStore.saveUser({id: user.id, [propName]: propValue}).then( user => {
            this.setState({saving: false})
        })
    }

    handleImageChange = (imageUrl) => {
        const user = userStore.currentUser
        user.image = imageUrl

        this.setState({saving: true})
        userStore.saveUser({id: user.id, image: imageUrl}).then( user => {
            this.setState({saving: false})
        })
    }

    render() {
        const user = userStore.currentUser
        const project = projectStore.currentProject

        return (
            <div className='unModal'>
                <h1>Export as PDF</h1>
                <X size={36} onClick={() => this.close()} className='close'/>
                <div className='content exportPDF'>
                    <div>
                        {/*<h2>Export {project.title} as PDF: </h2>*/}
                        <iframe src={uiStateStore.exportPreviewUrl} style={{width: '100%', height: '600px'}}/>
\                    </div>

                </div>
            </div>
        )
    }
}