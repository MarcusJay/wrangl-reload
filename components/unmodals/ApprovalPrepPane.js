import {Component} from 'react'
import {observer} from 'mobx-react'
import {Divider, Dropdown, Form, Input, Loader, TextArea} from 'semantic-ui-react'
import {ChevronDown, Mail, Star, ThumbsUp, UserPlus, X} from 'react-feather'
import {DropTarget} from 'react-dnd'
import {DndItemTypes} from '/config/Constants'


import userStore from "../../store/userStore";
import cardStore from "../../store/cardStore";
import projectStore from "../../store/projectStore";
import uiStateStore from "../../store/uiStateStore";
import {findDOMNode} from "react-dom";
import MemberAvatar from "../common/MemberAvatar";
import AtCard from "../Conversations/AtCard";
import UserPopup from "../Conversations/UserPopup";
import {
    addCardsToApproval, broadcastApproval, createApprovalProject,
    saveApprovalProject
} from "../../services/ApprovalService";
import ReactionApiService from "../../services/ReactionApiService";
import {ApprovalCompletionType, APPROVE, RATE, UserRole} from "../../config/Constants";
import {STARS, THUMBS} from "../../config/ReactionConfig";
import {EventSource} from "../../config/EventConfig";
import {abbrevOrTruncate} from "../../utils/solo/abbrevOrTruncate";
import {randomStr} from "../../utils/solo/randomStr";
import EmailService from "../../services/EmailService";
import Toaster from "../../services/Toaster";
import ProjectApiService from "../../services/ProjectApiService";

const modWarning = "Modifying an open request may confuse those who have already taken action on it."
const userWarning = "Please specify at least 1 person who should receive this approval request."
const cardWarning = "Please specify at least 1 item to be approved."

const paneTarget = {
    drop(props, targetMonitor, component) {
        let dragItem = targetMonitor.getItem ()

        // add card to approval
        if (dragItem.type === DndItemTypes.CARD) {
            component.addCard(dragItem.id)
        }

        // add user to approval
        if (dragItem.type === DndItemTypes.MEMBER_AVATAR) {
            component.addUser(dragItem.id)
        }
    }
}

@DropTarget([DndItemTypes.CARD, DndItemTypes.MEMBER_AVATAR], paneTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    item: monitor.getItem()
}))
@observer
export default class ApprovalPrepPane extends Component {

    apRequestId = null

    state = {
        saving: false, uploading: false, error: null, linkCopied: false,
        displayUser: null, showInstructions: true,
        userPopupBottom: 0,
        isDirty: false,
        attemptedSend: false,
        selectedUser: null, selectedUserIndex: null,
        title: null, description: null,
        showConfirmation: false, sentAPId: null,
        reviewType: THUMBS,
        showWarning: false,
        pendingNewUsers: [],
        completionType: ApprovalCompletionType.SINGLE_USER_COMPLETES,
    }

    handleClose = (ev) => {
        // If we started a request but never sent it, delete it on close, and erase Clipboard to prevent sharing dead links.
        if (uiStateStore.currentApprovalRequest !== null && uiStateStore.isAPStarterRequest && !uiStateStore.approvalSent) {
            projectStore.deleteProject(uiStateStore.currentApprovalRequest)
            if (this.state.linkCopied) {
                const linkNode = document.querySelector('#hdnLink')
                linkNode.value = 'Approval Request was Cancelled'
                linkNode.select()
                document.execCommand('copy')
            }
        }

        this.resetAP()
        uiStateStore.currentApprovalRequest = null
        uiStateStore.rightUnModal = null
        uiStateStore.showUserPopup = false
    }

    handleSend = (ev) => {
        cardStore.selectedCardIds = []
        this.handleClose(ev)
    }

    handleClick = (ev) => {
    }


    handleKeyPress = (ev) => {
    }

    handleTextChange = (ev) => {
        const propName = ev.target.name
        const propValue = ev.target.value
        this.setState({[propName]: propValue, isDirty: true})
    }

    // used for updating existing ap if any. For new requests, see userHitSave
    handleSave = (ev) => {
        if (uiStateStore.currentApprovalRequest !== null && !uiStateStore.isAPStarterRequest) {
            const propName = ev.target.name
            const propValue = ev.target.value
            const ap = uiStateStore.currentApprovalRequest
            // const field = propName === 'title'? 'project_title': propName === 'description'? 'project_description': propName // dumb translation why bother
            projectStore.updateProject ({id: ap.id, [propName]: propValue}).then( apiProject => {
                const apInArray = projectStore.approvalsSent.find( p => p.id === ap.id)
                const apIndex = projectStore.approvalsSent.findIndex( p => p.id === ap.id)

                if (apInArray) { // trigger mobx update to approvals library
                    projectStore.approvalsSent[apIndex] = 'wakeUpMobx'// setting it below, without first "erasing" it, will not fire mobx update.
                    projectStore.approvalsSent[apIndex] = apInArray
                }
            })
        }
    }

    //dnd
    addUser = (userId) => {
        // const user = userStore.getUserFromConnections(userId)
        // TODO check for uniqueness
        userStore.selectedUserIds.push(userId)
        this.setState({showInstructions: false, isDirty: true})
        this.hideUserPopup()

        // update existing ap if any immediately
        if (!uiStateStore.isAPStarterRequest && uiStateStore.currentApprovalRequest !== null) {
            const ap = uiStateStore.currentApprovalRequest
            projectStore.addUserToProject (ap.id, userStore.currentUser.id, userId, EventSource.Approval, UserRole.APPROVER)
        }
    }

    addCard = (cardId) => {
        cardStore.selectCard (cardId)
        if (!uiStateStore.isAPStarterRequest)
            this.setState ({showWarning: true})
        this.setState({isDirty: true})

        // update existing ap if any immediately
        if (!uiStateStore.isAPStarterRequest && uiStateStore.currentApprovalRequest !== null) {
            addCardsToApproval ([cardId], uiStateStore.currentApprovalRequest.id).then(apCardIds => {
                uiStateStore.currentApprovalRequest.cardIds = apCardIds
            })
        }
    }
    // end dnd

    showUserPopup = (ev) => {
        uiStateStore.showUserPopup = true // this.setState({showUserPopup: true})
    }

    hideUserPopup= (ev) => {
        uiStateStore.showUserPopup = false
        userStore.filteredConnections = userStore.currentConnections
    }

    // user popup callbacks
    selUser = (u,i) => {
        this.addUser(u.id)
    }

    overUser = (u,i) => {
        this.setState({displayUser: u, showInstructions: false})
        // this.props.overUser(u)
    }

    clearUser = (u,i) => {
        this.setState({displayUser: null})
        // this.props.clearUser(u)
    }

    addNewUser = (user) => {
        if (!user || !user.email)
            return

        const newUsers = this.state.pendingNewUsers
        newUsers.push(user)
        this.setState({pendingNewUsers: newUsers, isDirty: true})
    }

    // form pane
    removeUser = (userId,i) => {
        const index = userStore.selectedUserIds.indexOf(userId)
        if (index >= 0) {
            userStore.selectedUserIds.splice (index, 1)
            this.setState({isDirty: true})
        }

        // update existing ap if any immediately
        if (uiStateStore.currentApprovalRequest !== null && !uiStateStore.isAPStarterRequest) {
            const ap = uiStateStore.currentApprovalRequest
            projectStore.removeUserFromProject(ap.id, userStore.currentUser.id, userId, true)
        }
    }

    removeCard = (cardId) => {
        cardStore.deselectCard(cardId)
        this.setState({isDirty: true})

        // update existing ap if any immediately
        if (uiStateStore.currentApprovalRequest !== null && !uiStateStore.isAPStarterRequest) {
            const ap = uiStateStore.currentApprovalRequest
            cardStore.deleteCard(cardId, ap.id).then( response => {
                ap.cardIds = ap.cardIds? ap.cardIds.filter( apCardId => apCardId !== cardId) : []
            })
        }

    }

    setReviewType = (type) => {
        this.setState({reviewType: type, isDirty: true})
        const isNew = uiStateStore.isAPStarterRequest
        // this.setState ({showWarning: true})
        const ap = uiStateStore.currentApprovalRequest
        let rsId = ReactionApiService.reviewTypeToRSId(type)
        if (rsId)
            projectStore.updateProject ({project_id: ap.id, reaction_set: rsId})

    }

    setCompletionType = (type) => {
        this.setState({completionType: type, isDirty: true})
    }

    cancelReview() {

    }

    // We have a starter request AP. Time to persist users and cards
    userHitSend = (ev) => {
        // const {title, description, reviewType, completionType} = this.state
        // const cards = cardStore.selectedCards || []
        const AP = uiStateStore.currentApprovalRequest
        const approvers = userStore.selectedUserIds || []
        const cards = cardStore.selectedCardIds || []

        // TODO If we decide to keep starter AP project, then we can re-bind form fields to project itself, rather than component state.
        const {title, description, reviewType} = this.state
        const rsId = ReactionApiService.reviewTypeToRSId(reviewType)

        if (!AP.id || approvers.length === 0 || cards.length === 0) {
            console.error('Empty approval request attempted.')
            this.setState({attemptedSend: true})
            return
        }

        projectStore.updateProject ({id: AP.id, title: title, description: description, reactionSetId: rsId}).then( apiProject => {
            addCardsToApproval(cards, AP.id).then( apCards => {
                ProjectApiService.addUsersToProject(AP.id, userStore.currentUser.id, approvers, EventSource.ProjectAdd, UserRole.APPROVER)
                    .then( response => {
                        this.emailAllApprovers(AP, approvers)

                        // Start Conversation
                        projectStore.addToConvo(AP.description, AP.id).then( convoResponse => {
                        })

                        this.setState({
                            showConfirmation: true,
                            sentAPId: AP.id,
                            saving: false, uploading: false, error: null,
                            showWarning: false,
                            attemptedSend: false
                        })
                        uiStateStore.approvalSent = true
                        uiStateStore.isAPStarterRequest = false
                        cardStore.selectedCardIds = []

                        // Final step: trigger an event that will go to all project members.
                        // Should no longer be required since we delay adding users and cards to AP until final send.
                        // broadcastApproval(AP)

                    })
            })

        })
    }

    createPendingUsers() {
        const {pendingNewUsers} = this.state
        if (pendingNewUsers && pendingNewUsers.length > 0) {
            pendingNewUsers.forEach( user => {
                user.password = randomStr(4)
                userStore.createPendingUser(user.email, user.name, user.password)
            })
        }

    }

    emailAllApprovers(AP, approverIds) {
        const newUsers = this.state.pendingNewUsers
        const requestor = userStore.currentUser
        const reviewVerb = this.state.reviewType === THUMBS? 'approve' : 'rate'
        let apUrl, user
        newUsers && newUsers.forEach (user => {
            apUrl = 'https://pogo.io/approve/' + AP.id
            EmailService.sendInvite({
                recipientEmail: user.email,
                recipientName: user.name,
                recipientPW: user.password,
                senderName: (requestor.displayName || requestor.firstName),
                senderId: requestor.id,
                isAP: true,
                projectId: AP.id,
                imageUrl: AP.image,
                itemCount: AP.cardTotal,
                apType: reviewVerb,
                apDesc: AP.description})
        })
        approverIds && approverIds.forEach (userId => {
            user = userStore.getUserFromConnections(userId)
            apUrl = 'https://pogo.io/approve/' + AP.id
            if (user) {
                EmailService.sendInvite({
                    recipientEmail: user.email,
                    recipientName: user.displayName || user.firstName,
                    recipientPW: null,
                    senderName: (requestor.displayName || requestor.firstName),
                    senderId: requestor.id,
                    isAP: true,
                    projectId: AP.id,
                    imageUrl: AP.image,
                    itemCount: AP.cardTotal,
                    apType: reviewVerb,
                    apDesc: AP.description})

            }


        })
    }

    gotoAP = () => {
        const existingAPid = uiStateStore.currentApprovalRequest? uiStateStore.currentApprovalRequest.id : null
        const apId = existingAPid || this.state.sentAPId
        if (!window || !apId) {
            console.error("Can't open approval.")
            return
        }

        const apUrl = 'https://pogo.io/approve/'+apId
        const apWindow = window.open(apUrl, '_blank')
    }

    resetAP = () => {
        this.setState({
            saving: false, uploading: false, error: null,
            displayUser: null, showInstructions: true,
            userPopupBottom: 0,
            selectedUser: null, selectedUserIndex: null,
            title: null, description: null,
            showConfirmation: false, sentAPId: null,
            reviewType: THUMBS,
            showWarning: false,
            pendingNewUsers: []
        })
        uiStateStore.approvalSent = false
        uiStateStore.isAPStarterRequest = false
    }

    setUserPopupPosition() {
        const elem = findDOMNode(this)
        if (!elem)
            return
        const userPane = elem.querySelector('#uasel1')
        const userbox = userPane.getBoundingClientRect()
        const btm = 'calc('+userbox.top+ 'px - 3rem)'
        this.setState({userPopupBottom: 0 }) // TODO TEMP. Get correct position
    }

    copyAPLink = () => {
        const url = "https://pogo.io/approve/" + uiStateStore.currentApprovalRequest.id
        const linkNode = document.querySelector('#hdnLink')
        linkNode.value = url
        linkNode.select()
        document.execCommand('copy')
        Toaster.info('Link to Approval copied: ' +url)
        this.setState({linkCopied: true})
    }

    createStarterApproval() {
        createApprovalProject().then(starterRequest => {
            this.apRequestId = starterRequest.id
            uiStateStore.currentApprovalRequest = starterRequest
            uiStateStore.isAPStarterRequest = true
        })
    }

    getExistingApproval() {
        const ap = uiStateStore.currentApprovalRequest
        if (!ap)
            return

        // TODO remove this once userProjects includes cards in approval projects. March 25 2019: no cards. Keep.
        projectStore.getProjectById(ap.id).then( project => {
            this.apRequestId = ap.id
            // TODO keep all of this, just move out of closure, once userProjects includes cards in approval projects.
            const rs = ReactionApiService.getReactionSetByIdFromDefaults (ap.reactionSetId)
                || ReactionApiService.getReactionSetByTitleFromDefaults(THUMBS)
            const reviewType = ReactionApiService.rsIdToReviewType(rs.reaction_set_id)
            this.setState ({title: ap.title, description: ap.description, reviewType: reviewType})
            cardStore.selectedCards = ap.cards = project.cards
            ap.cardTotal = project.cardTotal
            userStore.selectedUserIds = ap.members ? ap.members.map (user => user.id) : []
        })
    }

    // To allow immediate link copying and auto-saving, and since we already built auto-save for existing requests, let's generate
    componentDidMount() {
        this.setUserPopupPosition()

        // Setup Starter Project
        if (!uiStateStore.currentApprovalRequest) {
            this.createStarterApproval()
        }

        else {
            this.getExistingApproval()
        }

        // this.thumbsRS = ReactionApiService.getReactionSetByTitleFromDefaults(THUMBS)
        // this.starsRS = ReactionApiService.getReactionSetByTitleFromDefaults(STARS)

    }

    componentDidUpdate() {
        console.warn('ap prep pane did update')
        if (this.apRequestId && this.apRequestId !== uiStateStore.currentApprovalRequest.id) {
            this.getExistingApproval()
        }
    }

    render() {
        const {saving, uploading, error, selectedUser, selectedUserIndex, userPopupBottom, pendingNewUsers, completionType,
               isDirty, attemptedSend, reviewType, title, description, showConfirmation, sentAPId, showWarning} = this.state
        const {isOver, item, connectDropTarget} = this.props
        const activeUser = userStore.currentUser
        const isNew = uiStateStore.isAPStarterRequest && !uiStateStore.approvalSent // it is a starter which hasn't been sent yet
        const cards = cardStore.selectedCards || []
        const userIds = userStore.selectedUserIds || []
        const approversOnly = userIds.filter(user => user.id !== activeUser.id) || []
        const usersInPopup = userStore.currentConnections /*projectStore.currentProject? projectStore.currentProject.members : */
        const project = uiStateStore.currentApprovalRequest || projectStore.currentProject
        const forMobxOnly = uiStateStore.rand
        const isUserDrop = isOver && item.type === DndItemTypes.MEMBER_AVATAR
        const isCardDrop = isOver && item.type === DndItemTypes.CARD
        const dropItem = isUserDrop? userStore.getUserFromConnections(item.id) : isCardDrop? cardStore.getCardFromCurrent(item.id) : null
        const showInstructions = userIds.length === 0 && (!pendingNewUsers || pendingNewUsers.length === 0)
        const saveLabel = approversOnly.length > 0 && cards.length > 0? 'Send!' : 'Save'
        const enableSave = project && approversOnly.length > 0 && cards.length > 0
        let member

        if (showConfirmation && sentAPId) {
            return (
                <div className='approvalPrepPane'>
                    <h1 className='fg'>Your Approval Request has been sent!</h1>
{/*
                    <div className='secondary large'>To track its progress,&nbsp;
                        <a href={'https://wrangle.me/approve/'+sentAPId} target='_blank'>click here.</a>
                    </div>
                    <div style={{marginTop: '3rem'}} className='centered'>
                        <button className='button go' onClick={this.gotoAP}>View Approval</button>
                    </div>
*/}
                    <Divider/>
                    <div className='mt1 fg'>
                        <h3>Next Steps:</h3>
                        <div className='mt1 ml05' style={{marginBottom: '3rem'}}>
                            <button className='jBtn' onClick={this.copyAPLink}>COPY APPROVAL LINK</button>
                            <input id='hdnLink' type='text' className='abs' style={{opacity: 0}}/>
                            <p className='tiny jSec mt05 italic jFG' style={{width: '90%'}}>
                                Copy a link to this approval that you can paste into emails and other apps.</p>
                        </div>

                        <div className='' style={{marginBottom: '3rem'}}>
                            <button className='button go' >
                                <a href={'https://pogo.io/approve/'+sentAPId} className='jFG' target='_blank'>OPEN THIS APPROVAL NOW</a>
                            </button>
                            <p className='ml05 jSec mt05 tiny italic' style={{width: '90%'}}>Open this approval and see what your approvers will see.</p>
                        </div>

                        <div className='ml05' style={{marginBottom: '3rem'}}>
                            <button className='jBtn' onClick={this.handleClose}>
                                Close
                            </button>
                            <p className='ml05 tiny jSec mt05 italic' style={{width: '90%'}}>Return to the original board.</p>
                        </div>
                    </div>
                </div>
            )
        }

        return connectDropTarget(
            <div className='approvalPrepPane'
                 onKeyPress={this.handleKeyPress}
                 onClick={this.handleClick}
            >
                <Loader active={saving || uploading}></Loader>
                <X size={32} onClick={this.handleClose} className='right hoh close pointer secondary' style={{margin: '0 0.5rem 0 0'}}/>

                <h1 className='fg' style={{marginTop: '0'}}>Request Approval</h1>

{/*
                <div className='msgs'>
                    {!cards || cards.length === 0 &&
                    <h2>Nothing to review. Please select items on the board you wish to have reviewed first. </h2>
                    }
                </div>
                {showWarning &&
                <div className='red'>{modWarning}</div>
                }
*/}

                {!sentAPId}
                <div className='form'>

                    <div className='frow'>
                        <label className='inline'>
                            Type:
                        </label>
                        <Dropdown
                            icon={<span className='secondary button inline' style={{padding:'0.75rem', margin: '.5rem', minWidth: '8.5rem'}}>
                                {reviewType === THUMBS &&
                                <span>
                                    <ThumbsUp size={20} style={{marginRight: '.5rem', marginBottom: '-4px'}}/>Approval
                                </span>
                                }

                                {reviewType === STARS &&
                                <span>
                                    <Star size={20} className={'secondary'} style={{marginBottom: '-2px'}}/>
                                    <Star size={20} className={'secondary'} style={{marginBottom: '-2px'}}/>
                                    <Star size={20} className={'secondary'} style={{marginBottom: '-2px', marginRight: '.5rem'}}/>
                                    Rating
                                </span>
                                }

                                <ChevronDown size={16} style={{marginLeft: '.5rem', marginBottom: '-4px'}}/>
                            </span>
                            }
                            inline={true}
                            // upward={true}
                            // pointing='top left'
                        >
                            <Dropdown.Menu vertical className='hioh fg' style={{width: '140%'}}>
                                <Dropdown.Item as='div' name='approval' onClick={() => this.setReviewType(THUMBS)} >
                                    <ThumbsUp size={18} className='inline secondary' style={{minWidth: '30%'}}/>
                                    <span className='inline secondary' >Approval</span>
                                </Dropdown.Item>
                                <Dropdown.Item as='div' name='rating' onClick={() => this.setReviewType(STARS)} >
                                <span className='inline' style={{minWidth: '30%'}}>
                                    <Star size={14} className={'secondary'}/>
                                    <Star size={14} className={'secondary'}/>
                                    <Star size={14} className={'secondary'}/>
                                </span>
                                    <span className='inline secondary' >Rating</span>
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>

                    <div className='frow'>
                        <label className='block'>
                            Subject
                        </label>
                        <input
                            name='title'
                            type='text'
                            className='formPane'
                            style={{width: '100%', }}
                            placeholder="Your feedback is requested"
                            value={title}
                            onChange={this.handleTextChange}
                            onBlur={this.handleSave}
                        />
                    </div>

                    <div className='frow'>
                        <label className='block'>
                            Message
                        </label>
                        <TextArea
                            name='description'
                            className='formPane'
                            placeholder="Hi team, please let us know what you think!..."
                            rows={4}
                            value={description}
                            onChange={this.handleTextChange}
                            onBlur={this.handleSave}
                        />

                    </div>

                    <div className='frow relative'>
                        <label className='inline mr1'>
                            Approvers
                        </label>
                        <button className='ml1 mb05 jBtn'
                                style={{padding: '.3rem .5rem'}}
                                onClick={this.copyAPLink}
                                data-tip="Copy a link to this approval that you can paste into emails and other apps"
                        >
                            Copy Link
                        </button>
                        <input id='hdnLink' type='text' className='abs' style={{opacity: 0}}/>

                        {uiStateStore.showUserPopup &&
                        <UserPopup allUsers={usersInPopup}
                                   title={title}
                                   activeUser={userStore.currentUser}
                                   hideActiveUser={true}
                                   selectedUser={selectedUser}
                                   selectedIndex={selectedUserIndex}
                                   bottom='4rem' // ignore userPopupBottom
                                   selUser={this.selUser}
                                   overUser={this.overUser}
                                   clearUser={this.clearUser}
                                   addNewUser={this.addNewUser}
                                   close={this.hideUserPopup}
                        />
                        }

                        <div id='uasel1' className={'formPane vaTop' +(isUserDrop? ' active':'')} style={{padding: '1rem', height: '3.8rem'}}>
                            {!isUserDrop && pendingNewUsers.map( (user,i) => {
                                return (
                                <div className='inline centered' style={{marginRight: '.5rem'}}>
                                    <Mail size={24} style={{marginTop: '-.5rem', marginBottom: '-8px'}} data-tip={user.name}/><br/>
                                    <span className='small secondary lighter'>{abbrevOrTruncate(user.name, 2)}</span>
                                </div>
                                )
                            })}

                            {!isUserDrop && userIds.map( (userId,i) => {
                                member = userStore.getUserFromConnections(userId)
                                return (member && member.id !== activeUser.id)?
                                <MemberAvatar member={member}
                                              activeUser={activeUser}
                                              enableAdmin={false}
                                              project={projectStore.currentProject}
                                              key={'ua'+i}
                                              index={i}
                                              color="black"
                                              showName={false}
                                              isEnabled={true}
                                              showMenu={false}
                                              showAttmMenu={true}
                                              removeAttm={this.removeUser}
                                              style={{verticalAlign: 'top'}}
                                /> : null
                            })}
                            {!isUserDrop &&
                            <UserPlus size={24} className='secondary pointer vaTop'
                                        style={{marginBottom: '-6px'}}
                                        onClick={this.showUserPopup}/>
                            }

                            {showInstructions && !isUserDrop &&
                            <span className='third small italic vaTop' style={{marginLeft: '.5rem'}} onClick={this.showUserPopup}>
                                <i>Click to specify users, or drag them here.</i>
                            </span>
                            }

                            {isUserDrop && dropItem &&
                            <span className='third small italic vaTop'><i>Add {dropItem.displayName}?</i></span>
                            }


                        </div>
                    </div>

                    {false && (pendingNewUsers.length > 1 || userIds.length > 1) &&
                    <div className='frow'>
                        <Dropdown
                            icon={<span className='link inline' style={{margin: '.5rem', minWidth: '8.5rem'}}>
                                Approval Rules <ChevronDown size={16} style={{marginLeft: '.5rem', marginBottom: '-4px'}}/></span>
                            }
                            inline={true}
                        >
                            <Dropdown.Menu vertical className='hioh fg' style={{width: '140%'}}>
                                <Dropdown.Item as='div' name='anyUser' >
                                    <Form.Field
                                        className='tiny lighter'
                                        label=" Any single person can complete this request."
                                        name='completionType'
                                        value={ApprovalCompletionType.SINGLE_USER_COMPLETES}
                                        control='input'
                                        type='radio'
                                        checked={completionType === ApprovalCompletionType.SINGLE_USER_COMPLETES}
                                        onChange={() => this.setCompletionType (ApprovalCompletionType.SINGLE_USER_COMPLETES)}
                                    />
                                </Dropdown.Item>
                                <Dropdown.Item as='div' name='someUsers' >
                                    <Form.Field
                                        className='tiny lighter'
                                        label=" A decisive majority is required."
                                        name='completionType'
                                        value={ApprovalCompletionType.MAJORITY_COMPLETES}
                                        control='input'
                                        type='radio'
                                        checked={completionType === ApprovalCompletionType.MAJORITY_COMPLETES}
                                        onChange={() => this.setCompletionType (ApprovalCompletionType.MAJORITY_COMPLETES)}
                                    />
                                </Dropdown.Item>
                                <Dropdown.Item as='div' name='allUsers' >
                                    <Form.Field
                                        className='tiny lighter'
                                        label=" 100% participation from everyone!"
                                        name='completionType'
                                        value={ApprovalCompletionType.ALL_USERS_REQUIRED}
                                        control='input'
                                        type='radio'
                                        checked={completionType === ApprovalCompletionType.ALL_USERS_REQUIRED}
                                        onChange={() => this.setCompletionType (ApprovalCompletionType.ALL_USERS_REQUIRED)}
                                    />
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                    }

                    <div className='frow' style={{marginTop:'.5rem'}}>
                        <label className='block'>
                            Items to approve
                        </label>
                        <div className={'cards formPane' +(isCardDrop? ' active':'')} style={{padding: '1rem'}}>
                            {!isCardDrop && cards.length === 0 &&
                            <div className='third small italic' style={{lineHeight:'2.5rem'}}>Click cards to select them, or drag them here.</div>
                            }

                            {isCardDrop && dropItem &&
                            <div className='third small italic' style={{lineHeight:'2.5rem'}}>Add "{dropItem.title}"?</div>
                            }

                            {!isCardDrop && cards.map ((card,i) => {
                                return <AtCard
                                        card={card}
                                        key={'ac'+card.domId}
                                        viewCard={null}
                                        showTitle={true}
                                        showAttmMenu={true}
                                        removeAttm={this.removeCard}
                                        rating={null}
                                />
                            })}
                        </div>
                    </div>

                    {isNew &&
                    <div className='frow' style={{marginTop: '1rem'}}>
                        <button className='jBtn mr05' onClick={this.handleClose}>Cancel</button>
                        <button className={'button' +(enableSave? ' go':' disabled')} onClick={this.userHitSend}>{saveLabel}</button>
                    </div>
                    }

                    {attemptedSend && approversOnly.length === 0 &&
                    <p className='red small'>
                        {userWarning}
                    </p>
                    }

                    {attemptedSend && cards.length === 0 &&
                    <p className='red small'>
                        {cardWarning}
                    </p>
                    }



                    {!isNew &&
                    <div className='frow fg lighter formPane' style={{marginTop: '1rem'}}>
                        This request is open for approval.<br/>
                        Changes you make will take effect immediately. All approvers will be notified.
                        In the distance, sirens.
                    </div>
                    }
                </div>

            </div>
        )
    }
}

