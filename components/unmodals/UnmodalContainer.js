import {Component} from 'react'
import {observer} from 'mobx-react'
import uiStateStore, {
    CardAttachments,
    CardEditor,
    CardViewer,
    EditProfile,
    ExportPreview,
    ImportContent,
    ManageContacts,
    ProjectSettings,
    UserSettings
} from '/store/uiStateStore'
import UserSettingsPane from "./UserSettingsPane";
import ProjectSettingsPane from "./ProjectSettingsPane";
import CardEditorPane from "./CardEditorPane";
import AttachmentsPane from "./AttachmentsPane";
import ExportPane from "./ExportPane";
import PeopleUnmodal from "../modals/PeopleUnmodal";
import CardViewerPane from "./CardViewerPane";
import ImportContentPane from "./ImportContentPane";

@observer
export default class UnmodalContainer extends Component {
    constructor(props) {
        super(props)
    }

/*
    onKeyDown(ev) {
        debugger
        if (ev.key === 'Esc') {
            uiStateStore.unModal = null
        }
    }

    componentDidMount() {
        const elem = findDOMNode(this)
        elem.addEventListener("keyup", this.onKeyDown);
        elem.addEventListener("keyDown", this.onKeyDown);
    }
*/

    render() {
        if (!uiStateStore.unModal)
            debugger

        const panelName = uiStateStore.unModal
        let panel
        switch (panelName) {
            case UserSettings:
            case EditProfile:
                panel = <UserSettingsPane />
                break
            case ExportPreview:
                panel = <ExportPane/>
                break
            case ProjectSettings:
                panel = <ProjectSettingsPane />
                break
            case ImportContent:
                panel = <ImportContentPane />
                break
            case CardEditor:
                panel = <CardEditorPane />
                break
            case CardViewer:
                panel = <CardViewerPane/>
                break
            case CardAttachments:
                panel = <AttachmentsPane />
                break
            case ManageContacts:
                panel = <PeopleUnmodal />
                break
            default:
                debugger
                panel = <span className='unModal'>{panelName}</span>
        }
        return (
            <div className='relative' style={{height: '100%'}}>
                {panel}
            </div>
        )

    }
}