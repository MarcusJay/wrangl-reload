import {Component} from 'react'
import {observer} from 'mobx-react'
import {Icon, Loader} from 'semantic-ui-react'
import {ArrowUpRight, Camera, ChevronLeft, ChevronRight, Edit, Edit2, Link, MessageSquare, X} from 'react-feather'
import userStore from "../../store/userStore";
import cardStore from "../../store/cardStore";
import projectStore from "../../store/projectStore";
import CardTagStrip from "../common/Card/CardTagStrip";
import uiStateStore from "../../store/uiStateStore";
import {findDOMNode} from "react-dom";
import EmbedContainer from "../common/EmbedContainer";
import ImageViewer from "../modals/ImageViewer";
import CardReactions from "../common/Card/CardReactions";
import {getExt} from "../../utils/solo/getExt";
import {extToIcon, isPreviewable} from "../../config/FileConfig";
import AttmThumb from "./AttachmentsPane/AttmThumb";
import {getShortUrl} from '../../utils/solo/getShortUrl'
import CardCarousel from "../Approvals/CardCarousel";
import eventStore from "../../store/eventStore";
import {DEFAULT_CAT, ProjectType, UserStatus} from "../../config/Constants";
import categoryStore from "../../store/categoryStore";
import DumbBadge from "../common/DumbBadge";
import ImagePainter from "../common/Card/ImagePainter";
import PaintToolbar from "../ProjectDetail/WorkPane/PaintToolbar";
import {deleteAnnotation, saveAnnotation, sendImage} from "../../services/ImageFileService";
import PDWTagToolbar from "../ProjectDetail/WorkPane/PDWTagToolbar";
import RU from "../../utils/ResponsiveUtils";
import CardCommentInput from "../Conversations/CardCommentInput";
import ImportModal from "../modals/ImportModal";
import ApprovalReactions from "../Approvals/ApprovalReactions";
import {getCardReactions} from "../../services/CardApiService";
import {getUserFromMembers} from "../../models/ProjectModel";
import {STARS, THUMBS, THUMBS_DOWN, THUMBS_UP} from "../../config/ReactionConfig";
import ReactionApiService, {voteTypeToId} from "../../services/ReactionApiService";
import StarReactions from "../common/Card/StarReactions";
import React from "react";

@observer
class CardViewerPane extends Component {

    constructor(props) {
        super (props)
        this.state = {
            reactions: null, currentCardId: null,
            saving: false, isDirty: false, uploading: false, error: null, hoverLink: false
        }
        this.prevProjectId = null
    }

    handleClose = (ev) => {
        const project = projectStore.currentProject
        const isApproval = project && project.projectType === ProjectType.APPROVAL
        if (isApproval) { // disable closing large view for approvals
            if (ev && ev.target)
                ev.target.blur ()
            return
        }

        this.handleSave (ev).then (response => {
            uiStateStore.setViewingCard (null)
            uiStateStore.unModal = null
            uiStateStore.filterDiscussionByCard = false
            uiStateStore.expandDiscussionInput = false
            window.removeEventListener ('keydown', this.handleKeyDown)
        })

    }

    handleEdit = (ev) => {
        uiStateStore.setEditingCard (uiStateStore.viewingCard)
        this.handleClose (ev)
    }

    onVote = (reactionResponse) => {
        // const atLastCard = this.props.index === this.props.total - 1
        this.getReactions ()
        this.props.onVote (reactionResponse)
    }

    getReactions = () => {
        const {card, author, index, total, apStats} = this.props
        if (!card)
            return
        getCardReactions (card.id, true).then (response => {
            this.setState ({reactions: response.data, currentCardId: card.id})
        })
    }

    toggleConvoFiltering = (ev) => {
        uiStateStore.showDiscussion = true

        uiStateStore.filterDiscussionByCard = true

        if (this.props.onInputRequest)
            this.props.onInputRequest (ev)
    }

    togglePaint = (ev) => {
        // this.setState({enablePaint: !this.state.enablePaint})
        uiStateStore.paintEnable = !uiStateStore.paintEnable
    }

    toggleMarkup = (ev) => {
        uiStateStore.paintVisible = !uiStateStore.paintVisible
    }

    onPaintCancel = (ev) => {
    }

    onPaintSave = (ev) => {
        let card = uiStateStore.viewingCard || this.props.card
        cardStore.getCardById (card.id).then (card => {
            uiStateStore.viewingCard = card
            if (this.props.onPaintSave)
                this.props.onPaintSave (card.title)
        })
    }

    onPaintClear = (ev) => {
        const card = uiStateStore.viewingCard
        if (card.annotations && card.annotations.length > 0) {
            deleteAnnotation ({cardId: card.id}).then (response => {
                card.annotations = null
                this.setState (this.state)
            })
        }
    }


    onCommentSave = (ev) => {
        projectStore.getCurrentProjectConvo ()
    }


    openAttm = (attm) => {
        if (attm && attm.url && window)
            window.open (attm.url)
    }

    closeAttm = () => {
        uiStateStore.setViewingAttm (null)
    }

    atFirstSlide() {
        return uiStateStore.viewingCardIndex === 0
    }

    atLastSlide() {
        return uiStateStore.viewingCardIndex === cardStore.currentCards.length - 1
    }

    // Feedback: don't follow link on click. Instead, allow image change.
    handleImageClick = (ev) => {
        let card = uiStateStore.viewingCard
    }

    handleChange = (ev) => {
        console.warn ('CardViewer change. ')
        let card = uiStateStore.viewingCard

        const propName = ev.target.name
        const propValue = ev.target.value

        card[propName] = propValue
        this.setState ({isDirty: true}) // propValue && propValue.length > 0})
    }

    handleKeyDown = (ev) => {
        console.warn (`CardViewer key down. key: ${ev.key}`)
        if (ev.key === 'Enter')
            this.handleSave (ev)
        else if (ev.key === 'Escape') {
            this.handleClose (ev)
        }
    }

    handleFocus = (ev) => {
        uiStateStore.arrowPriority = 'text'
    }

    handleSave = (ev) => {

        const card = uiStateStore.viewingCard
        if (ev && ev.target && ev.type === 'keydown') {
            ev.stopPropagation ()
            ev.target.blur ()
        }

        uiStateStore.arrowPriority = 'nav'
        return new Promise ((resolve, reject) => { // for resolving to other handlers like close
            if (this.state.saving) // patience
                resolve (false)

            if (!this.state.isDirty)
                resolve (true)
            this.setState ({saving: true})
            cardStore.saveCard (card).then (cardResponse => {
                this.setState ({saving: false})
                resolve (true)
            }).catch (error => {
                this.setState ({error: error})
            })
        })
    }

    handleImageChooserOpen = (ev) => {
        window.removeEventListener ('keydown', this.handleKeyDown)
    }

    handleImageChange = (ev) => {
        cardStore.getCardById (uiStateStore.viewingCard.id).then (card => {
            uiStateStore.viewingCard = card
        })
        window.addEventListener ('keydown', this.handleKeyDown)
    }


    /*
        hasPropChanged(propName) {
            if (!this.origCard || !uiStateStore.viewingCard)
                return false

            if (propName !== undefined)
                return this.origCard[propName] !== uiStateStore.viewingCard[propName]
            return false
        }
    */

    selCard = (cardId) => {
        const cards = cardStore.currentCards || []
        if (!cardId || !cards)
            return

        const index = cards.findIndex (c => c.id === cardId)
        this.selIndex (index)
    }

    selIndex = (index) => {
        uiStateStore.viewingCardIndex = index
        uiStateStore.setViewingCard (cardStore.currentCards[index])
    }


    gotoPreviousCard(ev) {
        ev.preventDefault ()
        if (this.atFirstSlide ())
            return

        uiStateStore.viewingCardIndex -= 1
        uiStateStore.setViewingCard (cardStore.currentCards[uiStateStore.viewingCardIndex])
    }

    gotoNextCard(ev) {
        ev.preventDefault ()
        if (this.atLastSlide ())
            return

        uiStateStore.viewingCardIndex += 1
        uiStateStore.setViewingCard (cardStore.currentCards[uiStateStore.viewingCardIndex])
    }

    gotoLink = (ev) => {
        const card = uiStateStore.viewingCard
        if (card && card.url && window)
            window.open (card.url)
    }

    hoverLink(hover) {
        this.setState ({hoverLink: hover})
    }

    componentDidMount() {
        this.elem = findDOMNode (this)
        if (this.elem)
            this.elem.focus ()

        const project = projectStore.currentProject
        const isApproval = project && project.projectType === ProjectType.APPROVAL
        if (isApproval) {

            // reaction sets
            this.thumbsRSId = ReactionApiService.reviewTypeToRSId (THUMBS)
            this.starsRSId = ReactionApiService.reviewTypeToRSId (STARS)

            // reactions
            this.thumbsUpId = voteTypeToId (THUMBS_UP)
            this.thumbsDownId = voteTypeToId (THUMBS_DOWN)
            this.starsId = ReactionApiService.getStarsId ()
            this.getReactions ()
        }

        this.origCropImages = !isApproval && userStore.userPrefs ? userStore.userPrefs.cropImages : false
        this.prevProjectId = projectStore.currentProject ? projectStore.currentProject.id : null
        if (userStore.userPrefs)
            userStore.userPrefs.cropImages = false

        uiStateStore.filterDiscussionByCard = true
        uiStateStore.expandDiscussionInput = true
        uiStateStore.isManualAttm = false
        window.addEventListener ('keydown', this.handleKeyDown)
    }

    componentDidUpdate() {
        const project = projectStore.currentProject
        // reset if we've just navigated to a new project while cardviewer is open
        if (project && project.id !== this.prevProjectId) {
            this.handleClose ()
        }

        const {card} = this.props
        const {currentCardId} = this.state
        if (card && currentCardId !== card.id) {
            this.getReactions ()
        }

        const elem = findDOMNode (this)
        if (elem)
            elem.focus ()

    }

    componentWillUnmount() {
        userStore.userPrefs.cropImages = this.origCropImages
        this.handleClose ()

    }

    render() {
        const {saving, uploading, reactions, error, hoverLink} = this.state
        const {apStats, index, total} = this.props
        let card = uiStateStore.viewingCard || this.props.card
        if (!card) {
            if (cardStore.currentCards && cardStore.currentCards.length > 0)
                card = cardStore.currentCards[0]
            else
                return null
        }
        const me = userStore.currentUser
        const project = projectStore.currentProject
        const isApproval = project && project.projectType === ProjectType.APPROVAL
        const rsId = project? project.reactionSetId : 0

        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        const device = RU.getDeviceType ()
        const mobile = ['computer', 'tablet'].indexOf (device) === -1
        const forMobxOnly = uiStateStore.rand
        const viewingAttmId = uiStateStore.viewingAttmId
        const viewingAttm = viewingAttmId ? card.attachments.find (attm => attm.attachment_id === viewingAttmId) : null
        // const cardComments = projectStore.currentProjectConvo.filter( cmt => cmt.at_cards && (cmt.at_cards.find(c => c.card_id === card.id) !== undefined)) || []

        const shortUrl = card.url ? getShortUrl (card.url, true) : null

        const authorId = card ? card.creatorId : null
        const author = authorId ? userStore.getUserFromConnections (authorId) : null

        const atFirst = this.atFirstSlide ()
        const atLast = this.atLastSlide ()
        const height = '100%' // uiStateStore.showTags? '87%':'94%'

        const commentClass = 'pointer mr05 secondary' //+ (uiStateStore.filterDiscussionByCard? ' jBlue':' secondary')

        let approvals = [], rejections = [], ratings = []
        const cardReactions = reactions ? (reactions.card_reactions || []) : []
        if (isApproval && cardReactions.length > 0) {
            approvals = cardReactions.filter (r => r.reaction_type_id === this.thumbsUpId) || []
            rejections = cardReactions.filter (r => r.reaction_type_id === this.thumbsDownId) || []
            ratings = cardReactions.filter (r => r.reaction_type_id === this.starsId) || []
        }

        let ext, iconName, preview, user
        return (
            <div className='cardViewer relative'
                 style={{width: '100%', height: height, marginTop: '-.5rem', overflow: 'hidden'}}
                 onClick={this.handleClick}
            >
                <Loader active={saving || uploading}></Loader>

                {!uiStateStore.paintEnable &&
                <CardCarousel
                    cards={cardStore.currentCards}
                    reactions={isApproval}
                    selCard={this.selCard}
                    selIndex={this.selIndex}
                    selectedCard={uiStateStore.viewingCard}
                    selectedIndex={uiStateStore.viewingCardIndex}
                    atFirst={atFirst}
                    atLast={atLast}
                />
                }

                {/*
                <PDWTagToolbar  user={userStore.currentUser}
                                cards={cardStore.currentCards}
                                mobile={mobile}
                                project={project}
                                tags={project.tagSet2}
                                showTags={true}
                />
*/}

                {uiStateStore.paintEnable &&
                <PaintToolbar onPaintSave={this.onPaintSave}
                              onPaintCancel={this.onPaintCancel}
                              onPaintClear={this.onPaintClear}
                              card={card}
                />
                }

                <div className='cardBG jFG card hidden-scroll relative' style={{height: '88%', overflowY: 'auto'}}>

                    <ImageViewer image={card.image} title={card.title}
                                 style={{height: '60%', maxHeight: '60%'}}
                                 imgStyle={{width: 'unset', objectFit: 'unset'}}
                                 selected={false} readonly={true} mobile={true}
                                 handleClick={this.handleImageClick} wkey='cvpc'
                                 hideBG={true}
                                 isAP={isApproval}
                                 showRotation={false}
                                 enablePaint={uiStateStore.paintEnable}
                                 card={card}
                    />

                    {/*top right image tools*/}
                    {!uiStateStore.paintEnable &&
                    <div className='abs jFG' style={{top: '.5rem', right: '.5rem'}}>

                        {!isApproval &&
                        <X size={42} color='white' onClick={this.handleClose} className='nav mb05 pointer block jRedBG'
                           style={{marginLeft: '-6px'}}
                           data-tip='C L O S E'/>
                        }

                        {!isApproval &&
                        <div className='mb05'>
                            <ImportModal project={projectStore.currentProject}
                                         imageOnly={true}
                                         card={card}
                                         title='Change image'
                                         onClose={this.handleImageChange}
                                         onOpen={this.handleImageChooserOpen}
                                         trigger={
                                             <Icon name='camera'
                                                   data-tip='Change image'
                                                   className='nav pointer hoh mb05 vaTop'
                                                   style={{
                                                       padding: '.4rem',
                                                       marginBottom: '0',
                                                       fontSize: '20px',
                                                       width: '32px',
                                                       height: '32px'
                                                   }}/>
                                         }
                            />
                        </div>
                        }

                        <div className='mb05'>
                        <Icon name='paint brush'
                              data-tip='Draw'
                              className='nav pointer hoh vaTop block'
                              style={{
                                  padding: '.4rem',
                                  marginBottom: '0',
                                  fontSize: '20px',
                                  width: '32px',
                                  height: '32px'
                              }}
                              onClick={this.togglePaint}/>
                        </div>

                        <Icon.Group
                            className='pointer mb05 vaTop block'
                            onClick={this.toggleMarkup}
                            style={{marginTop: '.25rem'}}
                        >
                            <Icon name='dont'
                                  className={uiStateStore.paintVisible? ' jThird':' red'}
                                  style={{
                                      strokeWidth: 0,
                                      padding: 0,
                                      marginBottom: '0',
                                      fontSize: '30px',
                                      width: '30px',
                                      height: '30px'
                                  }}
                            />
                            <Icon name='paint brush'
                                  className='jThird'
                                  style={{
                                      padding: '.25rem',
                                      marginBottom: '0',
                                      fontSize: '16px',
                                      width: '26px',
                                      height: '26px'
                                  }}
                            />
                        </Icon.Group>

                    </div>
                    }

                    {!isApproval &&
                    <div className='abs jSec' style={{bottom: '.5rem', right: '.5rem'}}>
                        <Edit size={32} onClick={this.handleEdit} className='pointer mr05'
                              data-tip='Advanced'
                              style={{padding: '.3rem', marginBottom: '0'}}/>
                    </div>
                    }

                    {isApproval &&
                    <div className='abs voteStats' style={{bottom: '40.5%', left: '0'}}>
                        {approvals.length > 0 &&
                        <div className='jFG'>
                            {approvals.map (r => {
                                user = getUserFromMembers (r.user_id, project) || userStore.getUserFromConnections (r.user_id)
                                if (!user)
                                    return null
                                if (user.id === me.id)
                                    return <div>Approved by me.</div>
                                else
                                    return <div>Approved by {user.displayName || user.firstName}.</div>
                            })}
                        </div>
                        }

                        {rejections.length > 0 &&
                        <div className='jFG'>
                            {rejections.map (r => {
                                user = getUserFromMembers (r.user_id, project) || userStore.getUserFromConnections (r.user_id)
                                if (!user)
                                    return null

                                if (user.id === me.id)
                                    return <div>Disliked by me.</div>
                                else
                                    return <div>Disliked by {user.displayName || user.firstName}.</div>
                            })}
                        </div>
                        }

                        {ratings.length > 0 &&
                        <div className='fg '>
                            {ratings.map (r => {
                                user = getUserFromMembers (r.user_id, project) || userStore.getUserFromConnections (r.user_id)
                                if (!user)
                                    return null
                                if (user.id === me.id)
                                    return <div>{r.reaction_value} stars from me.</div>
                                else
                                    return <div>{r.reaction_value} stars
                                        from {user.displayName || user.firstName}.</div>
                            })}
                        </div>
                        }

                        {/*empty cases*/}
                        {/*
                        <div className='jThird lighter tiny'>
                            {approvals.length === 0 &&
                            <div>No approvals</div>
                            }
                            {rejections.length === 0 &&
                            <div>No dislikes</div>
                            }
                            {false && ratings.length === 0 &&
                            <div>No ratings</div>
                            }
                        </div>
*/}

                        {/*
                        <div className='secondary small mt1' >
                            Item {index+1} of {total}
                        </div>
*/}

                    </div>
                    }

                    <div style={{padding: '.5rem 0 0 1rem'}}>
                        {!isApproval &&
                        <CardReactions card={card} size={24}/>
                        }

                        {isApproval && rsId === this.thumbsRSId &&
                        <ApprovalReactions
                            showNav={true}
                            atFirst={atFirst}
                            atLast={atLast}
                            project={project}
                            cardId={card.id}
                            selCard={this.props.selCard}
                            selIndex={this.props.selIndex}
                            selectedCard={uiStateStore.viewingCard}
                            selectedIndex={uiStateStore.viewingCardIndex}
                            rsum={cardReactions}
                            onVote={this.onVote}
                            size={300}/>
                        }
                        {isApproval && rsId === this.starsRSId &&
                        <StarReactions
                            cardId={card.id}
                            selectedCard={uiStateStore.viewingCard}
                            selectedIndex={uiStateStore.viewingCardIndex}
                            onVote={this.onVote}
                            size={300}/>
                        }
                    </div>

                    {/* INFO */}
                    <div className='mb05 relative' style={{padding: '0.25rem 0 0 1rem'}}>

                        <input type='text'
                               name='title'
                               onChange={this.handleChange}
                               onFocus={this.handleFocus}
                               onBlur={this.handleSave}
                               onKeyDown={this.handleKeyDown}
                               className='large fullW boh mt05'
                               style={{fontSize: '1.5rem', cursor: 'text', paddingLeft: 0}}
                               data-tip="Click to edit title!"
                               value={card.title} placeholder='Enter a title'/>

                        <div className='jSec small'>
                            {author &&
                            'By ' + author.displayName + (card.dateCreated ? (', ' + new Date (card.dateCreated).toLocaleDateString ()) : '')
                            }
                            {!author && card.dateCreated &&
                            'Added on ' + new Date (card.dateCreated).toLocaleDateString ()
                            }
                        </div>

                        <textarea
                            rows={2}
                            name='description'
                            data-tip="Click to edit description!"
                            onChange={this.handleChange}
                            onFocus={this.handleFocus}
                            onBlur={this.handleSave}
                            onKeyDown={this.handleKeyDown}
                            className='formPane jSec small subtle-scroll-dark'
                            style={{resize: 'none', padding: 0, border: 'none', cursor: 'text'}}
                            value={card.description}
                        >
                            {card.description}
                        </textarea>

                    </div>

                    <div className='mb05 ml1 cardBG ' style={{display: 'flex', flexDirection: 'row'}}>
                        <CardCommentInput card={card}
                                          isAnon={isAnon}
                                          mobile={mobile}
                                          onSubmit={this.onCommentSave}

                        />
                    </div>

                    {((card.url && card.url.length > 0) || (card.attachments && card.attachments.length > 0)) &&
                    <div className='ml1 mb05 cardBG capsule' style={{display: 'flex', justifyContent: 'bottom'}}>

                        {card.url && card.url.length > 0 &&
                        <span className='mr1 pointer vaTop jItemBG'
                              onClick={this.gotoLink}
                              onMouseEnter={() => this.hoverLink (true)}
                              onMouseLeave={() => this.hoverLink (false)}
                              style={{
                                  padding: '8px 8px 0 4px', borderRadius: '20px',
                                  height: '38px', maxWidth: '96%', transition: 'width 0.2s'
                              }}
                        >
                                    <ArrowUpRight size={24} className="fg vaTop"
                                                  data-tip={'Open link at ' + shortUrl}
                                    />
                                    <span className='inline vaTop'
                                          data-tip={'Open link at ' + shortUrl}
                                          style={{marginTop: '1px'}}> {shortUrl}
                                    </span>
                            {/*
                                    <Edit2 size={18} className='fg ml05 vaTop'
                                           data-tip='Edit link URL'
                                           style={{display: hoverLink? 'inline':'none', marginTop: '1px'}}/>
*/}
                        </span>
                        }

                        {card.attachments && card.attachments.map ((attm, i) => {
                            attm.title = attm.url
                            ext = getExt (attm.url)
                            iconName = extToIcon (ext)
                            preview = isPreviewable (ext)

                            return (
                                <AttmThumb file={attm} ext={ext} iconName={iconName} preview={preview}
                                           size={38} className='mr05 pointer vaTop attmInCard boxRadius'
                                           padIcon={true}
                                           openAttm={() => this.openAttm (attm)}/>

                            )
                        })}

                    </div>
                    }

                    {card.tags2 && card.tags2.length > 0 &&
                    <div className='ml1 mb1 fullW'>
                        <CardTagStrip tags={card.tags2}
                                      card={card}
                                      editMode={true}
                        />
                    </div>
                    }

                </div>

                {viewingAttm &&
                <span className='abs viewingAttm borderAll subtleShadow'
                      style={{top: '1rem', left: '1rem', width: '90%', height: '90%'}}>
                    <EmbedContainer url={viewingAttm.url} ext={viewingAttm.file_type} className='full'/>
                </span>
                }
            </div>
        )
    }
}

export default CardViewerPane