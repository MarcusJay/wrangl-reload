import {Component} from 'react'
import {observer} from 'mobx-react'
import uiStateStore, {SendForApproval} from '/store/uiStateStore'
import ApprovalPrepPane from "./ApprovalPrepPane";

@observer
export default class RightUnmodalContainer extends Component {

    render() {
        if (!uiStateStore.rightUnModal)
            return null

        const panelName = uiStateStore.rightUnModal
        let panel
        switch (panelName) {
            case SendForApproval:
                panel = <ApprovalPrepPane/>
                break
            default:
                panel = <span className='rightUnModal'>{panelName}</span>
        }
        return (
            <div >
                {panel}
            </div>
        )

    }
}