
const helpBase = '/static/img/stocks/help/'
const tempImg = '/static/img/stocks/b.jpg'

export default ({title, desc, image}) => {

    const imgUrl = image? helpBase+image : tempImg

    return (
        <div className='inline cardRadius'
             style={{marginRight: '2rem', overflow:'hidden', width: '25%', height: '96%'}}>
            <div className='cover' style={{minHeight: '70%', backgroundImage: 'url('+imgUrl+')'}}>&nbsp;</div>
            <div className='jBGinv jFGinv pad1 '>
                <div className='large bold'>{title}</div>
                <div className='tiny'>{desc}</div>
            </div>
        </div>
    )


}