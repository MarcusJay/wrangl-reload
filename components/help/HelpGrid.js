import React, {Component} from 'react'
import {Link, Router} from '~/routes'
import projectStore from '/store/projectStore'
import uiStateStore from '/store/uiStateStore'
import {Divider} from "semantic-ui-react";
import UserTour from "./UserTour";
import {LeftPaneTab} from "../../config/Constants";
import ImportModal from "../modals/ImportModal";
import userStore from "../../store/userStore";
import {ChevronLeft} from "react-feather";
import RU from "../../utils/ResponsiveUtils";
import HelpGridItem from "./HelpGridItem";

export default class HelpMenuLarge extends Component {

    showProjects = (ev) => {uiStateStore.lpTab = LeftPaneTab.PROJECTS}
    showApprovals = (ev) => {uiStateStore.lpTab = LeftPaneTab.APPROVALS}
    showContacts = (ev) => {uiStateStore.lpTab = LeftPaneTab.CONTACTS}
    toggleAddMembersHelp = (ev) => {this.setState({showAddMembers: !this.state.showAddMembers})}

    createProject = () => {

        this.props.createProject()
    }

    render() {
        const project = projectStore.currentProject
        const user = userStore.currentUser
        const device = RU.getDeviceType()
        const mobile = ['computer','tablet'].indexOf(device) === -1


        return (
            <div className="pad1 centered relative mt1" style={{height: '85%', width: mobile? '90%':'90%'}}>
                <h2 className='jFG thinner'>Welcome to Pogo.io. To help you get started:</h2>

                <div className='row mb2' style={{height: '45%'}}>
                    <HelpGridItem title='Create' desc='Create folders, boards, and cards' image='create.jpg'/>
                    <HelpGridItem title='Submit' desc='Submit your work for approval' image='submit.jpg'/>
                    <HelpGridItem title='Approve' desc='Approve incoming work' image='approve.jpg'/>
                </div>
                <div className='row' style={{height: '45%'}}>
                    <HelpGridItem title='Discuss' desc='Hold a group discussion' image='discuss.jpg'/>
                    <HelpGridItem title='Organize' desc='Organize your boards and files' image='organize.jpg'/>
                    <HelpGridItem title='Find' desc='Search your collection or the web' image='find.jpg'/>
                </div>


            </div>
        )
    }
}