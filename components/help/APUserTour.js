import React, {Component} from 'react'
import Joyride, {ACTIONS, EVENTS, STATUS} from 'react-joyride'
import {Circle, Frown, Link, MessageCircle, Play, Smile} from "react-feather";
import userStore from "../../store/userStore";


const jrOptions = {
    arrowColor: '#fff',
    backgroundColor: '#fff',
    beaconSize: 22,
    overlayColor: 'rgba(0, 0, 0, 0.5)',
    primaryColor: '#47C1F1', // jBlue
    spotlightShadow: '0 0 15px rgba(0, 0, 0, 0.5)',
    textColor: '#333',
    zIndex: 100,
};

class HelpTrigger extends Component {
    render() {
        const {onClick, onMouseEnter, title, ref} = this.props
        return (
                <span className='jBlueBG jFG round larger bold inline abs'
                      onClick={onClick}
                      onMouseEnter={onMouseEnter}
                      title={title}
                      style={{right: '-58px', top: 0, width: '22px', height: '22px', padding: '4px 0 0 5px'}}>
                    <Play size={14} color='white' className='pulse'/>
                </span>
        )
    }
}

export default class APUserTour extends Component {

    state = {
        stepIndex: 0,
        autoStart: false
    }

    steps = [

        {
            target: '.utAPHelpBegin',
            title: "Welcome to Pogo.io",
            disableBeacon: false,
            content:
                <div className='small'>
                    <span className='jPink bold'>Hi! Your approval has been requested.</span><br/><br/>
                    Hit Next for a quick tutorial, or Skip to discover on your own.
                </div>
        },

        {
            target: '.apUtFeed',
            title: "Items for Review",
            content: "Each item in this feed is here for your approval, rejection, feedback, changes, or exploration."
        },

        {
            target: '.apUtReactions',
            title: "Actions & Reactions",
            content:
                <div className='small' style={{textAlign: 'left'}}>
                    <div className='bold'>Things you can do with this item:</div><br/>
                    <div className='mb05 vaTop'><Smile size={18} style={{marginBottom: '-3px'}} className='jFGinv mr05'/> Approve</div>
                    <div className='mb05 vaTop'><Frown size={18} style={{marginBottom: '-3px'}} className='jFGinv mr05'/> Dislike, Reject</div>
                    <div className='mb05 vaTop'><MessageCircle  style={{marginBottom: '-3px'}} className='jFGinv mr05' size={18}/> Comment </div>
                    <div className='mb05 vaTop'><Link style={{marginBottom: '-3px'}} className='jFGinv mr05' size={18}/>
                        Open the original (if available)
                    </div>
                </div>
        },

        {
            target: '.apUtRequestor',
            title: "Requester",
            content: "The person who submitted this item for your approval."
        },

        {
            target: '.apUtConvo',
            title: "Group Conversation",
            content:
                <div className='' style={{textAlign: 'left'}}>
                    Open the general conversation for this request here. <br/><br/>
                    You can always tag specific <b>items</b> and <b>contacts</b> in the conversation.
                </div>
        },

        {
            target: '.apUtSB1',
            title: "Sidebar",
            content: "Here you can manage of all your incoming requests, contacts, and other boards."
        },

        {
            target: '.apUtDone',
            title: "Completing an Approval",
            content: "Once you've finished your comments and votes, you can Mark this approval as Done. It will be removed from your tasks."
        },

        {
            target: '.apUtXXX',
            title: "Claim your Account",
            content:
                <div className='small'>
                    To start participating, just tell us your name.<br/><br/>
                    To take advantage of all Pogo.io features, add a password and claim your account now!<br/><br/>
                    Thanks, and happy approving!<br/><br/>
                </div>
        }

    ]

    handleJoyRideStep = (jrState) => {
        const { action, index, status, type } = jrState;

        if (action === 'skip') {
            userStore.setUserPref(null, 'show_tour', false)
        }

        if ([EVENTS.STEP_AFTER, EVENTS.TARGET_NOT_FOUND].includes(type)) {
            // Update state to advance the tour
            this.setState({ stepIndex: index + (action === ACTIONS.PREV ? -1 : 1) });
        }
        else if ([STATUS.FINISHED, STATUS.SKIPPED].includes(status)) {
            // Need to set our running state to false, so we can restart if we click start again.
            this.setState({ run: false });
        }

        console.groupCollapsed(type);
        console.log(jrState); //eslint-disable-line no-console
        console.groupEnd()
    }

    componentDidMount() {
        const autoStart = userStore.userPrefs && userStore.userPrefs.autoShowTour
        this.steps[0].disableBeacon = autoStart
        this.setState({stepIndex: 0})
    }

    render() {
        const {stepIndex} = this.state

        return (
            <div id='utAP' className='relative inline' >
            <Joyride steps={this.steps}
                     stepIndex={stepIndex}
                     id='jrAP'
                     run={true}
                     styles={{options: jrOptions}}
                     continuous={true}
                     callback={this.handleJoyRideStep}
                     debug={true}
                     disableScrolling={true}
                     disableScrollParentFix={true}
                     // showProgress={true}
                     showSkipButton={true}
                     spotlightClicks={true}
                     beaconComponent={HelpTrigger}
            />
            </div>

        )
    }
}