import React, {Component} from 'react'
import {Link, Router} from '~/routes'
import projectStore from '/store/projectStore'
import uiStateStore from '/store/uiStateStore'
import {Divider} from "semantic-ui-react";
import UserTour from "./UserTour";
import {LeftPaneTab} from "../../config/Constants";
import ImportModal from "../modals/ImportModal";
import userStore from "../../store/userStore";
import {ChevronLeft} from "react-feather";
import RU from "../../utils/ResponsiveUtils";

export default class HelpMenuLarge extends Component {

    showProjects = (ev) => {uiStateStore.lpTab = LeftPaneTab.PROJECTS}
    showApprovals = (ev) => {uiStateStore.lpTab = LeftPaneTab.APPROVALS}
    showContacts = (ev) => {uiStateStore.lpTab = LeftPaneTab.CONTACTS}
    toggleAddMembersHelp = (ev) => {this.setState({showAddMembers: !this.state.showAddMembers})}

    createProject = () => {

        this.props.createProject()
    }

    render() {
        const project = projectStore.currentProject
        const user = userStore.currentUser
        const device = RU.getDeviceType()
        const mobile = ['computer','tablet'].indexOf(device) === -1


        return (
            <div className="pad1 relative" style={{margin: '5%', width: mobile? '90%':'70%'}}>
                <h1 className='jSec mb1'>Welcome to Pogo.io</h1>

                <p className='large jFG utHelpBegin uiHelpEnd'>Take the Guided Tour</p>
                {/*<p className='large jFG utHelpBegin uiHelpEnd'>Take the Approvals Tour</p>*/}

                <Divider />
                {!project &&
                <div className='large inline pointer jPink hoh' onClick={this.createProject}>
                    Create your first board
                    <img src='/static/svg/v2/plusBlue.svg' width={24} className='abs' style={{right: '.5rem'}}/>
                </div>
                }

                <Divider />
                <div className='pointer lighter jFG hoh mb1' onClick={this.showContacts}>
                    View and manage your contacts
                    <img src='/static/svg/v2/contacts.svg' width={22} className='abs' style={{right: '.5rem'}}/>
                </div>
                <div className='pointer lighter jFG hoh mb1' onClick={this.showProjects}>
                    View and manage your boards
                    <img src='/static/svg/v2/briefcase.svg' width={22} className='abs' style={{right: '.5rem'}}/>
                </div>
                <div className='pointer jFG hoh mb1' onClick={this.showApprovals}>
                    View and manage your approvals
                    <img src='/static/svg/v2/checkCircle.svg' width={22} className='abs' style={{right: '.5rem'}}/>
                </div>



                {project &&
                <span>
                    <Divider />
                    <ImportModal project={project}
                                 trigger={
                                     <div className='pointer hoh'>Add items</div>
                                 }
                                 importCards={this.importCards} createCards={this.createCards}
                                 importAssets={this.importAssets} uploadAssets={this.uploadAssets}/>

                    <div className='pointer hoh' onClick={this.toggleAddMembersHelp}>Add members</div>
                    <div className='small lighter'>
                        To add people to this board, simply drag them from your contacts onto the board board, or click their pictures and select 'Add to board'.
                    </div>
                </span>
                }

                {/*<Divider />*/}
                {/*<p >Contact us</p>*/}
                <UserTour/>
            </div>
        )
    }
}