import React, {Component} from 'react'
import Joyride, {ACTIONS, EVENTS, STATUS} from 'react-joyride'
import {Play} from "react-feather";
import userStore from "../../store/userStore";
import uiStateStore from "../../store/uiStateStore";

const jrOptions = {
    arrowColor: '#fff',
    backgroundColor: '#fff',
    beaconSize: 22,
    overlayColor: 'rgba(0, 0, 0, 0.5)',
    primaryColor: '#47C1F1', // jBlue
    spotlightShadow: '0 0 15px rgba(0, 0, 0, 0.5)',
    textColor: '#333',
    zIndex: 100,
};

class HelpTrigger extends Component {
    render() {
        const {onClick, onMouseEnter, title, ref} = this.props
        return (
            <span className='jBlueBG jFG round larger bold inline abs'
                  onClick={onClick}
                  onMouseEnter={onMouseEnter}
                  title={title}
                  style={{right: '-42px', top: '-5px', width: '22px', height: '22px', padding: '4px 0 0 5px'}}>
                    <Play size={14} color='white' className='pulse'/>
            </span>
        )
    }
}

export default class UserTour extends Component {

    constructor(props) {
        super (props)
        this.state = {run: true}

        this.steps = [
            {
                target: '.utHelpBegin',
                title: 'Welcome to Pogo',
                content: "Let's run through the basics so you can get to work!",
                disableBeacon: this.props.autoStart === true,
                placement: 'bottom',
                offset: 25
            },

            {
                target: '.utNewFolder',
                title: 'Here are a few folders to get you started',
                content: "Folders help you organize your work. We have a folder for things you need to review, and those you are waiting on for others."
            },

            {
                target: '.utNewProject', // 'new card placeholder',
                title: 'Create a Board',
                content: 'Click New Board to create a board. You can create a subject and add content, team members, and comments.',
            },

            {
                target: '.utNewCard', // 'new card placeholder',
                title: 'Adding Content to your Board',
                content: 'Add images, links, documents and more. Upload them from your device or use our chrome extension to pull them in from anywhere.',
            },

            {
                target: '.utChrome', // 'chrome ext download link',
                title: 'Chrome Extension',
                content: <span>Our <a
                    href="https://chrome.google.com/webstore/detail/pogoio/pnonlpnonheokmhpjmjnonhkijehjdcf"
                    target='_blank'>chrome extension</a> allows you to pull images and links from other sites directly into your boards!</span>,
            },

            {
                target: '.utProjDesc', // 'new board and description and image',
                title: 'Customizing Boards',
                content: 'Add a description and image to your board to make it easy to find. Choose a cover image or we’ll just pick the first one you upload.',
                placement: 'right',
                offset: 50
            },

            {
                target: '.utShare', // 'share btn',
                title: 'Sharing Boards',
                content: 'Share the board via email, or a link.',
            },

            {
                target: '.utRequest', // '.request approval',
                title: 'Requesting Feedback and Approvals',
                content: "Click one or more items on the board. You can send within Pogo.io or copy a link to send via email, messaging or anywhere you’d like.",
            },

            {
                target: '.utContacts', // 'contacts header',
                title: 'Contacts',
                content: "Add contacts via email or search for current ones. Your first contact is Michelle, our CEO. She would love any feedback and questions you might have!",
            },

/*
            {
                target: '.utProjects', // 'projects header',
                title: 'Boards',
                content: 'Click the boards icon to quickly find boards and keep an eye on the latest updates',
            },

            {
                target: '.utApprovals', // 'approvals header',
                title: 'Tasks',
                content: "Click the tasks icon to see what tasks are on your plate, and check the status of feedback requests that you've sent to others.",
            },
*/

            {
                target: '.utBadges', // 'badges over projects in left pane',
                title: 'Badges',
                content: "A badge with a number indicates that there is new activity since you last viewed the item."
            },

/*
            {
                target: '.utCats', // 'Category tab and + ', /!*change to folders*!/
                title: 'Folders',
                content: 'You can organize your boards into folders if you like. Click the plus icon to create a folder, and then drag your boards into the folder!',
            },
*/

            {
                target: '.utProfile', // 'profile menu',
                title: 'Profile',
                content: 'View and customize your profile here. Add a photo so people can recognize you, and a password to keep your account secure.',
            },

            {
                target: '.utHelpEnd', // 'profile menu',
                title: "Happy pogo-ing!",
                content: "So you’re ready to get started! Don’t forget to add a photo so people can recognize you. We’re always here to get your feedback.",
            }

        ]
    }

    handleJoyRideStep = (jrState) => {
        const { action, index, status, type } = jrState;

        if (action === 'skip' || action === 'close' || ([STATUS.FINISHED, STATUS.SKIPPED].includes(status))) {
            userStore.setUserPref(null, 'show_tour', false)
            this.setState({run: false})
            uiStateStore.tourInProgress = false
        } else {
            uiStateStore.tourInProgress = true
        }

        if (jrState.step.target === '.utContacts') {
            uiStateStore.showSidebar = true
            if (userStore.userPrefs) {
                userStore.userPrefs.leftPaneOpen = true
                userStore.userPrefs.contactsOpen = true
            }
        }
    }


    render() {
        const {run} = this.state

        return (
            <div id='ut1' className='relative' >
            <Joyride steps={this.steps}
                     id='jr1'
                     run={run}
                     styles={{options: jrOptions}}
                     callback={this.handleJoyRideStep}
                     continuous={true}
                     debug={true}
                     disableScrolling={true}
                     disableScrollParentFix={true}
                     // showProgress={true}
                     showSkipButton={true}
                     spotlightClicks={true}
                     beaconComponent={HelpTrigger}

            />
            </div>

        )
    }
}