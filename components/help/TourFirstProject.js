import React, {Component} from 'react'
import Joyride, {ACTIONS, EVENTS, STATUS} from 'react-joyride'
import userStore from "../../store/userStore";
import projectStore from "../../store/projectStore";
import uiStateStore from "../../store/uiStateStore";
import {LeftPaneTab} from "../../config/Constants";

const jrOptions = {
    arrowColor: '#fff',
    backgroundColor: '#fff',
    beaconSize: 22,
    overlayColor: 'rgba(0, 0, 0, 0.5)',
    primaryColor: '#47C1F1', // jBlue
    spotlightShadow: '0 0 15px rgba(0, 0, 0, 0.5)',
    textColor: '#333',
    zIndex: 100,
};

const chatStepIndex = 4
const contactsStepIndex = 5
const firstCardIndex = 0

export default class UserTour extends Component {

    state = {
        hasAddedCard: false
    }

    handleStep = (jrState) => {
        const { action, index, status, type } = jrState;
        const project = projectStore.currentProject
        this.setState({hasAddedCard: project.cardTotal > 0})

        if (action === 'start') {
            uiStateStore.tourInProgress = true
        }
        else if (action === 'skip' || status === 'finished') {
            uiStateStore.tourInProgress = false
            userStore.userPrefs.autoShowTour = false
            userStore.setUserPref(null, 'show_tour', false)
            return
        } else if (!uiStateStore.tourInProgress) {
            userStore.userPrefs.autoShowTour = false
            userStore.setUserPref(null, 'show_tour', false)
            return
        }

        if (index === chatStepIndex) {
            uiStateStore.showDiscussion = true
        } else if (index === contactsStepIndex) {
            uiStateStore.showSidebar = true
            uiStateStore.lpTab = LeftPaneTab.CONTACTS
        }

        if ([EVENTS.STEP_AFTER, EVENTS.TARGET_NOT_FOUND].includes(type)) {
            // Update state to advance the tour
            this.setState({ stepIndex: index + (action === ACTIONS.PREV ? -1 : 1) });
        }
        else if ([STATUS.FINISHED, STATUS.SKIPPED].includes(status)) {
            // Need to set our running state to false, so we can restart if we click start again.
            this.setState({ run: false });
        }

    }

    componentDidMount() {
        const {hasAddedCard} = this.state

        this.steps = [
            {
                target: '.utNewCard1HIDE',
                title: !hasAddedCard? 'Welcome to your first Board' : 'Success',
                content: !hasAddedCard?
                    "Let's add an item to the board. An item can be anything - an image, document, link, or video." :
                    "You've added your first item. Excellent choice.",

                disableBeacon: true,
                placement: 'right',

            },

            /* 1 */
            {
                target: '.utNewCard2', // 'new card placeholder',
                title: 'Add more items',
                content: 'You can always add more items here, including web search results, site links, uploads, and items from other boards.',
                disableBeacon: true,
                placement: 'right'
            },

            /* 2 */
            {
                target: '.utChrome', // 'chrome ext download link',
                title: 'Browser Button',
                content: <span>Get our <a href="https://chrome.google.com/webstore/detail/pogoio/pnonlpnonheokmhpjmjnonhkijehjdcf" target='_blank'>chrome extension</a> to pull images and links from other sites directly into your boards!</span>,
                disableBeacon: true,
                placement: 'left'
            },

            /* 3 */
            {
                target: '.utShare', // 'share btn',
                title: 'Sharing',
                content: 'Invite people to join you at this board.',
                disableBeacon: true,
            },

            /* 4 */
            {
                target: '.utOpenConvo',
                title: 'Chat',
                disableBeacon: true,
                placement: 'top start',
                content:
                    <div>
                        Chat with people about the board. You can tag people and items by dragging them into the conversation.

                        <br/><br/>
                        <video autoPlay loop src='/static/mov/taggingUsersAndCards.mp4' width='300' />
                    </div>
            },

            /* 5 */
            {
                target: '.oiahsdfoiahsdfoih',
                title: 'Contacts',
                content: 'Add more contacts to your Pogo.io account so you can add them to boards.',
                disableBeacon: true,
                placement: 'right'
            },

            /* 6 */
            {
                target: '.utDM',
                title: 'Contacts',
                content:
                    <div>
                        Add more contacts to your Pogo.io account so you can add them to boards.<br/><br/>
                        Chat directly with any contact
                        <br/><br/>
                        <img src='/static/img/contactsScrnShot.png' width='200'/>
                    </div>,
                disableBeacon: true,
                placement: 'right'
            },

            /* 7 */
            {
                target: '.utProfile',
                title: 'Profile',
                content: 'Customize your profile here. Add a photo so people can recognize you, and a password to keep your account secure.',
                disableBeacon: true,
            },

            /* 8 */
            {
                target: '.utHelpEnd', // 'profile menu',
                title: "Complete",
                content: "That's it. You can always find additional help in the Help menu above. Thanks for trying pogo.io!",
                disableBeacon: true,
            }

        ]


    }

    componentDidUpdate() {
        if (this.props.cardCount > 0) {
            this.steps[firstCardIndex].title = 'Success'
            this.steps[firstCardIndex].content = "You've added your first item. Excellent choice."
        }
    }

    render() {
        return (
            <div id='ut2' className='relative' >
            <Joyride steps={this.steps}
                     id='jr1'
                     run={true}
                     callback={this.handleStep}
                     styles={{options: jrOptions}}
                     continuous={true}
                     debug={true}
                     disableScrolling={true}
                     disableScrollParentFix={true}
                     // showProgress={true}
                     showSkipButton={true}
                     spotlightClicks={true}

            />
            </div>

        )
    }
}