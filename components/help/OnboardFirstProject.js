import React, {Component} from 'react'
import {Link, Router} from '~/routes'
import projectStore from '/store/projectStore'
import uiStateStore from '/store/uiStateStore'
import {LeftPaneTab} from "../../config/Constants";
import userStore from "../../store/userStore";
import RU from "../../utils/ResponsiveUtils";
import {firstWord} from "../../utils/solo/firstWord";
import {findDOMNode} from "react-dom";
import UserTour from "./UserTour";
import Dropzone from 'react-dropzone'
import {sendImage} from "../../services/ImageFileService";
import {Loader} from "semantic-ui-react";

export default class extends Component {

    state = {saving: false, dropzoneActive: false}

    showProjects = (ev) => {uiStateStore.lpTab = LeftPaneTab.PROJECTS}

    createProject = () => {
        this.props.createProject()
    }

    onDrop(files) {
        this.fileRefs = files
        if (files.length > 0) {
            let fileRef = files[0]
            sendImage (fileRef, false, this.updateProgress.bind(this))
            // see updateProgress for results handling
        }
    }

    onDragEnter = () => {
        this.setState({dropzoneActive: true})
    }

    onDragLeave = () => {
        this.setState({dropzoneActive: false})
    }

    updateProgress(count, msg, uploadedImageUrl) {
        if (msg === 'start') {
            this.setState({saving: true})
        } else if (msg === 'end') {
            const user = userStore.currentUser
            if (uploadedImageUrl)
                user.image = uploadedImageUrl
            userStore.saveUser({id: user.id, image: uploadedImageUrl}).then( response => {
                this.setState ({saving: false})
                if (this.fileRefs)
                    this.fileRefs.forEach (file => window.URL.revokeObjectURL (file.preview))
            })
        }
    }


    componentDidMount() {
        const elem = findDOMNode(this)
        if (!elem)
            return

        // Arrow positioning isn't reliable enough in all cases, browsers, and resizing
/*
        const cbox = elem.querySelector('#cc1')
        const canvas = elem.querySelector('#c1')
        canvas.width = cbox.clientWidth
        canvas.height = cbox.clientHeight

        const ctx = canvas.getContext('2d');


        const endX = canvas.width - 60
        const endY = canvas.height - 48

        ctx.strokeStyle = '#a0a0a0'
        ctx.lineWidth = 1
        ctx.lineJoin = 'round'

        ctx.beginPath();
        ctx.moveTo(0, 0);
        ctx.quadraticCurveTo(20,220, endX, endY);
        ctx.stroke();

        ctx.lineTo(endX - 12, endY - 8);
        ctx.stroke();

        ctx.moveTo(endX, endY)
        ctx.lineTo(endX - 12, endY + 4);
        ctx.stroke();
*/


    }

    render() {
        const project = projectStore.currentProject
        const user = userStore.currentUser
        const device = RU.getDeviceType()
        const mobile = ['computer','tablet'].indexOf(device) === -1
        const showTour = userStore.userPrefs? userStore.userPrefs.autoShowTour : false

        return (
            <div className="fixed pad1 centered"
                 style={{height: '99%', paddingTop: '10%', width: mobile? '90%':'80%', marginRight: mobile? '':'5%'}}>
                <Loader active={this.state.saving}>Saving...</Loader>

                {showTour &&
                <UserTour autoStart={true}/>
                }
                <div className='abs mb1 centered'
                     style={{top: '100px', width: '98%'}}>
                    <div className='round inline coverBG utHelpBegin pointer'
                         style={{top: '0', backgroundImage: 'url('+user.image+')', width: '5rem', height: '5rem'}}>
                        <Dropzone onDrop={this.onDrop.bind(this)}
                                  multiple={false}
                                  disabled={false}
                                  onDragEnter={this.onDragEnter}
                                  onDragLeave={this.onDragLeave}
                                  style={{width: '100%', height: '100%', padding: 0}}
                        >
                        </Dropzone>
                    </div>
                </div>

                <h1 className='abs jFG mb1 centered'
                    style={{top: '150px', width: '98%'}}>Welcome, {firstWord(user.displayName)}</h1>

                <p className='abs small fullW centered jFG'
                   style={{top: '220px', width: '98%'}}>YOU HAVE NO BOARDS YET</p>

                <div id='cc1' className='abs fullW'
                     style={{top: '250px', left: '50%', width: '45%', height: '45%'}}>
                    <canvas id='c1'>

                    </canvas>
                </div>

                <div className="fixed pointer" style={{bottom: '3rem', right: '3rem'}}
                     onClick={this.createProject}>
                    <img src='/static/svg/v2/plusBlue.svg' width={48} />
                </div>

            </div>
        )
    }
}