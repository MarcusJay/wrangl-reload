import React, {Component} from 'react'
import {findDOMNode} from "react-dom";
import {Link, Router} from '~/routes'
import projectStore from '/store/projectStore'
import uiStateStore from '/store/uiStateStore'
import {Circle, HelpCircle, X} from "react-feather";
import {Divider} from "semantic-ui-react";
import UserTour from "./UserTour";
import {LeftPaneTab, ProjectType} from "../../config/Constants";
import ImportModal from "../modals/ImportModal";
import APUserTour from "./APUserTour";

export default class HelpPopup extends Component {

    state = {showPopup: false, showAddMembers: false}

    onOpen = (ev) => {
        this.setState({showPopup: true})
    }
    onClose = (ev) => {
        this.setState({showPopup: false})
    }

    showProjects = (ev) => {uiStateStore.lpTab = LeftPaneTab.PROJECTS}
    showApprovals = (ev) => {uiStateStore.lpTab = LeftPaneTab.APPROVALS}
    showContacts = (ev) => {uiStateStore.lpTab = LeftPaneTab.CONTACTS}
    toggleAddMembersHelp = (ev) => {this.setState({showAddMembers: !this.state.showAddMembers})}

    createProject = () => {
        this.props.createProject()
    }

    componentDidMount() {
    }

    componentDidUpdate() {
        const elem = findDOMNode(this)
        if (!elem) {
            console.error("Help Popup has no element after mount. REALITY VIOLATION CODE 234-0843")
            return
        }
        // TODO Replace with REF
        const popup = elem.querySelector('#hpop')
        if (popup && window) {
            const screenRight = window.innerWidth
            const popupRight = popup.getBoundingClientRect().right
            if (popupRight > screenRight)
                popup.style.right = '10px'
        }
    }

    render() {
        const {className,style, popupStyle} = this.props
        const {showPopup, showAddMembers} = this.state
        const project = projectStore.currentProject
        const isApproval = project && project.projectType === ProjectType.APPROVAL

        const combinedClass = 'relative ' +(className || '')
        let pStyle = {textAlign: 'left', minWidth: '12rem', borderRadius: '5px', overflow: 'hidden'}
        if (popupStyle)
            pStyle = Object.assign(pStyle, popupStyle)


        return (
            <span className={combinedClass} style={style}>
                <HelpCircle size={24}
                            className='jSec inline pointer hoh'
                            onClick={this.onOpen}
                            style={{strokeWidth: 1.5, marginTop: '1px'}}/>

                {showPopup &&
                <div id='hpop' className='abs zModal borderAll modalShadow' style={pStyle}>
                    <div className='jSideBG jFG pad1'>
                        <span>Pogo Help</span>
                        <X size={18} onClick={this.onClose} className='abs pointer' style={{top: '1rem', right: '1rem'}}/>
                    </div>

                    <div className='jFGinv jBGinv pad1'>
                        {!isApproval &&
                        <p className='relative utHelpBegin utHelpEnd'>Guided Tour</p>
                        }

                        {isApproval &&
                        <p className='relative utAPHelpBegin utHelpEnd'>Approvals Tour</p>
                        }

                        <Divider />
                        <Link route='projectsPage'>
                            <div className='pointer hoh' onClick={this.showProjects}>View all boards</div>
                        </Link>
                        {/*<div className='pointer hoh' onClick={this.showApprovals}>View all approvals</div>*/}
                        <div className='pointer hoh' onClick={this.showContacts}>View all my contacts</div>

                        <Divider />

                        {!project &&
                        <div className='pointer hoh' onClick={this.createProject}>Create a new board</div>
                        }

                        {project &&
                        <span>
                            <ImportModal project={project}
                                         trigger={
                                             <div className='pointer hoh'>Add items</div>
                                         }
                                         importCards={this.importCards} createCards={this.createCards}
                                         importAssets={this.importAssets} uploadAssets={this.uploadAssets}/>

                            <div className='pointer hoh' onClick={this.toggleAddMembersHelp}>Add members</div>
                            {showAddMembers &&
                            <div className='small lighter'>
                                To add people to this board, simply drag them from your contacts onto the board board, or click their pictures and select 'Add to board'.
                            </div>
                            }
                        </span>
                        }

                        {/*<Divider />*/}
                        {/*<p >Contact us</p>*/}
                    </div>
                    <UserTour/>
                    <APUserTour/>


                </div>
                }

            </span>
        )
    }
}