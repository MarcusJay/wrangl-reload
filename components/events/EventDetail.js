import {Component} from 'react'
import {observer} from 'mobx-react'
import Moment from 'react-moment'

import {User} from 'react-feather'
import {Link} from '~/routes'
import projectStore from '/store/projectStore'
import {AddCommentToCard, AddCommentToProject} from "../../config/EventConfig";
import uiStateStore, {CardViewer} from "../../store/uiStateStore";

const ANON = 'An anonymous user'

@observer
export default class EventDetail extends Component {

    gotoProfile(user) {
        debugger
        if (!user)
            return

        if (!user.id)
            debugger

        console.log('Going to profile ' +user.id)
        Router.pushRoute('/profile', {id: user.id})
    }

    gotoCard = (ev) => {
      const card = this.props.card
      if (!card.id)
          return

      uiStateStore.unModal = CardViewer
      uiStateStore.setViewingCard(card)
      this.props.close()
    }

    render() {
        const {msg, icon, type, date, count, user, friend, card, close, isNew, showProjectThumb, className, isPV } = this.props
        const project = this.props.project || projectStore.currentProject

        // const postUserMsg = isPV? msg : msg.substring( msg.indexOf('[user]') + 6 )
        // const iconIdx = postUserMsg.indexOf('[icon]')
        const userClass = user? 'userLink pointer':'anon'
        // const userTip = user? 'Visit ' +firstWord(user.displayName)+ "'s profile." : ''
        const isComment = [AddCommentToCard, AddCommentToProject].indexOf(type) !== -1
        const msgClass = msg === 'approved'? 'bold blue' : msg === 'rejected'? 'bold red' : msg === 'rated'? 'bold yellow': null
        // const suffix = project.projectType === ProjectType.APPROVAL? 'to approval.' : ''

/*
        let parsedMsg = msg
        if (userIdx >= 0)
            parsed.replace()
*/
        // if (!user)
        //     debugger



        return(
            <div className={className}>
                {user && user.image && !isPV &&
                <span style={{backgroundImage: 'url(' + (user.image) + ')'}} className='member vaTop pointer' >&nbsp;</span>
                }
                {(!user || (user && !user.image)) && !isPV &&
                <User size={28} className='eventIcon' />
                }

                {card && count === 1 &&
                <span style={{backgroundImage: 'url(' + (card.image) + ')'}} className='inline cThumb vaTop pointer'
                      data-tip={card.title} onClick={this.gotoCard}>&nbsp;</span>
                }

                <span className={'msg' +(!isPV? ' noWrap':'')}>

                    {/* A user... */}
                    {user && user.id &&
                    <span className={userClass}>
                      {user.displayName || user.email || user.firstName}
                    </span>
                    }
                    {(!user || !user.id) && !isPV &&
                    <span>{ANON}</span>
                    }

                    {/* did an action... */}
                    {icon === null &&
                    <span className={msgClass}>{' '+msg+' '}</span>
                    }

                    {/* to an object. */}
                    {card &&
                    <span className='bold pointer' onClick={this.gotoCard}>{card.title}</span>
                    }

                    {friend &&
                    <span className='pointer'
                          onClick={() => this.gotoProfile(friend.id)}
                          style={{textDecoration: friend.id? 'underline':'none'}}>{friend.displayName || friend.email}
                    </span>
                    }

                    {date &&
                    <div className='date micro jSec' style={{textTransform: 'upperCase', marginTop: '-.1rem'}}>
                        <Moment subtract={{hours: 8}} format='MMMM D, YYYY, hh:mma'>{this.props.date}</Moment>
                    </div>
                    }

                </span>

                {showProjectThumb &&
                <span style={{backgroundImage: 'url(' + (project.image) + ')'}} className='cThumb' data-tip={project.title}>&nbsp;</span>
                }
            </div>
        )
    }
}