import {Component} from 'react'
import {observer} from 'mobx-react'
import {Loader} from 'semantic-ui-react'
import {Bell, Edit2, Eye, FileText, MessageCircle, Plus, ThumbsUp, X} from 'react-feather'

import eventStore from '/store/eventStore'
import projectStore from '/store/projectStore'
import uiStateStore from "/store/uiStateStore";
import PaperTrail from "./PaperTrail";
import {findDOMNode} from "react-dom";
import RU from "../../utils/ResponsiveUtils";

@observer
export default class EventDetailsPopup extends Component {

    constructor(props) {
        super (props)
        // this.state = {queue: [], currentEvent: null}
        this.state = {showPopup: false, showTrigger: true, visible: true, view: 'sum', loading: false, popupViewed: false}
        this.project = projectStore.currentProject
        this.allEvents = []
        this.flashy = false
    }

    togglePopup(ev) {
        const showPopup = !this.state.showPopup
        this.setState({showPopup: showPopup, view: 'sum'})
        if (showPopup)
            this.setState({popupViewed: true})
    }

    onPopupClose(ev) {
        this.setState({showPopup: false, showTrigger: false, view: 'sum'})
        // TODO comment below to disable dismissing
        // eventStore.getInstance().dismissAllInProject(projectStore.currentProject.id)
    }

    showSummary = () => {
        if (this.popup) {
            this.popup.style.width = '50%'
        }
        this.setState({ view: 'sum' })
    }

    showAD = (ev) => {
        if (this.popup) {
            this.popup.style.width = '75%'
        }
        this.setState({ view: 'AD' })
    }

    showPT = (ev) => {
        if (this.elem) {
            this.elem.style.height = '97%'
        }
        this.setState({ view: 'PT' })
    }


    toggleVisibility = () => this.setState({ visible: !this.state.visible })

    gotoEventTarget(eventInfo) {
        // TODO
        // if added/updated cards, highlight cards.
        // if added/updated 1 card, open card in expand or gallery.
        // if user rleft, go to their profile.

    }

    isEventNew( event, unseenEvents ) {
        return unseenEvents.find( ue => ue.id === event.id ) !== undefined
    }

    fetchHistory() {
        this.setState({loading: true})
        const evs = eventStore.getInstance()
        const project = projectStore.currentProject

        // Get old history only (for now). Unseen events had to be fetched before us in project.js.
        evs.fetchProjectHistory(project.id).then( response => {
            this.setState({loading: false})
        }).catch( error => console.error(error))
    }

    componentDidMount() {
        this.elem = findDOMNode(this)
        if (this.elem)
            this.popup = this.elem.querySelector('.eventDetails')
        this.project = projectStore.currentProject || null
    }

    componentDidUpdate() {
        console.warn('Event details popup did update')
        this.elem = findDOMNode(this)
        if (this.elem)
            this.popup = this.elem.querySelector('.eventDetails')
        this.project = projectStore.currentProject || null
    }

    componentWillReact() {
        if (projectStore.currentProject !== null)
            if (!this.project || this.project.id !== projectStore.currentProject.id)
                this.fetchHistory()

    }

    render() {
        const project = projectStore.currentProject
        if (!project)
            return null

        const evs = eventStore.getInstance()
        const forMobxOnly = evs.oldRtEventCounts[project.id]

        const device = RU.getDeviceType()
        const mobile = ['computer','tablet'].indexOf(device) === -1

        const {totalUnseen, totalViews, totalCardAddEvents, totalCardUpdateEvents,
               totalCardCommentEvents, totalProjectCommentEvents,
               totalReactionEvents, lastViewed }
               = evs.countEventBreakdown(project.id)

        const {view, showPopup, popupViewed} = this.state
        const isOpen = uiStateStore.forcePopupOpen || this.state.showPopup
        const isViewsOnly = totalUnseen === totalViews
        const viewsColor = totalViews > 0? '#548fb3' : '#999'
        const eventsColor = !popupViewed && totalUnseen > 0 && totalUnseen !== totalViews? '#f0592b' : '#999'

        // const bgColor = totalUnseen > 0? '#f0592b' : '#999' // gOrange and third

        return (
            <span>
                <span
                    onClick={(ev) => this.togglePopup(ev)}
                >
                    <span className="pointer eventPopupTrig" style={{marginRight: '1rem', backgroundColor: eventsColor}}>
                        <Bell size={16} style={{marginBottom: '-4px'}}/>
                        {totalUnseen - totalViews > 0 &&
                        <span> {totalUnseen - totalViews}</span>
                        }
                    </span>

                    {false && totalUnseen > 0 &&
                    <span className="pointer eventPopupTrig" style={{marginRight: '1rem', backgroundColor: viewsColor}}>
                        <Eye size={16} style={{marginBottom: '-4px'}}/>
                        {totalViews > 0 &&
                        <span> {totalViews}</span>
                        }
                    </span>
                    }

                </span>

                {showPopup &&
                <div className='eventDetails subtle-scroll-dark' style={{width: mobile? '75%':'50%'}}>
                    <Loader active={this.state.loading}></Loader>
                    <X size={28} onClick={(ev) => this.togglePopup(ev)} className='secondary absTopRight'/>

                    {view === 'sum' &&
                    <div className="summary">
                        <h2 className="ev">
                            What's New
                            <span style={{fontSize: '0.9rem', marginLeft: '0.5rem'}}>
                                since your last visit: {/*<Moment subtract={{hours: 8}} fromNow>{lastViewed}</Moment>*/}
                            </span>
                        </h2>

                        <div style={{display: 'inline-block', width: '50%'}}>
                            <div className="ev" style={{color: totalViews > 0 ? viewsColor : ''}}>
                                <Eye size={18}/>
                                <span className="count">{totalViews} project view{totalViews === 1 ? '' : 's'}</span>
                            </div>

                            <div className="ev" style={{color: totalProjectCommentEvents > 0 ? eventsColor: ''}}>
                                <MessageCircle size={18}/>
                                <span
                                    className="count">{totalProjectCommentEvents} new message{totalProjectCommentEvents === 1 ? '' : 's'}</span>
                            </div>

                            <div className="ev" style={{color: totalReactionEvents > 0 ? eventsColor: ''}}>
                                <ThumbsUp size={18}/>
                                <span
                                    className="count">{totalReactionEvents} new vote{totalReactionEvents === 1 ? '' : 's'}</span>
                            </div>
                        </div>

                        <div style={{display: 'inline-block', width: '50%'}}>
                            <div className="ev" style={{color: totalCardAddEvents > 0 ? eventsColor: ''}}>
                                <Plus size={18}/>
                                <span
                                    className="count">{totalCardAddEvents} new card{totalCardAddEvents === 1 ? '' : 's'}</span>
                            </div>

                            <div className="ev" style={{color: totalCardUpdateEvents > 0 ? eventsColor: ''}}>
                                <Edit2 size={18}/>
                                <span
                                    className="count">{totalCardUpdateEvents} card change{totalCardUpdateEvents === 1 ? '' : 's'}</span>
                            </div>

                            <div className="ev" style={{color: totalCardCommentEvents > 0 ? eventsColor: ''}}>
                                <FileText size={18}/>
                                <span
                                    className="count">{totalCardCommentEvents} new card note{totalCardCommentEvents === 1 ? '' : 's'}</span>
                            </div>
                        </div>
                        <div className='actions'>
                            <button className='button outline' onClick={this.showAD.bind(this)}>Activity By Type</button>
                            <button className='button basic' onClick={this.showPT.bind(this)}>Paper Trail</button>
                        </div>


                    </div>
                    }


{/*
                    {view === 'AD' &&
                    <ActivityDashboard goBack={this.showSummary} close={this.togglePopup.bind(this)}/>
                    }
*/}

                    {view === 'PT' &&
                    <PaperTrail project={project} goBack={this.showSummary} close={this.togglePopup.bind(this)}/>
                    }

                </div>
                }
            </span>
        )


    }
}

