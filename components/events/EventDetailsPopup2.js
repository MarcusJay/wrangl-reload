import {Component} from 'react'
import {observer} from 'mobx-react'

import eventStore from '/store/eventStore'
import projectStore from '/store/projectStore'
import uiStateStore from "/store/uiStateStore";
import PaperTrail from "./PaperTrail";
import {findDOMNode} from "react-dom";
import {AddReactionToCard, ProjectView} from '../../config/EventConfig'
import {ProjectType} from "../../config/Constants";

@observer
export default class EventDetailsPopup2 extends Component {

    constructor(props) {
        super (props)
        // this.state = {queue: [], currentEvent: null}
        this.state = {showPopup: false, showTrigger: true, visible: true, view: 'sum', loading: false, popupViewed: false}
        this.project = projectStore.currentProject
        this.allEvents = []
        this.flashy = false
    }

    togglePopup(ev) {
        const showPopup = !this.state.showPopup
        this.setState({showPopup: showPopup, view: 'sum'})
        if (showPopup)
            this.setState({popupViewed: true})
    }

    onPopupClose(ev) {
        this.setState({showPopup: false, showTrigger: false, view: 'sum'})
        // TODO comment below to disable dismissing
        // eventStore.getInstance().dismissAllInProject(projectStore.currentProject.id)
    }

    showSummary = () => {
        if (this.popup) {
            this.popup.style.width = '50%'
        }
        this.setState({ view: 'sum' })
    }

    showAD = (ev) => {
        if (this.popup) {
            this.popup.style.width = '75%'
        }
        this.setState({ view: 'AD' })
    }

    showPT = (ev) => {
        if (this.elem) {
            this.elem.style.height = '97%'
        }
        this.setState({ view: 'PT' })
    }


    toggleVisibility = () => this.setState({ visible: !this.state.visible })

    gotoEventTarget(eventInfo) {
        // TODO
        // if added/updated cards, highlight cards.
        // if added/updated 1 card, open card in expand or gallery.
        // if user rleft, go to their profile.

    }

    isEventNew( event, unseenEvents ) {
        return unseenEvents.find( ue => ue.id === event.id ) !== undefined
    }

    fetchHistory() {
        if (this.state.loading)
            return

        // this.setState({loading: true})
        const evs = eventStore.getInstance()
        const project = projectStore.currentProject

        // Get old history only (for now). Unseen events had to be fetched before us in project.js.
        evs.fetchProjectHistory(project.id).then( events => {
            // this.setState({loading: false})
        }).catch( error => console.error(error))
    }

    componentDidMount() {
        this.elem = findDOMNode(this)
        if (this.elem)
            this.popup = this.elem.querySelector('.eventDetails')
        this.project = projectStore.currentProject || null
        this.fetchHistory()
    }

    componentDidUpdate() {
        this.elem = findDOMNode(this)
        if (this.elem)
            this.popup = this.elem.querySelector('.eventDetails')
        this.project = projectStore.currentProject || null
        if (this.project)
            this.fetchHistory()
        console.warn('EventDetailsPopup did update.')
    }

    componentWillReact() {
        if (projectStore.currentProject !== null)
            if (!this.project || this.project.id !== projectStore.currentProject.id)
                this.fetchHistory()

    }

    render() {
        const project = projectStore.currentProject
        if (!project)
            return null

        const evs = eventStore.getInstance()
        const forMobxOnly = evs.oldRtEventCounts[project.id]

        const {totalUnseen, totalViews, totalCardAddEvents, totalCardUpdateEvents,
               totalCardCommentEvents, totalProjectCommentEvents,
               totalReactionEvents, lastViewed }
               = evs.countEventBreakdown(project.id)

        const {view, showPopup, popupViewed} = this.state
        const isOpen = uiStateStore.forcePopupOpen || this.state.showPopup
        const isViewsOnly = totalUnseen === totalViews
        const viewsColor = totalViews > 0? '#548fb3' : '#999'
        const eventsColor = !popupViewed && totalUnseen > 0 && totalUnseen !== totalViews? '#f0592b' : '#999'

        const isApproval = project && project .projectType === ProjectType.APPROVAL
        const evTypes = isApproval? [AddReactionToCard, ProjectView] : null

        return (
            <span>
                <span
                    onClick={(ev) => this.togglePopup(ev)}>
                    <img src='/static/svg/v2/notifsPinkWhite.svg' className='mr1 vaTop boh pointer' data-tip='Paper Trail' style={{width: '20px', marginTop: '3px'}}/>
                </span>

                {showPopup &&
                <div className='eventDetails' style={{width: '50%'}}>
                    <PaperTrail types={evTypes} project={project} goBack={this.showSummary} close={this.togglePopup.bind(this)}/>
                </div>
                }
            </span>
        )


    }
}

