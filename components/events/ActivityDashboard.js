import {Component} from 'react'
import Moment from 'react-moment'
import {Button} from 'semantic-ui-react'

import {AddCardToProject, AddCommentToCard, AddReactionToCard, CardUpdate, CreateCard} from "/config/EventConfig";
import eventStore from "/store/eventStore";
import projectStore from "/store/projectStore";

import ProjectViews from "./Tiles/ProjectViews";
import ProjectMembers from "./Tiles/ProjectMembers";
import CardsAdded from "./Tiles/CardsAdded";
import CardsUpdated from "./Tiles/CardsUpdated";
import CommentHighlights from "./Tiles/CommentHighlights";
import {ArrowLeft, X} from "react-feather";

export default class ActivityDashboard extends Component {
    constructor(props) {
        super(props)
        this.hoverTile = this.hoverTile.bind(this)
        this.moveTile = this.moveTile.bind(this)

        this.state = {
            showAllEvents: false,
            forceView: false,
        }
    }

    showAll = (yesOrNo, force) => {
        this.setState({showAllEvents: yesOrNo, forceView: force })
    }

    hoverTile = (dragIndex, hoverIndex) => {
        const tiles = this.state.tiles
        const dragTile = tiles[dragIndex]
        debugger
        tiles.splice(dragIndex, 1)              // delete from current position
        tiles.splice(hoverIndex, 0, dragTile)   // insert into new position
        this.setState({tiles: tiles})
    }

    moveTile = () => {

    }

    goBack = (ev) => {
        this.props.goBack()
    }

    close = (ev) => {
        this.props.close()
    }

    componentWillMount() {

    }

    render() {

        const project = projectStore.currentProject
        const evs = eventStore.getInstance()
        let all = this.state.showAllEvents

        const forMobxOnly = eventStore.getInstance().oldRtEventCounts[project.id]
        const oldEvents = evs.allEventHistory[project.id] || []
        const unseenEvents = evs.unseenEventHistoryNew[project.id].events || []

        let rtEvents = evs.rtEvents[project.id]
        if (!rtEvents || rtEvents.length === 0) // observableArray won't work with a || b
            rtEvents = []

        // Make it interesting. If nothing new, show All time.
        if (!this.state.forceView && rtEvents.length === 0 && unseenEvents.length === 0)
            all = true

        this.startDate = all? project.dateCreated : evs.unseenEventHistoryNew[project.id].lastViewed

        const allEvents = all? rtEvents.concat(oldEvents) : rtEvents.concat(unseenEvents)
        const allEventsTotal = allEvents.length

        const cardAddEvents = allEvents.filter( event => event.type === AddCardToProject || event.type === CreateCard) || []
        const cardUpdateEvents = allEvents.filter( event => event.type === CardUpdate) || []
        const reactionEvents = allEvents.filter( event => event.type === AddReactionToCard )
        const commentEvents = allEvents.filter( event => event.type === AddCommentToCard ) // || event.type === AddCommentToProject )

        // TODO reduce redundant code in each tile by nesting them each inside a single Tile class
        // TODO which contains the d&d support.

        const tiles = [
            <ProjectViews key={'tileA'} hoverTile={this.hoverTile} moveTile={this.moveTile} id={0} index={0}
                          events={allEvents} old={oldEvents} all={all} since={this.startDate} total={allEventsTotal} pct={24}/>,

            <CardsAdded key={'tileB'} hoverTile={this.hoverTile} moveTile={this.moveTile} id={1} index={1}
                        events={cardAddEvents} all={all} since={this.startDate} total={cardAddEvents.length} pct={49}/>,

            <ProjectMembers key={'tileF'} hoverTile={this.hoverTile} moveTile={this.moveTile} id={2} index={2}
                            events={allEvents} all={all} since={this.startDate} total={allEventsTotal} pct={24}/>,

            <CardsUpdated key={'tileC'} hoverTile={this.hoverTile} moveTile={this.moveTile} id={3} index={3}
                          events={cardUpdateEvents} since={this.startDate} total={cardUpdateEvents.length} pct={49}/>,

            <CommentHighlights key={'tileE'} hoverTile={this.hoverTile} moveTile={this.moveTile} id={4} index={4}
                               events={commentEvents} since={this.startDate} total={commentEvents.length} pct={49}/>

        ]

        return (
            <div className='activityDashboard'>
                <div style={{marginBottom: '1rem'}}>
                    <div className='listHdr' >
                        <ArrowLeft size={24} style={{margin: '0 0.7rem -7px 0.3rem'}} onClick={this.goBack} data-tip="Return to Summary"/>
                        Activity by type:
                        <Button className={'actHdr' +(!all? '':' sel')} onClick={()=> this.showAll(true)} style={{marginLeft: '2rem'}}>All time</Button>
                        <Button className={'actHdr' +(all? '':' sel')} onClick={()=> this.showAll(false, true)}>Since last visit</Button>
                        <X size={28} onClick={this.close} className='secondary absTopRight'/>
                    </div>

                    {false && !all &&
                    <h4 style={{display: 'inline-block', color: '#777'}}>Since&nbsp;
                        <Moment format='h:mma' >{this.startDate}</Moment>, <Moment format='MMMM D'>{this.startDate}</Moment>.
                    </h4>
                    }


                </div>

                {tiles.map( tile => {
                    return tile
                })}

                {/*
                <CardsAdded />
                <CardUpdatesAndNotes />
                <CardNotes />
                <CardsDeleted />
                <MemberStats />
                <VotingStats />
                */}

            </div>

        )
    }
}