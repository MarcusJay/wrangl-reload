import {Component} from 'react'
import {DragSource, DropTarget} from 'react-dnd'
import {DndItemTypes} from '/config/Constants'
import PropTypes from 'prop-types'
import {findDOMNode} from 'react-dom'
import {Popup} from 'semantic-ui-react'
import Moment from 'react-moment'
import {ThumbsDown, ThumbsUp} from 'react-feather'

import projectStore from '/store/projectStore'
import userStore from '/store/userStore'
import cardStore from '/store/cardStore'
import ReactionApiService from '/services/ReactionApiService'
import {THUMBS_DOWN, THUMBS_UP} from '/config/ReactionConfig'
import {firstWord} from "../../../utils/solo/firstWord";

const MAX_REACTIONS = 14

let origIndex = -1
const tileSource = {
    beginDrag(props) {
        origIndex = props.index
        return {
            id: props.id,
            index: props.index,
            origIndex: origIndex // seems redundant, but isn't: index will be overwritten by final props.index, origIndex will not.
        }
    },
    endDrag(props, monitor) {
        let finalIndex = props.index
        console.error("### final index: " +finalIndex)
        return {
            id: props.id,
            index: props.index,
        }
    }
}

const tileTarget = {
    drop(props, monitor, component) {
        let draggedItem = monitor.getItem()

        const dragIndex = draggedItem.index
        const hoverIndex = props.index
        // if (draggedItem.origIndex !== hoverIndex)
        //     props.moveTile(draggedItem.id, draggedItem.origIndex, hoverIndex)

    },

    hover(props, monitor, component) {
        let draggedItem = monitor.getItem()

        // Ignore tag hover, covered by css
        if (draggedItem.tag !== undefined) {
            return
        }

        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            // return
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            // return
        }

        props.hoverTile(dragIndex, hoverIndex)

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex
    }
}

@DropTarget([DndItemTypes.ACTIVITY_TILE], tileTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@DragSource(DndItemTypes.ACTIVITY_TILE, tileSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
}))
export default class ReactionStats extends Component {
    static propTypes = {
        connectDragSource: PropTypes.func,
        connectDropTarget: PropTypes.func,
        connectDragPreview: PropTypes.func,
        index: PropTypes.number,
        isDragging: PropTypes.bool,
        isOver: PropTypes.bool,
        canDrop: PropTypes.bool,
        item: PropTypes.any,
        id: PropTypes.any,
        moveTile: PropTypes.func,
        hoverTile: PropTypes.func
    }

    constructor(props) {
        super(props)
    }

    render() {
        const project = projectStore.currentProject

        const {
            id,
            index,
            total,
            events,
            pct,
            isDragging,
            isOver,
            item,
            connectDragSource,
            connectDropTarget,
        } = this.props

        events.sort( (e1,e2) => {
            return e2.dateModified - e1.dateModified
        })
        const limit = Math.min(MAX_REACTIONS, events.length)
        const reactions = events.slice(0,limit)
        let user, reaction, card

        return connectDragSource (
            connectDropTarget (
                <div className='activityTile row3' style={{width: pct+'%'}}>
                    <h3>{total > 0? total:''} Reactions
                    {/*<Move size={20} className='right third hidden'/>*/}</h3>
                    <div className='actBody hidden-scroll-light' style={{height: '17rem'}}>
                        <div className='stat'>
                            {reactions.map (event => {
                                user = userStore.getUserFromConnections(event.userId)
                                card = event.card || cardStore.getCardFromCurrent(event.cardId)
                                reaction = ReactionApiService.getReactionByTypeId( event.detail )

                                return (
                                    <Popup position='top center'
                                           inverted
                                           trigger={
                                               <div className='inlineCard'>
                                                   <div className='actCard' >
                                                       <div className='thumb' style={{backgroundImage: 'url(' + (card.image) + ')'}}>
                                                           &nbsp;
                                                       </div>
                                                   </div>
                                                   <div className='vote'>
                                                       <div className='user left'>
                                                           <img src={user.image} />
                                                       </div>
                                                       <div className='icon'>
                                                           {reaction && reaction.reaction_name === THUMBS_UP &&
                                                           <ThumbsUp size={26} />
                                                           }
                                                           {reaction  && reaction.reaction_name === THUMBS_DOWN &&
                                                           <ThumbsDown size={26} />
                                                           }
                                                           {!reaction && // TODO cheating with old missing data
                                                           <ThumbsUp size={26} />
                                                           }
                                                       </div>
                                                   </div>

                                               </div>
                                           }>
                                        <div className='cardPopup'>
                                            <h4>{card.title}</h4>
                                            {card.description &&
                                            <div className='small secondary'>{card.description}</div>
                                            }
                                            <div className=''>
                                                <img className='cardImg' src={card.image} />
                                            </div>

                                            <div className='small secondary'>
                                                <img src={user.image} className='userImg'/>
                                                <span  style={{marginRight: '0.2rem'}}>{firstWord(user.displayName)} voted </span>
                                                <div className='icon'>
                                                    {reaction && reaction.reaction_name === THUMBS_UP &&
                                                    <ThumbsUp size={26} />
                                                    }
                                                    {reaction  && reaction.reaction_name === THUMBS_DOWN &&
                                                    <ThumbsDown size={26} />
                                                    }
                                                    {!reaction && // TODO cheating with old missing data
                                                    <ThumbsUp size={26} />
                                                    }
                                                </div>

                                                <Moment subtract={{hours: 7}} fromNow>{event.dateModified}</Moment>.
                                            </div>
{/*
                                            <div className='spacer'></div>
                                            {card.reactions && card.reactions.length > 0 &&
                                            <div className='small secondary'>
                                                {card.reactions.length} reaction{card.reactions.length === 1? '':'s'}
                                            </div>
                                            }
                                            {card.comments && card.comments.length > 0 &&
                                            <div className='small secondary'>
                                                {card.comments.length} comment{card.comments.length === 1? '':'s'}
                                            </div>
                                            }
*/}
                                        </div>

                                    </Popup>

                                )
                            })}
                        </div>

                        {false && enoughToRank &&
                        <div className='stat'>
                            <h4>Most Active Users </h4>
                            {topUsers.map (userInfo => {
                                return (
                                    <div className='rankedUser'>
                                        <img src={userInfo.user.image} className='userImg'/>
                                        <span className='name'>{userInfo.user.displayName}:</span>
                                        <span className='count'>{userInfo.count} Votes</span>

                                    </div>
                                )
                            })}
                        </div>
                        }



                    </div>
                </div>
            )
        )
    }
}