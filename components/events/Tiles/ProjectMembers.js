import {Component} from 'react'
import Moment from 'react-moment'

import projectStore from '/store/projectStore'
import userStore from '/store/userStore'
import ArrayUtils from "/utils/ArrayUtils";
import {ProjectLeaveForever, ProjectTakeBreak} from '/config/EventConfig'
import MemberPopup from "../../common/MemberPopupOLD";

// let origIndex = -1
/*
const tileSource = {
    beginDrag(props) {
        origIndex = props.index
        return {
            id: props.id,
            index: props.index,
            origIndex: origIndex // seems redundant, but isn't: index will be overwritten by final props.index, origIndex will not.
        }
    },
    endDrag(props, monitor) {
        let finalIndex = props.index
        console.error("### final index: " +finalIndex)
        return {
            id: props.id,
            index: props.index,
        }
    }
}
*/

/*
const tileTarget = {
    drop(props, monitor, component) {
        let draggedItem = monitor.getItem()

        const dragIndex = draggedItem.index
        const hoverIndex = props.index
        // if (draggedItem.origIndex !== hoverIndex)
        //     props.moveTile(draggedItem.id, draggedItem.origIndex, hoverIndex)

    },

    hover(props, monitor, component) {
        let draggedItem = monitor.getItem()

        // Ignore tag hover, covered by css
        if (draggedItem.tag !== undefined) {
            return
        }

        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            // return
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            // return
        }

        props.hoverTile(dragIndex, hoverIndex)

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex
    }
}
*/

/*
@DropTarget([DndItemTypes.ACTIVITY_TILE], tileTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@DragSource(DndItemTypes.ACTIVITY_TILE, tileSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
}))
*/
export default class ProjectStats extends Component {
/*
    static propTypes = {
        connectDragSource: PropTypes.func,
        connectDropTarget: PropTypes.func,
        connectDragPreview: PropTypes.func,
        index: PropTypes.number,
        isDragging: PropTypes.bool,
        isOver: PropTypes.bool,
        canDrop: PropTypes.bool,
        item: PropTypes.any,
        id: PropTypes.any,
        moveTile: PropTypes.func,
        hoverTile: PropTypes.func}
*/

    render() {
        const project = projectStore.currentProject

        const {
            id,
            index,
            all,
            events,
            unseen,
            pct,
            isDragging,
            isOver,
            item,
            connectDragSource,
            connectDropTarget,
        } = this.props

        const totalMembers = project.memberTotal + project.invitedMemberTotal
        const totalInvited = project.invitedMemberTotal

        const whoJoined = ArrayUtils.uniqueUsers( events.filter( event => event.updatedKey === 'user_role' && event.detail < 3) || [] )
        const whoLeft = ArrayUtils.uniqueUsers( events.filter( event => event.type === ProjectLeaveForever ) || [] )
        const onBreak = ArrayUtils.uniqueUsers( events.filter( event => event.type === ProjectTakeBreak ) || [] )

        const memberEvents = [
            {title: 'Joined', events: whoJoined, action: 'Joined project'},
            {title: 'On break', events: onBreak, action: 'On break since'},
            {title: 'Left', events: whoLeft, action: 'Left project on'}
            ]
        let user, event

        return (
                <div className='activityTile row1' style={{width: pct+'%'}}>
                    <h3>Members</h3>
                    <div className='actBody hidden-scroll-light'>
                        {!all && memberEvents.map( section => {
                            return (
                            <div className='statBox' style={{marginBottom: '0.5rem'}}>
                                <div className='statHdr'>{section.title}</div>
                                {section.events.length === 0 &&
                                <div className='third small '>
                                    None since&nbsp;
                                    <Moment format='h:mma' >{this.startDate}</Moment>, <Moment format='MMMM D'>{this.startDate}</Moment>.
                                </div>
                                }

                                {section.events.map( event => {
                                    user = userStore.getUserFromConnections(event.userId)
                                    return user?
                                        <MemberPopup
                                            image={user.image}
                                            name={user.displayName}
                                            role={user.role}
                                            projectDate={event.dateModified}
                                            wrangleDate={user.dateCreated}
                                            action={section.action}
                                        /> : null

                                })}
                            </div>
                            )
                        })}

                        {all &&
                        <div className='statBox' style={{marginTop: '1.5rem'}}>
                            <div className='statHdr'>All </div>
                            {project.members.map( member => {

                                // user = userStore.getUserFromConnections(member.userId) // member is obj but doesn't have creation date
                                event = whoJoined.find( event => event.userId === member.userId)

                                return (
                                    <MemberPopup
                                        image={member.image}
                                        name={member.displayName}
                                        role={member.role}
                                        projectDate={member.dateModified}
                                        wrangleDate={null}
                                        action={'Joined'}
                                    />
                                )
                            })}
                        </div>
                        }
                    </div>
                </div>
            )
    }
}