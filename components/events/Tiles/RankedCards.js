import {Component} from 'react'
import {DragSource, DropTarget} from 'react-dnd'
import {DndItemTypes} from '/config/Constants'
import PropTypes from 'prop-types'
import {findDOMNode} from 'react-dom'
import {Popup} from 'semantic-ui-react'
import Moment from 'react-moment'

import projectStore from '/store/projectStore'
import cardStore from '/store/cardStore'
import userStore from '/store/userStore'
import uiStateStore from "../../../store/uiStateStore";

const MAX_CARDS = 5
const MAX_USERS = 5

let origIndex = -1
const tileSource = {
    beginDrag(props) {
        uiStateStore.enableFileDrops = false
        origIndex = props.index
        return {
            id: props.id,
            index: props.index,
            origIndex: origIndex // seems redundant, but isn't: index will be overwritten by final props.index, origIndex will not.
        }
    },
    endDrag(props, monitor) {
        uiStateStore.enableFileDrops = true
        let finalIndex = props.index
        console.error("### final index: " +finalIndex)
        return {
            id: props.id,
            index: props.index,
        }
    }
}

const tileTarget = {
    drop(props, monitor, component) {
        let draggedItem = monitor.getItem()

        const dragIndex = draggedItem.index
        const hoverIndex = props.index
        // if (draggedItem.origIndex !== hoverIndex)
        //     props.moveTile(draggedItem.id, draggedItem.origIndex, hoverIndex)

    },

    hover(props, monitor, component) {
        let draggedItem = monitor.getItem()

        // Ignore tag hover, covered by css
        if (draggedItem.tag !== undefined) {
            return
        }

        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            // return
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            // return
        }

        props.hoverTile(dragIndex, hoverIndex)

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex
    }
}

@DropTarget([DndItemTypes.ACTIVITY_TILE], tileTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@DragSource(DndItemTypes.ACTIVITY_TILE, tileSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
}))
export default class ReactionStats extends Component {
    static propTypes = {
        connectDragSource: PropTypes.func,
        connectDropTarget: PropTypes.func,
        connectDragPreview: PropTypes.func,
        index: PropTypes.number,
        isDragging: PropTypes.bool,
        isOver: PropTypes.bool,
        canDrop: PropTypes.bool,
        item: PropTypes.any,
        id: PropTypes.any,
        moveTile: PropTypes.func,
        hoverTile: PropTypes.func}

    constructor(props) {
        super(props)
    }

    render() {
        const project = projectStore.currentProject

        const {
            id,
            index,
            total,
            events,
            pct,
            isDragging,
            isOver,
            item,
            connectDragSource,
            connectDropTarget,
        } = this.props

        // Rank cards. Move elsewhere
        const cardIds = events.map( event => event.cardId ) || []
        const uniqueCardIds = [...new Set(cardIds)] // sets have no dups by definition
        let card, cards = []
        uniqueCardIds.map ( cardId => {
            card = cardStore.getCardFromCurrent(cardId)
            if (card)
                cards.push(card)
        })

        const enoughToRank = (cards.filter( card => !!card.reactions ) || []).length > 2

        cards.sort( (c1,c2) => {
            if (!c1.reactions || !c2.reactions)
                return 0
            return c2.reactions.length - c1.reactions.length
        })
        const cLimit = Math.min(MAX_CARDS, cards.length)
        const topCards = cards.slice(0,cLimit)




        // Rank users. Move elsewhere
        const userIds = events.map( event => event.userId ) || []
        let userCountMap = {}
        userIds.map( userId => {
            if (userCountMap[userId] === undefined)
                userCountMap[userId] = 1
            else
                userCountMap[userId] += 1
        })

        const uniqueUserIds = Object.keys( userCountMap )
        let userCounts = []
        uniqueUserIds.map( userId => {
            userCounts.push({user: userStore.getUserFromConnections(userId), count: userCountMap[userId]})
        })

        userCounts.sort( (u1,u2) => {
            return u2.count - u1.count
        })
        const uLimit = Math.min(MAX_USERS, userCounts.length)
        const topUsers = userCounts.slice(0,uLimit)
        let creator

        return connectDragSource (
            connectDropTarget (
                <div className='activityTile' style={{width: pct+'%'}}>
                    <h3>Votes Registered: {total}{/*<Move size={20} className='right third hidden'/>*/}</h3>
                    <div className='actBody hidden-scroll-light'>
                        {enoughToRank &&
                        <div className='stat'>
                            <h4>Top Cards </h4>
                            {topCards.map (card => {
                                creator = card? userStore.getUserFromConnections(card.creatorId) : {}

                                return (
                                    <Popup position='top center'
                                           trigger={
                                               <div className='inlineCard'>
                                                   <div className='actCard' >
                                                       <div className='thumb' style={{backgroundImage: 'url(' + (card.image) + ')'}}>
                                                           &nbsp;
                                                       </div>
                                                   </div>
                                                   <span className='rank'>
                                                    {card.reactions? card.reactions.length : 0}
                                                   </span>
                                               </div>
                                           }>
                                        <div className='cardPopup'>
                                            <h4>{card.title}</h4>
                                            {card.description &&
                                            <div className='small secondary'>{card.description}</div>
                                            }
                                            <div className=''>
                                                <img className='cardImg' src={card.image} />
                                            </div>

                                            <div className='small secondary'>
                                                <img src={creator.image} className='userImg'/>
                                                <span  style={{marginRight: '0.2rem'}}>{creator.displayName} added this card </span>
                                                <Moment subtract={{hours: 7}} fromNow>{event.dateModified}</Moment>.
                                            </div>
                                            {card.reactions && card.reactions.length > 0 &&
                                            <div className='small secondary'>
                                                {card.reactions.length} reactions
                                            </div>
                                            }
                                            {card.comments && card.comments.length > 0 &&
                                            <div className='small secondary'>
                                                {card.comments.length} comments
                                            </div>
                                            }
                                        </div>

                                    </Popup>

                                )
                            })}
                        </div>
                        }

                        {false && enoughToRank &&
                        <div className='stat'>
                            <h4>Most Active Users </h4>
                            {topUsers.map (userInfo => {
                                return (
                                    <div className='rankedUser'>
                                        <img src={userInfo.user.image} className='userImg'/>
                                        <span className='name'>{userInfo.user.displayName}:</span>
                                        <span className='count'>{userInfo.count} Votes</span>

                                    </div>
                                )
                            })}
                        </div>
                        }



                    </div>
                </div>
            )
        )
    }
}