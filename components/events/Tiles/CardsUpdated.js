import {Component} from 'react'
import {DragSource, DropTarget} from 'react-dnd'
import {DndItemTypes} from '/config/Constants'
import PropTypes from 'prop-types'
import {findDOMNode} from 'react-dom'
import {Popup} from 'semantic-ui-react'
import Moment from 'react-moment'

import projectStore from '/store/projectStore'
import cardStore from '/store/cardStore'
import userStore from '/store/userStore'
import {getCardUpdateMsg} from '/config/EventConfig'
import {fieldToName} from "../../../models/EventModel";

const MAX_CARDS = 21

let origIndex = -1
const tileSource = {
    beginDrag(props) {
        origIndex = props.index
        return {
            id: props.id,
            index: props.index,
            origIndex: origIndex // seems redundant, but isn't: index will be overwritten by final props.index, origIndex will not.
        }
    },
    endDrag(props, monitor) {
        let finalIndex = props.index
        console.error("### final index: " +finalIndex)
        return {
            id: props.id,
            index: props.index,
        }
    }
}

const tileTarget = {
    drop(props, monitor, component) {
        let draggedItem = monitor.getItem()

        // const dragIndex = draggedItem.index
        const hoverIndex = props.index
        // if (draggedItem.origIndex !== hoverIndex)
        //     props.moveTile(draggedItem.id, draggedItem.origIndex, hoverIndex)

    },

    hover(props, monitor, component) {
        let draggedItem = monitor.getItem()

        // Ignore tag hover, covered by css
        if (draggedItem.tag !== undefined) {
            return
        }

        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            // return
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            // return
        }

        props.hoverTile(dragIndex, hoverIndex)

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex
    }
}

@DropTarget([DndItemTypes.ACTIVITY_TILE], tileTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@DragSource(DndItemTypes.ACTIVITY_TILE, tileSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
}))
export default class CardsUpdated extends Component {
    static propTypes = {
        connectDragSource: PropTypes.func,
        connectDropTarget: PropTypes.func,
        connectDragPreview: PropTypes.func,
        index: PropTypes.number,
        isDragging: PropTypes.bool,
        isOver: PropTypes.bool,
        canDrop: PropTypes.bool,
        item: PropTypes.any,
        id: PropTypes.any,
        moveTile: PropTypes.func,
        hoverTile: PropTypes.func}

    constructor(props) {
        super(props)
    }

    render() {
        const project = projectStore.currentProject

        const {
            id,
            index,
            total,
            events,
            pct,
            isDragging,
            isOver,
            item,
            connectDragSource,
            connectDropTarget,
        } = this.props

        const limit = Math.min(MAX_CARDS, total)

        const displayEvents = events.slice(0,limit)
        let card, msg, user, field

        return connectDragSource (
            connectDropTarget (
                <div className='activityTile row2Dbl' style={{width: pct+'%'}}>
                    <h3>{total > 0? total:''} Card Updates {/*<Move size={20} className='right third hidden'/>*/}</h3>

                    <div className='actBody subtle-scroll-dark' style={{height: '90%', width: '101%'}}>
                        {total === 0 &&
                        <div className='noData'>
                            No card updates since&nbsp;
                            <Moment format='h:mma' >{this.startDate}</Moment>, <Moment format='MMMM D'>{this.startDate}</Moment>.
                        </div>
                        }

                        {displayEvents.map( event => {
                            card = cardStore.getCardFromCurrent(event.cardId)
                            user = userStore.getUserFromConnections(event.userId)
                            msg = getCardUpdateMsg(event, card, user)
                            field = fieldToName(event.updatedKey, true)
                            if (!card)
                                return null
                            return (
                                <Popup position='bottom center' wide
                                    inverted
                                    trigger={
                                    <div className='inlineCard'>
                                        <div className='actCard'>
                                            <div className='thumb' style={{backgroundImage: 'url(' + (card.image) + ')'}}>
                                                &nbsp;
                                            </div>
                                            <div className='user'>
                                                <img src={user.image} />
                                            </div>
                                        </div>
                                        <div className='field'>
                                            {field}
                                        </div>
                                    </div>
                                }>
                                    <div className='cardPopup'>

                                        <div className='small secondary'>
                                            <img src={user.image} className='userImg'/>
                                            <span  style={{marginRight: '0.1rem'}}>
                                                {user.displayName}
                                            </span>
                                            <span> changed the </span>
                                            <span className='fieldName'>{fieldToName(event.updatedKey)}</span>
                                            <span>
                                                {event.updatedKey === 'card_image' &&
                                                    <img className='cardImg' src={event.detail} />
                                                }
                                                {event.updatedKey !== 'card_image' &&
                                                <h3>{event.detail}</h3>
                                                }
                                            </span>
                                            <div>
                                                <Moment subtract={{hours: 7}} fromNow>{event.dateModified}</Moment>.
                                            </div>
                                        </div>
                                    </div>

                                </Popup>
                            )
                        })}
                    </div>
                </div>
            )
        )
    }
}