import {Component} from 'react'
import {DragSource, DropTarget} from 'react-dnd'
import {DndItemTypes} from '/config/Constants'
import PropTypes from 'prop-types'
import {findDOMNode} from 'react-dom'

import projectStore from '/store/projectStore'
import ArrayUtils from "/utils/ArrayUtils";
import {ProjectView} from '/config/EventConfig'
import uiStateStore from "../../../store/uiStateStore";

let origIndex = -1
const tileSource = {
    beginDrag(props) {
        uiStateStore.enableFileDrops = false
        origIndex = props.index
        return {
            id: props.id,
            index: props.index,
            origIndex: origIndex // seems redundant, but isn't: index will be overwritten by final props.index, origIndex will not.
        }
    },
    endDrag(props, monitor) {
        uiStateStore.enableFileDrops = true
        let finalIndex = props.index
        console.error("### final index: " +finalIndex)
        return {
            id: props.id,
            index: props.index,
        }
    }
}

const tileTarget = {
    drop(props, monitor, component) {
        let draggedItem = monitor.getItem()

        const dragIndex = draggedItem.index
        const hoverIndex = props.index
        // if (draggedItem.origIndex !== hoverIndex)
        //     props.moveTile(draggedItem.id, draggedItem.origIndex, hoverIndex)

    },

    hover(props, monitor, component) {
        let draggedItem = monitor.getItem()

        // Ignore tag hover, covered by css
        if (draggedItem.tag !== undefined) {
            return
        }

        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            // return
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            // return
        }

        props.hoverTile(dragIndex, hoverIndex)

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex
    }
}

@DropTarget([DndItemTypes.ACTIVITY_TILE], tileTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@DragSource(DndItemTypes.ACTIVITY_TILE, tileSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
}))
export default class ProjectViews extends Component {
    static propTypes = {
        connectDragSource: PropTypes.func,
        connectDropTarget: PropTypes.func,
        connectDragPreview: PropTypes.func,
        index: PropTypes.number,
        isDragging: PropTypes.bool,
        isOver: PropTypes.bool,
        canDrop: PropTypes.bool,
        item: PropTypes.any,
        id: PropTypes.any,
        moveTile: PropTypes.func,
        hoverTile: PropTypes.func}

    constructor(props) {
        super(props)
    }

    render() {
        const project = projectStore.currentProject

        const {
            id,
            index,
            total,
            events,
            old,
            all,
            unseen,
            pct,
            since,
            isDragging,
            isOver,
            item,
            connectDragSource,
            connectDropTarget,
        } = this.props



        // Unseen
        const whoViewedUS = events.filter( event => event.type === ProjectView ) || []
        const totalViewsUS = whoViewedUS.length
        const uniqueViewsUS = ArrayUtils.uniqueUsers(whoViewed).length

        // Alltime
        const whoViewed = old.filter( event => event.type === ProjectView ) || []
        const totalViews = whoViewed.length
        const uniqueViews = ArrayUtils.uniqueUsers(whoViewed).length

        // const whoJoined = ArrayUtils.uniqueUsers( unseen.filter( event => event.type === ProjectJoinDeprecated ) || [] )
        // const whoLeft = ArrayUtils.uniqueUsers( unseen.filter( event => event.type === ProjectLeaveForever ) || [] )

        return connectDragSource (
            connectDropTarget (
                <div className='activityTile row1' style={{width: pct+'%'}}>
                    <h3>Views{/*<Move size={20} className='right third hidden'/>*/}</h3>

                    <div className='actBody hidden-scroll-light'>

                        {!all &&
                        <div className='statBox'>
                            <div className='statHdr'>Recent</div>
                            <div className='stat'>
                                <span className='label'> Total: </span>
                                <span className='value'> {totalViewsUS} </span>
                            </div>
                            <div className='stat'>
                                <span className='label'> Unique users: </span>
                                <span className='value'> {uniqueViewsUS} </span>
                            </div>
                        </div>
                        }

                        <div className='statBox' style={{marginTop: '1.5rem'}}>
                            <div className='statHdr'>All Time</div>
                            <div className='stat'>
                                <span className='label'> Total: </span>
                                <span className='value'> {totalViews} </span>
                            </div>
                            <div className='stat'>
                                <span className='label'> Unique users: </span>
                                <span className='value'> {uniqueViews} </span>
                            </div>
                        </div>

                    </div>
                </div>
            )
        )
    }
}