import {Component} from 'react'
import {DragSource, DropTarget} from 'react-dnd'
import {DndItemTypes} from '/config/Constants'
import PropTypes from 'prop-types'
import {findDOMNode} from 'react-dom'
import {Popup} from 'semantic-ui-react'
import Moment from 'react-moment'
import projectStore from '/store/projectStore'
import cardStore from '/store/cardStore'
import userStore from '/store/userStore'
import {firstWord} from "../../../utils/solo/firstWord";

const MAX_CARDS = 24

let origIndex = -1
const tileSource = {
    beginDrag(props) {
        origIndex = props.index
        return {
            id: props.id,
            index: props.index,
            origIndex: origIndex // seems redundant, but isn't: index will be overwritten by final props.index, origIndex will not.
        }
    },
    endDrag(props, monitor) {
        let finalIndex = props.index
        console.error("### final index: " +finalIndex)
        return {
            id: props.id,
            index: props.index,
        }
    }
}

const tileTarget = {
    drop(props, monitor, component) {
        let draggedItem = monitor.getItem()

        const dragIndex = draggedItem.index
        const hoverIndex = props.index
        // if (draggedItem.origIndex !== hoverIndex)
        //     props.moveTile(draggedItem.id, draggedItem.origIndex, hoverIndex)

    },

    hover(props, monitor, component) {
        let draggedItem = monitor.getItem()

        // Ignore tag hover, covered by css
        if (draggedItem.tag !== undefined) {
            return
        }

        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            // return
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            // return
        }

        props.hoverTile(dragIndex, hoverIndex)

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex
    }
}

@DropTarget([DndItemTypes.ACTIVITY_TILE], tileTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@DragSource(DndItemTypes.ACTIVITY_TILE, tileSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
}))
export default class CardsAdded extends Component {
    static propTypes = {
        connectDragSource: PropTypes.func,
        connectDropTarget: PropTypes.func,
        connectDragPreview: PropTypes.func,
        index: PropTypes.number,
        isDragging: PropTypes.bool,
        isOver: PropTypes.bool,
        canDrop: PropTypes.bool,
        item: PropTypes.any,
        id: PropTypes.any,
        moveTile: PropTypes.func,
        hoverTile: PropTypes.func}

    constructor(props) {
        super(props)
    }

    render() {
        const project = projectStore.currentProject

        const {
            id,
            index,
            total,
            events,
            all,
            pct,
            isDragging,
            isOver,
            item,
            connectDragSource,
            connectDropTarget,
        } = this.props

        const limit = Math.min(MAX_CARDS, total)

        // special case: if all, just use cards themselves.
        const items = all? project.cards : events.slice(0,limit)
        const totalItems = all? project.cards.length : total
        let card, user

        return connectDragSource (
            connectDropTarget (
                <div className='activityTile row1' style={{width: pct+'%'}}>
                    <h3>{totalItems > 0? totalItems:''} {all? ' ' : 'New '}Cards
                    {/*<Move size={20} className='right third hidden'/>*/}
                    </h3>

                    <div className='actBody hidden-scroll-light'>
                        {total === 0 &&
                        <div className='noData'>
                            No new cards have been added since&nbsp;
                            <Moment format='h:mma' >{this.startDate}</Moment>, <Moment format='MMMM D'>{this.startDate}</Moment>.
                        </div>
                        }
                        {items.map( eventOrCard => {
                            card = all? eventOrCard : eventOrCard.card || cardStore.getCardFromCurrent(eventOrCard.cardId) // one could say this expresses low confidence in event.card hydration
                            user = card? userStore.getUserFromConnections(card.creatorId) : null
                            if (!card || !user) {
                                debugger
                                console.warn('CardsAdded: card or user not found. Perhaps deleted. skipping', eventOrCard.cardId, eventOrCard.userId)
                                return null
                            }

                            return (
                                <Popup position='bottom center'
                                       inverted
                                    trigger={
                                    <div className='actCard'>
                                        <div className='thumb' style={{backgroundImage: 'url(' + (card.image) + ')'}}>
                                            &nbsp;
                                        </div>
                                        <div className='user'>
                                            <img src={user.image} />
                                        </div>

                                        {/*
                                        <div className='title noWrap'>
                                            {card.title}
                                        </div>
                                        <div className='author noWrap'>
                                            by {user.displayName}
                                        </div>
                                        */}
                                    </div>
                                }>
                                    <div className='cardPopup'>
                                        <h4>{card.title}</h4>
                                        {card.description &&
                                        <div className='small secondary'>{card.description}</div>
                                        }
                                        <div className=''>
                                            <img className='cardImg' src={card.image} />
                                        </div>

                                        <div className='small secondary'>
                                            <img src={user.image} className='userImg'/>
                                            <span  style={{marginRight: '0.1rem'}}>{firstWord(user.displayName)} added </span>
                                            <Moment subtract={{hours: 7}} fromNow>{eventOrCard.dateModified}</Moment>.
                                        </div>
                                        <div className='spacer'></div>
                                        {card.reactions && card.reactions.length > 0 &&
                                        <div className='small secondary'>
                                            {card.reactions.length} reaction{card.reactions.length === 1? '':'s'}
                                        </div>
                                        }
                                        {card.comments && card.comments.length > 0 &&
                                        <div className='small secondary'>
                                            {card.comments.length} comment{card.comments.length === 1? '':'s'}
                                        </div>
                                        }
                                    </div>

                                </Popup>
                            )
                        })}
                    </div>
                </div>
            )
        )
    }
}