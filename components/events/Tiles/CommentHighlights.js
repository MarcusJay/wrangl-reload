import {Component} from 'react'
import {DragSource, DropTarget} from 'react-dnd'
import {DndItemTypes} from '/config/Constants'
import PropTypes from 'prop-types'
import {findDOMNode} from 'react-dom'
import Moment from 'react-moment'

import projectStore from '/store/projectStore'
import cardStore from '/store/cardStore'
import userStore from '/store/userStore'

const MAX_EVENTS = 20

let origIndex = -1
const tileSource = {
    beginDrag(props) {
        origIndex = props.index
        return {
            id: props.id,
            index: props.index,
            origIndex: origIndex // seems redundant, but isn't: index will be overwritten by final props.index, origIndex will not.
        }
    },
    endDrag(props, monitor) {
        let finalIndex = props.index
        console.error("### final index: " +finalIndex)
        return {
            id: props.id,
            index: props.index,
        }
    }
}

const tileTarget = {
    drop(props, monitor, component) {
        let draggedItem = monitor.getItem()

        const dragIndex = draggedItem.index
        const hoverIndex = props.index
        // if (draggedItem.origIndex !== hoverIndex)
        //     props.moveTile(draggedItem.id, draggedItem.origIndex, hoverIndex)

    },

    hover(props, monitor, component) {
        let draggedItem = monitor.getItem()

        // Ignore tag hover, covered by css
        if (draggedItem.tag !== undefined) {
            return
        }

        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            // return
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            // return
        }

        props.hoverTile(dragIndex, hoverIndex)

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex
    }
}

@DropTarget([DndItemTypes.ACTIVITY_TILE], tileTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@DragSource(DndItemTypes.ACTIVITY_TILE, tileSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
}))
export default class CommentHighlights extends Component {
    static propTypes = {
        connectDragSource: PropTypes.func,
        connectDropTarget: PropTypes.func,
        connectDragPreview: PropTypes.func,
        index: PropTypes.number,
        isDragging: PropTypes.bool,
        isOver: PropTypes.bool,
        canDrop: PropTypes.bool,
        item: PropTypes.any,
        id: PropTypes.any,
        moveTile: PropTypes.func,
        hoverTile: PropTypes.func}

    constructor(props) {
        super(props)
    }

    render() {
        const project = projectStore.currentProject

        const {
            id,
            index,
            total,
            events,
            pct,
            isDragging,
            isOver,
            item,
            connectDragSource,
            connectDropTarget,
        } = this.props

        // How shall we determine which comments are most highlight worthy?
        // They're not voted on. Could be based on most active people?
        // Let's just start with most recent.
        events.sort( (e1,e2) => {
            return e2.dateModified - e1.dateModified
        })
        const limit = Math.min(MAX_EVENTS, events.length)
        const recentComments = events.slice(0,limit)

        let user, card

        return connectDragSource (
            connectDropTarget (
                <div className='activityTile row2Dbl' style={{width: pct+'%'}}>
                    <h3>{total > 0? total:''} Card Comments{/*<Move size={20} className='right third hidden'/>*/}</h3>
                    <div className='actBody subtle-scroll-dark' style={{height: '90%'}}>
                        {total === 0 &&
                        <div className='noData'>
                            No new comments since&nbsp;
                            <Moment format='h:mma' >{this.startDate}</Moment>, <Moment format='MMMM D'>{this.startDate}</Moment>.
                        </div>
                        }

                        <div className='stat'>
                            {recentComments.map (event => {
                                user = userStore.getUserFromConnections(event.userId)
                                card = event.card || cardStore.getCardFromCurrent(event.cardId)
                                if (!card || !user) {
                                    console.warn('Comments: card or user not found. Card could have been deleted since comment! cardId: ' +event.cardId +', userId: ' +event.userId)
                                    return null
                                }

                                return (
                                    <div>
                                        <div className='rankedUser inline'>
                                           <img src={user.image} className='userImg smaller'/>
                                           {/*<span className='name'>{user.displayName}:</span>*/}
                                        </div>
                                        <div className='actCard smaller' >
                                           <div className='thumb' style={{backgroundImage: 'url(' + (card.image) + ')'}}>
                                               &nbsp;
                                           </div>
                                        </div>
                                        <div className='actCmt inline'>
                                            "{event.detail}"
                                        </div>
                                    </div>
                                )
                            })}
                        </div>


                    </div>
                </div>
            )
        )
    }
}