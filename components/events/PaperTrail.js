import {Component} from 'react'

import userStore from "../../store/userStore";
import {CardChangeEvents, getIcon, getMiniMsg, ProjectUpdate, ProjectView} from "../../config/EventConfig";
import cardStore from "../../store/cardStore";
import eventStore from '/store/eventStore'

import EventDetail from "./EventDetail";
import ArrayUtils from "../../utils/ArrayUtils";
import isAutoMsg from "../../utils/solo/isAutoMsg";
import {X} from "react-feather";
import {ProjectType} from "../../config/Constants";

const MAX_EVENTS = 100


export default class PaperTrail extends Component {

    close = (ev) => {
        this.props.close()
    }

    goBack = (ev) => {
        this.props.goBack()
    }

    render() {
        const {project, types} = this.props
        if (!project)
            return null

        let user, friend, card, msg, icon, className, isNew, requiresCard
        let allEvents = (eventStore.getInstance().allEventHistory[project.id] || [])
        allEvents = ArrayUtils.isAscendyByDate(allEvents, 'dateModified')?
                    allEvents.reverse() : allEvents
        if (types && types.length >= 0) {
            allEvents = allEvents.filter (ev => types.indexOf(ev.type) !== -1) || []
        }
        const projectTypeName = (project && project.projectType === ProjectType.APPROVAL)? 'approval request':'project'


        // const nonViewCount = allEvents? (allEvents.filter( ev => ev.type !== ProjectView) || []).length : 0

        return (
            <div style={{height: '99%'}} className='jSideBG subtle-scroll-dark'>
                <X size={24} onClick={this.close} className='vaTop pointer abs' style={{top: '1rem', right: '1rem'}}/>

                <div className='bold pad1'>
                    Paper Trail
                </div>

                {(!allEvents || allEvents.length === 0) &&
                    <div className='small light mt1'>
                        Activity in this project will appear here.
                    </div>
                }

                <div style={{overflowY: 'auto'}}>
                {allEvents.map( (event, i) => {
                    user = userStore.getUserFromConnections(event.userId)
                    if (!user)
                        debugger

                    if (event.friendId)
                        friend = userStore.getUserFromConnections(event.friendId)

                    msg = getMiniMsg(event, 1)
                    if (event.type === ProjectView) {
                        if (!user) // unhelpful
                            return null
                        else
                            msg += (' this ' + projectTypeName)
                    }

                    if (isAutoMsg(user, msg))
                        return null

                    card = event.card || event.cardId? cardStore.getCardFromCurrent(event.cardId) : null
                    requiresCard = CardChangeEvents.indexOf(event.type) !== -1
                    if (requiresCard && !card) {
                        debugger
                        return null
                    }

                    isNew = false // this.isEventNew(event, unseenEvents)
                    // if (isNew)
                    //     debugger

                    icon = getIcon(event)
                    className = isNew? 'event new' : 'event old'

                    if (!user)
                        debugger

                    if (!msg && event.type !== ProjectView) {
                        debugger
                        return null
                    }

                    return(
                        <EventDetail
                            type={event.type}
                            user={user}
                            friend={friend}
                            card={card}
                            project={event.project}
                            isNew={isNew}
                            close={this.close}
                            className={className}
                            msg={msg}
                            icon={icon}
                            count={1}
                            date={event.dateModified}
                            idx={i}
                            key={'evm'+i}
                        />
                    )
                })}
                </div>
            </div>
        )
    }
}