import {Component} from 'react'
import PropTypes from 'prop-types'

export default class Stub extends Component {
    static propTypes = {
        a: PropTypes.any,
        b: PropTypes.bool,
        person: PropTypes.instanceOf('UserModel'),
        c: PropTypes.func
    }

    constructor(props) {
        super(props)
    }

    render() {
        return(
            <div>stub</div>
        )
    }
}