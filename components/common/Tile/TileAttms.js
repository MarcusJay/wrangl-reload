import {Component} from 'react'

import {getExt} from "../../../utils/solo/getExt";
import {extToIcon, isPreviewable} from "../../../config/FileConfig";
import CardSlideShowModal from "../../modals/CardSlideShowModal";
import AttmThumb from "../../unmodals/AttachmentsPane/AttmThumb";

export default class TileAttms extends Component {

    constructor(props) {
        super (props)
    }

    openAttachment(id) {

    }


    render() {
        const {card, titles, overlays} = this.props
        let ext, iconName, preview

        return (
            <span className='attmsInTile'>
                {card.attachments.map ((attm, i) => {
                    ext = getExt (attm.url)
                    iconName = extToIcon (ext)
                    preview = isPreviewable (ext)
                    return (
                        <CardSlideShowModal cardId={this.props.card.id} trigger={
                            <AttmThumb file={attm} ext={ext} iconName={iconName}
                                       openAttm={(id) => this.openAttachment (id)}
                                       preview={preview} className={'attmInTile'}
                                       tip=' ' iTitle={false} iOverlay={false}
                            />
                        } attmId={attm.attachment_id}
                        />
                    )
                })}
            </span>
        )

    }
}