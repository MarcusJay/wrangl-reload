import {Component} from 'react'
import {findDOMNode} from 'react-dom'
import {Dimmer, Grid, Loader} from 'semantic-ui-react'

import Dropzone from 'react-dropzone'

import uiStateStore from '/store/uiStateStore.js'
import CardModel from '/models/CardModel'
import CardSizeSlider from "/components/common/CardSizeSlider";

import cardStore from '/store/cardStore'
import {requestPreviews, sendImage} from "../../services/ImageFileService";
import PreviewCard from "./PreviewCard";
import ResponsiveUtils from "../../utils/ResponsiveUtils";

export default class UploaderComponent extends Component {
    constructor(props) {
        super(props)

        let fileRefs

        this.state = {
            elem: null,
            uploading: false,
            uploadCount: 1,
            uploadProgress: 0,
            importing: false,
            selectedCard: new CardModel(),
            cards: []
        }

        this.isFinalUpload = false // out of state, because requires synchronous set

        this.handleChange = this.handleChange.bind(this)
        this.handleUrlChange = this.handleUrlChange.bind(this)
        this.saveChanges = this.saveChanges.bind(this)
        this.showUploadProgress = this.showUploadProgress.bind(this)

    }

    onDrop = (files) => {
        this.fileRefs = files
        if (this.props.imageOnly && this.props.card && files && files.length > 0) {
            return this.uploadImageToCard(files[0])
        }

        let card, cards = []
        files.forEach(file => {
            card = CardModel.FromFileUpload (file)
            uiStateStore.fileCardMap[file] = card
            cards.push(card)
        })

        this.setState({
            cards: cards
        }, () => {
            // TODO !! release object urls only after api upload is complete.
            // files.forEach( file => window.URL.revokeObjectURL(file.preview))

        });
    }

    uploadImageToCard(file) {
        const {card} = this.props
        this.setState ({uploading: true})
        sendImage(file, false, this.showUploadProgress.bind(this), card.id, 1)
    }

    uploadAssets() {
        this.setState ({importing: true})
        cardStore.createCardsFromFiles(this.state.cards, this.props.currentProject).then(response => {
            this.newCards = response.data.cards
            cardStore.attachFilesToCards(this.newCards, this.fileRefs, this.showUploadProgress.bind(this))
        })
    }

    // Upload progresses will arrive asynchronously
    showUploadProgress(count, msg, imageUrl) {
        const {card, imageOnly} = this.props
        if (msg === 'end') {
            this.setState ({uploadCount: Math.max (count, this.state.uploadCount)})
            if (card && imageOnly && imageUrl) {
                cardStore.saveCard ({id: card.id, image: imageUrl}).then (card => {
                    card.image = imageUrl
                    this.setState ({uploading: false})
                }).catch (error => {
                    this.setState ({uploading: false, error: error})
                }).finally( () => {
                    // RELEASE RESOURCES
                    if (this.fileRefs)
                        this.fileRefs.forEach (file => window.URL.revokeObjectURL (file.preview))
                    if (this.props.onClose)
                        this.props.onClose()
                })
            }

            else if (count === this.state.cards.length) {
                requestPreviews(this.newCards)
                this.props.uploadAssets () // TODO rename to refreshCards
                // RELEASE RESOURCES
                if (this.fileRefs)
                    this.fileRefs.forEach (file => window.URL.revokeObjectURL (file.preview))

                this.props.onClose ()
            }
        }
        else if (count === 'start') {
            this.setState({uploading: true, importing: true})
        } else {
            console.log("Progress: count = " +count+ ", msg: " +msg)
        }
    }

    handleChange(ev) {
        const target = ev.target;
        const propValue = target.type === 'checkbox' ? target.checked : target.value;
        const propName = target.name;
        this.state.selectedCard[propName] = propValue
        // this.forceUpdate()
    }

    handleUrlChange(ev) {
        this.state.selectedCard.urls[0] = ev.target.value
        // this.forceUpdate()
    }

    saveChanges() {
        this.props.onSave(this.state.selectedCard)
    }

    selectCard(card) {
        this.setState({selectedCard: card})
    }

    componentDidMount() {
        this.setState({elem: findDOMNode(this)})
    }

    render() {
        const {imageOnly, wizMode, card} = this.props
        const { importing, uploading, cards, selectedCard, uploadCount } = this.state
        const device = ResponsiveUtils.getDeviceType()
        const mobile = ['computer','tablet'].indexOf(device) === -1
        let selectedCardCount = cards.length
        console.log("CardEditor RENDER: selected card is : " +selectedCard)

        if (selectedCard.title === 'Untitled')
            selectedCard.title = ''
        if (selectedCard.description === 'No Description')
            selectedCard.description = ''

        return(
            <div className={wizMode? '':"cardEditor"} >   {/*onKeyPress={(ev) => {this.handleKeyPress(ev)}}*/}
                <Dimmer.Dimmable dimmed={importing || uploading}>
                    <Dimmer active={importing} inverted>
                        <Loader style={{marginTop: '0', height: '10%'}}>
                            <span className='jFG'>Importing: {uploadCount} of {selectedCardCount}</span>
                        </Loader>
                    </Dimmer>
                    <Dimmer active={uploading} inverted>
                        <Loader style={{marginTop: '0', height: '10%'}}>
                            {!imageOnly &&
                            <span className='jFG'>Uploading images ({uploadCount} of {selectedCardCount})</span>
                            }
                            {imageOnly &&
                            <span className='jFG'>Uploading new image</span>
                            }
                        </Loader>
                    </Dimmer>

                    {!wizMode &&
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={6}>
                                <div className="tab-description">
                                    {imageOnly ? "Upload an image from your device" : "Add Items from your Desktop or Phone:"}
                                </div>
                            </Grid.Column>
                            {!imageOnly &&
                            <Grid.Column width={6}>
                                <button disabled={selectedCardCount === 0} className='jBtn mr05'
                                        onClick={this.uploadAssets.bind (this)}>
                                    Add {selectedCardCount > 1 ? selectedCardCount + ' Items' : selectedCardCount === 1 ? selectedCardCount + ' Item' : 'Items'}
                                </button>
                                <button className='jBtn mt05' onClick={this.props.onClose}>
                                    cancel
                                </button>

                            </Grid.Column>
                            }
                            {!mobile && !imageOnly &&
                            <Grid.Column width={4}>
                                <CardSizeSlider cardContainer="cardEditor" disabled={!cards || cards.length === 0}/>
                            </Grid.Column>
                            }
                        </Grid.Row>
                    </Grid>
                    }

                    <Grid >
                    <Grid.Row style={{padding: wizMode? 0:'1rem'}}>

                        <Grid.Column width={16} className="formPane">
                          <Dropzone onDrop={this.onDrop}
                                    multiple={wizMode || !imageOnly}
                                    className="upload-box" >  {/* accept='image/*' */}
                            <div className="browse-files">
                                <h1 className="thinH jFG" >Drop assets here</h1>
                                <div className="thinH2">Or click to browse...</div>
                            </div>
                          </Dropzone>
                        </Grid.Column>

                        {cards.length > 0 &&
                        <Grid.Column width={16} >
                            <div className="card-import-box">
                                <div className="browse-cards">
                                    {cards.map( (cardModel, i) => {
                                        return (
                                            <PreviewCard
                                                card={cardModel}
                                                containerName='cardEditor'
                                                containerElem={this.state.elem}
                                                mobile={mobile}
                                                key={'pc'+i}
                                                id={cardModel.id}
                                                text={cardModel.title}
                                                readOnly={true}
                                                // selectCard={this.toggleSelectCard}
                                                selectCard={this.selectCard.bind(this)}
                                                // selected={selectedCards[ cardModel.id ] !== undefined}
                                                moveCard={null}
                                                deleteCard={null}
                                            />
                                        )
                                    })}
                                </div>
                            </div>
                        </Grid.Column>
                        }
                    </Grid.Row>
                </Grid>
                </Dimmer.Dimmable>
            </div>
        )
    }
}

