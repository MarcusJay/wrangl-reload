import {Component} from 'react'
import {Camera, Crop, ZoomIn, ZoomOut} from 'react-feather'

export default class ImageEditor extends Component {
    constructor(props) {
        super(props)
    }


    render() {
        return(
            <div className="imageEditor">
                <Camera className="imageIcon"/>
                <Crop className="imageIcon"/>
                {/*<Feather.Move className="imageIcon"/>*/}
                <ZoomIn className="imageIcon"/>
                <ZoomOut className="imageIcon"/>
            </div>
        )
    }
}

