import {Component} from 'react'
import {observer} from 'mobx-react'
import {Button, Card as SCard} from 'semantic-ui-react'
import {Folder} from 'react-feather';
import uiStateStore from '/store/uiStateStore'
import ImageViewer from "../modals/ImageViewer";
import DropboxService from "../../integrations/DropboxService";
import {isUrl} from "../../utils/solo/isUrl";
import {getShortUrl} from "../../utils/solo/getShortUrl";

/*
import CardModel from '/models/CardModel'
import CardComments from './Card/CardComments'
import CardCommentsPreview from './Card/CardCommentsPreview'
import CardReaction from './Card/CardReaction'
import CardTagStrip from "./Card/CardTagStrip";
import CardTag from "./Card/CardTag";
import TagConfig from '/config/TagConfig'
import ImageModal from "/components/modals/ImageModal"
import ImageChooserModal from "/components/modals/ImageChooserModal"
*/

@observer
export default class PreviewCard extends Component {

    constructor(props) {
        super(props)

        // this.setImageHeight = this.setImageHeight.bind(this)

    }

    selectCard = (ev) => {
        this.props.selectCard(ev.target, this.props.card)
    }

    gotoLink(ev) {
        ev.stopPropagation()
        ev.preventDefault()
        debugger
        if (this.props.card.url)
            window.open(this.props.card.url )
    }

    deleteCard(ev) {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({showPopup: false})
        this.props.deleteCard(this.props.card.id, this.props.index)
    }

    componentWillReact(a,b,c) {
        console.log("### Preview Card will react. ", a, b, c)
    }

    render() {
        const { mobile, view, card, readOnly, disableTitle, disableOverlay, selected } = this.props

        // style
        let cardClass = view === 'image'? 'LeImage' : 'LeCard card side preview'
        cardClass += (selected? ' selected' : '')
        const mobileCardStyle = {backgroundImage: 'url('+card.image+ ')', overflow: 'hidden',
            width: '4rem', height: '4rem', border: selected? '2px solid white':'1px solid #555'}
        if (view === 'image')
            cardClass += ' tight'

        // size
        const size = uiStateStore.getCardSize(this.props.containerName) || uiStateStore.defaultPreviewCardSize
        // console.log("### My size is : " +size)
        const sizeStyle = {width: (size +'px'), height: (size * 1.25) +'px'}
        const showLinkBtn = card.url && isUrl(card.url) && !card.path

        // Dropbox stuff
        const previewData = card.image
        const isFolder = card.type === 'folder'
        const mimeType = (!isFolder && card.path)? DropboxService.getMimeType( card.path ) : ''
        const isImageInline = (!!card.path && !!mimeType)
        const hClass = (!!card.path)? 'thinH3 dontWrap' : 'thinH2 dontWrap'

        if (isFolder) {
            return(
                <div className="folder" onClick={() => this.openFolder() }
                style={{display: 'block', margin: '1rem'}}>
                    <Folder size={64} style={{display: 'block'}}/>
                    <div className="noWrap ctitle" style={{fontSize: '1rem'}}>{card.title}</div>
                </div>
            )
        }

        if (mobile) {
            return (
                <div className='coverBG cardRadius mb1 mr1 inline vaTop'
                     style={mobileCardStyle}
                     onClick={this.selectCard}>
                    {!card.image &&
                    <span className='micro jThird centered'>{card.title}</span>
                    }
                </div>
            )
        }

        if (view === 'card' || !view) {
            return (
                <div id={'c' + card.id} className={cardClass} style={sizeStyle} onClick={this.selectCard}>
                    {!disableOverlay &&
                    <div className="hoverlay">

                        {!isImageInline &&
                        <div className="title topActions">
                            {showLinkBtn &&
                            <Button inverted basic data-tip="Visit Site" onClick={(ev) => {
                                this.gotoLink (ev)
                            }}>
                                Open in new tab
                            </Button>
                            }
                        </div>
                        }

                        <div className="">
                            {card.description || card.title}
                        </div>

                        {/*
                        <div className="small dontWrap" onClick={(ev) => { this.gotoLink(ev) }} >
                            <a href={card.url}>
                                {card.url}
                            </a>
                        </div>
*/}
                    </div>
                    }

                    {!isImageInline &&
                    <ImageViewer image={card.image} title={card.title} size="preview" mobile={mobile}
                                 selected={this.props.selected} readonly={readOnly}
                    />
                    }

                    {isImageInline &&
                    <img className="img" src={"data:" + mimeType + ";base64," + previewData}
                         style={{height: '100%'}}/>
                    }

                    <div className='main content' style={{padding: '0.5rem 0.5rem 0.1rem 0.5rem' }}>
                        {size > 75 &&
                        <div>
                            {readOnly && !disableTitle &&
                            <div className={hClass} style={{textOverflow: 'ellipsis'}}>
                                {card.title}
                            </div>
                            }
                            {!readOnly && !disableTitle &&
                            <div className={hClass} contentEditable
                                 style={{textOverflow: 'ellipsis'}}
                                 suppressContentEditableWarning={true}
                                 name="title"
                                // onFocus={this.startEditing}
                                 onClick={this.startEditing}
                                 onBlur={this.finishEditing}
                                 onKeyPress={this.handleFieldKeyPress}
                            >
                                {card.title}
                            </div>
                            }
                        </div>
                        }

                        {size > 125 &&
                        <div>
                            {card.url &&
                            <SCard.Description className="dontWrap">
                                <a href={card.url}>
                                    {getShortUrl(card.url, true)}
                                </a>
                            </SCard.Description>
                            }
                            {false && card.description &&
                            <SCard.Description className="desc small " style={{margin: 0}}>
                                {card.description || card.assetLink}
                            </SCard.Description>
                            }
                        </div>
                        }
                    </div>
                </div>
            )
        }

        else if (view === 'image') {
            return (
                <div id={'i' + card.id} className={cardClass} onClick={(ev) => this.selectCard (ev)}>
                    <img src={card.image} className={this.props.selected ? 'selected' : ''}
                         style={{maxWidth: '90%', maxHeight: '90%'}}/>
                </div>

            )
        }
    }
}


