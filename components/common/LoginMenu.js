import {Component} from 'react'
import {Router} from '~/routes'
import {Button} from 'semantic-ui-react'

export default class LoginMenu extends Component {
    constructor(props) {
        super(props)
    }

    gotoLogin() {
        Router.pushRoute('/login', {id: this.props.project.id})
    }

    gotoSignup() {
        Router.pushRoute('/signup', {id: this.props.project.id})
    }

    render() {
        return(
            <div >
                <Button basic className='' style={{marginRight: '1rem'}} onClick={() => this.gotoLogin()}>Login</Button>
                <Button className='button primary go' style={{marginRight: '1rem'}} onClick={() => this.gotoSignup()}>Sign up</Button>
            </div>
        )
    }
}