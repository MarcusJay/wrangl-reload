import {Component} from 'react'
import PropTypes from 'prop-types'
import {Popup} from 'semantic-ui-react'

import projectStore from "/store/projectStore";
import LoginOrSignup from "../Account/LoginOrSignup";

export default class Stub extends Component {
    static propTypes = {
        component: PropTypes.any,
        isAnon: PropTypes.bool
    }

    state = {authOpen: false}

    handleOpen = (ev) => {
        debugger
        console.log('handleOpen')
        ev.stopPropagation()
        this.setState({authOpen: true})
    }

    handleClose = (ev) => {
        this.setState({authOpen: false})
    }


    render() {
        const project = projectStore.currentProject

        if (!this.props.isAnon)
            return this.props.component
        else
            return (
                <Popup
                    open={this.state.authOpen}
                    onOpen={this.handleOpen}
                    onClose={this.handleClose}
                    on='click'
                    position='bottom left'
                    trigger={<span className='pointer' onClick={this.handleOpen} onKeyPress={this.handleOpen}>
                                <span className='readOnly'>{this.props.component}</span>
                            </span>}>

                    <div className='inline'>
                        <h1>Welcome, Wrangler!</h1>
                        <span>To vote, comment, and create your own Boards, just sign up! It only takes a few seconds.</span>
                        <LoginOrSignup embedded={true} show='signUp' projectId={project? project.id : '0'}/>
                    </div>

                </Popup>

            )
    }
}