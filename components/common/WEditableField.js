import React, {Component} from 'react'
import {findDOMNode} from 'react-dom'
import {observer} from 'mobx-react'
import {Edit2, Link} from 'react-feather'
import {FieldLengths} from "/config/Constants";
import {Loader} from "semantic-ui-react";

@observer
export default class WEditableField extends Component {
    constructor(props) {
        super(props)
        this.state = {editMode: false, prevClick: null, showPopup: false, loading :false}
    }

    showOverlay(ev) {
        this.setState({showPopup: true})
    }

    hideOverlay(ev) {
        this.setState({showPopup: false})
    }

    setEditable(editable) {
        console.log('setEditable: ' +editable)
        debugger
        this.elem.contentEditable = editable
        if (editable)
            this.elem.click()
        this.setState({editMode: editable})
    }

/*
    handleClick(ev) {
        // ev.stopPropagation()
        // ev.preventDefault()
        const now = new Date()
        console.log('click: ' +now+ '. Double = ' +now+ ' - ' +this.state.prevClick+ ' = ' +(Math.abs(this.state.prevClick - now))+ ' = ' +Utils.isDoubleClick(this.state.prevClick, now))
        if (Utils.isDoubleClick(this.state.prevClick, now)) {
            this.setState ({editMode: true})
            this.elem.contentEditable = true
            this.elem.click()
            this.startEditing(ev)
        } else {
            this.setState ({prevClick: now, editMode: false})
        }
    }
*/

    startEditing(ev) {
        ev.stopPropagation()
        ev.preventDefault()

        let fieldName = this.props.name // ev.target.name || ev.target.getAttribute !== undefined? ev.target.getAttribute('name') : null
        if (!fieldName) {
            debugger
            throw new Error('startedit: still event target issues')
        }
    }

    handleFieldKeyPress(ev) {
        let fieldName = this.props.name // this.state.editField // ev.target.name || ev.target.getAttribute !== undefined? ev.target.getAttribute('name') : null
        if (!fieldName) {
            debugger
            throw new Error('keypress: still event target issues')
        }
        let fieldValue = ev.target.value || ev.target.innerText

        if (fieldValue.length === FieldLengths[fieldName]) {
            ev.preventDefault()
            return
        }

        if (ev.key === 'Enter') {
            this.finishEditing(ev)
        }
    }

    finishEditing(ev) {
        let self = this

        // In card, editable fields do not have a defined target.name or value. Elsewhere, they do.
        let fieldName = this.props.name // this.state.editField
        var fieldValue = ev.target.value || ev.target.innerText
        if (!fieldName) {
            throw new Error("finishEditing: missing fieldName or value")
        }

        if (!fieldValue || fieldValue.length === 0)
            ev.target.value = fieldName

        ev.stopPropagation()
        ev.preventDefault()
        ev.target.blur()

        if (fieldValue === this.props.parent[fieldName]) {
            console.log('Skipping save: value is unchanged.' )
            this.setState({editMode: false})
            return
        }


        console.log("Requesting Save")
        let parent = {id: this.props.parent.id}
        parent[fieldName] = fieldValue || ""
        // this.elem.contentEditable = false
        this.setState({editMode: false})
        this.props.saveParent(parent)
    }

    gotoLink(ev) {
        ev.stopPropagation()
        ev.preventDefault()
        this.elem.blur()
        ev.target.blur()
        window.open(this.elem.innerText)
    }


    componentDidMount() {
        this.elem = findDOMNode(this)
    }

    render() {
        // console.log('Editable: ' +this.state.editMode)
        const { inline, name, style } = this.props
        const singleClickEdit = (name !== 'url')
        const defaultStyle = {display: inline? 'inline': 'block', textOverflow: 'ellipsis', position: 'relative', cursor: 'pointer'}
        const combinedStyle = Object.assign(defaultStyle, style)

        return (
            <div contentEditable={true}
                 className={this.props.className}
                 style={combinedStyle}
                 suppressContentEditableWarning={true}
                 onClick={(ev) => this.startEditing(ev)}
                 onMouseEnter={(ev) => this.showOverlay(ev)}
                 onMouseLeave={(ev) => this.hideOverlay(ev)}
                 onFocus={(ev) => this.startEditing(ev)}
                 onBlur={(ev) => this.finishEditing(ev)}
                 onKeyPress={(ev) => this.handleFieldKeyPress(ev)}
            >
                <Loader active={this.state.loading}>Saving</Loader>
                {this.props.value || ' '}
                {this.state.showPopup && this.props.name === 'url' &&
                <div className='fieldPopup'>
                    <Edit2 className='icon' size={16} onClick={(ev) => this.setEditable(true)}/>
                    <Link className='icon' size={16} onClick={(ev) => this.gotoLink(ev)}/>
                </div>
                }
            </div>
        )
    }
}

/*
    // deprecate
    handleFieldChange(ev) {
        if (ev.type === 'blur') {
            this.setState({editField: null})
            return
        }
        ev.stopPropagation()
        ev.preventDefault()

        // In card, onChange for editable fields is missing name
        let fieldName = this.state.editField
        let fieldValue = ev.target.value || ev.target.innerText

        if (!fieldName) {
            debugger
            throw new Error("handleFieldChange: missing fieldName or value")
        }

        // special case for array/collection fields. TODO expand for multiple urls. Pass in an isArray and index perhaps.
        // if (fieldName === 'urls')
        //     this.props.card.urls[0] = fieldValue
        // else
        this.props.card[fieldName] = fieldValue || ""
    }
*/
