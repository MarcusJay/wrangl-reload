import React, {Component} from 'react'
import {observer} from 'mobx-react'
import ReactPlayer from 'react-player'
import {Icon, Menu, Popup, TextArea} from 'semantic-ui-react'
// import Editable from 'react-contenteditable'
import {ArrowUpRight, CheckCircle, Circle} from 'react-feather';
import PropTypes from 'prop-types'
import {findDOMNode} from 'react-dom'
import {DragSource, DropTarget} from 'react-dnd'

import {DndItemTypes} from '/config/Constants'

import CardModel from '/models/CardModel'
// import CardReaction from './Card/CardReaction'
import CardTagStrip from "./Card/CardTagStrip";
import ImageChooserModal from "/components/modals/ImageChooserModal"
import AuthProtector from '/components/common/AuthProtector'
import userStore from '/store/userStore'
import tagStore from '/store/tagStore'
import uiStateStore, {CardAttachments} from '/store/uiStateStore'
import {getShortUrl} from '/utils/solo/getShortUrl'

import StateChangeRequestor, {
    REQUEST_CARD_DELETE_TAG,
    REQUEST_CARD_REACT,
    REQUEST_CARD_SAVE,
    REQUEST_CARD_UNREACT
} from '/services/StateChangeRequestor'
import ImageViewer from "../modals/ImageViewer";
import EmbedUtils from "/utils/EmbedUtils"
import EmbedTrigger from "./EmbedTrigger"
import cardStore from "../../store/cardStore";
import CardSlideShowModal from "../modals/CardSlideShowModal";
import {addLineBreaks} from "../../utils/solo/addLineBreaks";
import {firstWord} from "../../utils/solo/firstWord";
import CardReactions from "./Card/CardReactions";
import {extToIcon, isPreviewable, sourceToIcon} from "../../config/FileConfig";
import MoreHoriz from "./MoreHoriz";
import {UserStatus} from "../../config/Constants";
import {getCardDetails} from "../../services/CardApiService";
import DumbCapsule from "./DumbCapsule";
import {reverseTruncate} from "../../utils/solo/reverseTruncate";
import {MARK_AS_READ_TIMEOUT} from "../../config/EventConfig";
import markAsViewed from "../../utils/solo/markAsViewed";
import isInViewport from "../../utils/solo/isInViewport";
import {getExt} from "../../utils/solo/getExt";
import AttmThumb from "../unmodals/AttachmentsPane/AttmThumb";
// import ColorThief from 'color-thief'

// card sizes
const SIZE_LARGE = 'large'
const SIZE_FIT = 'fit' // fit to screen size (with same proportions)
const SIZE_DEFAULT = 'default'

let origIndex = -1
const cardSource = {
    beginDrag(props) {
        uiStateStore.enableFileDrops = false
        origIndex = props.index
        // props.prepareMove()
        return {
            id: props.card.id,
            type: DndItemTypes.CARD,
            stackId: props.stackId || null,
            index: props.index,
            origIndex: origIndex // seems redundant, but isn't: index will be overwritten by final props.index, origIndex will not.
        }
    },
    endDrag(props, monitor) {
        uiStateStore.enableFileDrops = true
        let finalIndex = props.index
        console.error("### final index: " +finalIndex)
        return {
            id: props.card.id,
            stackId: props.stackId || null,
            type: DndItemTypes.CARD,
            index: props.index,
            origIndex: origIndex
        }
    }
}

// let destIndex = -1
const cardTarget = {
    drop(props, monitor, component) {
        let draggedItem = monitor.getItem()

        // Tag drop
        if (draggedItem.tag !== undefined) {
            let card = props.card

            tagStore.tagCard(draggedItem.id, props.card.id).then( updatedCard => {
                props.card.tags2 = updatedCard.tags2
                component.forceUpdate ()
            })

/*
            StateChangeRequestor.awaitStateChange (REQUEST_CARD_ADD_TAG, {
                cardId: props.card.id,
                tagId: draggedItem.id
            }).then (tags2 => {
                props.card.tags2 = tags2 // .data.card_tags.split('|')
                component.forceUpdate ()
            })
*/

        // Card drop
        }
        else {
            const dragIndex = draggedItem.index
            const hoverIndex = props.index
            // Time to actually perform the action
            if (draggedItem.origIndex !== hoverIndex && props.moveCard)
                props.moveCard(draggedItem.id, draggedItem.origIndex, hoverIndex)
            // else
            //     props.cancelMove()
        }

    },

    hover(props, monitor, component) {
        let draggedItem = monitor.getItem()

        // Ignore tag hover, covered by css
        if (draggedItem.tag !== undefined) {
            return
        }

        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top

        // Only perform the move when the mouse has crossed half of the items height
        // When dragging downwards, only move when the cursor is below 50%
        // When dragging upwards, only move when the cursor is above 50%

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            // return
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            // return
        }

        if (props.hoverCard)
            props.hoverCard(dragIndex, hoverIndex)

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex
    }
}


@DropTarget([DndItemTypes.CARD, DndItemTypes.TAG], cardTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@DragSource(DndItemTypes.CARD, cardSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
}))
@observer
export default class Card extends Component {
    static propTypes = {
        connectDragSource: PropTypes.func,
        connectDropTarget: PropTypes.func,
        connectDragPreview: PropTypes.func,
        index: PropTypes.number,
        isDragging: PropTypes.bool,
        isOver: PropTypes.bool,
        canDrop: PropTypes.bool,
        item: PropTypes.any,
        id: PropTypes.any,
        text: PropTypes.string,
        moveCard: PropTypes.func,
        // cancelMove: PropTypes.func,
        // prepareMove: PropTypes.func,
        hoverCard: PropTypes.func,
        selectCard: PropTypes.func,
        deleteCard: PropTypes.func,
        filtered: PropTypes.bool
    }

    constructor(props) {
        super(props)
        this.state = {
            isFavorite: false,
            isExpanded: false,
            showComments: false,
            activeDropdownItem: '',
            showInfo: false,
            prevClick: null,
            showPopup: false,
            showCommentsPopup: false,
            showCardActions: false,
            myViewOnly: 'cards',
            // yes these change initialization are necessary
            cardIsNew: false,
            cardIsUpdated: false,
            imageChanged: false,
            titleChanged: false,
            descChanged: false,
            urlChanged: false,
            commentsChanged: false,
            attmsChanged: false,
            // unseenShown: false,
            nWhatsNew: 0,
            showBell: false,
            editingTitle: '', editingDesc: ''
        }
        // this.state.view = this.props.view || 'cards' // necessitated by .. see TODO at top

        this.handleMenuItemSelect = this.handleMenuItemSelect.bind(this)
        this.handleImageChange = this.handleImageChange.bind(this)
        this.setImageHeight = this.setImageHeight.bind(this)
        // this.highlightUnseenChanges(state)

    }

    clickCard = (event) => {
        this.props.selectCard(event.target, this.props.card)
    }

    hoverCard(isHover) {
        this.setState({isHover: isHover})
    }

    clickFullComments(ev) {
        // this.setState({view: 'cards'})
        uiStateStore.setCardView('cards')
    }

    // tag card is above in dropTarget
    onUnTagCard(tagId) {
        StateChangeRequestor.awaitStateChange (REQUEST_CARD_DELETE_TAG, {
            cardId: this.props.card.id,
            tagId: tagId
        }).then (updatedCard => {
            this.props.card.tags2 = updatedCard.tags2 // .data.card_tags.split('|')
            this.forceUpdate ()
        })

    }

/*
    openExpandedCard(expandedView) {
        if (this.props.readOnly)
            return
        let self = this
        this.origView = this.state.view
        this.origSize = this.state.elem ? this.state.elem.offsetWidth : uiStateStore.getCardSize (this.props.containerName) || uiStateStore.defaultCardSize
        this.setState ({showPopup: false})
        if (!expandedView)
            expandedView = this.state.view
        this.setCardSize (SIZE_FIT)
        this.props.onExpand(this.props.card.id)
        if (EmbedUtils.isEmbeddable (this.props.card.url)) {
            this.setState ({showEmbed: true, isExpanded: true, view: 'cards'})
        } else {
            getCardDetails (this.props.card.id).then (cardDetails => {
                let cardModel = new CardModel (cardDetails)
                Object.assign (this.props.card, cardModel) // TODO sin? merging in new properties to existing card
                self.setState ({isExpanded: true, view: expandedView}, () => ReactTooltip.rebuild ())
            })
        }
    }

    closeExpandedCard() {
        if (!this.state.isExpanded)
            return

        let self= this
        this.setCardSize(SIZE_DEFAULT)
        this.props.onShrink(this.props.card.id)
        this.setState({showComments: false, isExpanded: false, view: this.origView}, function() {
            self.refreshCard()
        })

    }
*/

/*
    flipCard(event) {
        event.stopPropagation(event)
        event.preventDefault()
        this.setState({showInfo: true})
        let flipElem = document.getElementById("flip" +this.props.card.domId)
        flipElem.style.transform = 'rotateY(180deg)';
    }
    unflipCard(event) {
        event.stopPropagation()
        event.preventDefault()
        this.setState({showInfo: false})
        let flipElem = document.getElementById("flip" +this.props.card.domId)
        flipElem.style.transform = 'none';
    }
*/

    onPopupOpen(ev) {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({showPopup: true})
    }

    onPopupClose(ev) {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({showPopup: false})
    }

/*
    onCommentPreviewOpen(ev) {
        this.setState({loadingCard: true})
        // fetch comments via cardRead
        getCardDetails(this.props.card.id).then(cardDetails => {
            this.setState ({loadingCard: false})
            let cardModel = new CardModel(cardDetails)
            Object.assign(this.props.card, cardModel)
        })
    }

    onCommentPreviewClose(ev) {
        this.setState ({showCommentsPopup: false})
    }
*/

    handleMenuItemSelect(ev, { name }) {
        ev.stopPropagation()
        ev.preventDefault()
        if (name.indexOf("Delete") === 0)
            this.deleteCard()
        this.setState({ activeDropdownItem: name })
    }


    toggleIsFavorite(event) {
        event.stopPropagation()
        this.setState({isFavorite: !this.state.isFavorite})
    }

    setCardSizeDELEte(sizeName) {
        let cardElem = findDOMNode(this)
        let cardsPane = this.props.containerElem
        let containerHeight = this.props.containerHeight
        // let semanticCardElem = cardsPane.querySelector('#sem'+this.props.card.domId)
        // console.log('SetSize: cardElem: ' +cardElem.getAttribute('id')+ ', semantic: ' +semanticCardElem.getAttribute('id'))
        // let currentWidth = cardElem.style.width.length > 0? parseInt(cardElem.style.width) : cardElem.offsetWidth
        // let currentHeight = cardElem.style.height.length > 0? parseInt(cardElem.style.height) : cardElem.offsetHeight
        let newWidth, newHeight
        switch(sizeName) {
            case SIZE_DEFAULT:
                newWidth = this.origSize
                newHeight = this.origSize * 1.15
                break;
            case SIZE_FIT:
                // this.setState({unexpandedWidth: currentWidth}, () => {
                newWidth = (cardsPane.clientHeight * 0.85) / 1.15
                newHeight = cardsPane.clientHeight * 0.95
                // })
                break;
            // no default plz
        }
        console.log("-- Setting card size to ", newWidth, 'x', newHeight)
        cardElem.style.width = newWidth+'px'
        cardElem.style.height = newHeight+'px'
        // semanticCardElem.style.width = newWidth+'px'
        // semanticCardElem.style.height = newHeight+'px'

    }

    setImageSize() {
        let img = document.querySelector('#img'+this.props.card.domId)
        let imgWidth = img.offsetWidth
        let imgHeight = img.offsetHeight
        let aspect = imgWidth / imgHeight
        console.log("aspect ratio: " +aspect)

        // landscape
        if (imgWidth > imgHeight) {
            img.style.width = '100%'
            // img.style.height = (imgHeight * aspect) + 'px'
        }
        // portrait
        else {
            img.style.height = '100%'
            // img.style.width = (imgWidth / aspect) + 'px'
        }
        // this.setImageBackground(img)
    }

    setImageBackground(imgElem) {
        // let elem = findDOMNode(this)
        // let container = elem.querySelector('.cardImage')
        // let colorThief = new ColorThief()
        // let color = colorThief.getColor(imgElem)
        // container.style.backgroundColor = color;
    }

    // TODO phase this out. use fg images. for expanded views, we can adjust image size based on what else is on card
    setImageHeight() {
        switch (this.props.card.totalComments) {
            case 0: return '55%'
            case 1: return '50%'
            case 2: return '44%'
            default: return '39%'
        }
    }

    gotoLink = (ev) => {
        ev.stopPropagation()
        ev.preventDefault()
        if (this.props.card.url)
            window.open(this.props.card.url )
    }

    gotoAttm = (attmUrl) => {
        if (attmUrl)
            window.open(attmUrl)
    }

    openMyNotesOnly(ev) {
        ev.stopPropagation()
        ev.preventDefault()
        getCardDetails(this.props.card.id).then(cardDetails => {
            // let cardModel = new CardModel (cardDetails)
            this.props.card.comments = cardDetails.comments
            this.setState({myViewOnly: 'comments'})
        }).catch( error => {
            console.error("calling card_read on card " +this.props.card.id+ ": ", error)
            this.props.card.comments = []
            this.setState({myViewOnly: 'comments'})
        })
    }

    closeMyNotesOnly(ev) {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({myViewOnly: 'cards'})
    }

    addAttachments(ev) {
        cardStore.getCardById(this.props.card.id).then( card => {
            uiStateStore.setEditingCard(card)
            uiStateStore.unModal = CardAttachments
        })
    }

    openAttachment(id) {
        let a = 1
    }

    refreshCard() {
        getCardDetails(this.props.card.id).then(cardDetails => {
            let cardModel = new CardModel (cardDetails)
            Object.assign (this.props.card, cardModel) // TODO sin: merging in new properties to existing card
            this.forceUpdate()
        })
    }

    deleteCard(ev) {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({showPopup: false})
        this.props.deleteCard(this.props.card.id, this.props.index)
    }

    previousCard() {
        this.props.previous()
    }

    nextCard() {
        this.props.next()
    }

    onEditorClose = () => {

    }

    // Image may have been uploaded or specified by url. Either way, we have its new hosted url.
    handleImageChange(image) {
        this.props.card.image = image
        if (image) {
            StateChangeRequestor.awaitStateChange (REQUEST_CARD_SAVE,
                {card: {id: this.props.card.id, image: image}}).then (updatedCard => {
                this.props.card.image = updatedCard.image
                this.setState (this.state) // force update
            })
        }
    }

    handleTextChange = (ev) => {
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon) {
            uiStateStore.setJoinPromptState(true)
            return
        }

        const {card} = this.props
        const propName = ev.target.name
        const propValue = ev.target.value
        card[propName] = propValue
        this.setState({[propName]: ev.target.value})
    }

    handleTextSave = (ev) => {
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon) {
            uiStateStore.setJoinPromptState(true)
            return
        }
        const {card, onTextSave} = this.props
        const {editingTitle, editingDesc} = this.state
        // const propName = ev.target.name
        // const propValue = ev.target.value
        // card.title = editingTitle
        // card.desc = editingDesc
        onTextSave(card)
        ev.target.blur()
    }

    handleTextClick = (ev) => {
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON
        if (isAnon)
            uiStateStore.setJoinPromptState(true)
    }

    handleKeyPress = (ev) => {
        if (ev.key === 'Enter') {
            ev.preventDefault()
            ev.stopPropagation()
            this.handleTextSave (ev)
        }
    }

    toggleReaction(reactionType, isActive, reaction) {

        let request = isActive ? REQUEST_CARD_UNREACT : REQUEST_CARD_REACT

        if (request === REQUEST_CARD_UNREACT && !reaction)
            throw new Error ("toggleReaction: cannot unreact without a reaction")

        let requestData
        if (request === REQUEST_CARD_REACT) {
            requestData = {card: this.props.card, reactionTypeId: reactionType.reaction_type_id}
        } else {
            requestData = {
                card: this.props.card,
                reactionTypeId: reactionType.reaction_type_id,
                reactionId: reaction.reaction_id
            }
        }
        StateChangeRequestor.awaitStateChange (request, requestData).then (response => {
            this.refreshCard ()
        }).catch (error => {
            console.error ("Error reacting to card: ", error)
            throw new Error (error)
        })
    }

    // check for unseen events of the type we're responsible for animating, and set related states accordingly
    // TODO this will be replaced with new currentBadges
    highlightUnseenEvents = (eventMap) => {
        if (!eventMap)
            return

        const card = this.props.card

        let showBell = false

        // TODO updatedKey is populated in realtime, but not in unseen. Must check both. TODO: Get api to support 1 100%
        const imageChanged = eventMap.updateEvents.some( event => event.detail === card.image || event.updatedKey === 'card_image')
        const titleChanged = eventMap.updateEvents.some( event => event.detail === card.title || event.updatedKey === 'card_title')
        const descChanged = eventMap.updateEvents.some( event => event.detail === card.description || event.updatedKey === 'card_description')
        const urlChanged = eventMap.updateEvents.some( event => event.detail === card.url || event.updatedKey === 'card_url')
        const attmsChanged = eventMap.updateEvents.some( event => event.detail && event.detail.indexOf('attachment') === 0)
        const commentsChanged = eventMap.commentEvents.length > 0
        const reactionsChanged = eventMap.reactionEvents.length > 0

        const cardIsNew = eventMap.isNew
        const cardIsUpdated = eventMap.updateEvents.length > 0 || commentsChanged || reactionsChanged

        console.log('Card ' +this.props.card.id+ ' highlighting unseen changes: ', imageChanged, titleChanged, descChanged, urlChanged)
        if (cardIsNew || cardIsUpdated || imageChanged || titleChanged || descChanged || urlChanged || commentsChanged || attmsChanged) {
            showBell = true
            // this.resetAnimation()
        }

/*
        if (state) {
            state.showBell = showBell
            state.cardIsNew = cardIsNew
            state.imageChanged = imageChanged
            state.titleChanged = titleChanged
            state.descChanged = descChanged
            state.urlChanged = urlChanged
            state.commentsChanged = commentsChanged
            state.attmsChanged = attmsChanged
            state.unseenShown = true
            // state.bscounter = uiStateStore.bscounter

        } else {
*/
        this.setState ({
            showBell: showBell,
            cardIsNew: cardIsNew,
            cardIsUpdated: cardIsUpdated,
            imageChanged: imageChanged,
            titleChanged: titleChanged,
            descChanged: descChanged,
            urlChanged: urlChanged,
            commentsChanged: commentsChanged,
            attmsChanged: attmsChanged
        })

    }

    resetAnimation() {
        const elem = this.state.elem
        if (!elem)
            return

        const semCard = elem.firstChild.firstChild // TODO Perhaps we can remove nested flippable/side and semantic card

    }

    // optional attm ID
    openCardViewer = (ev) => {
        // grab latest changes (e.g. annotations) before opening
        cardStore.getCardById(this.props.card.id).then(card => {
            this.props.openCardViewer(card)
        })
    }

    openCardEditor = (ev) => {
        // grab latest changes before opening
        cardStore.getCardById(this.props.card.id).then(card => {
            this.props.openCardEditor(card)
        })
    }

    componentDidMount() {
        const {card} = this.props
        const elem = findDOMNode(this)
        this.setState({elem: elem})
        if (isInViewport (elem)) {
            markAsViewed (card.id, 'card', null)
        }

    }

    componentWillReceiveProps(newProps) {
        // console.log('Card lifecycle: will receive props. id: '+this.props.card.id)

        if (this.props.showUnseenEvents)
            this.highlightUnseenEvents(newProps.events)

    }

    componentWillUnmount() {
        // console.log('Card lifecycle: will unmount. id: '+this.props.card.id)
    }

    shouldComponentUpdate(nextProps, nextState) {
        let result = uiStateStore.showWhatsNew || !this.props.readOnly || (nextProps.selected !== this.props.selected) || (nextState.isExpanded !== this.state.isExpanded)
        // console.log("Card lifecycle: Should Card " +this.props.card.title+ " update? " +result)
        return result
    }

    componentDidUpdate() {
        console.log('Card lifecycle: did update. desc: '+this.props.card.description)
        const {card} = this.props
        const {editingTitle, editingDesc} = this.state
        if (editingTitle !== card.title && editingDesc !== card.description) {
            this.setState({editingTitle: card.title, editingDesc: card.description})
            console.log('Card updated title and desc')
        }
        if (isInViewport (this.state.elem) && !this.state.dismissing) {
            this.setState({dismissing: true})
            markAsViewed (card.id, 'card', null)
        }

    }

    componentWillReact() {
        // console.log('Card lifecycle: will react. id: '+this.props.card.id)
    }

    render() {
        const {
            card,
            isNew,
            index,
            selected,
            readOnly,
            isOver,
            isDragging,
            canDrop,
            item,
            mobile,
            flash,
            // size,
            style,
            containerElem,
            isApproval,
            wkey,
            allowImageChange,
            showReactions,
            connectDragSource,
            connectDropTarget,
        } = this.props

        if (!card)
            return null

        const isActiveTarget = canDrop && isOver && item
        const { activeDropdownItem, cardIsNew, cardIsUpdated,
                imageChanged, titleChanged, descChanged, urlChanged, commentsChanged, attmsChanged,
                editingTitle, editingDesc} = this.state
        // console.log("Rendering Card " +this.props.card.title+ ", view: " +this.state.view+ ", url: " +this.props.card.url)
        const isHover = this.state.isHover // Note: isOver == dnd, isHover == cursor
        const showMyNotesOnly = this.state.myViewOnly === 'comments'

        const showBell = this.state.showBell

        // show changes
        const showChanges = true // !this.state.unseenShown // uiStateStore.bscounter !== this.state.bscounter || uiStateStore.showWhatsNew
        // const badgeColor = cardIsNew? '#f0592b' : cardIsUpdated? '#00a400' : '#777'
        const cardOutline = (cardIsNew || cardIsUpdated)? ' hasActivity':''
        const imgGlow = imageChanged && showChanges
        const titleGlow = titleChanged && showChanges? ' highlightGreen' : ''
        const descGlow = descChanged && showChanges? ' highlightGreen' : ''
        // const urlGlow = urlChanged && showChanges? ' highlightGreen' : ''
        // const commentGlow = commentsChanged && showChanges? ' highlightGreen' : ''
        // const cardToolsFade = attmsChanged && showChanges? ' shadowOrange' : ''
        const attmFlash = attmsChanged && showChanges? ' outlineOrange' : ''
        // const forceCardTools = attmsChanged && showChanges

        // if (this.state.imageChanged || this.state.titleChanged || this.state.descChanged || this.state.urlChanged || this.state.commentsChanged)
        //     console.log('Card: Showing Changes: ', cardGlow, imgGlow, titleFlash, descFlash, urlFlash, commentFlash)

        // style
        let cardClass = 'LeCard card side jFG cardBG ' + cardOutline + (selected? 'selected' : this.state.isExpanded? 'expanded' : '')
        // cardClass += (cardIsUpdated || cardIsNew)? ' cardFGinv cardBGinv':' jFG cardBG'

        if (index === 0)
            cardClass += ' utNewCard1'
        if (isActiveTarget && item.hasOwnProperty('tag'))
            cardClass += ' dropTarget'
        else if (isActiveTarget) // || isDragging)
            cardClass += " sortTarget"
        const showTags = uiStateStore.forceShowTags || this.props.showTags
        const stickyInfo = uiStateStore.stickyInfo
        const tagStripClass = 'inline dontWrap cardTagsMini hidden-scroll' + (showReactions? ' right':'')
        const shortUrl = card.url? getShortUrl(card.url, true) : null

        // TODO move size determination up to parent container.
        let size, sizeStyle
        if (mobile) {
            size = uiStateStore.cardsPane? uiStateStore.cardsPane.innerWidth : containerElem? containerElem.innerWidth : window? window.innerWidth - 75 : 325
            sizeStyle = {width: (size +'px')}
        } else {
            size = uiStateStore.getCardSize(this.props.containerName) || uiStateStore.defaultCardSize
            const ratio = 1.35 + (showReactions? .3 : 0) + (showTags? .2 : 0)
            sizeStyle = {width: (size +'px'), height: (size * ratio) +'px', opacity: isDragging? 0.1 : 1}
        }
        const combinedStyle = style? Object.assign({}, style, sizeStyle) : sizeStyle


        // user
        const creator = userStore.currentUser? userStore.getUserFromConnections(card.creatorId) : null
        const isAnon = !userStore.currentUser || userStore.currentUser.status === UserStatus.ANON

        // title
        const titleLen = card.title? card.title.length : 0
        const title = card.title // "isNew: " +isNew.toString()+ ", isUpdated: " +cardIsUpdated.toString() // isDragging? 'DRAGGING' : (card.title === 'docs.google.com')? 'Shared Google doc' : titleLen > 0? card.title : 'Untitled' // (uiStateStore.showExtensions || isHover)? card.title : stripExt(card.title)  // this.props.showUnseenEvents.toString() //
        const titleClass = 'formPaneTrns small jFG lighter'
        const descClass = "formPaneTrns tiny lighter subtle-scroll-dark jThird"

        // attachments
        const isEmbeddable = EmbedUtils.isEmbeddable(card.url)
        let ext, iconName, preview
        const isPlayable = ReactPlayer.canPlay(card.url)	 // isPlayableURL(card.url)

        // const showTools = (isHover || stickyInfo) && uiStateStore.cardView !== 'comments' && !showMyNotesOnly
        const dragWrapper = connectDragSource // ResponsiveUtils.isMobile()? function(elem) {return elem} : connectDragSource

        return dragWrapper(
            connectDropTarget(
                <div id={card.domId} className={cardClass} style={combinedStyle} key={wkey}
                     onMouseEnter={() => { this.hoverCard(true) }}
                     onMouseLeave={() => { this.hoverCard(false) }}
                >

                    {/* Main Card Image or media */}
                    <div className={"cardImage big" + (selected ? ' selected' : readOnly? ' readonly':'')}>

                        {isPlayable &&
                        <ReactPlayer url={card.url} light={true} controls={true} width={'100%'} height='100%'/>
                        }

                        {!isPlayable &&
                        <ImageViewer image={card.image} title={title} glow={imgGlow} wkey={wkey} mobile={mobile}
                                     selected={selected} readonly={readOnly} handleClick={this.openCardViewer}
                                     showRotation isHover={isHover}
                        />
                        }

                        {!card.image && allowImageChange &&
                        <CardSlideShowModal cardId={card.id} tip={false} trigger={
                            <ImageChooserModal cardId={this.id} handleImageChange={this.handleImageChange}
                                               showTrigger={true}/>
                        }/>
                        }

                        {!card.image && !allowImageChange && card.source &&
                        <img src={sourceToIcon(card.source)}
                             style={{marginTop: '10%', maxHeight: '90%', maxWidth: '90%'}}
                             onClick={this.gotoLink}/>
                        }

                        {!card.image && !card.source &&
                        <span className='abs centered pad1'
                              style={{top: '15%', left: 0, width: '100%', wordBreak: 'break-all'}}
                              onClick={this.openCardViewer}
                        >
                            <h3 className='jFG'>{title}</h3>
                        </span>
                        }

                        {creator && isHover && size > 75 &&
                        <div className="abs desc capsule lighter small cardBG fg"
                             style={{bottom: '0.5rem', left: '0.5rem', padding: '0 .5rem'}}>
                            <i>By {creator.firstName || firstWord(creator.displayName)}</i>
                        </div>
                        }

                        {!isApproval && !isAnon &&
                        <div className="abs more pointer" style={{bottom: '.25rem', right: '.5rem'}}>
                            {!readOnly && !uiStateStore.voMode &&
                            <Popup style={{padding: 0}}
                                   inverted
                                   trigger={<span className="roundIcon" data-tip='More...' style={{padding:'.1rem'}}>
                                               <MoreHoriz size={22} className='fg'/>
                                            </span>
                                   }
                                   open={this.state.showPopup}
                                   onOpen={(ev) => this.onPopupOpen (ev)}
                                   onClose={(ev) => this.onPopupClose (ev)}
                                   on='click'
                                   position='bottom center'

                            >
                                <Menu vertical className='hioh'>

                                    <Menu.Item name='reactions' active={activeDropdownItem === 'reactions'}
                                               onClick={this.openCardViewer} >
                                        Open...
                                    </Menu.Item>

                                    {card.url &&
                                    <Menu.Item name='open' active={activeDropdownItem === 'open'}
                                               onClick={(ev) => this.gotoLink (ev)}>
                                        Open link in new tab...
                                    </Menu.Item>
                                    }
                                    <Menu.Item name='delete' active={activeDropdownItem === 'delete'}
                                               onClick={(event) => {
                                                   this.deleteCard (event)
                                               }}>
                                        Delete...
                                    </Menu.Item>
                                </Menu>
                            </Popup>
                            }
                        </div>
                        }


                    </div>

                    {isNew && size > 125 &&
                    <DumbCapsule msg='New' className='abs' style={{top: '.5rem', right: '.5rem'}} />
                    }

                    {(!isNew && cardIsUpdated) && size > 125 &&
                    <DumbCapsule msg='Updated' className='abs' style={{top: '2rem', right: '.5rem'}} />
                    }

                    <div className='main content' style={{padding: '0.5rem 0.5rem 0 0.5rem' }}>
                        {size > 50 &&
                        <TextArea className={titleClass}
                                  name='title'
                                  rows={2}
                                  value={title}
                                  onClick={this.handleTextClick}
                                  onChange={this.handleTextChange}
                                  onBlur={this.handleTextSave}
                                  onKeyPress={this.handleKeyPress}
                                  style={{border: 'none', cursor: 'text', padding: 0, overflowY: 'auto', resize: 'none'}}
                        />
                        }

                        {size > 150 &&
                        <TextArea rows={2}
                                  name='description'
                                  value={card.description}
                                  className={descClass}
                                  data-tip={addLineBreaks (card.description)}
                                  data-multiline="true"
                                  onClick={this.handleTextClick}
                                  onChange={this.handleTextChange}
                                  onBlur={this.handleTextSave}
                                  onKeyPress={this.handleKeyPress}
                                  style={{marginTop: '.25rem', border: 'none', cursor: 'text', padding: 0, overflowY: 'auto', resize: 'none'}}
                        />
                        }

                        <div className='tagsAndReactions' style={{marginTop: '-2px'}}>
                            {size > 125 && showReactions &&
                            <div className='inline cardReactions'>
                                <AuthProtector isAnon={isAnon}
                                               component={
                                                   <CardReactions card={card} />
                                               }
                                />
                            </div>
                            }
                            {Boolean(size > 75 && card.tags2 && card.tags2.length > 0 && showTags && uiStateStore.cardView !== 'images') &&
                            <div className={tagStripClass}>
                                <CardTagStrip tags={card.tags2}
                                              card={card}
                                              rightAlign={!showReactions}
                                              onCard
                                              onUnTagCard={(tagId) => this.onUnTagCard (tagId)}/>
                            </div>
                            }

                        </div>


                    </div>

                    <div className='cardActions'
                         style={{opacity: (!readOnly && size > 125 && (isHover || isApproval || uiStateStore.forcePopupOpen))? 1:0}}>

                        {isApproval &&
                        <div className='centerAction pointer'>
                            {!selected &&
                            <Circle size={24} className='fg round subtleShadow' onClick={this.clickCard} style={{background: 'rgba(50,50,50,.5)'}}/>
                            }

                            {selected &&
                            <CheckCircle size={24} className='fg round subtleShadow' onClick={this.clickCard} style={{background: 'rgba(50,50,50,.95)'}}/>
                            }

                        </div>
                        }

                        {!isApproval &&
                        <span className='abs leftAction pointer'>
                            {card.url && card.url.length > 0 &&
                            <span className='capsule mr05 cardBG dontWrap vaTop' onClick={this.gotoLink}
                                  style={{padding: '2px 8px 4px 4px', maxWidth: '96%'}}
                                  data-tip={'Open link at ' +shortUrl}
                            >
                                <ArrowUpRight size={24} className="fg" />
                                <span className='inline vaTop ' style={{marginTop: '1px'}}> {reverseTruncate(shortUrl, 20, true)}</span>
                            </span>
                            }

                            {isEmbeddable && allowImageChange &&
                            <EmbedTrigger card={card}/>
                            }

                        </span>
                        }


                        {card.attachments &&
                        <span className='abs' style={{top: '2.5rem', left: '1rem'}}>
                                {card.attachments.map ((attm, i) => {
                                    ext = getExt (attm.url)
                                    iconName = extToIcon (ext)
                                    preview = isPreviewable (ext)
                                    return (
                                        <AttmThumb file={attm} ext={ext} iconName={iconName}
                                                   size={36}
                                                   openAttm={() => this.gotoAttm(attm.url)}
                                                   preview={preview}
                                                   className={'boxRadius mr05 attmInCard' + attmFlash}
                                                   tip='Open in Pogo.io'
                                        />
                                    )
                                })}
                            </span>
                        }

                    </div>

                    {card.annotations && card.annotations.length > 0 &&
                    <span className='abs' style={{top: '1rem', right: '.5rem'}}>
                            <Icon name='paint brush'
                                  data-tip='This item has drawings. Click to view.'
                                  className='vaTop jBlue'
                                  style={{fontSize: '18px'}}
                                  onClick={this.openCardViewer}
                            />

                    </span>
                    }


                </div>
            )
        )
    }
}


