import React, {Component} from 'react'
import {DragSource} from 'react-dnd'
import {DndItemTypes} from '~/config/Constants'
import {Link, Router} from '~/routes'
import uiStateStore from "../../store/uiStateStore";
import projectStore from "../../store/projectStore";
import MemberAvatar from "./MemberAvatar";

const memberSource = {
    beginDrag(props) {
        uiStateStore.enableFileDrops = false
        return {
            id: props.member.id,
            name: props.member.displayName || props.member.firstName,
            type: DndItemTypes.MEMBER_AVATAR
        }
    },
    endDrag(props, monitor) {
        uiStateStore.enableFileDrops = true
        const item = monitor.getItem()
        return {
            id: props.member.id,
            name: props.member.displayName || props.member.firstName,
            type: DndItemTypes.MEMBER_AVATAR
        }
    }
}


@DragSource(DndItemTypes.MEMBER_AVATAR, memberSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
}))
export default class DraggableMemberAvatar extends Component {

    overUser = () => {
        if (this.props.overUser)
            this.props.overUser(this.props.member)
    }

    clearUser = () => {
        if (this.props.clearUser)
            this.props.clearUser()
    }

    render() {
        const {connectDragSource, isDragging, member, activeUser, size,
            enableAdmin, showMenu, showAttmMenu, inMsgId, index, project, refresh,
            isEnabled, isChanged, style, showName, showBorder, removeAttm, attachUser, unattachUser, clickAction} = this.props

        return connectDragSource(
            <div className='inline'>
                <MemberAvatar member={member}
                              activeUser={activeUser}
                              enableAdmin={enableAdmin}
                              project={projectStore.currentProject}
                              key={'ua'+index}
                              index={index}
                              color="black"
                              attachUser={attachUser}
                              unattachUser={unattachUser}
                              showName={showName}
                              showMenu={showMenu}
                              showAttmMenu={showAttmMenu}
                              inMsgId={inMsgId}
                              removeAttm={removeAttm}
                              isEnabled={isEnabled}
                              overUser={this.overUser}
                              clearUser={this.clearUser}
                              clickAction={clickAction}
                              refresh={refresh}
                              showBorder={showBorder}
                              size={size}
                              style={style}
                />
            </div>
        )
    }
}

