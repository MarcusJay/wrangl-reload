import {Component} from 'react'
import PropTypes from 'prop-types'
import uiStateStore from '/store/uiStateStore'
import {ZoomIn, ZoomOut} from "react-feather";
import RU from '/utils/ResponsiveUtils'

const STEP_SIZE = 1
const MIN_SIZE = 10
const MAX_SIZE = 30

export default class CardSizeSlider extends Component {
    static propTypes = {
        cardContainer: PropTypes.string
    }

    constructor(props) {
        super(props)
        this.state = {
            sliderId: 'ss'+this.props.cardContainer,
            sizeValue: uiStateStore.tileHeight,
            valueId: 'sv'+this.props.cardContainer
        }
    }

    changeHandler(ev) {
        let newSize = ev.currentTarget.valueAsNumber
        this.setState({sizeValue: newSize})
        uiStateStore.setTileHeight(newSize)
    }

    zoomIn = (ev) => {
        const newSize = Math.min(this.state.sizeValue + STEP_SIZE, MAX_SIZE)
        this.setState({sizeValue: newSize})
        uiStateStore.setTileHeight(newSize)
    }

    zoomOut = (ev) => {
        const newSize = Math.max(this.state.sizeValue - STEP_SIZE, MIN_SIZE)
        this.setState({sizeValue: newSize})
        uiStateStore.setTileHeight(newSize)
    }

    componentDidMount() {
        const device = RU.getDeviceType()
        if (device !== 'computer' && !this.state.sizeValue)
            this.setState({sizeValue: 15})
    }

    render() {
        const device = RU.getDeviceType()

        return(
            <div className='right'>
                <span style={{marginRight: '0.5rem'}}>
                    Tile Size:
                </span>

                {device === 'computer' &&
                <span style={{marginTop: '0.2rem', verticalAlign: 'middle'}}>
                    <input id={this.state.sliderId} value={this.state.sizeValue} type="range" min={MIN_SIZE} max={MAX_SIZE} step={STEP_SIZE} style={{width: '7vw'}}
                    onChange={(ev) => this.changeHandler(ev)}/>
                </span>
                }

                {device !== 'computer' &&
                <span style={{marginTop: '-0.3rem', verticalAlign: 'middle'}}>
                    <ZoomOut size={29} className='secondary' onClick={this.zoomOut} style={{marginRight: '0.5rem'}}/>
                    <ZoomIn size={28} className='secondary' onClick={this.zoomIn} style={{marginRight: '0.5rem'}}/>
                </span>
                }
            </div>
        )
    }
}