// Singleton DragDropContext, to get around reactdnd's multiple HtmlContext complaints.
// All dnd-able components should import context from here, rather than decorate directly.

import {DragDropContext} from 'react-dnd';
import MultiBackend from 'react-dnd-multi-backend';
import HTML5toTouch from 'react-dnd-multi-backend/lib/HTML5toTouch'

export default DragDropContext(MultiBackend(HTML5toTouch))

// export default DragDropContext(DndBackend)


/*
let dragDropManager;

/!**
 * This is singleton used to initialize only once dnd in our app.
 * If you initialized dnd and then try to initialize another dnd
 * context the app will break.
 * Here is more info: https://github.com/gaearon/react-dnd/issues/186
 *
 * The solution is to call Dnd context from this singleton this way
 * all dnd contexts in the app are the same.
 *!/
export default function getDndContext() {
    if (!dragDropManager) {
        dragDropManager = new DragDropManager(HTML5Backend);
    }

    return dragDropManager;
}
*/
