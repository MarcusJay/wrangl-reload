import {Component} from 'react'
import {ChevronRight} from "react-feather";
import routes, {Link, Router} from '~/routes'
import categoryStore from '/store/categoryStore'


export default class Breadcrumb extends Component {

    render() {
        const {catId, project} = this.props
        const cat = categoryStore.getCategoryFromUser(catId)
        const catTitle = cat? cat.title : 'All Boards'

        return (
            <div className='tiny jSec jBlue'>
                <Link route='projectsPage' data-tip='Home'>
                    <span className='mb05 pointer hoh jBlue vaTop' >{catTitle}</span>
                </Link>

                {project &&
                <span>
                    <ChevronRight size={18} className='mb05 jBlue'/>
                    <Link prefetch route='project' key={'pla'} params={{id: project.id}}>
                        <span className='mb05 pointer hoh vaTop jBlue'>{project.title}</span>
                    </Link>
                </span>
                }
            </div>
        )
    }
}