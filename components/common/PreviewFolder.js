import {Component} from 'react'
import {observer} from 'mobx-react'
import {Folder} from 'react-feather';

import uiStateStore from '/store/uiStateStore'


@observer
export default class PreviewFolder extends Component {

    constructor(props) {
        super (props)

        // this.setImageHeight = this.setImageHeight.bind(this)

    }

    openFolder() {
        this.props.openFolder(this.props.folder.path)
    }

    componentWillReact(a, b, c) {
        console.log ("### Preview Folder will react. ", a, b, c)
    }

    render() {
        // size
        const size = uiStateStore.getCardSize(this.props.containerName) || uiStateStore.defaultCardSize
        // console.log("### My size is : " +size)
        const sizeStyle = {width: (size +'px'), height: (size * 1.25) +'px'}

        return (
            <div className="folder" key={'pl' + this.props.folder.id} onClick={() => this.openFolder () }
                 style={sizeStyle} data-tip={this.props.folder.title}>
                <Folder size={size} style={{display: 'block'}}/>
                <div className="noWrap ctitle" style={{fontSize: '1rem'}}>{this.props.folder.title}</div>
            </div>
        )
    }
}



