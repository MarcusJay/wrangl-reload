import {Component} from 'react'
import {observer} from 'mobx-react'

import cardStore from '/store/cardStore'
import projectStore from '/store/projectStore'
import uiStateStore from "../../../store/uiStateStore";
import Dropzone from "react-dropzone";
import CardModel from "../../../models/CardModel";
import {requestPreviews} from "../../../services/ImageFileService";
import {Loader} from "semantic-ui-react";

const MIN_ICON_SIZE = 24
const MIN_FONT_SIZE = 0.8

@observer
export default class CardCreator extends Component {

    constructor(props) {
        super (props)
        let fileRefs

        this.state = {
            elem: null,
            uploading: false,
            uploadCount: 1,
            uploadProgress: 0,
            importing: false,
            selectedCard: this.props.card || new CardModel (),
            cards: [],
            cardCount: 0
        }


    }

    onDrop = (files) => {
        this.fileRefs = files
        let card, cards = []
        files.forEach (file => {
            card = CardModel.FromFileUpload (file)
            uiStateStore.fileCardMap[file] = card
            cards.push (card)
        })

        this.setState ({
            cards: cards,
            cardCount: cards.length
        }, () => this.uploadAssets())

    }

    uploadAssets() {
        this.setState ({importing: true})
        cardStore.createCardsFromFiles (this.state.cards, this.props.currentProject).then (response => {
            this.newCards = response.data.cards
            cardStore.attachFilesToCards (this.newCards, this.fileRefs, this.showUploadProgress.bind (this))
        })
    }

    // Upload progresses will arrive asynchronously
    showUploadProgress(count, msg) {
        if (msg === 'end') {
            this.setState ({uploadCount: Math.max (count, this.state.uploadCount)})
            if (count === this.state.cards.length) {
                requestPreviews (this.newCards)
                this.props.uploadAssets () // TODO rename to refreshCards
                // RELEASE RESOURCES
                if (this.fileRefs)
                    this.fileRefs.forEach (file => window.URL.revokeObjectURL (file.preview))
                this.setState ({importing: false, uploading: false})
            }
        }
        else if (count === 'start') {
            this.setState ({uploading: true, importing: true})
        } else {
            console.log ("Progress: count = " + count + ", msg: " + msg)
        }
    }

    render() {
        const {mobile, showTags, showReactions, containerElem} = this.props
        const { importing, uploading, uploadCount, cards, cardCount } = this.state
        const project = projectStore.currentProject
        if (cardStore.isFiltering) // hide if filtering to avoid confusion
            return null

        let size, sizeStyle
        if (mobile) {
            size = uiStateStore.cardsPane? uiStateStore.cardsPane.innerWidth : containerElem? containerElem.innerWidth : window? window.innerWidth - 75 : 325
            sizeStyle = {width: (size +'px'), height: (size * 1.35) +'px'}
        } else {
            size = uiStateStore.getCardSize('main') || uiStateStore.defaultCardSize
            const ratio = 1.35 + (showReactions? .3 : 0) + (showTags? .2 : 0)
            sizeStyle = {width: (size +'px'), height: (size * ratio) +'px'}
        }


        return (
            <div className='inline'>
                <Dropzone onDrop={this.onDrop}
                          multiple={true}
                          disabled={uiStateStore.enableFileDrops === false}
                          style={sizeStyle}
                >
                    <div className='newCard cardRadius'
                         style={{width: '100%', height: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center'
                         }}>

                        <div className='third small centered'>
                            <img src='/static/svg/v2/dragOrUpload.svg'/>
                            <p>Drag or <span className='jBlue'>upload</span> files here</p>
                        </div>

                    </div>
                </Dropzone>
                <Loader inverted active={uploading}
                        style={{marginTop: '0', height: '10%'}}>
                    Uploading Cards: {uploadCount} of {cardCount}
                </Loader>
                <Loader inverted active={importing}
                        className='pad1 cardRadius jSideBG'
                        style={{marginTop: '0', height: '10%'}}>
                    Uploading Images: {uploadCount} of {cardCount}
                </Loader>

            </div>
        )
    }
}