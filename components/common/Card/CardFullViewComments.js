import {Component} from 'react'
import {Comment, Icon, Input} from 'semantic-ui-react'
import {Trash, User} from 'react-feather'
import Moment from 'react-moment'

import userStore from '/store/userStore' // TODO replace with readonlyStore
import StateChangeRequestor, {REQUEST_CARD_COMMENT, REQUEST_CARD_COMMENT_DELETE} from '/services/StateChangeRequestor'


export default class CardFullViewComments extends Component {
    constructor(props) {
        super(props)

        // TODO redo this props to state pattern with change requestor to direct mobx access.
        this.state = {comments: this.props.card.comments || [], inputText:'', parentElem: null }
        console.log("CArdComments: I have " +this.state.comments.length+ " comments.")

        this.onClick = this.onClick.bind(this)
        this.onChange = this.onChange.bind(this)
        this.setCommentsHeight = this.setCommentsHeight.bind(this)
        this.onKeyPress = this.onKeyPress.bind(this)
    }

    onClick(ev) {
        console.log('click. value: ' +ev.target)
        ev.stopPropagation()
        ev.preventDefault()
        // ev.target.focus()


    }

    onKeyPress(ev) {
        console.log('key. value: ' +ev.key+ ", val: " +ev.target.value)
        if (ev.target.value === '' && (ev.key === 'ArrowLeft' || ev.key === 'ArrowRight')) {
            this.props.handleNav(ev)
            return
        }
        if (ev.key === 'Enter')
            this.submitComment(ev)
    }

    submitComment(ev) {
        let text = ev.target.value

        StateChangeRequestor.awaitStateChange(REQUEST_CARD_COMMENT, {card: this.props.card, commentText: text})
            .then( comment => {
                console.log("Received back: " +comment.comment_text)
                this.setState({comments: [comment, ...this.state.comments], inputText: '' }, () => {
                    this.setCommentsHeight()
                })
                this.props.card.comments = this.state.comments // mutating our parent card. See antipattern note above.
                this.focusNav()
            })
    }

    deleteComment(ev, comment) {
        ev.stopPropagation()
        ev.preventDefault()

        StateChangeRequestor.awaitStateChange(REQUEST_CARD_COMMENT_DELETE, {commentId: comment.comment_id})
            .then( response => {
                if (response.success === true) {
                    let index = this.state.comments.findIndex( c => c.comment_id === comment.comment_id)
                    this.state.comments.splice(index,1)
                    this.setState({comments: this.state.comments, inputText: '' }, () => {
                        this.setCommentsHeight()
                    })
                    this.props.card.comments = this.state.comments // mutating our parent card. See antipattern note above.
                }
            })
    }

    onChange(ev) {
        this.setState({inputText: ev.target.value})
    }

    setCommentsHeight() {
        return;
        if (!this.state.parentElem) {
            return
        }

        if (this.props.showHeader === false)
            return (this.state.parentElem.offsetHeight * 0.9) + 'px'

        let scrollerMultiplier
        switch (this.props.card.totalComments) {
            case 0: scrollerMultiplier = 0.15; break
            case 1: scrollerMultiplier = 0.3; break
            case 2: scrollerMultiplier = 0.4; break
            default: scrollerMultiplier = 0.45
        }

        // this.setState({scrollerHeight: findDOMNode(this.props.cardElem).offsetHeight * scrollerMultiplier})
        return (this.state.parentElem.offsetHeight * scrollerMultiplier) + 'px'

    }

    componentWillMount() {
    }

    componentDidMount() {
/*
        const cardDomNode = findDOMNode(this.props.cardElem)
        this.setState({parentElem: cardDomNode}, () => {
            this.setCommentsHeight()
        })
*/
    }

    focusInput() {
        // TODO replace with reactive way
        const inputElem = document.querySelector('#cInput')
        if (inputElem)
            inputElem.focus()
    }
    focusNav() {
        // TODO replace with reactive way
        const inputElem = document.querySelector('#hnav')
        if (inputElem)
            inputElem.focus()
    }

    // TODO Redo this whole props to state antipattern with mobx. see above
    componentWillReceiveProps(nextProps) {
        this.setState({comments: nextProps.card.comments || [], inputText:''})
    }

    render() {
        let self = this
        const isAnon = !userStore.currentUser

        if (!isAnon)
            this.state.comments.forEach( comment => {
                comment.user = userStore.getUserFromConnections(comment.comment_creator)
            })

        return (

            <div className="comments" >
                <h3>Notes</h3>

                <div className="commentScroller" style={{maxHeight: this.setCommentsHeight()}}>
                    <Input placeholder={isAnon? "Signup to comment!" : "Add a note..."}
                       icon={<Icon name='comment' onClick={this.submitComment}/>}
                       className="fvInputWrapper"
                           inverted
                       onClick={this.onClick}
                       onChange={this.onChange}
                       id="cInput"
                       value={isAnon? '' : this.state.inputText}
                       onKeyPress={this.onKeyPress}/>

                    {this.state.comments.length === 0 &&
                    <div className="secondary" style={{marginTop: '1rem'}}>No notes</div>
                    }
                    {this.state.comments.length > 0 &&
                    <Comment.Group>
                        {this.state.comments.map (function (comment, i, comments) {
                            return (
                                <Comment key={'cfvc'+i}>
                                    {comment.user && comment.user.image &&
                                    <img className='userImg' src={comment.user.image}/>
                                    }
                                    {(!comment.user || !comment.user.image) &&
                                    <User size={24} />
                                    }
                                    <Comment.Content style={{display: isAnon? 'inline':'block'}}>
                                        {comment.user &&
                                        <Comment.Author as='a' className='darkSecondary'>{comment.user.displayName}</Comment.Author>
                                        }
                                        <Comment.Metadata>
                                            <Moment subtract={{hours: 7}} className="darkThird small"
                                                    fromNow>{comment.time_modified}</Moment>
                                            {comment.user && self.props.user && comment.user.id === self.props.user.id &&
                                            <Trash color="#777" size={16}
                                                             onClick={(ev) => self.deleteComment (ev, comment)}
                                                             className="deleteIcon"/>
                                            }
                                        </Comment.Metadata>
                                        <Comment.Text className='cText'>{comment.comment_text}</Comment.Text>
                                    </Comment.Content>
                                </Comment>
                            )
                        })}
                    </Comment.Group>
                    }
                </div>
                <input type='text' id='hnav' onKeyDown={this.handleKeyPress} className='hidden'/>
            </div>
        )
    }
}