import {Component} from 'react'
import {observer} from 'mobx-react'
import {Router} from '~/routes'
import PropTypes from 'prop-types'


import {ThumbsDown, ThumbsUp} from 'react-feather'

import uiStateStore from '/store/uiStateStore'
import {THUMBS_DOWN, THUMBS_UP} from '/config/ReactionConfig'
import {findDOMNode} from "react-dom";

const ANIM_CLASS = 'highlightOrange'

@observer
export default class CardReaction extends Component {
    static propTypes = {
        active: PropTypes.bool,
        count: PropTypes.number,
        card: PropTypes.any,
        userReaction: PropTypes.any,
        reactionType: PropTypes.any,
        toggleReaction: PropTypes.func
    }

    constructor(props) {
        super(props)
        this.toggleReaction = this.toggleReaction.bind(this)
        this.state = {isChanged: false} // {unseenShown: false}
        this.estCount = 0               // estimated reaction count
        this.acknowledgedEvents = []    // events we already know about
    }

    toggleReaction(ev) {

        ev.stopPropagation()
        ev.preventDefault()
        let self = this
        let rTypeId = this.props.reactionType.reaction_type_id

        this.props.toggleReaction(this.props.reactionType, this.props.active, this.props.userReaction)

    }

    // check for unseen events of the type we're responsible for animating, and set related states accordingly
    highlightUnseenEvents = (eventMap) => {
        if (!eventMap || !eventMap.reactionEvents)
            return

        const isChanged = eventMap.reactionEvents.some( event => event.reactionTypeId === this.props.reactionType.reaction_type_id )
        if (isChanged) {
            // this.estCount = ReactionApiService.countCardReactions (this.props.card, this.props.reactionType.reaction_type_id)
            // this.resetAnimation()
            this.setState({
                isChanged: true,
                nWhatsNew: uiStateStore.nWhatsNew
            })
        }
    }

    // LEFT OFF:
    // 1. Use nextProps rather than this.props.showUnseenEvents?
    // 2. It appears that isChanged is set to true in highlight, yet at point of render, it is false.


    highlightRealtimeChanges = (nextProps) => {
/*
        this.estCount = nextProps.count

        // ignore if not this card or not this reaction type
        // const ev = nextProps.rtEvent
        // if (!ev)// || ev.cardId !== this.props.card.id || ev.reactionTypeId !== this.props.reactionType.reaction_type_id)
        //     return

        const isActive = nextProps.active
        const wasActive = this.props.active

        const newCount = nextProps.count
        const oldCount = this.props.count

        const isChanged = newCount !== oldCount
        const activeChanged = isActive !== wasActive

        if (isChanged || activeChanged)
            this.resetAnimation()

        this.setState({
            isChanged: isChanged,
            activeChanged: activeChanged,
        })
*/

    }

    // TODO: Why not just apply the anim style to our top level element? Why separate icon and count?
    resetAnimation() {
        const elem = findDOMNode(this)
        if (!elem)
            return

        const svg = elem.firstChild
        const counter = elem.childNodes.length > 1? elem.childNodes[1] : null

        svg.classList.remove(ANIM_CLASS)
        if (counter)
            counter.classList.remove (ANIM_CLASS)

        setTimeout(() => {
            svg.classList.add(ANIM_CLASS)
            if (counter)
                counter.classList.add(ANIM_CLASS)
        }, 100)
    }

    componentWillMount() {
        // initial count before events stream in
        // this.estCount = ReactionApiService.countCardReactions (this.props.card, this.props.reactionType.reaction_type_id)
        // if (this.props.showUnseenEvents && this.props.events)
        //     this.highlightUnseenEvents(this.props.events)
    }

    componentWillReceiveProps(nextProps) {
        // console.log('CardReaction will receive props')
        // this.highlightRealtimeChanges(nextProps)
        // if (nextProps.showUnseenEvents)
        //     this.highlightUnseenChanges()
        if (this.props.showUnseenEvents)
            this.highlightUnseenEvents(nextProps.events)

    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.count !== this.props.count || uiStateStore.showWhatsNew
    }

    componentWillReact() {
        console.log ('Cardreaction will react.')
/*
        if (uiStateStore.nWhatsNew !== this.state.nWhatsNew) {
            console.log('Cardreaction Reanimating')
            this.resetAnimation()
        }
*/

    }

    render() {
        const { count, active, card, force } = this.props
        const myReactionName = this.props.reactionType.reaction_name

        // animate changes
        // const forMobx = uiStateStore.nWhatsNew !== this.state.nWhatsNew
        const glowMe = this.state.isChanged? ANIM_CLASS : ''

        const size = active? 26 : 20
        // const color = this.matchingReaction || this.matchingUnreaction? '#f0592b' : '#999'

        const iconClass = 'reactionIcon ' + glowMe
        const showMe = true // this.estCount > 0 || force || flashMe

        return (
            <div className="reaction cardAction" style={{opacity: showMe? 1 : 0}}>

                {myReactionName === THUMBS_UP &&
                    <ThumbsUp size={size} className={iconClass} onClick={this.toggleReaction}/>
                }
                {myReactionName === THUMBS_DOWN &&
                    <ThumbsDown size={size} className={iconClass} onClick={this.toggleReaction}/>
                }
                {/*<span>{this.props.showUnseenEvents.toString()}</span>*/}
                {count > 0 &&
                    <span className={glowMe}>{count}</span>
                }
            </div>
        )

    }
}

/*
active={ReactionApiService.hasUserReacted (card, reactionTypeObj.reaction_type_id)}
count={ReactionApiService.countCardReactions (card, reactionTypeObj.reaction_type_id)}
force={isHover}
events={this.props.events}
rtEvent={card.rtEvent}
showUnseenEvents={this.props.showUnseenEvents}
toggleReaction={(reactionType, isActive, userReaction) => this.toggleReaction (reactionType, isActive, userReaction)}
userReaction={ReactionApiService.getUserReactionByType (card, reactionTypeObj.reaction_type_id)}
reactionType={reactionTypeObj}/>
*/
