import {Component} from 'react'
import {getCardReactions} from "../../../services/CardApiService";
import {ArrowDown, ArrowUp} from "react-feather";
import ReactionApiService, {countCardVotes, voteTypeToId} from "../../../services/ReactionApiService";
import {THUMBS_DOWN, THUMBS_UP} from "../../../config/ReactionConfig";
import cardStore from '/store/cardStore'
import {UserStatus} from "../../../config/Constants";
import userStore from "../../../store/userStore";

const ActiveColor = '#aaa'
const HoverColor = '#bbb'

export default class VoteReactions extends Component {

    state = {upVoting: false, downVoting: false}

/*
    onHover(starNum) {
        this.setState({hoverStarNum: starNum, showStats: true})
    }

    onLeave() {
        this.setState({hoverStarNum: -1, showStats: false})
    }
*/

    toggleVote(cardId, voteType) {
        this.setState({upVoting: voteType === THUMBS_UP, downVoting: voteType === THUMBS_DOWN})
        ReactionApiService.registerVoteReaction(cardId, voteType).then( response => {
            getCardReactions(cardId, false).then( response => {
                this.props.card.reactions = response.data.card_reactions
                if (this.props.onVote)
                    this.props.onVote()
                this.setState({upVoting: false, downVoting: false})
            })
        })
    }

    componentDidMount() {
        const {card} = this.props
        this.thumbsUpId = voteTypeToId(THUMBS_UP)
        this.thumbsDownId = voteTypeToId(THUMBS_DOWN)
        getCardReactions(card.id, false).then( response => {
            card.reactions = response.data.card_reactions
            // force render to replace card_read's reaction counts (project independent)
            // with card_read_reaction's counts (project dependent)
            this.setState(this.state)
        })
    }

/*
    componentDidUpdate() {
        const {card} = this.props
        getCardReactions(card.id, false).then( response => {
            card.reactions = response.data.card_reactions
        })
    }
*/

    render() {
        const {card, cardId, mini, hideZeros} = this.props
        const {upVoting, downVoting} = this.state
        if (!card.reactions)
            return null

        const votes = countCardVotes(card)
        const me = userStore.currentUser
        const isAnon = !me || me.status === UserStatus.ANON
        const isApproved = card.reactions.find(r => r.user_id === me.id && r.reaction_type_id === this.thumbsUpId) !== undefined
        const isRejected = card.reactions.find(r => r.user_id === me.id && r.reaction_type_id === this.thumbsDownId) !== undefined

        return (
            <span className='fullW'>

                {!mini && (votes.up > 0 || !hideZeros) &&
                <span onClick={() => this.toggleVote (cardId, THUMBS_UP)}
                      className={'inline capsule mr05 animBG vaTop ' +(isApproved? 'jUpvoteBG':'jUpvoteBGOH')}
                      data-tip="Like"
                      style={{padding: '2px 4px', height: '26px'}}
                >
                    <img src='/static/svg/thumbsUp.svg'
                         width={21} height={21}
                         className='apUtReactions pointer hoh '
                         style={{marginTop: '-2px', marginLeft: '1px'}}/>
                    <span className='vaTop jFG tiny'>{votes.up}</span>
                </span>
                }

                {!mini && (votes.down > 0 || !hideZeros) &&
                <span onClick={() => this.toggleVote (cardId, THUMBS_DOWN)}
                      className={'inline capsule mr05 animBG vaTop ' +(isRejected? 'jDownvoteBG':'jDownvoteBGOH')}
                      data-tip="Dislike"
                      style={{padding: '2px 4px', height: '26px'}}
                >
                    <img src='/static/svg/thumbsDown.svg'
                         width={20} height={20}
                         className='pointer hoh '
                         style={{marginTop: '6px', marginLeft: '2px'}}/>
                    <span className='vaTop jFG tiny'>{votes.down}</span>
                </span>
                }

                {mini && (votes.up > 0 || !hideZeros) &&
                <span className='vaTop jUpvote mr05 tiny'>{votes.up}</span>
                }

                {mini && (votes.down > 0 || !hideZeros) &&
                <span className='vaTop jDownvote tiny'>{votes.down}</span>
                }

            </span>
        )
    }
}

