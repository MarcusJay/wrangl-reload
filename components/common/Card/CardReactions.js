import {Component} from 'react'

import projectStore from '/store/projectStore'
import {STARS, THUMBS} from "../../../config/ReactionConfig";
import VoteReactions from "./VoteReactions";
import StarReactions from "./StarReactions";
import {ProjectType} from "../../../config/Constants";
import ReactionApiService from '../../../services/ReactionApiService'

export default class CardReactions extends Component {

    state = {rset: null}

    onVote = (rset) => {
        if (this.props.onVote)
            this.props.onVote(rset)
    }

    componentDidMount() {
        const project = projectStore.currentProject
        if (project && project.projectType === ProjectType.APPROVAL && !project.reactionSetId) {
            project.reactionSet = ReactionApiService.getReactionSetByTitleFromDefaults(THUMBS)
            project.reactionSetId = project.reactionSet.reaction_set_id
        } else if (project && project.reactionSetId && !project.reactionSet) {
            project.reactionSet = ReactionApiService.getReactionSetByIdFromDefaults(project.reactionSetId)
        }

        if (project)
            this.setState({rset: project.reactionSet})

    }

    render() {
        const {card, hideZeros, mini, size, rsum, atFirst, atLast, tiny} = this.props
        const {rset} = this.state
        if (!rset)
            return null

        const rsTitle = rset.reaction_set_title
        const cardId = card.id || card.card_id // transitioning away from modeling small and numerous objects


        switch (rsTitle) {
            case THUMBS:
                return <VoteReactions mini={mini} card={card} cardId={cardId} sizePx={size} onVote={this.onVote} hideZeros={hideZeros}/>
            case STARS:
                return <StarReactions mini={mini} cardId={cardId} onVote={this.onVote}/>
            default:
                return null
        }

    }
}