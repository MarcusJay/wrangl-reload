import {Component} from 'react'

export default class CardInfo extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return(
            <div style={{overflowY: 'auto'}}>
                <div>Comments: {this.props.card.totalComments}</div>
                <div>Link: {this.props.card.urls[0] || 'none'}</div>
                <div>TBD stats, reactions, views.</div>

            </div>
        )
    }
}