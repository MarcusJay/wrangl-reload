import React, {Component} from 'react'
import {findDOMNode} from 'react-dom'
import {DropTarget} from 'react-dnd'
import uiStateStore from "../../../store/uiStateStore";
import userStore from "../../../store/userStore";
import projectStore from "../../../store/projectStore";
import cardStore from "../../../store/cardStore";
import tagStore from "../../../store/tagStore";
import {indexToCardLabel} from "../../../utils/solo/indexToCardLabel";
import Card from "../Card";
import {DndItemTypes} from '/config/Constants'

const stackTarget = {
    drop(props, monitor, component) {
        let draggedItem = monitor.getItem()

        const dragIndex = draggedItem.index
        const hoverIndex = props.index
        component.moveCardToStack(draggedItem)
    }
}


@DropTarget([DndItemTypes.CARD], stackTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
export default class CardStack extends React.Component {
    state = {elem: null, expanded: false, spreadWidth: 0, hSpread: 0, vSpread: 10}

    // In this case, we're [UNtagging] a card from its source tag stack and tagging it to its dest tag stack. Ugh. We need REAL folders.
    moveCardToStack(draggedItem) {
        const {tag, cards, size} = this.props

        if (cardStore.isFiltering)
            return

        const dragCard = cardStore.getCardFromCurrent(draggedItem.id)

        // 1. IF card has this tag (we a
        if (dragCard.stackId && dragCard.tags2.findIndex(tag => tag.id === dragCard.stackId) !== -1) {
            tagStore.untagCard(draggedItem.stackId, draggedItem.id) // nothing to do
        }

        // 2. TAG to this stack if not already in it.
        // if (dragCard.stackId !== draggedItem.stackId) {
            tagStore.tagCard (tag.id, draggedItem.id).then (movedCard => {
                // TODO rather than fetch project, how's about reorder cards according to response.data.destination_project_cards array
                projectStore.fetchCurrentProject ()
                // this.setState({isHovering: false})
            })
        // }
    }

    componentDidMount() {
        const elem = findDOMNode(this)
        const stack = this.refs.cardStack
        let width = 0
        if (stack) {
            const rect = stack.getBoundingClientRect()
            width = rect? rect.width : 0
        }

        this.setState({elem: elem, spreadWidth: width})
    }

    expand = (ev) => {
        const {size} = this.props
        const sizeVal = parseInt(size)
        this.setState({expanded: true, vSpread: sizeVal / 1.7}) // sizeVal * 1.6})
/*
        const expandTimer = setInterval( () => {
            this.setState({hSpread: this.state.hSpread + 10, vSpread: this.state.vSpread -= .25})
            if (this.state.hSpread >= 230 ) {
                clearInterval(expandTimer)
            }
        }, 20)
*/
    }

    shrink = (ev) => {
        this.setState({expanded: false, vSpread: 10})
/*
        const shrinkTimer = setInterval( () => {
            this.setState({hSpread: this.state.hSpread - 10, vSpread: this.state.vSpread += .25})
            if (this.state.hSpread <= 10 ) {
                clearInterval(shrinkTimer)
            }
        }, 20)
*/
    }

    render() {
        const {tag, cards, size, isOver, isDragging, canDrop, item, connectDropTarget} = this.props

        const {elem, expanded, hSpread, vSpread} = this.state
        const project = projectStore.currentProject
        if (!project)
            return null
        const sizeVal = parseInt(size)
        // const actualWidth = Math.max(sizeVal, spreadWidth)
        const cardCount = cards.length

        return connectDropTarget(
            <div className='inline jFG mb1 mr1 cardRadius'
                 style={{minWidth: size, minHeight: (sizeVal *1.7), height: (sizeVal * 1.7 * cardCount), /*!isOver? ((sizeVal*1.6) + (cardCount*10))+'px' : '2000px',*/
                         opacity: isOver? .5:1,
                         transition: 'background-color 0.2s ease-in',
                         backgroundColor: isOver? '#00000050':'transparent'
                 }}
                 onMouseEnter={this.expand}
                 onMouseLeave={this.shrink}
            >
                <div className='jFG relative centered capsule mb1 fullW'
                     style={{backgroundColor: isOver? 'black' : '#'+tag.color+'ff', padding: '.5rem .25rem'}}>
                     {tag.name}
                     <span className='abs' style={{right: '1rem'}}>
                         {cardCount}
                     </span>
                </div>

                {isOver &&
                <div className='jFG centered fullW fixed' style={{top: '1rem', left: '1rem'}}>
                    Add to {tag.name}?
                </div>
                }

                <div ref='cardStack' className='cardStack cardRadius relative'>
                {cards.map( (card, i) => {
                    card.stackId = tag.id
                    return (
                        <div className='cwrap abs cardRadius'
                             key={'w'+card.domId}
                             style={{
                                 padding: 0,
                                 margin: 0,
                                 top: (i * vSpread)+ 'px',
                                 left: (i * hSpread) + 'px',
                                 boxShadow: !expanded? '0px -5px 5px #00000070' : '0px -10px 7px #00000040',
                                 // filter: !expanded? ('blur(' +( (cardCount/2.5) - .15 - i)+ 'px) brightness(' +((cardCount-i)/(cardCount*1.2))+ ')')  : 'none'
                                 // filter: !expanded? 'brightness(' +((i+1)/cardCount)+ ')' : 'none'
                             }}>
                          <Card
                            card={card}
                            size={size}
                            stackId={tag.id}
                            style={{margin: 0}}
                            containerName='main'
                            containerElem={this.state.elem}
                            previous={null}
                            next={null}
                            key={card.domId}
                            wkey={'c'+i}
                            index={i}
                            prefSize={sizeVal}
                            user={userStore.currentUser}
                            view={uiStateStore.cardView}
                            showTags={this.props.showTags}
                            showLetters={this.props.showLetters}
                            showReactions={project.reactionSetId !== null}
                            openCardViewer={(card, attmId) => this.openCardViewer(card, attmId)}
                            openCardEditor={(card) => this.openCardEditor(card)}
                            ltr={indexToCardLabel (i)}
                            id={card.id}
                            text={card.title}
                            reactionSet={project.reactionSet}
                            allowImageChange={false}
                            isApproval={false}

                            mobile={false}

                            events={[]}
                            showUnseenEvents={false}

                            moveCard={this.moveCard}
                            hoverCard={this.hoverCard}
                            selectCard={this.clickCard}
                            selected={cardStore.isCardSelected(card.id)}

                            saveCard={null}
                            deleteCard={null}
                            expanded={false}

                            onExpand={this.onExpand}
                            onShrink={this.onShrink}
                            onTextChange={this.onTextChange}
                            onTextSave={this.onTextSave}
                          />
                        </div>
                    )

                })}
                </div>

            </div>
        )
    }
}