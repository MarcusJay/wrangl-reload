import {Component} from 'react'
import {Comment, Header, Segment} from 'semantic-ui-react'
import userStore from '/store/userStore' // TODO replace with readonlyStore


export default class CardCommentsPreview extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        let self = this

        this.props.comments.forEach( comment => {
            comment.user = userStore.getUserFromConnections(comment.comment_creator)
        })

        return(

            <div className="cardCommentsPreview" onClick={this.props.onClick}>
                <Segment>
                <div className="commentScroller" >
                    <Header as='h5'>Notes</Header>
                    <Comment.Group>
                        {this.props.comments.map (function (comment, i, comments) {
                            return (
                                <Comment key={'cgc'+i}>
                                    <Comment.Avatar src={comment.user.image} style={{width: '1.4rem'}}/>
                                    <Comment.Content style={{marginLeft: '2.5rem'}}>
                                        <Comment.Text>{comment.comment_text}</Comment.Text>
                                    </Comment.Content>
                                </Comment>
                            )
                        })}
                    </Comment.Group>
                </div>
                </Segment>
            </div>
        )
    }
}