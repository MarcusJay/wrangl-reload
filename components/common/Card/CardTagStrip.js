import {Component} from 'react'
import {observer} from 'mobx-react'
import {Plus, X} from 'react-feather'
import ColorUtils from '/utils/ColorUtils'
import ReactTooltip from 'react-tooltip'
import tagStore from '/store/tagStore'
import cardStore from '/store/cardStore'
import uiStateStore from '/store/uiStateStore'
import projectStore from '/store/projectStore'
import {abbrevOrTruncate} from "../../../utils/solo/abbrevOrTruncate";

@observer
export default class CardTagStrip extends Component {
    tagCard(tag) {
        const {card} = this.props
        tagStore.tagCard (tag.id, card.id).then( updatedCard => {

        })
    }

    untagCard(tag) {
        const {card} = this.props
        tagStore.untagCard(tag.id, card.id).then( updatedCard => {

        })

    }

    toggleTag(tag) {
        const {editMode, card } = this.props
        if (!editMode)
            return

        // const card = uiStateStore.editingCard
        const isAssigned = card.tags2.find( assignedTag => assignedTag.id === tag.id) !== undefined
        if (isAssigned)
            this.untagCard(tag)
        else
            this.tagCard(tag)
    }

    componentDidUpdate() {
        console.warn('card tag strip did update')
        ReactTooltip.rebuild()
    }

    render() {
        let {card, editMode, viewerMode} = this.props

        // const card = editMode? uiStateStore.editingCard : cardStore.getCardFromCurrent(cardId)
        // we're in the middle of transitioning to a new set of current cards
        if (!card)
            return null
        const project = projectStore.currentProject
        if (!project)
            return null

        const goBig = editMode || viewerMode
        const assignedTags = card.tags2
        const availTags = project.tagSet2

        let assignedTagCount = assignedTags.length
        const tagWidth = 100 / assignedTagCount
        const tagHeight = goBig? '22px' : '10px'
        const charsPerTag = 20 / assignedTagCount
        const borderWidth = goBig? '2px':'1px'
        const style= !goBig? {width: '97%'} : viewerMode? {lineHeight: '14px !important', width: '50%', marginLeft: 0, marginTop: '1rem'} : {lineHeight: '14px !important'}

        const displayTags = editMode? availTags : assignedTags
        const padding = goBig? '0 .5rem' : '0 0 0 0.25rem'

        let userTag, color, isAssigned, tip

        return (
            <div className="tagStrip" style={style}>
                {displayTags.map ((userTag, i) => {

                    // Warning: some tag sets have undefined tags. They were likely deleted but not unassigned.
                    if (!userTag)
                        return null

                    color = ColorUtils.isLight (userTag.color) ? '#333' : 'white'
                    isAssigned = assignedTags.find( tag => tag && tag.id === userTag.id) !== undefined // some cards have undefined assigned tags
                    tip = isAssigned? `Remove tag "${userTag.name}"` : `Add Tag "${userTag.name}"`
                    return userTag && userTag.name? (
                        <span key={'tag' + i} onClick={() => this.toggleTag(userTag)}
                              className='pointer relative tag'
                              style={{border: borderWidth+ ' solid #' +userTag.color,
                                      borderRadius: goBig? '8px':'4px',
                                      backgroundColor: isAssigned? ('#' + userTag.color) : 'transparent',
                                      color: isAssigned? color : 'white', // + userTag.color),
                                      opacity: isAssigned? 1 : 0.5,
                                      padding: padding,
                                      height: tagHeight,
                                      width: viewerMode? tagWidth : '',
                                      marginRight: goBig? '0.5rem':'',
                                      marginBottom: '.5rem'
                              }}
                              data-tip={tip}>

                                {goBig && userTag.name}
                                {/*{viewerMode && abbrevOrTruncate(userTag.name, 2)}*/}

{/*
                            {isAssigned && editMode &&
                            <X size={16} className='abs fg round hidden pointer attmAction centered'
                               style={{top: '-2px',right:'-1px'}}
                               onClick={() => this.untagCard (userTag)}/>
                            }
*/}

{/*
                            {!isAssigned && editMode &&
                            <Plus size={16} color={'#'+userTag.color} className='untagBtn pointer'
                               onClick={() => this.tagCard (userTag)}
                               />
                            }
*/}
                        </span>) : null
                })}
            </div>
        )
    }
}

/*
export class StripItem extends Component {
    static propTypes = {
        tag: PropTypes.any
    }

    render() {
        let tag = this.props.tags? this.props.tags.filter( tag => tag.name !== undefined ) : []
        let n = tags.length
        const tagWidth = 100 / n
        const charsPerTag = 20 / n

        return (
            <div className="tagStrip">
                {tags.map ( tagObj =>
                    <span key={'t'+tagObj.id} style={{width: tagWidth+'%', backgroundColor: '#'+tagObj.color, color: ColorUtils.isLight(tagObj.color)? '#777':'white', paddingTop: '2px' }}
                          data-tip={tagObj.name}>
                        {tagObj.name? abbrevOrTruncate(tagObj.name, charsPerTag ) : ''}
                    </span>
                )}
            </div>
        )
    }
}
*/

/*
getTagFromStore = (tagId) => {
    return tagStore.currentTagSet.find( tag => tag.id === tagId)
}*/
