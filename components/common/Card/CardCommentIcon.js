import {Component} from 'react'
import {MessageCircle} from 'react-feather'

export default class CardCommentIcon extends Component {
    constructor(props) {
        super(props)
    }

    handleCommentClick(ev) {
        ev.preventDefault()
        ev.stopPropagation()
        this.props.open(ev)
    }

    render() {
        return(
            <div className="commentAction cardAction">
                <MessageCircle size={24} color="#999" className={"commentIcon" +(this.props.comments && this.props.comments.length > 0? ' active':'')}
                                       onClick={(ev) => {this.handleCommentClick(ev)}}
                                       // onMouseEnter={() => this.props.showHoverPortal()}
                                       // onMouseLeave={() => this.props.hideHoverPortal()}
/*
                                       data-tip={"View card comments" }



*/
                />
                {this.props.comments.length > 0 &&
                <span className={"count"}>{this.props.comments.length}</span>
                }
            </div>
        )
    }
}
