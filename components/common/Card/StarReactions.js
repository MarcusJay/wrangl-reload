import {Component} from 'react'
import {getCardReactions} from "../../../services/CardApiService";
import {Star} from "react-feather";
import ReactionApiService from "../../../services/ReactionApiService";

const MAX_STARS = 5
const ActiveColor = '#aaa'
const HoverColor = '#bbb'

export default class StarReactions extends Component {

    state = {reactionSummary: null, hoverStarNum: -1, showStats: false}
    cardId = null

    onHover(starNum) {
        this.setState({hoverStarNum: starNum, showStats: true})
    }

    onLeave() {
        this.setState({hoverStarNum: -1, showStats: false})
    }

    submitRating(cardId, starNum) {
        ReactionApiService.registerStarReaction(cardId, starNum).then( response => {
            getCardReactions(this.props.cardId, false).then( response => {
                this.setState({reactionSummary: response.data})
                if (this.props.onVote)
                    this.props.onVote(response.data)
            })
        })
    }

    componentDidMount() {
        // details = false, so summary only
        this.cardId = this.props.cardId
        getCardReactions(this.props.cardId, false).then( response => {
            this.setState({reactionSummary: response.data})
        })
    }

    componentDidUpdate() {
        if (this.cardId !== null && this.cardId !== this.props.cardId) {
            this.cardId = this.props.cardId
            getCardReactions (this.props.cardId, false).then (response => {
                this.setState ({reactionSummary: response.data})
            })
        }
    }


    render() {
        const {cardId} = this.props
        const {reactionSummary, hoverStarNum, showStats} = this.state
        // if (!reactionSummary)
        //     return null


        const stars = [1,2,3,4,5]
        const starSize = 18

        let rating = 0, count = 0
        if (reactionSummary) {
            rating = reactionSummary.average || 0
            count = reactionSummary.card_reactions? reactionSummary.card_reactions.length : 0
        }

        return (
            <span className='inline relative fullW' style={{height: starSize+5, minWidth: '15rem'}}>
                {showStats &&
                <div className='abs tiny lighter z2 pad1 cardBG subtleShadow' style={{bottom: starSize+2, padding: '0.25rem'}}>
                    {'Average Rating: ' + (rating? rating.toPrecision(2):0) + ', Total votes: ' + count}
                </div>
                }

                {stars.map( (star, i) => {
                    const starNum = i+1
                    const selected = starNum <= rating
                    const selectedOnHover = starNum <= hoverStarNum
                    console.log('starNum: ' +starNum+ ', hoverStarNum: ' +hoverStarNum+ ', selected: ' +selected+ ', selOnHover: ' +selectedOnHover)
                    return (
                        <Star stroke-width={selected? 2:2}
                              fill={selectedOnHover? HoverColor : selected? ActiveColor : ''}
                              className={selected? 'selected':''}
                              size={starSize}
                              color={selectedOnHover? HoverColor : selected? ActiveColor : 'gray'}
                              style={{marginLeft: '0rem'}}
                              onClick={() => this.submitRating(cardId, starNum)}
                              onMouseEnter={() => this.onHover(starNum)}
                              onMouseLeave={() => this.onLeave()}
                        />
                    )
                })}

            </span>
        )
    }
}