import React from 'react'
import {findDOMNode} from "react-dom";

export default class ImagePainter2 extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            paths: [ [] ],
            isDrawing: false,
            x: 0,
            y: 0,
            dx: 0,
            dy: 0
        }
        this.prevX = -1
        this.prevY = -1
        this.prevWidth = 0
        this.prevHeight = 0
        this.prevImage = null
    }

    handleMouseDown = () => {
        if (!this.state.isDrawing) {
            this.setState({
                paths: [].concat(this.state.paths, [[]]),
            });
        }
        this.setState({ isDrawing: true });
    }

    handleMouseMove = (e) => {
        if (this.state.isDrawing) {
            const x = e.pageX - this.state.x;
            const y = e.pageY - this.state.y;
            const paths = this.state.paths.slice(0);
            const activePath = paths[paths.length - 1];
            activePath.push({ x, y });
            this.setState({ paths });
        }
    }

    handleMouseUp = () => {
        if (this.state.isDrawing) {
            this.setState({ isDrawing: false });
        }
    }

    clearCanvas = (ev) => {
        const {width, height} = this.props
        this.prevX = -1
        this.prevY = -1
        const container = findDOMNode (this.refs.paintBox);
        const rect = container.getBoundingClientRect ();
        // this.setState ({paths: [[]], isDrawing: false, x: rect.left + (width / 2), y: rect.top + (height / 2)});
        this.setState ({paths: [[]], isDrawing: false});
    }

    savePainting = () => {

    }

    componentDidMount() {
        const {image} = this.props
        const elem = findDOMNode(this);
        this.setState({elem: elem})
        this.prevImage = image

/*
        const {x, y} = this.state

        if (x !== this.prevX || y !== this.prevY) {
            const canvas = findDOMNode (this.refs.canvas);
            const elemRect = canvas.getBoundingClientRect ();
            this.prevX = x
            this.prevY = y
            this.setState ({x: elemRect.left - (width / 2), y: elemRect.top - (height / 2),});
        }

*/
    }

    componentDidUpdate(prevProps) {
        const {image, width, height} = this.props
        const dx = this.prevWidth? this.prevWidth - width : 0
        const dy = this.prevHeight? this.prevHeight - height : 0

        if (this.prevImage != image) { // STRINGS, loose equality
            this.savePainting()
            this.setState({paths: [[]], dx: dx, dy: dy})
            this.prevImage = image
            this.prevWidth = width
            this.prevHeight = height
        }

        const {x, y} = this.state

        if (x !== this.prevX || y !== this.prevY || this.prevImage != image) {
            const canvas = findDOMNode (this.refs.paintBox);
            const elemRect = canvas.getBoundingClientRect ();
            this.prevX = x
            this.prevY = y
            // this.setState ({x: elemRect.left - (width / 2), y: elemRect.top - (height / 2),});
            // this.setState ({x: elemRect.left - (width / 2) , y: elemRect.top - (height / 2) });
        }
    }


    render() {
        const {style, className, width, height} = this.props
        const {dx,dy} = this.state
        const ourStyle = {top: 0, width: width, height: height, margin: 'auto', backgroundColor: '#ff000020' }
        const combinedStyle = style? Object.assign(ourStyle, style) : ourStyle
        const paneClass = 'abs centered ' +(className? className : '')

        const paths = this.state.paths.map(_points => {
            let path = '';
            let points = _points.slice(0);
            if (points.length > 0) {
                path = `M ${points[0].x} ${points[0].y}`;
                var p1, p2, end;
                for (var i = 1; i < points.length - 2; i += 2) {
                    p1 = points[i];
                    p2 = points[i + 1];
                    end = points[i + 2];
                    path += ` C ${p1.x} ${p1.y}, ${p2.x} ${p2.y}, ${end.x} ${end.y}`;
                }
            }
            return path;
        }).filter(p => p !== '');

        return (
            <div ref='paintBox' className={paneClass} style={combinedStyle}>
                {/*dx: {dx}, dy: {dy}*/}
                <svg
                    style={{ cursor: 'crosshair' }}
                    width={width}
                    height={height}
                    ref="canvas"
                    onMouseDown={this.handleMouseDown}
                    onMouseUp={this.handleMouseUp}
                    onMouseMove={this.handleMouseMove}
                >
                    {/*<image x={0} y={0} xlinkHref={image} height={480} width={600} />*/}
                    {
                        paths.map(path => {
                            return (<path
                                key={path}
                                stroke="#ff0000a0"
                                strokeWidth={4}
                                d={path}
                                fill="none"
                            />);
                        })
                    }
                </svg>
            </div>
        );
    }
}

