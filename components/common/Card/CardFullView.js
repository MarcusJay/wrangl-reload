import React, {Component} from 'react'

import EmbedUtils from "utils/EmbedUtils";
import EmbedContainer from "../EmbedContainer";
import CardFullViewComments from "./CardFullViewComments";

import userStore from '/store/userStore'
import AuthProtector from "../AuthProtector";
import {isEmbeddable} from "/config/FileConfig";
import {isImage} from "../../../config/FileConfig";
import {handleBrokenImage} from "../../../utils/solo/handleBrokenImage";

export default class CardFullView extends Component {
    constructor(props) {
        super(props)
    }

    handleNav(ev) {
        this.props.handleNav(ev)
    }

    render() {
        const isCardEmbeddable = EmbedUtils.isEmbeddable(this.props.card.url) // TODO Replace with new FileConfig support
        const isAnon = !userStore.currentUser

        const { card, attm } = this.props
        const isAttmEmbeddable = attm? isEmbeddable(attm.file_type) : false
        const isAttmViewable = attm? isImage (attm.file_type) : false

        const attmCount = card && card.attachments? card.attachments.length : 0
        const imageClass = attmCount > 0? 'partial image':'full image'
        const embedClass = attmCount > 0? 'partial':'full'

        console.log('CardFUllView: My attm is: ' +(attm? attm.url : 'none') )
        return (
            <div className='cardFullView'>
                {/*<Transition.Group animation='browse' duration={500}>*/}
                    <div className='contentPane' >
                        {attm && attm.url && isAttmViewable &&
                        <span className='cfiBox'><img src={attm.url} className={imageClass} /></span>
                        }

                        {attm && attm.url && isAttmEmbeddable &&
                        <span className='cfiBox'>
                            <EmbedContainer url={attm.url} ext={attm.file_type} className={embedClass} />
                        </span>
                        }

                        {!attm && !isCardEmbeddable && card.image &&
                        <span className='cfiBox'>
                            <img src={card.image} className={imageClass}
                                 onClick={()=>this.props.handleClose()}
                                 onError={handleBrokenImage}
                            />
                        </span>
                        }

                        {!attm && !isCardEmbeddable && !card.image &&
                        <div className="empty image">
                            <div className="fvInfo">
                                <div className='t1'>{card.title}</div>
                                {card.description &&
                                <div className='d1'>{card.description}</div>
                                }


                            </div>
                        </div>
                        }

                        {isCardEmbeddable &&
                        <EmbedContainer url={card.url} className={embedClass}/>
                        }

                    </div>
                {/*</Transition.Group>*/}

                <div className='commentsPane'>
                    <AuthProtector isAnon={isAnon} component={
                        <CardFullViewComments card={card} handleNav={(ev) => this.handleNav (ev)}/>
                    }/>
                </div>
            </div>


        )
    }
}
