import {Component} from 'react'
import PropTypes from 'prop-types'

import {Icon} from 'semantic-ui-react'
import TagConfig from '/config/TagConfig'
import {abbrevOrTruncate} from "../../../utils/solo/abbrevOrTruncate";

export default class CardTag extends Component {
    static propTypes = {
        tag: PropTypes.any,
        tagColor: PropTypes.any
    }

    constructor(props) {
        super(props)
    }

    toggleTag() {
        this.props.toggleTag(this.props.tag)
    }

    render() {
        return (
            <span className="inlineTag">
                <span className="tagAbbrev"> {abbrevOrTruncate(this.props.tag, 3)}</span>
                <Icon name="square" size="large" style={{color: TagConfig.DefaultCardTags[this.props.tag]}}
                      onClick={() => {this.toggleTag()}}
                data-tip={this.props.tag}    />

            </span>
        )
    }
}