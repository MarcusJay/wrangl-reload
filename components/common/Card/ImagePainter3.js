import React from 'react'
import {observer} from 'mobx-react'
import {findDOMNode} from "react-dom";
import CanvasDraw from "react-canvas-draw";
import userStore from '/store/userStore'
import uiStateStore from "../../../store/uiStateStore";
import {saveAnnotation, sendImage} from "../../../services/ImageFileService";

@observer
export default class ImagePainter3 extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            isDrawing: false,
            saving: false,
            mergedMarkup: null
            // x: 0,
            // y: 0,
            // dx: 0,
            // dy: 0
        }
        // this.prevX = -1
        // this.prevY = -1
        // this.prevWidth = 0
        // this.prevHeight = 0
        this.prevImage = null
        this.drawing = null
        this.bgImage = null
        this.x = 0
        this.y = 0
    }

    handleMouseDown = (ev) => {
        this.x = ev.clientX
        this.y = ev.clientY
        // uiStateStore.paintDirty = true
        // uiStateStore.paintCount += 1
    }

    handleMouseUp = (ev) => {
        if (ev.clientX !== this.x || ev.clientY !== this.y) { // mouse must move to draw. Stationary clicks do not draw dots.
            // uiStateStore.paintDirty = true
            uiStateStore.paintCount += 1
            this.x = ev.clientX
            this.y = ev.clientY
        }
    }

    resetDrawing() {
        const canvas = this.refs.canvas
        if (canvas)
            canvas.clear()
    }

    componentDidMount() {
        const {image} = this.props
        const elem = findDOMNode(this);
        this.setState({elem: elem})
        this.prevImage = image
        uiStateStore.paintRef = this.refs.canvas
        // this.mergeMarkup()
    }

    componentDidUpdate(prevProps) {
        const {image, width, height} = this.props
        // const dx = this.prevWidth? this.prevWidth - width : 0
        // const dy = this.prevHeight? this.prevHeight - height : 0

        if (this.prevImage != image) { // STRINGS, loose equality
            this.prevImage = image
            this.resetDrawing()
            uiStateStore.paintRef = this.refs.canvas
            // this.mergeMarkup()

        }

    }

    render() {
        const {style, className, image, width, height, card} = this.props
        const {dx,dy, isDrawing, saving, mergedMarkup} = this.state
        const ourStyle = {top: 0, width: width, height: height, margin: 'auto' }
        const combinedStyle = style? Object.assign(ourStyle, style) : ourStyle
        const paneClass = 'abs centered ' +(className? className : '')
        const bgImage = mergedMarkup || card? card.image : null

        return (
            <div className={paneClass} style={combinedStyle}
                 onMouseUp={this.handleMouseUp}
                 onMouseDown={this.handleMouseDown}>
                <CanvasDraw
                    ref='canvas'
                    loadTimeOffset={1}
                    lazyRadius={1}
                    brushRadius={uiStateStore.paintBrushWidth}
                    brushColor={uiStateStore.paintColor}
                    catenaryColor={uiStateStore.paintColor}
                    gridColor={"rgba(150,150,150,0.1)"}
                    hideGrid={true}
                    canvasWidth={width}
                    canvasHeight={height}
                    disabled={false}
                    imgSrc={null}
                    saveData={false}
                    immediateLoading={false}
                    style={{margin: 'auto', backgroundColor: 'transparent'}}
                />
{/*
                <button className='jBtn button abs' style={{bottom: '.5rem', right: '-5rem'}}
                        onClick={this.saveDrawing}>
                    {saving? 'Saving...' : 'Save'}
                </button>
*/}

            </div>
        );
    }
}

