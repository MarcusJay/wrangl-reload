import {findDOMNode} from "react-dom";
import React from 'react'


export default class ImagePainter extends React.Component {

    currX = 0
    currY = 0
    color = 'red'
    drawing = false
    thickness = 5
    elem = null
    canvas = null

    draw = (ctx) => {
        if (this.mousedown) {
            ctx.fillStyle = this.color
            // start a path and paint a circle at mouse position
            ctx.beginPath()
            ctx.arc( this.mouseX, this.mouseY, this.thickness, 0, Math.PI*2, true )
            ctx.closePath()
            ctx.fill()
        }
    }

    saveDrawing = (ev) => {
        const {canvas} = this.state
        const drawingUrl = canvas.toDataURL()
        // TODO save it
    }

    onMouseDown = (ev) => {
        this.currX = ev.clientX //- canvas.offsetLeft;
        this.currY = ev.clientY //- canvas.offsetTop
        this.ctx.moveTo(this.currX, this.currY)
        this.drawing = true
    }

    onMouseUp = (ev) => {

    }

    onMouseMove = (ev) => {
        if( ev.offsetX ){
            this.mouseX = ev.offsetX;
            this.mouseY = ev.offsetY;
        } else {
            this.mouseX = ev.pageX - ev.target.offsetLeft;
            this.mouseY = ev.pageY - ev.target.offsetTop;
        }
        this.draw(ctx);

    }

    componentDidMount() {
        const {width, height} = this.props
        const elem = findDOMNode(this)
        if (!elem)
            return

        const canvas = elem.querySelector('canvas')
        if (!canvas)
            throw new Error('canvas missing')

        this.elem = elem
        this.canvas = canvas
        this.ctx = canvas.getContext( '2d' )
        // const ctx = canvas.getContext( '2d' )

        canvas.width = width
        canvas.height = height

        canvas.addEventListener( 'mousemove', this.onMouseMove)

        canvas.addEventListener( 'mousedown', this.onMouseDown)

        canvas.addEventListener( 'mouseup', this.onMouseUp)

    }

    componentDidUpdate() {
        const {width, height} = this.props
        this.elem.width = width
        this.elem.height = height
        this.canvas.width = width
        this.canvas.height = height

    }

    componentWillUnmount() {
        if (!this.canvas)
            return

        // TODO convert anon functions above to something we can reference here
        // this.canvas.removeEventListener('mousemove')
        // this.canvas.removeEventListener('mousedown')
        // this.canvas.removeEventListener('mouseup')
    }


    render() {
        const {style, className, width, height} = this.props
        const ourStyle = {top: 0, width: width, height: height, margin: 'auto' }
        const combinedStyle = style? Object.assign(ourStyle, style) : ourStyle
        const paneClass = 'abs centered jPinkBG ' +(className? className : '')

        return (
            <div className={paneClass} style={combinedStyle}>
                <canvas></canvas>
            </div>

        )
    }
}