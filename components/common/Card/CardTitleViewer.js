import React, {Component} from 'react'
import {observer} from 'mobx-react'

@observer
export default class CardTitleViewer extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {title, desc} = this.props
        const isHtml = desc && desc.startsWith('html:') && desc.length > 10

        return (
            <div className='titleOuter' >
                {desc && isHtml &&
                <div dangerouslySetInnerHTML={{ __html: desc.substring(5) }} />
                }
                {desc && !isHtml &&
                <div className='descBox tiny lighter' >{desc}</div>
                }
                {!desc && title &&
                <h2>{this.props.title}</h2>
                }
            </div>
        )
    }
}

