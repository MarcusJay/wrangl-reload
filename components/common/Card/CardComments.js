import {Component} from 'react'
import {findDOMNode} from 'react-dom'
import {Header, Input} from 'semantic-ui-react'
import {X} from 'react-feather'
import Moment from 'react-moment'

import userStore from '/store/userStore' // TODO replace with readonlyStore
import uiStateStore from '/store/uiStateStore'


import StateChangeRequestor, {REQUEST_CARD_COMMENT, REQUEST_CARD_COMMENT_DELETE} from '/services/StateChangeRequestor'
import {firstWord} from "../../../utils/solo/firstWord";


export default class CardComments extends Component {
    constructor(props) {
        super(props)

        // alleged antipattern if doing strict trickledown. But this component is refreshing its own comments.
        // To follow online patterns, move logic up at least 1 level. I'd rather keep Card a bit cleaner.
        this.state = {comments: this.props.card.comments || [], inputText:'', scrollerHeight: 0, parentElem: null }
        console.log("CArdComments: I have " +this.state.comments.length+ " comments.")

        this.onClick = this.onClick.bind(this)
        this.onChange = this.onChange.bind(this)
        this.setCommentsHeight = this.setCommentsHeight.bind(this)
        this.onKeyPress = this.onKeyPress.bind(this)
    }

    onClick(ev) {
        console.log('click. value: ' +ev.target)
        ev.stopPropagation()
        ev.preventDefault()
        ev.target.focus()


    }

    onKeyPress(ev) {
        console.log('key. value: ' +ev.key+ ", val: " +ev.target.value)
        if (ev.key === 'Enter')
            this.submitComment(ev)
    }

    submitComment(ev) {
        let text = ev.target.value

        StateChangeRequestor.awaitStateChange(REQUEST_CARD_COMMENT, {card: this.props.card, commentText: text})
            .then( comment => {
                console.log("Received back: " +comment.comment_text)
                this.setState({comments: [comment, ...this.state.comments], inputText: '' }, () => {
                    this.setCommentsHeight()
                })
                this.props.card.comments = this.state.comments // mutating our parent card. See antipattern note above.

                // Publish event
/*
                let user = userStore.currentUser
                let action = 'commented on'
                let target = this.props.card
                let parent = projectStore.currentProject
                let event = new WranglEvent(EVENT_ADD_COMMENT, user, action, target, parent, null, 1, text);
                PubSubService.publish(event)
*/
            })
    }

    deleteComment(ev, comment) {
        ev.stopPropagation()
        ev.preventDefault()

        StateChangeRequestor.awaitStateChange(REQUEST_CARD_COMMENT_DELETE, {commentId: comment.comment_id})
            .then( response => {
                if (response.success === true) {
                    let index = this.state.comments.findIndex( c => c.comment_id === comment.comment_id)
                    this.state.comments.splice(index,1)
                    this.setState({comments: this.state.comments, inputText: '' }, () => {
                        this.setCommentsHeight()
                    })
                    this.props.card.comments = this.state.comments // mutating our parent card. See antipattern note above.
                }
            })
    }

    onChange(ev) {
        this.setState({inputText: ev.target.value})
    }

    setCommentsHeight() {
        if (!this.state.parentElem) {
            return
        }

        if (this.props.showHeader === false)
            return (this.state.parentElem.offsetHeight * 0.9) + 'px'

        let scrollerMultiplier
        switch (this.props.card.totalComments) {
            case 0: scrollerMultiplier = 0.15; break
            case 1: scrollerMultiplier = 0.3; break
            case 2: scrollerMultiplier = 0.4; break
            default: scrollerMultiplier = 0.45
        }

        // this.setState({scrollerHeight: findDOMNode(this.props.cardElem).offsetHeight * scrollerMultiplier})
        return (this.state.parentElem.offsetHeight * scrollerMultiplier) + 'px'

    }

    componentWillMount() {
    }

    componentDidMount() {
        const cardDomNode = findDOMNode(this.props.cardElem)
        if (cardDomNode)
            this.setState({parentElem: cardDomNode}, () => {
                this.setCommentsHeight()
            })
        // TODO replace with REF
/*
        const inputElem = document.querySelector('#cInput')
        if (inputElem) // hidden in vo mode
            inputElem.focus()
*/
    }

    render() {
        let self = this
        const isAnon = !userStore.currentUser

        // TODO will it be possible to view comments by users not in your connections?
        this.state.comments.forEach( comment => {
            comment.user = userStore.getUserFromConnections(comment.comment_creator)
        })

        return(

            <div className="cardComments" >
                {false && this.props.showHeader &&
                <Header as='h4'>Comments</Header>
                }

                <div className="commentScroller" style={{maxHeight: this.setCommentsHeight()}}>
                    {!uiStateStore.voMode &&
                    <Input placeholder={isAnon ? 'Please login' : 'Add a note...'}
                           className="noteInput"
                           inverted
                           onClick={this.onClick}
                           onChange={this.onChange}
                           id="cInput"
                           value={this.state.inputText}
                           onKeyPress={this.onKeyPress}
                    />
                    }

                    {false && this.state.comments.length === 0 &&
                    <div className="secInfo inline">No Notes</div>
                    }
                    {this.state.comments.length > 0 &&
                    <div className='list'>
                        {this.state.comments.map (function (comment, i, comments) {
                            return (
                                <div key={'ccom'+i} className='cmt'>
                                    <div className='nText small'>{comment.comment_text}</div>

                                    <div className='tiny lighter inline meta'>- {comment.user? firstWord(comment.user.displayName): 'Disconnected user'}&nbsp;
                                        <Moment subtract={{hours: 7}} format={'MMM Do, h:mm a'}>{comment.time_modified}</Moment>
                                        {comment.user && self.props.user && comment.user.id === self.props.user.id &&
                                        <X size={16}
                                                         onClick={(ev) => self.deleteComment (ev, comment)}
                                                         className="abs delNote"/>
                                        }
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                    }
                </div>
            </div>
        )
    }
}