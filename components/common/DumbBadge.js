import {Component} from 'react'

export default class DumbBadge extends Component {
    render() {
        const {count, className, style, onClick} = this.props
        const combinedClass = 'dumbBadge ' +(className? className:'')

        return (
            <div className={combinedClass} style={style} onClick={onClick}>
                {count}
            </div>
        )
    }
}