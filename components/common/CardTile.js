// TODO
// Possible issue:
// Card's state is dictated by card.js AND cardspane.js
// It would be preferred to either make it controlled or not 100%.
// e.g. showComments is local state. showTags is parent props.

import {Component} from 'react'
import {observer} from 'mobx-react'
import {Button} from 'semantic-ui-react'
// import Editable from 'react-contenteditable'
import {ExternalLink} from 'react-feather';
import PropTypes from 'prop-types'
import {findDOMNode} from 'react-dom'
import {DragSource, DropTarget} from 'react-dnd'
import {DndItemTypes} from '/config/Constants'
import ResponsiveUtils from '/utils/ResponsiveUtils'

import CardModel from '/models/CardModel'
import userStore from '/store/userStore'
import uiStateStore, {CardAttachments} from '/store/uiStateStore'
import CardApiService from '/services/CardApiService'

import StateChangeRequestor, {
    REQUEST_CARD_ADD_TAG,
    REQUEST_CARD_DELETE_TAG,
    REQUEST_CARD_REACT,
    REQUEST_CARD_SAVE,
    REQUEST_CARD_UNREACT
} from '/services/StateChangeRequestor'
import CardTitleViewer from "/components/common/Card/CardTitleViewer"
import EmbedUtils from "/utils/EmbedUtils"
import cardStore from "../../store/cardStore";
import {getUrlComponents} from "../../utils/solo/getUrlComponents";
import TileAttms from "./Tile/TileAttms";
import {handleBrokenImage} from "../../utils/solo/handleBrokenImage";
import CardTagStrip from "./Card/CardTagStrip";
import MoreHoriz from "./MoreHoriz";
import {getCardDetails} from "../../services/CardApiService";
// import ColorThief from 'color-thief'

// card sizes
const SIZE_LARGE = 'large'
const SIZE_FIT = 'fit' // fit to screen size (with same proportions)
const SIZE_DEFAULT = 'default'

let origIndex = -1
const cardSource = {
    beginDrag(props) {
        uiStateStore.enableFileDrops = false
        origIndex = props.index
        // props.prepareMove()
        return {
            id: props.id,
            index: props.index,
            origIndex: origIndex // seems redundant, but isn't: index will be overwritten by final props.index, origIndex will not.
        }
    },
    endDrag(props, monitor) {
        uiStateStore.enableFileDrops = true
        let finalIndex = props.index
        console.error("### final index: " +finalIndex)
        return {
            id: props.id,
            index: props.index,
        }
    }
}

// let destIndex = -1
const cardTarget = {
    drop(props, monitor, component) {
        let draggedItem = monitor.getItem()

        // Tag drop
        if (draggedItem.tag !== undefined) {
            let card = props.card

            StateChangeRequestor.awaitStateChange (REQUEST_CARD_ADD_TAG, {
                cardId: props.card.id,
                tagId: draggedItem.id
            }).then (tags2 => {
                props.card.tags2 = tags2 // .data.card_tags.split('|')
                component.forceUpdate ()
            })

        // Card drop
        } else {
            const dragIndex = draggedItem.index
            const hoverIndex = props.index
            // Time to actually perform the action
            debugger
            if (draggedItem.origIndex !== hoverIndex)
                props.moveCard(draggedItem.id, draggedItem.origIndex, hoverIndex)
            // else
            //     props.cancelMove()
        }

    },

    hover(props, monitor, component) {
        let draggedItem = monitor.getItem()

        // Ignore tag hover, covered by css
        if (draggedItem.tag !== undefined) {
            return
        }

        const dragIndex = monitor.getItem().index
        const hoverIndex = props.index

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top

        // Only perform the move when the mouse has crossed half of the items height
        // When dragging downwards, only move when the cursor is below 50%
        // When dragging upwards, only move when the cursor is above 50%

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            // return
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            // return
        }

        props.hoverCard(dragIndex, hoverIndex)

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex
    }
}


@DropTarget([DndItemTypes.CARD, DndItemTypes.TAG], cardTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
}))
@DragSource(DndItemTypes.CARD, cardSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
}))
@observer
export default class Card extends Component {
    static propTypes = {
        connectDragSource: PropTypes.func,
        connectDropTarget: PropTypes.func,
        connectDragPreview: PropTypes.func,
        index: PropTypes.number,
        isDragging: PropTypes.bool,
        isOver: PropTypes.bool,
        canDrop: PropTypes.bool,
        item: PropTypes.any,
        id: PropTypes.any,
        text: PropTypes.string,
        moveCard: PropTypes.func,
        // cancelMove: PropTypes.func,
        // prepareMove: PropTypes.func,
        hoverCard: PropTypes.func,
        selectCard: PropTypes.func,
        editCard: PropTypes.func,
        deleteCard: PropTypes.func,
        filtered: PropTypes.bool
    }

    constructor(props) {
        super(props)
        this.state = {
            isFavorite: false,
            isExpanded: false,
            showComments: false,
            activeDropdownItem: '',
            showInfo: false,
            editField: null,
            prevClick: null,
            showPopup: false,
            showCommentsPopup: false,
            showCardActions: false,
            // yes these change initialization are necessary
            cardIsNew: false,
            cardIsUpdated: false,
            imageChanged: false,
            titleChanged: false,
            descChanged: false,
            urlChanged: false,
            commentsChanged: false,
            attmsChanged: false,
            // unseenShown: false,
            nWhatsNew: 0,
            showBell: false
        }
        // this.state.view = this.props.view || 'tiles' // necessitated by .. see TODO at top

        this.handleMenuItemSelect = this.handleMenuItemSelect.bind(this)
        this.handleImageChange = this.handleImageChange.bind(this)
        // this.setImageHeight = this.setImageHeight.bind(this)
        // this.highlightUnseenChanges(state)
    }

    clickCard(ev) {
        if (this.props.readOnly)
            this.props.selectCard(event.target, card)

        else if (!this.state.isExpanded)
            ; // this.openExpandedCard()

        // else
        //     this.closeExpandedCard()
    }

    hoverCard(isHover) {
        this.setState({isHover: isHover})
    }

    clickFullComments(ev) {
        // this.setState({view: 'cards'})
        uiStateStore.setCardView('cards')
    }

    // tag card is above in dropTarget
    unTagCard(tagId) {
        StateChangeRequestor.awaitStateChange (REQUEST_CARD_DELETE_TAG, {
            cardId: card.id,
            tagId: tagId
        }).then (tags2 => {
            card.tags2 = tags2 // .data.card_tags.split('|')
            this.forceUpdate ()
        })

    }

    flipCard(event) {
        event.stopPropagation(event)
        event.preventDefault()
        this.setState({showInfo: true})
        let flipElem = document.getElementById("flip" +card.domId)
        flipElem.style.transform = 'rotateY(180deg)';
    }

    unflipCard(event) {
        event.stopPropagation()
        event.preventDefault()
        this.setState({showInfo: false})
        let flipElem = document.getElementById("flip" +card.domId)
        flipElem.style.transform = 'none';
    }

    onPopupOpen(ev) {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({showPopup: true})
    }

    onPopupClose(ev) {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({showPopup: false})
    }

/*
    onCommentPreviewOpen(ev) {
        this.setState({loadingCard: true})
        // fetch comments via cardRead
        getCardDetails(card.id).then(cardDetails => {
            this.setState ({showCommentsPopup: true, loadingCard: false})
            let cardModel = new CardModel(cardDetails)
            Object.assign(card, cardModel)
        })
    }
*/

    onCommentPreviewClose(ev) {
        this.setState ({showCommentsPopup: false})
    }

    handleMenuItemSelect(ev, { name }) {
        ev.stopPropagation()
        ev.preventDefault()
        if (name.indexOf("Delete") === 0)
            this.deleteCard()
        this.setState({ activeDropdownItem: name })
    }


    toggleIsFavorite(event) {
        event.stopPropagation()
        this.setState({isFavorite: !this.state.isFavorite})
    }

    gotoLink = (ev) => {
        ev.stopPropagation()
        ev.preventDefault()
        if (this.props.card.url)
            window.open(this.props.card.url )
    }

    addAttachments(ev) {
        cardStore.getCardById(card.id).then( card => {
            uiStateStore.setEditingCard(card)
            uiStateStore.unModal = CardAttachments
        })
    }

    refreshCard() {
        getCardDetails(card.id).then(cardDetails => {
            let cardModel = new CardModel (cardDetails)
            Object.assign (card, cardModel) // TODO sin: merging in new properties to existing card
            this.forceUpdate()
        })
    }

    deleteCard(ev) {
        ev.stopPropagation()
        ev.preventDefault()
        this.setState({showPopup: false})
        this.props.deleteCard(this.props.card.id, this.props.index)
    }

    previousCard() {
        this.props.previous()
    }

    nextCard() {
        this.props.next()
    }

    // Image may have been uploaded or specified by url. Either way, we have its new hosted url.
    handleImageChange(image) {
        card.image = image
        if (image) {
            StateChangeRequestor.awaitStateChange (REQUEST_CARD_SAVE,
                {card: {id: card.id, image: image}}).then (updatedCard => {
                card.image = updatedCard.image
                this.setState (this.state) // force update
            })
        }
    }

    toggleReaction(reactionType, isActive, reaction) {

        let request = isActive ? REQUEST_CARD_UNREACT : REQUEST_CARD_REACT

        if (request === REQUEST_CARD_UNREACT && !reaction)
            throw new Error ("toggleReaction: cannot unreact without a reaction")

        let requestData
        if (request === REQUEST_CARD_REACT) {
            requestData = {card: card, reactionTypeId: reactionType.reaction_type_id}
        } else {
            requestData = {
                card: card,
                reactionTypeId: reactionType.reaction_type_id,
                reactionId: reaction.reaction_id
            }
        }
        StateChangeRequestor.awaitStateChange (request, requestData).then (response => {
            this.refreshCard ()
        }).catch (error => {
            console.error ("Error reacting to card: ", error)
            throw new Error (error)
        })
    }

    // check for unseen events of the type we're responsible for animating, and set related states accordingly
    highlightUnseenEvents = (eventMap) => {
        if (!eventMap)
            return

        const card = this.props.card

        let showBell = false
        debugger
        // TODO updatedKey is populated in realtime, but not in unseen. Must check both. TODO: Get api to support 1 100%
        const imageChanged = eventMap.updateEvents.some( event => event.detail === card.image || event.updatedKey === 'card_image')
        const titleChanged = eventMap.updateEvents.some( event => event.detail === card.title || event.updatedKey === 'card_title')
        const descChanged = eventMap.updateEvents.some( event => event.detail === card.description || event.updatedKey === 'card_description')
        const urlChanged = eventMap.updateEvents.some( event => event.detail === card.url || event.updatedKey === 'card_url')
        const attmsChanged = eventMap.updateEvents.some( event => event.detail && event.detail.indexOf('attachment') === 0)
        const commentsChanged = eventMap.commentEvents.length > 0
        const reactionsChanged = eventMap.reactionEvents.length > 0

        const cardIsNew = eventMap.isNew
        const cardIsUpdated = eventMap.updateEvents.length > 0 || commentsChanged || reactionsChanged

        console.log('Card ' +card.id+ ' highlighting unseen changes: ', imageChanged, titleChanged, descChanged, urlChanged)
        if (cardIsNew || cardIsUpdated || imageChanged || titleChanged || descChanged || urlChanged || commentsChanged || attmsChanged) {
            showBell = true
            debugger
            // this.resetAnimation()
        }

/*
        if (state) {
            state.showBell = showBell
            state.cardIsNew = cardIsNew
            state.imageChanged = imageChanged
            state.titleChanged = titleChanged
            state.descChanged = descChanged
            state.urlChanged = urlChanged
            state.commentsChanged = commentsChanged
            state.attmsChanged = attmsChanged
            state.unseenShown = true
            // state.bscounter = uiStateStore.bscounter

        } else {
*/
        this.setState ({
            showBell: showBell,
            cardIsNew: cardIsNew,
            cardIsUpdated: cardIsUpdated,
            imageChanged: imageChanged,
            titleChanged: titleChanged,
            descChanged: descChanged,
            urlChanged: urlChanged,
            commentsChanged: commentsChanged,
            attmsChanged: attmsChanged
        })

    }

    resetAnimation() {
        const elem = this.state.elem
        if (!elem)
            return

        const semCard = elem.firstChild.firstChild // TODO Perhaps we can remove nested flippable/side and semantic card

    }

    componentDidMount() {
        console.log('Card did mount')
        this.setState({elem: findDOMNode(this)})

    }

    componentWillReceiveProps(newProps) {
        console.log('Card will receive props')

        if (this.props.showUnseenEvents)
            this.highlightUnseenEvents(newProps.events)

    }

    componentWillUnmount() {
    }

    shouldComponentUpdate(nextProps, nextState) {
        let result = uiStateStore.showWhatsNew || !this.props.readOnly || (nextProps.selected !== this.props.selected) || (nextState.isExpanded !== this.state.isExpanded)
        console.log("Should Card " +this.props.card.title+ " update? " +result)
        return result
    }

    componentWillUpdate() {
        console.log('Card will update')
    }

    componentWillReact() {
        console.log('Card will react')
        // if (this.props.showUnseenEvents)
        //     this.highlightUnseenChanges()
    }

    render() {
        const {
            text,
            isDragging,
            isOver,
            canDrop,
            item,
            card, 
            connectDragSource,
            connectDropTarget,
        } = this.props
        const isActiveTarget = canDrop && isOver && item
        const { activeDropdownItem, cardIsNew, cardIsUpdated, imageChanged, titleChanged, descChanged, urlChanged, commentsChanged, attmsChanged } = this.state
        // console.log("Rendering Card " +card.title+ ", view: " +this.state.view+ ", url: " +card.url)
        const isHover = this.state.isHover // Note: isOver == dnd, isHover == cursor

        const popupTrigger = ( // wrap ellipse to allow imprecise clicking
            <div className="moreBox" data-tip='More Options...'><MoreHoriz size={24} style={{marginBottom: '0.3rem', marginLeft: '0.5rem'}}/></div>
        )

        const showBell = this.state.showBell

        // show changes
        const showChanges = true // !this.state.unseenShown // uiStateStore.bscounter !== this.state.bscounter || uiStateStore.showWhatsNew
        // const badgeColor = cardIsNew? '#f0592b' : cardIsUpdated? '#00a400' : '#777'
        const cardOutline = !showChanges? '' : cardIsNew? ' outlineOrange' : cardIsUpdated? ' outlineOrange' : ''
        // const imgGlow = imageChanged && showChanges
        // const titleGlow = titleChanged && showChanges? ' highlightGreen' : ''
        // const descGlow = descChanged && showChanges? ' highlightGreen' : ''
        // const urlGlow = urlChanged && showChanges? ' highlightGreen' : ''
        // const commentGlow = commentsChanged && showChanges? ' highlightGreen' : ''
        // const cardToolsFade = attmsChanged && showChanges? ' shadowOrange' : ''
        // const attmFlash = attmsChanged && showChanges? ' outlineOrange' : ''
        // const forceCardTools = attmsChanged && showChanges

        // if (this.state.imageChanged || this.state.titleChanged || this.state.descChanged || this.state.urlChanged || this.state.commentsChanged)
        //     console.log('Card: Showing Changes: ', cardGlow, imgGlow, titleFlash, descFlash, urlFlash, commentFlash)

        // style
        let tileClass = 'LeTile ' + cardOutline + (this.props.selected? 'selected' : this.state.isExpanded? 'expanded' : '')
        if (uiStateStore.cardView === 'images')
            tileClass += ' tight'
        if (!card.image)
            tileClass += 'noImg'
        if (isActiveTarget && item.hasOwnProperty('tag'))
            tileClass += ' dropTarget'
        else if (isActiveTarget || isDragging)
            tileClass += (item.index < this.props.index)? " sortTarget" : " sortTarget" // left/rightSortTarget
        const showTags = uiStateStore.forceShowTags || this.props.showTags
        const stickyInfo = false; // uiStateStore.stickyInfo

        // size
        let height = uiStateStore.tileHeight + 'rem'

        // user
        const creator = (userStore.currentUser && uiStateStore.showAuthors && card.creatorId !== userStore.currentUser.id)? userStore.getUserFromConnections(card.creatorId) : null
        const isAnon = !userStore.currentUser

        // title
        const title = card.title // (uiStateStore.showExtensions || isHover)? card.title : stripExt(card.title)  // this.props.showUnseenEvents.toString() //
        const titleClass = ''// 'dontWrap' + (size > 75? ' thinH3' : ' small') + titleGlow

        // attachments
        const isEmbeddable = EmbedUtils.isEmbeddable(card.url)
        let ext, iconName, preview
        let link = card.url || card.image
        if (link)
            link = getUrlComponents(link).host
        const dragWrapper = ResponsiveUtils.isMobile()? function(elem) {return elem} : connectDragSource

        return dragWrapper(
            connectDropTarget(
                <div className={tileClass} >
                <div id={card.domId} style={{height: height}}
                     onClick={(ev) => { this.clickCard(ev) }}
                     onMouseEnter={() => { this.hoverCard(true) }}
                     onMouseLeave={() => { this.hoverCard(false) }}
                >
                    <div className='toverlay'>
                        <div className='top'>
                            {card.image &&
                            <h2>{card.title}</h2>
                            }
                        </div>

                        <div className='mid'>
                            {card.attachments &&
                            <TileAttms card={card} titles={false} overlays={false}/>
                            }
                        </div>

                        {(card.url || card.image) &&
                        <div className='bottom'>
                            <Button icon={<ExternalLink/>} basic inverted style={{borderRadius: '5px'}}
                                    onClick={this.gotoLink}>
                            {link}
                        </Button>
                        </div>
                        }
                    </div>

                    {card.image &&
                    <img src={card.image} onError={handleBrokenImage} style={{height: height}}/>
                    }

                    {!card.image &&
                    <CardTitleViewer title={title} style={{height: height, width: height}}/>
                    }
                </div>
                    {card.tags2 && card.tags2.length > 0 && !this.state.isExpanded && showTags && uiStateStore.cardView !== 'images' &&
                    <CardTagStrip tags={card.tags2} untagCard={(tagId) => this.unTagCard(tagId)} cardId={card.id}/>
                    }
                </div>
            )
        )
    }
}


