import {Component} from 'react'
import {observer} from 'mobx-react'

import {DragSource} from 'react-dnd'
import {DndItemTypes} from '/config/Constants'
// import { Image, TextArea, Input } from 'semantic-ui-react'
import {Folder} from 'react-feather'

import ReactTooltip from 'react-tooltip'
import uiStateStore from "../../store/uiStateStore";

// import Utils from '~/utils/Utils'
// import StateChangeRequestor from '/services/StateChangeRequestor'
// import { REQUEST_PROJECT_COMMENT } from '/services/StateChangeRequestor'

const thumbSource = {
    beginDrag(props) {
        uiStateStore.enableFileDrops = false
        return {
            id: props.id,
            type: DndItemTypes.CARD_THUMB
        }
    },
    endDrag(props, monitor) {
        uiStateStore.enableFileDrops = true
        return {
            id: props.id,
            type: DndItemTypes.CARD_THUMB
        }
    }
}

@DragSource(DndItemTypes.CARD_THUMB, thumbSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
}))
@observer
export default class CardLibrary extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    selectCard() {
        ReactTooltip.rebuild()
        this.props.selectCard(this.props.card)
    }

    openFolder() {
        this.props.openFolder(this.props.card.path)
    }

    render() {
        const { isDragging, connectDragSource } = this.props
        const imgClass = "img" + (this.props.selected? " selected" :"")
        const tip = this.props.card.title // this.props.selected? ("Card '" +this.props.card.title + "' is selected to add.") : (this.props.card.title)
        const image = this.props.card.image

        if (this.props.card.type !== 'folder') {
            return connectDragSource (
                <div className='row' key={'pl' + this.props.card.id} onClick={() => {
                    this.selectCard ()
                }}
                     data-tip={tip}
                      data-type={this.props.selected ? "light" : 'dark'}
                     data-place="right">
                    {/*<img src={project.image} />*/}
                    <div className={imgClass} style={{backgroundImage: image ? 'url(' + image + ')' : 'none'}}></div>
                    <div className="noWrap ctitle darkSecondary">{this.props.card.title}</div>
                </div>
            )
        } else {
            return (
                <div className="folder" key={'pl' + this.props.card.id} onClick={() => this.openFolder() }>
                    <Folder size={24} />
                    <div className="noWrap ctitle darkSecondary">{this.props.card.title}</div>
                </div>
            )
        }
    }
}