import {Component} from 'react'

export default class DumbCapsule extends Component {
    render() {
        const {msg, className, style, onClick} = this.props
        const combinedClass = 'dumbCapsule ' +(className? className:'')

        return (
            <div className={combinedClass} style={style} onClick={onClick}>
                {msg}
            </div>
        )
    }
}