import React, {Component} from 'react'
import {Icon, Input} from 'semantic-ui-react'
import {SearchConfig} from '/config/Constants'
import RU from '/utils/ResponsiveUtils'

export default class CardSearch extends Component {
    constructor(props) {
        super (props)
        this.state = {query: '', searchResults: [], lastKeyPress: 0, searching: false, isVirgin: true, waiting: false}
        // this.importSelectedAssets = this.importSelectedAssets.bind (this)
    }

    handleOpen = () => this.setState ({modalOpen: true})

    handleClose = () => this.setState ({modalOpen: false})

    resetSearch() {
        this.setState ({isLoading: false, searchResults: [], value: ''})
    }

    onChange(ev) {
        let query = ev.target.value
        console.log('change: query = ' +query)
        let now = new Date()
        let elapsed = this.state.lastKeyPress === 0? 0 : now - this.state.lastKeyPress
        this.setState({query: ev.target.value, lastKeyPress: now})

        setTimeout( () => {
            if (query.length >= SearchConfig.MIN_SEARCH_CHARS) {
                this.submitSearch (query)
            } else if (!this.state.isVirgin) {
                console.log ('Clearing search')
                this.props.handleResults()
            } else {
                console.log('Skipping search ' + query + ' at ' +elapsed+ 'ms')
            }
            this.setState ({waiting: false})

        }, SearchConfig.QUERY_DELAY_MS )

    }

    submitSearch(query) {
        console.log("Searching for " +query+ "...")

        this.setState ({searching: true, isVirgin: false})
        this.props.handleResults(query)
    }


  render() {
    const device = RU.getDeviceType()
    const width = device === 'computer'? '15rem':'10rem'

    return (
        <span className='relative'>
            <input name='Search'
                   data-tip='Search for cards by title and description'
                   onChange={(ev) => {this.onChange (ev)}}
                   value={this.state.query}
                   id="cardSearch"
                   className='jFG vaTop'
                   style={{width: width, height: '2.1rem', marginRight: '0.75rem', border: 'none!important'}}
                   placeholder='Filter...'
            />
            <span className='abs' style={{right: '1rem', top: '-10px'}}>
                <Icon name="search" className='jSec'/>
            </span>

        </span>
    )
  }
}
