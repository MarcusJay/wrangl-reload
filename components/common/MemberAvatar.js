import React, {Component} from 'react'
import {Image} from 'semantic-ui-react'
import {Plus, X} from 'react-feather'
import {Link, Router} from '~/routes'
import {hasRole, isActive} from "../../models/ProjectModel";
import {firstWord} from "../../utils/solo/firstWord";
import MemberAvatarPopup from "./MemberAvatarPopup";
import {UserRole} from "../../config/Constants";

export default class MemberAvatar extends Component {

    state = {showTip: false}

    overUser = () => {
        this.setState({showTip: true})
        if (this.props.overUser)
            this.props.overUser(this.props.member)
    }

    clearUser = () => {
        this.setState({showTip: false})
        if (this.props.clearUser)
            this.props.clearUser()
    }

    handleClick = () => {
        const {clickAction, isEnabled, member} = this.props
        if (clickAction && isEnabled)
            clickAction(member)
    }

    render() {
        const {member, activeUser, enableAdmin, showMenu, showAttmMenu, inMsgId, index, project, refresh, style, size,
               isEnabled, isChanged, showName, showBorder, removeAttm, attachUser, unattachUser, clickAction} = this.props
        if (!member)
            return null

        const {showTip} = this.state
        const isDragged = index < 0
        const isConfirmed = isActive( member.id, project) || isDragged
        const isRequestor = hasRole( member.id, UserRole.REQUESTOR, project)
        const isApprover = hasRole( member.id, UserRole.APPROVER, project)
        const tip = isConfirmed? member.displayName : member.displayName + ' (Invited)'
        let combinedStyle = {padding: showName? '.2rem 1rem .2rem .2rem':0}
        combinedStyle = Object.assign(combinedStyle, style)
        let memberClass = 'member inline' +(isEnabled? ' boh pointer':'') +(showName? ' borderAll capsule':' round')
        const nameClass = 'inline tiny jSecInv' +(isEnabled? 'hoh pointer' : '')
        const memberGlow = isChanged? ' glow':''
        const firstName = member.firstName || firstWord(member.displayName)
        const route = isEnabled? 'profile' : ''
        const imgBorderStyle = showBorder? {border: '2px solid rgba(250,250,250,0.9)'}:{}
        const imgSize = size? {width: size, height: size} : {}
        const imgClass = 'image' + (showName? ' mr05':'mb05') + memberGlow + (isConfirmed || isApprover || isRequestor || inMsgId? '' : ' invited')
        const imgStyle = Object.assign({}, imgBorderStyle, imgSize)

        const avatar = (
            <span className={memberClass} style={combinedStyle} data-tip={tip}
                  onClick={this.handleClick}
                  onMouseEnter={this.overUser}
                  onMouseLeave={this.clearUser}
            >
                <Image inline src={member.image}
                       className={imgClass}
                       style={imgStyle}
                       data-tip={tip}/>
                {false && !isConfirmed && isEnabled && !inMsgId && !showAttmMenu &&
                <Plus size={14} className='invitedIcon' style={{color: this.props.color}}/>
                }

                {false && isApprover &&
                <span className='small highlight abs round' style={{bottom: '-.15rem', right: '-.15rem'}}>A</span>
                }

                {false && isRequestor &&
                <span className='small orange abs round' style={{bottom: '-.15rem', right: '-.15rem'}}>R</span>
                }

                {showName &&
                <span className={nameClass}>{firstName}</span>
                }
                {showAttmMenu &&
                <X size={16} className='abs fg round hidden attmAction'
                   style={{top: '-5px',right:'-5px'}}
                   onClick={removeAttm? () => removeAttm(member.id) : null}
                />
                }

            </span>
        )

        if (!showMenu && !showAttmMenu && !clickAction) {
            return (
                <Link route={route} params={{id: member.id}}>
                    {avatar}
                </Link>
            )
        }

        if (showAttmMenu || clickAction) {
            return avatar
        }

        if (showMenu) {
            return <MemberAvatarPopup
                    trigger={avatar}
                    member={member} activeUser={activeUser}
                    inMsgId={inMsgId}
                    enableAdmin={enableAdmin}
                    attachUser={attachUser}
                    unattachUser={unattachUser}
                    refresh={refresh}
            />
        }
    }
}

