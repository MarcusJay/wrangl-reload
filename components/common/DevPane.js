import {Component} from 'react'
import {observer} from 'mobx-react'

import RU from "../../utils/ResponsiveUtils";
import uiStateStore from '/store/uiStateStore'

@observer
export default class DevPane extends Component {

    render() {
        if (!uiStateStore.showDev)
            return null

        // Dev
        debugger
        const device = RU.getDeviceType()
        const deviceWidth = RU.getViewPortWidth()
        const isMobile = RU.isMobile()? '1':'0'
        const isTablet = RU.isTablet()? '1':'0'
        const uaIsMobile = RU.isUaMobile? '1':'0'
        const uaIsTablet = RU.isUaTablet? '1':'0'

        const osName = RU.os && RU.os.name? RU.os.name.toString() : 'unavailable'
        const osVersion = RU.os && RU.os.version? RU.os.version.toString() : 'unavailable'
        const dVendor = RU.device && RU.device.vendor? RU.device.vendor.toString() : 'unavailable'
        const dModel = RU.device && RU.device.model? RU.device.model.toString() : 'unavailable'
        const dType = RU.device && RU.device.type? RU.device.type.toString() : 'unavailable'
        const bName = RU.browser && RU.browser.name? RU.browser.name : 'unavailable'

        return(
            <div className='devPane'>
                {device +':' +deviceWidth+ '. m: ' +isMobile+ ', t: ' +isTablet+ '. uam: ' +uaIsMobile+ '. uat: ' +uaIsTablet}<br/>
                browser: {bName}<br/>
                os: {osName}, v: {osVersion}<br/>
                device vendor: {dVendor}, model: {dModel}, type: {dType}<br/>
                uaString: {RU.uaString}<br/>

            </div>
        )
    }
}