import {Component} from 'react'
import {observer} from 'mobx-react'

import {DragSource} from 'react-dnd'
import {DndItemTypes} from '/config/Constants'
// import { Image, TextArea, Input } from 'semantic-ui-react'
import {FileText, Folder} from 'react-feather'

import ReactTooltip from 'react-tooltip'
import DropboxService from "/integrations/DropboxService";
import uiStateStore from "../../store/uiStateStore";

// import Utils from '~/utils/Utils'
// import StateChangeRequestor from '/services/StateChangeRequestor'
// import { REQUEST_PROJECT_COMMENT } from '/services/StateChangeRequestor'

const thumbSource = {
    beginDrag(props) {
        uiStateStore.enableFileDrops = false
        return {
            id: props.id,
            type: DndItemTypes.DROPBOX_THUMB,
            asset: props.card
        }
    },
    endDrag(props, monitor) {
        uiStateStore.enableFileDrops = true
        return {
            id: props.id,
            type: DndItemTypes.DROPBOX_THUMB,
            asset: props.card
        }
    }
}

@DragSource(DndItemTypes.DROPBOX_THUMB, thumbSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
}))
@observer
export default class CardLibrary extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    selectCard() {
        ReactTooltip.rebuild()
        this.props.selectCard(this.props.card)
    }

    openFolder() {
        this.props.openFolder(this.props.card.path)
    }

    // TODO Replace this with FileConfig call
    isImageFile(card) {
        return (card.path && (['.jpg','.png','.jpeg','.gif', '.bmp', '.webp'].lastIndexOf(card.path.toLowerCase()) !== -1))
    }

    render() {
        const { isDragging, connectDragSource } = this.props
        const imgClass = "img" + (this.props.selected? " selected" :"")
        const tip = this.props.card.title // this.props.selected? ("Card '" +this.props.card.title + "' is selected to add.") : (this.props.card.title)
        const previewData = this.props.card.image
        const isFolder = this.props.card.type === 'folder'
        const mimeType = (!isFolder && this.props.card.path)? DropboxService.getMimeType( this.props.card.path ) : ''

        // console.log('Thumb ' +this.props.card.title + ' mime type: ' +mimeType)
        if (!isFolder) {
            return connectDragSource (
                <div className='row'
                     data-tip={tip} data-type={this.props.selected ? "light" : 'dark'} data-place="right">

                    {previewData && mimeType &&
                    <img className="img" src={"data:" +mimeType+ ";base64," +previewData} />
                    }
                    {(!previewData || !mimeType) &&
                    <FileText size={32} className="folder file"/>
                    }

                    <div className="noWrap ctitle">{this.props.card.title}</div>
                </div>
            )
        } else {
            return (
                <div className="folder" key={'pl' + this.props.card.id} onClick={() => this.openFolder() }>
                    <Folder size={24} />
                    <div className="noWrap ctitle">{this.props.card.title}</div>
                </div>
            )
        }
    }
}