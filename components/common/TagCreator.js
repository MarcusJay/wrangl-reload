import {Component} from 'react'
import {Plus} from 'react-feather'
import {Popup} from "semantic-ui-react";
import {SketchPicker} from "react-color";

import tagStore from '/store/tagStore'
import ColorUtils from "../../utils/ColorUtils";

export default class TagCreator extends Component {
    constructor(props) {
        super(props)
        this.newTagColor = ColorUtils.getNextColor()
        this.state = {name: '', color: this.newTagColor, showForm: false, showEmptyNameError: false}
    }

    toggleForm(ev) {
        const newShowForm = !this.state.showForm
        this.setState({showForm: newShowForm})
        if (newShowForm) {
            this.setState({name: ''})
        }
    }

    handleKeyPress(ev) {
        if (ev.key === 'Enter') {
            ev.preventDefault()
            this.saveTag()
        }
    }

    handleChange(ev) {
        this.setState({name: ev.target.value})
    }

    handleColorChangeComplete = (color, ev) => {
        this.setState({color: color.hex})
        // this.props.updateTag(this.props.tag.name, color.hex, this.props.tag )
    }

    saveTag = (ev) => {
        const {name, color} = this.state
        if (!name || name.length === 0) {
            this.setState ({showEmptyNameError: true})
            return
        }

        tagStore.addTagToCardableSet(this.props.project.id, name, color).then( tagSet => {
            this.newTagColor = ColorUtils.getNextColor()
            this.setState({name: '', color: this.newTagColor, showForm: false})
        })
    }

    render() {
        const { defaultOpenForm } = this.props
        const { showForm, name, color, showEmptyNameError } = this.state

        return (
            <div className="abs inline" style={{top: '3px', marginLeft: '0.5rem'}}>
                {!defaultOpenForm &&
                <Plus size={24} className='secondary pointer' onClick={() => this.toggleForm ()}
                      style={{marginBottom: '-6px'}}/>
                }

                {(showForm || defaultOpenForm) &&
                <span style={{marginLeft: '0.5rem'}}>
                    <input type="text"
                           onKeyPress={(ev) => {this.handleKeyPress (ev)}}
                           placeholder={showEmptyNameError? 'Please choose a name!' : "Create"}
                           onChange={(ev) => this.handleChange (ev)}
                           value={name}
                    />

                    <Popup style={{padding: 0}}
                           inverted
                           trigger={
                               <span className='swatch inline pointer' style={{color: color, backgroundColor: color, marginLeft: '0.5rem', marginBottom: '2px'}}>&nbsp;</span>
                           }
                           on='click'
                           position='bottom right'
                    >
                        <SketchPicker disableAlpha presetColors={['#B80000', '#DB3E00', '#FCCB00', '#008B02', '#006B76', '#1273DE', '#004DCF', '#5300EB', '#EB9694', '#FAD0C3', '#FEF3BD', '#C1E1C5', '#BEDADC', '#C4DEF6', '#BED3F3', '#D4C4FB']}
                                      width={220} color={color}
                                      // onChange={ this.handleColorChange }
                                      onChangeComplete={ this.handleColorChangeComplete }
                        />
                    </Popup>

                    <div className={'large inline pointer ' +(name && name.length > 0? ' secondary':'third')}
                         style={{marginLeft: '0.5rem'}} onClick={this.saveTag}>Save</div>
                </span>
                }
            </div>
        )
    }
}