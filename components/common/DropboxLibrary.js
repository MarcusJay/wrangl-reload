import {Component} from 'react'
import {observer} from 'mobx-react'

import {Icon, Loader} from 'semantic-ui-react'
import {ArrowUp} from 'react-feather'

import DropboxService from '/integrations/DropboxService'

import uiStateStore from '/store/uiStateStore'
import integrationStore from "/store/IntegrationStore";
import DropboxThumb from "./DropboxThumb";
import {isBrowser} from "../../utils/solo/isBrowser";

@observer
export default class DropboxLibrary extends Component {
    constructor(props) {
        super(props)
        // this.toggleSelectCard = this.toggleSelectCard.bind(this)
        // this.importSelectedCards = this.importSelectedCards.bind(this)
        this.state = {selectedCardIds: {}, searching: false, connecting: true, importing: false,
                      query: '', searchResults: [], lastKeyPress: 0, isVirgin: true, waiting: false, elem: null}
        console.log('234 constructor: isBrowser? ' +isBrowser() )
    }

/*
    listFiles() {
        this.setState({connecting: true})
        integrationStore.listFilesInFolder().then( response => {
            this.setState({connecting: false, searchResults: response.data.items})
        })
    }
*/

    drillDown(folderPath) {
        this.setState({connecting: true})
        integrationStore.dropboxDrilldown(folderPath)
    }

    drillUp() {
        this.setState({connecting: true})
        if (!integrationStore.isRoot)
            integrationStore.dropboxDrillup()
    }

/*
    onSearchChange(ev) {
        let query = ev.target.value
        console.log('change: query = ' +query)
        let now = new Date()
        let elapsed = this.state.lastKeyPress === 0? 0 : now - this.state.lastKeyPress
        this.setState({query: ev.target.value, lastKeyPress: now})

        setTimeout( () => {
            if (query.length >= SearchConfig.MIN_SEARCH_CHARS) {
                this.submitSearch (query)
            } else if (!this.state.isVirgin) {
                console.log ('Clearing search')
                this.setState ({searchResults: this.props.userCards})
            } else {
                console.log('Skipping search ' + query + ' at ' +elapsed+ 'ms')
            }
            this.setState ({waiting: false})

        }, SearchConfig.MIN_DELAY_MS )

    }

    submitSearch(query) {
        console.log("Searching for " +query+ "...")

        this.setState ({searching: true, isVirgin: false})

        CardApiService.findCards (query.toLowerCase()).then (response => {
            if (!response.data || response.data.length === 0)
                throw new Error ('Card search failed')

            this.setState ({
                searching: false,
                searchResults: response.data.cards.map (apiCard => {
                    return new CardModel (apiCard)
                }) || []
            })
        }).catch (error => {
            console.error ("Error searching for " + query, error)
            this.setState ({searching: false})
            throw new Error (error)
        })
    }
*/

    toggleSelectCard(card) {
        // TODO Ignore selections in PL until there's a UI for selecting destination project
        if (uiStateStore.currentPage !== uiStateStore.PD)
            return

        console.log("selectCard: " +card.card_title);
        if (!this.state.selectedCardIds[ card.id ])
            this.state.selectedCardIds[ card.id ] = card
        else
            delete this.state.selectedCardIds[ card.id ]
        this.setState(this.state)

    }

    importSelectedCards() {
        this.setState({importing: true})
        let cardArray = Object.keys(this.state.selectedCardIds)
        if (cardArray.length === 0) {
            console.error("importSelectedCards: 0 cards selected.")
            return
        }

        this.props.importCards(cardArray)
        this.resetSelectedCards()

    }

    resetSelectedCards() {
        this.setState({selectedCardIds: {}, importing: false, searchResults: this.props.userCards})
    }

    openAuthInNewTab() {
        const dropboxAuthUrl = DropboxService.getAuthorizationUrl()
        const authWindow = window.open(dropboxAuthUrl, '_blank', 'top=200,left=200,height=500,width=600,menubar=no,location=no,resizable=no,scrollbars=no,status=no')
    }

    componentWillReceiveProps(newProps) {
    }

    componentDidMount() {
    }

    componentWillReact() {
/*
        if (integrationStore.dropboxLoading)
            this.setState({searchResults: []})
        else if (integrationStore.dropboxCurrentFiles)
            this.setState({searchResults: integrationStore.dropboxCurrentFiles})
*/
    }

    render() {
        if (!this.state.searchResults)
            return null
        const connected = integrationStore.isDropboxAuthorized() // && integrationStore.dropboxCurrentFiles !== null
        const loading = integrationStore.dropboxLoading

        const fullScreen = this.props.mode === 'full'

        const selectedCardCount = this.state.selectedCardIds? Object.keys(this.state.selectedCardIds).length : 0
        const isRoot = connected? integrationStore.dropboxNavStack.length === 0 : false
        const containerClass = fullScreen? 'main-pane' : this.props.mobile? 'm-side-pane':'side-pane'
        const hasFiles = integrationStore.dropboxCurrentFolder && integrationStore.dropboxCurrentFiles && integrationStore.dropboxCurrentFiles.length > 0

        if (!connected) {
            return (
                <div className={containerClass}>
                    <div className="hoh" onClick={() => this.openAuthInNewTab()}>
                        <Icon name="dropbox" size="big" className='darkSecondary' style={{display: 'inline-block', marginLeft: '0.5rem'}}/>
                        <div className="pointer" className='darkSecondary' style={{marginLeft: '0.5rem', display: 'inline'}}>Connect Now </div>
                    </div>

                </div>
            )
        }

        // style={{top: '40%'}}
        return(
            <div className={containerClass} style={{position: 'relative'}}>
                <Loader active={loading}></Loader>

                {!loading &&
                <div className="thumbLibrary subtle-scroll-dark">
                    <div className='currDir' onClick={() => this.drillUp()}>
                        {!isRoot &&
                        <span>
                            <ArrowUp size={12} style={{marginBottom: '-1px'}}/>
                            <span>{integrationStore.dropboxCurrentFolder}</span>
                        </span>
                        }
                    </div>
                    {hasFiles && integrationStore.dropboxCurrentFiles.map( (card,i) => {
                        return (
                            <DropboxThumb
                                card={card}
                                id={card.id}
                                key={'k'+i}
                                selectCard={this.toggleSelectCard}
                                openFolder={(folder) => this.drillDown(folder)}
                                // selected={this.state[ cardModel.domId ] === true}
                                selected={this.state.selectedCardIds[ card.id ] !== undefined}
                            />
                        )
                    })}
                </div>
                }
            </div>
        )
    }
}