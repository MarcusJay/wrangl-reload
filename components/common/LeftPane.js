import React, {Component} from 'react'
import {Link} from '~/routes'
import {observer} from 'mobx-react'
import ContactLibrary from '/components/ProjectSummaries/LeftPane/ContactLibrary'
import ProjectLibrary from "/components/ProjectDetail/LeftPane/ProjectLibrary";
import projectStore from "/store/projectStore";
import {MsgLibrary} from "/config/Messages";
import uiStateStore from "/store/uiStateStore";
import userStore from "/store/userStore";
import InlineHint from "/components/onboarding/InlineHint";
import {isBrowser} from "/utils/solo/isBrowser.js";
import {ArrowLeft, ChevronRight} from "react-feather";
import {ManageContacts} from "../../store/uiStateStore";
import LeftUnmodalContainer from "../unmodals/LeftUnmodalContainer";
import ApprovalLibrary from "../Approvals/ApprovalLibrary";
import {APPROVAL_CAT, DEFAULT_CAT, LeftPaneTab, REQUEST_CAT} from "../../config/Constants";
import DumbBadge from "./DumbBadge";
import eventStore from "../../store/eventStore";

const lineColor = '#343C46'
const activeLineColor = '#47C1F1'

export const INPUT_HEIGHT = 2
export const SECTION_HEIGHT = 4

@observer
export default class PLLeftPane extends Component {

    state = {showMore: false}

    toggleMore = (ev) => {
        this.setState({showMore: !this.state.showMore})
    }

    showContacts = () => {
        uiStateStore.showSidebar = true
        uiStateStore.lpTab = LeftPaneTab.CONTACTS
        userStore.setUserPref (null, 'left_pane_tab', LeftPaneTab.CONTACTS)
        userStore.setUserPref (null, 'left_pane_open', true)
    }

    showBoards = () => {
        uiStateStore.showSidebar = true
        uiStateStore.lpTab = LeftPaneTab.PROJECTS
        userStore.setUserPref (null, 'left_pane_tab', LeftPaneTab.PROJECTS)
        userStore.setUserPref (null, 'left_pane_open', true)
    }

    showApprovals = () => {
        uiStateStore.showSidebar = true
        uiStateStore.lpTab = LeftPaneTab.APPROVALS
        userStore.setUserPref (null, 'left_pane_tab', LeftPaneTab.APPROVALS)
        userStore.setUserPref (null, 'left_pane_open', true)
    }

    showBoth() {
        uiStateStore.showSidebar = true
    }

    toggleEditContacts() {
        // uiStateStore.unModal = ManageContacts
        uiStateStore.maximizeLeftCol = !uiStateStore.maximizeLeftCol
        setTimeout (() => {
            uiStateStore.leftUnModal = ManageContacts
        }, 250)
    }

    filterByUser = (userId) => {
        this.props.filterByUser (userId)
    }

    refreshMembers = () => {
        projectStore.fetchCurrentProject (projectStore.currentProject.id).then (project => {
        })
    }


    minimizePane = (ev) => {
        uiStateStore.minimizeLeftCol = true
    }

    restorePane = (ev) => {
        uiStateStore.minimizeLeftCol = false
    }

    toggleMobSidebar = (ev) => {
        uiStateStore.toggleMobSidebarL()
    }

    // use consts
    switchThemes = (ev) => {
        if (uiStateStore.theme === 'dark')
            uiStateStore.theme = 'light'
        else
            uiStateStore.theme = 'dark'
    }

    componentDidMount() {
        if (userStore.userPrefs && uiStateStore.lpTab < 0)
            uiStateStore.lpTab = userStore.userPrefs.selectedLeftPaneTab || LeftPaneTab.CONTACTS
        if (!userStore.userPrefs)
            userStore.getUserPrefs()
    }

    componentDidUpdate() {
        if (userStore.userPrefs && uiStateStore.lpTab < 0)
            uiStateStore.lpTab = userStore.userPrefs.selectedLeftPaneTab || LeftPaneTab.CONTACTS
    }

    componentWillReact() {
        console.warn('LeftPane will react')
    }

    render() {
        const {mobile, project, projects, approvals} = this.props
        const {showMore} = this.state
        const tab = uiStateStore.lpTab
        const scrollClass = mobile ? ' subtle-scroll-dark' : ' hidden-scroll'
        const paneStyle = uiStateStore.currentPage === uiStateStore.PD ? {
            // overflowY: 'auto',
            paddingTop: 0,
            marginTop: '-2rem'
        } : {overflowY: 'auto'}
        const isOpen = uiStateStore.showSidebar || (mobile && uiStateStore.showMobSidebarL)
        const inProject = uiStateStore.currentPage === uiStateStore.PD && projectStore.currentProject

        const evs = eventStore.getInstance()
        const dmCount = evs.currentBadges && evs.currentBadges.direct_messages? evs.currentBadges.direct_messages.total : 0
        const cagBadges = evs.currentBadges? evs.currentBadges.categories : null
        const apOnMeCount = cagBadges && cagBadges[APPROVAL_CAT]? cagBadges[APPROVAL_CAT].total : 0
        const apOnOthersCount = cagBadges && cagBadges[REQUEST_CAT]? cagBadges[REQUEST_CAT].total : 0
        const projectEventCount = cagBadges && cagBadges[DEFAULT_CAT]? cagBadges[DEFAULT_CAT].total : 0
        const apTotalCount = apOnMeCount + apOnOthersCount

        /*
                if (uiStateStore.minimizeLeftCol) {
                    return (
                        <div className={'utLeftPane left-col darkFraming' +scrollClass+ (mobile? ' mobileBG':'')} style={{ overflowY: 'auto' }}
                             onClick={this.restorePane} data-tip="Open Navigator">
                            <ChevronRight size={22} className='third subtleBtn'/>


                            <div className={'small third pointer rotate90cw'}
                                 style={{margin: '4.5rem 0 0 -4.25rem', fontSize: '0.7rem', minWidth: '8rem'}}>
                                Navigator
                            </div>
                        </div>

                    )
                }
        */

/*
        if (!isOpen) {
            return (
                <nav className='centered' style={{padding: '1.5rem 1rem'}}>
                    <span className='utAddContact inline relative'>
                        <img src="/static/svg/v2/contacts.svg" width='26px'
                             className='block mb2' style={{margin: '0 auto 2rem auto'}} onClick={this.showContacts}/>
                        {dmCount > 0 &&
                        <DumbBadge count={dmCount} className='abs' style={{top: '-.75rem', right: '-.75rem'}}/>
                        }

                    </span>
                    <span className='inline relative'>
                        <img src='/static/svg/v2/briefcase.svg' width='26px'
                             className='block mb2' style={{margin: '0 auto 2rem auto'}} onClick={this.showBoards}
                             data-tip="View Boards"/>
                        {projectEventCount > 0 &&
                        <DumbBadge count={projectEventCount} className='abs' style={{top: '-.75rem', right: '-.75rem'}} />
                        }

                    </span>
                    <span className='inline relative'>
                        <img src='/static/svg/v2/checkCircleInv.svg' width='24px'
                             className='block mb2' style={{margin: '0 auto 2rem auto'}} onClick={this.showApprovals}
                             data-tip="View Approvals"/>

                        {apTotalCount > 0 &&
                        <DumbBadge count={apTotalCount} className='abs' style={{top: '-.75rem', right: '-.75rem'}} />
                        }

                    </span>
                </nav>
            )
        }
*/

        if (uiStateStore.maximizeLeftCol && uiStateStore.leftUnModal !== null) {
            return <LeftUnmodalContainer/>
        }

        return (
            <div className={'left-col darkFraming' + scrollClass + (mobile ? ' mobileBG' : '')}
                 style={paneStyle}>

                {mobile &&
                <div style={{padding: '.5rem'}}>
                    <Link route='projectsPage'>
                        <img src="/static/img/pogoMulti.png" width="64px" className='pointer' data-tip="Home"
                             style={{padding: '2rem 0 0 1rem'}}
                        />
                    </Link>

                    {isOpen &&
                    <span onClick={this.toggleMobSidebar} >
                        <ArrowLeft size={26} className='jSec abs hoh pointer' style={{right: '1rem', top: '1rem'}}/>
                    </span>
                    }

                </div>
                }

                <nav className='utLeftPane lpTabs centered'>
{/*
                    <div className='lpTab inline hoh relative fullW' onClick={this.showContacts}
                         style={{paddingTop: '10px', borderBottomColor: lineColor}}
                    >
                        <Users onClick={this.showContacts} size={18} data-tip="View Contacts"/>
                        <img src="/static/svg/v2/contacts.svg" width='24px'/>
                        {dmCount > 0 &&
                        <DumbBadge count={dmCount} className='abs' style={{top: '0', right: '32%'}}/>
                        }


                    </div>
*/}

{/*
                    <div className='utProjects lpTab inline hoh pointer relative borderB' data-tip="View Boards"
                         onClick={this.showBoards}
                         style={{paddingTop: '10px', borderBottomColor: tab === LeftPaneTab.PROJECTS ? activeLineColor : lineColor}}
                    >
                        <img src='/static/svg/v2/briefcase.svg' width='24px'/>

                        {projectEventCount > 0 &&
                        <DumbBadge count={projectEventCount} className='abs' style={{top: '0', right: '1rem'}} />
                        }

                    </div>
*/}

{/*
                    <div className='utApprovals lpTab inline hoh pointer relative borderB' data-tip="View Approvals"
                         onClick={this.showApprovals}
                         style={{paddingTop: '10px', borderBottomColor: tab === LeftPaneTab.APPROVALS ? activeLineColor : lineColor}}
                    >
                        <img src='/static/svg/v2/checkCircleInv.svg' width='22px'/>
                        {apTotalCount > 0 &&
                        <DumbBadge count={apTotalCount} className='abs' style={{top: '1px', right: '32%'}} />
                        }

                    </div>
*/}

                    {/*
                    <div className='inline hoh pointer vaTop' >
                        <ChevronLeft size={18} className='jSec' onClick={this.minimizePane}
                                     data-tip='Minimize'/>
                    </div>
*/}

                </nav>

                {isBrowser () && uiStateStore.showInlineHints && uiStateStore.hintLibrary &&
                <InlineHint name='hintLibrary' cookie='hlib' title='Global Library'
                            body={MsgLibrary} place='side'
                />
                }

                <section className='lpSection' >
                    <ContactLibrary project={project}
                                    badgeTotal={dmCount}
                                    isMini={!isOpen}
                                    mobile={mobile}
                                    openMembers={uiStateStore.openMembers}
                                    refreshMembers={this.refreshMembers}
                                    filterByUser={this.filterByUser}/>
                </section>

                <section className='lpSection'>
                    <ProjectLibrary mobile={mobile}
                                    isMini={!isOpen}
                                    badgeTotal={projectEventCount}
                                    createProject={() => this.props.createProjects (1)}
                    />
                </section>


{/*

                    <section className='lpSection'>
                        <ApprovalLibrary mobile={mobile}
                                         isMini={!isOpen}
                                         badgeTotal={apTotalCount}
                        />
                    </section>
*/}
{/*

                <div style={{position: 'fixed', bottom: 0, left: '1rem'}}>
                    <span className='jBtn' onClick={this.switchThemes}>
                        theme
                    </span>
                </div>
*/}
            </div>
        )
    }
}