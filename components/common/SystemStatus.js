import {Component} from 'react'
import {XCircle} from 'react-feather'
import {observer} from 'mobx-react'

import UserSessionUtils from '/utils/UserSessionUtils'
import SystemStore, {ERROR, NORMAL, WARNING} from '/store/SystemStore'

@observer
export default class SystemStatus extends Component {
    constructor(props) {
        super(props)
        this.state = {showStatus: false}
    }

    closeStatus() {
        UserSessionUtils.setSessionPref('statusRead', SystemStore.currentStatus.message)
        this.setState({showStatus: false})
    }

    getStatusClass() {
        if (!SystemStore.currentStatus)
            return 'normal'

        switch (SystemStore.currentStatus.status) {
            case NORMAL: return 'normal'
            case WARNING: return 'warning'
            case ERROR: return 'error'
            default: return 'normal'
        }
    }

    render() {
        if (SystemStore.currentStatus && SystemStore.currentStatus.message === UserSessionUtils.getSessionPref('statusRead'))
            return null

        if (this.state.showStatus || !SystemStore.currentStatus || SystemStore.currentStatus.status === NORMAL)
            return null

        let status = SystemStore.currentStatus
        let statusClass = this.getStatusClass()

        if (status.message === '')
            return null

        return(

            <div className="systemStatus">

                <div className={statusClass}>
                    {/*<Feather.Info className="icon"/>*/}
                    <span>
                        {/*<Moment subtract={{ hours: 8 }} format="hh:mm Z">{new Date(status.time_modified)}</Moment>:*/}
                        <span >{status.message}</span>
                    </span>
                    <XCircle className="close" onClick={() => this.closeStatus()}/>
                </div>
            </div>
        )
    }
}