import React, {Component} from 'react'
import EmailService from "../../services/EmailService";
import RU from "../../utils/ResponsiveUtils";
import userStore from '../../store/userStore'
import projectStore from '../../store/projectStore'

export default class ErrorBoundary extends Component {

    state = {error: null, errorInfo: null}

    componentDidCatch(error, errorInfo) {
        this.setState({error: error, errorInfo: errorInfo})

        const project = projectStore.currentProject
        const projectId = project? project.id : 'none'
        const user = userStore.currentUser
        const userId = user? user.id : 'none'
        const device = RU.getDeviceType()
        const deviceProps = RU.getDeviceProps()
        const firstChild = this.props.children && this.props.children.length > 0? this.props.children[0] : null

        console.error('ErrorBoundary: ')
        if (error)
            console.error(error.toString())
        console.error(errorInfo.componentStack)
        console.error('firstChild: ' +firstChild)

        EmailService.sendError({
            thrower: 'Error Boundary',
            subject: 'Error Boundary at children: ' +this.props.children,
            msg: (error? error.toString():'no error obj') + '. Stack: ' +errorInfo.componentStack,
            error: {status: error.toString(), path: errorInfo.componentStack, project: projectId, device: device, userId: userId, deviceProps: deviceProps},
            project: projectId,
            user: userId
        })
    }

    render() {
        const {error, errorInfo} = this.state
        if (errorInfo) {
            return (
                <div>
                    <h4 className='jSec lighter'>Something went wrong.</h4>
                    <details className='jSec'>
                        {error && error.toString()}
                        <br />
                        Source: {errorInfo.componentStack}
                    </details>
                </div>
            )
        }

        return this.props.children
    }
}