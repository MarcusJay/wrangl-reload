import {Component} from 'react'
import {observer} from 'mobx-react'
import EmbedUtils from '/utils/EmbedUtils'
import CardSlideShowModal from "../modals/CardSlideShowModal";
import projectStore from '/store/projectStore'
import cardStore from '/store/cardStore'

@observer
export default class EmbedTrigger extends Component {

    constructor(props) {
        super(props)
    }

    componentWillMount() {
        if (this.props.card.source)
            this.iconSrc = EmbedUtils.getIconFromSource(this.props.card.source)
        else
            this.iconSrc = EmbedUtils.getIconFromUrl(this.props.card.url)

    }

    render() {
        const card = this.props.card

        return (
            <CardSlideShowModal cards={cardStore.currentCards} projectTitle={projectStore.currentProject.title}
                                cardId={card.id} triggerIconSrc={card.sourceIcon || this.iconSrc}
            />
        )
    }
}
