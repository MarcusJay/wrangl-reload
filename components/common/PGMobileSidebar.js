import React, {Component} from 'react'
import {observer} from 'mobx-react'
import uiStateStore from "../../store/uiStateStore";
import ErrorBoundary from "./ErrorBoundary";

@observer
export default class PGMobileSidebar extends Component {

    onClose = (ev) => {
        const pos = this.props.position || 'left'
        if (pos === 'left')
            uiStateStore.showMobSidebarL = false
        else if (pos === 'right')
            uiStateStore.showMobSidebarR = false
    }

    onOverlayClick = (ev) => {
        this.onClose(ev)
    }

    mute = (ev) => {
        ev.stopPropagation()
    }

    render() {
        const {component, position} = this.props
        const pos = position || 'left'
        const isOpen = pos === 'left'? uiStateStore.showMobSidebarL : uiStateStore.showMobSidebarR

        const x = isOpen? 0 : pos === 'left'? '-100%' : '200%'

        const style = {
            top: 0,
            bottom: 0,
            left: x,
            width: '100%',
            transition: 'left 0.2s ease-in-out',
            background: 'rgba(0,0,0,0)'
        }
        const innerWidth = pos === 'left'? '60%':'80%'
        let innerStyle = {
            top: 0,
            bottom: 0,
            width: innerWidth,
            left: pos === 'left' ? 0 : '20%',
            overflowY: 'auto'
        }

        return (
            <div className='fixed' style={style}
                 onClick={this.onOverlayClick}>

                <div className='abs jFG jSideBG borderAll modalShadow subtle-scroll-dark' style={innerStyle}
                     onClick={this.mute}
                >
                    <ErrorBoundary>
                        {component}
                    </ErrorBoundary>
                </div>

            </div>
        )
    }
}
