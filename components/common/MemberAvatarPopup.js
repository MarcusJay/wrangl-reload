import React, {Component} from 'react'
import {Divider, Menu, Popup} from 'semantic-ui-react'
import RMoment from 'react-moment'
import {Link, Router} from '~/routes'
import {deleteMember} from "../../models/ProjectModel";
import {firstWord} from "../../utils/solo/firstWord";
import uiStateStore from "../../store/uiStateStore";
import projectStore from '/store/projectStore'
import userStore from '/store/userStore'
import {UserRole} from "../../config/Constants";
import UserApiService from "../../services/UserApiService";
import {EventSource} from "../../config/EventConfig";
import ProjectApiService from "../../services/ProjectApiService";

export default class MemberAvatarPopup extends Component {

    state = {menu: false}

    open(ev) {
        this.setState({menu: true})
    }

    close(ev) {
        this.setState({menu: false })
    }

    gotoProfile = (ev) => {
        this.close (ev)
        const user = this.props.person || this.props.member
        Router.pushRoute ('profile', {id: user.id})
    }

    openDM = (ev) => {
        this.close (ev)
        const user = this.props.person || this.props.member
        if (!user)
            return

        const contact = userStore.getUserFromConnections(user.id)
        if (contact)
            uiStateStore.openDM (contact.id)
        else
            userStore.getUserById(user.id).then( user => {
                uiStateStore.openDM (null, user)
            })
    }

    attachUser = (ev) => {
        this.close (ev)
        if (this.props.attachUser)
            this.props.attachUser (this.props.member)
    }

    unattachUser = (ev) => {
        this.close (ev)
        if (this.props.unattachUser)
            this.props.unattachUser (this.props.inMsgId, this.props.member)
    }

    removeFromProject = (ev) => {
        const {member, activeUser, enableAdmin} = this.props
        const project = projectStore.currentProject

        if (!enableAdmin)
            return
        projectStore.removeUserFromProject (project.id, activeUser.id, member.id, true).then (response => {
            deleteMember (member.id, project)
            if (this.props.refresh)
                this.props.refresh ()
        })
    }

    addToContacts = (user) => {
        const {sessionId} = this.props
        if (!sessionId || !user) {
            console.error(`onselectuser: missing params: ${sessionId} or ${user}`)
            return
        }

        UserApiService.connectFoundUser(sessionId, user.item_index).then( response => {
            if (this.props.contactAdded)
                this.props.contactAdded()
        })

    }

    render() {
        const {trigger, attachUser, unattachUser, member, activeUser, open, inMsgId, enableAdmin} = this.props
        if (!member)
            return null

        const firstName = member.firstName || firstWord (member.displayName)
        const commonProjectsCount = null // TODO
        const isContact = !!userStore.getUserFromConnections(member.id, true)

        return (
            <Popup
                trigger={trigger}
                on='click'
                open={open}
                onOpen={(ev) => this.open (ev)}
                onClose={(ev) => this.close (ev)}
                position='bottom right'
                style={{padding: 0, minWidth: '17rem'}}
                hoverable
            >
                <Menu vertical className='hioh' style={{width: '100%'}}>

                    {/* Contacts */}
                    {!isContact &&
                    <Menu.Item name="contacts" onClick={this.addToContacts}>
                        <span className='small'>Add to contacts</span>
                    </Menu.Item>
                    }

                    {/* Visit */}
                    {isContact &&
                    <Menu.Item name="visit" onClick={this.gotoProfile}>
                        {/*<User size={20} className='secondary' style={{marginRight: '0.75rem'}}/>*/}
                        <span className='small'>Visit Profile</span>
                    </Menu.Item>
                    }

                    {/* DM */}
                    {!activeUser || activeUser.id !== member.id &&
                    <Menu.Item name="message" onClick={this.openDM}>
                        {/*<MessageCircle size={20} className='secondary flipHoriz' style={{marginRight: '0.75rem'}}/>*/}
                        <span className='small'>Direct Message</span>
                    </Menu.Item>
                    }

                    {/* Remove from Project */}
                    {enableAdmin && isContact &&
                    <Menu.Item name="remove" onClick={this.removeFromProject} disabled={!enableAdmin}
                               className={!enableAdmin ? 'disabled' : ''}
                               data-tip={!enableAdmin ? 'You must be an Admin to remove members' : ''}>
                        {/*<LogOut size={20} className='secondary' style={{marginRight: '0.75rem'}}/>*/}
                        <span className='small'>Remove from board</span>
                    </Menu.Item>
                    }

{/*
                    <Menu.Item name="tag">
                        <span className='small'>Drag to tag </span>
                    </Menu.Item>
*/}

                </Menu>
            </Popup>
        )
    }
}

