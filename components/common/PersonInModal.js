import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {DragSource} from 'react-dnd'
import {Loader, Menu} from 'semantic-ui-react'
import {Link, Router} from '~/routes'
import eventStore from '/store/eventStore'
import uiStateStore from '/store/uiStateStore'
import projectStore from '/store/projectStore'

import {DndItemTypes} from '~/config/Constants'
import {truncate} from "../../utils/solo/truncate";
import {AlertOctagon, Check, LogIn, LogOut, MessageCircle, Star, User, X} from "react-feather";
import DumbBadge from "./DumbBadge";
import {observer} from 'mobx-react'
import {firstWord} from "../../utils/solo/firstWord";
import {UserRole} from "../../config/Constants";

const actionIconSize = 36

const personSource = {
    beginDrag(props) {
        uiStateStore.enableFileDrops = false
        return {
            id: props.id,
            name: props.name,
            type: DndItemTypes.PERSON
        }
    },
    endDrag(props, monitor) {
        uiStateStore.enableFileDrops = true
        const item = monitor.getItem()
        return {
            id: props.id,
            name: props.name,
            type: DndItemTypes.PERSON
        }
    }
}

@DragSource(DndItemTypes.PERSON, personSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
}))
@observer
export default class PersonInModal extends Component {
    static propTypes = {
        connectDragSource: PropTypes.func,
        isDragging: PropTypes.bool,
        id: PropTypes.any,
        name: PropTypes.string
    }

    state = {menu: false, loading: false, commonProjectsCount: 0}

    openMenu = (ev) => {

        const {person, activeUser} = this.props
        projectStore.countCommonProjects([activeUser.id, person.id]).then( count => {
            this.props.setOpenPersonId(person.id)
            this.setState({menu: true, commonProjectsCount: count})
        })

        // }, 300)
    }

    closeMenu = (ev) => {
        this.setState({menu: false})
    }

    toggleMenu = (ev) => {
        const isMenuOpen = this.state.menu
        if (isMenuOpen)
            this.closeMenu(ev)
        else
            this.openMenu(ev)
    }

    handleKeyPress = (ev) => {
        if (ev.key === 'Escape')
            this.closeMenu (ev)
    }


    addMember = (ev) => {
        this.props.setMembership(UserRole.INVITED)
    }

    removeMember = (ev) => {
        this.props.setMembership(UserRole.DELETED)
    }

    pinUser = (ev) => {
        // ev.preventDefault()
        // ev.stopPropagation()
        this.props.pinUser(this.props.person.id)
    }

    unpinUser = (ev) => {
        // ev.preventDefault()
        // ev.stopPropagation()
        this.props.unpinUser(this.props.person.id)
    }

    unfriend = (ev) => {
        // ev.preventDefault()
        // ev.stopPropagation()
        if (!this.props.isMember)
            this.props.disconnectUser(this.props.person.id)
    }

    gotoProfile = (ev) => {
        // ev.preventDefault()
        // ev.stopPropagation()
        Router.pushRoute('profile', {id: this.props.person.id})
    }

    dm = (ev) =>{
        // ev.preventDefault()
        // ev.stopPropagation()
        // don't talk to yourself
        if (this.props.activeUser.id === this.props.person.id)
            return

        // ZERO OUT unreadMsgCounts
        eventStore.getInstance().unreadMsgCounts[this.props.person.id] = 0
        this.props.directMessage(this.props.person.id)
    }

    componentDidUpdate() {
        if (this.state.menu === true && this.props.openPersonId !== this.props.person.id)
            this.closeMenu()
    }

    componentWillReact() {
        console.log('%c DMs. Person will react: ' +this.props.person.id, 'color: lightblue')
    }

    render() {
        const { person, /*aMenuIsActive,*/ isMember, activeUser, enableAdmin, project, showBadge, isDragging, connectDragSource, mobile, pinned } = this.props
        const {commonProjectsCount, loading, menu} = this.state
        const isMe = activeUser && activeUser.id === person.id
        const opacity = isDragging ? 0.5 : 1
        const personClass = "person inline relative" + (this.props.isMember? ' memberContact':'')
        //const isCreator = project && project.creatorId === person.id
        const maxChars = mobile? 18 : 26
        const memberName = truncate( person.displayName || person.email, maxChars)
        const firstName = person.firstName || firstWord(memberName)

        const removeContactTip = isMember? "Fellow members of a board can't be unfriended (yet)" : "Remove " +memberName+ " from your contacts"
        const profileTip = !isMe? 'Visit ' +firstName : 'Hi, you'
        const memberTip = project? (firstName + ' is ' +(!isMember? 'not ':'')+ ' a member of this board'):''

        const evs = eventStore.getInstance()
        const badgeCount = evs.unreadMsgCounts[person.id] || 0
        // const rtBadgeCount = eventStore.getInstance().countNewMsgs(user.id)
        const mobx = evs.unreadMsgCounts
        const rand = evs.rand

        const personStyle = {overflow: 'visible', width: '12rem', marginRight: '1rem'}
        const memberStyle = {fontWeight: badgeCount > 0? '700':'300'}
        const msgIconStyle={float: 'right', marginRight: '0.5rem', marginTop: '0'}

        return connectDragSource(
            <div name={person.domId} className={personClass} style={personStyle}
                 onClick={this.toggleMenu}
            >
                <div className='left relative'>
                    <div style={{backgroundImage: 'url('+person.image+')'}}
                         className='avatar inline '
                         // onMouseEnter={this.openMenu}
                         // onMouseLeave={this.closeMenu}
                    />

                    {isMember &&
                        <span className='abs' style={{top: '0', left: '3.2rem'}} data-tip={memberTip}>
                            <Check size={14} stroke-width='5' className='secondary' />
                        </span>
                    }

                    {showBadge && badgeCount > 0 && !isMe &&
                    <DumbBadge count={badgeCount} style={{position: 'absolute', top: '0', left: '37px'}} />
                    }

                    <span className="uName inline dontWrap" style={memberStyle}
                          // onMouseEnter={this.openMenu}
                          // onMouseLeave={this.closeMenu}
                    >
                        {truncate(memberName, 25)}
                        {/*<MoreHoriz size={18} onClick={this.toggleMenu} className='showonh secondary' style={{marginLeft: '0.5rem', verticalAlign: 'bottom'}}/>*/}

                    </span>
                </div>


                {menu &&
                <div className='abs wModal'
                     style={{left: '0', top: '3rem', padding: '1rem', minWidth: '20rem'}}
                     onKeyPress={this.handleKeyPress}
                >
                    <Loader active={loading}></Loader>
                    <X size={24} onClick={this.closeMenu} className='abs' style={{top: '1rem', right: '1rem'}}/>
                    <div style={{backgroundImage: 'url(' + person.image + ')'}}
                         className='avatar inline '
                    />
                    <h3 className='inline vaTop'
                        style={{marginLeft: '1rem', marginTop: '0.75rem'}}
                    >{person.displayName}</h3>

                    {person.dateCreated &&
                    <div className='small lighter'>Joined: {person.dateCreated}</div>
                    }
                    {person.dateModified &&
                    <div className='small lighter'>Last Active: {person.dateModified}</div>
                    }
                    <div className='small lighter'>You
                        have {commonProjectsCount} board{commonProjectsCount === 1 ? '' : 's'} in common.
                    </div>


                    <Menu vertical className='hioh' style={{width: '100%', zIndex: '1003 !important'}}>
                        <Menu.Item name="visit" onClick={this.gotoProfile}>
                            <User size={20} className='secondary' style={{marginRight: '0.75rem'}}/>
                            <span>Visit {firstName}</span>
                        </Menu.Item>
                        <Menu.Item name="message" onClick={this.dm}>
                            <MessageCircle size={20} className='secondary flipHoriz' style={{marginRight: '0.75rem'}}/>
                            <span>Message {firstName}</span>
                        </Menu.Item>

                        {pinned &&
                        <Menu.Item name="visit" onClick={this.unpinUser}>
                            <Star size={20} className='secondary' style={{marginRight: '0.75rem'}}/>
                            Remove from favorites
                        </Menu.Item>
                        }

                        {!pinned &&
                        <Menu.Item name="visit" onClick={this.pinUser}>
                            <Star size={20} className='secondary' style={{marginRight: '0.75rem'}}/>
                            Add to favorites
                        </Menu.Item>
                        }

                        {project && isMember && enableAdmin &&
                        <Menu.Item name="remove" onClick={this.removeMember}>
                            <LogOut size={20} className='secondary' style={{marginRight: '0.75rem'}}/>
                            <span>Remove {firstName} from this board</span>
                        </Menu.Item>
                        }

                        {project && !isMember &&
                        <Menu.Item name="remove" onClick={this.addMember}>
                            <LogIn size={20} className='secondary' style={{marginRight: '0.75rem'}}/>
                            <span>Add {firstName} to this board</span>
                        </Menu.Item>
                        }

                        <Menu.Item name="remove" onClick={this.unfriend}>
                            <AlertOctagon size={20} className='secondary' style={{marginRight: '0.75rem'}}/>
                            <span>Unfriend {firstName}</span>
                        </Menu.Item>
                    </Menu>
                </div>
                }



                {/*
                <div className='uActions showonh abs' style={{left: '4rem'}}> showonh
                    {!isMe &&
                    <MessageCircle size={actionIconSize} className='userAction hoh'
                                   data-tip={'Message ' + memberName} data-type="light" data-place="right"/>
                    }

                    Contacts actions
                    {pinned && !isMe &&
                    <ArrowDown className='third hoh' size={actionIconSize} onClick={(ev) => this.unpinUser(ev)}
                          data-tip={'Unfavorite ' +memberName}    data-type="light" data-place="right"/>
                    }
                    {!pinned && !isMe &&
                    <Star className='hoh' size={actionIconSize} onClick={(ev) => this.pinUser(ev)}
                          data-tip={'Favorite ' +memberName}    data-type="light" data-place="right"/>
                    }
                    {false && !isMe &&
                    <X className={(this.props.isMember? 'fourth':'hoh')} size={actionIconSize} onClick={(ev) => this.disconnectUser(ev)}
                       disabled={this.props.isMember}
                       data-tip={removeContactTip}    data-type="light" data-place="right"/>
                    }

                    Membership actions
                    {false && this.props.isMember && this.props.project && !isCreator &&
                    <LogIn className='hoh' size={actionIconSize} style={{transform: 'rotate(180deg)'}} onClick={(ev) => this.toggleMembership(ev)}
                           data-tip={memberTip}    data-type="light" data-place="right"/>
                    }
                    {false && !this.props.isMember && !this.props.isInvited && this.props.project &&
                    <LogIn className='hoh' size={actionIconSize} onClick={(ev) => this.toggleMembership(ev)}
                           data-tip={memberTip}    data-type="light" data-place="right"/>
                    }
                    {false && this.props.isInvited &&
                    <span className="tiny third right hoh" onClick={(ev) => this.toggleMembership(ev)}>
                        invited
                    </span>
                    }
                    {false && this.props.isMember && this.props.project && isCreator &&
                    <Lock data-tip={memberName + ' is the owner.'} data-type="light" data-place="right"/>
                    }


                </div>
*/}

            </div>
        )
    }
}
