import {Component} from 'react'
import {Router} from '~/routes'
import {observer} from 'mobx-react'
import PropTypes from 'prop-types'
import ReactTooltip from 'react-tooltip'
import {Icon, Menu, Popup} from 'semantic-ui-react'
import {LogOut, Settings, XCircle} from 'react-feather'
import UserSessionUtils from '/utils/UserSessionUtils'
import DropboxService from "/integrations/DropboxService";
import integrationStore from '/store/IntegrationStore'
import uiStateStore from "/store/uiStateStore";
import userStore from "/store/userStore";
import StateChangeRequestor, {REQUEST_USER_UPDATE} from "/services/StateChangeRequestor"
import RU from "../../utils/ResponsiveUtils";
import {firstWord} from "../../utils/solo/firstWord";
import PusherService from "../../services/PusherService";


@observer
export default class ProfileMenu extends Component {

    prevName = null
    state = {showPopup: false, dummy: false}

    open(ev) {
        this.setState({showPopup: true})
        ReactTooltip.rebuild()
    }

    close(ev) {
        this.setState({showPopup: false})
        ReactTooltip.rebuild()
    }

    openAuthInNewTab() {
        if (integrationStore.isDropboxAuthorized())
            return
        const dropboxAuthUrl = DropboxService.getAuthorizationUrl()
        const authWindow = window.open(dropboxAuthUrl, '_blank', 'top=200,left=200,height=500,width=600,menubar=no,location=no,resizable=no,scrollbars=no,status=no')
    }

    // MOVE TO EDITPROFILE. Image may have been uploaded or specified by url. Either way, we have its new hosted url.
    handleImageChange = (image) => {
        this.props.user.image = image
        debugger
        if (image)
            StateChangeRequestor.awaitStateChange(REQUEST_USER_UPDATE,
                {user: {user_id: this.props.user.id, image: image}}).then( updatedUser => {
                    debugger
                this.setState(this.state) // force update
            })
    }

    editProfile() {
        // Router.pushRoute('/login', {id: this.props.project.id})
        this.close()
        uiStateStore.unModal = 'editProfile'
    }

    disconnectDropbox() {
        integrationStore.unauthorizeDropbox(this.props.user.id)
    }

    switchTheme = (ev) => {
        const old = uiStateStore.theme
        uiStateStore.theme = old === 'dark'? 'light':'dark'
    }

    logout = () => {
        UserSessionUtils.endUserSession()
        PusherService.unfollowUser()
        window.location.href = '/login'
    }

    componentDidMount() {
        const me = this.props.user || userStore.currentUser
        if (me)
            this.prevName = me.displayName || me.firstName || me.email
    }

    componentDidUpdate() {
        console.warn('Profile Menu did update')
        const me = this.props.user || userStore.currentUser
/*
        if (me && this.prevName && this.prevName !== me.displayName && this.prevName !== me.firstName && this.prevName !== me.email)
            this.setState({dummy: true})
*/
    }

    componentWillReact() {
        console.log('Profile menu will react')
    }

    render() {
        const dropboxAuthUrl = DropboxService.getAuthorizationUrl()
        const isDropboxConnected = integrationStore.isDropboxAuthorized()
        const dropboxClass = isDropboxConnected? 'highlight' : 'third'
        const dropboxTip = isDropboxConnected? 'You are linked to Dropbox. Click to disconnected or change accounts' :
                                               'Link your Dropbox account.'
        const {mobile, user} = this.props
        const me = user || userStore.currentUser // can't rely on mobx to update us, so look to props

        const forMobxOnly = uiStateStore.rand + uiStateStore.counter + userStore.randomId
        const dummy = this.state.dummy

        return(
            <div className='inline vaTop' style={{marginTop: '2px'}}>

                <Popup
                    trigger={
                        <span className="utProfile inline pointer vaTop" data-tip="Open Profile Menu">
                            <img src={me.image} className='member inline round' style={{width: "1.6rem", height: "1.6rem"}}/>
                            <span className='vaTop'>{!mobile && firstWord(me.displayName)}</span>
                            <Icon name="dropdown" size="large" className='dropdownArrow'/>
                        </span>
                    }
                    on='click'
                    open={this.state.showPopup}
                    onOpen={(ev) => this.open(ev)}
                    onClose={(ev) => this.close(ev)}
                    position='top right'
                    style={{padding: 0}}
                >
                    <Menu vertical className='profileMenu'>
                        <Menu.Item name="user" className='user' onClick={() => this.editProfile()}>
                            <div className='userImage round' style={{backgroundImage: 'url(' +me.image+ ')'}}></div>
                            <span className='name'>{this.props.user.displayName}</span>
                        </Menu.Item>
{/*
                        <Menu.Item name="user" className='user' onClick={() => this.editProfile()}>
                            <button onClick={this.switchTheme}>Switch Theme</button>
                        </Menu.Item>
*/}
                        <Menu.Item name="editProfile" onClick={() => this.editProfile()}>
                            <Settings size={20} color={'#777'} />
                            <span className="hoh" >Edit Profile</span>
                        </Menu.Item>

{/*
                        <Menu.Item data-tip={dropboxTip} className='integrations'>
                            <div className="hoh" >
                                <Icon name="dropbox" size='big' className={dropboxClass} onClick={() => this.openAuthInNewTab()}/>
                                {isDropboxConnected &&
                                    <div className='label'>
                                        <span className="tiny highlight">Connected</span>
                                        <XCircle className='disconnect' size={18} style={{marginLeft: '0.5rem', marginBottom: '-0.3rem'}}
                                                 onClick={() => this.disconnectDropbox()}/>
                                    </div>
                                }
                                {!isDropboxConnected &&
                                <div className='label'>
                                    <span className="tiny hoh" onClick={() => this.openAuthInNewTab()}>Connect</span>
                                </div>
                                }
                                <Icon name="check" size="large" className='highlight' style={{marginTop: '-2rem', marginLeft: '-0.5rem'}}/>
                            </div>
                        </Menu.Item>
*/}
                        <Menu.Item name="logout" onClick={this.logout}>
                            <LogOut size={20} color={'#777'} />
                            <span className='hoh' >Logout</span>
                        </Menu.Item>
                    </Menu>
                </Popup>
                <div>
                </div>
            </div>
        )
    }
}