import {Component} from 'react'
import {Popup} from 'semantic-ui-react'
import Moment from 'react-moment'
import {UserRole} from "/config/Constants";

export default class MemberPopup extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        const { image, name, role, projectDate, wrangleDate, action } = this.props

        return(
            <Popup inverted position='left top' trigger={
                <img src={image} className='statUser' />
            }>

                <h3><img src={image} />{name}</h3>
                <span className='small secondary'>
                    Status: {[UserRole.ACTIVE, UserRole.ADMIN].indexOf(role) !== -1? 'Active' : role === UserRole.INVITED? 'Invited' : 'Inactive'}
                </span>
                {role === UserRole.ADMIN &&
                <span className='bold orange right'>Admin</span>
                }

                {projectDate &&
                <div className='small secondary'>{action}&nbsp;
                    <Moment format='MMMM D, YYYY'>{projectDate}</Moment>
                </div>
                }

                {wrangleDate &&
                <div className='small secondary'>
                    Member since <Moment format='MMMM D, YYYY'>{wrangleDate}</Moment>
                </div>
                }

            </Popup>
        )
    }
}