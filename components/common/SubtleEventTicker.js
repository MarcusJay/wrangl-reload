import {Component} from 'react'
import {observer} from 'mobx-react'

import eventStore from '/store/eventStore'
import projectStore from '/store/projectStore'
import {isBrowser} from "../../utils/solo/isBrowser";


@observer
export default class SubtleEventTicker extends Component {

    constructor(props) {
        super (props)
        // this.state = {queue: [], currentEvent: null}
        this.queue = []
        this.tickerJob = null
    }

    startTicker() {
        if (this.tickerJob !== null)
            clearInterval(this.tickerJob)

        let event
        this.tickerJob = setInterval( () => {
            event = this.deQueueNextEvent()
            this.currentEvent = event
            console.log('Tick. event = ' +(event? event.id : 'none'))
            this.forceUpdate()
        }, 1000)
    }

    queueEvent(event) {
        this.queue.push(event)
    }

    deQueueNextEvent() {
        if (this.queue.length === 0)
            return null

        return this.queue.splice(0, 1)[0]
    }

    componentWillReact() {
        console.log('Event ticker will react')


    }

    componentDidMount() {
        if (isBrowser())
            this.startTicker()
    }

    render() {
        if (!projectStore.currentProject)
            return null

        /**
         * Observe projectEventCounts to trigger mobx Reaction only.
         * projectEvents itself will not trigger reaction. Tried several structures and options.
         */
        const newEventCount = eventStore.getInstance().oldRtEventCounts[projectStore.currentProject.id]

        const rtEvents = eventStore.getInstance().rtEvents[projectStore.currentProject.id]
        if (!rtEvents || rtEvents.length === 0)
            return 'waiting...'

        rtEvents.forEach( event => {
            this.queueEvent( event )
        })

        return(
            <div className="eventTicker">
                event: {this.currentEvent? this.currentEvent.id : 'none'}
            </div>
        )
    }
}