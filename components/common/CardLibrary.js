import {Component} from 'react'
import {observer} from 'mobx-react'
import {SearchConfig} from '/config/Constants'

import {Form, Icon} from 'semantic-ui-react'

import CardApiService from '/services/CardApiService'
import CardModel from '/models/CardModel'
import CardLibraryThumb from "./CardLibraryThumb";

import cardStore from '/store/cardStore'
import uiStateStore from '/store/uiStateStore'
import userStore from "../../store/userStore";
import {findCards} from "../../services/CardApiService";

@observer
export default class CardLibrary extends Component {
    constructor(props) {
        super(props)
        this.toggleSelectCard = this.toggleSelectCard.bind(this)
        this.importSelectedCards = this.importSelectedCards.bind(this)
        this.state = {selectedCardIds: {}, searching: false, importing: false, query: '', searchResults: cardStore.userCards || [], lastKeyPress: 0, isVirgin: true, waiting: false, elem: null}
    }

    onSearchChange(ev) {
        let query = ev.target.value
        console.log('change: query = ' +query)
        let now = new Date()
        let elapsed = this.state.lastKeyPress === 0? 0 : now - this.state.lastKeyPress
        this.setState({query: ev.target.value, lastKeyPress: now})

        setTimeout( () => {
            if (query.length >= SearchConfig.MIN_SEARCH_CHARS) {
                this.submitSearch (query)
            } else if (!this.state.isVirgin) {
                console.log ('Clearing search')
                this.setState ({searchResults: cardStore.userCards})
            } else {
                console.log('Skipping search ' + query + ' at ' +elapsed+ 'ms')
            }
            this.setState ({waiting: false})

        }, SearchConfig.QUERY_DELAY_MS )

    }

    submitSearch(query) {
        console.log("Searching for " +query+ "...")

        this.setState ({searching: true, isVirgin: false})

        findCards (query.toLowerCase()).then (response => {
            if (!response.data || response.data.length === 0)
                throw new Error ('Card search failed')

            this.setState ({
                searching: false,
                searchResults: response.data.cards.map (apiCard => {
                    return new CardModel (apiCard)
                }) || []
            })
        }).catch (error => {
            console.error ("Error searching for " + query, error)
            this.setState ({searching: false})
            throw new Error (error)
        })
    }

    toggleSelectCard(card) {
        // TODO Ignore selections in PL until there's a UI for selecting destination project
        if (uiStateStore.currentPage !== uiStateStore.PD)
            return

        console.log("selectCard: " +card.card_title);
        if (!this.state.selectedCardIds[ card.id ])
            this.state.selectedCardIds[ card.id ] = card
        else
            delete this.state.selectedCardIds[ card.id ]
        this.setState(this.state)

    }

    importSelectedCards() {
        this.setState({importing: true})
        let cardArray = Object.keys(this.state.selectedCardIds)
        if (cardArray.length === 0) {
            console.error("importSelectedCards: 0 cards selected.")
            return
        }

        this.props.importCards(cardArray)
        this.resetSelectedCards()

    }

    resetSelectedCards() {
        this.setState({selectedCardIds: {}, importing: false, searchResults: cardStore.userCards})
    }

    componentWillReceiveProps(nextProps) {
        console.log('Card Library will receive props.')
        // const project = nextProps.project

        // If user cards have changed, update them
        if (cardStore.userCards && this.state.searchResults && this.state.searchResults.length !== cardStore.userCards.length)
            this.setState({searchResults: cardStore.userCards})
    }

    componentDidMount() {
        const userId = userStore.currentUser.id
        cardStore.getUserCards(userId, true, true) // accept cached cards, use cardmodel

    }

/*
    not triggering. not surprised. use props above.
    componentWillReact() {
        console.log('Card Library will react.')
        debugger
    }
*/

    render() {
        // if (!this.props.userCards)
        //     return null
        // const projectId = this.props.projectId

        // const cardsInProject = this.props.userCards.filter( c => c.projectId === this.props.project.id)
        // const otherCards = this.props.userCards.filter( c => c.projectId !== this.props.project.id)
        let selectedCardCount = Object.keys(this.state.selectedCardIds).length
        const paneClass = this.props.mobile? 'm-side-pane':'side-pane'

        return(
            <div className={paneClass}>
                <Form inverted style={{textAlign: 'center'}} className="thumbSearch">
                    <Form.Input icon={<Icon name="search" />}
                                name='Search'
                                className="sidePaneInput"
                                onChange={(ev) => {this.onSearchChange(ev)}}
                                value={this.state.query}
                                id="cardSearch"
                                // style={{color: '#777'}}
                                placeholder='Search...'
                    />
                    {(Object.keys (this.state.selectedCardIds).length > 0 && uiStateStore.currentPage === uiStateStore.PD) &&
                    <div className='sidePaneTextButton' style={{margin: '-0.7rem 0 -0.7rem 0'}} >
                        <span onClick={() => this.importSelectedCards()}>
                            Add {selectedCardCount > 1 ? selectedCardCount + ' Cards' : selectedCardCount === 1 ? selectedCardCount + ' Card' : 'Cards'}?
                        </span>
                        <Icon name="cancel" style={{marginLeft: '0.5rem'}} onClick={() => this.resetSelectedCards()}
                              data-tip={"Cancel" }


                        />
                        <Icon name="check" onClick={() => this.importSelectedCards()}
                              data-tip={"Yes, add files" }


                        />
                        {/*<Feather.ArrowRight />*/}
                    </div>
                    }
                </Form>
                <div className="thumbLibrary">
                {this.state.searchResults.map( (card,i) => {
                    return (
                        <CardLibraryThumb
                            card={card}
                            id={card.id}
                            key={'clt'+i}
                            selectCard={this.toggleSelectCard}
                            // selected={this.state[ cardModel.domId ] === true}
                            selected={this.state.selectedCardIds[ card.id ] !== undefined}
                        />
                    )
                })}
                </div>
            </div>
        )
    }
}