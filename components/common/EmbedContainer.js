import {Component} from 'react'
import EmbedUtils from '/utils/EmbedUtils'
import {isEmbeddable} from "/config/FileConfig";

export default class EmbedContainer extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        // TODO phase out EmbedUtils, use FileCOnfig
        debugger
        const embedClass = this.props.className? (this.props.className+ ' embedPane') : 'embedPane'
        if (!EmbedUtils.isEmbeddable(this.props.url) && !isEmbeddable(this.props.ext))
            return (
                <div>This url is not an embeddable resource.</div>
            )

        return (
            <div className={embedClass}>
                <iframe src={this.props.url}>
                </iframe>
            </div>
        )
    }
}
