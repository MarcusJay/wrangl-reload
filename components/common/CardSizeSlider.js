import {Component} from 'react'
import PropTypes from 'prop-types'
import uiStateStore from '/store/uiStateStore'
import {ZoomIn, ZoomOut} from "react-feather";
import RU from '/utils/ResponsiveUtils'

// let MIN_CARD_INFO_SIZE = 100
const STEP_SIZE = 25
const MIN_SIZE = 50
const MAX_SIZE = 500
const TILE_STEP_SIZE = 3
const TILE_MIN_SIZE = 5
const TILE_MAX_SIZE = 30

/**
 * Reusable slider for adjusting card display size
 * PLEASE specify the following attributes:
 *
 * cardContainer        - id or class selector for the elem containing the cards to be resized.
 * label                - optional
 *
 */
// TODO make this an observer and eliminate local state
export default class CardSizeSlider extends Component {
    static propTypes = {
        // sliderElemId: PropTypes.string,
        // sliderValueId: PropTypes.string,
        cardContainer: PropTypes.string
    }

    constructor(props) {
        super(props)
        // let sliderCount = uiStateStore.addSlider()
        this.state = {
            sliderId: 'ss'+this.props.cardContainer,
            // sizeValue: UserSessionUtils.getSessionPref('cardSize'+this.props.cardContainer) || uiStateStore.defaultCardSize,
            sizeValue: uiStateStore.getCardSize(this.props.cardContainer) || uiStateStore.calcCardSize(this.props.container) || uiStateStore.defaultCardSize,
            valueId: 'sv'+this.props.cardContainer
        }
    }

    changeHandler(ev) {
        let newSize = ev.currentTarget.valueAsNumber
        this.setState({sizeValue: newSize})
        uiStateStore.setCardSize( newSize, this.props.cardContainer )
    }

    zoomIn = (ev) => {
        const newSize = Math.min(this.state.sizeValue + STEP_SIZE, MAX_SIZE)
        this.setState({sizeValue: newSize})
        uiStateStore.setCardSize( newSize, this.props.cardContainer )
    }

    zoomOut = (ev) => {
        const newSize = Math.max(this.state.sizeValue - STEP_SIZE, MIN_SIZE)
        this.setState({sizeValue: newSize})
        uiStateStore.setCardSize( newSize, this.props.cardContainer )
    }

    componentDidMount() {
        const device = RU.getDeviceType()
        if (device !== 'computer' && !this.state.sizeValue)
            this.setState({sizeValue: uiStateStore.calcCardSize(this.props.container)})
    }

    render() {
        const device = RU.getDeviceType()
        const disabled = this.props.disabled
        const cl = disabled? 'disabled readOnly':''

        return(
            <div className={cl}
                 style={{padding: '.25rem', opacity: disabled? 0.3:1}}
                 data-tip="Adjust item size"
            >
{/*
                <span className='tiny' style={{marginRight: '0.5rem', marginTop: '-0.3rem', verticalAlign: 'middle'}}>
                    {this.state.sizeValue / 2}%
                </span>
*/}

                {device === 'computer' &&
                <span style={{marginTop: '0.2rem', verticalAlign: 'middle', paddingLeft: '8px', paddingRight: '8px'}}>
                    <input id={this.state.sliderId} value={this.state.sizeValue} type="range"
                           min={MIN_SIZE} max={MAX_SIZE} step={STEP_SIZE} style={{width: '100px', minWidth: 'unset'}}
                           onChange={(ev) => this.changeHandler(ev)}
                           disabled={disabled}
                    />
                    <span style={{display: 'none'}} id={this.state.valueId}>200</span>
                </span>
                }

                {device !== 'computer' &&
                <span style={{marginTop: '-0.3rem', verticalAlign: 'middle'}}>
                    <ZoomOut size={29} className='secondary' onClick={this.zoomOut} style={{marginRight: '0.25rem'}}
                             disabled={disabled}/>
                    <ZoomIn size={28} className='secondary' onClick={this.zoomIn} style={{marginRight: '0.5rem'}}
                            disabled={disabled}/>
                </span>
                }

            </div>
        )
    }
}