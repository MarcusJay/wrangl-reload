import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {DragSource} from 'react-dnd'
import {Menu, Popup} from 'semantic-ui-react'
import {Link, Router} from '~/routes'
import eventStore from '/store/eventStore'
import uiStateStore from '/store/uiStateStore'
import userStore from '/store/userStore'

import {DndItemTypes} from '~/config/Constants'
import {truncate} from "../../utils/solo/truncate";
import {Check, Star} from "react-feather";
import DumbBadge from "./DumbBadge";
import {observer} from 'mobx-react'
import {firstWord} from "../../utils/solo/firstWord";
import {UserRole} from "../../config/Constants";
import {ManageContacts} from "../../store/uiStateStore";
import projectStore from "../../store/projectStore";
import UserApiService from "../../services/UserApiService";

const actionIconSize = 36

const personSource = {
    beginDrag(props) {
        uiStateStore.enableFileDrops = false
        return {
            id: props.person.id,
            name: props.name,
            type: DndItemTypes.PERSON
        }
    },
    endDrag(props, monitor) {
        uiStateStore.enableFileDrops = true
        const item = monitor.getItem()
        return {
            id: props.person.id,
            name: props.name,
            type: DndItemTypes.PERSON
        }
    }
}

@DragSource(DndItemTypes.PERSON, personSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
}))
@observer
export default class Person extends Component {
    static propTypes = {
        connectDragSource: PropTypes.func,
        isDragging: PropTypes.bool,
        id: PropTypes.any,
        name: PropTypes.string
    }

    state = {menu: false}

    checkPerson = (ev) => {
        const {person, isMember, isConnected} = this.props
        if (!isConnected) {
            // alert('User is not in your contacts. ')
        }

    }

    openMenu = (ev) => {

        const {person, activeUser} = this.props
        // projectStore.countCommonProjects([activeUser.id, person.id]).then( count => {
            this.props.setOpenPersonId(person.id)
            this.setState({menu: true}) //, commonProjectsCount: count})
        // })

        // }, 300)
    }

    closeMenu = (ev) => {
        this.setState({menu: false})
    }

    toggleMenu = (ev) => {
        const isMenuOpen = this.state.menu
        if (isMenuOpen)
            this.closeMenu(ev)
        else
            this.openMenu(ev)
    }

    addMember = (ev) => {
        this.props.setMembership(UserRole.INVITED)
    }

    removeMember = (ev) => {
        this.props.setMembership(UserRole.DELETED)
    }

    pinUser = (ev) => {
        // ev.preventDefault()
        // ev.stopPropagation()
        this.props.pinUser(this.props.person.id)
        this.closeMenu(ev)
    }

    unpinUser = (ev) => {
        // ev.preventDefault()
        // ev.stopPropagation()
        this.props.unpinUser(this.props.person.id)
        this.closeMenu(ev)
    }

    addToContacts = (ev) => {
        const {sessionId, person} = this.props
        if (!sessionId || !person) {
            console.error(`addToContacts: missing params: ${sessionId} or ${person}`)
            return
        }

        UserApiService.connectFoundUser(sessionId, person.item_index).then( response => {
            if (this.props.contactAdded)
                this.props.contactAdded()
        })
        this.closeMenu(ev)
    }

    unfriend = (ev) => {
        // ev.preventDefault()
        // ev.stopPropagation()
        if (!this.props.isMember)
            this.props.disconnectUser(this.props.person.id)
        this.closeMenu(ev)
    }

    gotoProfile = (ev) => {
        // ev.preventDefault()
        // ev.stopPropagation()
        Router.pushRoute('profile', {id: this.props.person.id})
        this.closeMenu(ev)
    }

    dm = (ev) =>{
        // ev.preventDefault()
        // ev.stopPropagation()
        // don't talk to yourself
        if (this.props.activeUser.id === this.props.person.id)
            return

        // ZERO OUT unreadMsgCounts
        eventStore.getInstance().unreadMsgCounts[this.props.person.id] = 0
        this.props.directMessage(this.props.person.id)
        this.closeMenu(ev)
    }

    render() {
        const { person, isMember, isConnected, sessionId, activeUser, enableAdmin, project, badgeCount, showBadge, isDragging, connectDragSource, mobile, pinned } = this.props
        const {commonProjectsCount, loading, menu} = this.state

        const isMe = activeUser && activeUser.id === person.id
        const personHasId = !!person.id
        const isCurrentFriend = person
            && userStore.currentFriend
            && person.id === userStore.currentFriend.id
            && uiStateStore.currentPage === uiStateStore.PF
        // const opacity = isDragging ? 0.5 : 1
        const imInBigPane = uiStateStore.maximizeLeftCol && uiStateStore.leftUnModal === ManageContacts

        const personClass = "person relative" + (isCurrentFriend? ' active':'') + (imInBigPane? ' inline':'')
        const personStyle = imInBigPane? {width: '12rem', marginRight: '1rem'} : {}

        const maxChars = mobile? 18 : 26
        const memberName = truncate( person.displayName || person.display_name, maxChars)
        const firstName = person.firstName || firstWord(memberName)

        const removeContactTip = isMember? "Fellow members of a board can't be unfriended (yet)" : "Remove " +memberName+ " from your contacts"
        const profileTip = !isMe? 'Visit ' +firstName : 'Hi, you'
        const memberTip = project? (firstName + ' is ' +(!isMember? 'not ':'')+ ' a member of this board'):''

        const evs = eventStore.getInstance()
        // const badgeCount = evs.unreadMsgCounts[person.id] || 0
        // const rtBadgeCount = eventStore.getInstance().countNewMsgs(user.id)
        const mobx = evs.unreadMsgCounts
        const rand = evs.rand

        const memberStyle = {fontWeight: badgeCount > 0? '700':'300', marginBottom: '.25rem'}
        const msgIconStyle={float: 'right', marginRight: '0.5rem', marginTop: '0'}

        const dragWrapper = isConnected? connectDragSource : (elem) => elem
        return connectDragSource (
            <div className={personClass} style={personStyle} onMouseDown={this.checkPerson}>

                    <Popup
                        trigger={
                            <div className='relative ' style={{width: '100%'}}>
                                <div style={{backgroundImage: 'url('+person.image+')'}}
                                     className='avatar inline mr1'
                                />

                                {false && pinned && imInBigPane &&
                                <span className='abs' style={{top: '0', left: '26px'}} data-tip={memberTip}>
                                    <Star size={14} className='secondary' />
                                </span>
                                }

                                {false && isMember &&
                                <span className='abs' style={{top: '-3px', left: (pinned && imInBigPane? '32px':'24px')}} data-tip={memberTip}>
                                    <img src='/static/svg/v2/briefcase.svg' width='12px' className='secondary' style={{opacity: 0.5}}/>
                                </span>
                                }

                                {showBadge && badgeCount > 0 && !isMe &&
                                <div className='abs' style={{top: '-.5rem', left: '1.5rem'}}>
                                    <DumbBadge count={badgeCount}/>
                                </div>
                                }
                                <span className="small jFG inline dontWrap" style={memberStyle} >
                                    {memberName}
                                </span>
                            </div>
                        }
                        on='click'
                        open={menu}
                        onOpen={(ev) => this.openMenu(ev)}
                        onClose={(ev) => this.closeMenu(ev)}
                        position='top left'
                        style={{padding: '0', minWidth: '20rem'}}
                        hoverable
                    >
                        {/*<X size={24} onClick={this.closeMenu} className='abs' style={{top: '1rem', right: '1rem'}}/>*/}
{/*
                        <div style={{backgroundImage: 'url(' + person.image + ')'}}
                             className='avatar inline '
                        />
                        <h3 className='inline vaTop'
                            style={{marginLeft: '1rem', marginTop: '0.75rem'}}
                        >{person.displayName}</h3>

                        {person.dateCreated &&
                        <div className='small lighter'>Joined: {person.dateCreated}</div>
                        }
                        {person.dateModified &&
                        <div className='small lighter'>Latest Activity {person.dateModified}</div>
                        }

                        <div className='small lighter'>You
                            have {commonProjectsCount} board{commonProjectsCount === 1 ? '' : 's'} in common.
                        </div>
*/}

                        <Menu vertical className='hioh' style={{width: '100%', zIndex: '1003 !important'}}>
                            {!personHasId && sessionId &&
                            <Menu.Item name="visit" onClick={this.addToContacts}>
                                <span className='small'>Add to Contacts</span>
                            </Menu.Item>
                            }

                            {!mobile && personHasId &&
                            <Menu.Item name="visit" onClick={this.gotoProfile}>
                                <span className='small'>Visit Profile</span>
                            </Menu.Item>
                            }

                            {!mobile && personHasId &&
                            <Menu.Item name="message" onClick={this.dm}>
                                <span className='small'>Direct Message</span>
                            </Menu.Item>
                            }

                            {pinned && personHasId &&
                            <Menu.Item name="visit" onClick={this.unpinUser}>
                                <span className='small'>Remove from favorites</span>
                            </Menu.Item>
                            }

                            {!pinned && personHasId &&
                            <Menu.Item name="visit" onClick={this.pinUser}>
                                <span className='small'>Add to favorites</span>
                            </Menu.Item>
                            }

                            {project && isMember && enableAdmin && personHasId &&
                            <Menu.Item name="remove" onClick={this.removeMember}>
                                <span className='small'>Remove from this board</span>
                            </Menu.Item>
                            }

                            {project && !isMember && personHasId &&
                            <Menu.Item name="remove" onClick={this.addMember}>
                                <span className='small'>Add to this board</span>
                            </Menu.Item>
                            }

                            {isConnected && personHasId &&
                            <Menu.Item name="remove" onClick={this.unfriend} >
                                <span className='small'>Remove from Contacts</span>
                            </Menu.Item>
                            }


                        </Menu>
                    </Popup>
                {/*</div>*/}


{/*
                <div className='uActions showonh abs' style={{left: '4rem'}}> showonh
                    {!isMe &&
                    <MessageCircle size={actionIconSize} className='userAction hoh'
                                   data-tip={'Message ' + memberName} data-type="light" data-place="right"/>
                    }

                    Contacts actions
                    {pinned && !isMe &&
                    <ArrowDown className='third hoh' size={actionIconSize} onClick={(ev) => this.unpinUser(ev)}
                          data-tip={'Unfavorite ' +memberName}    data-type="light" data-place="right"/>
                    }
                    {!pinned && !isMe &&
                    <Star className='hoh' size={actionIconSize} onClick={(ev) => this.pinUser(ev)}
                          data-tip={'Favorite ' +memberName}    data-type="light" data-place="right"/>
                    }
                    {false && !isMe &&
                    <X className={(this.props.isMember? 'fourth':'hoh')} size={actionIconSize} onClick={(ev) => this.disconnectUser(ev)}
                       disabled={this.props.isMember}
                       data-tip={removeContactTip}    data-type="light" data-place="right"/>
                    }

                    Membership actions
                    {false && this.props.isMember && this.props.project && !isCreator &&
                    <LogIn className='hoh' size={actionIconSize} style={{transform: 'rotate(180deg)'}} onClick={(ev) => this.toggleMembership(ev)}
                           data-tip={memberTip}    data-type="light" data-place="right"/>
                    }
                    {false && !this.props.isMember && !this.props.isInvited && this.props.project &&
                    <LogIn className='hoh' size={actionIconSize} onClick={(ev) => this.toggleMembership(ev)}
                           data-tip={memberTip}    data-type="light" data-place="right"/>
                    }
                    {false && this.props.isInvited &&
                    <span className="tiny third right hoh" onClick={(ev) => this.toggleMembership(ev)}>
                        invited
                    </span>
                    }
                    {false && this.props.isMember && this.props.project && isCreator &&
                    <Lock data-tip={memberName + ' is the owner.'} data-type="light" data-place="right"/>
                    }


                </div>
*/}

            </div>
        )
    }
}
