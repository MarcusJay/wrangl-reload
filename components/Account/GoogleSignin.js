import {Component} from 'react'
import UserApiService from '/services/UserApiService';

import userStore from '/store/userStore'
import routes from '~/routes'

import {Button, Icon} from 'semantic-ui-react'

class GoogleLogin extends Component {

  constructor (props) {
    super(props)

    this.state = {
    }

    this.googleInit = this.googleInit.bind(this)
    this.handleGoogleLogin = this.handleGoogleLogin.bind(this)
  }

  componentWillMount() {
  }

  componentDidMount() {
      if (typeof window !== 'undefined') {
          // Load Google Auth library
          let self = this;
          if (typeof(gapi) === 'undefined')
              return
          gapi.load('auth2', {
              callback: () => {
                  // Handle gapi.client initialization.
                  self.googleInit();
              },
              onerror: (error) => {
                  // Handle loading error.
                  console.error(error)
              },
              timeout: 5000, // 5 seconds.
              ontimeout: () => {
                  // Handle timeout.
                  console.log('Google timed out, took more than 5 seconds');
              }
          });
      }
  }

  googleInit() {
    //initialize Google Auth
    gapi.auth2.init({
      client_id: '862445081010-fonnl3na30sanh6i2hodkn2l5duc67h6.apps.googleusercontent.com',
      cookiepolicy: 'single_host_origin',
      fetch_basic_profile: true  //this sets scope to: profile, email, openid
    }).then( (authResult) => {
        console.log('Successful Google initialization');
    });
  }

  // not used - if want to attach sign in function to another element
  // attachGoogleSignin() {
  //   const element = document.getElementById('google-oauth2');
  //   var authInstance = gapi.auth2.getAuthInstance();
  //   authInstance.attachClickHandler(element, {}, function(googleUser) {
  //     //user obj with tokens is here
  //   }, function(err) {
  //     // error handle
  //   });
  // }

  handleGoogleLogin(event) {
    event.stopPropagation();
    let self = this

    if (!gapi || !gapi.auth2)
        return

    let auth2 = gapi.auth2.getAuthInstance();
    auth2.signIn({
      scope: 'profile email openid'
    }).then((googleUser) => { // we get back our tokens, and user obj
        const id_token = googleUser.getAuthResponse().id_token;
        console.log('ID TOKEN', googleUser.getAuthResponse().id_token)
        //const access_token = googleUser.getAuthResponse().access_token;
        console.log('GOOGLE LOGIN SUCCESS');
        //alert('success. idToken is ' + googleUser.getAuthResponse().id_token);

        //make call
        UserApiService.loginUserByOAuth(id_token, 'google').then(response => {
          // alert('THIS WORKED')
            userStore.setCurrentUser( response.data )
            if (self.props && self.props.redirect && self.props.redirect !== "login")
                routes.Router.replaceRoute('/project/'+self.props.redirect, '/project/'+self.props.redirect, { shallow: false })
            else
                routes.Router.replaceRoute('projectsPage', 'projectsPage', { shallow: false })
        })
    })
  }

  render () {
    return (
      <Button style={{backgroundColor: '#f0592b'}} onClick={this.handleGoogleLogin} className="socialLogin">
        <Icon name='google plus' /> Google
      </Button>
    )
  }
}

export default GoogleLogin;
