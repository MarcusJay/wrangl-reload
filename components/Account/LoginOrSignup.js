/**
 * Warning: gratuitous use of ... spread. I can barely read it. Hope you can.
 */
import {Component} from 'react'
import {
    Button,
    Container,
    Dimmer,
    Divider,
    Form,
    Grid,
    Header,
    Loader,
    Message,
    Popup,
    Segment
} from 'semantic-ui-react'
import FacebookSignin from './FacebookSignin'
import userStore from '/store/userStore'
import projectStore from '/store/projectStore'
import routes from '~/routes'
// const Router = require('next-routes')();
import UserApiService from '/services/UserApiService';
import UserSessionUtils from "../../utils/UserSessionUtils";
import isEmail from 'validator/lib/isEmail';
import {X} from "react-feather";
import {getUrlComponents} from "../../utils/solo/getUrlComponents";
import EmailService from "../../services/EmailService";
import {ProjectType} from "../../config/Constants";
import isValidEmail from "../../utils/solo/isValidEmail";

const WRONG_PASSWORD = "user_login | password incorrect"

const embedStyle = {boxShadow: 'none !important', border: '0 !important', backgroundColor: 'transparent !important'}
const pageStyle = {
    boxShadow: '0 0 40px rgba(0,0,0,0.3) !important',
    backgroundColor: 'rgba(255,255,255,0.97) !important',
    color: '#333'
}

export default class LoginOrSignup extends Component {

    static async getInitialProps(ctx) {
        console.error ("## Only pages are alleged to have getInitialProps. This is not a page. If you are reading this, nextjs has lied.")
    }

    constructor(props) {
        super (props)

        console.log ("SHOW", routes.Router)
        // this.redirect = props.redirect
        this.query = props.query
        this.req = props.req
        userStore.logoutUser ()

        this.state = {
            identifier: '',
            password: '',
            email: '',

            redirect: props.projectId,
            loading: false,

            loginWarning: false,
            loginWarningMessages: [],
            loginWarningMessage: 'Something is amiss...',
            loginIdentifierError: false,
            loginPasswordError: false,
            loginSuccessful: false,

            signUpWarning: false,
            signUpWarningMessages: [],
            signUpWarningMessage: 'Something is amiss...',
            signUpIdentifierError: false,
            signUpEmailError: false,
            signUpPasswordError: false,
            signUpSuccesful: false,

            promocode: '',
            promocodeWarning: false,

            showRecoveryConfirm: false,
            show: 'signUp' // this.props.show
        }

        let projectId

        if (this.state.redirect === undefined && typeof(window) !== 'undefined') {
            let url = getUrlComponents (window.location.href)
            if (url.search !== undefined && url.search.length > 0)
                projectId = url.search.substr (1)
            else if (url.pathname !== undefined && url.pathname.length > 0)
                projectId = url.pathname.substr (url.pathname.lastIndexOf ('/') + 1)

            if (["signup", "login"].indexOf (projectId) === -1) {
                this.state.redirect = projectId
            }
        }
    }

    setLoading(val) {
        this.setState ({loading: val})
    }


    // mock data example
    signupPayload() {
        return {
            "API_Access_Key": "BCE1674E-8E67-C1C9-BB15-D51BAFDB6FF2", // Required
            "display_name": "Joe Smith Jr.", // max length: 32
            "first_name": "Joeseph", // max length: 32
            "last_name": "Smith", // max length: 32
            "image": "https://www.example.com/example.jpg", // max length: 128
            "email": "j.smith@example.com", // max length: 32
            "password": "short", // min length: 4, max length: 32
            "phone": "555-555-5555" // max length: 32
        }
    }

    // helper function for testing
    makeSuffix() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt (Math.floor (Math.random () * possible.length));

        return text;
    }

    /**
     * Sign Up Handler
     *
     * Sign up user for
     */
    signUpHandler() {
        // pull down our state value
        const {identifier, password} = this.state

        let updateState = {}
        let invalidUserInput = false
        let collectWarnings = []

        // TODO : validationn :: check for valid name chars / length
        if (!identifier) {
            updateState = {
                ...updateState, // TODO why this spread madness?
                ...{
                    signUpWarning: true,
                    signUpIdentifierError: true
                }
            }
            collectWarnings.push ('Please enter a valid email')
            invalidUserInput = true
        }
        else {
            // reset
            updateState = {
                ...updateState,
                ...{
                    signUpWarning: false,
                    signUpIdentifierError: false
                }
            }
        }

        // TODO : validation :: check for valid password chars / length
        if (!password) {
            updateState = {
                ...updateState,
                ...{
                    signUpWarning: true,
                    signUpPasswordError: true
                }
            }
            collectWarnings.push ('Please enter a valid password')
            invalidUserInput = true
        }
        else {
            // reset
            updateState = {
                ...updateState,
                ...{
                    signUpWarning: false,
                    signUpPasswordError: false
                }
            }
        }

        // save the warnings
        updateState = {
            ...updateState,
            ...{
                signUpWarningMessages: collectWarnings
            }
        }


        // update the state
        if (invalidUserInput) {
            this.setState (updateState)
            return false
        }
        else {
            // reset
            updateState = {
                ...updateState,
                ...{
                    signUpWarning: false,
                    signUpIdentifierError: false,
                    signUpPasswordError: false,
                    signUpWarningMessages: []
                }
            }

        }

        this.setState ({loading: true})

        // Check for existing user first. If correct creds, log them in. If not, prevent dups.
        UserApiService.loginUser (
            identifier,
            password
        ).then ((response) => {
            userStore.setCurrentUser (response.data)

            const projectId = this.state ? this.state.redirect : null
            if (!projectId || projectId.length === 0 || projectId === "login")
                routes.Router.replaceRoute ('/', {shallow: false})
            else {
                projectStore.getProjectById (projectId).then (project => {
                    if (!project) {
                        routes.Router.replaceRoute ('/', {shallow: false})
                        return
                    }

                    const isApproval = project.projectType === ProjectType.APPROVAL
                    const path = isApproval ? 'approve' : 'project'
                    routes.Router.replaceRoute ('/' + path + '/' + projectId, '/' + path + '/' + projectId, {shallow: false})

                }).catch (error => {
                    console.log (error)
                    routes.Router.replaceRoute ('/', {shallow: false})
                })
            }

        }).catch (errorObj => {

            // User exists already
            if (errorObj.response.data.error && errorObj.response.data.error.startsWith(WRONG_PASSWORD)) {
                this.setState ({
                    loading: false,
                    signUpWarning: true,
                    signUpWarningMessages: ['This name is already taken.', "If it's you, please Login instead."]
                })
                return
            }

            // User is new. Proceed with sign up.
            const email = isEmail (identifier) ? identifier : null
            const name = isEmail (identifier) ? null : identifier

            UserApiService.createUser (
                email,
                name,
                password
            ).then ((response) => {

                UserSessionUtils.setDefaultPrefs ()
                userStore.setCurrentUser (response.data)
                userStore.hydrateNewUser (response.data)

                const projectId = this.state ? this.state.redirect : null
                if (!projectId || projectId.length === 0 || projectId === "login")
                    routes.Router.replaceRoute ('/', {shallow: false})
                else {
                    projectStore.getProjectById (projectId).then (project => {
                        if (!project) {
                            routes.Router.replaceRoute ('/', {shallow: false})
                            return
                        }

                        const isApproval = project.projectType === ProjectType.APPROVAL
                        const path = isApproval ? 'approve' : 'project'
                        routes.Router.replaceRoute ('/' + path + '/' + projectId, '/' + path + '/' + projectId, {shallow: false})

                    }).catch (error => {
                        console.log (error)
                        routes.Router.replaceRoute ('/', {shallow: false})
                    })
                }

            }).catch ((error) => {
                updateState = {
                    ...updateState,
                    ...{
                        signUpWarning: true,
                        signUpWarningMessages: error
                    }
                }
                throw new Error (error)
            }).then (() => {
                updateState = {
                    ...updateState,
                    ...{
                        loading: false
                    }
                }

                // updateState object merge is our ghetto reducer superstar
                // resolve our state conditions
                this.setState (updateState)
            })
        })

    }


    /**
     *
     */
    loginHandler() {
        // pull down our state value
        const {identifier, password} = this.state

        // holds state
        let updateState = {}
        let invalidUserInput = false

        // TODO : validationn :: check for valid name chars / length
        if (!identifier) {
            updateState = {
                ...updateState,
                ...{
                    loginWarning: true,
                    loginWarningMessage: 'Please enter a name or email',
                    loginIdentifierError: true
                }
            }
            invalidUserInput = true
        }
        else {
            updateState = {
                ...updateState,
                ...{
                    loginWarning: false,
                    loginIdentifierError: false
                }
            }
        }

        // TODO : validation :: check for valid password chars / length
        if (!password) {
            updateState = {
                ...updateState,
                ...{
                    loginWarning: true,
                    loginWarningMessage: 'Please enter a a valid password',
                    loginPasswordError: true
                }
            }

            invalidUserInput = true
        }
        else {
            // reset
            updateState = {
                ...updateState,
                ...{
                    loginWarning: false,
                    loginPasswordError: false
                }
            }
        }

        // update the state
        if (invalidUserInput) {
            this.setState (updateState)
            return false
        }
        else {
            // reset
            updateState = {
                ...updateState,
                ...{
                    loginWarning: false,
                    loginIdentifierError: false,
                    loginPasswordError: false
                }
            }

        }

        this.setState ({loading: true})

        /**
         * User submited valid inputs
         * Send to Api
         */
        UserApiService.loginUser (
            identifier,
            password
        ).then ((response) => {

            userStore.setCurrentUser (response.data)

            const projectId = this.state ? this.state.redirect : null
            if (!projectId || projectId.length === 0 || projectId === "login")
                routes.Router.replaceRoute ('/', {shallow: false})
            else {
                projectStore.getProjectById (projectId).then (project => {
                    if (!project) {
                        routes.Router.replaceRoute ('/', {shallow: false})
                        return
                    }

                    const isApproval = project.projectType === ProjectType.APPROVAL
                    const path = isApproval ? 'approve' : 'project'
                    routes.Router.replaceRoute ('/' + path + '/' + projectId, '/' + path + '/' + projectId, {shallow: false})

                }).catch (error => {
                    console.log (error)
                    routes.Router.replaceRoute ('/', {shallow: false})
                })
            }
        }).catch ((error, reponse) => {
            // update component state with errors
            updateState = {
                ...updateState,
                ...{
                    loginWarning: true,
                    loginWarningMessage: "login failed, try again"
                }
            }
            console.log ('error -> here -->', error)
        }).then (() => {

            // no loger in load state
            updateState = {
                ...updateState,
                ...{
                    loading: false
                }
            }

            // update component with final state
            this.setState (updateState)
        })

    }

    /*
        loginDemoUser() {
            userStore.setDemoUser()
            routes.Router.replaceRoute('projectsPage', 'projectsPage', { shallow: true })
        }
    */

    /**
     * Input handler
     *
     * update component stats with user input values
     **/
    inputHandleChange = (e, {name, value}) => {

        let validation = {}

        if (name === 'identifier') {
            // todo add all the rules
            // validation check
            if (value.length > 3) {
                validation = {...validation, ...{signUpIdentifierError: false}}
            }

        }

        if (e.target.id === 'signup') {
            this.setState({showEmailWarning: !isValidEmail(value)})
        }


        // password
        if (name === 'password') {

            // todo add all the rules
            // validation check
            if (value.length > 3) {
                validation = {...validation, ...{signUpPasswordError: false}}
            }

        }

/*
        else if (name === 'promocode') {
            // validation check
            if (value.length > 3) {
                validation = {...validation, ...{promocodeWarning: false}}
            }

        }
*/

        this.setState ({
            ...{
                [name]: value
            },
            ...validation
        })
    }

    checkForExistingUser = (ev) => {
        if (!this.state.identifier)
            return
        UserApiService.verifyUserEmail(this.state.identifier).then( response => {
            if (response.data.user_exists) {
                // this.setState({signUpWarningMessages: ['This email already exists. Please login or choose another email.']})
                this.setState({showEmailExists: true})
            }
        })
    }

    onRecoveryOpen = (ev) => {
        this.setState ({recoveryOpen: true, showRecoveryConfirm: false, showRecoveryError: false})
    }
    onRecoveryClose = (ev) => {
        this.setState ({recoveryOpen: false})
    }

    handleRecoveryChange = (ev) => {
        this.setState ({identifier: ev.target.value})
    }

    handleRecoverySubmit = (ev) => {
        this.setState ({showRecoveryConfirm: false, showRecoveryError: false})
        const email = this.state.identifier
        UserApiService.generatePasswordResetCode (email).then (response => {
            if (response && response.data) {
                EmailService.sendPasswordReset (response.data)
                this.setState ({showRecoveryConfirm: true})
            }
        }).catch (error => {
            console.log (error)
            // Toaster.info('There was a problem resetting your password. Please retry in a few minutes.')
            this.setState ({showRecoveryError: true, error: error})
        })

    }

    render() {
        const {showEmailWarning, showEmailExists} = this.state
        const sizeStyle = this.props.embedded ? {width: '20%', minWidth: '300px'} : {}
        const boxStyle = this.props.embedded ? embedStyle : pageStyle
        const showDefault = !this.state.showRecoveryConfirm && !this.state.showRecoveryError
        const isEmailValid = isValidEmail(this.state.identifier)

        const redirect = this.state ? this.state.redirect : null

        // TODO use /redirect or ?redirect, not both
        return (
            <div style={{textAlign: 'center'}}>
                <Dimmer.Dimmable dimmed={this.state.loading}>
                    <Dimmer active={this.state.loading} inverted>
                        <Loader inverted style={{marginTop: '0', height: '10%'}}>Loading...</Loader>
                    </Dimmer>
                    <Container className='loginPage' style={sizeStyle}>
                        {this.props.show === 'signUp' &&
                        <div className='centered'>
                            <Segment padded className='signupBox' style={boxStyle}>
                                <Header>Sign Up</Header>
                                <FacebookSignin redirect={redirect} loading={(val) => this.setLoading (val)}/>
                                {/*<GoogleSignin redirect={redirect}/>*/}
                                <Divider horizontal>Or</Divider>
                                <Form warning={this.state.signUpWarning}
                                      onSubmit={() => {
                                          this.signUpHandler ()
                                      }}>
                                    <Form.Field>
                                        <Form.Input

                                            label='Enter an email'
                                            name='identifier' /*identifier'*/
                                            id='signup'
                                            value={this.state.identifier}
                                            error={this.state.signUpIdentifierError}
                                            placeholder='name@email.com'
                                            onChange={this.inputHandleChange}
                                            onBlur={this.checkForExistingUser}
                                        />
                                    </Form.Field>
                                    {showEmailWarning &&
                                    <span className='red left italic'
                                          style={{marginTop: '-.5rem', marginLeft: '.5rem'}}>
                                        Please enter a valid email
                                    </span>
                                    }
                                    {showEmailExists &&
                                    <span className='red italic'
                                          style={{textAlign: 'left', marginTop: '-.5rem', marginLeft: '.5rem'}}>
                                        This email already has an account. <br/>
                                        Please <b><a href="https://pogo.io/login">Login</a></b> or choose another email.
                                    </span>
                                    }

                                    <Form.Field>
                                        <Form.Input

                                            name='password'
                                            value={this.state.password}
                                            onChange={this.inputHandleChange}
                                            onFocus={this.checkForExistingUser}
                                            error={this.state.signUpPasswordError}
                                            label='Password' type='password' placeholder='Password'/>
                                    </Form.Field>
                                    <Form.Field>
                                        <Form.Input
                                            label='Promo Code (optional)'
                                            name='promocode' /*identifier'*/
                                            id='promocode'
                                            value={this.state.promocode}
                                            error={this.state.promocodeError}
                                            placeholder='Promo code'
                                            onChange={this.inputHandleChange}
                                        />
                                    </Form.Field>
                                    <Message
                                        warning
                                        // header='Could you check something?'
                                        list={this.state.signUpWarningMessages}
                                    />
                                    <Button content='Sign Up'
                                            disabled={!isEmailValid}
                                            className={'manualLogin' + (isEmailValid? '':' disabled')}
                                            type='submit'
                                            fluid/>
                                    <div className="panelSwitcher loginDefaults">
                                        Already a member?
                                        <a href={redirect && redirect.length > 0 ? "/login/" + redirect + "?" + redirect : "/login"}
                                           className="loginLink"> Login</a>

                                    </div>
                                </Form>
                            </Segment>
                        </div>
                        }


                        {this.props.show === 'login' &&
                        <div className="centered">
                            <Segment padded className="loginBox" style={boxStyle}>
                                <Header>Member Login</Header>
                                <FacebookSignin redirect={redirect} loading={(val) => this.setLoading (val)}/>
                                {/*<GoogleSignin redirect={redirect}/>*/}
                                <Form warning={this.state.loginWarning}
                                      onSubmit={() => {
                                          this.loginHandler ()
                                      }}>
                                    <Divider horizontal>Or</Divider>
                                    <Form.Field>
                                        <Form.Input

                                            label='Display Name or Email'
                                            name='identifier'
                                            value={this.state.identifier}
                                            error={this.state.loginIdentifierError}
                                            placeholder='name or name@email.com'
                                            onChange={this.inputHandleChange}
                                        />
                                    </Form.Field>
                                    <Form.Field>
                                        <Form.Input

                                            name='password'
                                            value={this.state.password}
                                            onChange={this.inputHandleChange}
                                            error={this.state.loginPasswordError}
                                            label='Password' type='password' placeholder='Password'/>
                                    </Form.Field>
                                    <Message
                                        warning
                                        header='Incorrect email or password'
                                        content={'Please check spelling and try again. '}
                                    />
                                    <Button content='Login' className="manualLogin" type='submit' fluid/>
                                </Form>

                                <div className="panelSwitcher loginDefaults">
                                    Not a member yet?
                                    {redirect &&
                                    <a href={"/signup/" + redirect + "?" + redirect} className="signupLink"> Signup</a>
                                    }
                                    {!redirect &&
                                    <a href={redirect && redirect.length > 0 ? "/signup/" + redirect + "?" + redirect : "/signup"}
                                       className="signupLink"> Signup</a>
                                    }
                                    <Popup trigger={<div style={{marginTop: '0.5rem'}}><a className='small pointer'>Forgot
                                        password?</a></div>}
                                           on='click' wide
                                           style={{left: '35%', minHeight: '60%', minWidth: '30%', paddingTop: '4rem'}}
                                           open={this.state.recoveryOpen}
                                           onOpen={this.onRecoveryOpen}
                                           onClose={this.onRecoveryClose}
                                           position='top center'
                                           className='loginDefaults'
                                    >
                                        <div style={{padding: '2rem'}} className='loginDefaults'>
                                            <div className='absTopRight'>
                                                <X className='secondary' onClick={this.onRecoveryClose}/>
                                            </div>
                                            <Form onSubmit={(ev) => this.handleRecoverySubmit (ev)}>
                                                <div>
                                                    {showDefault &&
                                                    <div className='recoveryBody'>
                                                        <h2>Forgot your password? <br/>
                                                            No problem!</h2>

                                                        <p style={{marginTop: '3rem'}}>
                                                            We'll be happy to send you a link to reset it. Simply enter
                                                            your email below!
                                                        </p>
                                                    </div>
                                                    }

                                                    {this.state.showRecoveryConfirm &&
                                                    <div className='recoveryBody'>
                                                        <h2>Email sent! </h2>
                                                        <p>We've sent you an email containing a link to reset your
                                                            password. It should arrive in a couple minutes!</p>
                                                    </div>
                                                    }

                                                    {this.state.showRecoveryError &&
                                                    <div className='recoveryBody'>
                                                        <h2>Email not found. </h2>
                                                        <p>Hm, we don't have an account with that email. </p>
                                                        <p>Would you like to try a different email? Or you can <a
                                                            href={"/signup/" + redirect + "?" + redirect}
                                                            className="signupLink"> Signup</a>.</p>
                                                    </div>
                                                    }
                                                    {!this.state.showRecoveryConfirm &&
                                                    <Form.Input
                                                        className='loginDefaults'
                                                        name='email'
                                                        value={this.state.identifier}
                                                        onChange={(ev) => this.handleRecoveryChange (ev)}
                                                        label='Email' placeholder='name@example.com'/>
                                                    }
                                                </div>
                                                {!this.state.showRecoveryConfirm &&
                                                <Button content='Submit' className="manualLogin go" type='submit'
                                                        fluid/>
                                                }
                                                {this.state.showRecoveryConfirm &&
                                                <Button content='Close' className="manualLogin go" fluid
                                                        onClick={this.onRecoveryClose}/>
                                                }
                                            </Form>
                                        </div>
                                    </Popup>
                                </div>
                            </Segment>
                        </div>
                        }


                    </Container>
                </Dimmer.Dimmable>
            </div>
        )
    }
}
