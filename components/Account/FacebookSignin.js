import {Component} from 'react'
import UserApiService from '/services/UserApiService';
import userStore from '/store/userStore';
// import { routes } from '/routes';
// import routes from '~/routes'
import {Router} from '~/routes'

import {Button, Icon} from 'semantic-ui-react'
import {ProjectType} from "../../config/Constants";
import projectStore from "../../store/projectStore";
import Toaster from '/services/Toaster'

class FacebookLogin extends Component {
    state = {loading: false}

    loginFBUser = (token) => {
        UserApiService.loginUserByOAuth (token, 'facebook').then (response => {
            userStore.setCurrentUser (response.data)
            this.setState ({loading: false})

            const projectId = self.props ? self.props.redirect : null
            if (!projectId || projectId.length === 0 || projectId === "login")
                Router.replaceRoute ('/', {shallow: false})
            else {
                projectStore.getProjectById (projectId).then (project => {
                    if (!project) {
                        Router.replaceRoute ('/', {shallow: false})
                        return
                    }

                    const isApproval = project.projectType === ProjectType.APPROVAL
                    const path = isApproval ? 'approve' : 'project'
                    Router.replaceRoute ('/' + path + '/' + projectId, '/' + path + '/' + projectId, {shallow: false})

                }).catch (error => {
                    console.log (error)
                    Router.replaceRoute ('/', {shallow: false})
                })
            }
        }).catch (error => {
            this.setState ({loading: false})
            console.error (error)
            Toaster.error ("Facebook login is currently unavailable.")
        })
    }


    componentWillMount() {
        if (typeof window !== 'undefined') {
            // TODO move this higher, as it's needed elsewhere
            window.fbAsyncInit = function() {
                FB.init({
                    appId            : '1742126849378172',
                    autoLogAppEvents : true,
                    xfbml            : true,
                    version          : 'v3.3'
                });
            };

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName (s)[0];
                if (d.getElementById (id)) {
                    return;
                }
                js = d.createElement (s);
                js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore (js, fjs);
            } (document, 'script', 'facebook-jssdk'));

        }
    }

    /**
     * FB response format:
     {
         status: connected | not_authorized | unknown
         authResponse: {
             accessToken: '...',
             expiresIn:'...',
             signedRequest:'...',
             userID:'...'
         }
     }
     */
    handleFacebookLogin = (event) => {
        debugger
        event.stopPropagation ();
        let self = this;
        if (!FB) {
            console.warn ("FB script not loaded yet")
            return
        }
        this.setState ({loading: true})
        this.props.loading (true) // old. deprecate.

        try {
/*          Calling FB.login from promise resolution triggers mobile safari popup blocker
            FB.getLoginStatus( (response) => {
                if (response.status === 'connected') {
                    this.loginFBUser (response.authResponse.accessToken)
                }
                else {
*/
                    FB.login ((response) => {
                        this.loginFBUser(response.authResponse.accessToken)
                        }, {
                            scope: 'public_profile, email' // , user_friends'
                        }
                    )
/*
                }
            })
*/
        } catch (error) {
            this.setState ({loading: false})
            console.error (error)
            Toaster.error ("Facebook login is currently unavailable.")
        } finally {
            this.setState ({loading: false})

        }
    }

    render() {
        return (
            <Button color='facebook' onClick={this.handleFacebookLogin} className="socialLogin">
                {!this.state.loading &&
                <span>
                <Icon name='facebook'/> Facebook
              </span>
                }
            </Button>
        )
    }
}

export default FacebookLogin;
