# CHANGELOG
-----------
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

- all changelog unreleased are sourced from _TODO.md
- all _TODO items are discussed in _DISCUSSION.md

#### guidelines
`
Added        for new features.
Changed      for changes in existing functionality.
Deprecated   for soon-to-be removed features.
Removed      for now removed features.
Fixed        for any bug fixes.
Security     in case of vulnerabilities.
`

## [Unreleased]
- ADD dynamic routing

## [0.0.3](#0.0.3) - 2017-10-05
### ADDED
- ADD sass compilation

## [0.0.2](#0.0.2) - 2017-10-05
### ADDED
- an apiStore to recieve data from api Calls
- react-inspector
- appStore to index page
- layouts folder
- getInitialProps folder for server side rendering logic seperation

### CHANGED
- CHANGED appStore to appStore
- CHANGED appStore reference to _root

## [0.0.1](#0.0.1) - 2017-09-27
### ADDED
- folders
    + models
    + _getInitialProps
    + api
    + services
- libs
    + inline-react-svg
    + jest
    + markdown to jsx
    + markdown to jsx [github](https://github.com/probablyup/markdown-to-jsx)
    + micro
    + mobx
    + nextjs
    + react-table
    + semantic ui
    + yarn
- docs
    + _CHANGELOG.md
    + _DISCUSSION.md
    + _REF.md
    + _TODO.md
    + _TODO.md
    + subfolder READ.md
- configuration
    + .editorconfig [airbnb](https://raw.githubusercontent.com/airbnb/javascript/master/.editorconfig)
    + liscense.txt