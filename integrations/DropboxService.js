import axios from 'axios'
import userStore from '/store/userStore'
import integrationStore from '/store/IntegrationStore'

export const ImagePreviewTypes = ['.jpg','.png','.jpeg','.gif', '.bmp', '.webp']
export const PDFPreviewTypes = ['.ai', '.doc', '.docm', '.docx', '.eps', '.odp', '.odt', '.pps', '.ppsm', '.ppsx', '.ppt', '.pptm', '.pptx', '.rtf']
export const HTMLPreviewTypes = ['.csv', '.ods', '.xls', '.xlsm', '.xlsx.']

const START_AUTH_FLOW = "https://www.dropbox.com/oauth2/authorize"
const APP_KEY = '1gygsvwbtlxbd0z'
const LOCAL_REDIRECT_URI = "http://localhost:8080/dropboxAuth"
const REMOTE_REDIRECT_URI = "https://pogo.io/dropboxAuth"

const remoteDropboxer = "https://dropbox.pogo.io/"
const localDropboxer = "http://localhost:3000/"

export const currDropboxer = remoteDropboxer // localDropboxer //
export const currRedirectUri = REMOTE_REDIRECT_URI // LOCAL_REDIRECT_URI //

export default class DropboxService {

    // NOT USED. If successful, this will hit dropboxAuth.
/*
    static authorize() {
        return axios ({
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: DropboxService.getAuthorizationUrl()
        })
    }
*/

    static getAuthorizationUrl() {
        const userId = userStore.currentUser.id
        const now = new Date()
        const r = Math.random()
        let state = "u-" +userId+ "|rn=" +(r*now)

        return START_AUTH_FLOW + '/?response_type=code&client_id=' +APP_KEY+ '&redirect_uri=' +currRedirectUri+ '&state='+state
    }

    // oauth2 code flow: exchange code for token on server side
    static requestToken(code) {
        return axios ({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: currDropboxer + 'token',
            data: {
                code: code
            }
        })
    }


    static revokeToken(token) {
        return axios ({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: currDropboxer + 'revoke',
            data: {
                token: token
            }
        })
    }

    // UNUSED
/*
    static registerAccessToken(token, uid, accountId) {
        integrationStore.setDropboxAccessToken(token)
        debugger

    }
*/

    static getShareLinks(assets) {

        // TODO refactor this: add a dropbox endpoint that takes an array of paths

        let dropboxerCalls = []
        assets.forEach( asset => {
            dropboxerCalls.push( this.getShareLink(asset))
        })

        let asset
        return new Promise( (resolve, reject) => {

            Promise.all( dropboxerCalls ).then( results => {
                results.forEach( result => {
                    asset = assets.find( asset => asset.id === result.data.id)
                    if (asset)
                        asset.url = result.data.link
                })
                resolve( assets )
            }).catch( error => {
                reject( error )
            })
        })

    }

    static getShareLink(file) {
        const token = integrationStore.dropboxToken

        return axios ({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: currDropboxer + 'shareLink',
            data: {
                path: file.path,
                id: file.id,
                token: token
            }
        })
    }

    static listFilesInFolder(path) {
        if (!integrationStore.isDropboxAuthorized())
            throw new Error('listfiles: dropbox is not authorized.')

        const token = integrationStore.dropboxToken

        if (!path)
            path = ''

        return axios ({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: currDropboxer + 'list?path='+path,
            data: {
                path: path,
                token: token
            }
        })
    }

    static getFileImage(file) {
        if (!integrationStore.isDropboxAuthorized())
            throw new Error('getFileImage: dropbox is not authorized.')

        const token = integrationStore.dropboxToken

        return axios ({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: currDropboxer + 'image?path='+file.path,
            data: {
                path: file.path,
                id: file.id,
                token: token
            }
        })
    }

    static getMimeType( path ) {
        let ext = path.substring( path.lastIndexOf('.') )
        if (!ext)
            return null

        ext = ext.toLowerCase()
        if (ImagePreviewTypes.indexOf( ext ) !== -1)
            return 'image/jpeg'
        else if (PDFPreviewTypes.indexOf( ext ) !== -1)
            return 'application/pdf'
        else if (HTMLPreviewTypes.indexOf( ext ) !== -1)
            return 'text/html'
        else //
            return null
    }
}

