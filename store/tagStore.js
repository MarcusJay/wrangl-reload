import {action, observable} from 'mobx';

import TagApiService from '/services/TagApiService'
import ProjectApiService from '/services/ProjectApiService'

import projectStore from '/store/projectStore'
import cardStore from '/store/cardStore'
import uiStateStore from '/store/uiStateStore'

import TagModel from '/models/TagModel'
import ProjectModel from '/models/ProjectModel'
import CardModel from '/models/CardModel'
import {DefaultTags} from "/config/TagConfig"
import ColorUtils, {DROPBOX_COLOR} from "../utils/ColorUtils";
import {getHostname} from "../utils/solo/getHostname";
import {pathToTag} from "../utils/solo/pathToTag";
import {getCardDetails} from "../services/CardApiService";

class tagStore {
    @observable userTags = []
    @observable currentTagSet = [] // tag set for the currently open project

    constructor() {
    }

    areTaggedCardsOnDisplay() {

        const cards = cardStore.isFiltering? cardStore.filteredCards : cardStore.currentCards
        if (cards.some === undefined)
            debugger

        return cards.some( card => card.tags2 !== undefined && card.tags2.length > 0)
    }

    getUserTags(userId) {
        return new Promise((resolve, reject) => {
            TagApiService.getUserTags (userId).then (response => {
                console.log ("Loaded user tags: " + response)
                this.userTags = response.data.tags.map (apiTag => new TagModel (apiTag));
                resolve (this.userTags)
            })
        })
    }

    @action createUserTag(name, color) {
        return new Promise((resolve, reject) => {
            TagApiService.createUserTag (name, color).then (response => {
                resolve( new TagModel(response.data) )
            })
        })
    }

    @action updateTag(tagObj) {
        return new Promise((resolve, reject) => {
            TagApiService.updateUserTag(tagObj).then( response => {
                this.getUserTags ().then (response => {
                    resolve (this.userTags)
                })
            })
        })
    }

    @action deleteUserTag(tag) {
        return new Promise((resolve, reject) => {
            TagApiService.deleteUserTag(tag).then( response => {
                this.getUserTags ().then (response => {
                    resolve (this.userTags)
                })
            })
        })
    }

    // Project Level Tags
    @action tagProject(tagId, projectId) {
        return new Promise((resolve, reject) => {
            TagApiService.tagProject(tagId, projectId).then( response => {
                ProjectApiService.getProjectById(projectId).then (response => {
                    let project = new ProjectModel( response.data )
                    resolve( project.tags2 )
                })
            })
        })
    }

    // Cardable Project Sets
    @action addTagToCardableSet(projectId, name, color) {
        return new Promise((resolve, reject) => {
            TagApiService.createUserTag (name, color).then (response => {
                let tag = new TagModel( response.data )
                TagApiService.addTagToCardableSet(tag.id, projectId).then (response => {

                    ProjectApiService.getProjectById(projectId).then (response => {
                        let project = new ProjectModel( response.data )
                        this.currentTagSet = project.tagSet2
                        resolve( this.currentTagSet )
                    })
                })
            })
        })
    }

    @action updateTagInCardableSet(projectId, tagObj) {
        return new Promise((resolve, reject) => {
            TagApiService.updateUserTag(tagObj).then (response => { // update original tag, change propagates to project as it's a join
                ProjectApiService.getProjectById(projectId).then (response => {
                    let project = new ProjectModel( response.data )
                    this.currentTagSet = project.tagSet2
                    resolve( this.currentTagSet )
                })
            })
        })
    }

    @action deleteTagFromCardableSet(projectId, tagId) {
        return new Promise ((resolve, reject) => {
            TagApiService.deleteTagFromCardableSet(tagId, projectId).then (response => {
                ProjectApiService.getProjectById (projectId).then (response => {
                    let project = new ProjectModel (response.data)
                    this.currentTagSet = project.tagSet2
                    resolve (this.currentTagSet)
                })
            })
        })
    }

    // Card Level Tags
    @action tagCard(tagId, cardId) {
        return new Promise((resolve, reject) => {
            TagApiService.tagCard(tagId, cardId).then( response => {
                uiStateStore.forceShowTags = true // Let's assume if user is tagging, they want to see them immediately. Appropriate?
                getCardDetails(cardId).then (apiCard => {
                    let card = new CardModel( apiCard )
                    if (uiStateStore.viewingCard && uiStateStore.viewingCard.id === card.id)
                        uiStateStore.setViewingCard(card)
                    else if (uiStateStore.editingCard && uiStateStore.editingCard.id === card.id)
                        uiStateStore.setEditingCard(card)
                    resolve (card)
                })
            })
        })
    }

    @action untagCard(tagId, cardId) {
        return new Promise((resolve, reject) => {
            TagApiService.untagCard(tagId, cardId).then( response => {
                getCardDetails(cardId).then (apiCard => {
                    let card = new CardModel( apiCard )
                    if (uiStateStore.viewingCard && uiStateStore.viewingCard.id === card.id)
                        uiStateStore.setViewingCard(card)
                    else if (uiStateStore.editingCard && uiStateStore.editingCard.id === card.id)
                        uiStateStore.setEditingCard(card)
                    resolve (card)
                })
            })
        })
    }

    // using callback to indicate when entire process is complete, because there are 3 possible levels of chained promises.
    // works, but currently unused.
    @action autoTagCardsFromSources(cards, assets, onComplete) {
        let source, userTag, cardableTag, newTagCount = 0, matchingAsset, apiCalls = []
        const projectId = projectStore.currentProject.id
        const finalCardId = cards[ cards.length - 1].card_id
        cards.forEach( card => {
            matchingAsset = assets? assets.find( asset => asset.title === card.card_title && asset.url === card.card_url && asset.description === card.card_description ) : null
            source = matchingAsset? matchingAsset.source : getHostname(card.card_url)

            cardableTag = this.currentTagSet? this.currentTagSet.find( tag => tag.name === source) : null
            userTag = this.userTags.find( tag => tag.name === source)

            // project has this tag. Assign it.
            if (cardableTag) {
                TagApiService.tagCard (cardableTag.id, card.card_id).then (response => {
                    if (response.data.card_id === finalCardId)
                        onComplete ()
                })
            }

            // user has this tag, but not in project. Add to project, then assign to card.
            else if (userTag) {
                TagApiService.addTagToCardableSet(userTag.id, projectId).then( response => {
                    TagApiService.tagCard(userTag.id, card.card_id).then( response => {
                        if (response.data.card_id === finalCardId)
                            onComplete()
                    })
                })
            }

            // user nor project has this tag. Create, then assign.
            else if (!userTag) {
                this.createUserTag(source).then( newTag => {
                    TagApiService.addTagToCardableSet(newTag.id, projectId).then (cardableResponse => {
                        newTagCount++
                        TagApiService.tagCard(newTag.id, card.card_id).then( tagCardResponse => {
                            if (tagCardResponse.data.card_id === finalCardId)
                                onComplete()
                        })
                    })
                })
            }
        })
    }

    @action createDropboxRootTag() {
        return new Promise( (resolve, reject) => {
            const existingTag = this.getCardableTagByName('Dropbox')
            if (existingTag)
                resolve(existingTag)
            else {
                this.addTagToCardableSet(projectStore.currentProject.id, 'Dropbox', DROPBOX_COLOR).then( response => {
                    const dropboxTag = this.getCardableTagByName('Dropbox')
                    resolve( dropboxTag )
                })
            }
        })
    }

    @action
    createDefaultTags(projectId) {
        Object.keys( DefaultTags ).map( tagName => {
            this.addTagToCardableSet(projectId, tagName, DefaultTags[tagName])
        })
    }

    @action
    createDropboxFolderTags(cards, assets) {
        debugger
        let folderTag, tagName, color, matchingAsset

        this.createDropboxRootTag().then( rootTag => {

            cards.forEach (card => {
                this.tagCard(rootTag.id, card.card_id)

                matchingAsset = assets ? assets.find (asset => asset.title === card.card_title && asset.url === card.card_url) : null
                if (!matchingAsset)
                    debugger
                tagName = pathToTag (matchingAsset.parentFolder) || null
                if (!tagName)
                    return

                folderTag = this.getCardableTagByName (tagName)
                color = ColorUtils.getNextColor ()
                if (!folderTag) {
                    this.addTagToCardableSet (projectStore.currentProject.id, tagName, color).then (response => {
                        debugger
                        const newTag = this.getCardableTagByName (tagName)
                        this.tagCard (newTag.id, card.card_id)
                    })
                } else {
                    this.tagCard (folderTag.id, card.card_id)
                }
            })
        })
    }




    // no api
    getTagFromSet(tagId) {
        if (!this.currentTagSet)
            return null

        return this.currentTagSet.find( tag => tag.id === tagId)
    }

    getTagByName(tagName) {
        let tag
        if (this.currentTagSet)
            tag = this.currentTagSet.find( tag => tag.name === tagName)
        if (!tag && this.userTags)
            tag = this.userTags.find( tag => tag.name === tagName)
        return tag
    }

    getCardableTagByName(tagName) {
        return this.currentTagSet? this.currentTagSet.find( tag => tag.name === tagName) : null
    }

    // bulk no api
    tagIdsToTags(tagIds) {
        if (!this.currentTagSet || !tagIds)
            return []
        let tagObjs = []

        tagIds.forEach( tagId => tagObjs.push(this.getTagFromSet(tagId)) )
        return tagObjs
    }
}

export default new tagStore()
// export default crap

