import {action, extendObservable, observable} from 'mobx';

import cardStore from '/store/cardStore'
import projectStore from '/store/projectStore'
import userStore from '/store/userStore'
import uiStateStore from '/store/uiStateStore'
import integrationStore from '/store/IntegrationStore'

import EventApiService from "/services/EventApiService";
import ReactionApiService from "/services/ReactionApiService";
import EventModel from '/models/EventModel'
import {
    AddCardToProject,
    AddCommentToCard,
    AddCommentToProject,
    AddReactionToCard,
    CardChangeEvents,
    CardDelete,
    CardUpdate,
    CreateCard,
    getIcon,
    getMessage,
    isToasty,
    ProjectChangeEvents,
    ProjectUpdate,
    ProjectView
} from '/config/EventConfig'

import Toaster from '/services/Toaster'
import {
    ApprovalProjectCreate,
    BadgeWorthyEvents, CardMarkupAdd, CardMarkupDelete,
    CardSetEvents, FolioChanged,
    SendDirectMessage,
    UserInvite
} from "../config/EventConfig";
import {isBrowser} from "../utils/solo/isBrowser";
import {ProjectType} from "../config/Constants";
import swapFriends from "../utils/solo/swapFriends";
import {getUserApprovals} from "../services/ApprovalService";
import extractArrayFromTreeByKey from "../utils/solo/extractArrayFromTreeByKey";

let instance = null
export default class eventStore {
    initialized = false
    @observable outsideEvents = [] // new EventCollection()

    @observable rtEvents = {}               // realtime incoming, key: projectId, value: array of events
    @observable oldRtEventCounts = {}       // realtime incoming, used to enforce mobx. Delete if/when mobx works without ut.
    @observable unseenEventHistory = []     // events since our last check-in.
    @observable unseenEventHistoryNew = {}  // events since last project read. key: projectId, value: last viewed, events.
    @observable unseenEventCounts = {}
    @observable allEventHistory = {}        // past events, persistent
    @observable newEventTotal = 0           // single count of all unseen and rt events
    @observable unreadMsgCounts = {}        // key: contactId, value: count
    @observable unreadPConvoCounts = {}     // key: projectId, value: count

    @observable currentBadges = {}          // original hierarchical tree of category > project > cards
    @observable cardBadgesFlat = {}         // flattened map of cards for fast lookup on dismissal


    @observable rand = Math.random()

    static getInstance(ssr) {
        // console.log('eventStore instance')
        if (!instance) {
            instance = new eventStore(ssr)
        }
        return instance
    }

    static exists() {
        return instance !== null
    }

    constructor(ssr) {
        console.log ('New Event Store instance. ')
    }

    // new, will eventually replace unseen events
    @action
    getUserBadges() {
        EventApiService.getUserBadges().then(response => {
            this.currentBadges = response.data.user_badges;
            this.cardBadgesFlat = extractArrayFromTreeByKey({key: 'cards', tree: this.currentBadges})
            console.warn(JSON.stringify(this.cardBadgesFlat))
        })
    }

    @action
    dismissUserBadges(eventIds, idType) {
        EventApiService.dismissUserBadges(eventIds, idType).then(response => {
            this.currentBadges = response.data.user_badges;
            this.cardBadgesFlat = extractArrayFromTreeByKey({key: 'cards', tree: this.currentBadges})
        })
    }

    @action
    fetchProjectHistory(projectId, sinceDate) {
        return new Promise( (resolve, reject) => {
            EventApiService.getEventHistory(projectId, sinceDate).then(response => {
                if (response.data && response.data.events) {

                    const events = response.data.events.map(apiEvent => {
                        return new EventModel( apiEvent )
                    })
                    this.allEventHistory[projectId] = events
                    console.log(events.length+ " events in history.")

                    // populate with info avail on client. no api calls.
                    console.log('FetchHistory: Hydrating events for project ' +projectId)
                    events.forEach( event => {
                        this.hydrateEvent(event)
                    })

                    resolve(this.allEventHistory[projectId])
                }
            }).catch(error => reject(error))
        })
    }

    // until if/when unseen_events gives us a summary, we'll DIY it here.
    @action setUnseenPConvoCounts() {
        let events, convoCount
        Object.keys(this.unseenEventHistoryNew).forEach( projectId => {
            events = this.unseenEventHistoryNew[projectId]? this.unseenEventHistoryNew[projectId].events : [] // TODO no longer possible to have uninitialized projectId pair right?
            convoCount = (events.filter (event => event.type === AddCommentToProject) || []).length
            if (convoCount > 0)
                this.unreadPConvoCounts[projectId] = convoCount
        })
    }

    @action
    observeUserProjects(userProjects) {
        if (!isBrowser())
            throw new Error("Attempting to observe all projects from ssr. Stop it.")
        if (!userProjects) {
            console.error ('we received no userProjects: ', userProjects)
            return
        }
        userProjects.map( project => {
            extendObservable( this.rtEvents, {
                [project.id] : observable([])
            })
            extendObservable( this.oldRtEventCounts, {
                [project.id] : 0
            })
        })
        this.initialized = true
    }

    // TODO if this is effective, refactor above to call it in loop
    @action
    observeProject(project) {
        if (!isBrowser())
            throw new Error("Attempting to observe a project from ssr. Stop it.")
            extendObservable( this.rtEvents, {
                [project.id] : observable([])
            })
            extendObservable( this.oldRtEventCounts, {
                [project.id] : 0
            })
    }

    /**
     * Convenience method to dismiss appropriate events for a top-level project read.
     * new cards are considered viewed. - TODO only cards in viewport?
     * reactions are considered viewed - TODO only on cards in viewport?
     * comments are not viewed.
     * @param projectId
     */
    @action
    markProjectAsViewed(projectId) {
        return
        let rtEvents = (this.rtEvents && this.rtEvents[projectId])?
            this.rtEvents[projectId].filter(event => [AddCardToProject, AddReactionToCard, ProjectUpdate, CardUpdate, CardDelete].indexOf(event.type) !== -1 ) : []
        let oldEvents = this.unseenEventHistory?
            this.unseenEventHistory.filter( event => event.projectId === projectId && [AddCardToProject, AddReactionToCard, ProjectUpdate, CardUpdate, CardDelete].indexOf(event.type) !== -1 ) : []
        let eventIds
        if (rtEvents)
            eventIds = rtEvents.map( event => event.id )
        if (oldEvents)
            eventIds = eventIds.concat( oldEvents.map( event => event.id ))

        if (eventIds && eventIds.length > 0) {
            // TODO RE-ENABLE! this.dismissEvents (eventIds, projectId)
        }

    }

    // RT Realtime Events. These are incoming events in realtime while app is running, requiring affected clients to be updated
    @action
    addEvent(apiEvent) {
        console.error('### EVENT')
        const activeUser = userStore.currentUser
        let event = new EventModel(apiEvent)
        this.hydrateEvent(event)
        if (event.userBadges) {
            this.currentBadges = event.userBadges
            this.cardBadgesFlat = extractArrayFromTreeByKey({key: 'cards', tree: this.currentBadges})
        } else if (event.userBadgeMap) {
            this.currentBadges = event.userBadgeMap[activeUser.id]? event.userBadgeMap[activeUser.id].user_badges : null
            this.cardBadgesFlat = extractArrayFromTreeByKey({key: 'cards', tree: this.currentBadges})
        }

        // Friend swap for api swap on 4
        if (event.type === UserInvite) {
            swapFriends(event)
        }

        const currProject = projectStore.currentProject

        // 2. Display notification if appropriate
        if (isToasty(event)                                                                         // event is deemed toast worthy
            && (!userStore.currentUser || event.userId !== userStore.currentUser.id)                // and it came from someone else
            && (!currProject || currProject.id !== event.projectId) // TODO and we're outside of the project
            && (event.type !== ProjectView || (event.project && event.project.projectType === ProjectType.APPROVAL)) // Only report on approval views
            && (event.type !== AddCommentToProject || (event.detail !== '[markup]'))            // ignore drawings
            && (!uiStateStore.isAPStarterRequest || (event.type !== ApprovalProjectCreate && event.type !== AddCommentToProject))) // and this isn't a starter request that may never be sent
        {
            console.log('Toasty. current project: ' +(currProject? currProject.id:'none')+ ', event project: ' +event.projectId)
            const msg = getMessage (event, 1)
            const icon = getIcon (event, 1)
            Toaster.info (msg, event, 1, icon)
            uiStateStore.setFaviconAlert(true)
            // uiStateStore.showFavAlert = true
        } else {
            const reason = !userStore.currentUser? 'no current user' : event.userId === userStore.currentUser.id? "user's own event" : "other reason"
            console.log('No Toast: ' +reason)
        }

        // Update project folio
        if (event.type === UserInvite || event.type === FolioChanged) {
            // TODO is it worth fetching project to find out if its an approval? Meanwhile, just update both projects and approvals.
            projectStore.getUserProjects()
            getUserApprovals()
        }


        // Update message counts
        if (event.type === SendDirectMessage) {
            const senderId = event.userId // for clarity
            if (senderId !== userStore.currentUser.id)
                this.addToMsgCount(senderId, 1)
            this.propagateEvent(event)
            return
        }

        else if (event.type === AddCommentToProject) {
            const senderId = event.userId // for clarity
            if (senderId !== userStore.currentUser.id)
                this.addToPConvoCount(event.projectId, 1)
            this.propagateEvent(event)
            return
        }

        // Update event counts
        const newCount = this.oldRtEventCounts[event.projectId] + 1
        this.oldRtEventCounts[event.projectId] = newCount
        // TODO Note 1: Should we be updating unseen count when this is RT by def, and breakdown providers will sum rt with unseen?
        this.unseenEventCounts[event.projectId] += 1
        console.log('%c addEvent. count = ' +this.oldRtEventCounts[event.projectId] )

        // Update event maps
        this.rtEvents[event.projectId] && this.rtEvents[event.projectId].push( event )
        // TODO Note 1b: Should we be updating unseen map when this is RT by def, and breakdown providers will combine rt with unseen?
        if (this.unseenEventHistoryNew[event.projectId] && this.unseenEventHistoryNew[event.projectId].events)
            this.unseenEventHistoryNew[event.projectId].events.push( event)
        console.warn('%c addEvent: Added: ' +event.toString(), 'color: purple' )
        this.propagateEvent(event)

    }

/*
    // singular type
    getProjectEventsOfType(projectId, eventType) {
        return this.rtEvents[projectId].filter(event => event.type === eventType ) || []
    }

    // plural types
    getProjectEventsOfTypes(projectId, eventTypes) {
        return this.rtEvents[projectId].filter(event => eventTypes.includes(event.type) ) || []
    }
*/

// TODO handle markup comment events, e.g. type = 11, detail = [markup]
    propagateEvent(event) {
        console.error('### EVENT')
        console.log('Propagate event: ', event.userId, event.projectId)
        const currProject = projectStore.currentProject
        const me = userStore.currentUser

        // Direct messaging event
        if (event.type === SendDirectMessage) {
            uiStateStore.refreshDM (event.userId, event.detail) // userId sent msg to friendId, which is us.
            if (me && event.userId !== me.id)
                uiStateStore.playSound ()

        // Project discussion
        } else if (event.type === AddCommentToProject) {
            // if in current project, refresh convo. OR could remove else and move this below.
            if (currProject && currProject.id === event.projectId)
                projectStore.getCurrentProjectConvo(event.projectId)
            if (me && event.userId !== me.id)
                uiStateStore.playSound ()

        // user event. Refresh integrations. NO. This would prevent new event types, and dropbox is currently off.
/*
        } else if (event.userId && !event.friendId && !event.projectId) {
            integrationStore.init(userStore.currentUser.id, '', true)
*/

        // Event affects a card currently open in large viewer or editor
        // event doesn't contain projectId, so we can't check whether it's the open project. Just check first.
        } else if (uiStateStore.viewingCard && uiStateStore.viewingCard.id === event.cardId) {
            if (event.type === CardMarkupAdd || event.type === CardMarkupDelete)
                cardStore.getCardById(event.cardId).then( card => {
                    uiStateStore.setViewingCard(card)
                })
        } else if (uiStateStore.editingCard && uiStateStore.editingCard.id === event.cardId) {
            if (event.type === CardMarkupAdd || event.type === CardMarkupDelete)
                cardStore.getCardById (event.cardId).then (card => {
                    uiStateStore.setEditingCard (card)
                })

        // Event in the current project
        } else if (currProject && currProject.id === event.projectId) {

            // Event affects project itself
            if (ProjectChangeEvents.indexOf( event.type ) >= 0) {
                projectStore.fetchCurrentProject(event.projectId, null, false)

            // Event adds or removes cards from the project collection
            } else if (CardSetEvents.indexOf( event.type ) >= 0) {
                cardStore.modifyCurrentCards( event )

                // Event affects an existing card in the project
            } else if (CardChangeEvents.indexOf( event.type ) >= 0) {
                if (event.type === CardUpdate && !!event.detail)
                    cardStore.refreshCard(event.cardId, event )

            }

        // Other project event
        } else if (uiStateStore.currentPage === uiStateStore.PP) {
            projectStore.getUserProjects (userStore.currentUser.id, false)
        }


    }

    @action hydrateEvent(event) {
        if (!event)
            debugger

        let apiCalls = []

        // 1. info available from client
        if (event.projectId && !event.project) {
            event.project = projectStore.getProjectFromUserProjects (event.projectId)
            // console.log('HydrateEvent: projectId = ' +event.projectId+ '. ' +(event.project? 'Found ' +event.project.title : 'Not found in user projects.') )
            // if (!event.project)
            //     debugger
        }
        if (event.reactionTypeId && !event.reaction) {
            event.reaction = ReactionApiService.getReactionByTypeId (event.reactionTypeId)
            if (!event.reaction)
                console.warn ('Attempt to get reaction by type ' + event.reactionTypeId + ' failed.')
        }
        if (event.userId && !event.user) {
            event.user = userStore.getUserFromConnections(event.userId)
        }
        if (event.cardId && !event.card) {
            event.card = cardStore.getCardFromCurrent (event.cardId) || null  // Project may not be current. TODO fetch?
            // if (!event.card)
            //     debugger
        }

        // debugging
        // if (event.type)


        // 2. info needed from api. no.
/*
        return new Promise( (resolve, reject) => {

            if (event.cardId) {
                apiCalls.push( cardStore.getCardById(event.cardId) )
            }

            if (event.commentId) {
                apiCalls.push( CommentApiService.getCommentById (event.commentId) )
            }

            Promise.all( apiCalls ).then( responses => {
                if (event.cardId)
                    event.card = responses.find( r => r.id === event.cardId)
                else if (event.commentId) {
                    event.comment = responses.find (r => r.comment_id === event.commentId)
                }
                resolve( event )
            }).catch( error => {
                if (error && error.response.status === 404) {
                    console.warn ('event hydrating: item not found. ')
                    resolve (event)
                }
                else {
                    reject(error)
                }
            })
        })
*/

        return event
    }

    /**
     * The following methods return breakdowns of events organized by type.
     * counts vs actual events
     */
    getEventBreakdown(projectId, includeMe) {
        if (!projectId)
            throw new Error('missing projectId')


        // 1. combine unseen and new events
        const ueInfo = this.unseenEventHistoryNew[projectId]
        const unseenEvents = ueInfo? ueInfo.events : []
        const rtEvents = this.rtEvents[projectId] || []
        const events = unseenEvents.concat(rtEvents)

        // const dmEvents = events.filter (event => (event.type === DirectMessage) && !isMe(event.userId, includeMe)) || []
        const cardAddEvents = events.filter (event => (event.type === AddCardToProject || event.type === CreateCard) && !isMe(event.userId, includeMe)) || []
        const cardUpdateEvents = events.filter (event => event.type === CardUpdate && !isMe(event.userId, includeMe)) || []
        const projectUpdateEvents = events.filter (event => event.type === ProjectUpdate && !isMe(event.userId, includeMe)) || []
        const cardCommentEvents = events.filter (event => event.type === AddCommentToCard && !isMe(event.userId, includeMe)) || []
        const projectCommentEvents = events.filter (event => event.type === AddCommentToProject && !isMe(event.userId, includeMe)) || []
        const reactionEvents = events.filter (event => event.type === AddReactionToCard && !isMe(event.userId, includeMe)) || []
        const views = events.filter (event => event.type === ProjectView && !isMe(event.userId, includeMe)) || []

        return {
            views: views,
            cardAddEvents: cardAddEvents,
            cardCommentEvents: cardCommentEvents,
            projectCommentEvents: projectCommentEvents,
            reactionEvents: reactionEvents,
            cardUpdateEvents: cardUpdateEvents,
            projectUpdateEvents: projectUpdateEvents,
            lastViewed: ueInfo? ueInfo.lastViewed : null,
            includesRT: rtEvents.length > 0
        }
    }

    countEventBreakdown(projectId) {
        const { views, cardAddEvents, cardCommentEvents, projectCommentEvents, reactionEvents, cardUpdateEvents,
                projectUpdateEvents, lastViewed, includesRT }
            = this.getEventBreakdown(projectId)
        const sum = views.length + cardAddEvents.length + cardUpdateEvents.length + cardCommentEvents.length
            + projectCommentEvents.length + projectUpdateEvents.length + reactionEvents.length

        return {
            totalUnseen: sum,
            totalViews: views.length,
            totalCardAddEvents: cardAddEvents.length,
            totalCardUpdateEvents: cardUpdateEvents.length,
            totalCardCommentEvents: cardCommentEvents.length,
            totalProjectCommentEvents: projectCommentEvents.length,
            totalReactionEvents: reactionEvents.length,
            projectUpdateEvents: projectUpdateEvents.length,
            lastViewed: lastViewed,
            includesRT: includesRT
        }
    }

    countEventsOfType(projectId, eventType, includeMe) {
        let events = this.getEventBreakdown(projectId)
        if (!includeMe) {
            const me = userStore.currentUser.id
            events = events.filter( ev => ev.userId !== me)
        }
        return events[eventType].length
    }

    getCardBreakdowns(projectId, includeSelf) {
        if (!projectId)
            throw new Error('missing projectId')
        // if (!this.initialized)   we've fetched unseen events on serverside. Rt events will arrive in rt.
        //     throw new Error('not initialized')

        // combine events
        const ueInfo = this.unseenEventHistoryNew[projectId]
        const unseenEvents = ueInfo? ueInfo.events : []
        const rtEvents = this.rtEvents[projectId] || []
        const events = unseenEvents.concat(rtEvents.slice())
        if (!events || events.length === 0)
            return {}

        // get affected card ids
        const cardIds = events.map( event => event.cardId !== undefined? event.cardId : 0 )
        const uniqueCardIds = [...new Set(cardIds)]

        // popuplate breakdowns
        let cardMap = {}
        uniqueCardIds.forEach( cardId => {
            if (cardId !== 0)
                cardMap[cardId] = this.getCardBreakdown( cardId, events.filter( event => event.cardId === cardId) || [], includeSelf)
        })
        return cardMap
    }

    getCardBreakdown(cardId, cardEvents, includeMe) {
        if (!cardId)
            throw new Error('missing cardId')

        const isNew = cardEvents.some (event => (event.type === AddCardToProject || event.type === CreateCard) && !isMe(event.userId, includeMe) )
        const commentEvents = cardEvents.filter (event => event.type === AddCommentToCard && !isMe(event.userId, includeMe)) || []
        const reactionEvents = cardEvents.filter (event => event.type === AddReactionToCard && !isMe(event.userId, includeMe)) || []
        const cardUpdateEvents = cardEvents.filter (event => event.type === CardUpdate && !isMe(event.userId, includeMe)) || []

        return {
            isNew: isNew,
            commentEvents: commentEvents,
            reactionEvents: reactionEvents,
            updateEvents: cardUpdateEvents
        }
    }

    countCardBreakdown(cardId, cardEvents) {
        const {commentEvents, reactionEvents, updateEvents} = this.getCardBreakdown (cardId, cardEvents)

        return {
            commentEvents: commentEvents.length,
            reactionEvents: reactionEvents.length,
            updateEvents: updateEvents.length
        }
    }

    @action initMsgCounts() {
        userStore.currentConnections.forEach( friendo => {
            if (this.unreadMsgCounts[friendo.id] === undefined) {
                extendObservable( this.unreadMsgCounts, {
                    [friendo.id] : 0
                })
            }
        })
        this.rand = Math.random()
    }

    @action initConvoCounts() {
        projectStore.userProjects.forEach( project => {
            if (this.unreadPConvoCounts[project.id] === undefined) {
                extendObservable( this.unreadPConvoCounts, {
                    [project.id] : 0
                })
            }
        })
        this.rand = Math.random()
    }

    @action addToMsgCount(contactId, n) {
        this.unreadMsgCounts[contactId] += n
    }

    @action markAsRead(contactId) {
        this.unreadMsgCounts[contactId] = 0
    }

    @action addToPConvoCount(projectId, n) {
        this.unreadPConvoCounts[projectId] += n
    }

    // UNUSED- confirm and delete
    countNewMsgs(contactId) {
        console.log('%c DMs. countNewMsgs for ' +contactId, 'color: lightblue')
        const rtDMEvents = this.outsideEvents.filter( ev => ev.type === SendDirectMessage && ev.friendId === contactId) || []
        const unreadMsgCount = this.unreadMsgCounts[contactId] || 0
        return rtDMEvents.length + unreadMsgCount
    }

    @action dismissProjectEventsByType(eventType, projectId) {
        if (!eventType || !projectId)
            throw new Error('missing params')

        const ueInfo = this.unseenEventHistoryNew[projectId]
        ueInfo.events = ueInfo.events.filter(event => event.type !== eventType)
        if (this.rtEvents[projectId])
            this.rtEvents[projectId] = this.rtEvents[projectId].filter(event => event.type !== eventType)
    }


    @action dismissAllInProject(projectId) {
    }

/*
    countRT = () => {
        let total = 0  // no
        console.log('typeof obj: ' +(typeof this.unseenEventCounts))
        Object.keys (this.unseenEventCounts).forEach( pId => total += this.unseenEventCounts[pId])
        /!*
            Object.keys (this.rtEvents).forEach( pId => {

                if (BadgeWorthyEvents.indexOf(event.type) !== -1 )
                total += this.rtEvents[pId])
        *!/
        return total
    }
*/

}

const isMe = (userId, ignoreMe) => {
    const selfId = userStore.currentUser? userStore.currentUser.id : null
    if (!selfId || !userId || ignoreMe)
        return false
    return userId === selfId
}

