import {action, computed, extendObservable, observable} from 'mobx';
import {Router} from '~/routes'
import USU from "../utils/UserSessionUtils";
import {isBrowser} from "../utils/solo/isBrowser";
import DirectMessage from "../components/Conversations/DirectMessage";
import FloatingPane from "../components/Conversations/FloatingPane";
import userStore from '/store/userStore'
import cardStore from '/store/cardStore'
import eventStore from '/store/eventStore'
import Sound from "react-sound";
import localforage from 'localforage'
import RU from "../utils/ResponsiveUtils";
import {MARK_AS_READ_TIMEOUT} from "../config/EventConfig";
import markAsViewed from "../utils/solo/markAsViewed";

// NonModals aka edit panels that appear in work pane of any page
export const EditProfile = 'editProfile'
export const UserSettings = 'userSettings'
export const ProjectSettings = 'projectSettings'
export const CardEditor = 'cardEditor'
export const CardViewer = 'cardViewer'
export const CardAttachments = 'cardAttachments'
export const ExportPreview = 'exportPreview'
export const ImportContent = 'importContent'
// Left
export const ManageContacts = 'manageContacts'
// Right
export const SendForApproval = 'sendForApproval'


const hasCount = /\([0-9]+\)$/
const startsWithBang = /^!+\s+/

/**
 * This store is for the current state of the UI itself - e.g. zoom level, selected cards, views, etc. NO APP DATA
 * Because of its purely UI scope, we can allow reactive components to update it directly
 */
class uiStateStore {
    defaultCardSize = 200
    defaultPreviewCardSize = 200
    @observable cardSizeMap = {
        main: 200,
        combinedSearchTab: 150,
        cardEditor: 100,
        cardsTab: 150,
        dropboxTab: 100

    }
    @observable counter = 0
    @observable rand = Math.random()
    @observable theme = 'dark'             // start with light/dark, add more later
    @observable tileHeight = 15
    @observable fileCardMap = {}
    @observable currentPage
    @observable floatCount = 0              // num of floating panes currently open. UNUSED?
    @observable focusedPane = null          // the floating pane with focus.
    @observable showDev = false             // dev info
    @observable showSidebar = false         // desktop
    @observable openMembers = false         // board members

    @observable showMobSidebarL = false      // mobile
    @observable showMobSidebarR = false    // mobile

    @observable arrowPriority = 'nav'       // nav, text - arrow keys may be used for navigation or text editing

    @observable lpTab = -1                  // active tab in left pane
    @observable minimizeLeftCol = false     // desktop
    @observable maximizeLeftCol = false     // desktop
    @observable enableFavAlert = false      // favIcon alert badge
    @observable pusherConnection = null     // to reuse connections and disconnect on close, to keep down usage quota
    @observable soundPlayStatus = Sound.status.STOPPED
    @observable enableFileDrops = true

    @observable tourInProgress = false

    // bulk ops
    @observable importingAssets = false
    @observable creatingProjects = false
    @observable deletingProjects = false
    @observable loadingNotes = false
    @observable exporting = false


    @observable voMode = false              // presentation view
    @observable openDMs = {}                // open Direct Message panes. Key: contactId. Value: {Pane, Convo}
    @observable embeddedDM = {convo: []}    // If profile page, the DM embedded in that user's profile. Only 1.

    // key aspects of discussion
    @observable showDiscussion = true            // project discussion
    // @observable discussPosition= 'right'
    @observable filterDiscussionByCard = false  // when true, show only comments relating to card this.viewingCard
    @observable expandDiscussionInput = false        // when true, discussion input expands to show attachments and options
    @observable discussionPlaceholder = null    // optional- helps indicate what item user is comment on
    @observable showDiscussionExtras = false    // extras: attachment selection popups

    // tagging users and cards in comments
    @observable showUserPopup = false
    @observable showCardPopup = false
    // @observable matchingCards = []

    // @observable selectedLPView = 'approvals'// selected Left Pane view: contacts, boards, both.
    @observable disableUPFetch= false       // suppress fetching user projects
    @observable unModal = null              // the currently open unmodal panel, or null if none.
    @observable leftUnModal = null          // the currently open left pane unmodal panel, or null if none.
    @observable rightUnModal = null         // the currently open right pane unmodal panel, or null if none.
    @observable exportPreviewUrl  = null
    previousSearch = ''                     // search tab opens with previous search
    @observable forcePopupOpen = false      // when opening a child component of a popup, use this to prevent it from closing and ruining the party

    // Approvals
    @observable currentApprovalRequest = null     // currently open request, if any. In rightUnmodal.
    @observable isAPStarterRequest = false        // is currently open request a starter project or real (sent) one?
    @observable approvalSent = false              // approval request is still open in right unmodal, with next actions, but we are done.
    @observable showJoinPrompt = false          // when true, shows a low friction 'tell us your name' prompt for approvals

    // shared dom element refs
    @observable cardsPane = null

    // card detail view state. was in cardStore, but never triggered. Seems fair to be here.
    @observable editingCard = null
    @observable editingCardIndex = null
    @observable viewingCard = null
    @observable viewingCardIndex = null
    @observable viewingAttmId = 0
    @observable isManualAttm = false        // card was attached manually by user, not auto by app.

    // card view options
    @observable flashCardId = null          // flash a card momentarily to draw attention
    @observable cardView = 'cards'
    @observable stickyInfo = true
    @observable cropImages = true
    @observable isFullViewOpen = false
    @observable showDescriptions = false
    @observable showAuthors = false
    @observable showActions = false
    @observable showExtensions = false
    @observable showStacks = false          // Cheat stacks view using tags, whilst awaiting backend "folder" solution
    @observable showTags = false
    @observable forceShowTags = false       // TODO possibly totally unnecessary and obsolete and should be set on fire
    // @observable showTagBar = false          // tag bar 2 words hence cap
    @observable showImportToolbar = false   // A toolbar of item types to import to a board
    @observable activeImportTool = null     // the currently selected tool in the aforementioned toolbar
    @observable showWhatsNew = false        // highlight all unseen changes
    @observable nWhatsNew = 0               // highlight all unseen changes - integer works where boolean fails

    // Discussion options
    @observable showMembers = true          // above project discussion

    // Scroll positions
    @observable cardsPaneSP = 0

    // Onboarding: Inline Hints
    @observable showInlineHints = USU.getSessionPref('showInlineHints') !== undefined? USU.getSessionPref('showInlineHints') : false
    @observable hintPlToolbar = USU.getSessionPref('hplt') !== undefined? USU.getSessionPref('hplt') : true
    @observable hintPdToolbar = USU.getSessionPref('hpdt') !== undefined? USU.getSessionPref('hpdt') : true
    @observable hintProjects = USU.getSessionPref('hps') !== undefined? USU.getSessionPref('hps') : true
    @observable hintProject = USU.getSessionPref('hp') !== undefined? USU.getSessionPref('hp') : true
    @observable hintLibrary = USU.getSessionPref('hlib') !== undefined? USU.getSessionPref('hlib') : true
    @observable hintCards = USU.getSessionPref('hcard') !== undefined? USU.getSessionPref('hcard') : true
    @observable hintContacts = USU.getSessionPref('hcon') !== undefined? USU.getSessionPref('hcon') : true
    @observable hintComments = USU.getSessionPref('hcom') !== undefined? USU.getSessionPref('hcom') : true

    // Painting Markup Annotations
    @observable paintEnable = false
    @observable paintVisible = true
    @observable paintTool = 'brush'
    @observable paintBrushWidth = 3
    @observable paintColor = 'red'
    @observable paintFontSize = '18px'
    @observable paintCardId = null
    @observable paintRef =  null
    @observable paintCount =  0

    PD = 'projectDetail'
    PS = 'projectSettingsWiz'
    AP = 'approvalPage'
    PP = 'projectsPage'
    PF = 'profilePage'
    LS = 'loginSignup'
    RP = 'resetPassword'
    C1 = 'customizeUser'
    AC = 'accountCode'

    constructor(rootStore) {
    }

    @action setJoinPromptState (state) {
        this.showJoinPrompt = state
    }

    @action setViewingCard( card, attmId ) {
        this.viewingCard = card
        this.viewingCardIndex = card? cardStore.currentCards.findIndex(c => c.id === card.id) : 0
        this.setViewingAttm(attmId)
    }

    @action setViewingAttm( attmId ) {
        this.viewingAttmId = attmId
        this.rand = Math.random()
    }

    @action setEditingCard( card ) {
        this.editingCard = card
        this.editingCardIndex = card? cardStore.currentCards.findIndex(c => c.id === card.id) : 0
    }

    @action updateEditingCard( propName, propValue) {
        if (this.editingCard && propName && propValue !== undefined) {
            this.editingCard[propName] = propValue
        }
    }

    getCardSize(container) {
        if (RU.device === 'mobile')
            return '96%'

        if (!container) {
            throw new Error ('getCardSize requires a container')
        }
        return this.cardSizeMap[container]
    }

    @action setCardSize(size, container) {
        if (!container) {
            debugger
            throw new Error ('setCardSize requires a container')
        }
        this.cardSizeMap[container] = size

        console.log("uiStateStore: setting card size to " +size+ " for " +container)
    }

    @action setTileHeight(height) {
        this.tileHeight = height
    }

    // responsive: multiple card sizes depending on device. No single default. Could hardcode here, or let's just find out.
    calcCardSize(container) {
        if (!isBrowser())
            return 0

        if (!container)
            container = 'main'

        // TODO REPLACE WITH REFS
        const pane = document? document.querySelector('.'+container) : null
        if (!pane)
            return 0

        const card = pane.querySelector('.LeCard')
        if (!card || !card.style || !card.style.width)
            return 0
        this.cardSizeMap[container] = card.style.width
        console.log('Calc actual card width: ' +card.style.width)
        return card.style.width

    }

    @action setCardView(view) {
        this.cardView = view;
    }

    @action setStickyInfo(sticky) {
        this.stickyInfo = sticky;
    }

    @action toggleStickyInfo() {
        this.stickyInfo = !this.stickyInfo
    }

    @action setCreatingProjects(creating) {
        this.creatingProjects = creating
    }

    setFaviconAlert(alert) {
        if (typeof document === 'undefined' || !this.enableFavAlert)
            return

        // title toggle bang
        let title = document.title
        const startIdx = startsWithBang.test(title)? 2 : 0 // title.lastIndexOf('!') : title.length
        title = title.substring(startIdx)
        if (alert)
            title = '! ' + title
        document.title = title
        const v = alert? 7:6

        // icon toggle badge
        const path = '/static/favicon' +(alert? '/alert':'')
        let elem
        elem = document.querySelector('.fav32')
        if (elem)
            elem.setAttribute('href', (path+ '/favicon-32x32.png?v='+v))
        elem = document.querySelector('.fav16')
        if (elem)
            elem.setAttribute('href', (path+ '/favicon-16x16.png?v='+v))
        elem = document.querySelector('.fav180')
        if (elem)
            elem.setAttribute('href', (path+ '/apple-touch-icon.png?v='+v))
        elem = document.querySelector('.favishort')
        if (elem)
            elem.setAttribute('href', (path+ '/favicon.ico?v='+v))
    }

/*@action addSlider() {
        this.cardSizeSliderCount += 1
        this.cardSizes.push(200)
        return this.cardSizeSliderCount
    }

    @action removeSlider() {
        this.cardSizeSliderCount -= 1
        this.cardSizes.pop()
        return this.cardSizeSliderCount
    }
*/

    getProjectsView() {
        return 'grid'
/*
        if (localStorage)
            return localStorage.getItem('projectsView')
        else
            return 'grid'
*/
    }

    @action setProjectsView(view) {
/*
        if (localStorage)
            localStorage.setItem('projectsView', view)
*/
    }

    @action toggleSidebar() {
        this.showSidebar = !this.showSidebar
        console.log('show sidebar: ' +this.showSidebar)
        userStore.setUserPref(null, 'left_pane_open', this.showSidebar)
    }

    @action toggleMobSidebarL() {
        this.showMobSidebarL = !this.showMobSidebarL
        console.log('show sidebar: ' +this.showMobSidebarL)
        // userStore.setUserPref(null, 'mobile_sidebar_open', this.showMobSidebarL)

    }

    @action openDM(contactId, contact) { // for backward comp
        const evs = eventStore.getInstance()
        this.counter++ // test dummy
        const dmCount = Object.keys(this.openDMs).length
        if (!contact && contactId)
            contact = userStore.getUserFromConnections(contactId)
        if (!contact) {
            console.error('Contact ' +contactId+ ' not found.')
            return
        }

/*
        // update new user badges
        if (evs.currentBadges && evs.currentBadges.direct_messages) {
            const senders = evs.currentBadges.direct_messages.senders
            if (senders && senders[contactId] && senders[contactId].total > 0) {
                const eventIds = senders[contactId].events.map( event => event.event_id) || []
                if (eventIds.length > 0) {
                    setTimeout (() => {
                        evs.dismissUserBadges (eventIds)
                    }, MARK_AS_READ_TIMEOUT)
                }
            }
        }
*/
        markAsViewed(contactId, 'contact')

        if (this.openDMs[contactId]) {
            // focus this pane
        } else {
            // PusherService.getInstance().followUser(contactId) // start listening

            const dm = <DirectMessage contactId={contactId} isPF={false}/>
            const pane =
                <FloatingPane
                    component={dm}
                    index={dmCount}
                    title={contact.displayName || contact.email}
                    titleHint={'Visit ' +contact.displayName}
                    image={contact.image}
                    onTitleClick={() => this.gotoProfile(contactId)}
                    onFocus={() => evs.markAsRead(contactId)}
                    onClick={() => evs.markAsRead(contactId)}
                    onClose={() => this.closeDM(contactId)}
                />

            extendObservable( this.openDMs, {
                [contactId] : {pane: pane, convo: []}
            })
            this.focusedPane = pane
            this.rand = Math.random() // test dummy

            const activeUser = userStore.currentUser
            if (activeUser) {
                try {
                    localforage.setItem ('openDMs-' + activeUser.id, Object.keys (this.openDMs))
                } catch (error) {
                    debugger
                    console.log(error)
                }
            }

        }
    }

    // if contactId isn't currently open, open it. We have an incoming msg. \
    // Or update embedded DM.
    @action refreshDM(contactId, msg) {
        if (!contactId)
            throw new Error ('Attempted updateDM without contactId')

        const self = this
        const activeUser = userStore.currentUser
        const msgObj = {comment_creator: contactId, recipient_id: activeUser.id, comment_text: msg, time_modified: new Date().toLocaleTimeString()}
        const isContactEmbedded = !this.isDMOpen(contactId)
                                  && this.embeddedDM !== null
                                  && [contactId, activeUser.id].indexOf(this.embeddedDM.contactId) !== -1
                                  && this.embeddedDM.convo !== null

        // update DM on profile page if any. see below
/*
        if (isContactEmbedded) {
            this.embeddedDM.convo ? this.embeddedDM.convo.push (msgObj) : this.embeddedDM.convo = [msgObj]
        }
*/

        // create new open DM if not already embedded
        if (!this.openDMs[contactId] && contactId !== userStore.currentUser.id && !isContactEmbedded) {
            //throw new Error ('Attempted updateDM for unopen contactId ' + contactId)
            this.openDM (contactId)
            this.openDMs[contactId].convo = []
        }

        // Immediate display, to be followed by api fetch
        // appears circular, only because we want to assign directly to the convo field to trigger mobx
        let updatedConvo = null // isContactEmbedded? this.embeddedDM.convo : this.openDMs[contactId].convo
        if (isContactEmbedded && this.embeddedDM.convo) {
            this.embeddedDM.convo.push(msgObj)
        } else if (this.openDMs[contactId]) {
            this.openDMs[contactId].convo.push(msgObj)
        }
        // updatedConvo.push (msgObj)
        // isContactEmbedded? this.embeddedDM.convo = updatedConvo : this.openDMs[contactId].convo = updatedConvo
        this.rand = Math.random()

        return new Promise( (resolve, reject) => {
            userStore.fetchConvo(null, contactId).then( convo => {
                if (isContactEmbedded && this.embeddedDM.convo) {
                    self.embeddedDM.convo = convo
                } else if (this.openDMs[contactId]) {
                    self.openDMs[contactId].convo = convo
                }
                self.rand = Math.random()
                resolve(convo)
            }).catch( error => {
                reject (error)
            })
        })

    }

    @action addToDMConvo(contactId, msg) {
        if (!contactId)
            throw new Error ('Attempted addToDMConvo for bad contactId ' + contactId)

        const openDM = this.openDMs[contactId]
        if (openDM)
            openDM.convo.push(msg)

        const isContactEmbedded =
            !this.isDMOpen(contactId)
            && this.embeddedDM !== null
            && [contactId, userStore.currentUser.id].indexOf(this.embeddedDM.contactId) !== -1
            && this.embeddedDM.convo !== null

        if (isContactEmbedded)
            this.embeddedDM.convo.push(msg)
    }


    @action closeDM(contactId) {
        this.counter++ // test dummy
        if (!contactId || !this.openDMs[contactId])
            throw new Error ('Attempted closeDM for unopen contactId ' + contactId)

        delete this.openDMs[contactId]
        // PusherService.getInstance().unfollowUser(contactId) // stop listening
        this.rand = Math.random() // test dummy
        const activeUser = userStore.currentUser
        if (activeUser) {
            try {
                localforage.setItem('openDMs-'+activeUser.id, Object.keys(this.openDMs))
            } catch (error) {
                debugger
                console.log(error)
            }
        }

    }

    @action isDMOpen(contactId) {
        return contactId && this.openDMs[contactId] !== undefined
    }

    @computed get dmCount() {
        return Object.keys(this.openDMs).length
    }

    // return array of contactIds with open dms
    @action restoreDMState() {
        const activeUser = userStore.currentUser
        if (!activeUser)
            return

        return new Promise( (resolve, reject) => {
            try {
                localforage.getItem('openDMs-'+activeUser.id).then( lastOpenDMs => {
                    console.log("RESTORING DM STATE. OPENED DMS: " +lastOpenDMs)
                    if (!lastOpenDMs || lastOpenDMs.length === 0) {
                        resolve([])
                    } else {
                        console.log("RESTORING DM STATE. OPENED DMS: " +lastOpenDMs.length)
                        lastOpenDMs.forEach( contactId => {
                            console.log("RESTORING DM STATE. LOOP. Opening ContactId: " +contactId)
                            this.openDM(contactId)
                        })
                        resolve(lastOpenDMs)
                    }
                }).catch (error => {
                    debugger
                    console.log(error)
                    resolve([])
                })
            } catch (error) {
                debugger
                console.log(error)
                resolve([])
            }
        })
    }

    gotoProfile(contactId) {
        Router.pushRoute('profile', {id: contactId})
    }

    @action playSound() {
        this.soundPlayStatus = Sound.status.PLAYING
    }

}

export default new uiStateStore()


