import axios from 'axios'

import {action, observable} from 'mobx';

import {ApiTempKeyName, ApiTempKeyValue} from '~/config/ApiEndpoints'
import {systemStatus} from "~/config/ApiEndpoints";

export const NORMAL = 1
export const WARNING = 2
export const ERROR = 3

class SystemStore {
    @observable currentStatus = null
    poller = null

    constructor() {
        this.fetchSystemStatus()
        if (this.poller === null)
            this.pollSystemStatus()
    }

    @observable isStatusNormal() {
        return (this.currentStatus === null || this.currentStatus.status === NORMAL)
    }

    @observable getStatusType() {
        return this.currentStatus !== null? this.currentStatus.status : NORMAL
    }

    @action setCurrentStatus(status) {
        this.currentStatus = status
    }

    fetchSystemStatus() {
        return new Promise((resolve, reject) => {
            axios ({
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                url: systemStatus,
                data: {
                    [ApiTempKeyName]: ApiTempKeyValue,
                    "status_id": 0
                }
            }).then ( response => {
                this.setCurrentStatus( response.data )
                resolve( this.currentStatus )
            }).catch( error => {
                reject("Error getting system status: " +error)
            })
        })
    }

    pollSystemStatus() {
        let self = this
        if (self.poller === null)
        self.poller = setInterval( function() {self.fetchSystemStatus()}, 1000 * 120 )
    }
}

export default new SystemStore( )