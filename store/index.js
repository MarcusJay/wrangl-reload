// import firebase from 'firebase'

// try {
//   firebase.initializeApp({
//     apiKey: 'AIzaSyCWTseJj_vykE2KCMPl_eAsOYtfIaMYAiA',
//     authDomain: 'nextjs-firebase.firebaseapp.com',
//     databaseURL: 'https://nextjs-firebase.firebaseio.com'
//   })
// } catch (err) {
//     // taken from https://github.com/now-examples/next-news/blob/master/lib/db.js
//   if (!/already exists/.test(err.message)) {
//     console.error('Firebase initialization error', err.stack)
//   }
// }

// export const auth = firebase.auth()
// export const db = firebase.database()

export { default as initRootStore} from './_initRootStore'

export { default as apiStore } from './apiStore'
export { default as userStore } from './userStore'
export { default as cardStore } from './cardStore'
export { default as projectStore } from './projectStore'
// export { default as getAuthStore } from './_auth-store'
// export { default as getHashStore } from './_hash-store'