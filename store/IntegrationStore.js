import {action, computed, observable} from 'mobx'
import DropboxService from "../integrations/DropboxService";
import UserApiService from '/services/UserApiService'
import LifoStack from "/utils/LifoStack"
import {EventSource} from "../config/EventConfig";


class IntegrationStore {
    @observable currentIntegrations = []

    @observable dropboxIntegration = null
    @observable dropboxCurrentFolder = null         // TODO replace with cache
    @observable dropboxCurrentFiles = []            // TODO replace with cache
    @observable dropboxNavStack = new LifoStack()
    @observable dropboxLoading = false
    @observable dropboxFileCache = {}


    @action
    init = (userId, root, force) => {
        if (this.isDropboxAuthorized() && !force) {
            console.log("integration init: already initialized.")
            return // already initialized.
        }

        this.dropboxLoading = true
        console.log("integration init: Starting...")
        UserApiService.getUserIntegrations(userId).then( response  => {
            if (response.data && response.data.service_integrations) {
                this.currentIntegrations = response.data.service_integrations
            }

            this.dropboxIntegration = this.getDropboxIntegration (userId)
            if (!this.dropboxIntegration)
                this.dropboxLoading = false
            else {
                console.log("Dropbox integration found.")
                if (!root)
                    root = '/'

                if (!this.dropboxFileCache[root]) {
                    console.log ('Dropbox init at ', root)
                    this.dropboxDrilldown (root)
                }
                else {
                    console.log ('Dropbox init: returning ' + this.dropboxFileCache[root].length + ' existing files in ' + root)
                    // return Promise.resolve (this.dropboxCurrentFiles)
                }
            }
        })
    }

    @action
    dropboxDrilldown(folderPath) {
        console.log('drillDown: requesting files in ' +folderPath)
        this.dropboxLoading = true

        this.dropboxListFilesInFolder(folderPath).then( files => {
            this.dropboxCurrentFiles = files

            // update navigation state // TODO factor this out for DRY
            const newCurrent = folderPath
            const newParent = this.dropboxCurrentFolder// previous current before updating state
            this.dropboxCurrentFolder = newCurrent
            if (newParent) // if root, parent is null.
                this.dropboxNavStack.push(newParent)

            this.dropboxLoading = false
            console.log('drillDown: done. New current folder: ' +newCurrent+ ', new parent: ' +newParent+ '. '
                        +this.dropboxCurrentFiles.length+ ' Files.')
        })
    }

    @action
    dropboxDrillup() {
        const parent = this.dropboxNavStack.pop()
        console.log('drillUp: popped folder ' +parent+ '. Requesting files in it.')

        this.dropboxLoading = true
        this.dropboxListFilesInFolder(parent).then( files => {
            this.dropboxCurrentFiles = files

            // update navigation state
            const newCurrent = parent
            const newParent = this.dropboxNavStack.peak()
            this.dropboxCurrentFolder = newCurrent

            this.dropboxLoading = false
            console.log('drillDown: done. New current folder: ' +newCurrent+ ', new parent: ' +newParent+ '. '
                +this.dropboxCurrentFiles.length+ ' Files.')
        })
    }

    @action
    dropboxListFilesInFolder(folderId) {
        return new Promise( (resolve, reject) => {
            if (this.dropboxFileCache[folderId] !== undefined) {
                console.log ('Dropbox Cache Hit: ' + this.dropboxFileCache[folderId].length + ' items')
                resolve(this.dropboxFileCache[folderId])
            } else {
                DropboxService.listFilesInFolder (folderId).then (response => {
                    const files = response.data.items
                    if (files && files.length > 0)
                        files.forEach( file => file.parentFolder = folderId )
                    this.dropboxFileCache[folderId] = files
/*
                    files.forEach( file => {
                        if (file.image !== undefined) {

                        }


                    })
*/
                    console.log ('Dropbox Dynamic Fetch: ' + response.data.total + ' items')
                    resolve (files)
                }).catch (error => {
                    reject (error)
                })
            }
        })
    }

    @computed get isRoot() {
        return this.dropboxNavStack.length === 0
    }

    // OAuth methods:

    isDropboxAuthorized() {
        // const dbxa = UserSessionUtils.getSessionPref(userStore.currentUser.id+'dbxa')
        return (!!this.dropboxIntegration)
    }

    getDropboxIntegration(userId) {
        if (!this.currentIntegrations)
            return null

        else {
            let dbxAccounts = this.currentIntegrations.filter (acct => acct.service === EventSource.Dropbox)
            if (!dbxAccounts)
                return null
            else if (dbxAccounts.length > 1)
                throw new Error("Multiple Dropbox accounts not supported yet. Disallow. ")
            else {
                return dbxAccounts[0]
            }
        }
    }

    @computed get dropboxToken() {
        return this.dropboxIntegration? this.dropboxIntegration.token : null
    }

    // User has granted us access to a Dropbox account.
    // 1. Delete existing integration
    // 2. Save this one.
    // 3. TBD endpoint to delete all of a type
    @action
    setDropboxAccessToken(userId, token) {
        if (!token)
            throw new Error('setDropboxAccessToken: called without token!')

        //
        // NOTE: We are in a separate session initiated by dropbox redirect. Integrations aren't populated. Must fetch.
        //
        UserApiService.getUserIntegrations(userId).then( response  => {
            let previousDropboxIntegration
            if (response.data && response.data.service_integrations) {
                this.currentIntegrations = response.data.service_integrations
                previousDropboxIntegration = this.getDropboxIntegration (userId)
            }

            if (!previousDropboxIntegration)
                UserApiService.addUserIntegration (userId, EventSource.Dropbox, token)

            else {
                UserApiService.deleteUserIntegration (userId, previousDropboxIntegration.integration_id).then (response => {
                    UserApiService.addUserIntegration (userId, EventSource.Dropbox, token)
                })
            }
        })
    }

    @action
    unauthorizeDropbox(userId) {
        if (!userId)
            userId = userStore.currentUser.id

        if (!this.dropboxIntegration)
            throw new Error('unauthorizeDropbox: dropbox is not authorized')

        UserApiService.deleteUserIntegration(userId, this.dropboxIntegration.integration_id)
        const index= this.currentIntegrations.findIndex( acct => acct.integration_id === this.dropboxIntegration.integration_id)
        this.currentIntegrations.splice( index, 1)
        this.dropboxIntegration = null

/*
        DropboxService.revokeToken(this.dropboxIntegration).then(response => {
            const index= this.currentIntegrations.findIndex( acct => acct.integration_id === this.dropboxIntegration.integration_id)
            this.currentIntegrations.splice( index, 1)
            this.dropboxIntegration = null
        }).catch( error => {
            console.log('Error revoking dropbox token: ' +error)
        })
*/
    }


}

export default new IntegrationStore()
// export default crap

function compareDates(date1, date2) {
    const d2 = new Date(date1).getTime()
    const d1 = new Date(date2).getTime()

    if (d1 < d2)
        return -1

    if (d1 > d2) {
        return 1
    }
    return 0
}