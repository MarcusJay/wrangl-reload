import {action, extendObservable, observable, reaction} from 'mobx';

// import other stores

const messages = [
    "nice to have you here",
    "i like you",
    "welcome ❤️",
    "let's drink a beer together mate!",
    "you look awesome today"
];


class Store {
    @observable helloMessage = messages[Math.floor(Math.random() * (messages.length - 1))]

    constructor(store) {
        extendObservable(this, {
            isServer: null,
           _store_desc: 'root store'
        })

        Object.assign(this, store);
    }

    // general setter
    @action setData = (key, data) => {
        if(typeof data === 'object') {
            this[key] = {...this[key], ...data}
        } else {
            this[key] = data
        }
    }

    @action setObj = (obj) => {
        Object.assign(this, obj);
    }


    @action start = () => {
        this.timer = setInterval(() => {
            this.helloMessage = messages[Math.floor(Math.random() * (messages.length - 1))]
        }, 7000);
    }

    @action stop = () => clearInterval(this.timer)


} let si = new Store()


// reaction example
reaction(
    () => si._store_desc,
    (_store_desc) => {
        console.log('_store_desc was changed', _store_desc)
    }
)


// export our singelton
export default si