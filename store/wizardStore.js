import {action, computed, extendObservable, observable} from 'mobx';
import {Router} from '~/routes'
import USU from "../utils/UserSessionUtils";
import {isBrowser} from "../utils/solo/isBrowser";
import DirectMessage from "../components/Conversations/DirectMessage";
import FloatingPane from "../components/Conversations/FloatingPane";
import userStore from '/store/userStore'
import cardStore from '/store/cardStore'
import eventStore from '/store/eventStore'
import Sound from "react-sound";
import localforage from 'localforage'
import RU from "../utils/ResponsiveUtils";
import {MARK_AS_READ_TIMEOUT} from "../config/EventConfig";
import markAsViewed from "../utils/solo/markAsViewed";

// NonModals aka edit panels that appear in work pane of any page
export const EditProfile = 'editProfile'
export const UserSettings = 'userSettings'
export const ProjectSettings = 'projectSettings'
export const CardEditor = 'cardEditor'
export const CardViewer = 'cardViewer'
export const CardAttachments = 'cardAttachments'
export const ExportPreview = 'exportPreview'
export const ImportContent = 'importContent'
// Left
export const ManageContacts = 'manageContacts'
// Right
export const SendForApproval = 'sendForApproval'


/**
 * This store is for the current state of the UI itself, specific to board and folder setup wizards.
 */
class wizardStore {
    @observable rand = Math.random()
    @observable bwCurrStep = 0


}

export default new wizardStore()


