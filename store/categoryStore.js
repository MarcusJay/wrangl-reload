import {action, observable} from 'mobx';

import projectStore from '/store/projectStore'

import CategoryModel from '/models/CategoryModel'
import {APPROVAL_CAT, DEFAULT_CAT, REQUEST_CAT} from "../config/Constants";
import {
    createCategory,
    deleteCategory,
    getUserCategories, moveCategory,
    moveProjectCategory,
    updateCategory,
} from "../services/CategoryApiService";

class categoryStore {
    @observable userCategories = []
    @observable currCatId = null

    getUserCategories(userId) {
        console.log('##### getUserCategories start')
        return new Promise((resolve, reject) => {
            getUserCategories (userId).then (response => {
                if (response.data && response.data.folio_structure) {
                    this.userCategories = response.data.folio_structure.map (apiCategory => new CategoryModel (apiCategory));
                    this.userCategories.forEach( cag => updateAPCagName(cag))
                    projectStore.userFolio = this.userCategories // TODO one and the same? 2 endpoints, 2 data sources. Eliminate one.
                    if (projectStore.currentProject)
                        this.currCatId = this.getProjectCategory(projectStore.currentProject.id)
                    else
                        this.currCatId = DEFAULT_CAT
                    console.log('##### getUserCategories num of categories looks like we have: ' +this.userCategories.length)
                    resolve (this.userCategories)
                } else
                    resolve( [] )
            }).catch( error => reject('Error getting user categories', error))
        })
    }

    @action createCategory(title) {
        return new Promise((resolve, reject) => {
            createCategory(title).then (response => {
                this.getUserCategories ().then (r => {
                    resolve (this.userCategories)
                })
            })
        })
    }

    @action updateCategory(cag) {
        return new Promise((resolve, reject) => {
            updateCategory(cag).then (response => {
                this.getUserCategories ().then (r => {
                    resolve (this.userCategories)
                })
            })
        })
    }

    @action deleteCategory(categoryId) {
        return new Promise((resolve, reject) => {
            deleteCategory(categoryId).then( response => {
                this.getUserCategories ().then (response => {
                    resolve (this.userCategories)
                })
            })
        })
    }

    // move without persisting, to support drag animation.
    @action hoverCategory = (dragIndex, hoverIndex) => {
        this.preDragCags = this.userCategories.slice()
        const dragCag = this.userCategories[dragIndex]
        this.userCategories.splice(dragIndex, 1)              // delete from current position
        this.userCategories.splice(hoverIndex, 0, dragCag)   // insert into new position
    }

    @action moveCategory(catId, newIndex) {
        return new Promise((resolve, reject) => {
            moveCategory(catId, newIndex).then( response => {
                this.getUserCategories ().then (categories => {
                    resolve (categories)
                })
            })
        })
    }


    @action moveProjectCategory(toCatId, projectId) {
        return new Promise((resolve, reject) => {
            moveProjectCategory(toCatId, projectId).then( response => {
                this.getUserCategories ().then (response => {
                    resolve (this.userCategories)
                })
            })
        })
    }

    assignProjectToCategory = this.moveProjectCategory


    // no api

    // project can only have 1 category (currently)
    getProjectCategory(projectId) {
        if (!projectId || !this.userCategories)
            return null

        return this.userCategories.find(
            cat => cat.projectIds
                && cat.projectIds.length > 0
                && cat.projectIds.findIndex( pId => pId === projectId) !== -1
        ) || null
    }

    getCurrentCategory() {
        if (!this.currCatId) {
            return null
        }

        return this.getCategoryFromUser(this.currCatId)
    }

    getCategoryFromUser(categoryId) {
        if (!this.userCategories)
            return null

        return this.userCategories.find( category => category.id === categoryId)
    }

    getCategoryByTitle(title) {
        if (!this.userCategories)
            return null

        return this.userCategories.find(category => category.title === title)
    }

    isProjectInCategory(projectId, catId) {
        const cat = this.userCategories.find( cat => cat.id === catId)
        if (!cat)
            return false
        return cat? cat.projects.indexOf(projectId) !== -1 : false
    }

    isProjectCategorized(projectId) {
        let cat
        for (let i=0; i<this.userCategories.length; i++) {
            if (this.userCategories[i] && this.userCategories[i].projects.indexOf (projectId) !== -1)
                return true
        }
        return false
    }

}

export default new categoryStore()

export const updateAPCagName = (apCag) => {
    if (apCag.id === APPROVAL_CAT)
        apCag.title = 'Received Requests'
    else if (apCag.id === REQUEST_CAT)
        apCag.title = 'Sent Requests'
}