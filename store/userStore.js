import {action, observable} from 'mobx';

import UserApiService from '/services/UserApiService'
import UserModel from '/models/UserModel'
// TODO Remove store when jsCookie is finished
import jsCookie from 'js-cookie'
import UserPrefsModel from "../models/UserPrefsModel";
import {createCommentApi, getConversationApi} from "../services/CommentApiService";
import uiStateStore from '/store/uiStateStore' // not thrilled with having userStore be aware of uiStateStore. Consider other options
import eventStore from '/store/eventStore'
import {hydrateConversation} from "../utils/solo/hydrateConvo";
import {getUserFromMembers} from "../models/ProjectModel";
import projectStore from "./projectStore";
import {compareDates} from "../utils/solo/compareDates";
import localforage from "localforage";
import {UserStatus} from "../config/Constants";
import {StartingContacts} from "../config/OnBoardingConfig";

const DEMO_USER_ID = "A5F6C7AD-D16F-363F-6D03-EF92F354FCCB" // michelle normal pic
const MICHELLE_2 = ""
export const CHRIS_USER_ID = "096EF93B-A87C-7BA4-6DCF-B5C047AC61D0" // chris
export const MJFB_USER_ID = "5E928686-81A4-395B-A0B4-2E4ED20C5CB2"
export const MJ7 = "B75416D0-8B81-985E-BDF9-6EB469AB60C8"

class userStore {
    @observable randomId = Math.random() * 4000
    @observable currentUser = null // clientside
    @observable currentFriend = null
    @observable selectedUserIds = []
    @observable userSessions = {} // serverside
    @observable currentCreator = null
    @observable userPrefs = null
    @observable userTags = []
    @observable currentConnections = []
    @observable filteredConnections = []
    @observable isFiltering = false
    @observable totalConnections = 0
    @observable currentConvo = []


    @observable currentMembers = []
    @observable superGroup
    @observable callHistory = {}

    constructor(rootStore) {
/*
        extendObservable(this, {
            currentUser: this.getDemoUser()
        });
*/
    }

    // Individual Users

    getUserById = (userId) => {
        if (!userId)
            debugger
        return new Promise ((resolve, reject) => {
            console.log("userStore.getUserById: getting user " +userId)
            UserApiService.getUserById (userId).then (response => {
                console.log("userStore.getUserById: Received user " +response.data.user_id)
                let user = new UserModel (response.data )
                console.log("userStore.getUserById: Resolving with user " +(user? user.displayName:'null'))
                resolve (user)
            }).catch( error => {
                if (error.response.status === 404)
                    resolve(null)
                else
                    reject(error)
            })
        })
    }

    @action setCurrentUser = (user, skipConnections) => {
        if (!user)
            debugger
        if (!user.hasOwnProperty('id')) {
            user = new UserModel( user )
        }

        this.currentUser = user
        if (!skipConnections)
            this.getUserConnections(user.id)

        // save to local Storage
        jsCookie.set('userId', user.id, { expires: 14 });

    }

    @action setCurrentUserProp = (propName, propValue) => {
        this.currentUser[propName] = propValue
    }

    @action logoutUser = () => {
        if (this.currentUser)
            console.log("%c## userStore.logoutUser: " +this.currentUser['id']+ ' ' +this.currentUser['user_id'], 'color: green' )
        this.currentUser = null
    }

    isCurrentUser = (userId) => {
        return (this.currentUser && this.currentUser.id === userId)
    }

    createPendingUser = ( email, name, pw ) => {
        UserApiService.verifyUserEmail(email).then( response => {
            if (response.data.user_exists !== true) {
                UserApiService.createUser (email, name, pw).then (response => {
                    if (response.data) {
                        UserApiService.connectUsers (this.currentUser.id, response.data.user_id) // resolve ignored
                    }
                })
            }
        })
    }

    @action saveUser = (user) => {
        return new Promise ((resolve, reject) => {
            UserApiService.updateUser(user).then( response => {
                const updatedUser = new UserModel( response.data )
                if (updatedUser.id === this.currentUser.id) {
                    this.getUserById(updatedUser.id).then( user => {  // don't rely on api response to update
                        resolve( updatedUser )
                    })
                }

            }).catch( error => {
                reject( error )
            })
        })
    }

    @action connectUsers(userId, contactId) {
        if (!userId)
            userId = this.currentUser.id
        UserApiService.connectUsers(userId, contactId).then( response => {
            this.getUserConnections(userId)
        })
    }

    @action disconnectUsers(userId, contactId) {
        if (!userId)
            userId = this.currentUser.id
        if (!contactId)
            throw new Error('Cannot disconnect from no one')
        UserApiService.disconnectUsers(userId, contactId).then( response => {
            this.currentConnections = this.currentConnections.filter( contact => contact.id !== contactId)
        })
    }

    @action toggleUserPin(contactId, pinned) {
        const userId = this.currentUser.id
        UserApiService.connectUsers(userId, contactId, pinned).then( response => {
            this.getUserConnections(userId)
        })
    }

    @action updateCurrentUserClientOnly = (key, value) => {
        this.currentUser[key] = value
    }

    createDemoUser = () => {
        let user = new UserModel()
        user.id = DEMO_USER_ID
        user.displayName = "Michelle"
        user.email= "mcrames@email.com"
        user.image = "https://momstamp-production.s3.amazonaws.com/uploads/production/user/picture/1813/profile_11015750_10153521730857112_8843940629600522193_n.jpg"
        return user
    }

    getUserFromConnections(userId, ignoreProject) {
        if (this.isCurrentUser(userId)) {
            return this.currentUser
        }

        if (!this.currentConnections || this.currentConnections.length === 0) {
            return null // this.currentUser
        }

        let user = this.currentConnections.find( user => user.id === userId) || null
        if (!user && !ignoreProject && projectStore.currentProject) // since project members may no longer be connected, check here if avail
            user = getUserFromMembers(userId, projectStore.currentProject)
        return user
    }

    filterConnections(query) {
        if (!this.currentUser || !this.currentConnections || !query || query.length === 0)
            return []
        query = query.toLowerCase()
        this.filteredConnections = this.currentConnections.filter( user =>
            (user.displayName && user.displayName.toLowerCase().startsWith(query)) ||
            (user.firstName && user.firstName.toLowerCase().startsWith(query)) ||
            (user.lastName && user.lastName.toLowerCase().startsWith(query))
        ) || []
        return this.filteredConnections
    }

    getUserFromFilteredConnections (userIndex) {
        const users = this.filteredConnections && this.filteredConnections.length > 0? this.filteredConnections : this.currentConnections
        return userIndex >= 0? users[userIndex] : null

    }

    @action getCachedUserConnections = function(userId) {
        if (!userId && this.currentUser)
            userId = this.currentUser.id

        const cacheKey = 'uc'+userId
        return new Promise((resolve, reject) => {
            localforage.getItem(cacheKey).then( connections => {
                if (connections !== null && Array.isArray(connections)) {
                    console.log ('))) Cache hit on user connections. Returning ' +connections.length+ ' projects.')
                    // Note: This is for quick display only. These are plain arrays now, not observables.
                    this.currentConnections = connections
                }
                resolve (connections || [])
            }).catch( error => {
                console.error(error)
                resolve([])
            })
        })
    }

    // action because it updates this.currentconnections.
    @action getUserConnections = (userId) => {
        if (!userId) {
            userId = this.currentUser.id
        }
        return new Promise((resolve, reject) => {
            UserApiService.getUserConnections(userId).then( response => {
                console.log("Got connections. Count: " +response.data.connections.length)

                // if (response.data.connections && response.data.connections.length > 0) {
                this.currentConnections = response.data.connections.map( apiUser => {return new UserModel( apiUser) })
                if (this.currentConnections.length > 0)
                    this.currentConnections.sort( (a,b) => { return a.displayName.toLowerCase().localeCompare(b.displayName.toLowerCase())} )
                this.filteredConnections = this.currentConnections
                this.totalConnections = response.data.member_total
                localforage.setItem('uc'+userId, this.currentConnections).then( response => {
                    console.log('))) Cached user connections: ' +this.totalConnections)
                })
                eventStore.getInstance().initMsgCounts()

                console.log("Resolving connections with " +response.data.connections.length+ " connections.")
                resolve(this.currentConnections)
            }).catch(error => {
                console.error("Error getting connections", error)
                reject(error)
            })
        })
    }

    @action setUserConnections = (users) => {
        this.currentConnections = users
        this.filteredConnections = users
        this.totalConnections = users? users.length : 0

    }

    @action
    getUserPrefs(userId) {
        const isAnon = !this.currentUser || this.currentUser.status === UserStatus.ANON

        if (!userId && this.currentUser) {
            userId = this.currentUser.id
        }
        return new Promise((resolve, reject) => {
            if (isAnon) {
                resolve (new UserPrefsModel ())
                return
            }

            UserApiService.getUserPrefs(userId).then( response => {
                this.userPrefs = new UserPrefsModel( response.data )
/*
                if (this.userPrefs.isNew()) {
                }
*/
                uiStateStore.showSidebar = this.userPrefs.leftPaneOpen
                uiStateStore.cropImages = this.userPrefs.cropImages // because @observable
                resolve(this.userPrefs)
            }).catch(error => {
                console.error("Error getting user prefs", error)
                reject(error)
            })
        })
    }

    @action
    setUserPref(userId, prefName, prefValue) {
        const isAnon = !this.currentUser || this.currentUser.status === UserStatus.ANON
        if (isAnon)
            return

        if (!userId) {
            userId = this.currentUser.id
        }
        const pref = {user_id: userId, [prefName]: prefValue}
        return new Promise((resolve, reject) => {
            UserApiService.setUserPrefs(pref).then( response => {
                this.userPrefs = new UserPrefsModel( response.data )
                resolve(this.userPrefs)
            }).catch(error => {
                console.error("Error setting user prefs", error)
                reject(error)
            })
        })
    }

    @action setUserPrefs(prefs) {
        const isAnon = !this.currentUser || this.currentUser.status === UserStatus.ANON
        if (isAnon)
            return

        if (prefs)
            this.userPrefs = prefs
    }

    @action
    getUserTags(userId) {
        if (!userId)
            userId = this.currentUser.id

        return new Promise((resolve, reject) => {
            UserApiService.getUserTags(userId).then( response => {

                if (response.data.tags) {
                    this.userTags = response.data.tags
                    resolve(this.userTags)
                } else {
                    resolve([])
                }
            }).catch(error => {
                console.error("Error getting user tags", error)
                reject(error)
            })
        })
    }

    @action saveUserTag(tag, oldTag) {
        return UserApiService.saveUserTag(this.currentUser.id, tag, oldTag)
    }

    @action deleteUserTag(tag) {
        return UserApiService.deleteUserTag(this.currentUser.id, tag)
    }

    @action hydrateNewUser(user) {
        if (!user)
            debugger
        if (!user.hasOwnProperty('id')) {
            user = new UserModel( user )
        }

        // starting contacts
        // Michelle only
        StartingContacts.forEach( contactId => UserApiService.connectUsers(user.id, contactId) )

        // projects
        // StartingProjects.forEach( projectId => projectStore.addUserToProject(projectId, user.id, user.id, EventSource.OnBoarding, UserRole.ACTIVE) )

        // cards
        // if (StartingCards.length > 0)
        //     StartingCards.forEach( cardId => CardApiService.cloneCard(cardId, user.id) )

        // tags

        // help/tours

    }

    /*
        Endpoints have different response ordering:
        -> userstore.fetchConvo calls conversation_read: ascending order
        projectStore.getCurrentProjectConvo calls project_comments_read_2: descending order
    */
    @action fetchConvo(userId, friendId, makeCurrent) {
        if (!userId)
            userId = this.currentUser.id
        return new Promise ((resolve, reject) => {
            // ASCENDING. Good to go.
            getConversationApi (userId, friendId).then (response => {
                // sort anyway
                response.data.comments.sort((c1,c2) => compareDates(c1.time_created,c2.time_created) )

                const convo = hydrateConversation(response.data.comments, (userId) => this.getUserFromConnections(userId), null)
                if (makeCurrent)
                    this.currentConvo = convo
                resolve (convo)
            })
        })
    }

    // ASCENDING. Push to end.
    @action addToConvo(text, userId, friendId, atCards, atUsers) {
        if (!userId)
            userId = this.currentUser.id
        return new Promise ((resolve, reject) => {
            createCommentApi (text, null, friendId, atCards, atUsers).then (response => {
                const msg = response.data.comments[0]
                msg.user = this.getUserFromConnections (msg.comment_creator)
                this.currentConvo.push (msg)
                resolve (this.currentConvo)
            })
        })
    }

    @action addToDM(text, friendId, atCards, atUsers) {
        return new Promise ((resolve, reject) => {
            createCommentApi (text, null, friendId, atCards, atUsers).then (response => {
                const msg = response.data.comments[0]
                msg.user = this.getUserFromConnections (msg.comment_creator)
                // This hands off to uiStateStore to push to DM convo.
                // uiStateStore because it handles multiple open DM panes. Better way?
                uiStateStore.addToDMConvo(friendId, msg)
                const openDM = uiStateStore.openDMs[friendId]
                if (openDM)
                    resolve (openDM.convo)
                else if (uiStateStore.embeddedDM)
                    resolve (uiStateStore.embeddedDM.convo)
            })
        })
    }

}

export default new userStore()


