import {action, computed, observable} from 'mobx';
import localforage from 'localforage'

import cardStore from '/store/cardStore'
import userStore from '/store/userStore'
import categoryStore from '/store/categoryStore'
import tagStore from '/store/tagStore'
import eventStore from '/store/eventStore'

import ProjectApiService from '/services/ProjectApiService'
import CommentApiService from '/services/CommentApiService'
import ProjectModel, {addMember, deleteMember} from '/models/ProjectModel'
import {ARCHIVED_CAT, NOTIF_ALL, ProjectType, UserRole} from "../config/Constants";
import {isBrowser} from "../utils/solo/isBrowser";
import CategoryModel from "../models/CategoryModel";
import uiStateStore from "./uiStateStore";
import {hydrateConversation} from "../utils/solo/hydrateConvo";
import {createCommentApi, deleteCommentApi, getProjectConvoApi} from "../services/CommentApiService";
import isTestEnv from "../utils/solo/isTestEnv";
import {updateAPCagName} from "./categoryStore";

class projectStore {
    @observable userProjects = []
    @observable approvalsSent = []
    @observable approvalsReceived = []
    @observable filteredAPSent = []
    @observable filteredAPReceived = []
    @observable userFolio = []
    @observable projectsInCommon = []
    @observable filteredProjects = []
    @observable isFiltering = false
    @observable currentProject      // currently OPEN in detail view.
    @observable currentProjectConvo = [] // sep endpoint from project read
    @observable settingsProject     // currently open in settings pane
    @observable importProject       // currently open in import content pane
    @observable selectedProjects    // currently selected in projects view. TODO unused?
    @observable totalProjects = 0
    fetchingCached = false
    fetching = false

    myId = Math.random()
    getMyId() {
        return this.myId
    }
/*
    constructor(rootStore) {
    }
*/
    @computed get userApprovals() {
        return this.approvalsReceived.concat(this.approvalsSent)
    }

    @action setApprovals(received, sent) {
        this.approvalsReceived = received
        this.approvalsSent = sent
    }

    @action setFilteredApprovals = (received, sent, forceFilter) => {
        this.filteredAPReceived = received
        this.filteredAPSent = sent
        this.isFiltering = (forceFilter || (received && received.length > 0) || (sent && sent.length > 0))
    }

    @action createProject = function() {
        return new Promise( (resolve, reject) => {
            ProjectApiService.createProject().then(response => {
                let apiProject = response.data
                let project = new ProjectModel (apiProject)
                // project.members = [userStore.currentUser]
                this.userProjects.unshift (project)
                localforage.setItem('upf'+userStore.currentUser.id, {projects: this.userProjects, folio: this.userFolio})
                // tagStore.createDefaultTags(project.id)
                eventStore.getInstance().observeProject(project)
                // PusherService.getInstance().followProject(project.id)
                console.log("Added project " +project.id+ " to userProjects. " +this.userProjects.length+ " total now.")
                resolve(project)
            }).catch(error => {
                console.error(error)
                reject(error)
            })
        })
    }

    @action createProjects = function(n,name) {
        return new Promise( (resolve, reject) => {
            ProjectApiService.createProjects(n,name).then(response => {
                this.getUserProjects().then( response => {
                    resolve ({projects: response.userProjects, folio: response.userFolio})
                })
            }).catch(error => {
                console.error(error)
                reject(error)
            })
        })
    }

    @action updateProject = function(project) {
        return new Promise( (resolve, reject) => {
            ProjectApiService.updateProject(project).then( response => {
                if (this.currentProject && this.currentProject.id === response.data.project_id) {
                    this.fetchCurrentProject(response.data.project_id, userStore.currentUser.id, false).then( cp => {

                        // update cache
                        const pInArray = this.userProjects.find( p => p.id === project.id)
                        if (pInArray && cp) {
                            Object.assign (pInArray, cp)
                        }
                        localforage.setItem('upf'+userStore.currentUser.id, {projects: this.userProjects, folio: this.userFolio}).then( r => {})
                        resolve( cp )
                    })
                } else {
                    resolve (project)
                }
            }).catch( error => {
                reject(error)
            })
        })
    }

    @action getProjectById = (projectId) => {
        return new Promise((resolve, reject) => {
            if (!projectId) {
                resolve(null)
                return
            }

            ProjectApiService.getProjectById (projectId, null, false).then (response => {
                console.log('@@@ %c getProjectById Returned. ', 'color: pink')
                const project = new ProjectModel (response.data)
                resolve( project )
            }).catch(error => {
                reject(error)
            })
        })
    }


    // TODO No longer modifies state. Replace with direct API calls.
    @action addUserToProject = function(projectId, userId, newMemberId, eventSource, userRole) {
        return this.addUsersToProject(projectId, userId, [newMemberId], eventSource, userRole)
    }

    // TODO No longer modifies state. Replace with direct API calls.
    @action addUsersToProject = function(projectId, userId, memberIds, eventSource, userRole) {
        return new Promise( (resolve, reject) => {
            ProjectApiService.addUsersToProject(projectId, userId, memberIds, eventSource, userRole).then( response => {
                // TODO wouldn't this only work if newMemberId is current userId?
                // PusherService.getInstance().followProject(projectId)
                resolve(response)
            }).catch( error => {
                reject(error)
            })
        })
    }


    @action removeUserFromProject = function(projectId, userId, memberId, isPermanent) {
        if (!projectId || !memberId)
            throw new Error("removeUserFromProject: missing project or user ID")
        return new Promise( (resolve, reject) => {
            const origCat = categoryStore.getProjectCategory(projectId)
            categoryStore.moveProjectCategory(ARCHIVED_CAT, projectId).then( response => {
                ProjectApiService.removeUserFromProject (projectId, userId, memberId, isPermanent).then (response => {
                    categoryStore.currCatId = origCat? origCat.id : ARCHIVED_CAT
                    resolve (response)
                }).catch (error => {
                    reject (error)
                })
            }).catch (error => {
                reject (error)
            })
        })
    }

    @action tagProject = function(projectId, tag) {
        return ProjectApiService.tagProject(projectId, tag)
    }

    // tags v2. tag obj must already exist.
/*
    @action tagProject2 = function(tagId, projectId) {
        return TagApiService.tagProject(tagId, projectId)
    }
*/

    @action saveTagToCardableTagSet = function(projectId, tag, oldTag) {
        return ProjectApiService.saveTagToCardableTagSet(projectId, tag, oldTag)
    }

    @action deleteTagFromCardableTagSet = function(projectId, tag) {
        return ProjectApiService.deleteTagFromCardableTagSet(projectId, tag)
    }

    @action selectProject = (project) => {
        this.selectedProjects.push( project )
    }


    // fetch project with Id from api, and make it the observable currentProject.
    @action fetchCurrentProject = (projectId, userId, logEvent) => {
        if (!projectId && this.currentProject !== null)
            projectId = this.currentProject.id // refresh current project

        let self = this
        if (!projectId || projectId.length < 10) {
            throw new Error('fetchCurrentProject: bad projectId ' +projectId)
        }

        return new Promise((resolve, reject) => {
            if (self.fetching)
                resolve(self.currentProject)

            ProjectApiService.getProjectById (projectId, userId, logEvent).then (response => {
                if (!response.data) {
                    reject('Project not found')
                }
                self.fetching = false
                console.log('@@@ %c getProjectById Returned. ', 'color: pink')
                self.currentProject = new ProjectModel (response.data)
                tagStore.currentTagSet = self.currentProject.tagSet2 // TODO why not model? because why model?
                cardStore.setCurrentCards( self.currentProject.cards )
                // uiStateStore.showTagBar = (tagStore.currentTagSet && tagStore.currentTagSet.length > 0)
                resolve(self.currentProject)
            }).catch(error => {
                let msg = 'Error from getProjectById: '+ error
                console.error(msg)
                reject(msg)
            })
        })
    }

    @action setCurrentProject = function(project) {
        this.currentProject = project
    }

    @action clearCurrentProject = function() {
        this.currentProject = null
        cardStore.setCurrentCards([])

    }

    /*
      Endpoints have different response ordering:
        userstore.fetchConvo calls conversation_read: ascending order
        -> projectStore.getCurrentProjectConvo calls project_comments_read_2: descending order
    */
    @action getCurrentProjectConvo = function(projectId) {
        if (!projectId && this.currentProject)
            projectId = this.currentProject.id

        getProjectConvoApi(projectId).then( response => {
            // DESCENDING. Must reverse.
            // response.data.comments.reverse()

            // Per Chris, sort anyway.
            // response.data.comments.sort((c1,c2) => compareDates(c1.time_created,c2.time_created) )

            action(this.currentProjectConvo = hydrateConversation(
                                            response.data.comments,
                                            (userId) => userStore.getUserFromConnections(userId),
                                            null))
        })
    }
    getConvo = this.getCurrentProjectConvo

    // friendId optional. If present, we're seeking common projects
    @action getCachedUserProjects = function(userId, friendId) {
        if (!userId && userStore.currentUser)
            userId = userStore.currentUser.id

        const cacheKey = friendId? 'cpf'+userId+friendId : 'upf'+userId
        return new Promise((resolve, reject) => {
            localforage.getItem(cacheKey).then( item => {
                if (item !== null && item.hasOwnProperty('projects') && item.projects !== {}) {
                    console.log ('))) Cache hit on user projects/folio. Returning ' +item.projects+ ' projects.')
                    // Note: This is for quick display only. These are plain arrays now, not observables.
                    if (friendId) {
                        this.projectsInCommon = item.projects
                    } else {
                        this.userProjects = item.projects
                        this.userFolio = item.folio
                    }
                    resolve (item)
                }
                else if (!friendId)
                    resolve({projects: [], folio: []})
            }).catch( error => {
                debugger
                console.error(error)
                if (!friendId)
                    resolve({projects: [], folio: []})
            })
        })
    }

    @action getUserProjects = function(userId, sortBy, ssr) {
        if (!userId && userStore.currentUser)
            userId = userStore.currentUser.id

        return new Promise((resolve, reject) => {
            ProjectApiService.getUserProjects (userId, sortBy).then (response => {
                if (!response.data) {
                    console.error ('### Projects request returned no data')
                    reject ('No projects data')
                }
                let projects = response.data.projects.map (apiProject => new ProjectModel (apiProject) )
                let folio = response.data.folio_structure? response.data.folio_structure.map (apiCat => new CategoryModel(apiCat)) : []

                if (folio && folio.length > 0 && folio[0].title === 'Recent Activity')
                    folio[0].title = 'All'

                console.log('GetUserProjects: Got ' +projects.length+ ' projects.')
                folio.forEach( cag => updateAPCagName(cag))
                categoryStore.userCategories = folio
                this.setActiveStatus(userId, projects)
                this.setUserProjects( projects, folio) // response.data.project_total, ssr )
                eventStore.getInstance().initConvoCounts()
                console.log("Resolving with " +this.userProjects.length+ " projects.")
                resolve ({projects: this.userProjects, folio: this.userFolio})
            }).catch (error => {
                console.error (error)
                reject (error)
            })
        })
    }

    @action setUserProjects = function(projects, folio, userId) {
        if (!userId && userStore.currentUser)
            userId = userStore.currentUser.id

        this.userProjects = projects || []
        this.totalProjects = projects.length
        this.filteredProjects = this.userProjects
        this.userFolio = folio || []
        if (!isTestEnv () && projects && projects.length > 0) {
            localforage.setItem ('upf' + userId, {
                projects: this.userProjects.peek (),
                folio: this.userFolio.peek ()
            }).then (response => {
                console.log ('))) Cached user projects: ' + this.userProjects.length + ', folio: ' + this.userFolio.length)
            })
        }

        if (isBrowser()) {
            // PusherService.getInstance ().followMyProjects ()
            eventStore.getInstance ().observeUserProjects (this.userProjects)
        }
    }

    @action setFilteredProjects = (projects, forceFilter) => {
        this.filteredProjects = projects
        this.isFiltering = (forceFilter || (projects && projects.length > 0))
    }

    @action stopFiltering = () => {
        this.isFiltering = false
    }

    // call whenever updating this.user or filtered projects.
    // Projects we've taken a break from are still in user projects.
    // Mark them active/inactive wrt current user so app components can choose to filter or not.
    // TODO issue with Profile page
    setActiveStatus( userId, projects ) {
        console.log('setActiveStatuys: userId = ' +userId)
        let userInProject
        projects.map( project => {
            if (!project) {
                debugger
                return
            }

            userInProject = project.members.find( member => member.id === userId) || null
            // console.log('setActiveStatuys: userInProject = ' +userInProject)

            if (!userInProject) {
                // TODO Idenfity and prevent this case
                console.warn('User not found in User\'s own projects')
                project.isActive = true
                project.userRole = UserRole.ACTIVE

            } else {
                project.isActive = [UserRole.INACTIVE_USER, UserRole.INACTIVE_ADMIN].indexOf (userInProject.role) === -1
                project.userRole = userInProject.role
            }
        })
    }

    getUserProjectsSorting = function(userId) {
        return new Promise((resolve, reject) => {
            ProjectApiService.getUserProjectsSorting(userId).then (response => {
                if (!response.data || !response.data.project_ids) {
                    reject('getUserProjectsSorting: returned no data.')
                }
                resolve (response.data.project_ids)
            }).catch (error => {
                console.error (error)
                reject (error)
            })
        })
    }

    @action updateUserProjectsSorting = function(projectIds) {
        return ProjectApiService.updateUserProjectsSorting(projectIds)
    }

    @action getUserProjectSettings = (userId, projectId) => {
        return new Promise((resolve, reject) => {
            ProjectApiService.getUserProjectSettings (userId, projectId).then (response => {

                let project
                if (this.currentProject && this.currentProject.id === projectId)
                    project = this.currentProject
                else
                    project = this.getProjectFromUserProjects (projectId)

                if (project) {
                    project.userRole = response.data.user_role
                    project.notifLevel = response.data.notification_preference
                }
                resolve( project )
            })
        })
    }

    @action setUserProjectSettings = (userId = userStore.currentUser.id, projectId, notifLevel, role) => {
        return new Promise((resolve, reject) => {
            if (!projectId && this.currentProject)
                projectId = this.currentProject.id
            let project = this.getProjectFromUserProjects(projectId) || this.currentProject
            if (project) {
                const userInProject = project.members.find (member => member.id === userId)
                const isMember = userInProject ? userInProject.role === UserRole.ACTIVE : false
                const isInvited = userInProject ? userInProject.role === UserRole.INVITED : false

                // Ignore repeat attempts to invite user
                if (role === UserRole.INVITED && (isMember || isInvited)) {
                    resolve (project)
                    return
                }
            }

            ProjectApiService.setUserProjectSettings(userId, projectId, notifLevel, role).then( response => {

                // Now update project so react components don't have to resync
                if (project && project.projectType !== ProjectType.APPROVAL) {
                    project.notifLevel = notifLevel
                    if (role === UserRole.DELETED) {
                        deleteMember (userId, project)
                    } else if ([UserRole.INVITED, UserRole.ACTIVE].indexOf (role) !== -1) {
                        const newMember = userStore.getUserFromConnections (userId)
                        if (newMember)
                            addMember (newMember, project)
                    }
                }
                resolve( project )
            })
        })
    }

    // convenience method plucked from plpeople
    setMembership(userId, projectId, newRole) {
        const project = projectId? this.getProjectFromUserProjects(projectId) : projectStore.currentProject
        if (!project) {
            console.error('setMembership: project not found')
            return
        }

        const notifLevel = (newRole === UserRole.INVITED)? NOTIF_ALL : null

        return this.setUserProjectSettings(userId, project.id, notifLevel, newRole)
    }

    @action
    getCommonProjects = function(userIds) {
        return new Promise((resolve, reject) => {
            ProjectApiService.getCommonProjects(userIds).then (response => {
                if (!response.data.project_ids) { // TODO ask api to return zero results in empty array of same name, rather than 'projects'
                    resolve ([])
                    return
                }

                this.projectsInCommon = []
                let project
                response.data.project_ids.forEach(projectId => {
                    project = this.getProjectFromUserProjects(projectId)
                        || this.approvalsSent.find (p => p.id === projectId)
                        || this.approvalsReceived.find (p => p.id === projectId)
                    if (project)
                        this.projectsInCommon.push(project)
                })

                if (userIds.length > 1) {
                    const friendId = userIds[1]
                    this.setActiveStatus(friendId, this.projectsInCommon)
                }

                resolve (this.projectsInCommon)
            }).catch (error => {
                console.error (error)
                reject (error)
            })
        })
    }

    countCommonProjects = function(userIds) {
        return new Promise((resolve, reject) => {
            ProjectApiService.countCommonProjects(userIds).then (response => {
                resolve (response.data.project_total)
            }).catch (error => {
                console.error (error)
                reject (error)
            })
        })
    }



    // get the projects for a different user
    getFriendProjects = function(friendId) {
        if (!friendId)
            throw new Error('getFriendProjects: missing friendId')

        return new Promise((resolve, reject) => {
            ProjectApiService.getUserProjects (friendId).then (response => {
                if (!response.data) {
                    console.error ('### Projects request returned no data')
                    reject ('No friend projects data')
                }
                let friendProjects = response.data.projects.map (apiProject => new ProjectModel (apiProject))
                resolve (friendProjects)
            }).catch (error => {
                console.error (error)
                reject (error)
            })
        })
    }

    // no api call
    getProjectFromUserProjects = (projectId) => {
        if (!this.userProjects || typeof this.userProjects.find !== 'function') {
            debugger
            return null
        }
        return this.userProjects? this.userProjects.find( project => project.id === projectId ) : null
    }

    @action deleteProject = function(project, ignoreCache) {
        if (!project) {
            console.error ("Missing project to delete")
            return
        }

        if (project.creatorId !== userStore.currentUser.id) {
            console.warn("deleteProject: non-creator user cannot delete project. Removing instead. ")
            return this.removeUserFromProject(project.id, null, userStore.currentUser.id, true)
        }

        // PusherService.getInstance().unfollowProject(project.id)

        // update cache
        if (!ignoreCache) {
            this.userProjects = this.userProjects.filter( p => p.id !== project.id)
            localforage.setItem('upf'+userStore.currentUser.id, {projects: this.userProjects, folio: this.userFolio}).then( r => {})
        }

        return ProjectApiService.deleteProject(project.id)
    }

    @action deleteProjects = function(projectIds) {
        if (!projectIds || projectIds.length === 0) {
            console.error ("Missing projects to delete")
            return
        }

        return new Promise((resolve, reject) => {
            let project, apiCalls = []
            projectIds.forEach (pId => {
                project = this.getProjectFromUserProjects (pId)
                if (project) {
                    apiCalls.push (this.deleteProject (project, true))
                    // PusherService.getInstance ().unfollowProject (pId)
                }
            })
            Promise.all (apiCalls).then (results => {

                // update cache
                // TODO FIX THIS
                // this.userProjects = this.userProjects.filter( p => projectIds.indexOf(p.id) === -1)
                // localforage.setItem('upf'+userStore.currentUser.id, {projects: this.userProjects, folio: this.userFolio}).then( r => {})

                resolve( results )
            })
        })

    }


    // filter by arbitrary user.
    @action filterProjectsByUserId = function(userId) {
        if (!this.userProjects)
            return null

        this.filteredProjects = this.userProjects.filter( project => {
            return project.members.find(member => {
                return member.id === userId
            })
        })
        return this.filteredProjects
    }

    @action resetFilters = function() {
        if (!this.userProjects)
            return null

        this.filteredProjects = this.userProjects
    }

    @action filterProjectsByQuery(query) {
        return new Promise((resolve, reject) => {
            ProjectApiService.findProjects (query).then (response => {
                const apiProjects = response.data.projects.filter (project => project.project_type === ProjectType.NORMAL) || []
                // const apiApprovals = response.data.projects.filter (project => project.project_type === ProjectType.APPROVAL) || []

                // TODO stop modeling projects and use raw api data, for optimized filtering for starters?
                const projectIds = apiProjects.map( project => project.project_id)
                let projects = projectIds.map( projectId => this.getProjectFromUserProjects(projectId) )
                projects = projects.filter( p => !!p)
                this.setActiveStatus(userStore.currentUser.id, projects)
                this.setFilteredProjects(projects, true)
                resolve(this.filteredProjects)
            }).catch(error => reject(error))
        })

    }

    // Endpoint is descending. We reverse on read. So push here.
    @action addToConvo(text, projectId, atCards, atUsers) {
        if (!projectId)
            projectId = this.currentProject.id
        return new Promise ((resolve, reject) => {
            createCommentApi (text, projectId, null, atCards, atUsers).then (response => {
                const msg = response.data.comments[0]
                msg.user = userStore.getUserFromConnections (msg.comment_creator)
                if (this.currentProject && projectId === this.currentProject.id)
                    this.currentProjectConvo.push (msg)
                resolve ({msg: msg, convo: this.currentProjectConvo})
            })
        })
    }

    @action deleteFromConvo(cmtId, convo, projectId) {
        if (!projectId)
            projectId = this.currentProject.id
        return new Promise ((resolve, reject) => {
            deleteCommentApi(cmtId).then (response => {
                const index = convo.findIndex( c => c.comment_id === cmtId)
                convo.splice(index,1)
/*
                if (this.currentProject && projectId === this.currentProject.id) {
                    this.currentProjectConvo.splice(index,1)
                }
*/
                resolve( convo )
            })
        })
    }

    isProjectInCategory(project, cat) {
        if (!project || !cat)
            return false

        return cat.projectIds.find(projectId => projectId === project.id) !== undefined
    }


}

export default new projectStore()
// export default crap

