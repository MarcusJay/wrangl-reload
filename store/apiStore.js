import {action, extendObservable, observable} from 'mobx';

class subStore {
    @observable dinner = 'is served'
    @observable callHistory = {}
    @observable wranglCollection = {}

    constructor(rootStore) {
        extendObservable(this, {
            store_desc: 'holds api responses',
            from : {}
        });
    }

/*
    axios({GET}).then( response => {
        subStore.wranglCollection = response.data.,whatever

    })
*/

    // general setter
    @action setData = (key, data) => {
        this[key] = {...this[key], ...data}
    }
}

// export default subStore
let si = new subStore()
export default si