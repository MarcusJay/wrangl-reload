import {observable} from 'mobx';
import {observer} from "mobx-react";

import userStore from './userStore'
import projectStore from './projectStore'

/**
 * Read-only Store, cannot change state. This is the only store that lower level components should import.
 */
@observer
class readOnlyStore {
    @observable currentProject
    @observable currentUser
    constructor() {
        this.currentUser = userStore.currentUser
        this.currentProject = projectStore.currentProject
    }
}

export default new readOnlyStore()


