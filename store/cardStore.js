import {action, computed, observable} from 'mobx';

import CardModel from '/models/CardModel'
import ReactionApiService from '/services/ReactionApiService'
import DropboxService, {currDropboxer} from '/integrations/DropboxService'
import {CardDelete} from '/config/EventConfig'
import normalizeUrl from 'normalize-url'

import projectStore from '/store/projectStore'
import tagStore from '/store/tagStore'
import {AddCardToProject, CreateCard, CreateCards, EventSource} from "../config/EventConfig";
import WebImportService from "../services/import/WebImportService";
import {getShortUrl} from "../utils/solo/getShortUrl";
import {getUrlComponents} from "../utils/solo/getUrlComponents";
import {truncate} from "../utils/solo/truncate";
import {sendImage} from "../services/ImageFileService";
import {
    attachFilesToCards,
    cloneCardsIntoProject,
    createCardsFromAssets,
    createCardsFromFiles,
    deleteCard,
    deleteCards,
    getCardDetails,
    getUserCards,
    moveCard,
    saveCard,
    tagCard
} from "../services/CardApiService";
import {createCommentApi, deleteCommentApi} from "../services/CommentApiService";

class cardStore {
    @observable currentCards = []
    @observable filteredCards = []
    preDragCards = null
    @observable isFiltering = false
    @observable selectedCardIds = []
    tmpSelectedCards = []
    @observable userCards = []
    reactionSets = []

    constructor() {
        ReactionApiService.getDefaultReactionSets().then( reactionSets => {
            console.log("Loaded default reaction sets: " +reactionSets)
            this.reactionSets = reactionSets;
        })
    }

    getCardById(cardId) {
        return new Promise((resolve, reject) => {
            getCardDetails (cardId).then (apiCard => {
                resolve (new CardModel (apiCard))
            }).catch (error => {
                reject (error)
            })
        })
    }

    getCardFromCurrent(cardId) {
        return this.currentCards? this.currentCards.find( card => card.id === cardId) : null
    }

    @action setCardInCurrent(card) {
        if (this.currentCards && card) {
            const idx = this.currentCards.findIndex (c => c.id === card.id)
            if (idx >= 0)
                this.currentCards[idx] = card
        }
    }

    getPreDragCard(position) {
        return this.preDragCards? this.preDragCards[position] : null
    }

    // TODO consider custom endpoint if this is too expensive
    // Who's using this? PDWToolbar Card Notes button must load all comments.
    getAllCardDetailsInCurrentProject() {
        return new Promise((resolve, reject) => { 
            let cardReads = []
            projectStore.currentProject.cards.forEach (card => cardReads.push (getCardDetails (card.id)))
            Promise.all (cardReads).then (apiCards => {
                projectStore.currentProject.cards = apiCards.map (apiCard => new CardModel (apiCard))
                this.setCurrentCards (projectStore.currentProject.cards)
                resolve (this.currentCards)
            }).catch (error => {
                reject ("GetAllCardDetails: error " + error)
            })
        })
    }

    @action getUserCards(userId, acceptCached, useCardModel) { // action because it modifies this.userCards
        return new Promise((resolve, reject) => {
            // Disable cache as it needs to be per user and cleared at approp times.
            // if (acceptCached && this.userCards.length > 0)
            //     resolve(this.userCards)

            getUserCards(userId).then( response => {
                this.userCards = useCardModel? response.data.cards.map (apiCard => new CardModel (apiCard)) : response.data.cards
                resolve (this.userCards)
            }).catch (error => {
                reject ("GetUserCards: error " + error)
            })
        })
    }

    @action setUserCards(cards) {
        this.userCards = cards
    }

    // create new or update existing card
    @action saveCard = (card) => {
        // card = restoreModel(card)
        return new Promise((resolve, reject) => {
            let parentProject = projectStore.currentProject
            saveCard(parentProject.id, card).then( response => {
                if (response.data) {

                    // Update project.cards collection without refetching everything
                    if (card.id === 'new') {
                        let newCard = new CardModel (response.data)

                        // prepend
/*
                        this.currentCards.unshift(newCard)
                        projectStore.currentProject.cards.unshift( newCard )
                        this.userCards.unshift(newCard)
*/
                        // append
                        this.currentCards.push(newCard)
                        projectStore.currentProject.cards.push( newCard )
                        this.userCards.push(newCard)

                        resolve(newCard)
                    } else {

                        // card_update doesn't return entire card. refetch
                        getCardDetails(card.id).then( apiCard => {
                            let editedCard = new CardModel (apiCard)

                            // TODO These are really project side effects. Move them into separate call!
                            let oldCardIndex = projectStore.currentProject.cards.findIndex( card => card.id === editedCard.id )
                            projectStore.currentProject.cards[oldCardIndex] = editedCard
                            this.setCurrentCards(projectStore.currentProject.cards)

                            resolve(editedCard)
                        })
                    }
                    // parentProject.cards.unshift( response.cards[0] )
                } else {
                    console.error("Error saving card " +card.id+ ": " +response)
                    reject(response)
                }
            }).catch (error => {
                console.error("Error saving card " +card.id+ ": " +error)
                reject("Error saving card " +card.id+ ": " +error)
            })
        })
    }

    @action selectCard = (cardId) => {
        // TODO check for uniqueness or make it a Set
        this.selectedCardIds.push( cardId )
    }

    @action deselectCard = (cardId) => {
        const idx = this.selectedCardIds.indexOf(cardId)
        if (idx >= 0)
            this.selectedCardIds.splice(idx, 1)
    }

    get selectedCards() {
        let cards = [], card
        this.selectedCardIds.forEach (cardId => {
            card = this.getCardFromCurrent(cardId)
            if (!card && this.tmpSelectedCards)
                card = this.tmpSelectedCards.find(c => c.id === cardId)
            cards.push(card)
        })
        return cards
    }

    set selectedCards(cards) {
        if (cards && cards.length > 0) {
            this.tmpSelectedCards = cards // TODO because cards are not in current card collection. Solution? Use cards rather than IDs throughout?
            this.selectedCardIds = cards.map (card => card.id)
        } else {
            this.selectedCardIds = []
        }
    }

    isCardSelected = (cardId) => {
        return this.selectedCardIds.length > 0 && cardId && this.selectedCardIds.indexOf(cardId) !== -1
    }

    // client-only simple field change, since direct edit in react component does not trigger mobx reaction
/*
    @action editCard = (propName, value) => {
        if (!this.editingCard)
            throw new Error("Card edit attempt with no actual editingCard")
        this.editingCard[propName] = value
    }
*/


    @action setCurrentCards = (cards) => {
        this.currentCards = cards
        this.filteredCards = cards

        // hydrate card tags. Tags brought back by request, but unsure if this will stick around. Api used to send objects.
        if (tagStore.currentTagSet && tagStore.currentTagSet.length > 0) {
            cards.forEach (card => {
                if (card.tags2.length > 0 && typeof card.tags2[0] === 'string') {
                    card.tags2 = card.tags2.map (tagId => tagStore.currentTagSet.find (tag => tag.id === tagId))
                }
            })
        }
    }

    // rt card add or delete event. Modify our collection without refresh so we can animate.
    @action modifyCurrentCards = (event) => {
        if (!event || (!event.cardId && !event.card)) {
            // throw new Error ('event is missing card')
            // no longer an error, as events like 33 CardsCreate provides a count, not card ids
        }

        const cardIndex = this.currentCards.findIndex(card => card.id === event.cardId)

        if (event.type === CardDelete) {
          if (cardIndex >= 0) {
            this.currentCards.splice (cardIndex, 1)
          }
          // else we are the user who deleted it.
        }

        // multiple cards: pipe-separated card ids are in event.detail
        else if (event.type === CreateCards) {
            const cardIds = event.detail? event.detail.split('|') : []
            if (cardIds.length > 0) {
                this.getCardsSynchronously(cardIds)
            }
        }

        // single card: event.cardId
        else if ([CreateCard, AddCardToProject].indexOf (event.type) !== -1) {
            this.getCardById (event.cardId).then (newCard => {
                if (cardIndex >=0 )
                    this.currentCards.splice(cardIndex, 1, newCard)
                else
                    this.currentCards.unshift (newCard)
            })
        }
        this.filteredCards = this.currentCards
    }

    async getCardsSynchronously (cardIds) {
        // for (let cardId of cardIds) { no because reverse. we could cardIds.reverse() but why do extra work.
        let cardId
        for (let i = cardIds.length-1; i >= 0; i--) { // reverse
            try {
                cardId = cardIds[i]
                if (cardId === "1" || cardId === 1) // ignore api glitch plz
                    continue
                const cardIndex = this.currentCards.findIndex (card => card.id === cardId)
                const newCard = await this.getCardById (cardId)
                if (cardIndex >= 0)
                    this.currentCards.splice (cardIndex, 1, newCard)
                else
                    this.currentCards.unshift (newCard)
            } catch (error) {
                console.error(error)
            }
        }
    }


/*
    @action appendToCurrentCards = (card) => {
        if (!card)
            throw new Error('missing card')
        debugger
        this.currentCards.push(card)
        this.filteredCards = this.currentCards
    }
*/

    @action setFilteredCards = (cards, forceFilter) => {
        this.filteredCards = cards
        this.isFiltering = (forceFilter || (cards && cards.length > 0))
    }

    @action stopFiltering = () => {
        this.isFiltering = false
    }

    // move without persisting, to support drag animation.
    @action hoverCard = (dragIndex, hoverIndex) => {
        if (this.isFiltering)
            return
        this.preDragCards = this.currentCards.slice()
        const dragCard = this.currentCards[dragIndex]
        this.currentCards.splice(dragIndex, 1)              // delete from current position
        this.currentCards.splice(hoverIndex, 0, dragCard)   // insert into new position
    }


    @action moveCard = (card, destIndex) => {
        return new Promise( (resolve, reject) => {
            moveCard(card, destIndex).then( response => {
                // card.nextId = response.data.next_card_id
                resolve(response.data)
            }).catch( error => {
                reject(error)
            })
        })
    }

    // Note that creating cards can generate CardUpdate events before card has been added to current
    // project, therefore not in currentCards. Not an error, since updated card will be inserted.
    @action refreshCard = (cardId, event) => {
        this.getCardById(cardId).then( refreshedCard => {
            refreshedCard.rtEvent = event
            let idx = this.currentCards.findIndex( card => card.id === cardId )
            if (idx >= 0) {
                let oldCard = this.currentCards.splice (idx, 1, refreshedCard)
            } else {
                console.warn('refreshCard: card not in current cards (yet)')
            }

            if (this.isFiltering) {
                idx = this.filteredCards.findIndex( card => card.id === cardId )
                if (idx >= 0)
                    this.filteredCards.splice(idx, 1, refreshedCard)
            }
        })
    }

    // TODO direct API call or mobx action needed?
    @action tagCard = function(cardId, tag) {
        return tagCard(cardId, tag)
    }

    // TODO direct API call or mobx action needed?
    @action cloneCardsIntoProject = (cardIds, project) => {
        if (!project)
            project = projectStore.currentProject

        return cloneCardsIntoProject(cardIds, project)
    }

    @action createCardFromText = (txt) => {
        return new Promise( (resolve, reject) => {
            if (!txt)
                reject()

            const pId = projectStore.currentProject? projectStore.currentProject.id : 0
            const card = {id: 'new', title: 'Note - added ' +new Date().toLocaleTimeString(), description: truncate(txt, 1024)}
                saveCard(pId, card).then( response => {
                resolve (card)
            }).catch( saveError => reject(saveError) )
        })
    }

    @action createCardFromHtml = (html) => {
        return new Promise( (resolve, reject) => {
            if (!html)
                reject()

            const pId = projectStore.currentProject? projectStore.currentProject.id : 0
            const card = {id: 'new', title: 'Note - added ' +new Date().toLocaleTimeString(), description: 'html:' +truncate(html, 1019)}
            saveCard(pId, card).then( response => {
                resolve (card)
            }).catch( saveError => reject(saveError) )
        })
    }

    @action createCardFromUrl = (url) => {
        return new Promise( (resolve, reject) => {
            if (!url)
                reject()

            url = normalizeUrl(url)
            const urlParts = getUrlComponents (url)
            let card = new CardModel()
            WebImportService.fetchEverythingFromUrl (url).then (response => {
                const metaTags = response.data.metaTags

                card.title = WebImportService.getNameFromMetaTags (metaTags)
                if (!card.title) {
                    const domain = getShortUrl (url, true)
                    card.title = (domain === 'docs.google.com')? 'Google doc' : domain
                }
                card.image = WebImportService.getImageFromMetaTags (metaTags)
                const dim = WebImportService.getImageDimensionsFromMetaTags (metaTags)
                if (dim) {
                    card.imageWidth = dim.width
                    card.imageHeight = dim.height
                }

                card.url = urlParts.href
                card.description = WebImportService.getDescFromMetaTags (metaTags)
                card.source = WebImportService.getHostFromMetaTags (metaTags) || urlParts.host.toLowerCase ()
                card.sourceIcon = WebImportService.getIconFromMetaTags (metaTags)
                card.sourceType = EventSource.ScrapeUrl
                // card.type = urlType requires another async call
                card.domId = 'siteCard'

                // scale image if needed
                const pId = projectStore.currentProject? projectStore.currentProject.id : 0
                saveCard(pId, card).then( response => {
                    resolve (card)
                }).catch( saveError => reject(saveError) )
            }).catch (error => {
                console.log('Error fetching everything from url: ' +url)
            })

        })
    }

    @action createCardsFromAssets = (assets, project) => {
        if (!project)
            project = projectStore.currentProject

        // TODO rather than clear out inline image data assets, support them using same methods as file uploads.
        // let filteredAssets = assets.filter( asset => !asset.image || (!asset.image.startsWith('data:') && asset.image.length < 300) )
        assets.map( asset => {
            if (!asset.image || asset.image.startsWith('data:') || asset.image.length > 500)
                asset.image = null // TODO
        })

        return createCardsFromAssets(assets, project)
    }

/*
    @action createCardsFromDropbox = (assets, project) => {
        if (!project)
            project = projectStore.currentProject

        return createCardsFromAssets(assets, project, 'dropbox')
    }
*/

    // TODO not an action. Replace with direct api call
    createCardsFromFiles = (files, project, source) => {
        if (!project)
            project = projectStore.currentProject

        return createCardsFromFiles(files, project, source)
    }

    // TODO mobx action needed? direct api call
    @action attachFilesToCards = (cards, fileRefs, callback) => {
        attachFilesToCards(cards, fileRefs, callback)
    }

    @action attachDropboxImagesToCards = (cards, assets, callback) => {
        assets.forEach( (asset, i) => {
            if (asset.image) {
                DropboxService.getFileImage(asset).then( imageResponse => {
                    let {id, path, image} = imageResponse.data
                    const binaryImage = image // atob(image) // test with base64
                    const matchingCard = cards.find ( card => card.card_description === path) // TODO use url or path when api supports
                    if (matchingCard)
                        sendImage(binaryImage, true, callback, matchingCard.card_id, i+1)

                }).catch( error => {
                    debugger
                    console.error('Error getting file image from ', currDropboxer, error)
                })
            }
        })

        return attachFilesToCards(cards, assets, callback)
    }


    @computed get cardCount() {
        let parentProject = projectStore.currentProject
        return parentProject.cards.length
    }

    // TODO direct api call
    @action deleteCard = (cardId, projectId) => {
        return deleteCard(cardId, projectId)
    }

    // TODO direct api call
    @action deleteCards = (cardIds, projectId) => {
        if (projectId && cardIds && cardIds.length > 0)
            return deleteCards(cardIds, projectId)
    }

    @action addComment = (card, commentText) => {
        return new Promise((resolve, reject) => {
            createCommentApi(commentText, card.id).then( response => {
                console.log("Add: Response from Comment API: " +response)
                if (response.data) {
                    if (response.data.comment_text !== commentText) {
                        console.warn("Api comment text differs from submitted comment. Submitted: " +commentText+ ", api: " +response.data.comment_text)
                    }
                    resolve(response.data)
                } else {
                    reject('no data returned from add comment endpoint')
                }
            }).catch(error => {
                reject('Add comment error: ' +error)
            })
        })
    }

    @action deleteComment = (commentId) => {
        return new Promise((resolve, reject) => {
            deleteCommentApi(commentId).then( response => {
                console.log("Delete: Response from Comment API: " +response)
                if (response.data) {
                    resolve(response.data)
                } else {
                    reject('no data returned from del comment endpoint')
                }
            }).catch(error => {
                reject('Delete comment error: ' +error)
            })
        })
    }

    // Card reactions

    getDefaultReactionSet = () => { return this.reactionSets.find ( rs => rs.reaction_set_title.indexOf('Thumbs') === 0) }

    getReactionSetById = (id) => { return this.reactionSets.find( rs => rs.reaction_set_id === id)}

    @action addReaction = (card, reactionTypeId, reactionValue) => {
        return ReactionApiService.registerReaction(card.id, reactionTypeId, reactionValue)
    }

    @action deleteReaction = (card, reactionTypeId, reactionId) => {
        return ReactionApiService.unregisterReaction(card.id, reactionTypeId, reactionId)
    }

}

export default new cardStore()
// export default crap

