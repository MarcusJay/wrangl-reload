const withBundleAnalyzer = require("@zeit/next-bundle-analyzer");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");

module.exports = withBundleAnalyzer({
    // useFileSystemPublicRoutes: false,
    analyzeServer: ["server", "both"].includes(process.env.BUNDLE_ANALYZE),
    analyzeBrowser: ["browser", "both"].includes(process.env.BUNDLE_ANALYZE),
    bundleAnalyzerConfig: {
        server: {
            analyzerMode: 'static',
            reportFilename: '../../bundles/server.html'
        },
        browser: {
            analyzerMode: 'static',
            reportFilename: '../bundles/client.html'
        }
    },

    webpack: (config, { dev }) => {
        if(!dev) {
            config.devtool = 'source-map'
        }
        config.module.rules.push({
                test: /\.(css|scss|md)/,
                loader: 'emit-file-loader',
                options: {
                    name: 'dist/[path][name].[ext]',
                }
            }, {
                test: /\.md/,
                loader: 'raw-loader',
            }, {
                test: /\.css$/,
                use: ['babel-loader', 'raw-loader', 'postcss-loader']
            } , {
                test: /\.s(a|c)ss$/,
                use: ['babel-loader', 'raw-loader', 'postcss-loader',
                    { loader: 'sass-loader' }
                ]
            }
        )
        return config
    }
})


