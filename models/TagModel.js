export default class Tag {
    constructor(apiTag) {
        this.id = apiTag.tag_id
        this.name = apiTag.tag_text
        this.creatorId = apiTag.tag_creator
        this.color = apiTag.tag_color
        this.dateModified = apiTag.time_modified
    }

    static newTag(name, color) {
        return {name: name, color: color || 'white'}
    }
}