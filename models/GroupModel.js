export default class Group {
    constructor(apiGroup) {
        if (!apiGroup)
            return this
        this.id = apiGroup.group_id
        this.name = apiGroup.group_name
        this.creatorId = apiGroup.group_founder
        this.image = apiGroup.cover_image
        this.description = apiGroup.group_description
        this.privacy = parseInt (apiGroup.group_privacy || 0)
        this.dateModified = apiGroup.time_modified // TODO parse to Date obj
        this.dateCreated = apiGroup.time_created
        this.members = apiGroup.members_2
    }

    toApiPayload() {
        return {
            group_id: this.id,
            group_name: this.name,
            group_description: this.description,
            group_founder: this.creatorId,
            cover_image: this.image,
            group_privacy: this.privacy,
            time_modified: this.dateModified,
            time_created: this.dateCreated,
            members: this.members_2
        }
    }


}
