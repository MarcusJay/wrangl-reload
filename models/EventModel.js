export default class EventModel {
    constructor(apiEvent) {
        if (!apiEvent)
            return this
        this.id = apiEvent.event_id                   // id of this instance
        this.type = apiEvent.event_type         // id of this type of event
        this.level = apiEvent.event_level
        this.detail = apiEvent.event_detail
        this.source = apiEvent.source_type      // Constants.EventSource
        this.userId = apiEvent.user_id
        this.displayName = apiEvent.display_name
        this.projectId = apiEvent.project_id
        this.projectTitle = apiEvent.project_title
        this.projectType = apiEvent.project_type
        this.cardId = apiEvent.card_id
        this.antId = apiEvent.annotation_id
        this.commentId = apiEvent.comment_id
        this.reactionId = apiEvent.reaction_id
        this.reactionTypeId = apiEvent.reaction_type_id
        this.tagId = apiEvent.tag_id
        this.friendId = apiEvent.friend_id
        this.dateModified = new Date(apiEvent.time_modified)
        this.users = apiEvent.notify_users
        this.updatedKey = apiEvent.updated_key
        this.userBadgeMap = apiEvent.badges_by_user // present in incoming RT events, e.g. notify_users
        this.userBadges = this.user_badges // present in direct user_badges endpoint calls
    }

}

export const fieldToName = (field, short) => {
    switch(field) {
        case 'card_title': return 'Title'
        case 'card_description': return short? 'Desc': 'Description'
        case 'card_url': return 'Url'
        case 'card_image': return 'Image'
        default: return null
    }
}

