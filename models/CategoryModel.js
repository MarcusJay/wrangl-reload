export default class CategoryModel {
    constructor(apiCat) {
        this.id = apiCat.category_id
        this.title = apiCat.title
        this.projectIds = apiCat.projects || []
        this.sortIndex = apiCat.sort_index
        this.color = apiCat.color
    }
}