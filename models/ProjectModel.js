import CardModel from '/models/CardModel'
import UserModel from '/models/UserModel'
import TagModel from "./TagModel";
import {UserRole} from "../config/Constants";
import moment from "moment";

export default class ProjectModel {
    constructor(apiProject) {
        if (!apiProject) {
            this.title = "Untitled Board"
            this.description = ""
            this.comments = []
            this.initial_members = []
            // this.dateCreated = moment().format('YYYY-MM-DD hh:mm:ss');  // 2018-12-13 23:58:49
            return
        }
        this.id = apiProject.project_id
        this.domId = 'p' +apiProject.project_id.substring(0,18)
        this.title = apiProject.project_title
        this.creatorId = apiProject.project_creator
        this.image = apiProject.project_image
        this.description = apiProject.project_description

        // old: strings
        // this.tagSet = apiProject.project_tag_set || []
        // this.tags = apiProject.user_tags || []
        // new: objects
        this.tagSet2 = apiProject.tag_set || []
        this.tags2 = apiProject.user_tags || []

        this.securityLevel = apiProject.security_level
        this.projectType = parseInt (apiProject.project_type || 0)
        this.status = parseInt (apiProject.project_status || 0)
        this.dateModified = apiProject.time_modified // TODO parse to Date obj
        this.dateCreated = apiProject.time_start

        // summary readonly fields present in user_projects endpoint
        this.cardTotal = apiProject.card_total
        this.cardCommentTotal = apiProject.card_comment_total
        this.memberTotal = apiProject.member_total || 0
        this.invitedMemberTotal = apiProject.invited_member_total || 0
        this.inactiveMemberTotal = apiProject.inactive_member_total || 0
        this.projectCommentTotal = apiProject.project_comment_total || 0

        this.reactionSetId = apiProject.reaction_set || null

        this.members = apiProject.members || []

        this.membersOld = apiProject.membersOld || []
        this.invitedMembersOld = apiProject.invited_members || []
        this.inactiveMembersOld = apiProject.inactive_members || []
        this.comments = apiProject.comments || []
        this.cards = apiProject.cards || []

        if (this.tags2 && this.tags2.length > 0 && typeof this.tags2[0] === 'object') // not just ids
            this.tags2 = this.tags2.map( tag => new TagModel( tag ))
        if (this.tagSet2 && this.tagSet2.length > 0 && typeof this.tagSet2[0] === 'object') // not just ids
            this.tagSet2 = this.tagSet2.map( tag => new TagModel( tag ))
        if (this.cards.length > 0 && typeof this.cards[0] === 'object') // not just ids
            this.cards = this.cards.map( card => new CardModel( card ))

        if (this.members.length > 0)
            this.members = this.members.map(member => new UserModel( member ))

    }

/*  None of these are visible. TODO how to expose public instance methods
    allMembers = function() {
        return this.members.concat(this.invitedMembers)
    }

    allMembersTotal = () => {
        return this.memberTotal + this.invitedMemberTotal
    }

    isMemberConfirmed = (member) => {
        return (this.members.find( m => m.id === member.id ) !== undefined)
    }

    containsMember = (memberId) => {
        return this.getMember(memberId) !== undefined
    }

    getMember = (memberId) => {
        return this.allMembers().find( member => member.id === memberId)
    }
*/

    // deprecate
/*
    toApiPayload() {
        return {
            project_id: this.id,
            project_title: this.title,
            project_description: this.description,
            security_level: this.securityLevel,
            tag_set: this.tagSet,
            user_tags: this.tags,
            project_creator: this.creatorId,
            project_image: this.image,
            project_status: this.status,
            project_type: this.projectType,
            time_modified: this.dateModified,
            time_start: this.dateCreated,
            // members: this.members,
            // cards: this.cards
        }
    }
*/

}

// Following utils are needed since api uses separate arrays rather than member.status,
// and has moved membership roles to a separate user project settings object.
/*
export const isInAllMembers = (userId, project) => {
    if (!project || !userId)
        return false

    return project.membersOld.findIndex (member => member.id === userId) !== -1 ||
        project.invitedMembersOld.findIndex (member => member.id === userId) !== -1 ||
        project.inactiveMembersOld.findIndex (member => member.id === userId) !== -1
}
*/

/*
export const isMember = (project, userId) => {
    if (!project || !project.members || !userId)
        return false
    return project.members.find (member => member.id === userId ) !== undefined
}
*/

export const isActive = (userId, project) => {
    if (!project || !project.members || !userId)
        return false

    const member = project.members.find (member => member.id === userId)
    return (member && [UserRole.ACTIVE, UserRole.ADMIN, UserRole.REQUESTOR, UserRole.APPROVER].includes(member.role))
}

export const hasRole = (userId, role, project) => {
    if (!project || !project.members || !userId || role === null)
        return false

    const member = project.members.find (member => member.id === userId)
    return (member && member.role === role)
}

export const getUserFromMembers = (userId, project) => {
    if (!project || !project.members || !userId)
        return false

    return project.members.find (member => member.id === userId) || null
}

// add and delete member from clientside member array to mirror state of server
// TODO Better would be endpoint to get members only
export const addMember = (user, project) => {
    if (!project || !user) {
        throw new Error('addMember: missing params')
    }

    const memberIdx = project.members.findIndex (member => member.id === user)
    if (memberIdx >= 0) {
        console.warn('User ' +user.displayName+ ' already belongs to project ' +project.title)
        return
    }
    project.members.unshift (user)
    project.memberTotal += 1
}

export const deleteMember = (userId, project) => {
    if (!project || !project.members || !userId) {
        throw new Error('deleteMember: missing params')
    }

    const memberIdx = project.members.findIndex (member => member.id === userId)
    if (memberIdx >= 0) {
        project.members.splice (memberIdx, 1)
        project.memberTotal -= 1
    } else {
        debugger
        throw new Error('deleteMember: Attempt to delete non-member ' +userId+ ' from project ' +project.title)
    }

}

