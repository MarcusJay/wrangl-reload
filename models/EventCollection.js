import moment from 'moment'

import * as EventConfig from '/config/EventConfig'
import {compareDates} from "../utils/solo/compareDates";

export default class EventCollection {
    events = [] // of EventModel
    mergedEvents = [] // of {event: EventModel, count: number}
    eventMap = {}

    constructor(events) {
        this.newEvents = events
        this.setEvents(events)

    }

    getMergedEvents() {
        return this.mergedEvents || []
    }

    setEvents(events) {
        if (!events)
            console.warn('EventCollection: no events. ')
        else {
            this.removeDupEvents(this.newEvents)
            this.mergeRelatedEvents(this.newEvents)
        }
    }

    removeDupEvents() {
        this.newEvents = this.newEvents.filter( (unusedEvent, index) => {
            return this.newEvents.findIndex (event => {
                return this.areEventsDuplicate (event, this.newEvents[index]) === index
            })
        })
    }

    mergeRelatedEvents() {
        let key, mostRecent, eventMap = {}
        this.newEvents.forEach( (event, i)  => {
            // if mergeable event exists, event.count++. else this is a new event.
            key = this.getEventKey(event, i)
            if (eventMap[key]) {
                eventMap[key].count++
                mostRecent = moment(event.dateModified).isAfter(eventMap[key].event.dateModified)? event.dateModified : eventMap[key].event.dateModified
                eventMap[key].event.dateModified = mostRecent
            } else {
                eventMap[key] = {event: event, count: 1}
            }
        })
        this.mergedEvents = Object.values( eventMap ) || []
        this.mergedEvents = this.mergedEvents.sort( (e1,e2) => compareDates(e2.event.dateModified,e1.event.dateModified) )
    }

    areEventsDuplicate(a, b) {
        return EventCollection.allPropsEqual(a, b, ['userId', 'type', 'projectId', 'cardId', 'commentId'])
    }

    getEventKey(event, i) {
        if (!event.userId) {
            event.userId = 'anon'
        }
        let key = event.userId + '-' + event.type
        if (event.projectId)
            key += ('-' +event.projectId)

        // to effectively prevent all merging, add i to key.
        // to enable merging, comment this line:
        // key += ('-' + i)

        switch (event.type) {
            // action to a project
            case EventConfig.CreateCard:
            case EventConfig.AddCardToProject:
            case EventConfig.CardDelete:
                if (event.cardId)   // TODO test this a bit, see if it's preferable
                    key += ('-' +event.cardId)
                break

            case EventConfig.AddCommentToProject:
                // if (event.commentId) // TODO test this a bit, see if it's preferable
                //     key += ('-' +event.commentId)
                break

            // action to a card
            case EventConfig.AddCommentToCard:
            case EventConfig.CardUpdate:
            case EventConfig.DeleteCommentFromCard:
                key += ('-' +event.cardId)
                break

            // potentially repetitive actions
            case EventConfig.AddReactionToCard:
            case EventConfig.DeleteReactionToCard:
                key += '-r'
        }
        return key
    }

    static allPropsEqual(a,b, props) {
        let prop
        for (let i=0 ;i<props.length; i++) {
            prop = props[i]
            if (a[prop] !== b[prop])
                return false
        }
        return true
    }

    // TODO: unneeded? They criteria by which the key is generated serves the purpose of this function.
    areEventsMergeable(a, b) {
        if (!EventCollection.allPropsEqual(a, b, ['userId', 'type', 'projectId']))
            return false

        switch (a.type) {
            // action to a project
            case EventConfig.CreateCard:
            case EventConfig.AddCardToProject:
            case EventConfig.AddCommentToProject:
            case EventConfig.CardDelete:
                return true

            // action to a card
            case EventConfig.AddCommentToCard:
            case EventConfig.AddReactionToCard:
            case EventConfig.CardUpdate:
            case EventConfig.DeleteCommentFromCard:
            case EventConfig.DeleteReactionToCard:
                return (a.cardId === b.cardId)

            default:
                return false
        }
    }


}

class EventUtils {

}