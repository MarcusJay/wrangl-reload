import {firstWord} from "../utils/solo/firstWord";
import {stripLeadingNumbers} from "../utils/solo/stripLeadingNumbers";

export default class User {
    constructor(apiUser) {
        if (!apiUser)
            return this
        this.id = apiUser.user_id
        this.domId = 'u'+apiUser.user_id.substring(0,18)
        this.status = apiUser.user_status
        this.role = apiUser.user_role
        this.image = apiUser.image
        this.displayName = apiUser.display_name || apiUser.email
        this.firstName = apiUser.first_name || firstWord(apiUser.displayName)
        this.lastName = apiUser.last_name
        this.bio = apiUser.user_bio
        this.email = apiUser.email
        this.phone = apiUser.phone
        this.socialProvider = apiUser.social_type

        this.pinned = apiUser.pinned === true || false

        // privacy settings
        this.isFirstNamePublic = apiUser.first_name_public
        this.isLastNamePublic = apiUser.last_name_public
        this.isEmailPublic = apiUser.email_public
        this.isPhonePublic = apiUser.phone_public

        this.dateModified = apiUser.time_modified
        this.dateCreated = apiUser.time_created // DB col does't exist
        this.lastActive = apiUser.latest_activity
    }

}
