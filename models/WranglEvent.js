// Card level events
export const EVENT_REACT = 'react'
export const EVENT_UNREACT = 'unreact'

export const EVENT_ADD_CARD = 'add_card'
export const EVENT_ADD_MULTIPLE_CARDS = "add_multi_cards" // via search multiselect, or drag&drop multiple files
export const EVENT_UPDATE_CARD = 'update_card'
export const EVENT_DEL_CARD = "del_card"

// Mixed events
export const EVENT_ADD_COMMENT = 'add_comment'
export const EVENT_DEL_COMMENT = 'del_comment'

// Wrangl level events
export const EVENT_USER_JOIN = 'user_joined'
export const EVENT_USER_LEAVE = 'user_left'
export const EVENT_USER_ADDED = 'user_added'

export const EVENT_UPDATE_WRANGL_TITLE = "udpate_wrangl"
export const EVENT_DEL_WRANGL = "del_wrangl"

export const ignoredEvents = [EVENT_UNREACT, EVENT_DEL_CARD, EVENT_USER_LEAVE, EVENT_USER_JOIN] // TODO adjust as desired
export const ignoredUsers = ["5E928686-81A4-395B-A0B4-2E4ED20C5CB2" ]

export default class WranglEvent {
    constructor(eventType, actor, action, target, parent, recipient, count, value) {
        this.eventType = eventType
        this.action = action
        
        this.actor = {id: actor.id, name: actor.displayName}
        this.count = (count !== undefined)? count : 1
        if (target) {
            if (target['id'])
                this.target = {id: target.id, title: target.title}
            else
                this.target = target
        }

        if (parent)
            this.target.parent = {id: parent.id, title: parent.title}

        // convert target/parent to choice/wrangl format per dev request
        if (!parent) { // target is a wrangl
            this.choice = null
            this.wrangl = {id: target.id, title: target.title}
        } else {
            this.choice = target
            this.wrangl = {id: parent.id, title: parent.title}
        }

        this.recipient = recipient
        this.value = value
        this.date = new Date().getTime()
    }

    setMessage(msg) {
        this.message = msg
    }

    static isEventValid(event) {
        return true;
    }

}
