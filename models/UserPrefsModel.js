import {viewNumToName} from '/config/Constants'

export default class UserPrefsModel {
    constructor(apiUserPrefs) {
        if (!apiUserPrefs) {
            this.id = 'default'
            this.projectView = 'grid'
            this.cardView = 'cards'
            this.cropImages = true
            this.showTags = false
            this.leftPaneOpen = true
            this.autoShowTour = false
            this.contactsOpen = false
            return
        }

        this.id = apiUserPrefs.user_id
        this.projectViewNum = apiUserPrefs.project_view // Integer.
        this.projectView = viewNumToName(this.projectViewNum)
        this.cardViewNum = apiUserPrefs.card_view
        this.cardView = viewNumToName(this.cardViewNum)
        this.cardSize = apiUserPrefs.card_size // Integer. pixels.
        this.cropImages = Boolean(apiUserPrefs.crop_images || false)

        this.promoCode = apiUserPrefs.promo_code
        this.promoEnd = apiUserPrefs.promo_end
        this.showTags = apiUserPrefs.show_tags
        this.autoShowTour = Boolean(apiUserPrefs.show_tour)
        this.leftPaneOpen = apiUserPrefs.left_pane_open
        this.rightPaneOpen = apiUserPrefs.right_pane_open
        this.mobileSBOpen = apiUserPrefs.mobile_sidebar_open

        this.favoritesOpen = apiUserPrefs.favorites_open
        this.contactsOpen = apiUserPrefs.contacts_open
        this.projectMembersOpen = apiUserPrefs.project_members_open

        this.selectedImportTab = apiUserPrefs.card_add_tab
        this.selectedLeftPaneTab = apiUserPrefs.left_pane_tab


    }

    isNew() {
        return this.projectViewNum === 0 || this.cardSize === 0
    }

}
