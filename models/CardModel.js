import TagModel from "./TagModel";
import {EventSource} from "../config/EventConfig";
import {generateGUID} from "../utils/solo/generateGUID";
import {randomStr} from "../utils/solo/randomStr";
import {truncate} from "../utils/solo/truncate";
import {getExt} from "../utils/solo/getExt";
import {getShortUrl} from "../utils/solo/getShortUrl";
import normalizeUrl from 'normalize-url'
import {randomInt} from "../utils/solo/randomlnt";
// import {toAbsoluteUrl} from "../utils/solo/toAbsoluteUrl";
import {getHostname} from "../utils/solo/getHostname";

export default class Card {
    constructor(apiCard) {
        if (!apiCard) {
            this.id = 'new'
            this.title = 'Untitled'
            this.description = ''
            this.comments = []
            // this.urls = new Array(1)
            this.tags2 = []
            return this
        }
        this.id = apiCard.card_id
        this.indexLtr = apiCard.card_letter
        this.domId = 'c'+apiCard.card_id.substring(0,18)
        // this.nextId = apiCard.next_card_id
        this.sortIdx = apiCard.card_sort_index
        this.title = apiCard.card_title
        this.creatorId = apiCard.card_creator
        this.image = apiCard.card_image
        this.imageRotation = apiCard.card_image_rotation
        this.url = apiCard.card_url
        this.projectId = apiCard.project_id
        this.urls = apiCard.urls || ['']
        this.description = apiCard.card_description || ''
        this.dateModified = apiCard.time_modified // TODO parse to Date obj
        this.dateCreated = apiCard.time_created
        this.totalComments = apiCard.comment_total || 0
        this.comments = apiCard.comments || []
        this.reactions = apiCard.card_reactions || apiCard.reactions || []

        this.source = apiCard.card_source
        this.sourceType = apiCard.source_type
        this.sourceIcon = apiCard.card_source_icon

        this.tags2 = apiCard.card_tags || []
        if (this.tags2.length > 0 && typeof this.tags2[0] === 'object') // not just ids
            this.tags2 = this.tags2.map( tag => new TagModel( tag ))
        // 3. tag Ids are returned from tag searches
        this.tagIds = apiCard.tag_ids || []

        this.attachments = apiCard.attachments
        this.annotations = apiCard.annotations || []

    }

    static FromImportableAsset(asset, assetHostname) {
        let card = new Card()
        const providerName= assetHostname.replace('www.','')
        // const link = getUrlComponents(asset.href) may be relative path to provider.
        card.id = asset.wid
        card.title = truncate( asset.title || providerName, 63 )
        card.description = 'Imported ' +(new Date().toLocaleString())
        card.domId = 'c'+ card.id

        card.source = providerName
        card.sourceIcon = asset.icon
        card.sourceType = EventSource.ScrapeResults

        debugger
        card.image = normalizeUrl(asset.src)
        card.imageWidth = asset.width
        card.imageHeight = asset.height
        card.imageType = asset.type
        card.imageUnits = asset.hUnits

        const assetUrl = asset.url || asset.href
        card.urls = assetUrl? [normalizeUrl(assetUrl)] : [assetHostname]
        card.url = assetUrl? normalizeUrl(assetUrl) : assetHostname
        card.tags = []
        card.assetLink = asset.href
        return card
    }

    static FromWebSearch(item, searchProvider) {
        if (!item) {
            throw new Error ('fromWebSearch: item is undefined')
            debugger
        }
        let card = new Card()
        card.id = generateGUID()
        card.title = item.name? truncate ( item.name, 63 ) : item.name
        card.description = item.desc || getShortUrl(item.link)
        card.domId = 'c'+card.id.substring(0,18)
        card.image = (item.img && item.img.w > 100)? item.img.url : item.thumbnail.url // TODO try removing w > 0 condition, for larger images.
        card.urls = item.link? [item.link] : []
        card.url = item.link? item.link : null

        const host = card.url? getHostname(card.url) : null
        card.source = host || searchProvider
        // card.sourceIcon = /host? IconConfig.sourceToIcon(host) : item.thumbnail? item.thumbnail.url : null
        card.sourceType = EventSource.WebSearch

        card.tags = []
        return card
    }

    static FromFileUpload(file) {
        let card = new Card()
        card.id = generateGUID()
        if (file.name && !file.name.startsWith('image.'))
            card.title = truncate ( file.name, 63 )
        card.ext = getExt(file.name)
        card.type = file.type
        card.description = "Uploaded " +new Date().toLocaleDateString()
        card.domId = 'c'+card.id.substring(0,18)
        if (file.preview)
            card.image = file.preview
        card.url = null
        return card
    }

    static createMockCard() {
        let card = new Card()
        card.id = randomInt()
        // card.nextId = 0
        card.title = randomStr(10)
        card.creatorId = randomInt()
        // card.image = ''
        // card.url = 'http://google.com' // TODO multiple links
        card.description = randomStr(20)
        card.dateModified = new Date()
        card.dateCreated = new Date()
        return card
    }
}