# DISCUSSION
------------

## DISCUSSION RULES (DO NOT DELETE)
- Add Messages to Top of File
- Create Data Headline ex.. 09/17/17
- reference project members by [github](https://github.com "Visit Github") name @developerwok
- ref es6 learning [link](http://es6katas.org/)




#### 10/05/17
- Figure out how to intergrate semantic-ui sass files
- create an example of global styles being use in a component

#### 09/29/17
- preload/init should be more explict >> getInitProps
- how  manage linux box memmory? what tools?
- look at $ sudo apt-get install htop [link](http://hisham.hm/htop/)

#### 09/26/17
- figure out a way to call a function in axios, in either then or catch instances

#### 09/26/17
- should we move our initPage to our proxy API?
- attach unique keys to components
- what react dashboard lib can we use? https://strml.github.io/react-grid-layout/examples/0-showcase.html

#### 09/22/17
- template literals [link](https://ponyfoo.com/articles/es6-template-strings-in-depth)


#### 09/20/17
* example of handlers for micro [github](https://github.com/zeit/micro/issues/16)
* yarn global path is not automatic >> [github](https://github.com/yarnpkg/yarn/issues/1151#issuecomment-269195959)
* https://medium.com/@kevinsimper/dont-use-nodemon-there-are-better-ways-fc016b50b45e
* load config files with the node command `$ node --require 'dotenv/config' --require 'babel-register'`
* [link](https://github.com/zeit/micro-cluster)
* nextjs doesn't transpile server side, you need configure it [github](https://github.com/zeit/next.js/issues/1735)
* scaling node servers [link](https://medium.freecodecamp.org/scaling-node-js-applications-8492bd8afadc)


#### 09/19/17
* explore markdown to json [link](https://www.npmjs.com/package/markdown-to-json)

* techinal considerations

    * client
        * ui states
        * views
        * events
        * preference settings

    * generic projects data models
        * collection of collection
        * collection
        * items
        * attachments
        * users
        * events
        * clients

    * projects services
        * authenthication
        * pub/sub notifications
        * email
        * text messaging
        * logging
        * search
        * data ingestion
        * sockets

    * devops needs
        * cloud server management
        * dev server enviroment
        * deployment scripts/services
        * code repos
        * issue tracking

#### 09/16/17
* looked into documentation generator [link](http://www.davidboyne.co.uk/2016/05/26/automating-react-documentation.html)
* decide on code commenting rules [link](https://github.com/airbnb/javascript#comments)

#### 09/15/17
* incorportate markdown to jsx rendering

* add libs and structure folders

* how to run nextjs under node
    * node -> nextjs
    * https://jaketrent.com/post/serve-markdown-nextjs-server/
    * allows you to process node code prior to next js running
    * will be useful later

* markdown to jsx
    * used emit nextjs emit fileloader and markedown-to-js
    * markdown is not rendered serverside
    * production not tested

